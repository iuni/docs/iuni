#!/bin/bash

isDir(){
    local dirName=$1
    if [ ! -d $dirName ]; then
    return 1
    else
    return 0
    fi
}

recursionDir(){
    local dir=$1
    local filelist=`ls -tr "${dir}"`
    for filename in $filelist
    do
        local fullpath="$dir"/"${filename}";
        if isDir "${fullpath}";then
            recursionDir "${fullpath}"
        else
            if [ "${fullpath:(-5):5}" == "smali" ]; then
                echo "${fullpath}"
            fi
        fi
    done
}

rm -rf out
mkdir out
rm -rf filelist*

echo "apktool framework.jar."
if [ -f aurora/framework.jar ];then
    rm -rf aurora/framework.jar.out
    rm -rf original/framework.jar.out
    rm -rf original/framework2.jar.out
    if [ ! -f original/framework.jar ];then
        echo "error:can not find original framework.jar."
        exit 1
    fi

    ./apktool d -b aurora/framework.jar aurora/framework.jar.out
    ./apktool d -b original/framework.jar original/framework.jar.out
    if [ -f original/framework.jar ];then
        ./apktool d -b original/framework2.jar original/framework2.jar.out
    fi
    cat /dev/null  > filelist_framework.in
    recursionDir aurora/framework.jar.out/smali  | sed s#aurora/framework.jar.out/##g >> filelist_framework.in
fi

echo "apktool framework2.jar."
if [ -f aurora/framework2.jar ];then
    rm -rf aurora/framework2.jar.out

    ./apktool d -b aurora/framework2.jar aurora/framework2.jar.out
    cat /dev/null  > filelist_framework2.in
    recursionDir aurora/framework2.jar.out/smali  | sed s#aurora/framework2.jar.out/##g >> filelist_framework2.in
fi


echo "apktool android.policy.jar"
if [ -f aurora/android.policy.jar ];then
    rm -rf aurora/android.policy.jar.out
    rm -rf original/android.policy.jar.out
    if [ ! -f original/android.policy.jar ];then
        echo "error:can not find original android.policy.jar."
        exit 1
    fi
    
    ./apktool d -b aurora/android.policy.jar aurora/android.policy.jar.out
    ./apktool d -b original/android.policy.jar original/android.policy.jar.out
    cat /dev/null  > filelist_policy.in
    recursionDir aurora/android.policy.jar.out/smali  | sed s#aurora/android.policy.jar.out/##g >> filelist_policy.in
fi

echo "apktool service.jar"
if [ -f aurora/services.jar ];then
    rm -rf aurora/services.jar.out
    rm -rf original/services.jar.out
    if [ ! -f original/services.jar ];then
        echo "error:can not find original services.jar."
        exit 1
    fi
    ./apktool d -b aurora/services.jar aurora/services.jar.out
    ./apktool d -b original/services.jar original/services.jar.out
    cat /dev/null  > filelist_service.in
    recursionDir aurora/services.jar.out/smali  | sed 's#aurora/services.jar.out/##g' >> filelist_service.in
fi

add_insideapi(){
     tmp_string=${1}
     cat /dev/null > textview_tmp.in
     cat_string=`echo ${tmp_string} | sed 's#\/#\\\/#g'`
     sed -n "/^${cat_string}/,/^.end method/p" ${src_file} | grep aurora >> textview_tmp.in
     sed 's#\/#\\\/#g' -i textview_tmp.in
     sed 's#{#\\\{#g' -i textview_tmp.in
     sed 's#}#\\\}#g' -i textview_tmp.in

     while read -r line
     do
         trans_bracket=`echo ${tmp_string} | sed 's#(#\\\(#g' | sed 's#)#\\\)#g' | sed 's#\/#\\\/#g'`
         sed -r -i "/^${trans_bracket}/,/^.end method/{;s/return-void/${line}\n     &/g}" ${dst_file}

     done < textview_tmp.in

}

if [ -s filelist_framework.in ];then
     echo "smali/android/widget/TextView.smali"
     src_file=aurora/framework.jar.out/smali/android/widget/TextView.smali
     
     if [ -f original/framework.jar.out/smali/android/widget/TextView.smali  ];then
         dst_file=original/framework.jar.out/smali/android/widget/TextView.smali
     else
         dst_file=original/framework2.jar.out/smali/android/widget/TextView.smali
     fi
     cat /dev/null > textview_method.in
     cat /dev/null > textview_field.in
     grep aurora  ${src_file} | grep method | grep -v aurora_org >> textview_method.in
     grep aurora  ${src_file} | grep field >> textview_field.in

     sed -i 's#\/#\\\/#g' textview_method.in
     sed -i 's#\/#\\\/#g' textview_field.in

     while read -r line
     do
         sed -n "/^${line}/,/^.end method/p" ${src_file} >> ${dst_file}

     done < textview_method.in

     while read -r line
     do
         sed "60 a${line}\n" -i ${dst_file}

     done < textview_field.in

fi

if [ -s filelist_framework.in ];then
     echo "smali/android/text/Layout.smali"
     src_file=aurora/framework.jar.out/smali/android/text/Layout.smali

     if [ -f original/framework.jar.out/smali/android/text/Layout.smali  ];then
         dst_file=original/framework.jar.out/smali/android/text/Layout.smali
     else
         dst_file=original/framework2.jar.out/smali/android/text/Layout.smali
     fi
     cat /dev/null > layout_method.in
     cat /dev/null > layout_field.in
     grep aurora  ${src_file} | grep method | grep -v aurora_org >> layout_method.in
     grep aurora  ${src_file} | grep field >> layout_field.in

     sed -i 's#\/#\\\/#g' layout_method.in
     sed -i 's#\/#\\\/#g' layout_field.in

     while read -r line
     do
         sed -n "/^${line}/,/^.end method/p" ${src_file} >> ${dst_file}

     done < layout_method.in

     while read -r line
     do
         sed "60 a${line}\n" -i ${dst_file}

     done < layout_field.in

fi

aurora_replace(){

sed -i 's#\[#\\\[#g' ${1}
sed -i 's#\$#\\\$#g' ${1}
sed -i 's#\/#\\\/#g' ${1}
while read -r line
do
    echo "${file}:${line}"
    if [[ ${line} =~ "aurora_org_constructor_init" ]];then
        para_string=`echo ${line} | grep -oP '(?<=\().*(?=\))'`
        construct_api=`grep "constructor <init>(${para_string})" ${src_file_path}`
        transferr_api=`echo ${construct_api} | sed 's#\/#\\\/#g'`
        insert_string=`sed -n "/^${transferr_api}/,/^.end method/p" ${src_file_path} | grep "aurora_org_constructor_init" | sed 's#\/#\\\/#g' | sed 's#{#\\\{#g' | sed 's#}#\\\}#g'`
        trans_bracket=`echo ${transferr_api} | sed 's#(#\\\(#g' | sed 's#)#\\\)#g'`
        sed -r -i "/^${trans_bracket}/,/^.end method/{;s/return-void/${insert_string}\n     &/g}" ${dst_file_path}

        sed -n "/^${line}/,/^.end method/p" ${src_file_path} >> ${dst_file_path}
     else
        org_api_name=`echo $line | sed s#aurora_org_##g`
        sed -i "s/${org_api_name}/${line}/g" ${dst_file_path}
        sed -n "/^${org_api_name}/,/^.end method/p" ${src_file_path} >> ${dst_file_path}
     fi
done < ${1}

}

#framework.jar
echo "smali_jar:framework.jar"
if [ -s filelist_framework.in ];then
    for file in `cat filelist_framework.in`
    do
        cat /dev/null > filelist_org_api.in
        src_file_path=aurora/framework.jar.out/${file}       
        grep "aurora_org" ${src_file_path} | grep ".method" >> filelist_org_api.in
        
        if [ -s filelist_org_api.in ];then
            if [ -f original/framework.jar.out/${file} ];then
                 dst_file_path=original/framework.jar.out/${file}
            elif [  -f original/framework2.jar.out/${file}  ];then
                 dst_file_path=original/framework2.jar.out/${file}
            else
                 echo "Waring:${file} does not exist original system."
                 continue
            fi
            aurora_replace filelist_org_api.in
        fi
    done
    if [ -d aurora/framework.jar.out/smali/com/aurora ];then
    cp -rf aurora/framework.jar.out/smali/com/aurora original/framework.jar.out/smali/com
    fi
    sync

fi


echo "smali_jar:framework2.jar"
if [ -s filelist_framework2.in ];then
    for file in `cat filelist_framework2.in`
    do
        cat /dev/null > filelist_org_api.in
        src_file_path=aurora/framework2.jar.out/${file}
        grep "aurora_org" ${src_file_path} | grep ".method" >> filelist_org_api.in

        if [ -s filelist_org_api.in ];then
            if [ -f original/framework.jar.out/${file} ];then
                 dst_file_path=original/framework.jar.out/${file}
            elif [  -f original/framework2.jar.out/${file}  ];then
                 dst_file_path=original/framework2.jar.out/${file}
            else
                 echo "Waring:${file} does not exist original system."
                 continue
            fi
            aurora_replace filelist_org_api.in
        fi
    done
    if [ -d aurora/framework2.jar.out/smali/com/aurora ];then
    cp -rf aurora/framework2.jar.out/smali/com/aurora original/framework2.jar.out/smali/com
    fi

    sync
fi



if [ -d original/framework.jar.out ];then
    echo "code out/framework.jar"
    ./apktool b original/framework.jar.out out/framework.jar
fi

if [ -d original/framework2.jar.out ];then
    echo "code out/framework2.jar"
    ./apktool b original/framework2.jar.out out/framework2.jar
fi



#android.policy.jar
echo "smali_jar:android.policy.jar"
if [ -s filelist_policy.in ];then
    for file in `cat filelist_policy.in`
    do
        cat /dev/null > filelist_org_api.in
        src_file_path=aurora/android.policy.jar.out/${file}
        dst_file_path=original/android.policy.jar.out/${file}
        grep "aurora_org" ${src_file_path} | grep ".method" >> filelist_org_api.in

        if [ -s filelist_org_api.in ];then
            if [ ! -f ${dst_file_path} ];then
                echo "Waring:${file} does not exist original system."
                continue
            fi
            aurora_replace filelist_org_api.in
        fi
    done
    sync
    ./apktool b original/android.policy.jar.out out/android.policy.jar
fi


#services.jar
echo "smali_jar:service.jar"
if [ -s filelist_service.in ];then
    for file in `cat filelist_service.in`
    do
        cat /dev/null > filelist_org_api.in
        src_file_path=aurora/services.jar.out/${file}
        dst_file_path=original/services.jar.out/${file}
        grep "aurora_org" ${src_file_path} | grep ".method" >> filelist_org_api.in

        if [ -s filelist_org_api.in ];then
            if [ ! -f ${dst_file_path} ];then
                echo "Waring:${file} does not exist original system."
                continue
            fi
             aurora_replace filelist_org_api.in
        fi
    done
    sync
    ./apktool b original/services.jar.out out/services.jar
fi
rm *.in
echo "Done."
