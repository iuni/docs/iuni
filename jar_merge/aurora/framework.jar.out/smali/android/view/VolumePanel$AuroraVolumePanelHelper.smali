.class Landroid/view/VolumePanel$AuroraVolumePanelHelper;
.super Landroid/view/AuroraClassUtils;
.source "VolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/VolumePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AuroraVolumePanelHelper"
.end annotation


# instance fields
.field final context:Landroid/content/Context;

.field volumeService:Landroid/media/AudioService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioService;)V
    .locals 6

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/view/AuroraClassUtils;-><init>()V

    iput-object p1, p0, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->context:Landroid/content/Context;

    iput-object p2, p0, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->volumeService:Landroid/media/AudioService;

    const-string v0, "android.view.AuroraVolumePanel"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v4

    aput-object p2, v1, v5

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v4

    const-class v3, Landroid/media/AudioService;

    aput-object v3, v2, v5

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->getTarget(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public auroraPost(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->invokeMethod(Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public auroraPost(Ljava/lang/String;I)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, p1, v0, v1}, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->invokeMethod(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public auroraPost(Ljava/lang/String;II)V
    .locals 5

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, p1, v0, v1}, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->invokeMethod(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public auroraPost(Ljava/lang/String;Z)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, p1, v0, v1}, Landroid/view/VolumePanel$AuroraVolumePanelHelper;->invokeMethod(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
