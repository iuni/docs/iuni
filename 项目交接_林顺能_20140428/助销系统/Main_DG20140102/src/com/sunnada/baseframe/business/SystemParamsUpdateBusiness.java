/**
 * $RCSfile: SystemParamsUpdateBusiness.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.business;

import android.content.Context;
import android.content.Intent;

import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.database.KeyValueDao;
import com.sunnada.baseframe.database.MenuDao;
import com.sunnada.baseframe.database.MessageDao;
import com.sunnada.baseframe.database.NumberSegmentDao;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.ByteUtil;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.StringUtil;

/**
 * <p>Title: SystemParamsUpdateBusiness</p> 
 * <p>Description: 系统参数更新业务</p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-30
 * @version 1.0
 */

public class SystemParamsUpdateBusiness extends BaseBusiness
{
	/**
	 * 系统收到收件箱广播
	 */
	public static final String ACTION_MESSAGE_BOX = "com.sunnada.baseframe.action.MESSAGE_BOX";
	
	/**
	 * 菜单更新广播
	 */
	public static final String ACTION_MENU_UPDATE = "com.sunnada.baseframe.action.ACTION_MENU_UPDATE";
	
	public static final byte PARAM_TYPE_MENU_ID = 0x01;//菜单id
	public static final byte PARAM_TYPE_HAO_DUAN = 0x02;//号段
	public static final byte PARAM_TYPE_PRINT_AD = 0x03;//打印广告
	public static final byte PARAM_TYPE_MESSAGE_BOX = 0x04;//收件箱

	public static final String CRC_KEY_MENU = "CRC_KEY_MENU";//菜单crc的键值
	public static final String CRC_KEY_NUMBER_SEGMENT = "CRC_KEY_NUMBER_SEGMENT";//号段crc的键值
	public static final String CRC_KEY_PRINT_AD = "CRC_PRINT_AD";//打印广告crc的键值
	public static final String CRC_KEY_MESSAGE_BOX = "CRC_MESSAGE_BOX";//收件箱crc的键值
	
	private KeyValueDao keyValueDao = null;
	

	public SystemParamsUpdateBusiness(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService)
	{
		super(context, equipmentService, socketService,dataBaseService);
		keyValueDao = new KeyValueDao(dataBaseService);
	}
	
	public int updateAllParams()
	{
		int ret = ReturnEnum.FAIL;
		
		String error = "";
		
		ret = loadParams_0021(PARAM_TYPE_MENU_ID);
		
		if(ret != ReturnEnum.SUCCESS)
		{
			error +="菜单编码更新失败,";
		}
		
		ret = loadParams_0021(PARAM_TYPE_MESSAGE_BOX);
		
		if(ret != ReturnEnum.SUCCESS)
		{
			error +="收件箱更新失败,";
		}
		
		ret = loadParams_0021(PARAM_TYPE_HAO_DUAN);
		
		if(ret != ReturnEnum.SUCCESS)
		{
			error +="号段更新失败,";
		}
		
		ret = loadParams_0021(PARAM_TYPE_PRINT_AD);
		
		if(ret != ReturnEnum.SUCCESS)
		{
			error +="凭条广告信息更新失败,";
		}
		
		if(error.length()>0)
		{
			error = error.substring(0, error.length()-1);
			setLastknownError(error);
			return ReturnEnum.FAIL;
		}
		return ReturnEnum.SUCCESS;
	}
	
	public int heartBeat_0003()
	{
		Log.log("-----------------------------开始0003命令单元-----------------------------");
		int ret = ReturnEnum.FAIL;
		
		sendPackage = new Package();
		sendPackage.setId("00");
		sendPackage.setSubId("03");
		sendPackage.setServiceCommandID("FFFF");
		
		ret = send(sendPackage);
		if(ret!=ReturnEnum.SUCCESS)
		{
			return ret;
		}
		
		ret = receiveOnePackage("0011");
		if(ret!=ReturnEnum.SUCCESS)
		{
			return ret;
		}
		
		if(recivePackage.containsKey("0145"))
		{
			parseUnit0145(recivePackage);
		}
		Log.log("-----------------------------结束0003命令单元-----------------------------");
		return ret;
	}
	
	protected boolean parseUnit0145(Package package1)
	{
		boolean ret = false;
		
		if(package1.containsKey("0145"))
		{
			byte[] buf = package1.get("0145");
			byte[] crc = null;
			int map = 0;
			int pos = 0;
			
			map = Commen.bytes2int(buf, pos, 2);
			pos += 2;
			
			if((map & 0x01) != 0)
			{
				crc = ByteUtil.subBytesWithFromAndLength(buf, pos, 2);
				String crcStr = Commen.hax2str(new byte[]{crc[1],crc[0]});
				pos+=2;
				String oldCrc = getCrcByType(PARAM_TYPE_MENU_ID);
				Log.log("菜单下发CRC:", crcStr);
				Log.log("菜单本地CRC:", oldCrc);
				
				if(!crcStr.equals(oldCrc))
				{
					loadParams_0021(PARAM_TYPE_MENU_ID);
				}
				else
				{
					Log.log("CRC相同不更新");
				}
			}
			
			if((map & 0x02) != 0)
			{
				crc = ByteUtil.subBytesWithFromAndLength(buf, pos, 2);
				String crcStr = Commen.hax2str(new byte[]{crc[1],crc[0]});
				pos += 2;
				String oldCrc = getCrcByType(PARAM_TYPE_HAO_DUAN);
				Log.log("号段下发CRC:", crcStr);
				Log.log("号段本地CRC:", oldCrc);
				
				int crcValue = Commen.bytes2int(crc);
				if(crcValue>0)
				{
					loadParams_0021(PARAM_TYPE_HAO_DUAN);
				}
				else
				{
					Log.log("号段crc整型值为0，不更新");
				}
			}
			
			if((map & 0x04) != 0)
			{
				crc = ByteUtil.subBytesWithFromAndLength(buf, pos, 2);
				String crcStr = Commen.hax2str(new byte[]{crc[1],crc[0]});
				pos += 2;
				String oldCrc = getCrcByType(PARAM_TYPE_PRINT_AD);
				Log.log("打印广告下发CRC:", crc);
				Log.log("打印广告本地CRC:", oldCrc);
				
				if(!crcStr.equals(oldCrc))
				{
					loadParams_0021(PARAM_TYPE_PRINT_AD);
				}
				else
				{
					Log.log("CRC相同不更新");
				}
			}
			
			if((map & 0x08) != 0)
			{
				crc = ByteUtil.subBytesWithFromAndLength(buf, pos, 2);
				//String crcStr = Commen.hax2str(new byte[]{crc[1], crc[0]});
				pos += 2;
				String oldCrc = getCrcByType(PARAM_TYPE_MESSAGE_BOX);
				Log.log("收件箱下发CRC:", crc);
				Log.log("收件箱本地CRC:", oldCrc);
				
				int crcValue = Commen.bytes2int(crc);
				if(crcValue>0)
				{
					loadParams_0021(PARAM_TYPE_MESSAGE_BOX);
				}
				else
				{
					Log.log("收件箱crc整型值为0，不更新");
				}
			}
			ret = true;
		}
		return ret;
	}
	
	/**
	 * 更新系统CRC
	 * @param paramsType
	 * @param crc
	 * @return
	 */
	public int loadParams_0021(byte paramsType)
	{
		Log.log("-----------------------------开始0021命令单元-----------------------------");
		Log.log("请求类型paramsType = "+paramsType);
		int ret = ReturnEnum.FAIL;
		
		sendPackage = new Package();
		sendPackage.setId("00");
		sendPackage.setSubId("21");
		sendPackage.setServiceCommandID("FFFF");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add(paramsType);
		
		buffer.add("0000");
		sendPackage.addUnit("0146", buffer.getBytes());
		ret = send(sendPackage);
		
		if(ret!=ReturnEnum.SUCCESS)
		{
			return ret;
		}
		
		ret = receiveOnePackage("0021");
		
		if(ret!=ReturnEnum.SUCCESS)
		{
			return ret;
		}
		
		if(recivePackage.containsKey("0146"))
		{
			parseUnit0146(recivePackage);
		}

		Log.log("-----------------------------结束0021命令单元-----------------------------");
		return ret;
	}
	
	
	protected String getCrcByType(byte paramsType)
	{
		String crcStr = "00 00";
		
		switch (paramsType)
		{
		case PARAM_TYPE_HAO_DUAN:
			crcStr = keyValueDao.get(CRC_KEY_NUMBER_SEGMENT);
			break;
			
		case PARAM_TYPE_MENU_ID:
			crcStr = keyValueDao.get(CRC_KEY_MENU);
			break;
			
		case PARAM_TYPE_MESSAGE_BOX:
			crcStr = keyValueDao.get(CRC_KEY_MESSAGE_BOX);
			break;
			
		case PARAM_TYPE_PRINT_AD:
			crcStr = keyValueDao.get(CRC_KEY_PRINT_AD);
			break;

		default:
			break;
		}
		
		if(StringUtil.isEmptyOrNull(crcStr))
		{
			crcStr = "0000";
		}
		
		return crcStr;
	}
	
	/**
	 * 解析0146命令单元
	 * @param package1
	 * @return
	 */
	private boolean parseUnit0146(Package package1)
	{
		boolean ret = false;
		
		if(package1.containsKey("0146"))
		{
			byte [] buf = package1.get("0146");
			byte [] crc = new byte[2];
			byte [] data = null;
			byte paramsType = (byte) 0xFF;
			int pos = 0;
			int map = Commen.bytes2int(buf, pos, 2);
			pos+=2;
			
			if((map & 0x01)!=0)
			{
				paramsType = buf[pos];
				Log.log("返回类型paramsType = "+paramsType);
				pos++;
			}
			
			if((map & 0x02)!=0)
			{
				crc[0] = buf[pos];
				crc[1] = buf[pos+1];
				Log.log("返回crc",Commen.hax2str(crc));
				pos+=2;
			}
			
			if((map & 0x04) != 0)
			{
				data = ByteUtil.subBytesWithStartAndEnd(buf, pos, buf.length);
			}
			
			Log.log("paramsType = "+paramsType);
			switch (paramsType)
			{
			case PARAM_TYPE_MENU_ID:
				updateMenuIds(paramsType, crc, data);
				break;
			case PARAM_TYPE_HAO_DUAN:
				updateNumberSegment(paramsType, crc, data);
				break;
			case PARAM_TYPE_PRINT_AD:
				updatePrintAd(paramsType, crc, data);
				break;
			case PARAM_TYPE_MESSAGE_BOX:
				updateMessageBox(paramsType, crc, data);
				break;

			default:
				break;
			}
		}

		return ret;
	}
	
	/**
	 * 更新收件箱
	 * @param paramsType
	 * @param data
	 */
	private void updateMessageBox(byte paramsType,byte[] crc,byte[] data)
	{
		MessageDao dao = new MessageDao(dataBaseService);
		
		Log.log("----------开始更新收件箱----------");
		
		int pos= 0 ;
		int count = data[pos++];//消息总数
		Log.log("收件箱收到消息个数:"+count);
		dao.beginSQLBatch();
		for(int i = 0;i<count;i++)
		{
			int number = Commen.bytes2int(data, pos, 4);
			pos+=4;
			byte type = data[pos++];
			
			String typeStr = "";
			
			if(type == 00)
			{
				typeStr = "普通信件";
			
			}else if(type == 01)
			{
				typeStr = "重要信件";
				
			}else if(type == 02)
			{
				typeStr = "广告信件";
				
			}else
			{
				typeStr = "其他信件";
			}
			
			String date = StringUtil.encodeWithGBK(data, pos, 14);
			pos+=14;
			
			int len = Commen.bytes2int(data, pos, 1);
			pos++;
			String content = StringUtil.encodeWithGBK(data, pos, len);
			pos+=len;
			
			Log.log("收到消息:"+number+"-"+typeStr+"-"+date+"-"+content);
			
			dao.insert(number+"",typeStr, date, content);
			
		}
		keyValueDao.insert(CRC_KEY_MESSAGE_BOX, Commen.hax2str(crc));
		dao.executeSQLBath();
		
		if(count>0)
		{//发送系统广播
			Intent intent = new Intent(ACTION_MESSAGE_BOX);
			intent.putExtra("count", count);
			context.sendBroadcast(intent);
		}
		
		Log.log("----------更新收件箱结束----------");
	}

	
	/**
	 * 打印广告更新
	 * @param paramsType
	 * @param data
	 */
	private void updatePrintAd(byte paramsType,byte[] crc,byte[] data)
	{
		Log.log("----------开始更新打印广告----------");
		
		int pos= 0 ;
		int len = Commen.bytes2int(data,pos,pos+1);// data[pos];
		pos++;
		String ad = StringUtil.encodeWithGBK(data, pos, len);
		Log.log("打印广告信息:"+ad);
		keyValueDao.insert(KeyValueDao.KEY_PRINT_AD, ad);
		keyValueDao.insert(CRC_KEY_PRINT_AD, Commen.hax2str(crc));
		Log.log("----------更新打印广告结束----------");
	}
	
	
	/**
	 * 更新菜单ID
	 * @param paramsType
	 * @param data
	 */
	private void updateMenuIds(byte paramsType,byte[] crc,byte[] data)
	{
		MenuDao menuDao = new MenuDao(dataBaseService);
		
		Log.log("----------开始更新菜单编码----------");
		
		int pos= 0 ;
		int menuCount = 0;
		
		menuCount = data[pos];
		Log.log("菜单总数:"+menuCount);
		pos++;
		
		menuDao.beginSQLBatch();
		menuDao.deleteAllMneuId();//清空当前数据库菜单编码
		for(int i = 0;i<menuCount;i++)
		{
			byte[] code = new byte[2];
			System.arraycopy(data, pos, code, 0, 2);
			String menuCode = Commen.sprinthax(Commen.haxchange(code), "");
			pos+=2;

			int len = data[pos];
			pos += 1;
			String name = StringUtil.encodeWithGBK(data, pos, len);
			pos += len;
			Log.log("菜单:" + menuCode + " -- " + name);
			menuDao.insertMneuId(menuCode);
		}
		menuDao.executeSQLBath();
		keyValueDao.insert(CRC_KEY_MENU, Commen.hax2str(crc));
		
		
		Intent intent = new Intent(ACTION_MENU_UPDATE);
		context.sendBroadcast(intent);
		Log.log("----------更新菜单编码结束----------");
	}

	
	/**
	 * 更新号段
	 * @param paramsType
	 * @param data
	 */
	private void updateNumberSegment(byte paramsType,byte[] crc,byte[] data)
	{
		NumberSegmentDao dao = new NumberSegmentDao(dataBaseService);
		
		Log.log("----------开始更新号段----------");
		
		int pos= 0 ;
		int menuCount = 0;
		
		menuCount = data[pos];
		Log.log("号段总数:"+menuCount);
		pos++;
		dao.beginSQLBatch();
		for(int i = 0;i<menuCount;i++)
		{
			byte code = data[pos];
			pos++;

			int len = data[pos];
			pos += 1;
			String name = StringUtil.encodeWithGBK(data, pos, len);
			pos += len;
			Log.log("号段:" + code + " -- " + name);
			dao.insertOrUpdate(code+"", name);
		}
		dao.executeSQLBath();
		
		keyValueDao.insert(CRC_KEY_NUMBER_SEGMENT, Commen.hax2str(crc));
		Log.log("----------更新号段结束----------");
	}
}


