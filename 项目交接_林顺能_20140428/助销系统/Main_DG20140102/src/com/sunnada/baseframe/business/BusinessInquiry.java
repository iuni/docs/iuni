package com.sunnada.baseframe.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.equipment.IDevice;
import com.sunnada.baseframe.equipment.IntellgentDeviceOpr;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

// 统一余额播报协议
public class BusinessInquiry extends BaseBusiness
{
	public static final String				TITLE_STR				= "手机消费查询业务提示";	
	public static final String				INQUIRY_CODE			= "inquiry_code";
	public static final String				INQUIRY_NAME			= "inquiry_name";
	
	public byte								mType                   = 0x01;
	public String							mChargedate             = "";
	public boolean							CheckResult             = false;
	public String							mBalance                = "0";
	public String							mBusitype               = "";
	public IDevice							m_DeviceInc             = null;								// 蓝牙设备
	
	public int								mTotalPage              = 0;
	public int								mCurPage             	= 1;
	
	public List<Map<String, Object>>		mListBillData        	= new ArrayList<Map<String, Object>>();
	public String							mStrCurMonthDetail		= "";								// 当月实时话费 
    public String							mStrMobileBillContent	= "";								// 手机账单内容
    
	public BusinessInquiry(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		m_DeviceInc = new IntellgentDeviceOpr(context);
	}
	
	// 获取随机验证码
	public boolean bll010D(String strTelNum) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("0D");
		sendPackage.setId("01");
		
		// 号码(手机号码、固话号码、宽带号码)
		sendPackage.addUnit("0205", strTelNum.getBytes());
		
		// 发收
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			DialogUtil.showMessage(TITLE_STR, socketService.getLastKnowError());
			return false;
		}
		ret = receiveOnePackage("010D");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取验证码失败!";
			if(!StringUtil.isEmptyOrNull(ss)) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}

	// 随机码验证
	public boolean bll010E(String strTelNum, String strRandom)
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0E");
		sendPackage.setId("01");
		
		// 号码
		sendPackage.addUnit("0205", strTelNum.getBytes());
		// 随机码
		sendPackage.addUnit("0207", strRandom.getBytes());

		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			DialogUtil.showMessage(TITLE_STR, socketService.getLastKnowError());
			return false;
		}
		ret = receiveOnePackage("010E");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "校验验证码失败，请重新输入!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.closeProgress();
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		// 当月实时话费
		if (!recivePackage.containsKey("0324")) 
		{
			String ss = "校验验证码失败!\n失败原因: [服务器响应数据非法, 缺少0324命令单元]";
			DialogUtil.closeProgress();
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		} 
		else 
		{
			byte[] unit = recivePackage.get("0324");
			int offset = 0;

			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset + 1]});
			offset += 2;
			// 查询项是否有数据
			if ((map & 0x01) != 0) 
			{
				int flag = unit[offset++];
				if (flag == 1) 
				{
					if ((map&0x02) != 0) 
					{
						offset += ((unit[offset]&0xFF) + 1);
					}
					// 当月实时话费
					if ((map&0x04) != 0) 
					{
						int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
						offset += 2;
						//Log.e("", "当月实时话费长度: " + len);
						mStrCurMonthDetail = StringUtil.encodeWithGBK(unit, offset, len);
						offset += len;
						Log.e("", "当月实时话费详情: " + mStrCurMonthDetail);
					}
				}
			}
		}
		
		// 手机月账单列表内容
		if (!recivePackage.containsKey("0325")) 
		{
			String ss = "校验验证码失败!\n失败原因: [服务器响应数据非法, 缺少0325命令单元]";
			DialogUtil.closeProgress();
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		} 
		else 
		{
			byte[] unit = recivePackage.get("0325");
			int offset = 0;
			
			// 数据域
			int bitmap = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 是否有数据
			if ((bitmap & 0x01) != 0) 
			{
				int flag = unit[offset++];
				if (flag == 1) 
				{
					// 总共有几个月的账单
					int nBillsTotal = unit[offset++];
					Log.e("", "手机账单月份总数: " + nBillsTotal);
					// 月账单列表长度
					int nBillsLen = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
					offset += 2;
					if (nBillsLen > 500) 
					{
						String ss = "校验验证码失败!\n失败原因: [服务器响应数据非法, 手机月账单列表长度非法]";
						DialogUtil.showMessage(TITLE_STR, ss);
						setLastknownError(ss);
						return false;
					}
					
					// 清空账单列表数据
					mListBillData.clear();
					for (int i=0; i<nBillsTotal; i++) 
					{
						Map<String, Object> map = new HashMap<String, Object>();
						// 月账单编码
						int nInquiryCode = unit[offset++];
						map.put(INQUIRY_CODE, String.valueOf(nInquiryCode));
						// 月账单列表内容
						int len = unit[offset++];
						String strInquiryName = StringUtil.encodeWithGBK(unit, offset, len);
						offset += len;
						map.put(INQUIRY_NAME, strInquiryName);
						mListBillData.add(map);
					}
				}
			}
			DialogUtil.closeProgress();
			return true;
		}
    }
	
	// 手机账单内容
	public boolean bll010F(String strTelNum, int nInquiryCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0F");
		sendPackage.setId("01");
		
		Buffer buffer = new Buffer();
		buffer.add("0B00");
		buffer.add((byte)strTelNum.length());
		buffer.add(strTelNum.getBytes());
		buffer.add((byte)nInquiryCode);
		buffer.add((byte)mTotalPage);
		buffer.add((byte)mCurPage);
		sendPackage.addUnit("0323", buffer.getBytes());
		
		// 发收
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			return false;
		}
		ret = receiveOnePackage("010F");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取账单详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 手机账单详情
		if (!recivePackage.containsKey("0323")) 
		{
			String ss = "获取账单详情失败!\n失败原因: [服务器响应数据非法, 缺少0323命令单元]";
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0323");
			int offset = 0;
			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 查询账号
			if ((map & 0x01) != 0) 
			{
				offset += ((unit[offset]&0xFF) + 1);
			}
			// 查询项编码
			if ((map & 0x02) != 0) 
			{
				offset += 1;
			}
			// 查询内容
			if ((map & 0x08) != 0) 
			{
				// 查询总页数
				offset++;
				// 查询当前页数
				offset++;
				// 查询内容长度
				int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				// 查询内容
				mStrMobileBillContent = StringUtil.encodeWithGBK(unit, offset, len);
				Log.e("", "账单详情: " + mStrMobileBillContent);
			}
			return true;
		}
	}
    
    // 手机账单详情
	public boolean bll0110(String strTelNum, int nInquiryCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("10");
		sendPackage.setId("01");
		
		Buffer buffer = new Buffer();
		buffer.add("0B00");
		buffer.add((byte)strTelNum.length());
		buffer.add(strTelNum.getBytes());
		buffer.add((byte)nInquiryCode);
		buffer.add((byte)mTotalPage);
		buffer.add((byte)mCurPage);
		sendPackage.addUnit("0323", buffer.getBytes());

		// 发收
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			return false;
		}
		ret = receiveOnePackage("0110");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取查询子项详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		if (!recivePackage.containsKey("0323")) 
		{
			String ss = "获取查询子项详情失败!\n失败原因: [服务器响应数据非法, 缺少0323命令单元]";
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0323");
			int offset = 0;
			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 查询子项详情
			if ((map & 0x08) != 0) 
			{
				int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset++]});
				offset += 2;
				// 查询详情
				mStrMobileBillContent = StringUtil.encodeWithGBK(unit, offset, len);
				Log.e("", "查询子项详情: " + mStrMobileBillContent);
			}
			return true;
		}
	}
}
