package com.sunnada.baseframe.business;

import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.bluetooth.Commen;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

// 充值交费的基类
public abstract class BusinessCharge extends BaseBusiness 
{
	private Handler					mHandler			= null;
	public String 					mStrTelNum 			= "";
	public String					mStrPswd			= "";
	public String 					mStrNotifyNum		= "";					// 接收通知的电话
	public String 					mStrChargeMoney 	= "";
	public int 						mChargeMoney 		= 0;
	public String					mStrPreMoney 		= "";
	public byte 					mQueryType;
	
	public BusinessCharge(Context context, IEquipmentService equipmentService, ISocketService socketService, 
			IDataBaseService dataBaseService, Handler handler) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		mHandler 		= handler;
	}
	
	// 查询
	public boolean bll0103(int nQueryType, String[] strUserInfo) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("03");
		sendPackage.setId("01");
        
		Log.e("号码为：", mStrTelNum);
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add((byte)mStrTelNum.length()); 	// 手机号长度
		buffer.add(mStrTelNum.getBytes()); 		// 手机号码
		buffer.add((byte)nQueryType); 			// 查询类型
		sendPackage.addUnit("0321", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0103");
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "查询余额失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		
		// 解析
		if(recivePackage.containsKey("0321") == false)
		{
			setLastknownError("查询余额失败!\n失败原因: [服务器响应数据非法, 缺少0321命令单元]");
			return false;
		}
		else
		{
			StringBuffer resbuffer = new StringBuffer();
			byte[] unit = recivePackage.get("0321");
			int offset = 0;
			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 查询账号
			if ((map & 0x0001) != 0) 
			{
				int len = unit[offset++];
				String tempstr = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				resbuffer.append("充值号码:").append(tempstr).append("\n");
			}
			// 查询类型
			if ((map & 0x0002) != 0) 
			{
				offset += 1;
			}
			// 自业务查询类型
			if ((map & 0x0004) != 0) 
			{
				offset += 1;
			}
			// 查询日期
			if ((map & 0x0008) != 0) 
			{
				offset += 6;
			}
			// 查询详单起始日期
			if ((map & 0x0010) != 0) 
			{
				offset += 8;
			}
			// 查询详单截止日期
			if ((map & 0x0020) != 0) 
			{
				offset += 8;
			}
			// 当前余额
			if ((map & 0x0040) != 0) 
			{ 
				int remain = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1], unit[offset+2], unit[offset+3]});
				resbuffer.append("当前余额:").append(remain).append("\n");
			}
			// 查询内容
			if ((map & 0x0080) != 0) 
			{
				// 总页数
				offset++;
				// 当前页数
				offset++;
				// 文本长度
				int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				// 文本内容
				String tempstr = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				//
				//resbuffer.append("交易金额:" + mStrChargeMoney + "元\n");
				resbuffer.append(tempstr);
			}
			strUserInfo[0] = resbuffer.toString();
			return true;
		}
	}
	
	// 查询手机号用户名
	public boolean bll0105(String[] strUserInfo) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("05");
		sendPackage.setId("01");
		
		sendPackage.addUnit("0205", mStrTelNum.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0105");
		}
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "查询账户信息失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		
		// 解析数据
		if(recivePackage.containsKey("020D") == false) 
		{
			setLastknownError("查询账户信息失败!\n失败原因: [服务器响应数据非法, 缺少020D命令单元]");
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("020D");
			strUserInfo[0] = StringUtil.encodeWithGBK(unit, 0, unit.length);
			return true;
		}
	}
	
	// 获取流水号
	public boolean bll0201_0206(int type) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId(type==0x01? "01":"06");
		sendPackage.setId("02");
		
		// 交易类型
		sendPackage.addUnit("0202", new byte[] {(byte)mTranceType});
		// 交易密码
		Buffer buffer = new Buffer();
		byte[] pwd = PWDEncryption(mStrPswd);
		buffer.add((byte)pwd.length);
		buffer.add(pwd);
		sendPackage.addUnit("0215", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage(type==0x01? "0201":"0206");
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_PWD_FAIL, mHandler);
			setLastknownError(ss);
			return false;
		}
		
		// 解析数据
		if(recivePackage.containsKey("0202") == false) 
		{
			String ss = "获取流水号失败!\n失败原因: [服务器响应数据非法, 缺少0202命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0202");
			int len = unit[1]&0xFF;
			mStrBillNum = StringUtil.encodeWithGBK(unit, 2, len);
			Log.i(TAG, "业务流水号为" + mStrBillNum);
			return true;
		}
	}
	
	// 发送短信通知
	public boolean bll0205() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("05");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		buffer.add((byte)mStrTelNum.length());
		buffer.add(mStrTelNum.getBytes());
		buffer.add(mChargeMoney); 					// 金额
		buffer.add((byte)mStrNotifyNum.length());
		buffer.add(mStrNotifyNum.getBytes());
		System.err.println("通知的手机号码为: " + mStrNotifyNum);
		sendPackage.addUnit("0302", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0205");
		}
		
		// 解析
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发送短信通知失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_NOTIFY_FAIL, mHandler);
			setLastknownError(ss);
			return false;
		}
		else
		{
			DialogUtil.MsgBox(TITLE_STR, 
					"短信发送成功！", 
					"确定", 
					Statics.MSG_NOTIFY_OK, 
					null, 
					0x00, 
					Statics.MSG_NOTIFY_OK, 
					mHandler);
			return true;
		}
	}
	
	public abstract String backtype();

	public String getmStrTelNum() 
	{
		return mStrTelNum;
	}

	public void setmStrTelNum(String mStrTelNum) 
	{
		this.mStrTelNum = mStrTelNum;
	}
}
