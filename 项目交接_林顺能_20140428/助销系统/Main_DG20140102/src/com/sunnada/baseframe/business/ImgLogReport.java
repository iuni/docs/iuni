package com.sunnada.baseframe.business;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.database.ImgLogDBAdapter;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.FtpHelper;
import com.sunnada.baseframe.util.FtpHelper.UploadStatus;
import com.sunnada.baseframe.util.PictureShowUtils;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.PSAM_Data;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BaseBusiness;
import com.sunnada.baseframe.util.FileOpr;
import com.sunnada.baseframe.util.StringUtil;

import android.content.Context;
import android.os.SystemClock;

/**
 * 拍照日志上传的业务类
 * 
 * @author acer
 * 
 */
public class ImgLogReport extends BaseBusiness 
{
	ImgLogDBAdapter adapter = null;

//	protected InData indata;
//	protected Pdu pdu;
//	protected Buffer buffer;
	ImgLogDBAdapter imgLogDBAdapter;
//	String uploadUrl = SharedUtil.getString(main_activity,
//			SharedUtil.IMG_UPLOAD_URL, "");							
//	String uploadUsername = SharedUtil.getString(main_activity,
//			SharedUtil.IMG_UPLOAD_USERNAME, "");
//	String uploadPwd = SharedUtil.getString(main_activity,
//			SharedUtil.IMG_UPLOAD_PWD, "");
//	String uploadPort = SharedUtil.getString(main_activity, SharedUtil.IMG_UPLOAD_PORT, "21");
	
	// 以下4个数据应该从开机时获取.但是我看00代码好像没有这一块,大概是还没移植?
	String uploadUrl = "";							//图片上传的FTP地址				
	String uploadUsername = "";						//图片上传的用户名
	String uploadPwd = "";							//图片上传的密码
	String uploadPort = "";							//图片上传的端口号
	
	public ImgLogReport(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		adapter = new ImgLogDBAdapter(context);
		//buffer = new Buffer();
		//pdu = new Pdu();
		imgLogDBAdapter = new ImgLogDBAdapter(context);
	}

	/**
	 * 命令标识0x0F80(终端->平台)，终端发送身份信息。
	 * 
	 * 0x0F81 … ANS 数据域：0007 交易流水：strN uint1: 交易流水长度 strN(N-1): 交易流水 照片张数：uint1
	 * 照片名字： Uint2: 照片名字长度 strN(N-1): 照片名字
	 * 
	 * @param currBllNum
	 * @param photoNum
	 * @param photoNames
	 * @return
	 */
	public int bll0F80(String currBllNum, int photoNum, String photoNames) {
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("80");
		sendPackage.setId("0F");
		
		Buffer buffer = new Buffer();
//		Pdu pdu = new Pdu();
		buffer.clear();

		buffer.add("0700");
		buffer.add((byte) (currBllNum.length())); // 交易流水长度
		buffer.add(currBllNum.getBytes()); // 交易流水
		buffer.add((byte) (photoNum)); // 照片张数
		buffer.add((short) (photoNames.length())); // 照片名字长度
		buffer.add(photoNames.getBytes()); // 照片名字

		sendPackage.addUnit("0F81", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) {
			return ret;
		}

		ret = receiveOnePackage("0F80");
		
		return ret;
		
		
//		boolean boolResult = true;
//		indata = new InData();
//		indata.setServiceCommandID("FFFF");
//		indata.setSubid("80");
//		indata.setId("0F");
//		Pdu pdu = new Pdu();
//		buffer.clear();
//
//		buffer.add("0700");
//		buffer.add((byte) (currBllNum.length())); // 交易流水长度
//		buffer.add(currBllNum.getBytes()); // 交易流水
//		buffer.add((byte) (photoNum)); // 照片张数
//		buffer.add((short) (photoNames.length())); // 照片名字长度
//		buffer.add(photoNames.getBytes()); // 照片名字
//
//		pdu.put("0F81", buffer.getbytes());
//
//		indata.setPdu(pdu);
//		soket.AddSendBuff(indata);
//
//		if (!this.readoutdata("0F80", main_activity.iTimeout_busisubmit)) {
//
//			boolResult = false;
//		}
//
//		return boolResult;
	}

	private static byte[] uploadLock = new byte[0];					//文件上传的同步锁对象.创建byte[0]的代价是最小的
	
	/**
	 * 在心跳时执行0F80动作,及继续上传图片动作
	 */
	public synchronized void tickAction() 
	{
		synchronized (uploadLock) 
		{
			try 
			{
				List<Map<String, String>> logs = imgLogDBAdapter.queryImgLogs();
				if (logs == null || logs.size() > 0) {
					String bllNum, photoNum, photoNames = null;
					for (int i = 0; i < logs.size(); i++) {
						Map<String, String> imgLog = logs.get(i);
						bllNum = imgLog.get("BLLNUM");
						photoNum = imgLog.get("PHOTONUM");
						photoNames = imgLog.get("PHOTONAMES");
						int flag = bll0F80(bllNum, Integer.valueOf(photoNum), photoNames);
						if (flag==ReturnEnum.SUCCESS) {
							imgLogDBAdapter.deleteImgLog(bllNum);
						}
					}
				}
			} 
			catch (Exception e2) 
			{
				e2.printStackTrace();
			}
			
			try 
			{
				//处理未上传完成的图片
				List<Map<String, String>> uploadImgList = imgLogDBAdapter
						.queryUploadLogs();
				if (uploadImgList != null && uploadImgList.size() > 0) 
				{
					// 处理之前未上传完成的图片
					//String uploadUrl = SharedUtil.getString(main_activity, SharedUtil.IMG_UPLOAD_URL, "");
					//String uploadUsername = SharedUtil.getString(main_activity, SharedUtil.IMG_UPLOAD_USERNAME, "");
					//String uploadPwd = SharedUtil.getString(main_activity, SharedUtil.IMG_UPLOAD_PWD, "");
					//String uploadPort = SharedUtil.getString(main_activity, SharedUtil.IMG_UPLOAD_PORT, "21");
					FtpHelper myFtp = new FtpHelper();
					
					try 
					{
						myFtp.connect(uploadUrl, Integer.valueOf(uploadPort), uploadUsername, uploadPwd);
						String rootFolder = myFtp.ftpClient.printWorkingDirectory();
						try 
						{
							if(StringUtil.isEmptyOrNull(rootFolder)){
								rootFolder = context.getString(R.string.ftp_rootfolder);
							}
						} 
						catch (Exception e1) 
						{
							rootFolder = context.getString(R.string.ftp_rootfolder);
						}
						System.out.println("FTP文件上传连接成功");
						for (int i = 0; i < uploadImgList.size(); i++) 
						{
							Map<String, String> uploadImgInfo = uploadImgList.get(i);
							try 
							{
								UploadStatus status = myFtp.upload(rootFolder,
										uploadImgInfo.get("FILEPATH"),
										uploadImgInfo.get("REMOTEPATH"));
								// 上传成功
								if (status == UploadStatus.Upload_New_File_Success
										|| status == UploadStatus.Upload_From_Break_Success
										|| status == UploadStatus.File_Exits||
										status == UploadStatus.Local_Not_Exist) 
								{
									imgLogDBAdapter.deleteImgUploadLog(uploadImgInfo
											.get("FILEPATH"));
									
								}
								else 
								{
									continue;
								}
							} 
							catch (Exception e) 
							{
								e.printStackTrace();
								continue;
							}
							File img = new File(uploadImgInfo.get("FILEPATH"));
							try 
							{
								if(!img.exists()||img.delete()) 
								{
									continue;
								}
							} 
							catch (Exception e) 
							{
								e.printStackTrace();
							}
							// 文件删除失败,2秒后重新删除一次
							final File temp = img;
							new Thread() 
							{
								public void run() 
								{
									SystemClock.sleep(2000);
									try 
									{
										temp.delete();
									} 
									catch (Exception e) 
									{
										e.printStackTrace();
									}
								}
							}.start();
						}
						try 
						{
							myFtp.disconnect();
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}
					} 
					catch (Exception e1) 
					{
						e1.printStackTrace();
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			try 
			{
				File photoDirectory = new File(FileOpr.getSdcardPath()+"/mini/photo");
				if(photoDirectory.isDirectory()) 
				{
					File[] directorys = photoDirectory.listFiles();
					for (int i = 0; i < directorys.length; i++) 
					{
						// 找照片上级目录
						File temp = directorys[i];
						if(!temp.isDirectory()) 
						{
							continue;
						}
						// 只处理30分钟以外的文件
						if(temp.lastModified()>System.currentTimeMillis()-1000*60*30) 
						{
							continue;
						}
						// 非流水号目录,直接删除
						if(temp.getName().length()<15) 
						{
							FileOpr.delDir(temp.getAbsolutePath());
							continue;
						}
						addFileLog(temp);
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	// 把图片加入记录
	private void addFileLog(File jpgFolder) 
	{
		//directory
		File[] files = jpgFolder.listFiles();
		if(files == null || files.length == 0) 
		{
			jpgFolder.delete();
		}
		
		int photoCount = files.length;
		StringBuffer fileName = new StringBuffer(); // 图片名的拼接

		// 拼装所需要的各种参数
		for (int i = 0; i < files.length; i++) 
		{
			// 跳过bin文件
			if (!files[i].getName().endsWith(".jpg")) 
			{
				continue;
			}
			if (i != 0) 
			{
				fileName.append(",");
			}
			fileName.append(files[i].getName());
		}
		//Log.i("imageUploader", "照片文件:" + fileName);
		uploadImgs(files,jpgFolder.getName());
		updateImgLogInfo(jpgFolder.getName(), photoCount, fileName.toString());
		files = null;
	}

	/**
	 * 把要上传的文件路径等加入到本地数据库,等待上传
	 * 
	 * @param files
	 */
	private void uploadImgs(File[] files,String bllNum) 
	{
		final String textMark = "仅供中国联通开户专用";
		String psamId = "";
		if (Statics.IS_DEBUG) 
		{
			psamId = "51000000000001";
		} 
		else 
		{
			// PSAM卡号
			psamId = PSAM_Data.psaminfo.getPsamID();
		}
		// psamId="51000000000001";
		// 要提交的目录
		StringBuffer remotePath = new StringBuffer(context.getString(R.string.ftp_image_upload_folder));
		remotePath.append("/" + psamId.substring(0, 2));
		remotePath.append("/" + psamId.substring(2, 4));
		remotePath.append("/" + psamId);
		// 目录是"/Photos/省份/地市/psamid/流水号"的格式
		if (Statics.IS_DEBUG) 
		{
			remotePath.append("/" + "101906412341305220912340280701"); 
		} 
		else 
		{
			remotePath.append("/" +bllNum );
		}

		//Log.i("PHOTO_UPLOAD", "照片提交目录:" + remotePath);
		for (int i = 0; i < files.length; i++) 
		{
			File img = files[i];
			// 图像文件
			if (img.isFile() && img.getName().endsWith(".jpg")) 
			{
				try 
				{
					// 身份证照片打水印
					PictureShowUtils.addTextMark(img.getAbsolutePath(), textMark);
					// 比起号卡销售里, 这里多一步删除的
					adapter.deleteImgUploadLog(img.getAbsolutePath());
					adapter.addUploadLog(img.getAbsolutePath(), remotePath + "/" + img.getName());
					//Log.i("PHOTO_UPLOAD", img.getName() + "照片插入数据库成功");
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			} 
			// 身份证识别
			else if (img.isFile() && img.getName().endsWith(".bin")) 
			{
				try 
				{
					adapter.addUploadLog(img.getAbsolutePath(), context.getString(R.string.ftp_image_upload_folder)
											+ "/idfile/" + img.getName());
					//Log.i("PHOTO_UPLOAD", img.getName() + "bin文件插入数据库成功");
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			}
		}
		System.out.println("上传信息保存完毕");
	}

	// 本地缓存并上传照片日志信息
	private void updateImgLogInfo(final String bllNum, final int photoCount, final String fileName) 
	{
		synchronized (this) 
		{
			// 本地缓存
			// 比起号卡销售里, 这里多一步删除的
			adapter.deleteImgLog(bllNum);
			adapter.addImgLog(bllNum, String.valueOf(photoCount), fileName);
			//Log.i("PHOTO_UPLOAD", "照片日志上传:" + bllNum + "--" + photoCount + "张:" + fileName);
			
			int flag = 0;
			if (Statics.IS_DEBUG) 
			{
				flag = 1;
			} 
			else 
			{
				flag = bll0F80(bllNum, photoCount, fileName);
			}
			System.out.println("信息上报:" + flag);
			if (flag == ReturnEnum.SUCCESS) 
			{
				adapter.deleteImgLog(bllNum);
				//Log.i("PHOTO_UPLOAD", "照片日志删除:" + result);
			}
		}
	}
}
