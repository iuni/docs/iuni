package com.sunnada.baseframe.business;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sunnada.jni.Commcenter;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.ConsumePlan;
import com.sunnada.baseframe.bean.ContractData;
import com.sunnada.baseframe.bean.IdentifyMsg;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.PackageInfo;
import com.sunnada.baseframe.bean.Product3G;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.bean.contractBuff;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.StringUtil;

public class Business_0C extends BaseBusiness
{
	private String						lastKnownError			= "未知错误";
	public String						iccid;															
	public String						slectenum;														// 被选中的号码
	private String						smsNum					= "";
	private String						im						= "";
	public byte[]						IMSI;
	private String						search_type				= null;
	// private String sLastNum = null;
	// private String sNumtype = null;
	public static String				networkProtocol			= "";									// 入网协议
	public String						lineNum					= "12";
	public List<String>					resnum					= new ArrayList<String>();				// 号码列表
	public List<String>					telPrintBuffer			= new ArrayList<String>();				// 号码费用详情
	public int							telnumTotal				= 0;
	private String						sjNumber				= "0";
	private String						packageName				= null;									// 套餐名称
	public int							ProductCode				= 0;									// 套餐编码
	public static List<Rule>			numberRules				= new ArrayList<Rule>();				// 靓号规则
	public String						Identity;														// 身份证号
	public int							ProductPackCode			= 0;
	public List<Product3G>				productList				= new ArrayList<Product3G>();
	//private int						old_productcode;
	public String						pro_msginfo;													// 套餐详情
	//private String					packageDetail			= "暂无套餐详情";						// 套餐详情

	public List<contractBuff>			ls						= new ArrayList<contractBuff>();
	public int							contractflag			= 1;									// 合约标识
	public String						contractContent			= "";									// 合约计划内容
	public static List<Productzifei>	productZifei			= new ArrayList<Productzifei>();
	// 判断是否选择了合约计划，是否打印合约计划详情
	public boolean						ishyok					= false;								// 在获取费用的时候判断是否达到获取合约计划详情的条件，true条件到达
	public int							pid;															// 获取产品id
	public int							did;															// 获取合约期限id

	// 合约计划信息
	public static List<ContractData>	contractList			= new ArrayList<ContractData>();
	public PackageInfo					mPackageInfo			= null;
	public static int					mPayType				= 1;									// 付费类型

	public String						IdentityEndtime;
	public String						tjperson				= null;
	public int							openCardType			= 0;									// 0 成卡 1 白卡
	public String						cust_type				= "";									// 证件类型
	public String						username;
	public String						address;
	public String						sex;
	//private String						money					= "0";
	private String 						mFeiYongTotal			= "0.00";
	public String						feiyong;
	public int							zife;															// 资费编码
	// POS刷卡
	public int							postype					= 0;									// 刷卡类型
	public int							posresult				= 0;									// 刷卡结果
																										// 0-成功
	public int							pospay					= 0;									// 刷卡金额
	public String						posinfo					= null;									// 8583发起协议
	public byte[]						pos8583					= null;									// 8583返回结果
	// POS刷卡支付佣金数 
	public static int					posCommFalg				= 0;									// 佣金类别：第几期佣金
	public static int					posCommission			= 150;									// 佣金额度
	public static byte[]				posBanKID;														// 银行卡标示号
	public static int					posOptType				= 0;									// mispos类型

	public int							meclen					= 0;									// 支付机构长度
	public byte[]						mecbusflowno			= null;									// 支付机构流水
	public int							chanlen					= 0;									// 支付渠道长度
	public byte[]						chanbusflowno			= null;									// 支付渠道流水

	public String						res;
	public byte							tranceType;
	public String						password				= "888888";								// 输入的密码
	public String						user_id					= "888888";								// 输入的工号

	public byte[]						interCode				= null;									// 国际区域编码
	public byte[]						bllnum;															// 流水号
	public String						sBllnum;														// 流水号

	public int							taxType					= 0;									// 发票类型
	public String						taxNo					= null;									// 发票号码
	public String						taxContent				= null;									// 发票内容

	public String						t_crc					= "0000";								// 异常上报需要的开始的crc
	public byte[]						fuwumima;
	public String						fuwupwd;
	public byte							simtype;
	public boolean						isHaoyuanTypeFirst		= true;									// 是否是号源规则的第一次执行
	public StringBuffer					sPrintBuffer			= new StringBuffer();					// 打印信息
	public String						sp_packagename			= null;									// 获取套餐的名称
	public StringBuffer					proPrintBuffer			= new StringBuffer();					// 费用信息
	public String						Production				= " ";

	public static String				serviceCommandID0C03	= "";
	public String						sPicFolder				= "";
	public boolean						state					= true;

	public int							contractType			= 0;
	public int							productID;
	public String						contractNo				= "";
	public int							deadlineId;

	// 注销项目新增
	public int							m0D04AddQueryTerm		= 1;
	public List<String>					mTelFeeLevelsList		= new ArrayList<String>();				// 号码费用档次列表
	public List<Integer>				mConsumeLevels			= new ArrayList<Integer>();				// 消费档次
	public List<List<ConsumePlan>>		mConsumePlan			= new ArrayList<List<ConsumePlan>>();	// 消费计划
	public List<Integer>				mActType				= new ArrayList<Integer>();				// 活动类型列表
	public int 							mReWriteCount 			= 3;									// 重写白卡次数
	public boolean 						mIsWritting 			= false;								// 是否正在写卡
	// 交易时间
	public String						mTranceTime;
	
	public Business_0C(Context context, IEquipmentService equipmentService, ISocketService socketService,
			IDataBaseService dataBaseService)
	{
		super(context, equipmentService, socketService, dataBaseService);
		this.tranceType = 0x09;
	}

	// strType:合约机型 strID:终端串号
	public boolean bll0C01(String strType, String strID)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("01");
		sendPackage.setId("0C");

		Buffer buffer = new Buffer();
		buffer.clear();
		buffer.add("0300");
		buffer.add((byte) strType.length());
		buffer.add(strType.getBytes());
		buffer.add((byte) strID.length());// 终端串号（北六使用）
		buffer.add(strID.getBytes());
		sendPackage.addUnit("0801", buffer.getBytes());

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C01");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取合约计划失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		// 获取合约信息
		if (recivePackage.containsKey("0801"))
		{
			byte[] buf = recivePackage.get("0801");
			int count = 0;
			int len = 0;
			int pos = 2;

			count = Commen.bytes2int(buf, pos, 2);
			pos += 2;

			// 清空列表
			contractList.clear();

			// 循环获取数据
			for (; count > 0; count--)
			{
				ContractData conData = new ContractData();

				// 获取活动类型
				conData.actType = Commen.bytes2int(buf, pos, 1);
				pos++;
				
				// 获取活动类型名称
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				conData.actName = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;

				// 合约期限
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				conData.timeLimit = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;

				// 套餐ID
				conData.proID = Commen.bytes2int(buf, pos, 4);
				pos += 4;

				// 套餐金额
				conData.proMoney = Commen.bytes2int(buf, pos, 2);
				pos += 2;

				// 套餐付费类型
				conData.proType = Commen.bytes2int(buf, pos, 1);
				pos++;

				// 计划类型
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				conData.planType = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;

				// 合约ID
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				conData.planID = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;

				/* 20131029 修改了协议
				// 预存话费
				conData.actMoney = Commen.bytes2int(buf, pos, 2);
				pos += 2;

				// 手机金额
				conData.phoneMoney = Commen.bytes2int(buf, pos, 2);
				pos += 2;
				contractList.add(conData);
				*/
				// 合约内容
				len = Commen.bytes2int(buf, pos, 2);
				pos += 2;
				conData.planContent = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				contractList.add(conData);
			}
		}

		// 入网协议
		if (recivePackage.containsKey("049E"))
		{
			byte[] buf = recivePackage.get("049E");
			int pos = 0;
			int len = Commen.bytes2int(buf, pos, 2);
			networkProtocol = StringUtil.encodeWithGBK(buf, pos, len);
		}
		return true;
	}

	// 终端发送编码获取各类详情。
	// 参数 ：详情类型   各类编码(套餐ID)
	public int bll0C02(String timelimit, int actType, int proID, String planID)
	{
		System.out.println("actType :" + actType + "  proId:" + proID + " palnId :" + planID);
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("02");
		sendPackage.setId("0C");
		Buffer buffer = new Buffer();
		buffer.add("0F00");
		int limit = Integer.parseInt(timelimit);
		buffer.add((byte)limit);
		buffer.add((byte)actType);
		buffer.add(proID);
		buffer.add((byte)planID.length());
		buffer.add(planID.getBytes());

		sendPackage.addUnit("0495", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}
		ret = receiveOnePackage("0C02");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取各类详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return ret;
		}

		// 获取号码列表
		if (!recivePackage.containsKey("0495"))
		{
			String ss = "获取各类详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [缺少0495命令单元]";
			}
			setLastknownError(ss);
			return ret;
		}
		else
		{
			byte[] buf = recivePackage.get("0495");
			int map = 0;
			int len = 0;
			int pos = 0;

			map = Commen.bytes2int(buf, 0, 2);
			pos += 2;
			if ((map & 0x01) != 0)
			{
				pos++;
			}
			if ((map & 0x02) != 0)
			{
				pos += 4;
			}

			if ((map & 0x04) != 0)
			{
				mPackageInfo = new PackageInfo();
				// 国内语音拨打分钟数
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmCall(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内流量
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmFlow(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内短信发送条数
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmMsgCount(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 接听免费
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmFreeIncomingCall(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内语音拨打费用（元、分钟）
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmFeeCall(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内流量费用
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmFeeFlow(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内可视电话拨打费用
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmFeeVedioCall(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 其他业务
				len = Commen.bytes2int(buf, pos, 1);
				pos++;
				mPackageInfo.setmOther(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;
			}
		}
		return ret;
	}

	// 获取号码请求
	// search_type:选号类型 selectFirstType:第一个查询类型
	// selectFirstStr:第一个查询条件,如果无数据传""不能传NULL selectSecondType:第二个查询类型
	// selectSecondStr:第二个查询条件,如果无数据传""不能传NULL selectThirdType:第三个查询类型
	// paytype:付费类型 0x00:无号码类型 0x01:3G后付费号码 0x02:3G预付费号码
	// isFirst:是否第一次查询，首次查询不必传查询条件 is0491：是否需要传0491单元，翻页时不必传
	public boolean bll0C03(int selectFirstType, String selectFirstStr, int selectSecondType, String selectSecondStr,
			int selectThirdType, int paytype, short startnum, boolean isFirst, boolean is0491, short proMoney)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(serviceCommandID0C03);
		sendPackage.setSubId("03");
		sendPackage.setId("0C");
		Buffer buffer = new Buffer();
		if (is0491)
		{
			System.out.println("0C03 is0491...");
			if (isFirst)
			{
				System.out.println("0C03 isFirst...");
				buffer.add("0100");
				buffer.add((byte)paytype);
				mPayType = paytype;
			}
			else
			{
				buffer.add("0F00");
				buffer.add((byte) paytype);
				mPayType = paytype;
				// 第一类查询条件 靓号规则
				buffer.add((byte) selectFirstType);
				if (!selectFirstStr.equals(""))
				{
					buffer.add((byte) Integer.parseInt(selectFirstStr));
				}
				else
				{
					buffer.add(selectFirstStr.getBytes());
				}
				// 第二类查询条件 号段
				buffer.add((byte) selectSecondType);
				buffer.add(selectSecondStr.getBytes());
				// 第三类查询条件 价格区间
				buffer.add((byte) selectThirdType);
			}
			sendPackage.addUnit("0491", buffer.getBytes());
		}
		buffer.clear();
		buffer.add("0700");
		buffer.add("0000");		// 总,接入说这个标志没什么用
		buffer.add(startnum);	// 起始
		// buffer.add("0100");	// 起始
		buffer.add((byte) Integer.parseInt(lineNum));// 一页的行数
		sendPackage.addUnit("0492", buffer.getBytes());
		sendPackage.addUnit("0495", Commen.short2bytes(proMoney));

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C03");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取号码列表失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		// 获取号码列表
		if (!recivePackage.containsKey("0492"))
		{
			String ss = "获取号码列表失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [缺少0492命令单元]";
			}
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0492");
			byte[] bitmap = new byte[2];
			System.arraycopy(unit, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);
			// telnumTotal = unit[2] & 0xFF;
			// int count = unit[6] & 0xFF;
			telnumTotal = Commen.bytes2int(unit, 2, 2);
			Log.e("0C03", "total :" + telnumTotal);
			int count = Commen.bytes2int(unit, 6, 1);
			int begin = 7;
			resnum.clear();
			telPrintBuffer.clear();
			for (int i = 0; i < count; i++)
			{
				int len = Commen.bytes2int(unit, begin, 1);
				begin++;
				byte[] info = new byte[len];
				System.arraycopy(unit, begin, info, 0, len);
				begin += len;
				try
				{
					resnum.add(new String(info, "ASCII"));
					Log.i("haoka", new String(info, "ASCII"));
				}
				catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
			}

			// 获取号码详情
			if ((map & 0x0010) != 0)
			{
				System.out.println("count==" + count);
				int j = 0;
				for (j = 0; j < count; j++)
				{
					System.out.println("");
					int len = unit[begin] & 0xFF;
					if (0 == len)
					{
						byte[] info = "".getBytes();
						begin++;
						begin += len;
						try
						{
							telPrintBuffer.add(new String(info, "GBK"));
							Log.i("haoka", new String(info, "ASCII"));
						}
						catch (UnsupportedEncodingException e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						byte[] info = new byte[len];
						System.arraycopy(unit, ++begin, info, 0, len);
						begin += len;
						try
						{
							String str = new String(info, "GBK");
							telPrintBuffer.add(str);
							Log.i("haoka", new String(info, "ASCII"));
						}
						catch (UnsupportedEncodingException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		return true;
	}

	// 预占手机号
	// 参数：  预占类型
	public boolean bll0C04(byte orderType)
	{
		int ret = ReturnEnum.FAIL;
		@SuppressWarnings("unused")
		boolean boolResult = true;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("04");
		sendPackage.setId("0C");
		sendPackage.addUnit("0205", slectenum.getBytes());

		Buffer buffer = new Buffer();
		buffer.add("0300");

		// 对于合约机来说只有预付费非淘宝和后付费
		if (mPayType == 0x02)// 预付费非套包
		{
			// buffer.add("02");
			buffer.add(((byte) mPayType));
		}
		else// 后付费
		{
			buffer.add(((byte) mPayType));
			// buffer.add("01");
		}

		if (mPayType == 3)
		{
			Log.log("预付费套包，选好类型为0x04,号码为:" + sjNumber);
			buffer.add((byte) 0x04);// 选号类型
		}
		else
		{
			Log.log("选号类型:" + search_type);
			buffer.add((byte) 0);// 选号类型
		}
		sendPackage.addUnit("0491", buffer.getBytes());
		sendPackage.addUnit("0211", orderType);

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C04");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "预占号码失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		sjNumber = slectenum;
		Log.i("haokao", "预占号码:" + slectenum + "成功");

		if (recivePackage.containsKey("0494"))
		{
			int pos = 0;
			byte[] buf = recivePackage.get("0494");
			int len = buf[pos];
			pos += 1;
			iccid = StringUtil.encodeWithGBK(buf, pos, len);// iccid卡号
			// System.out.println("0C04.iccid=="+iccid);
			Log.log("iccid = " + iccid);
		}

		if (mPayType == 3 && recivePackage.containsKey("04D0"))
		{
			int pos = 0;
			byte[] buf = recivePackage.get("04D0");

			if (buf != null && buf.length > 0)
			{
				ProductCode = Commen.bytes2int(buf, pos, 4);// 套餐id
				pos += 4;
				int len = buf[pos];
				pos += 1;
				packageName = StringUtil.encodeWithGBK(buf, pos, len);// 套餐名称
				Log.log("套餐编码:" + ProductCode + " 套餐名称:" + packageName);
			}
			else
			{
				String ss = "预占号码失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [套餐未绑定]";
				}
				setLastknownError(ss);
				return false;
			}
		}
		return true;
	}

	// 获取靓号规则
	public boolean bll0C05()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		serviceCommandID0C03 = recivePackage.getServiceCommandID();
		sendPackage.setServiceCommandID(serviceCommandID0C03);
		sendPackage.setSubId("05");
		sendPackage.setId("0C");

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C05");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "预占号码失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		if (recivePackage.containsKey("0501"))
		{
			Log.log("====================开始解析0501单元====================");
			byte[] buf = recivePackage.get("0501");
			int pos = 0;

			numberRules.clear();
			// 是否有靓号
			if (buf[pos] == 1)// 有
			{
				// 20131112 不限靓号规则
				Rule rule = new Rule();
				rule.setId(-1);
				rule.setContent("不限");
				numberRules.add(rule);
				
				pos += 1;
				int len = Commen.bytes2int(new byte[]
				{ buf[pos], buf[pos + 1] });
				pos += 2;
				int count = buf[pos];// 规则总数搜索
				Log.log("有" + count + "个靓号");
				pos += 1;

				for (int i = 0; i < count; i++)
				{
					rule = new Rule();
					rule.setId(buf[pos]);
					pos += 1;
					len = buf[pos];
					pos += 1;
					rule.setContent(StringUtil.encodeWithGBK(buf, pos, len));
					pos += len;
					Log.log("靓号:	编码--" + rule.getId() + "	内容--" + rule.getContent());
					numberRules.add(rule);
				}
				ret = ReturnEnum.SUCCESS;
			}
			else
			{
				Log.log("无靓号");
			}
			Log.log("====================解析0501单元完成====================");
		}
		else
		{
			Log.log("xxxxxxxxxxxxxxxxxxxx无0501单元xxxxxxxxxxxxxxxxxxxx");
		}

		// 获取入网协议
		if (recivePackage.containsKey("049E"))
		{
			byte[] buf = recivePackage.get("049E");
			int pos = 0;
			int len = Commen.bytes2int(new byte[]
			{ buf[0], buf[1] });
			pos += 2;
			networkProtocol = StringUtil.encodeWithGBK(buf, pos, len);
			Log.log("入网协议:" + networkProtocol);
		}

		// 获取入网资费
		if (recivePackage.containsKey("049C"))
		{
			int haveData = 0x00;
			int pos = 0;
			int total = 0;
			int len = 0;
			byte[] unit = recivePackage.get("049C");
			haveData = unit[0];
			if (haveData == 0)
			{
				String ss = "获取资费信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [返回无数据]";
				}
				setLastknownError(ss);
				return false;
			}
			pos = 3;
			total = unit[pos];
			pos++;
			productZifei.clear();
			if (total > 0)
			{
				// pdaAdapter.deletebytype(ParamType.CURFEE3G);
			}
			for (int i = 0; i < total; i++)
			{
				Productzifei pz = new Productzifei();
				len = 0;
				// int code = unit[pos];
				pz.setCode(unit[pos]);
				pos++;
				len = unit[pos];
				pos++;
				byte[] info = new byte[len];
				System.arraycopy(unit, pos, info, 0, len);
				pz.setName(StringUtil.encodeWithGBK(info));
				pos += len;
				productZifei.add(pz);
				// pdaAdapter.insertData(ParamType.CURFEE3G, pz.getCode() + "",
				// pz.getName(), "", "");
				Log.e("haoka", "编码:" + pz.getCode() + "; 内容:" + pz.getName());
			}
		}
		return true;
	}

	// 获取套餐详情
	public boolean bll0C07()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("07");
		sendPackage.setId("0C");
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add((byte) 0x01); // 详情类型
		Log.log("详情编码:" + this.ProductCode);
		buffer.add(this.ProductCode); // 编码
		sendPackage.addUnit("0495", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}

		ret = receiveOnePackage("0C07");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取套餐详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		if (!recivePackage.containsKey("0495"))
		{
			String ss = "获取套餐详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [缺少0495命令单元]";
			}
			setLastknownError(ss);
			return false;
		}

		byte[] unit = recivePackage.get("0495");
		int len = 0;
		len = Commen.bytes2int(new byte[]
		{ unit[7], unit[8] });
		if (len > 400)
		{
			String ss = "获取套餐详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [数据太长]";
			}
			setLastknownError(ss);
			return false;
		}
		byte[] info = new byte[len];
		System.arraycopy(unit, 9, info, 0, len);

		//old_productcode = ProductCode;
		pro_msginfo = StringUtil.encodeWithGBK(info);
		Log.log("套餐详情:" + pro_msginfo);
		//setPackageDetail(pro_msginfo);
		Log.e("haoka", pro_msginfo);
		return true;
	}
	
	// 获取入网当月资费信息
	public boolean bll0C0A()
	{
		int ret = ReturnEnum.FAIL;
		try
		{
			Log.log("获取入网当月资费信息");
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("0A");
			sendPackage.setId("0C");

			// 发收
			send(sendPackage);
			if (ret != ReturnEnum.SUCCESS)
			{
				return false;
			}
			ret = receiveOnePackage("0C0A");
			if (ret != ReturnEnum.SUCCESS)
			{
				String ss = "获取资费信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				return false;
			}

			if (!recivePackage.containsKey("049C"))
			{
				String ss = "获取资费信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [缺少049C命令单元]";
				}
				setLastknownError(ss);
				return false;
			}
			int haveData = 0x00;
			int pos = 0;
			int total = 0;
			int len = 0;
			byte[] unit = recivePackage.get("049C");
			haveData = unit[0];
			if (haveData == 0)
			{
				String ss = "获取资费信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [返回无数据]";
				}
				setLastknownError(ss);
				return false;
			}
			pos = 3;
			total = unit[pos];
			pos++;
			productZifei.clear();
			if (total > 0)
			{
				// pdaAdapter.deletebytype(ParamType.CURFEE3G);
			}
			for (int i = 0; i < total; i++)
			{
				Productzifei pz = new Productzifei();
				len = 0;
				// int code = unit[pos];
				pz.setCode(unit[pos]);
				pos++;
				len = unit[pos];
				pos++;
				byte[] info = new byte[len];
				System.arraycopy(unit, pos, info, 0, len);
				pz.setName(StringUtil.encodeWithGBK(info));
				pos += len;
				productZifei.add(pz);
				// pdaAdapter.insertData(ParamType.CURFEE3G, pz.getCode() + "",
				// pz.getName(), "", "");
				Log.e("haoka", "编码:" + pz.getCode() + "; 内容:" + pz.getName());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			// if (pdaAdapter != null)
			// pdaAdapter.close();
		}
		return true;
	}
	
	// 获取费用信息
	public boolean bll0C0B()
	{
		try
		{
			/*
			if (!"".equals(pro_msginfo) && ProductCode == old_productcode)
			{
			} 
			else 
			{ 
				// 重新取下
				bll0C07(); 
			}
			// 重新执行一遍获取合约详情 
			if ("".equals(contractContent) && ishyok) 
			{ 
				bll0B09(did, pid); 
				ishyok = false; // 改为初始状态
			}
			*/
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("0B");
			sendPackage.setId("0C");
			
			// 3G后付费省略用户验证信息
			if (mPayType != 5) 
			{
				Buffer buffer = new Buffer();
				buffer.add("0200");
				buffer.add(IdentityEndtime.getBytes()); 		// 证件有效期
				buffer.add((byte)Identity.length());			// 证件号码长度
				buffer.add(Identity.getBytes());				// 证件号码
				sendPackage.addUnit("036D", buffer.getBytes());
			}
			
			Buffer buffer = new Buffer();
			int map = 0x2738;
			// 数据域, 3G后付费不含推荐人
			if (mPayType == 5 && StringUtil.isEmptyOrNull(tjperson)) 
			{
				Log.log("3G后付费不含推荐人");
				buffer.add("3827");
				map = 0x2738;
			}
			// 数据域, 3G后付费含推荐人
			else if (mPayType == 5 && !StringUtil.isEmptyOrNull(tjperson)) 
			{
				Log.log("3G后付费含推荐人");
				buffer.add("3867");
				map = 0x6738;
			}
			else
			{
				buffer.add("387F");
				map = 0x7F38;
			}
			
			// 手机号码
			if((map & 0x08) != 0)
			{
				buffer.add(slectenum.getBytes());
			}
			// 空卡信息
			if((map & 0x10) != 0) 
			{
				Log.log("空卡卡号: " + iccid + " 类型：" + openCardType);
				buffer.add((byte)openCardType);		// 卡类型: 0x00-成卡 0x01白卡
				buffer.add((byte)iccid.length());
				buffer.add(iccid.getBytes());
			}
			// 套餐编码
			if((map & 0x20) != 0)
			{
				buffer.add(this.ProductCode);
			}
			// 证件类型
			if((map & 0x100) != 0) 
			{
				if ("".equals(cust_type)) 
				{
					buffer.add((byte)(Identity.length()==15 ? 0x01:0x02));
				}
				else
				{
					buffer.add(cust_type);
				}
			}
			// 证件号码
			if((map & 0x200) != 0) 
			{
				buffer.add((byte) Identity.length());
				buffer.add(this.Identity.getBytes());
			}
			// 证件姓名
			if((map & 0x400) != 0)
			{
				try
				{
					byte[] name = this.username.getBytes("GBK");
					buffer.add((byte)name.length);
					buffer.add(name);
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			// 证件性别 0x01-女 02-男
			if((map & 0x800) != 0) 
			{
				buffer.add(this.sex);
			}
			// 证件地址
			if((map & 0x1000) != 0) 
			{
				try
				{
					byte[] addr = this.address.getBytes("GBK");
					buffer.add((byte)addr.length);
					buffer.add(addr);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 资费编码
			if((map & 0x2000) != 0) 
			{
				Log.log("资费编码: " + zife);
				buffer.add((byte)this.zife);
			}
			// 推荐人信息
			if((map & 0x4000) != 0)
			{
				if (!StringUtil.isEmptyOrNull(tjperson))
				{
					buffer.add((byte) tjperson.length());
					buffer.add(tjperson.getBytes());
				}
			}
			sendPackage.addUnit("04D1", buffer.getBytes());
			
			sendPackage.addUnit("0371", new byte[] { 0x01 });
			sendPackage.addUnit("0374", new byte[] { 0x01 });
			// 添加合约计划标识
			sendPackage.addUnit("0505", (byte)contractflag);
			
			// 数据收发
			int ret = send(sendPackage);
			if (ret != ReturnEnum.SUCCESS)
			{
				return false;
			}
			ret = receiveOnePackage("0C0B");
			if (ret != ReturnEnum.SUCCESS)
			{
				String ss = "获取费用信息失败,即将退出当前流程！";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				return false;
			}
			
			// 获取POS刷卡金额
			if (recivePackage.containsKey("0218"))
			{
				byte[] unit3 = recivePackage.get("0218");
				byte[] pay = new byte[4];
				System.arraycopy(unit3, 0, pay, 0, 4);
				pospay = Commen.bytes2int(pay);
			}
			
			// 套餐详情
			if (!recivePackage.containsKey("0495"))
			{
				String ss = "获取费用信息失败,即将退出当前流程！";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [缺少0495命令单元]";
				}
				setLastknownError(ss);
				return false;
			}
			else
			{
				byte[] unit = recivePackage.get("0495");
				int pos = 2;
				int bitmap = Commen.bytes2int(new byte[] {unit[0], unit[1]});
				if ((bitmap & 0x0001) != 0)
				{
					pos++;
				}
				if ((bitmap & 0x0002) != 0)
				{
					pos += 4;
				}
				if ((bitmap & 0x0004) != 0)
				{
					int len = Commen.bytes2int(new byte[] {unit[pos], unit[pos+1]});
					pos += 2;
					// 套餐详情
					this.res = StringUtil.encodeWithGBK(unit, pos, len);
					pos += len;
					Log.e("", "套餐详情: " + this.res);
				}
			}
			
			// 费用信息文本
			if (!recivePackage.containsKey("0497"))
			{
				String ss = "获取费用信息失败,即将退出当前流程！";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [缺少0497命令单元]";
				}
				setLastknownError(ss);
				return false;
			}
			else
			{
				byte[] unit = recivePackage.get("0497");
				int len = 0;
				int pos = 0;
				map = Commen.bytes2int(new byte[] {unit[0], unit[1]});
				pos += 2;
				if((map & 0x0001) != 0)  
				{
					len = Commen.bytes2int(unit, pos, 1);
					pos++;
					mFeiYongTotal = StringUtil.encodeWithGBK(unit, pos, len);
					pos += len;
					Log.e("", "总费用: " + this.mFeiYongTotal);
				}
				if((map & 0x0002) != 0)
				{
					len = Commen.bytes2int(unit, pos, 2);
					pos += 2;
					this.feiyong = StringUtil.encodeWithGBK(unit, pos, len);
					pos += len;
				}
				Log.e("", "费用详情: " + this.feiyong);
			}
			//money = feiyong;
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			String ss = "获取费用信息失败,即将退出当前流程！";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}
	}
	
	// 开户，获取流水号
	public boolean bll0C0C()
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0C");
		sendPackage.setId("0C");
		sendPackage.addUnit("0202", new byte[] { tranceType }); // 业务
		// sendPackage.addUnit("0215", this.PWDEncryption(this.password,
		// indata));
		// sendPackage.addUnit("0215", this.PWDEncryption(this.password));
		Buffer buffer = new Buffer();
		buffer.clear();
		buffer.add((byte)this.PWDEncryption(this.password).length);
		buffer.add(this.PWDEncryption(this.password));
		//buffer.add((byte) password.length());
		//buffer.add(password.getBytes());
		sendPackage.addUnit("0215", buffer.getBytes());
		System.out.println("交易密码 :" + this.password);
		// sendPackage.addUnit("0217", (byte) this.postype);

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C0C");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取流水号失败!,将退出当前流程.\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		// 获取交易状态
		if (recivePackage.containsKey("0F49"))
		{
			int pos1 = 0;
			int len1 = 0;
			byte[] unit1 = recivePackage.get("0F49");
			byte[] bitmap = new byte[2];
			System.arraycopy(unit1, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);
			pos1 += 2;

			// 银行卡号
			if ((map & 0x0001) != 0)
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			// 查询状态
			if ((map & 0x0002) != 0)
			{
				posOptType = unit1[pos1];
				pos1 += 1;
			}
			// 交易账号
			if ((map & 0x0004) != 0)
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			// 国际行政区域号
			if ((map & 0x0008) != 0)
			{
				len1 = unit1[pos1];
				pos1 += 1;
				interCode = new byte[len1];
				System.arraycopy(unit1, pos1, interCode, 0, len1);
				pos1 += len1;
			}
		}

		if (!recivePackage.containsKey("0202"))
		{
			String ss = "获取流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [缺少0202命令单元]";
			}
			setLastknownError(ss);
			return false;
		}
		byte[] unit = recivePackage.get("0202");

		int len = unit[1];
		bllnum = new byte[len];
		System.arraycopy(unit, 2, bllnum, 0, len);
		try
		{
			sBllnum = new String(bllnum, "ASCII");
			Log.e("业务流水号为:", sBllnum);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			String ss = "获取流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}
		return true;
	}
	
	// 开户
	public boolean bll0C0D()
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0D");
		sendPackage.setId("0C");

		Buffer buffer = new Buffer();
		buffer.add("0200");
		buffer.add(this.IdentityEndtime.getBytes()); 	// 身份证有效期
		buffer.add((byte) this.Identity.length());		// 新增
		buffer.add(this.Identity.getBytes());			// 新增
		sendPackage.addUnit("036D", buffer.getBytes());

		buffer.clear();
		buffer.add("3F7F");					 			// 数据域 待确认 xuhe
		buffer.add("0000"); 							// 金融CRC
		buffer.add(this.tranceType); 					// 业务类型
		buffer.add((byte) this.bllnum.length); 			// 流水号长度
		buffer.add(this.encryptSerialNumber(this.bllnum)); // 加密流水号
		//buffer.add(this.bllnum); 						// 流水号
		try
		{
			System.out.println("流水号..." + new String(bllnum, "ASCII"));
		}
		catch (UnsupportedEncodingException e1)
		{
			e1.printStackTrace();
		}
		buffer.add("00"); 								// 扣款状态 0扣款 1查询

		buffer.add(slectenum.getBytes());	 			// 手机号

		System.out.println("开户 卡类型 ：" + openCardType);
		buffer.add((byte) openCardType);				// uint1:卡类型 0成卡 1白卡
		buffer.add((byte) this.iccid.length());			// 增加：黄杰桢
		buffer.add(this.iccid.getBytes());				// uint1:卡号长度 strN sim卡号内容
		buffer.add(this.ProductCode);		 			// 套餐编码 4位

		if ("".equals(cust_type))
		{
			buffer.add((byte) (Identity.length() == 15 ? 0x01 : 0x02));
		}
		else
		{
			buffer.add(cust_type); 						// 从前面传过来的
		}

		buffer.add((byte) Identity.getBytes().length); 	// 身份证长度
		buffer.add(this.Identity.getBytes()); 			// 证件号码

		try
		{
			int len = 0;
			byte[] name = this.username.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len);
			buffer.add(name);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return false;
		}
		buffer.add(this.sex); // 01 女 02男

		try
		{
			int len = 0;
			byte[] name = this.address.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len); 			// 地址
			buffer.add(name);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		buffer.add((byte) this.zife); 			// 资费编码

		if (!StringUtil.isEmptyOrNull(tjperson))
		{
			int tjlength = tjperson.length();
			buffer.add((byte) tjlength); 		// 推荐人长度
			buffer.add(tjperson.getBytes()); 	// 推荐人名称
		}
		else
		{
			buffer.add("00"); 					// 推荐人编码长度
		}
		// pdu.put("04D1", buffer.getbytes());

		byte[] tt = buffer.getBytes(); // CRC计算
		byte[] bb = new byte[tt.length - 4];
		System.arraycopy(tt, 4, bb, 0, tt.length - 4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("04D1", tt);

		// 这个需要提出crc，然后修改数据库中这个状态
		byte[] err_crc = new byte[3];
		err_crc[0] = crc[0];
		err_crc[1] = crc[1];
		int errint = err_crc.length;
		t_crc = Commen.hax2str(err_crc, 0, errint);

		if (2 == postype)
		{
			sendPackage.addUnit("0F20", pos8583);
			buffer.clear();
			buffer.add((byte) meclen);
			buffer.add(mecbusflowno);
			buffer.add((byte) chanlen);
			buffer.add(chanbusflowno);
			sendPackage.addUnit("0F25", buffer.getBytes());
		}
		sendPackage.addUnit("0371", new byte[]{ 0x01 });
		sendPackage.addUnit("0374", new byte[]{ 0x01 });
		// 预先记录异常
		mStrErrBill 	=  sBllnum; 									// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { tranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		mBusinessNum.insertData(
				App.getUserNo(), 
				slectenum, 
				"优惠购机", 
				mFeiYongTotal, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mFeiYongTotal, 
				tranceType);

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C0D");
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "开户请求办理失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		
		// 重置交易时间
		mTranceTime = "";
		if(recivePackage.containsKey("0204"))
		{
			byte[] unit = recivePackage.get("0204");
			if(unit.length >= 14)
			{
				mTranceTime = StringUtil.encodeWithGBK(unit, 0, 14);
				mTranceTime = mTranceTime.substring(0, 4)+"-"+
							  mTranceTime.substring(4, 6)+"-"+
						      mTranceTime.substring(6,8)+" "+
						      mTranceTime.substring(8, 10)+":"+
						      mTranceTime.substring(10, 12)+":"+
						      mTranceTime.substring(12, 14);
			}
			else
			{
				mTranceTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			}
		}

		// 获取POS刷卡的冲正标示
		if (recivePackage.containsKey("0F21"))
		{
			byte[] unit1 = recivePackage.get("0F21");
			posresult = unit1[0];
		}

		// 获取8583协议包
		if (recivePackage.containsKey("0F20"))
		{
			pos8583 = recivePackage.get("0F20");
		}

		if (!recivePackage.containsKey("0498"))
		{
			String ss = "开户请求办理失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [缺少0498命令单元]";
			}
			setLastknownError(ss);
			return false;
		}
		byte[] unit = recivePackage.get("0498");

		int len = unit[0];
		fuwumima = new byte[len];
		System.arraycopy(unit, 1, fuwumima, 0, len);
		try
		{
			fuwupwd = new String(fuwumima, "ASCII");
			Log.e("服务密码为:", fuwupwd);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			String ss = "开户请求办理失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}

		// 如果号卡成功，则把号源模式设置为首次；这样就可以重新办理号卡业务
		if (ret == ReturnEnum.FAIL)
		{
			this.isHaoyuanTypeFirst = true;
		}

		if (recivePackage.containsKey("04C1"))
		{
			/*********************** 发票打印 ***********************/
			int pos1 = 0;
			int len1 = 0;
			byte[] unit1 = recivePackage.get("04C1");
			byte[] bitmap = new byte[2];
			System.arraycopy(unit1, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);
			pos1 += 2;

			// 发票类型
			if ((map & 0x0001) != 0)
			{
				pos1 += 1;
			}
			// 批次号
			if ((map & 0x0002) != 0)
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			// 发票号
			if ((map & 0x0004) != 0)
			{
				len1 = unit1[pos1];
				pos1 += 1;
				byte[] IMSI1 = new byte[len1];
				System.arraycopy(unit1, pos1, IMSI1, 0, len1);
				pos1 += len1;
				try
				{
					this.taxNo = new String(IMSI1);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 发票内容
			if ((map & 0x0008) != 0)
			{
				// len1 = unit1[pos1];
				// pos1 += 1;
				byte[] invoice = new byte[2];
				System.arraycopy(unit1, pos1, invoice, 0, 2);
				len1 = Commen.bytes2int(invoice);
				pos1 += 2;

				byte[] IMSI2 = new byte[len1];
				System.arraycopy(unit1, pos1, IMSI2, 0, len1);
				pos1 += len1;
				try
				{
					this.taxContent = new String(IMSI2, "GBK");
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	// 获取ISMI
	public boolean bll0C0E()
	{
		int ret = ReturnEnum.FAIL;
		Log.log("开始获取号卡数据");

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0E");
		sendPackage.setId("0C");

		Buffer buffer = new Buffer();
		buffer.add("0F00"); 					// 数据域
		buffer.add((byte) 0x00); 				// 第一次获取IMSI:0x00
		buffer.add((byte) iccid.length()); 		// ICCID号长度
		buffer.add(iccid.getBytes());
		buffer.add((byte) iccid.length()); 		// ICCID号长度
		buffer.add(iccid.getBytes());
		buffer.add((byte) slectenum.length()); 	// 手机号码长度
		buffer.add(slectenum.getBytes());

		sendPackage.addUnit("04B0", buffer.getBytes());
		sendPackage.addUnit("04B2", (byte) 0x01);
		ret = send(sendPackage);

		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}

		ret = receiveOnePackage("0C0E");

		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取IMSI失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		if (recivePackage.containsKey("04B0"))
		{
			/*********************** 短信中心号 ***********************/
			byte[] buf = recivePackage.get("04B0");
			int pos = 0;
			int len = 0;
			int map = Commen.bytes2int(buf, pos, 2);
			pos += 2;

			if ((map & 0x0001) != 0)
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("新ICCID号码为:" + iccid);
			}

			if ((map & 0x0002) != 0)
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("旧ICCID号码为:" + iccid);
			}
			if ((map & 0x0004) != 0)
			{
				len = buf[pos];
				pos += 1;
				String str = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("手机号码为:" + str);
			}
			if ((map & 0x0008) != 0)
			{
				Log.log("文弄说这个数据不会有");
			}

			if ((map & 0x0010) != 0)
			{
				Log.log("文弄说这个数据不会有");
			}
			if ((map & 0x0020) != 0)
			{
				len = buf[pos];
				pos += 1;
				this.smsNum = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("短信中心号码为:" + smsNum);
			}
			if ((map & 0x0040) != 0)
			{
				len = buf[pos];
				pos += 1;
				this.im = StringUtil.encodeWithGBK(buf, pos, len);
				IMSI = new byte[len];
				System.arraycopy(buf, pos, IMSI, 0, len);
				pos += len;
				Log.log("imsi号码为:" + im);
			}
			if ((map & 0x0080) != 0)
			{
				len = buf[pos];
				Log.log("sim卡类型为:" + len + " 其中1为2G	2为3G");
			}
		}
		return true;
	}

	// 上报开卡结果
	// 成功为0x00 失败wie0x01
	public boolean bll0C0F(byte result)
	{
		try
		{
			int ret = ReturnEnum.FAIL;
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("0F");
			sendPackage.setId("0C");

			Buffer buffer = new Buffer();
			buffer.add("3F00"); 				// 数据域
			buffer.add(result); 				// 开卡结果
			buffer.add((byte) 0x00); 			// 业务状态
			buffer.add((byte) iccid.length()); 	// ICCID号长度
			buffer.add(iccid.getBytes());
			buffer.add((byte) iccid.length());	// ICCID号长度
			buffer.add(iccid.getBytes());

			buffer.add((byte) IMSI.length); 	// ISMI号码
			buffer.add(IMSI);

			buffer.add((byte) slectenum.length()); // 手机号码长度
			buffer.add(slectenum.getBytes());
			sendPackage.addUnit("04B1", buffer.getBytes());
			ret = send(sendPackage);
			
			if (ret != ReturnEnum.SUCCESS)
			{
				return false;
			}

			ret = receiveOnePackage("0C0F");
			if (ret != ReturnEnum.SUCCESS)
			{
				/*
				String ss = "上报开卡结果失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				*/
				setLastknownError(socketService.getLastKnowError());
				return false;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			/*
			String ss = "上报开卡结果失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			*/
			setLastknownError(socketService.getLastKnowError());
			return false;
		}
		return true;
	}

	// 开户流程
	public boolean doOpenCard(Handler handler)
	{
		boolean flag;
		DialogUtil.showProgress("正在获取流水号...");
		flag = bll0C0C();// 获取流水号
		if (!flag)
		{
			return false; 
		}
		DialogUtil.showProgress("正在进行开户...");
		flag = bll0C0D();// 号卡请求
		if (!flag)
		{
			return false;
		}
		if (openCardType == 1)
		{
			// 如果是白卡开户则上平台上获取
			flag = doWriteCard(handler);
		}

		if (flag)
		{
			// 发送开户成功确认
			flag = bll0C10();
			// 开户成功后，修改存放身份证信息的文件夹的名称为流水号---暂时屏蔽
			File oldFile = new File(sPicFolder);
			File newFile = new File(oldFile.getParent() + "/" + sBllnum);
			oldFile.renameTo(newFile);
			Log.log("发送开户成功过确认包");
			// this.printScrean();//显示交易结果----暂时屏蔽
		}
		return flag;
	}

	/*
	// 写白卡
	public boolean doWriteCard()
	{
		boolean flag;
		try
		{
			Log.log("开始获取卡数据...");
			DialogUtil.showProgress("正在获取写卡数据...");
			flag = bll0C0E();// 获取卡数据
			if (flag) 
			{
				//DialogUtil.showMessage("正在写卡中，请勿拔卡...");
				// 物理写卡
				Log.log("开始写卡...");
				Log.log("开始写入IMSI...");
				DialogUtil.showProgress("正在写卡，请勿拔卡...");
				if (equipmentService.writeSimCard(IMSI, (byte) 0x02))
				{
					if(!StringUtil.isEmptyOrNull(smsNum))
					{
						Log.log("开始写入短信中心号码..."); 
						if(equipmentService.writesmscentre(smsNum, (byte) 0x02)) 
						{ 
							 Log.e("3G", "写卡成功"); 
							 flag = this.bll0C0F((byte)0x00);
						} 
						else 
						{ 
							 Log.log("3G", "写短信中心号码失败"); 
							 this.bll0C0F((byte) 0x01);
							 String ss = "写卡失败\n失败原因: [写短信中心号码失败]";
							 setLastknownError(ss);
							 bll0C0F((byte) 0x01);
						 	 flag = false; 
						}
					}
					else
					{
						 Log.e("3G", "写卡成功"); 
						 flag = this.bll0C0F((byte)0x00);
					}
				}
				else 
				{ 
					 Log.log("3G", "写IMSI失败"); 
					 this.bll0C0F((byte) 0x01);
					 String ss = "写卡失败\n失败原因: [写IMSI失败]";
					 setLastknownError(ss);
					 bll0C0F((byte) 0x01);
				 	 flag = false; 
				}
			}
			else
			{
				String ss = "获取号卡数据失败！";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				Log.e("3G", "获取号卡数据失败");
				return false;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String ss = "开卡失败\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}
		return flag;
	}
	*/
	
	private boolean mFlag = true;
	// 写白卡
	public boolean doWriteCard(final Handler handler)
	{
		boolean flag = true;
		try
		{
			Log.log("开始获取卡数据...");
			DialogUtil.showProgress("正在获取写卡数据..."); 
			// 获取卡数据
			if(bll0C0E())
			{
				while(mFlag)
				{ 
					if(mIsWritting)
					{
						continue;
					}
					if(!writeToCard())
					{
						if(mReWriteCount > 0)
						{
							DialogUtil.MsgBox("温馨提示", "写卡失败，是否重试？\n剩余重试次数：" + mReWriteCount, 
							"重试", new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mReWriteCount --;
									mIsWritting = false;
									Log.e("重试", "重试：" + mReWriteCount);
								}
							},
							"取消", new OnClickListener()
							{ 
								@Override
								public void onClick(View v)
								{
									mFlag = false;
								}
							}, new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mFlag = false;
								}
							});
						}
						else
						{
							//bll0C0F((byte)0x01);
							//bll0C16();
							mFlag = false;
						}
						flag = false;
					}
					else
					{
						Log.e("物理写卡", "写卡成功！");
						flag = true;
						// 写卡成功,继续往下走
						break;
					}
				}
				DialogUtil.showProgress("正在上报开卡结果...");
				if(flag == false)
				{
					Log.e("写卡", "上报失败开卡结果！");
					bll0C0F((byte)0x01);
					flag = false;
					if(mReWriteCount == 0)
					{
						setLastknownError("系统异常，写卡失败");
					}
					else
					{
						setLastknownError("写卡失败");
					}
				}
				else
				{
					Log.e("写卡", "上报成功开卡结果！");
					bll0C0F((byte)0x00);
					flag = true;
				}
			}
			else
			{
				Log.e("3G", "获取写卡数据失败");
				flag = false;
				String ss = "获取写卡数据失败";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				flag = false;
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
			 String ss = "开户失败";
			 if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			 {
				ss += "\n失败原因: [未知错误]";
			 }
			 setLastknownError(ss);
			 flag = false;
		}
		return flag;
	}

	boolean bIsStart = false;
	private boolean writeToCard()
	{
		mIsWritting = true;
		if(!bIsStart)
		{
			DialogUtil.MsgBox("温馨提示", "即将写卡，请勿拔卡！", "确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					bIsStart = true;
				}
			}, "", null, new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					bIsStart = true;
				}
			});
		}
		while(!bIsStart)
		{
			
		}
		// 物理写卡
		boolean flag = true;
		Log.log("开始写卡...");
		Log.log("开始写入IMSI...");
		if(mReWriteCount == 3)
		{
			DialogUtil.showProgress("正在写卡，请勿拔卡...");
		}
		else
		{
			DialogUtil.showProgress("正在进行第" + (3 - mReWriteCount) + "次写卡尝试，请勿拔卡...");
		}
		if (equipmentService.writeSimCard(IMSI, (byte) 0x02))
		{
			if(StringUtil.isEmptyOrNull(smsNum))
			{
				Log.e("3G", "写卡成功"); 
				flag = true;
			}
			else
			{
				Log.log("开始写入短信中心号码..."); 
				if(equipmentService.writesmscentre(smsNum, (byte) 0x02)) 
				{ 
					 Log.e("3G", "写卡成功"); 
					 flag = true;
				} 
				else 
				{ 
					 Log.log("3G", "写短信中心号码失败"); 
					 String ss = "写卡失败\n失败原因: [写短信中心号码失败]";
					 setLastknownError(ss);
					 flag = false;
				}
			}
		}
		else 
		{ 
			 Log.log("3G", "写IMSI失败"); 
			 String ss = "写卡失败\n失败原因: [写IMSI失败]";
			 setLastknownError(ss);
		 	 flag = false; 
		}
		DialogUtil.closeProgress();
		return flag;
	}

	// 开户成功信息
	public boolean bll0C10()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("10");
		sendPackage.setId("0C");

		Buffer buffer = new Buffer();
		buffer.add(tranceType); 	// 业务类型
		buffer.add("1E");		 	// 流水长度:
		buffer.add(bllnum); 		// 流水号:
		sendPackage.addUnit("0202", buffer.getBytes());
		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x09}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}

		ret = receiveOnePackage("0C10");
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "发送成功确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				pospay+"", 
				mStrErrBill, 
				pospay+"");

		if (!recivePackage.containsKey("0202"))
		{
			String ss = "发送成功确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [数据接收失败，没有0202单元]";
			}
			setLastknownError(ss);
			return false;
		}
		try
		{
			// 改为24小时制
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDate = df.format(new Date());

			int sb_length = sPrintBuffer.length();// 取得字符串的长度
			sPrintBuffer.delete(0, sb_length);
			if (cust_type.equals("07"))
			{
				sPrintBuffer.append("业务类型:3G号卡销售\n")
						.append(this.feiyong)
						// .append("流水号为:" + new String(bllnum,
						// "ASCII")).append("\n")
						.append("客户名称:").append(this.username)
						.append("\n")
						// .append("用户性别:").append("01".equals(this.sex)? "女" :
						// "男").append("\n")
						.append("证件号码:").append(this.Identity).append("\n")
						// .append("身份证有效日期:").append(this.IdentityEndtime).append("\n")
						.append("证件地址:").append(this.address).append("\n").append("初始服务密码为:" + this.fuwupwd)
						.append("\n")
						// .append("活动类型:" + this.actiontype).append("\n")
						.append("交易时间:" + currentDate).append("\n").append("套餐名称:" + this.sp_packagename);
			}
			else
			{
				sPrintBuffer.append("业务类型:3G号卡销售\n")
						.append(this.feiyong)
						// .append("流水号为:" + new String(bllnum,
						// "ASCII")).append("\n")
						.append("用 户 名:").append(this.username)
						.append("\n")
						// .append("用户性别:").append("01".equals(this.sex)? "女" :
						// "男").append("\n")
						.append("身份证号码:").append(this.Identity).append("\n")
						// .append("身份证有效日期:").append(this.IdentityEndtime).append("\n")
						.append("用户地址:").append(this.address).append("\n").append("初始服务密码为:" + this.fuwupwd)
						.append("\n")
						// .append("活动类型:" + this.actiontype).append("\n")
						.append("交易时间:" + currentDate).append("\n").append("套餐名称:" + this.sp_packagename);

			}
			// UIHandle.MsgBox_3("3G号卡销售成功，开户信息如下",msg.toString(),
			// UIHandle.MSG_TICKETPRINT);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String ss = "发送成功确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}

		try
		{
			proPrintBuffer.delete(0, proPrintBuffer.length());
			Production = this.res;
			// userPrintBuffer.delete(0, userPrintBuffer.length());
			// userPrintBuffer.append("用  户  名:").append(this.username).append("\n")
			// .append("用户性别:").append("01".equals(this.sex) ? "女" :
			// "男").append("\n")
			// .append("身份证号码:").append(this.Identity).append("\n")
			// // .append("身份证有效日期:").append(this.IdentityEndtime).append("\n")
			// .append("用户地址:").append(this.address).append("\n")
			// .append("服务密码为:" + new String(fuwumima)).append("\n");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String ss = "发送成功确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [未知错误]";
			}
			setLastknownError(ss);
			return false;
		}
		/*
		// 交易成功更改数据库状态 ErrorInfo.setErrorInfoState(main_activity, t_psam,
		t_type, "0", "0", t_crc, t_no); // 对应的值状态进行改变
		bussinessNum.updateBussinessNum(new
		SimpleDateFormat("yyyy-MM-dd HH:mm:ss"
		).format(Calendar.getInstance().getTime()), "交易成功",
		String.format("%.2f", (float) this.pospay / 100), t_no, pospay);
		*/
		return true;
	}

	// 送开户成功信息成功接收确认包
	public int bll0C11()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("11");
		sendPackage.setId("0C");
		Buffer buffer = new Buffer();
		buffer.add(tranceType); // 业务类型
		buffer.add("1E"); // 流水长度:
		buffer.add(bllnum); // 流水号:
		sendPackage.addUnit("0202", buffer.getBytes());

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}
		ret = receiveOnePackage("0C11");
		if (ret != ReturnEnum.SUCCESS)
		{
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}

		return ret;
	}

	// 获取快捷开户各项信息数据。
	public int bll0C12()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("12");
		sendPackage.setId("0C");

		sendPackage.addUnit("04B4", new byte[]
		{ 0x00, 0x00 });

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}

		ret = receiveOnePackage("0C12");

		if (ret != ReturnEnum.SUCCESS)
		{
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}
		return parseUnit04B4(recivePackage);
	}

	// 解析04B4子命令单元
	private int parseUnit04B4(Package recivePackage)
	{
		int ret = ReturnEnum.FAIL;

		if (recivePackage.containsKey("04B4"))
		{
			Log.log("====================开始解析04B4单元====================");
			int pos = 0;
			byte[] buf = recivePackage.get("04B4");
			int map = Commen.bytes2int(new byte[]
			{ buf[pos], buf[pos + 1] });
			pos += 2;

			zife = -1;
			if ((map & 0x01) != 0)
			{
				zife = buf[pos];// 资费编码
				pos += 1;
				Log.log("资费编码:" + zife);
			}

			sjNumber = null;
			if ((map & 0x02) != 0)
			{
				sjNumber = StringUtil.encodeWithGBK(buf, pos, 11);
				pos += 11;
				Log.log("手机号码:" + sjNumber);
			}

			ProductCode = -1;
			if ((map & 0x04) != 0)
			{
				ProductCode = Commen.bytes2int(buf, pos, 4);
				pos += 4;
				Log.log("套餐编码:" + ProductCode);

			}

			packageName = null;
			if ((map & 0x08) != 0)
			{
				int len = buf[pos];
				pos += 1;
				packageName = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("套餐名称:" + packageName);
			}

			contractContent = null;
			if ((map & 0x10) != 0)
			{
				int len = Commen.bytes2int(buf, pos, 2);
				pos += 2;
				contractContent = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("合约详情:" + contractContent);
			}
			ret = ReturnEnum.SUCCESS;
			Log.log("====================解析04B4单元完成====================");
		}
		else
		{
			Log.log("xxxxxxxxxxxxxxxxxxxx无04B4单元xxxxxxxxxxxxxxxxxxxx");
		}
		return ret;
	}

	// 获取开户默认用户信息
	public int bll0C13()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("13");
		sendPackage.setId("0C");
		sendPackage.addUnit("0499", new byte[]{ 0x00, 0x00 });

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}

		ret = receiveOnePackage("0C13");
		if (ret != ReturnEnum.SUCCESS)
		{
			// setLastKnownError("获取快捷开户各项信息数据信息失败");
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}
		return parseUnit0499(recivePackage);
	}

	// 解析0499子命令单元
	private int parseUnit0499(Package recivePackage)
	{
		int ret = ReturnEnum.SUCCESS;

		if (recivePackage.containsKey("0499"))
		{
			Log.log("====================开始解析0419单元====================");
			int pos = 0;
			byte[] buf = recivePackage.get("0499");
			int map = Commen.bytes2int(buf, pos, 2);
			pos += 2;
			
			// 证件号码
			if ((map & 0x01) != 0)
			{
				int len = buf[pos];
				pos += 1;
				Identity = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("证件号码:" + Identity);
			}

			// 有效日期
			if ((map & 0x02) != 0)
			{
				int len = buf[pos];
				pos += 1;
				IdentityEndtime = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("有效日期:" + IdentityEndtime);
			}

			// 客户姓名
			if ((map & 0x04) != 0)
			{
				int len = buf[pos];
				pos += 1;
				username = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("客户姓名:" + username);
			}

			// 客户性别
			if ((map & 0x08) != 0)
			{
				if (buf[pos] == 0x01)
				{
					sex = "女";
				}
				else if (buf[pos] == 0x02)
				{
					sex = "男";
				}
				pos += 1;
				Log.log("客户性别:" + sex);
			}

			// 客户地址
			if ((map & 0x10) != 0)
			{
				int len = buf[pos];
				pos += 1;
				address = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("客户地址:" + address);
			}
			Log.log("====================解析0419单元完成====================");
		}
		else
		{
			Log.log("xxxxxxxxxxxxxxxxxxxx无0419单元xxxxxxxxxxxxxxxxxxxx");
		}
		return ret;
	}

	// 发送身份证信息给平台
	public int bll0C14(IdentifyMsg identifyMsg)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("14");
		sendPackage.setId("0C");

		Buffer buffer = new Buffer();

		byte[] buf = null;

		// 姓名
		buf = identifyMsg.name.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 性别
		buf = identifyMsg.sex.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 名族编码
		buf = identifyMsg.minzu.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 畜生日期
		buf = identifyMsg.birth_date.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 地址
		buf = identifyMsg.address.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 身份证号码长度
		buf = identifyMsg.identify_no.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 签发机关
		buf = identifyMsg.sign_org.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		// 有效日期
		buf = identifyMsg.end_time.getBytes();
		buffer.add((byte) buf.length);
		buffer.add(buf);

		buf = buffer.getBytes();

		buffer.clear();
		buffer.add((byte) buf.length);// 添加信息内容长度
		buffer.add(buf);

		sendPackage.addUnit("0F19", buffer.getBytes());

		// buffer.clear();
		// buffer.add("头像长度");
		// buffer.add("头像数据");
		// pdu.put("0F18", buffer.getbytes());

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}
		ret = receiveOnePackage("0C14");
		if (ret != ReturnEnum.SUCCESS)
		{
			// setLastKnownError("身份信息验证失败");
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}

		return ret;
	}

	// 发送头像信息给平台
	public int bll0C15(IdentifyMsg identifyMsg)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("15");
		sendPackage.setId("0C");

		// buffer.clear();
		// buffer.add("头像长度");
		// buffer.add("头像数据");
		// pdu.put("0F18", buffer.getbytes());

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}
		ret = receiveOnePackage("0C15");
		if (ret != ReturnEnum.SUCCESS)
		{
			// setLastKnownError("身份证头像信息发送失败");
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}
		return ret;
	}

	// 退出号卡销售流程 撤单标识为1
	public int bll0C16()
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("16");
		sendPackage.setId("0C");

		sendPackage.addUnit("0001", "0201".getBytes());
		sendPackage.addUnit("049F", "01".getBytes());

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return ret;
		}
		/*
		ret = receiveOnePackage("0C16");
		if (ret != ReturnEnum.SUCCESS)
		{
			// setLastKnownError("0B16:数据接收失败，没有收到成功回应信息");
			setLastknownError(socketService.getLastKnowError());
			return ret;
		}
		*/
		return ret;
	}
	
	// 退出号卡销售流程    撤单标识为0
	public boolean bll0C16(byte type)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("16");
		sendPackage.setId("0C");

		sendPackage.addUnit("0001", "0201".getBytes());
		sendPackage.addUnit("049F", type);

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0C16");
		if (ret != ReturnEnum.SUCCESS)
		{
			// setLastKnownError("0B16:数据接收失败，没有收到成功回应信息");
			setLastknownError(socketService.getLastKnowError());
			return false;
		}
		return true;
	}
	
	/*
	public void setPackageDetail(String packageDetail)
	{
		this.packageDetail = packageDetail;
	}
	*/
	
	public String getNetworkProtocol()
	{
		return networkProtocol;
	}
	
	public String getLastknownError()
	{
		if (StringUtil.isEmptyOrNull(lastKnownError))
		{
			lastKnownError = "未知错误";
		}
		return lastKnownError;
	}

	public void setLastknownError(String lastknownError)
	{
		this.lastKnownError = lastknownError;
	}
	
}
