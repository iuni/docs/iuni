package com.sunnada.baseframe.business;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.EcardInquiryData;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

public class BusinessEcardInquiry extends BaseBusiness
{  
	public int				         mInquiryType			    = 0x01;			// 0x01-购买电子卡 0x02-购卡充值
	public int				         mNumberType				= 0x00;			// 0x00-手机 0x03-固话 0x04-宽带ADSL 0x05-宽带LAN 0x06-小灵通
	public String			         mStrTelNum				    = null;			// 交易号码
	public String			         mStrPswd				    = null;			// 业务交易密码或者服务密码
	
	public int				         mInquiryTotal			    = 0x00;
	public int				         mInquiryStartIdx		    = 0x01;
	public int				         mInquiryLineNum			= 0x0A;
	public int                       mReturnLineNum             = 0x00;         // 返回的显示的行数
	
	private String 			         mLastKnownError 			= "Unkown Error";
	
	public List<EcardInquiryData>    mEcardInquryDataList;
	
	
	public BusinessEcardInquiry(Context context,IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		TITLE_STR 	= "电子卡卡密打印提醒";
		TAG 		= "电子卡卡密打印";
		mEcardInquryDataList  = new ArrayList<EcardInquiryData>();
	}
	
	// 终端向平台请求交易流水号
	public boolean bll03A1() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("A1");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add("0100");					// 数据域
		buffer.add((byte)mInquiryType);		// 查询类型
		
		sendPackage.addUnit("0202", buffer.getBytes());
		
		buffer.clear();
		// 加密后的业务密码 的长度
		buffer.add((byte)PWDEncryption(mStrPswd).length);
		// 加密后的业务密码
		buffer.add(PWDEncryption(mStrPswd));
		sendPackage.addUnit("0215", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			/*
			String ss = "获取交易流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因:" + socketService.getLastKnowError();
			}
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			*/
			return false;
		}
		ret = receiveOnePackage("03A1");
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			/*
			String ss = "获取交易流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因:" + socketService.getLastKnowError();
			}
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			*/
			return false;
		}
		
		// 解析数据
		if(!recivePackage.containsKey("0202")) 
		{
			DialogUtil.closeProgress();
			/*
			String ss = "获取交易流水号失败!\n失败原因:服务器响应数据非法, 缺少0202命令单元";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			*/
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0202");
			int offset = 0;
			// 查询类型
			offset++;
			// 流水号长度
			int len = unit[offset++]&0xFF;
			mStrBillNum = StringUtil.encodeWithGBK(unit, offset, len);
			Log.e("", "流水号为: " + mStrBillNum);
			return true;
		}
	}
	
	// 获取未打印卡密
	public int bll03A2() 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("A2");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add("0700");						// 数据域
		buffer.add((short)mInquiryTotal);		// 查询总数
		buffer.add((short)mInquiryStartIdx);	// 查询起始数
		buffer.add((byte)mInquiryLineNum);		// 显示行数
		sendPackage.addUnit("0492", buffer.getBytes());
		
		// 数据收发
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			String ss = "获取卡密列表失败!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因:" + socketService.getLastKnowError();
			}
			// DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			return ret;
		}
		ret = receiveOnePackage("03A2");
		if (ret != ReturnEnum.SUCCESS) 
		{
			DialogUtil.closeProgress();
			String ss = "获取卡密列表失败!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因:" + socketService.getLastKnowError();
			}
			// DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			return ret;
		}
		
		// 解析数据
		if (!recivePackage.containsKey("0492")) 
		{
			DialogUtil.closeProgress();
			String ss = "获取卡密列表失败!\n失败原因:服务器响应数据非法, 缺少0492命令单元";
			// DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			return ret;
		}
		else 
		{
			byte[] unit = recivePackage.get("0492");
			int offset = 0;
			int total = 0;
			// 数据域
			int bitmap = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 查询总数
			if ((bitmap & 0x01) != 0) 
			{
				mInquiryTotal = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				if(mInquiryTotal <= 0)
				{
					mEcardInquryDataList.clear();
				}
				Log.e("", "查询总数: " + mInquiryTotal);
			}
			// 查询起始索引
			if ((bitmap & 0x02) != 0) 
			{
				int nStartIdx = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				Log.e("", "起始索引为: " + nStartIdx);
			}
			// 显示行数
			if ((bitmap & 0x04) != 0)
			{
				total = unit[offset++]&0xFF;
				mReturnLineNum = total;
				Log.e("", "本次下发个数为: " + total);
				if(total >= 0)
				{
					mEcardInquryDataList.clear();
				}
			}
			// 卡密列表
			if ((bitmap & 0x08) != 0) 
			{
				for (int i = 0; i < total; i++) 
				{
					EcardInquiryData ecardData = new EcardInquiryData();
					// 金额
					int money = Commen.bytes2int(unit, offset, 2);
					offset += 2;
					// 时间长度
					int timeLen = unit[offset++]&0xFF;
					// 时间
					String time = StringUtil.encodeWithGBK(unit, offset, timeLen);
					offset += timeLen;
					// 卡密长度
					int cardKeyLen = unit[offset++]&0xFF;
					// 卡密
					String cardKey = StringUtil.encodeWithGBK(unit, offset, cardKeyLen);
					offset += cardKeyLen;
					// 流水号长度
					int flowNoLen = unit[offset++]&0xFF;
					// 流水号
					String flowNo = StringUtil.encodeWithGBK(unit, offset, flowNoLen);
					offset += flowNoLen;
					
					ecardData.setmMoney(money);
					ecardData.setmTime(time);
					ecardData.setmECardKey(cardKey);
					ecardData.setmFlowNo(flowNo);
					mEcardInquryDataList.add(ecardData);
				}
			}
			return ret;
		}
	}
	
	// 终端上报打印成功的卡密列表 
	public int bll03A3(String carCode, String serialNumber)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("A3");
		sendPackage.setId("03");

		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("0300");
		// 查询类型 判断是电子卡还是充值卡密
		byte queryType = (byte) mInquiryType;
		buffer.add(queryType);
		// 卡密长度
		buffer.add((byte) carCode.length());
		// 卡密
		buffer.add(carCode.getBytes());
		// 流水号长度
		buffer.add((byte) serialNumber.length());
		// 流水号
		buffer.add(serialNumber.getBytes());
		sendPackage.addUnit("0323", buffer.getBytes());
		
		// 数据收发
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			return ret;
		}
		ret = receiveOnePackage("03A3");
		if (ret != ReturnEnum.SUCCESS) 
		{
			/*
			String ss = "平台回复成功!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			*/
			// DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		return ret;
	}
	
	// 终端异常上报需要打印的卡密列表 
	public int bll03A4(int queryType, String carCode, String serialNumber)
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("A4");
		sendPackage.setId("03");

		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("0300");
		// 查询类型 判断是电子卡还是充值卡密
		buffer.add((byte)queryType);
		// 卡密长度
		buffer.add((byte) carCode.length());
		// 卡密
		buffer.add(carCode.getBytes());
		// 流水号长度
		buffer.add((byte) serialNumber.length());
		// 流水号
		buffer.add(serialNumber.getBytes());
		sendPackage.addUnit("0324", buffer.getBytes());
		
		// 数据收发
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			return ret;
		}
		ret = receiveOnePackage("03A4");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "平台回复成功!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			// DialogUtil.MsgBox(TITLE_STR, ss);
			setLastKnownError(ss);
			return ret;
		}
		return ret;
	}
	
	public void setLastKnownError(String lastKnowError)
	{
		this.mLastKnownError = lastKnowError;
	}
	
	public String getLastKnownError()
	{
		if (StringUtil.isEmptyOrNull(mLastKnownError))
		{
			mLastKnownError = "未知错误";
		}
		return mLastKnownError;
	}
}
