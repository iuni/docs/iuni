package com.sunnada.baseframe.business;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

import sunnada.jni.Commcenter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

// 充值交费 业务受理类
public class BusinessRecharge extends BusinessCharge
{
	private boolean				mIsDoRecharge 				= false;			// 正在交费流程中
	private Handler				mHandler 					= null;
	
	public BusinessRecharge(Context context, IEquipmentService equipmentService, ISocketService socketService, 
			IDataBaseService dataBaseService, Handler handler) 
	{
		super(context, equipmentService, socketService, dataBaseService, handler);
		TITLE_STR 	= "交费业务办理提醒";
		TAG			= "交费: ";
		mHandler	= handler;
	}
	
	// 充值交易过程
	public void doRecharge(int type) 
	{
		if(mIsDoRecharge == false) 
		{
			mIsDoRecharge = true;
			// 获取流水号
			DialogUtil.showProgress("正在获取交易流水号...");
			if(bll0201_0206(0x01)) 
			{
				// 充值请求
				DialogUtil.showProgress("正在发起交费请求...");
				if(bll0202()) 
				{
					// 测试
					//checkErrorDeal();
					// 一次确认
					DialogUtil.showProgress("正在发起一次交易确认...");
					for(int i=0; i<3; i++) 
					{
						if(bll0203(i)) 
						{
							// 充值缴费流程需要二次确认
							// 号卡快捷支付流程不需要二次确认
							if(type != 0x03) 
							{
								// 二次确认
								DialogUtil.showProgress("正在发起二次交易确认...");
								bll0204();
							}
							mHandler.sendEmptyMessage(Statics.MSG_RECHARGE_OK);
							break;
						}
					}
				}
			}
			mIsDoRecharge = false;
			DialogUtil.closeProgress();
		}
	}
	
	// 进行充值交费
	private boolean bll0202() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("02");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add("3F00"); 						// 数据域
		buffer.add("0000"); 						// CRC先设为0
		buffer.add((byte)mTranceType); 				// 业务类型
		buffer.add("1E"); 							// 流水长度
	    //buffer.add(mStrBillNum.getBytes()); 		// 流水号
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes())); // 加密流水号
		buffer.add("00"); 							// 0: 扣款状态 1: 查询状态即扣款无响应时的查询
		buffer.add(mChargeMoney); 					// 充值金额
		buffer.add((byte)mStrTelNum.length()); 		// 帐号长度
		buffer.add(mStrTelNum.getBytes()); 			// 帐号
		
		byte[] tt = buffer.getBytes();
		byte[] bb = new byte[tt.length-4];
		System.arraycopy(tt, 4, bb, 0, tt.length-4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("0301", tt);
		
		// 预先记录异常
		mStrErrBill 	=  mStrBillNum; 								// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { mTranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		// 同时插入到数据库中
		mBusinessNum.insertData(
				App.getUserNo(), 
				mStrTelNum, 
				backtype(), 
				mStrChargeMoney, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mStrChargeMoney, 
				mTranceType);
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0202");
		}
		/*
		// debug
		if(ret == ReturnEnum.SUCCESS) 
		{
			return false;
		}
		// debug end
		*/
		// 更新异常交易状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "受理交费请求失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_RECHARGE_FAIL, mHandler);
			setLastknownError(ss);
			return false;
		}
		
		// 处理数据
		// 交易时间
		if(recivePackage.containsKey("0204")) 
		{
			mBillTime = recivePackage.get("0204");
		}
		// 具体交易信息
		if (!recivePackage.containsKey("0301")) 
		{
			String ss = "受理交费请求失败!\n失败原因: [服务器响应数据非法, 缺少0301命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_RECHARGE_FAIL, mHandler);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0301");
			int offset = 0;
			int bitmap = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 请求信息CRC域
			if ((bitmap & 0x01) != 0) 
			{
				offset += 2;
			}
			// 交易流水号
			if ((bitmap & 0x02) != 0)
			{
				offset += 1;
				offset += (unit[offset]&0xFF) + 1;
			}
			// 交易状态
			if ((bitmap & 0x04) != 0) 
			{
				offset++;
			}
			// 金额
			if ((bitmap & 0x08) != 0) 
			{
				offset += 4;
			}
			// 账号字节长度
			if ((bitmap & 0x10) != 0) 
			{
				int len = unit[offset++]&0xFF;
				// 账号
				if ((bitmap & 0x20) != 0) 
				{
					offset += len;
				}
			}
			// 交费前余额
			if ((bitmap & 0x40) != 0) 
			{
				try
				{
					byte[] money = new byte[4];
					System.arraycopy(unit, offset, money, 0, 4);
					offset += 4;
					
					mStrPreMoney = String.format("%.2f", (float)Commen.bytes2int(money)/100);
					Log.i(TAG, "交费前金额为" + mStrPreMoney);
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
			
			mBusinessNum.updateBussinessNum(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
					"交易失败", mStrChargeMoney, mStrErrBill, mStrChargeMoney);
			return true;
		}
	}
	
	// 确认充值成功
	private boolean bll0203(int nTimes) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("03");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());
		
		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x0D}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0203");
		}
		// 更新异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发起一次交易确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			if(nTimes == 2)
			{
				DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_RECHARGE_FAIL, mHandler);
			}
			return false;
		}
		
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				mStrChargeMoney, 
				mStrErrBill, 
				mStrChargeMoney);
		return true;
	}
	
	// 二次交易确认
	private boolean bll0204() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("04");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS) 
		{
			return false;
		}
		/*
		ret = receiveOnePackage("0204", 3*1000);
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发起二次交易确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		*/
		Log.i(TAG, "发起二次交易确认成功!");
		return true;
	}
	
	// 打印信息
	private StringBuffer printMsg() 
	{
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("业务类型: ");
		switch (mTranceType) 
		{
			case 0x04:
				strbuf.append("手机交费");
				break;
			case 0x05:
				strbuf.append("固话交费");
				break;
			case 0x0E:
				strbuf.append("宽带ADSL交费");
				break;
			case 0x0F:
				strbuf.append("宽带LAN交费");
				break;
			case 0x0D:
				strbuf.append("小灵通交费");
				break;
			case 0x17:
				strbuf.append("号卡快捷支付");
				break;
			default:
				strbuf.append("无效类型");
				break;
		}

		if(mTranceType != 0x17) 
		{
			strbuf.append("\n交费号码: ").append(mStrTelNum)
				.append("\n交费金额: " + mStrChargeMoney + "元")
				.append("\n交易前余额: " + mStrPreMoney + "元")
				.append("\n充值时间: ").append(StringUtil.converDateString(StringUtil.encodeWithGBK(mBillTime, 0, mBillTime.length)));
		}
		else 
		{
			strbuf.append("\n扣款号码:").append(mStrTelNum)
				.append("\n扣款金额:" + mStrChargeMoney + "元")
				.append("\n扣款时间:").append(StringUtil.converDateString(StringUtil.encodeWithGBK(mBillTime, 0, mBillTime.length)));
		}
		return strbuf;
	}
	
	// 接受交易类型存储，需求需要
	public String backtype()
	{
		String typename = "";
		switch(mTranceType)
		{
			case 0x04:
				typename = "手机交费  "; 
				break;
			case 0x05:
				typename = "固话交费  "; 
				break;
			case 0x0E:
				typename = "宽带ADSL交费  "; 
				break;
			case 0x0F:
			 	typename = "宽带LAN交费  "; 
			 	break;
			case 0x0D:
				typename = "小灵通交费  ";
				break;				
			case 0x017:
				typename = "号卡快捷支付";
				break;				
			default:
				break;
		}
		return typename;
	}

	/*
	// 打印至屏幕
	private void printScreen(int type) 
	{
		if(type == 1) 
		{
		    if (2 == CommonValue.sPrintType) 
		    {
			    UIHandle.MsgBox(mTitleStr, printMsg().toString(), UIHandle.MSG_TICKETPRINT);
		    } 
		    else 
		    {
			    UIHandle.MsgBox(mTitleStr, printMsg().toString(), UIHandle.MSG_PRINT);
		    }
		}
		else 
		{
			if (2 == CommonValue.sPrintType) 
			{
				UIHandle.MsgBoxOne(mTitleStr, printMsg().toString(), 2);
			} 
			else 
			{
				UIHandle.MsgBox(mTitleStr, printMsg().toString(), UIHandle.MSG_PRINT);
			}
		}
	}
	*/

	// 打印业务凭条
	public void print() 
	{
		print(printMsg(), null, false);
		
		/*
		// 打印银联交易单
		if(2 == postype) 
		{
			this.printPosDeal(null, null);
		}
		*/
	}
}
