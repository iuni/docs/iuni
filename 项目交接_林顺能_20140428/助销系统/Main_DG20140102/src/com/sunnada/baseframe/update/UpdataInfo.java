package com.sunnada.baseframe.update;

public class UpdataInfo 
{
	private String version; 			  		// 版本号
	private String url; 				  		// 升级地址
	private String description; 		  		// 版本描述
	private String update_time;           		// 强制升级时间
	private String min_version = "0.0.0"; 		// 强制升级时间
	private String psams;				  		// 更新参数, 如果为*则要升级; 如果只是部分升级，则指定psam卡
	private String area_code;

	public String getPsams() 
	{
		return psams;
	}

	public void setPsams(String psams) 
	{
		this.psams = psams;
	}

	public String getVersion() 
	{
		return version;
	}

	public void setVersion(String version) 
	{
		this.version = version;
	}

	public String getUrl() 
	{
		return url;
	}

	public String getUpdate_time() 
	{
		return update_time;
	}

	public void setUpdate_time(String update_time) 
	{
		this.update_time = update_time;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getMin_version()
	{
		return min_version;
	}

	public void setMin_version(String min_version)
	{
		this.min_version = min_version;
	}

	public String getArea_code()
	{
		return area_code;
	}

	public void setArea_code(String area_code)
	{
		this.area_code = area_code;
	}
}
