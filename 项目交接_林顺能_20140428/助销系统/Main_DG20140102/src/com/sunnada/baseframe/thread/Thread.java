package com.sunnada.baseframe.thread;

import com.sunnada.baseframe.listener.OnExceptionListener;
import com.sunnada.baseframe.util.Log;

/**安全线程抽象类
 * File: Thread.java
 * Description:安全线程抽象类，此线程类已经封闭run函数，添加抽象函数runs，后者只需将业务代码放入runs中即可，可有效防止在线程中出现未捕获的异常而导致程序崩溃
 * @author 丘富铨
 * @date 2013-1-18
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public abstract class Thread extends java.lang.Thread
{
	/**
	 * 默认异常回调接口，全局唯一
	 */
	private static OnExceptionListener defaultOnExceptionListener = null;
	
	
	/**
	 * 异常回调接口
	 */
	private OnExceptionListener onExceptionListener = null;
	
	/**
	 * 线程 实际执行函数，已被封闭，请实现runs方法将业务处理相关动作放在里面
	 */
	@Override
	public final void run()
	{
		try
		{
			runs();
		} 
		catch (Exception e)
		{
			Log.log("xxxxxxxxxxxxxxxxxxxx线程执行出现未捕获异常，请重视xxxxxxxxxxxxxxxxxxxx");
			Log.log(e);
			e.printStackTrace();
			
			if(onExceptionListener != null)
			{
				onExceptionListener.onException(e);
			}
			else if(defaultOnExceptionListener != null)
			{
				defaultOnExceptionListener.onException(e);
			}
		}
	}
	
	/**
	 * 线程执行函数，里面执行业务代码
	 */
	public abstract void runs();
	
	public OnExceptionListener getOnExceptionListener()
	{
		return onExceptionListener;
	}

	/**
	 * 当前线程的异常回调接口，若不指定或者为null则使用全局默认异常回调接口
	 * @param defaultOnExceptionListener
	 */
	public void setOnExceptionListener(OnExceptionListener onExceptionListener)
	{
		this.onExceptionListener = onExceptionListener;
	}

	/**
	 * 全局默认异常接口，若未指定当前线程的异常回调接口，则使用全局默认异常回调接口
	 * @param defaultOnExceptionListener
	 */
	public static void setDefaultOnExceptionListener(OnExceptionListener defaultOnExceptionListener)
	{
		Thread.defaultOnExceptionListener = defaultOnExceptionListener;
	}
}
