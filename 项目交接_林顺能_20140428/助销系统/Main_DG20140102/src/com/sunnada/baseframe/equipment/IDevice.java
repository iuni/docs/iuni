package com.sunnada.baseframe.equipment;

import slam.ajni.IDCardInfo;
import android.content.Context;

import com.sunnada.baseframe.bean.PsamInfo;
import com.sunnada.baseframe.bean.ReturnInfo;
import com.sunnada.bluetooth.BluetoothClient;

// 对设备操作的接口
public interface IDevice 
{
	// 初始化设备
	public boolean initEquipment();
	// 资源卸载
	public boolean releaseEquipment();
	// 握手
	public boolean handshake();
	// 获取失败原因
	public String getStrResult();
	
	// 设置蓝牙设备接口
	public boolean setBluetoothClient(BluetoothClient bluetoothClient);
	
	// 获取厂家编码
	// 0x00	无效，禁止使用
	// 0x01	福建三元达通讯股份有限公司
	// 0x02	福建三元达软件有限公司
	// 0x03	江苏怡丰通信设备有限公司
	// 0x04	广东天波信息技术有限公司
	// 0x05	杭州益唐科技有限公司
	// 0x06~0xFE 预留使用
	// 0xFF	未知厂家
    public boolean getFactoryCode(byte[] szMcode);

    // 读取系统各种版本
    // 0x01: BOOTLOADER版本 
	// 0x02: 驱动版本 
	// 0x03: 应用版本 
	// 0x04: 硬件版本 
	// 0x05: 协议版本
    // 0x06: JAR版本
	public String readVersion(byte szVerType);

	// M3文件升级
	// @param path_name 	文件路径 如"/mnt/sdcard/"
	// @param file_name 	文件名 如"CPU-M66-DRV-STM32CV1.0-20130106_1130.bin"
	// @param file_type		0x11：升级驱动，0x81：应用升级
	// @param updata_sum	文件校验和4个字节(低位在前, 高位在后, 其中实际只使用低位两个字节) 根据终端部说crc可以乱传, 我擦, 那我就传一个0吧
	// @return 				成功为true 失败为false
	public boolean m3Update(String path_name, String file_name, byte file_type, int updata_sum);
	
	// 探测PSAM卡
	public boolean detectPsam();
	// 读PSAM ID
	public boolean readPsamId(PsamInfo info);
	// 读PSAM信息
	public boolean readPsamInfo(PsamInfo info);
	// PSAM加密
	public boolean psamEncrypt(byte[] InData,byte InDataLen,byte DesType);
	// 验证开机密码
	public ReturnInfo PSAM_CheckPasswd(String pin);
	// 修改开机密码
	public ReturnInfo PSAM_ModifyPasswd(String oldpin, String newpin);
	// 读取PSAM卡信息(不包含代理商信息)
	public boolean readOpenPasswdInfo(PsamInfo info);
	// 读取PSAM卡信息(包含代理商信息)
	public boolean readNewOpenPasswdInfo(PsamInfo info);
	
	// 热敏打印
	// 打印机状态检测
	public int checkPrinterStatus();
	// 判断打印机是否有纸
	// true有纸  false无纸
	public boolean isPrinterHasPaper();
	public void setPrintFont(int iFont);
	public void printMsg(String sMsg);	
	public void startPrint();
	// 检测黑标是否存在 
	public boolean isBlackLabelExist();
	// 检测打印过程中是否有错误发生
	public boolean isPrintError();
	// 获取打印错误描述
	public String getPrintErrorMsg();
	// 获取打印错误描述
	public String getPrintErrorMessage();
	// 打印LOGO
	public void printLogo(String file_name);
	public boolean printLogo(int[] logoData);
	public int goBlack();
	
	// SIM卡操作
	// 读ICCID
	public String readIccid();
	// 0错误	 1白卡 	2空卡
	public int isWhiteCard(byte szSimType);
	// 白卡写IMSI号
	public boolean writeSimCard(byte[] szImsi, byte szSimType);
	// 白卡写短消息中心号
	public boolean writeSmsCentre(String strSmsc, byte szSimType);
	
	// 重置3G网络，针对MINI
	public boolean resetModem();
	// 二代证阅读器识别
	public IDCardInfo getIDCardInfo();
	
	public void Mini_audio_init();
	public void Mini_audio_deinit();
	public void Mini_audio_switch(byte on1_off0);
	public int Mini_lowpower_init(Context context);
	public int Mini_lowpower_deinit();
	
    // 读取SIM卡序列号，失败返回null
	public String readMobilSimSerialNumber();
    // 读取SIM卡ICCID号，失败返回null
	public String readMobilSimIccidNumber();
    // 写入SIM卡ICCID号
	public boolean writeMobilSimIccidNumber(String iccid);
    // 读取SIM卡IMSI号，失败返回null
	public String readMobilSimImsiNumber();
    // 写入SIM卡IMSI号
	public boolean writeMobilSimImsiNumber(String imsi);
    // 读短白卡信中心，失败返回null
	public String readMobilSimSmscNumber();
    // 写入白卡短信中心
	public boolean writeMobileSimSmscNumber(String smsc);
	// 修改指定索引的PIN和PUK
	// index 索引   1为修改pin1和puk1  2为修改pin2和puk2
	public boolean modifyMobilPinAndPuk(String pin, String puk, int index);
	
	// 福州移动白卡
	// 写福州移动白卡
	public boolean writeFZMobileWhiteCard(String iccid, String imsi, String smsCenter, String oldPin1, String newPin1, 
			String oldPin2, String newPin2, String oldPuk1, String newPuk1, String oldPuk2, String newPuk2, String ki);
	// 读取福州移动白卡ICCID
	public String readFZMobileIccid();
	// 写福州移动白卡ICCID
	public boolean writeFZMobileIccid(String iccid);
	// 读取福州移动IMSI
	public String readFZMobileImsi();
	// 写福州移动IMSI
	public boolean writeFZMobileImsi(String imsi);
	// 读取福州移动短信中心号
	public String readFZMobileSmsCenter();
	// 写入福州移动短信中心号
	public boolean writeFZMobileSmsCenter(String smsCenter);
	// 更新福州移动PUK和PIN
	public boolean updateFZMobilePinAndPuk(String oldPin1,String newPin1,String oldPin2, String newPin2, String oldPuk1, String newPuk1, String oldPuk2, String newPuk2);
	// 读取福州移动卡序列号
	public String readFZMobileSearialNumber();
}
