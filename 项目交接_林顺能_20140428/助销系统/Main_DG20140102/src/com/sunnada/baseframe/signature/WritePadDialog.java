package com.sunnada.baseframe.signature;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;

import com.sunnada.baseframe.activity.R;

public class WritePadDialog extends Dialog
{

	Activity					main_activity;
	LayoutParams				p;
	private OnBitmapGetListener	onBitmapGetListener;
	int							screenWidth;
	int							screenHeight;
	int							bitmapHeight;
	int							bitmapWidth;

	private static final float	TOUCH_TOLERANCE	= 4;

	public WritePadDialog(Activity context)
	{
		super(context);
		this.main_activity = context;
	}

	static final int	BACKGROUND_COLOR	= Color.WHITE;
	static final int	BRUSH_COLOR			= Color.BLACK;

	PaintView			mView;

	/** The index of the current color to use. */
	int					mColorIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.signature_dialog);

		DisplayMetrics dm = new DisplayMetrics();
		main_activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = dm.heightPixels;
		int width = dm.widthPixels;
		
		if((height == 720)&&(width == 1024))
		{
			p = getWindow().getAttributes(); // 获取对话框当前的参数值
			p.width = 960;
			p.height = 690;
			
			getWindow().setAttributes(p); // 设置生效

			bitmapHeight = 600;
			bitmapWidth = 550;
		}
		else
		{
			p = getWindow().getAttributes(); // 获取对话框当前的参数值
			p.width = (int) main_activity.getResources().getDimension(R.dimen.SIGNATURE_X);
			p.height = (int) main_activity.getResources().getDimension(R.dimen.SIGNATURE_Y);
			getWindow().setAttributes(p); // 设置生效

			bitmapHeight = (int) main_activity.getResources().getDimension(R.dimen.SIGNATURE_BITMAP_X);
			bitmapWidth = (int) main_activity.getResources().getDimension(R.dimen.SIGNATURE_BITMAP_Y);
		}
		
		mView = new PaintView(main_activity);
		FrameLayout frameLayout = (FrameLayout) findViewById(R.id.tablet_view);
		frameLayout.addView(mView);
		mView.requestFocus();
		Button btnClear = (Button) findViewById(R.id.tablet_clear);
		btnClear.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				mView.clear();
			}
		});

		Button btnOk = (Button) findViewById(R.id.tablet_ok);
		btnOk.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				try
				{
					WritePadDialog.this.dismiss();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				if (onBitmapGetListener != null)
				{
					onBitmapGetListener.onBitmapGet(mView.getCachebBitmap());
				}
			}
		});

		Button btnCancel = (Button) findViewById(R.id.tablet_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				cancel();
				// 点击X的时候，清掉字迹 20131210
				mView.clear();
			}
		});
	}

	public interface OnBitmapGetListener
	{
		public void onBitmapGet(Bitmap bitmap);
	};

	public void setOnBitmapGetListener(OnBitmapGetListener onBitmapGetListener)
	{
		this.onBitmapGetListener = onBitmapGetListener;
	}

	/**
	 * This view implements the drawing canvas.
	 * 
	 * It handles all of the input events and drawing functions.
	 */
	class PaintView extends View
	{
		private Paint	paint;
		private Canvas	cacheCanvas;
		private Bitmap	cachebBitmap;
		private Path	path;

		public Bitmap getCachebBitmap()
		{
			return cachebBitmap;
		}

		public PaintView(Context context)
		{
			super(context);
			init();
		}

		private void init()
		{
			paint = new Paint();
			paint.setAntiAlias(true);
			paint.setStrokeWidth(5);
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(Color.BLACK);
			path = new Path();
			cachebBitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Config.ARGB_8888);
			cacheCanvas = new Canvas(cachebBitmap);
			// 设置签名的背景颜色
			// cacheCanvas.drawColor(Color.parseColor("#F3F3F3"));
		}

		public void clear()
		{
			if (cacheCanvas != null)
			{

				// paint.setColor(BACKGROUND_COLOR);
				// cacheCanvas.drawPaint(paint);
				// paint.setColor(Color.BLACK);
				// cacheCanvas.drawColor(Color.parseColor("#F3F3F3"));
				Paint paint1 = new Paint();
				paint1.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
				cacheCanvas.drawPaint(paint1);
				paint1.setXfermode(new PorterDuffXfermode(Mode.SRC));

				invalidate();
			}
		}

		@Override
		protected void onDraw(Canvas canvas)
		{
			canvas.drawBitmap(cachebBitmap, 0, 0, null);
			canvas.drawPath(path, paint);
		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh)
		{

			int curW = cachebBitmap != null ? cachebBitmap.getWidth() : 0;
			int curH = cachebBitmap != null ? cachebBitmap.getHeight() : 0;
			if (curW >= w && curH >= h)
			{
				return;
			}

			if (curW < w)
				curW = w;
			if (curH < h)
				curH = h;

			Bitmap newBitmap = Bitmap.createBitmap(curW, curH, Bitmap.Config.ARGB_8888);
			Canvas newCanvas = new Canvas();
			newCanvas.setBitmap(newBitmap);
			if (cachebBitmap != null)
			{
				newCanvas.drawBitmap(cachebBitmap, 0, 0, null);
			}
			cachebBitmap = newBitmap;
			cacheCanvas = newCanvas;
		}

		private float	cur_x, cur_y;

		@Override
		public boolean onTouchEvent(MotionEvent event)
		{

			float x = event.getX();
			float y = event.getY();

			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					cur_x = x;
					cur_y = y;
					path.moveTo(cur_x, cur_y);
					break;
				}

				case MotionEvent.ACTION_MOVE:
				{
					touch_move(x, y);
					invalidate();
					break;
				}

				case MotionEvent.ACTION_UP:
				{
					cacheCanvas.drawPath(path, paint);
					path.reset();
					break;
				}
			}

			invalidate();

			return true;
		}

		private void touch_move(float x, float y)
		{
			float dx = Math.abs(x - cur_x);
			float dy = Math.abs(y - cur_y);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE)
			{
				path.quadTo(cur_x, cur_y, (x + cur_x) / 2, (y + cur_y) / 2);
				cur_x = x;
				cur_y = y;
			}
		}
	}

	public void clear()
	{
		mView.clear();
	}

}
