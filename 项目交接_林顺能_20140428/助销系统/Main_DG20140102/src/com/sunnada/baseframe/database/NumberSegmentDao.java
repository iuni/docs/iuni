/**
 * $RCSfile: NumberSegmentDao.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.service.IDataBaseService;

/**
 * <p>Title: NumberSegmentDao</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author ����
 * @date 2013-4-30
 * @version 1.0
 */

public class NumberSegmentDao extends BaseDao
{
	public static final String TABLE_NAME = "numberSegment";
	public static final String COLUMN_ID = "id";//����
	public static final String COLUMN_CODE = "t_code";//����
	public static final String COLUMN_VALUE = "t_value";//ֵ

	
	public NumberSegmentDao(IDataBaseService dataBaseService)
	{
		super(dataBaseService);
		String sql = "create table if not exists "+TABLE_NAME+" ("+
																	COLUMN_ID+" integer primary key autoincrement,"+
																	COLUMN_CODE+" text unique,"+
																	COLUMN_VALUE+" text"+
																")";
		
		executeUpdateSQL(sql);
	}


	
	public void insertOrUpdate(String code,String value)
	{
		String deleteSQL = "delete from "+TABLE_NAME+" where "+COLUMN_CODE+" = '"+code+"'";
		String insertSQL = "insert into "+TABLE_NAME+"("+COLUMN_CODE+","+COLUMN_VALUE+") values('"+code+"','"+value+"')";
		
		executeUpdateSQL(deleteSQL);
		executeUpdateSQL(insertSQL);
	}
	
	public List<Map<String,String>> getAll()
	{
		String sql = "select * from "+TABLE_NAME +" order by "+COLUMN_VALUE +" desc";
		return executeQuerySQL(sql);
	}
}


