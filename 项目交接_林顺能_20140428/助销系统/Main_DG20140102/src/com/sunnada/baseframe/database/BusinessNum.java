package com.sunnada.baseframe.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class BusinessNum extends DBadapter 
{
	public static final String DB_TABLE 			= "BUSSINESS";					// 数据库表名
	public static final String BUSI_ID 				= "busi_id";					// 数据ID, 主键
	public static final String BUSI_USERNO			= "busi_user_no";				// 当前营业员工号
	public static final String BUSI_TEL				= "busi_tel";					// 手机号码
	public static final String BUSI_NAME 			= "busi_name";					// 交易类型名称
	public static final String BUSI_MONEY 			= "busi_money";					// 表中的交易金额
	public static final String BUSI_STATE 			= "busi_state";					// 交易状态(成功、失败)
	public static final String BUSI_BILL 			= "busi_bill";					// 流水号
	public static final String BUSI_TIME 			= "busi_time";					// 交易时间
	public static final String BUSI_POS_MONEY		= "busi_pos_money";				// 刷卡金额
	public static final String BUSI_TYPE			= "busi_type";					// 交易类型
    
    // 设置当天的时间
	private SimpleDateFormat 	mSimpleDateFormat 	= new SimpleDateFormat("yyyy-MM-dd");
	private List<Map<String, Object>> mBusiList 	= new ArrayList<Map<String, Object>>();
	
	public BusinessNum(Context context) 
	{
		super(context);
	}
	
	// 插入记录
	public long insertData(String strUserNo, String strBusiTel, String strBusiName, String strBusiMoney, String strBusiState,
			String strBusiBill, String strBusiTime, String strPosMoney, int nBusiType) 
	{
		long iTemp = 0;
		
		try {
			// 打开
			open();
			
			ContentValues initialValues = new ContentValues();
			initialValues.put(BUSI_USERNO,	strUserNo);			// 营业员工号
			initialValues.put(BUSI_TEL,		strBusiTel);
			initialValues.put(BUSI_NAME, 	strBusiName);
			initialValues.put(BUSI_MONEY, 	strBusiMoney);
			initialValues.put(BUSI_STATE, 	strBusiState);
			initialValues.put(BUSI_BILL, 	strBusiBill);
			initialValues.put(BUSI_TIME, 	strBusiTime); 
			initialValues.put(BUSI_POS_MONEY, strPosMoney);
			initialValues.put(BUSI_TYPE, 	nBusiType);
			
			iTemp = mDB.insert(DB_TABLE, BUSI_ID, initialValues);
			String ss = "交易记录, " 
					+ ", 工号: " 	 	+ strUserNo 
					+ ", 交易号码: " 	+ strBusiTel 
					+ ", 交易名称: " 	+ strBusiName
					+ ", 交易金额: " 	+ strBusiMoney
					+ ", 交易状态: " 	+ strBusiState
					+ ", 交易流水: " 	+ strBusiBill
					+ ", 交易时间: " 	+ strBusiTime
					+ ", POS刷卡金额: " 	+ strPosMoney
					+ ", 交易类型: " 	+ nBusiType;
			if(iTemp < 0) 
			{
				ss += "插入失败!";
			}
			else
			{
				ss += "插入成功!";
			}
			Log.e("", ss);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			close();
		}
		return iTemp;
	}
	
	// 更新记录操作
	public boolean updateBussinessNum(String strBusiTime, String strBusiState, String strBusiMoney, String strBusiBill, String strPosMoney) 
	{
		ContentValues args = new ContentValues();
		boolean result = false;
		
        try
        {
        	// 打开
        	open();
        	
        	args.put(BUSI_TIME, 		strBusiTime);
        	args.put(BUSI_STATE, 		strBusiState);
        	args.put(BUSI_MONEY, 		strBusiMoney);
        	args.put(BUSI_POS_MONEY, 	strPosMoney);
        	
        	result = mDB.update(DB_TABLE, args, BUSI_BILL+"=?", new String[]{strBusiBill}) > 0;
        	//
        	String ss = "交易记录, 流水: " + strBusiBill + ", 交易状态: " + strBusiState;
        	if(result == true) 
        	{
        		ss += "更新成功!";
        	}
        	else
        	{
        		ss += "更新失败!";
        	}
        	Log.e("", ss);
        }
        catch(Exception e) 
        {
        	e.printStackTrace();
        }
        finally 
        {
        	close();
        }
        return result;
	}
	
	// 根据交易情况来查询符合条件的所有交易记录
	public List<Map<String, Object>> checkshow(String strBusiState) 
	{
		try {
			// 清空列表
			mBusiList.clear();
			// 打开数据库
			open();
			// 查询当天的交易记录
			Cursor cursor = mDB.rawQuery("select * from BUSSINESS where busi_state='" + strBusiState
					+ "' and busi_time like '" + mSimpleDateFormat.format(new Date()) + "%' order by busi_time desc", null);
			if(cursor != null) 
			{
				cursor.moveToFirst();
				int nTotal = cursor.getCount();
				if(nTotal != 0) 
				{
					while (!cursor.isAfterLast()) 
					{
						Map<String, Object> map = new HashMap<String, Object>();
						map.put(BUSI_TEL, 	cursor.getString(cursor.getColumnIndex(BUSI_TEL)));
						map.put(BUSI_NAME, 	cursor.getString(cursor.getColumnIndex(BUSI_NAME)));
						map.put(BUSI_MONEY, cursor.getFloat(cursor.getColumnIndex(BUSI_MONEY)));
						map.put(BUSI_STATE, cursor.getString(cursor.getColumnIndex(BUSI_STATE)));
						map.put(BUSI_BILL, 	cursor.getString(cursor.getColumnIndex(BUSI_BILL)));
						map.put(BUSI_TIME, 	cursor.getString(cursor.getColumnIndex(BUSI_TIME)));
						map.put(BUSI_POS_MONEY, cursor.getString(cursor.getColumnIndex(BUSI_POS_MONEY)));
						map.put(BUSI_TYPE, 	cursor.getString(cursor.getColumnIndex(BUSI_TYPE)));
						
						mBusiList.add(map);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			// 关闭数据库
			close();
		}
		return mBusiList;
	}
	
	// 查询当天全部交易记录
	public List<Map<String, Object>> checkall() 
	{
		try {
			// 清空列表
			mBusiList.clear();
			// 打开数据库
			open();
			
			String sql="select * from BUSSINESS where busi_time like '" + mSimpleDateFormat.format(new Date()) + 
					"%' order by busi_time desc";
			Log.v("SQL", sql);
			Cursor cursor = mDB.rawQuery(sql, null);
			if(cursor != null) 
			{
				cursor.moveToFirst();
				int nTotal = cursor.getCount();
				Log.e("", "共有" + nTotal + "条记录");
				if(nTotal != 0) 
				{
					while (!cursor.isAfterLast()) 
					{
						Map<String, Object> mapall = new HashMap<String, Object>();
						mapall.put(BUSI_TEL, 		cursor.getString(cursor.getColumnIndex(BUSI_TEL)));
						mapall.put(BUSI_NAME, 		cursor.getString(cursor.getColumnIndex(BUSI_NAME)));
						mapall.put(BUSI_MONEY, 		cursor.getFloat(cursor.getColumnIndex(BUSI_MONEY)));
						mapall.put(BUSI_STATE, 		cursor.getString(cursor.getColumnIndex(BUSI_STATE)));
						mapall.put(BUSI_BILL, 		cursor.getString(cursor.getColumnIndex(BUSI_BILL)));
						mapall.put(BUSI_TIME, 		cursor.getString(cursor.getColumnIndex(BUSI_TIME)));
						mapall.put(BUSI_POS_MONEY, 	cursor.getString(cursor.getColumnIndex(BUSI_POS_MONEY)));
						mapall.put(BUSI_TYPE, 		cursor.getString(cursor.getColumnIndex(BUSI_TYPE)));
						
						mBusiList.add(mapall);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			// 关闭数据库
			close();
		}
		return mBusiList;
	}
    
	// 根据参数查询返回成功交易的笔数 或则失败的总笔数
	public int checksucces(String strBusiState) 
	{
        Cursor cursor = null;
        int nTotal = 0;
        
        try {
        	// 打开数据库
        	open();
        	// 查询
        	cursor = mDB.rawQuery("select count(*) as nums from BUSSINESS where busi_state = '" + strBusiState 
					+ "' and busi_time like '" + mSimpleDateFormat.format(new Date()) + "%'", null);
			if(cursor != null) 
			{
				cursor.moveToFirst();
				String tempstr = cursor.getString(cursor.getColumnIndex("nums"));
				Log.e("", "交易记录共有" + tempstr);
				try
				{
					nTotal = Integer.parseInt(tempstr); 
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
				cursor.close();
			}
		} 
        catch (Exception e) 
        {
			e.printStackTrace();
		} 
        finally 
        {
			// 关闭数据库
        	close();
		} 
		return nTotal;
	}
	
	// 查询当天所有的交易记录
	public int checksall() 
	{
	    Cursor cursor;
	    int nTotal = 0;
	    
	    try {
	    	// 打开数据库
	    	open();
	    	// 查询
	    	cursor = mDB.rawQuery("select count(*) as allnum from BUSSINESS where busshowshijian like + '" + 
	    			mSimpleDateFormat.format(new Date())+"%' ", null);
	    	if(cursor != null)
	    	{
	    		cursor.moveToFirst();
	    		String tempstr = cursor.getString(cursor.getColumnIndex("allnum"));
	    		Log.e("", "交易记录共有" + tempstr);
	    		try
	    		{
	    			nTotal = Integer.parseInt(tempstr);
	    		}
	    		catch(Exception e) 
	    		{
	    			e.printStackTrace();
	    		}
	    		cursor.close();
	    	}
	    } 
	    catch (Exception e) 
	    {
	    	e.printStackTrace();
	    } 
	    finally 
	    {
	    	close();
	    } 
	    return nTotal;
	}
	
	// 获取当天全部交易金额
	public String allmoney() 
	{
		Cursor cursor;
		String strTotalMoney = null;
		
		try {
			// 打开数据库
			open();
			// 查询
			cursor = mDB.rawQuery("select sum(busi_money) as allmoneys from BUSSINESS where busshowshijian like '"
					+ mSimpleDateFormat.format(new Date())+"%' ", null);
			if(cursor != null) 
			{
				cursor.moveToFirst();
				String tempstr = cursor.getString(cursor.getColumnIndex("allmoneys"));
				Log.e("", "当日交易总额为: " + tempstr);
				
				strTotalMoney = tempstr;
				cursor.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			// 关闭数据库
			close();
		}
		return strTotalMoney;
	}
	
	// 查询当日交易成功的总金额或者交易失败的总金额
	public String successOrFailMoney (String strBusiState, String strUserNo)  
	{
	    Cursor cursor = null;
	    String strTotalMoney = null;
	    
	    try {
	    	// 打开数据库
	    	open();
			// 查询
	    	cursor = mDB.rawQuery("select sum(busi_money) as nums from BUSSINESS where busi_time like '" 
	    			+ mSimpleDateFormat.format(new Date()) 
	    			+ "%' and busi_state = '" + strBusiState 
	    			+ "' and busi_user_no = '" + strUserNo + "'", 
	    			null);
	    	if(cursor != null) 
	    	{
	    		cursor.moveToFirst();
	    		String tempstr = cursor.getString(cursor.getColumnIndex("nums"));
	    		Log.e("", strBusiState + "的交易总额为: " + tempstr);
	    		// 
	    		strTotalMoney = tempstr;
	    		cursor.close();
	    	}
		} 
	    catch (Exception e) 
	    {
			e.printStackTrace();
		} 
	    finally 
	    {
			// 关闭数据库
	    	close();
		} 
		return strTotalMoney;
	}
	
	/*
	// 获取今日交易总额
	public String getTodayTradeAmount()
	{
		Double result = new Double(0);
		DecimalFormat df = new DecimalFormat("0.##");
		String isok = "交易成功";
		String sql = new String("select sum("+BUSI_MONEY+") "+BUSI_MONEY+" from "+DB_TABLE+" t where date(t."+BUSI_TIME+") " +
				"= date('now','localtime') and busshowstat='"+isok+"' ");
		Log.e("当天成功交易金额-------->", sql);
		
		try{
			// 打开数据库
			open();
			// 查询
			Cursor cursor = mDB.rawQuery(sql, new String[]{});
			if(cursor == null || !cursor.moveToFirst()) 
			{
				
			}
			else 
			{
				result = cursor.getDouble(cursor.getColumnIndex(BUSI_MONEY));
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			// 关闭数据库
			close();
		}
		return df.format(result);
	}
	
	// 获取上次交易时间
	public String getLastTradeTime()
	{
		String fristTime = null;
		
		try {
			// 打开
			open();
			String sql = new String("select max("+BUS_TIME+") "+BUS_TIME+" from "+DB_TABLE+" t");
			
			Cursor cursor = mDB.rawQuery(sql, new String[]{});
			Map<String, String> map = cursorToMap(cursor);
			if(StringUtil.isEmptyOrNull(map.get(BUS_TIME))||map.get(BUS_TIME).equals("null"))
			{
				fristTime= "暂无交易记录";
			}
			else
			{
				fristTime= map.get(BUS_TIME);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			close();
		}
		 return fristTime;
	}
   
	// 根据日期参数查询返回(全部交易笔数)
	public int checkallnum(String start,String end) 
	{ 
		int i=0;
	    Cursor  cursorall = null;
		try { 
			// 打开
			open();
			String sql="select count(*) as nums from BUSSINESS  where  strftime('%Y%m%d',busshowshijian) between '"+start+"' and   '"+end+"' ";
				
			cursorall=mDB.rawQuery(sql,null);
			Log.e("------>显示sql", sql);
			cursorall.moveToFirst();
			if(!cursorall.isAfterLast())
			Log.e("------->查询的值", cursorall.getString(cursorall.getColumnIndex("nums"))+"");
			Log.e( "-----o", cursorall.getString(cursorall.getColumnIndex("nums"))); 
			i=Integer.valueOf(cursorall.getString(cursorall.getColumnIndex("nums")));

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			close();
		} 
		return i;
	}
	
	// 通过起止日期来查询本地数据库中的数据成功交易笔数
	public int  succchecksall(String start,String end,String istype) 
	{
		int i=0;
		Cursor  cursorall;
		try {
			open(); 
			cursorall=mDB.rawQuery("select count(*) as nums from BUSSINESS  where   busshowstat='"+istype+"' and  strftime('%Y%m%d' ,busshowshijian) between '"+start+"' and  '"+end+"' ",null);
			cursorall.moveToFirst();
			Log.e("----->显示查询到的结果",  cursorall.getCount()+""); 
			Log.e("-----nnnd99---", cursorall.getString(cursorall.getColumnIndex("nums"))) ; 
			i = Integer.valueOf(cursorall.getString(cursorall.getColumnIndex("nums")) );  
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			close();
		} 
		return i;
	}
	
	// 根据查询条件的日期和成功和失败来查询详细明细
	// 根据查询的时间查询记录
	public List<Map<String, Object>> checkalls(String start ,String end) 
	{
		try {
			open();
			buslist.clear();
			String sql="select * from BUSSINESS where strftime('%Y%m%d' , busshowshijian) between  '"+start+"' and  '"+end+"'"+" order by busshowshijian desc";
 					
			Log.v("SQL", sql);
			Cursor cursor = mDB.rawQuery(sql,null);
			cursor.moveToFirst();
			Log.e("----长度---", cursor.getCount()+"");
			if (cursor.getCount() == 0) {
				return null;
			} 
			else 
			{
				while (!cursor.isAfterLast()) 
				{
					Map<String, Object> mapall = new HashMap<String, Object>();
					mapall.put("numbers",
					cursor.getString(cursor.getColumnIndex(BUS_NUMBER)));
					mapall.put("bustype",cursor.getString(cursor.getColumnIndex(Bus_type))); 
					mapall.put("busmeony",
					cursor.getFloat(cursor.getColumnIndex(BUS_MONEY)));
					mapall.put("buststatic",cursor.getString(cursor.getColumnIndex(BUS_STATIC)));
					mapall.put("busno",cursor.getString(cursor.getColumnIndex(BUS_NO)));
					mapall.put("bustime",
					cursor.getString(cursor.getColumnIndex(BUS_TIME)));
					mapall.put("cerverce",
					cursor.getString(cursor.getColumnIndex(BUS_CERVERCE)));
					mapall.put("numtype",
					cursor.getString(cursor.getColumnIndex(BUS_TYPETWO)));
					buslist.add(mapall);
					cursor.moveToNext();
				}
			}
			cursor.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			close();
		}
		return buslist;
	}
	
	// 根据查询条件的日期和成功和失败来查询详细明细	 
	public List<Map<String, Object>> checkallother(String start ,String end,String istype) 
	{
		try {
			open();
			buslist.clear();
		    String sql="select *   from BUSSINESS  where  busshowstat='"+istype+"' and  strftime('%Y%m%d' , busshowshijian) between  '"+start+"' and  '"+end+"'"+" order by busshowshijian desc";
		    Log.v("SQL", sql);
			Cursor cursor = mDB.rawQuery(sql,null);
			cursor.moveToFirst();
			Log.e("----长度---", cursor.getCount()+"");
			if (cursor.getCount() == 0) 
			{
				return null;
			} 
			else 
			{
				while (!cursor.isAfterLast()) 
				{
					Map<String, Object> mapall = new HashMap<String, Object>();
					mapall.put("numbers", cursor.getString(cursor.getColumnIndex(BUS_NUMBER)));
					mapall.put("bustype", cursor.getString(cursor.getColumnIndex(Bus_type)));
					mapall.put("busmeony",cursor.getFloat(cursor.getColumnIndex(BUS_MONEY)));
					mapall.put("buststatic", cursor.getString(cursor.getColumnIndex(BUS_STATIC)));
					mapall.put("busno", cursor.getString(cursor.getColumnIndex(BUS_NO)));
					mapall.put("bustime", cursor.getString(cursor.getColumnIndex(BUS_TIME)));
					mapall.put("cerverce", cursor.getString(cursor.getColumnIndex(BUS_CERVERCE)));
					mapall.put("numtype", cursor.getString(cursor.getColumnIndex(BUS_TYPETWO)));
					buslist.add(mapall);
					cursor.moveToNext();
				}
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return buslist;
	}
	*/
	
	// 删除交易记录
	public static void clean(Context context, String date)
	{
		BusinessNum busiNum = new BusinessNum(context);
		busiNum.open();
		try {
			busiNum.delete(date);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (busiNum != null)
			{
				busiNum.close();
			}
		}	
	}
	
	public boolean delete(String date)
	{
		int iResult = mDB.delete(DB_TABLE, "busi_time < ?", new String[] {date});
		return iResult <= 0 ? false : true;
	}
}
