package com.sunnada.baseframe.database;

import com.sunnada.baseframe.bean.ErrorInfoBean;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

public class ErrorDealAdapter extends DBadapter 
{
	private static final String DB_TABLE 		= "ERRORINFO";			// 数据库表名
	private static final String BUSI_ID 		= "busi_id";			// 记录ID
	private static final String BUSI_PASMID 	= "busi_pasmid";		// PSAM卡号
	public  static final String BUSI_TYPE 		= "busi_type";			// 交易类型
	public  static final String BUSI_BILL 		= "busi_bill";			// 交易流水号
	public  static final String BUSI_REASON 	= "busi_reason";		// 异常原因
	public  static final String BUSI_CRC 		= "busi_crc";			// CRC信息
	public  static final String BUSI_STATE 		= "busi_state";			// 处理结果
	
	public ErrorDealAdapter(Context context) 
	{
		super(context);
	}
	
	// 插入数据
	public long insertData(String t_pasmid, String t_type, String t_no, String t_reason, String t_crc, String t_state) 
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(BUSI_PASMID, 	t_pasmid);
		initialValues.put(BUSI_TYPE, 	t_type);
		initialValues.put(BUSI_BILL, 	t_no);
		initialValues.put(BUSI_REASON, 	t_reason);
		initialValues.put(BUSI_CRC, 	t_crc);
		initialValues.put(BUSI_STATE, 	t_state);
		
		long busiId = mDB.insert(DB_TABLE, BUSI_ID, initialValues);
		if(busiId > 0) 
		{
			Log.e("", "插入异常交易记录成功!");
		}
		else
		{
			Log.e("", "插入异常交易记录失败!");
		}
		return busiId;
	}

	// 删除一条数据
	public boolean deleteData(long rowId) 
	{
		return mDB.delete(DB_TABLE, BUSI_ID + "= '" + rowId + "'", null) > 0;
	}
	
	// 提取所有的异常交易记录
	public Cursor fetchAllData() 
	{
		Cursor mCursor = mDB.query(DB_TABLE, new String[] { BUSI_ID, BUSI_PASMID, BUSI_TYPE, BUSI_BILL,
									BUSI_REASON, BUSI_CRC, BUSI_STATE }, null, null, null, null, null);
		if (mCursor != null) 
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor fetchDataByTag(String t_pasmid, String t_type) 
	{
		Cursor mCursor = mDB.query(
				DB_TABLE, 
				new String[] { BUSI_BILL, BUSI_REASON, BUSI_CRC, BUSI_STATE }, 
				BUSI_PASMID + " = ? and " + BUSI_TYPE + " = ? and " + BUSI_STATE + " = ?", 
				new String[] { t_pasmid, t_type, "1" }, 
				null, 
				null, 
				null);
		if (mCursor != null) 
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	public Cursor fetchTaxData(String t_pasmid, String t_type, String t_no) 
	{
		Cursor mCursor = mDB.query(
				DB_TABLE, 
				new String[] { BUSI_BILL, BUSI_REASON, BUSI_CRC, BUSI_STATE }, 
				BUSI_PASMID + " = ? and " + BUSI_TYPE + " = ? and " + BUSI_STATE + " = ? and "+ BUSI_BILL + " = ?", 
				new String[] { t_pasmid, t_type, "0" , t_no},
				null, 
				null, 
				null);
		
		if (mCursor != null) 
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// 查询指定数据
	public Cursor fetchData(long rowId) throws SQLException 
	{
		Cursor mCursor = mDB.query(
				true, 
				DB_TABLE, 
				new String[] { BUSI_PASMID, BUSI_TYPE, BUSI_BILL, BUSI_REASON, BUSI_CRC, BUSI_STATE }, 
				BUSI_ID + "= '" + rowId + "'", 
				null,
				null, 
				null, 
				null, 
				null);
		if (mCursor != null) 
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// 更新状态
	public boolean updateinfo(String t_pasmid, String t_type, String t_no, String t_reason, String t_crc, String t_state) 
	{
		ContentValues args = new ContentValues();
		
		args.put(BUSI_REASON, 	t_reason);
		args.put(BUSI_CRC, 		t_crc);
		args.put(BUSI_STATE, 	t_state);
		
		int result = mDB.update(
				DB_TABLE, 
				args, 
				BUSI_PASMID + " = ? and " + BUSI_TYPE + " = ? and " + BUSI_BILL + " = ?",  
				new String[] { t_pasmid, t_type }
				);
		if(result > 0)
		{
			Log.e("", "更新异常交易记录成功!");
			return true;
		}
		else
		{
			Log.e("", "更新异常交易记录失败!");
			return false;
		}
	}

	public boolean updateData(String t_pasmid, String t_type, String t_reason, String t_state, String t_crc, String t_no) 
	{
		ContentValues args = new ContentValues();

		args.put(BUSI_REASON, 	t_reason);
		args.put(BUSI_STATE, 	t_state);
		args.put(BUSI_CRC, 		t_crc);
		
		Log.e("修改的的值", "异常原因: " + t_reason + ", 异常状态: " + t_state + ", 异常CRC: " + t_crc);
		int result = mDB.update(
				DB_TABLE, 
				args, 
				BUSI_PASMID + " = ? and " + BUSI_TYPE + " = ? and " + BUSI_BILL + " = ?", 
				new String[] { t_pasmid, t_type, t_no }
				);
		if(result > 0) 
		{
			Log.e("", "更新异常交易记录成功!");
			return true;
		}
		else
		{
			Log.e("", "更新异常交易记录失败!");
			return false;
		}
	}
	
	// 从当前游标处获得指定数据
	public String getData(Cursor cur, String key) 
	{
		return cur.getString(cur.getColumnIndex(key));
	}
	
	// 获取流水号、异常原因、CRC、最终状态
	public static ErrorInfoBean[] getErrorInfo(Context context, String t_pasmid, String t_type) 
	{
		Log.e("", "PSAM ID为: " + t_pasmid + ", 异常交易类型为: " + t_type);
		// 读取偏好设置数据库
		ErrorDealAdapter errorDealAdapter = new ErrorDealAdapter(context);
		ErrorInfoBean[]  errroBean = null;
		int index = 0;
		
		try {
			errorDealAdapter.open();
			Cursor cur = errorDealAdapter.fetchDataByTag(t_pasmid, t_type);
			if (null != cur && cur.getCount() != 0) 
			{
				errroBean = new ErrorInfoBean[cur.getCount()];
				Log.v("", "找到"+cur.getCount()+"条异常交易记录!");
				while (!cur.isAfterLast())
				{
					errroBean[index] 		= new ErrorInfoBean();
					errroBean[index].no 	= cur.getString(cur.getColumnIndex(BUSI_BILL));
					errroBean[index].reason = cur.getString(cur.getColumnIndex(BUSI_REASON));
					errroBean[index].crc 	= cur.getString(cur.getColumnIndex(BUSI_CRC));
					errroBean[index].state  = cur.getString(cur.getColumnIndex(BUSI_STATE));
					Log.e("异常交易数据", errroBean[index].no + "|" + errroBean[index].reason + "|" 
							+ errroBean[index].crc + "|" + errroBean[index].state);
					cur.moveToNext();
					index++;
				}
			}
			else
			{
				Log.e("", "未找到异常交易记录!");
			}
			if(cur != null) 
			{
				cur.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if(errorDealAdapter != null) 
			{
				errorDealAdapter.close();
			}
		}
		return errroBean;
	}

	// 号卡、购买电子卡、充值流水号时候插入数据
	public static void setErrorInfo(Context context, String t_pasmid, String t_type, String t_no, String t_reason, String t_crc, String t_state)
	{
		ErrorDealAdapter errorDealAdapter = new ErrorDealAdapter(context);
		Log.e("插入记录", "异常PSAM卡号: " + t_pasmid + ", 异常类型: " + t_type + ", 异常流水: " + t_no 
				+ ", 异常原因: " + t_reason + ", 异常CRC: " + t_crc + ", 异常状态: " + t_state);
		
		// 判断是否有记录
		try {
			errorDealAdapter.open();
			Cursor cur = errorDealAdapter.fetchDataByTag(t_pasmid, t_type);
			// 如果没有数据
			if (null == cur || cur.getCount() == 0) 
			{
				Log.e("", "无记录, 插入");
				errorDealAdapter.insertData(t_pasmid, t_type, t_no, t_reason, t_crc, t_state);
			} 
			else 
			{
				Log.e("", "有记录, 更新");
				errorDealAdapter.updateinfo(t_pasmid, t_type, t_no, t_reason, t_crc, t_state);
			}
			if(cur != null) 
			{
				cur.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if(errorDealAdapter != null) 
			{
				errorDealAdapter.close();
			}
		}
	}
	
	// 更新异常交易记录的状态
	public static void setErrorInfoState(Context context, String t_pasmid, String t_type, String t_reason, String t_state, String t_crc, String t_no) 
	{
		ErrorDealAdapter errDealAdapter = new ErrorDealAdapter(context);
		
		try 
		{ 
			errDealAdapter.open();
			errDealAdapter.updateData(t_pasmid, t_type, t_reason, t_state, t_crc, t_no);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if(errDealAdapter != null) 
			{
				errDealAdapter.close();
				errDealAdapter = null;
			}
		}
	}
	
	// 删除交易记录
	public static void clean(Context context) 
	{
		ErrorDealAdapter errorDealAdapter = new ErrorDealAdapter(context);
		errorDealAdapter.open();
		
		try 
		{
			errorDealAdapter.delete("0");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (errorDealAdapter != null)
			{
				errorDealAdapter.close();
			}
		}
	}
	
	// 删除状态为state的异常交易记录
	public boolean delete(String state) 
	{
		int iResult = mDB.delete(DB_TABLE, BUSI_STATE + " = '?'", new String[] {state});
		if(iResult < 0) 
		{
			Log.e("", "删除异常交易记录失败!");
			return false;
		}
		else
		{
			Log.e("", "删除异常交易记录成功!");
			return true;
		}
	}
}
