/**
 * $RCSfile: BaseDao.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.service.IDataBaseService;

/**
 * <p>Title: BaseDao</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-30
 * @version 1.0
 */

public class BaseDao
{
	/**
	 * 批量sql语句
	 */
	private List<String> sqls = new ArrayList<String>();
	
	protected IDataBaseService dataBaseService = null;
	private boolean batch = false;//是否开启批量SQL更新语句

	public BaseDao(IDataBaseService dataBaseService)
	{
		super();
		this.dataBaseService = dataBaseService;
	}
	
	/**
	 * 开启批量更新语句
	 * @param sql
	 */
	public void beginSQLBatch()
	{
		batch = true;
	}
	
	/**
	 * 添加一个sql到当前事务
	 * @param sql
	 */
	public void addToSQLBatch(String sql)
	{
		sqls.add(sql);
	}
	
	/**
	 * 提交事务
	 */
	public boolean executeSQLBath()
	{
		String [] sqlArray = new String[sqls.size()];
		
		for(int i = 0;i<sqls.size();i++)
		{
			sqlArray[i] = sqls.get(i);
		}
		
		boolean ret = executeUpdateSQL(sqlArray);;
		clearUpdateSQLBatch();
		return ret;
			
	}
	
	/**
	 * 清除批量sql
	 */
	public void clearUpdateSQLBatch()
	{
		sqls.clear();
		batch = false;
	}
	
	/**
	 * 执行更新语句
	 * @param sql
	 * @return
	 */
	public boolean executeUpdateSQL(String sql)
	{
		boolean ret = false;
		
		try
		{
			if(batch)
			{
				addToSQLBatch(sql);
				return true;
			}
			ret = dataBaseService.executeUpdateSQL(sql);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 执行更新语句
	 * @param sql
	 * @return
	 */
	public boolean executeUpdateSQL(String sql, String[] params)
	{
		boolean ret = false;
		
		try
		{
			ret = dataBaseService.executeUpdateSQLWithParams(sql,params);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 执行更新语句
	 * @param sql
	 * @return
	 */
	public boolean executeUpdateSQL(String []sql)
	{
		boolean ret = false;
		
		try
		{
			ret = dataBaseService.executeUpdateSQLWithTransaction(sql);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 执行查询语句
	 * @param sql
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql)
	{
		List<Map<String, String>> list = null;
		try
		{
			list = dataBaseService.executeQuerySQL(sql);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 执行分页查询
	 * @param sql 查询语句
	 * @param start 开始位置
	 * @param count 个数
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql,int start,int count)
	{
		List<Map<String, String>> list = null;
		try
		{
			list = dataBaseService.executeQuerySQLByPage(sql, start, count);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}


	/**
	 * 执行查询语句
	 * @param sql
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql,String[] params)
	{
		List<Map<String, String>> list = null;
		try
		{
			list = dataBaseService.executeQuerySQLWithParams(sql,params);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 执行查询语句
	 * @param sql
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql,String[] params,int start,int count)
	{
		List<Map<String, String>> list = null;
		try
		{
			list = dataBaseService.executeQuerySQLByPageAndParams(sql, params, start, count);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
}


