package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.bean.HistoryPhoneModel;
import com.sunnada.baseframe.bean.PhoneModel;

import android.content.Context;
import android.database.Cursor;

public class HistoryPhoneYhgjDao extends SqLitBase
{
	public static final String	TABLE_NAME				= "HS_PHONE_PARAMS";
	private static final String	COLUMN_ID				= "p_id";
	private static final String	COLUMN_NAME				= "p_name";
	private static final String	COLUMN_BAND				= "p_band";

	private static final String	COLUMN_COLOR			= "p_color";
	private static final String	COLUMN_PIC				= "p_pic";
	private static final String	COLUMN_BIGPIC			= "p_bigpics";
	private static final String	COLUMN_PRICE			= "p_price";
	private static final String	COLUMN_SCREEN			= "p_screen";
	private static final String	COLUMN_SCREENPARAM		= "p_screenparam";
	private static final String	COLUMN_OP				= "p_op";
	private static final String	COLUMN_NET				= "p_net";
	private static final String	COLUMN_CORENUM			= "p_corenum";
	private static final String	COLUMN_CPU				= "p_cpu";
	private static final String	COLUMN_SIZE				= "p_size";
	private static final String	COLUMN_ROM				= "p_rom";
	private static final String	COLUMN_RAM				= "p_ram";
	private static final String	COLUMN_EXTEND			= "p_extend";
	private static final String	COLUMN_BATTERYSTORAGE	= "p_batterystorage";
	private static final String	COLUMN_BATTERY			= "p_battery";
	private static final String	COLUMN_MUSICFORMAT		= "p_musicformat";
	private static final String	COLUMN_VEDIOFORMAT		= "p_vedioformat";
	private static final String	COLUMN_JAVA				= "p_java";
	private static final String	COLUMN_RADIO			= "p_radio";
	private static final String	COLUMN_RECORD			= "p_record";
	private static final String	COLUMN_CAMERA1			= "p_camera1";
	private static final String	COLUMN_CAMERA2			= "p_camera2";
	private static final String	COLUMN_ZOOM				= "p_zoom";
	private static final String	COLUMN_DATATRANS		= "p_datatrans";
	private static final String	COLUMN_GPS				= "p_gps";
	private static final String	COLUMN_CALLTIME			= "p_calltime";
	private static final String	COLUMN_WAITTIME			= "p_waittime";
	private static final String	COLUMN_ACCESS			= "p_access";
	public static final String	EDIT_TIME				= "p_edit_time";

	public HistoryPhoneYhgjDao(Context context)
	{
		super(context);
		//CreatTable();
	}
	
	/*
	public void CreatTable()
	{
		Exec(createSQL);
	}
	*/
	
	// 插入数据
	public void insertData(PhoneModel model)
	{
		String editTiem = Long.toString(System.currentTimeMillis());
		String sql = "INSERT INTO " + TABLE_NAME + " (" + COLUMN_ID + "," + COLUMN_BAND + "," + COLUMN_NAME + ","
				+ COLUMN_COLOR + "," + COLUMN_PIC + "," + COLUMN_BIGPIC + "," + COLUMN_PRICE + "," + COLUMN_SCREEN
				+ "," + COLUMN_SCREENPARAM + "," + COLUMN_OP + "," + COLUMN_NET + "," + COLUMN_CORENUM + ","
				+ COLUMN_CPU + "," + COLUMN_SIZE + "," + COLUMN_ROM + "," + COLUMN_RAM + "," + COLUMN_EXTEND + ","
				+ COLUMN_BATTERY + "," + COLUMN_BATTERYSTORAGE + "," + COLUMN_MUSICFORMAT + "," + COLUMN_VEDIOFORMAT
				+ "," + COLUMN_JAVA + "," + COLUMN_RADIO + "," + COLUMN_RECORD + "," + COLUMN_CAMERA1 + ","
				+ COLUMN_CAMERA2 + "," + COLUMN_ZOOM + "," + COLUMN_DATATRANS + "," + COLUMN_GPS + ","
				+ COLUMN_CALLTIME + "," + COLUMN_WAITTIME + "," + COLUMN_ACCESS + "," + EDIT_TIME
				+ ") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s')";
		sql = String.format(sql, model.getId(), model.getBand(), model.getName(), model.getColor(), model.getPicPath(),
				model.getBigPic(), model.getPrice(), model.getScreenSize(), model.getScreenParam(), model.getOp(),
				model.getNet(), model.getCoreNum(), model.getCpu(), model.getSize(), model.getRom(), model.getRam(),
				model.getExtendRom(), model.getBattery(), model.getBatteryStorage(), model.getMusicFormat(),
				model.getVedioFormat(), model.getJava(), model.getRadio(), model.getRecord(), model.getCamera1(),
				model.getCamera2(), model.getZoom(), model.getDataTrans(), model.getGps(), model.getCallTime(),
				model.getWaitTime(), model.getAccess(), editTiem);
		Exec(sql);
	}
	
	// 清楚全部记录
	public void clear()
	{
		Exec("delete from " + TABLE_NAME + " where 1=1");
	}
	
	// 删除某条记录
	public boolean deletePhone(String phoneId)
	{
		String sql = "delete from " + TABLE_NAME + " where " + COLUMN_ID + " = '%s'";
		sql = String.format(sql, phoneId);
		return executeUpdateSQL(sql);
	}
		
	
	public boolean deletePhone(HistoryPhoneModel model)
	{
		int  result = delete(model);
		return result > 0 ? true : false;
	}
	
	// 获取记录
	public List<PhoneModel> getData()
	{
		String sql  = "select * from " + TABLE_NAME + " order by " + EDIT_TIME + " DESC";
		List<PhoneModel> data = new ArrayList<PhoneModel>();
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		if (mapList != null && mapList.size() > 0)
		{
			for(int i = 0; i < mapList.size(); i++)
			{
				Map<String, String> map = mapList.get(i);
				data.add(getModel(map));
			}
		}
		if(data.size() > 0)
		{
			return data;
		}
		return null;
	}
	
	// 20131108 新增
	public <T> long insert(HistoryPhoneModel model)
	{
		return super.insert(model);
	}
	
	// 20131108 新增
	public List<HistoryPhoneModel> getData(HistoryPhoneModel model)
	{
		Cursor cursor = accurateSearch(model, 0, 0, new String[]{EDIT_TIME}, new String[]{"DESC"});
		List<Map<String, String>> selectList = new ArrayList<Map<String,String>>();
		if(cursor != null)
		{
			if (cursor.getCount() == 0)
			{
				return null;
			}
			else 
			{
				cursor.moveToFirst();
				// 获取每条记录列名
				String[] colName = cursor.getColumnNames();
				while (!cursor.isAfterLast())
				{
					Map<String, String> map = new HashMap<String, String>();
					// 循环获取列名对应的数据
					for (int j = 0; j < colName.length; j++)
					{
						map.put(colName[j], cursor.getString(cursor.getColumnIndex(colName[j])));
					}
					selectList.add(map);
					cursor.moveToNext();
				} 
			}
			// 关闭游标
			cursor.close();
		}
		List<HistoryPhoneModel> result = new ArrayList<HistoryPhoneModel>();
		if(selectList.size() > 0)
		{
			for(int i = 0; i < selectList.size(); i++)
			{
				result.add(getModel(selectList.get(i)));
			}
		}
		return result;
	}
	
	// 解析机型数据
	private HistoryPhoneModel getModel(Map<String, String> map)
	{
		HistoryPhoneModel model = new HistoryPhoneModel();
		model.setId(COLUMN_ID);
		model.setAccess(map.get(COLUMN_ACCESS));
		model.setBand(map.get(COLUMN_BAND));
		model.setBattery(map.get(COLUMN_BATTERY));
		model.setBatteryStorage(map.get(COLUMN_BATTERYSTORAGE));
		model.setBigPic(map.get(COLUMN_BIGPIC));
		model.setCallTime(map.get(COLUMN_CALLTIME));
		model.setCamera1(map.get(COLUMN_CAMERA1));
		model.setCamera2(map.get(COLUMN_CAMERA2));
		model.setColor(map.get(COLUMN_COLOR));
		model.setCoreNum(map.get(COLUMN_CORENUM));
		model.setCpu(map.get(COLUMN_CPU));
		model.setDataTrans(map.get(COLUMN_DATATRANS));
		model.setExtendRom(map.get(COLUMN_EXTEND));
		model.setGps(map.get(COLUMN_GPS));
		model.setId(map.get(COLUMN_ID));
		model.setJava(map.get(COLUMN_JAVA));
		model.setMusicFormat(map.get(COLUMN_MUSICFORMAT));
		model.setName(map.get(COLUMN_NAME));
		model.setNet(map.get(COLUMN_NET));
		model.setOp(map.get(COLUMN_OP));
		model.setPicPath(map.get(COLUMN_PIC));
		model.setPrice(map.get(COLUMN_PRICE));
		model.setRadio(map.get(COLUMN_RADIO));
		model.setRam(map.get(COLUMN_RAM));
		model.setRecord(map.get(COLUMN_RECORD));
		model.setRom(map.get(COLUMN_ROM));
		model.setScreenParam(map.get(COLUMN_SCREENPARAM));
		model.setScreenSize(map.get(COLUMN_SCREEN));
		model.setSize(map.get(COLUMN_SIZE));
		model.setVedioFormat(map.get(COLUMN_VEDIOFORMAT));
		model.setWaitTime(map.get(COLUMN_WAITTIME));
		model.setZoom(map.get(COLUMN_ZOOM));
		model.setEdittime(map.get(EDIT_TIME));
		model.isParamLoaded = true;
		return model;
	}
	
}
