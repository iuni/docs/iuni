package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.RecommendPackage;

import android.content.Context;

public class RecommendPackageDao extends SqLitBase
{
	private static final String	TABLE_NAME				= "RECOMMEND_PACK_PARAMS";
	private static final String	COLUMN_ID				= "p_id";
	private static final String	COLUMN_MODEL_ID			= "p_model_id";
	private static final String	COLUMN_NAME				= "p_name";
	private static final String	COLUMN_BAND				= "p_band";

	private static final String	COLUMN_COLOR			= "p_color";
	private static final String	COLUMN_PIC				= "p_pic";
	private static final String	COLUMN_BIGPIC			= "p_bigpics";
	private static final String	COLUMN_PRICE			= "p_price";
	private static final String	COLUMN_SCREEN			= "p_screen";
	private static final String	COLUMN_SCREENPARAM		= "p_screenparam";
	private static final String	COLUMN_OP				= "p_op";
	private static final String	COLUMN_NET				= "p_net";
	private static final String	COLUMN_CORENUM			= "p_corenum";
	private static final String	COLUMN_CPU				= "p_cpu";
	private static final String	COLUMN_SIZE				= "p_size"; 
	private static final String	COLUMN_ROM				= "p_rom";
	private static final String	COLUMN_RAM				= "p_ram";
	private static final String	COLUMN_EXTEND			= "p_extend";
	private static final String	COLUMN_BATTERYSTORAGE	= "p_batterystorage";
	private static final String	COLUMN_BATTERY			= "p_battery";
	private static final String	COLUMN_MUSICFORMAT		= "p_musicformat";
	private static final String	COLUMN_VEDIOFORMAT		= "p_vedioformat";
	private static final String	COLUMN_JAVA				= "p_java";
	private static final String	COLUMN_RADIO			= "p_radio";
	private static final String	COLUMN_RECORD			= "p_record";
	private static final String	COLUMN_CAMERA1			= "p_camera1";
	private static final String	COLUMN_CAMERA2			= "p_camera2";
	private static final String	COLUMN_ZOOM				= "p_zoom";
	private static final String	COLUMN_DATATRANS		= "p_datatrans";
	private static final String	COLUMN_GPS				= "p_gps";
	private static final String	COLUMN_CALLTIME			= "p_calltime";
	private static final String	COLUMN_WAITTIME			= "p_waittime";
	private static final String	COLUMN_ACCESS			= "p_access";
	
	// 推荐包信息
	private static final String	COLUMN_PACK_ID			= "r_pack_id";			// 整个套餐包ID
	private static final String	COLUMN_DES				= "r_des";				// 推荐包宣传口号
	private static final String	COLUMN_AERA_LONG		= "r_area_long";		// 所属地区 如：全国-福建省-福州
	private static final String	COLUMN_AERA_SHORT		= "r_area_short";		// 所属地区如：福州
	private static final String	COLUMN_AERA_CODE		= "r_area_code";		// 区域编码
	
	private static final String	COLUMN_CONTRACT_ID		= "r_contract_id";		// 合约ID
	private static final String	COLUMN_CONTRACT_CODE	= "r_contract_code";	// 合约编码
	private static final String	COLUMN_CONTRACT_NAME	= "r_contract_name";	// 合约名称
	private static final String	COLUMN_CONTRACT_DATE	= "r_contract_date";	// 合约期限
	
	private static final String	COLUMN_PRODUCT_ID		= "r_product_id";		// 套餐ID
	private static final String	COLUMN_PRODUCT_CODE		= "r_product_code";		// 套餐编码
	private static final String	COLUMN_PRODUCT_NAME		= "r_product_name";		// 套餐名称
	private static final String	COLUMN_PRODUCT_PRICE	= "r_product_price";	// 套餐档次
	private static final String	COLUMN_PRODUCT_TYPE		= "r_product_type";		// 套餐类型
	
	private static final String	COLUMN_ACTIVITY_TYPE	= "r_activity_type";	// 活动类型
	private static final String	COLUMN_ACTIVITY_CONTENT	= "r_activity_content";	// 活动内容
	private static final String	COLUMN_ACTIVITY_DETAILS	= "r_activity_details";	// 活动详情
	private static final String	COLUMN_MAKETTING_TAG	= "r_marketing_tag";	// 营销标签
	
	private static final String	COLUMN_PIC2				= "r_pic";				// 小图名称
	private static final String	COLUMN_BIGPIC2			= "r_bigpics";			// 大图名称
	
	private static final String	COLUMN_FLOW				= "r_flow";				// 国内流量
	private static final String	COLUMN_CALL_TIME		= "r_call_time";		// 国内拨打分钟数
	private static final String	COLUMN_MSG_COUNT		= "r_msg_count";		// 国内短信发送条数
	private static final String	COLUMN_CALL_FREE		= "r_call_free";		// 国内免费拨打分钟数
	private static final String	COLUMN_CALL_FEE			= "r_call_fee";			// 国内拨打费用
	private static final String	COLUMN_FLOW_FEE			= "r_flow_fee";			// 国内流量费用
	private static final String	COLUMN_VEDIO_CALL		= "r_vedio_call";		// 视屏通话分钟数
	private static final String	COLUMN_OTHER			= "r_other";			// 其他
	
	private static final String	COLUMN_IS_GIFT			= "r_is_gift";			// 是否有赠品
	private static final String	COLUMN_GIFT_INFO		= "r_gift_info";		// 赠品说明
	private static final String	COLUMN_GIFT1			= "r_gift1";			// 赠品1
	private static final String	COLUMN_GIFT2			= "r_gift2";			// 赠品2
	private static final String	COLUMN_GIFT3			= "r_gift3";			// 赠品3
	private static final String	COLUMN_GIFT4			= "r_gift4";			// 赠品4
	private static final String	COLUMN_GIFT5			= "r_gift5";			// 赠品5
	

	public RecommendPackageDao(Context context)
	{
		super(context);
	}
	
	/*
	public void CreatTable()
	{
		Exec(createSQL);
	}
	*/

	public void clear()
	{
		Exec("delete from " + TABLE_NAME + " where 1=1");
	}
	
	public void insertData(RecommendPackage pack)
	{
		System.out.println("insert into RecommendPackage_Params...");
		PhoneModel model = pack.getmPhoneModel();
		ProductInfo product = pack.getmProduct();
		String sql = "INSERT INTO " + TABLE_NAME + " (" + COLUMN_ID + "," + COLUMN_MODEL_ID + "," + COLUMN_BAND + "," + COLUMN_NAME + ","
				+ COLUMN_COLOR + "," + COLUMN_PIC + "," + COLUMN_BIGPIC + "," + COLUMN_PRICE + "," + COLUMN_SCREEN
				+ "," + COLUMN_SCREENPARAM + "," + COLUMN_OP + "," + COLUMN_NET + "," + COLUMN_CORENUM + ","
				+ COLUMN_CPU + "," + COLUMN_SIZE + "," + COLUMN_ROM + "," + COLUMN_RAM + "," + COLUMN_EXTEND + ","
				+ COLUMN_BATTERY + "," + COLUMN_BATTERYSTORAGE + "," + COLUMN_MUSICFORMAT + "," + COLUMN_VEDIOFORMAT
				+ "," + COLUMN_JAVA + "," + COLUMN_RADIO + "," + COLUMN_RECORD + "," + COLUMN_CAMERA1 + ","
				+ COLUMN_CAMERA2 + "," + COLUMN_ZOOM + "," + COLUMN_DATATRANS + "," + COLUMN_GPS + ","
				+ COLUMN_CALLTIME + "," + COLUMN_WAITTIME + "," + COLUMN_ACCESS + ","
				
				+ COLUMN_PACK_ID + "," + COLUMN_DES + ","+ COLUMN_AERA_LONG + "," + COLUMN_AERA_SHORT + ","
				+ COLUMN_AERA_CODE + "," + COLUMN_CONTRACT_ID + "," + COLUMN_CONTRACT_CODE + "," 
				+ COLUMN_CONTRACT_NAME + "," + COLUMN_CONTRACT_DATE + "," + COLUMN_PRODUCT_ID + ","
				+ COLUMN_PRODUCT_CODE + "," + COLUMN_PRODUCT_NAME + "," + COLUMN_PRODUCT_PRICE + "," + COLUMN_PRODUCT_TYPE + "," 
				+ COLUMN_ACTIVITY_TYPE + "," + COLUMN_ACTIVITY_CONTENT + ","  + COLUMN_ACTIVITY_DETAILS + "," + COLUMN_MAKETTING_TAG + "," 
				+ COLUMN_BIGPIC2 + "," + COLUMN_PIC2 + "," + COLUMN_CALL_FEE + "," 
				+ COLUMN_FLOW + ","+ COLUMN_CALL_TIME + ","+ COLUMN_MSG_COUNT+ "," + COLUMN_CALL_FREE + "," + COLUMN_FLOW_FEE + ","
				+ COLUMN_VEDIO_CALL + ","+ COLUMN_OTHER+ "," + COLUMN_IS_GIFT + "," + COLUMN_GIFT_INFO + "," + COLUMN_GIFT1 + ","
				+ COLUMN_GIFT2 + ","+ COLUMN_GIFT3+ "," + COLUMN_GIFT4 + "," + COLUMN_GIFT5
				+ ") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s',"
				+"'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s')";
		sql = String.format(sql, pack.getmId(), model.getId(), model.getBand(), model.getName(), model.getColor(), model.getPicPath(),
				model.getBigPic(), model.getPrice(), model.getScreenSize(), model.getScreenParam(), model.getOp(),
				model.getNet(), model.getCoreNum(), model.getCpu(), model.getSize(), model.getRom(), model.getRam(),
				model.getExtendRom(), model.getBattery(), model.getBatteryStorage(), model.getMusicFormat(),
				model.getVedioFormat(), model.getJava(), model.getRadio(), model.getRecord(), model.getCamera1(),
				model.getCamera2(), model.getZoom(), model.getDataTrans(), model.getGps(), model.getCallTime(),
				model.getWaitTime(), model.getAccess(), 
				
				product.getId(),product.getmDescribe(), product.getmAreaLong(), product.getmAreaShort(), product.getmAreaCode(), product.getmContractID(),
				product.getmContractCode(), product.getmContractName(), product.getmContractDate(), product.getmProductID(), product.getmProductCode(), product.getmProductName(),
				product.getmProductPrice(), product.getmProductType(), product.getmActivityType(), product.getmActivityContent(), product.getmActivityDetails(), product.getmMarktingTag(),
				product.getmBigPic(), product.getmPicPath(),product.getmFlow1(), product.getmCallTime(), product.getmMsgCount(), product.getmFree(), product.getmCallFee(),product.getmFlow2(), 
				product.getmVideoCall(), product.getmOther(), product.getmIsGift(), product.getmGiftInfo(), product.getmGift1(), product.getmGift2(),
				product.getmGift3(),product.getmGift4(),product.getmGift5());
		Exec(sql);
	}

	// 根据机型ID获取推荐包s
	public RecommendPackage getPacakgeByPhoneID(String phoneID)
	{
		if (phoneID == null)
		{
			return null;
		}
		String sql = "select * from " + TABLE_NAME + " where " + COLUMN_MODEL_ID + " = " + phoneID;
		RecommendPackage pack = null;
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		if (mapList != null && mapList.size() > 0)
		{
			Map<String, String> map = mapList.get(0);
			pack = getPakage(map);
			return pack;
		}
		return null;
	}
	
	// 获取全部推荐包信息
	public List<RecommendPackage> getAllRecommendPackages()
	{
		String sql = "select * from " + TABLE_NAME;
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		RecommendPackage pack = null;
		List<RecommendPackage> list = new ArrayList<RecommendPackage>();
		if (mapList != null && mapList.size() > 0)
		{
			for(int i = 0; i < mapList.size(); i++)
			{
				Map<String, String> map = mapList.get(i);
				pack = getPakage(map);
				list.add(pack);
			}
		}
		return list;
	}
		
	private RecommendPackage getPakage(Map<String, String> map)
	{
		RecommendPackage pack = new RecommendPackage();
		pack.setmId(map.get(COLUMN_ID)); 
		PhoneModel model = pack.getmPhoneModel();
		ProductInfo product = pack.getmProduct();
		// 手机参数信息
		model.setId(map.get(COLUMN_MODEL_ID));
		model.setAccess(map.get(COLUMN_ACCESS));
		model.setBand(map.get(COLUMN_BAND));
		model.setBattery(map.get(COLUMN_BATTERY));
		model.setBatteryStorage(map.get(COLUMN_BATTERYSTORAGE));
		model.setBigPic(map.get(COLUMN_BIGPIC));
		model.setCallTime(map.get(COLUMN_CALLTIME));
		model.setCamera1(map.get(COLUMN_CAMERA1));
		model.setCamera2(map.get(COLUMN_CAMERA2));
		model.setColor(map.get(COLUMN_COLOR));
		model.setCoreNum(map.get(COLUMN_CORENUM));
		model.setCpu(map.get(COLUMN_CPU));
		model.setDataTrans(map.get(COLUMN_DATATRANS));
		model.setExtendRom(map.get(COLUMN_EXTEND));
		model.setGps(map.get(COLUMN_GPS));
		model.setId(map.get(COLUMN_MODEL_ID));
		model.setJava(map.get(COLUMN_JAVA));
		model.setMusicFormat(map.get(COLUMN_MUSICFORMAT));
		model.setName(map.get(COLUMN_NAME));
		model.setNet(map.get(COLUMN_NET));
		model.setOp(map.get(COLUMN_OP));
		model.setPicPath(map.get(COLUMN_PIC));
		model.setPrice(map.get(COLUMN_PRICE));
		model.setRadio(map.get(COLUMN_RADIO));
		model.setRam(map.get(COLUMN_RAM));
		model.setRecord(map.get(COLUMN_RECORD));
		model.setRom(map.get(COLUMN_ROM));
		model.setScreenParam(map.get(COLUMN_SCREENPARAM));
		model.setScreenSize(map.get(COLUMN_SCREEN));
		model.setSize(map.get(COLUMN_SIZE));
		model.setVedioFormat(map.get(COLUMN_VEDIOFORMAT));
		model.setWaitTime(map.get(COLUMN_WAITTIME));
		model.setZoom(map.get(COLUMN_ZOOM));
		model.isParamLoaded = true;
		
		// 推荐包信息
		product.setId(map.get(COLUMN_PACK_ID));
		product.setmPrice(map.get(COLUMN_PRICE));
		product.setmDescribe(map.get(COLUMN_DES));
		product.setmAreaLong(map.get(COLUMN_AERA_LONG));
		product.setmAreaShort(map.get(COLUMN_AERA_SHORT));
		product.setmAreaCode(map.get(COLUMN_AERA_CODE));
		
		product.setmContractID(map.get(COLUMN_CONTRACT_ID));
		product.setmContractCode(map.get(COLUMN_CONTRACT_CODE));
		product.setmContractName(map.get(COLUMN_CONTRACT_NAME));
		product.setmContractDate(map.get(COLUMN_CONTRACT_DATE));
		
		product.setmProductID(map.get(COLUMN_PRODUCT_ID));
		product.setmProductCode(map.get(COLUMN_PRODUCT_CODE));
		product.setmProductName(map.get(COLUMN_PRODUCT_NAME));
		product.setmProductPrice(map.get(COLUMN_PRODUCT_PRICE));
		product.setmProductType(map.get(COLUMN_PRODUCT_TYPE));
		
		product.setmActivityType(map.get(COLUMN_ACTIVITY_TYPE));
		product.setmActivityContent(map.get(COLUMN_ACTIVITY_CONTENT));
		product.setmActivityDetails(map.get(COLUMN_ACTIVITY_DETAILS));
		product.setmMarktingTag(map.get(COLUMN_MAKETTING_TAG));
		
		product.setmPicPath(map.get(COLUMN_PIC2));
		product.setmBigPic(map.get(COLUMN_BIGPIC2));
		
		product.setmFlow1(map.get(COLUMN_FLOW));
		product.setmCallTime(map.get(COLUMN_CALL_TIME));
		product.setmMsgCount(map.get(COLUMN_MSG_COUNT));
		product.setmFree(map.get(COLUMN_CALL_FREE));
		product.setmCallFee(map.get(COLUMN_CALL_FEE));
		product.setmFlow2(map.get(COLUMN_FLOW_FEE));
		product.setmVideoCall(map.get(COLUMN_VEDIO_CALL));
		product.setmOther(map.get(COLUMN_OTHER));
		
		product.setmIsGift(map.get(COLUMN_IS_GIFT));
		product.setmGiftInfo(map.get(COLUMN_GIFT_INFO));
		product.setmGift1(map.get(COLUMN_GIFT1));
		product.setmGift2(map.get(COLUMN_GIFT2));
		product.setmGift3(map.get(COLUMN_GIFT3));
		product.setmGift4(map.get(COLUMN_GIFT4));
		product.setmGift5(map.get(COLUMN_GIFT5));
		return pack;
	}
}
