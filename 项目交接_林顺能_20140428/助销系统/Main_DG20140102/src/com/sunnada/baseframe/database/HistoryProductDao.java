package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.RecommendPackage;

import android.content.Context;

public class HistoryProductDao extends SqLitBase
{ 
	private static final String	TABLE_NAME				= "HS_PRODUCT_PARAMS";
	private static final String	COLUMN_ID				= "r_id";				// 套餐rec_ID整个包的ID
	private static final String	COLUMN_PRICE			= "r_price";			// 套餐价格
	private static final String	COLUMN_DES				= "r_des";				// 推荐包宣传口号
	private static final String	COLUMN_AERA_LONG		= "r_area_long";		// 所属地区 如：全国-福建省-福州
	private static final String	COLUMN_AERA_SHORT		= "r_area_short";		// 所属地区如：福州
	private static final String	COLUMN_AERA_CODE		= "r_area_code";		// 区域编码
	
	private static final String	COLUMN_CONTRACT_ID		= "r_contract_id";		// 合约ID
	private static final String	COLUMN_CONTRACT_CODE	= "r_contract_code";	// 合约编码
	private static final String	COLUMN_CONTRACT_NAME	= "r_contract_name";	// 合约名称
	private static final String	COLUMN_CONTRACT_DATE	= "r_contract_date";	// 合约期限
	
	private static final String	COLUMN_PRODUCT_ID		= "r_product_id";		// 套餐ID
	private static final String	COLUMN_PRODUCT_CODE		= "r_product_code";		// 套餐编码
	private static final String	COLUMN_PRODUCT_NAME		= "r_product_name";		// 套餐名称
	private static final String	COLUMN_PRODUCT_PRICE	= "r_product_price";	// 套餐档次
	private static final String	COLUMN_PRODUCT_TYPE		= "r_product_type";		// 套餐类型
	
	private static final String	COLUMN_ACTIVITY_TYPE	= "r_activity_type";	// 活动类型
	private static final String	COLUMN_ACTIVITY_CONTENT	= "r_activity_content";	// 活动内容
	private static final String	COLUMN_ACTIVITY_DETAILS	= "r_activity_details";	// 活动详情
	private static final String	COLUMN_MAKETTING_TAG	= "r_marketing_tag";	// 营销标签
	
	private static final String	COLUMN_PIC				= "r_pic";				// 小图名称
	private static final String	COLUMN_BIGPIC			= "r_bigpics";			// 大图名称
	
	private static final String	COLUMN_FLOW				= "r_flow";				// 国内流量
	private static final String	COLUMN_CALL_TIME		= "r_call_time";		// 国内拨打分钟数
	private static final String	COLUMN_MSG_COUNT		= "r_msg_count";		// 国内短信发送条数
	private static final String	COLUMN_CALL_FREE		= "r_call_free";		// 国内免费拨打分钟数
	private static final String	COLUMN_CALL_FEE			= "r_call_fee";			// 国内拨打费用
	private static final String	COLUMN_FLOW_FEE			= "r_flow_fee";			// 国内流量费用
	private static final String	COLUMN_VEDIO_CALL		= "r_vedio_call";		// 视屏通话分钟数
	private static final String	COLUMN_OTHER			= "r_other";			// 其他
	
	private static final String	COLUMN_IS_GIFT			= "r_is_gift";			// 是否有赠品
	private static final String	COLUMN_GIFT_INFO		= "r_gift_info";		// 赠品说明
	private static final String	COLUMN_GIFT1			= "r_gift1";			// 赠品1
	private static final String	COLUMN_GIFT2			= "r_gift2";			// 赠品2
	private static final String	COLUMN_GIFT3			= "r_gift3";			// 赠品3
	private static final String	COLUMN_GIFT4			= "r_gift4";			// 赠品4
	private static final String	COLUMN_GIFT5			= "r_gift5";			// 赠品5
	private static final String	COLUMN_EDIT_TIME		= "r_edit_time";		// 修改时间
	
	public HistoryProductDao(Context context)
	{
		super(context);
		//CreatTable();
	}
	
	/*
	public void CreatTable()
	{
		Exec(createSQL);
	}
	*/
	
	// 插入数据
	public void insertData(RecommendPackage pack)
	{
		ProductInfo product = pack.getmProduct();
		String editTiem = Long.toString(System.currentTimeMillis());
		String sql = "INSERT INTO " + TABLE_NAME + " (" 
				+ COLUMN_ID + ","+ COLUMN_PRICE + "," + COLUMN_DES + ","+ COLUMN_AERA_LONG + "," + COLUMN_AERA_SHORT + ","
				+ COLUMN_AERA_CODE + "," + COLUMN_CONTRACT_ID + ","+ COLUMN_CONTRACT_CODE + "," 
				+ COLUMN_CONTRACT_NAME + "," + COLUMN_CONTRACT_DATE + "," + COLUMN_PRODUCT_ID + ","
				+ COLUMN_PRODUCT_CODE + "," + COLUMN_PRODUCT_NAME + "," + COLUMN_PRODUCT_PRICE + "," + COLUMN_PRODUCT_TYPE + "," 
				+ COLUMN_ACTIVITY_TYPE + "," + COLUMN_ACTIVITY_CONTENT + ","  + COLUMN_ACTIVITY_DETAILS + "," + COLUMN_MAKETTING_TAG + "," + COLUMN_PIC + "," 
				+ COLUMN_BIGPIC + "," + COLUMN_FLOW + ","+ COLUMN_CALL_TIME + ","+ COLUMN_MSG_COUNT+ "," + COLUMN_CALL_FREE + "," 
				+ COLUMN_CALL_FEE + "," + COLUMN_FLOW_FEE + ","+ COLUMN_VEDIO_CALL + ","+ COLUMN_OTHER+ "," + COLUMN_IS_GIFT + "," 
				+ COLUMN_GIFT_INFO + "," + COLUMN_GIFT1 + ","+ COLUMN_GIFT2 + ","+ COLUMN_GIFT3+ "," + COLUMN_GIFT4 + "," 
				+ COLUMN_GIFT5 + ","  + COLUMN_EDIT_TIME 
				+ ") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
		sql = String.format(sql, product.getId(), product.getmPrice(), product.getmDescribe(), product.getmAreaLong(), product.getmAreaShort(), product.getmAreaCode(),product.getmContractID(), 
				product.getmContractCode(), product.getmContractName(), product.getmContractDate(), product.getmProductID(), product.getmProductCode(), product.getmProductName(),
				product.getmProductPrice(), product.getmProductType(), product.getmActivityType(), product.getmActivityContent(), product.getmActivityDetails(), product.getmMarktingTag(),
				product.getmPicPath(), product.getmBigPic(), product.getmFlow1(), product.getmCallTime(), product.getmMsgCount(), product.getmFree(), product.getmCallFee(),
				product.getmFlow2(), product.getmVideoCall(), product.getmOther(), product.getmIsGift(), product.getmGiftInfo(), product.getmGift1(), product.getmGift2(),
				product.getmGift3(),product.getmGift4(),product.getmGift5(), editTiem);
		Exec(sql);
	}
	
	// 清楚全部记录
	public void clear()
	{
		Exec("delete from " + TABLE_NAME + " where 1=1");
	}
	
	// 删除某条记录
	public boolean deleteProduct(String packID)
	{
		String sql = "delete from " + TABLE_NAME + " where " + COLUMN_ID + " = '%s'";
		System.out.println("删除历史记录："+sql);
		sql = String.format(sql, packID);
		return executeUpdateSQL(sql);
	}
	
	// 获取记录
	public List<RecommendPackage> getData()
	{
		String sql  = "select * from " + TABLE_NAME + " order by " + COLUMN_EDIT_TIME + " DESC";
		RecommendPackage pack = null; 
		ProductInfo product = null;
		List<RecommendPackage> list = new ArrayList<RecommendPackage>();
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		if (mapList != null && mapList.size() > 0)
		{
			for(int i = 0; i < mapList.size(); i++)
			{
				pack = new RecommendPackage();
				product = pack.getmProduct();
				Map<String, String> map = mapList.get(i);
				pack.setmId(map.get(COLUMN_ID));
				// 推荐包信息
				product.setId(map.get(COLUMN_ID));
				product.setmPrice(map.get(COLUMN_PRICE));
				product.setmDescribe(map.get(COLUMN_DES));
				product.setmAreaLong(map.get(COLUMN_AERA_LONG));
				product.setmAreaShort(map.get(COLUMN_AERA_SHORT));
				product.setmAreaCode(map.get(COLUMN_AERA_CODE));
				
				product.setmContractID(map.get(COLUMN_CONTRACT_ID));
				product.setmContractCode(map.get(COLUMN_CONTRACT_CODE));
				product.setmContractName(map.get(COLUMN_CONTRACT_NAME));
				product.setmContractDate(map.get(COLUMN_CONTRACT_DATE));
				
				product.setmProductID(map.get(COLUMN_PRODUCT_ID));
				product.setmProductCode(map.get(COLUMN_PRODUCT_CODE));
				product.setmProductName(map.get(COLUMN_PRODUCT_NAME));
				product.setmProductPrice(map.get(COLUMN_PRODUCT_PRICE));
				product.setmProductType(map.get(COLUMN_PRODUCT_TYPE));
				
				product.setmActivityType(map.get(COLUMN_ACTIVITY_TYPE));
				product.setmActivityContent(map.get(COLUMN_ACTIVITY_CONTENT));
				product.setmActivityDetails(map.get(COLUMN_ACTIVITY_DETAILS));
				product.setmMarktingTag(map.get(COLUMN_MAKETTING_TAG));
				
				product.setmPicPath(map.get(COLUMN_PIC));
				product.setmBigPic(map.get(COLUMN_BIGPIC));
				
				product.setmFlow1(map.get(COLUMN_FLOW));
				product.setmCallTime(map.get(COLUMN_CALL_TIME));
				product.setmMsgCount(map.get(COLUMN_MSG_COUNT));
				product.setmFree(map.get(COLUMN_CALL_FREE));
				product.setmCallFee(map.get(COLUMN_CALL_FEE));
				product.setmFlow2(map.get(COLUMN_FLOW_FEE));
				product.setmVideoCall(map.get(COLUMN_VEDIO_CALL));
				product.setmOther(map.get(COLUMN_OTHER));
				
				product.setmIsGift(map.get(COLUMN_IS_GIFT));
				product.setmGiftInfo(map.get(COLUMN_GIFT_INFO));
				product.setmGift1(map.get(COLUMN_GIFT1));
				product.setmGift2(map.get(COLUMN_GIFT2));
				product.setmGift3(map.get(COLUMN_GIFT3));
				product.setmGift4(map.get(COLUMN_GIFT4));
				product.setmGift5(map.get(COLUMN_GIFT5));
				list.add(pack);
			}
		}
		return list;
	}
}
