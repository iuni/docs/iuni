package com.sunnada.baseframe.database.reflect;

public class FieldDataType
{
	public static final String	DATATYPE_INT		= "int";
	public static final String	DATATYPE_DOUBLE		= "double";
	public static final String	DATATYPE_FLOAT		= "float";
	public static final String	DATATYPE_LONG		= "long";
	public static final String	DATATYPE_DATE		= "date";
	public static final String	DATATYPE_STRING		= "string";
	public static final String	DATATYPE_BOOLEAN	= "boolean";
}
