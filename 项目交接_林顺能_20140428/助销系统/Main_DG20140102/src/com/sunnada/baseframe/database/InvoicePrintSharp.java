package com.sunnada.baseframe.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.sunnada.baseframe.activity.selfservice.BusiInvoicePrint;

public class InvoicePrintSharp 
{
	private final String       RECORD               = "record";
	private BusiInvoicePrint   mBusiInvoicePrint;
	private SharedPreferences  mSharp;
	private Editor             mEditor; 
	
	public InvoicePrintSharp(BusiInvoicePrint busiInvoicePrint)
	{
		this.mBusiInvoicePrint = busiInvoicePrint;
		mSharp = mBusiInvoicePrint.getPreferences(Context.MODE_PRIVATE);
		mEditor = mSharp.edit();
	}
	
	public void putRecord(String record)
	{
		mEditor.putString(RECORD, record);
		mEditor.commit();
	}
	
	public String getRecordValue()
	{
		return mSharp.getString(RECORD, "");
	}
	
	public void clearData()
	{
		if(mEditor != null)
		{
			mEditor.clear();
			mEditor.commit();
		}
	}
}
