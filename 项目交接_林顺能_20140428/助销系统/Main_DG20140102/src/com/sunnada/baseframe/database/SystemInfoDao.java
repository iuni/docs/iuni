package com.sunnada.baseframe.database;

import java.util.HashMap;


import android.content.Context;

public class SystemInfoDao extends SqLitBase
{
	private static final String TABLE_NAME 		= "SYSTEM_INFO";
	private static final String COLUMN_TYPE 	= "s_type";				// 系统参数类型
	private static final String COLUMN_NAME 	= "s_name";				// 名称
	private static final String COLUMN_VALUE 	= "s_value";			// 值
	
	private static final String createSQL = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" (" + 
																						"_id integer PRIMARY KEY autoincrement, " + //自增主键
																						COLUMN_TYPE +" TEXT," + //类型
																						COLUMN_NAME + " TEXT," +//名称
																						COLUMN_VALUE + " TEXT)";//值
	
	// 构造
	public SystemInfoDao(Context con)
	{
		super(con);
		//CreatTable();
	}
	
	// 每次开机执行一次建表语句
	public void CreatTable()
	{
		// 建立开机上报更新参数表
		Exec(createSQL);
	}
	
	// 查询方法
	// 根据类型查询，返回HashMap结果集
	public HashMap<String, String> getSystemInfo(String type) 
	{
		String sql  = String.format("select s_name,s_value from SYSTEM_INFO where s_type = '%s'", type);
		HashMap<String, String> result = SelectHash(sql);
		if (0 == result.size()) 
		{
			result = null;
		}
		return result;
	}
	
	/**
	 * 根据类型查询返回名称
	 * @param type
	 * @return
	 */
	public String getSystemInfoName(String type) 
	{
		String sReturn="";
		String sql  = String.format("select s_type,s_name from SYSTEM_INFO where s_type = '%s'", type);
		HashMap<String, String> result = SelectHash(sql);
		if (0 == result.size()) 
		{
			sReturn="";
		}
		else
		{
			sReturn=String.valueOf(result.get(type));
		}
		return sReturn;
	}
	
	/**
	 * 根据类型查询返回值
	 * @param type
	 * @return
	 */
	public String getSystemInfoValue(String type) 
	{
		String sReturn="";
		String sql  = String.format("select s_type,s_value from SYSTEM_INFO where s_type = '%s'", type);
		HashMap<String, String> result = SelectHash(sql);
		if (0 == result.size()) 
		{
			sReturn="";
		}
		else
		{
			sReturn=String.valueOf(result.get(type)); 
		}
		return sReturn;
	}
	
	/* 插入信息  */
	public void InsertSystemInfo(String type, String name) 
	{
		InsertSystemInfo(type,name,name);
	}
	
	public void InsertSystemInfo(String type, String name, String value) 
	{
		String sql = String.format("insert into SYSTEM_INFO (s_type,s_name,s_value) values ('%s','%s','%s')", type, name, value);
		Exec(sql);
	}
	
	/* 更新信息 */
	public void UpdateSystemInfo(String type, String name) 
	{
		UpdateSystemInfo(type,name,name);
	}
	
	public void UpdateSystemInfo(String type, String name, String value) 
	{
		String sql = String.format("update SYSTEM_INFO set s_name='%s',s_value='%s' where s_type='%s'",  name, value,type);
		Exec(sql);
	}

	/**
	 * 删除信息
	 * @param type
	 */
	public void DeleteParamsValue(String type)
	{
		String sql = String.format("delete * from "+TABLE_NAME+" where s_type = '%s'", type);
		Exec(sql);
	}
	
	/**
	 * 设置系统参数信息，如果不存在则新增，如果存在则修改
	 * @param type
	 * @param name
	 * @param value
	 */
	public void setSystemInfo(String type,String name, String value) 
	{
		
		HashMap<String, String> result = getSystemInfo(type);
		if (result == null) 
		{
			InsertSystemInfo(type,name,value);
		}
		else
		{
			UpdateSystemInfo(type,name,value);
		}        
	}
	
	public void setSystemInfo(String type,String name) 
	{		
		setSystemInfo(type, name, name);
	}
}
