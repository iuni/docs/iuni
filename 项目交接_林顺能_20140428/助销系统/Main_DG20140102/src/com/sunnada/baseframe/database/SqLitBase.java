package com.sunnada.baseframe.database;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.database.reflect.FieldDataType;
import com.sunnada.baseframe.database.reflect.FieldInfo;
import com.sunnada.baseframe.database.reflect.TableInfo;
import com.sunnada.baseframe.util.StringUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

public class SqLitBase extends DBadapter
{
	public SqLitBase(Context context) 
	{
		super(context);
	}
		
	// 执行建表或者更新语句或者删除表
	// strDB:执行SQL语句
	public boolean Exec(String strSql) 
	{
		try
		{
			open();
			mDB.execSQL(strSql);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
		return true;
	}
	
	// 执行查询语句
	// strDB:执行SQL语句返回LIST
	public List<Map<String, String>> SelectList(String strSql)
	{
		List<Map<String, String>> selectList = new ArrayList<Map<String, String>>();
		try
		{
			open();
			mCursor = null;
			// 清空查询列表
			mCursor = mDB.rawQuery(strSql, null);
			if(mCursor != null)
			{
				Log.e("", "<------查询数据库记录数------>" + mCursor.getCount());
				if (mCursor.getCount() == 0)
				{
					return null;
				}
				else
				{
					mCursor.moveToFirst();
					// 获取每条记录列名
					String[] colName = mCursor.getColumnNames();
					while (!mCursor.isAfterLast())
					{
						Map<String, String> map = new HashMap<String, String>();
						// 循环获取列名对应的数据
						for (int j = 0; j < colName.length; j++)
						{
							map.put(colName[j], mCursor.getString(mCursor.getColumnIndex(colName[j])));
							Log.e("", colName[j] + ": "+ mCursor.getString(mCursor.getColumnIndex(colName[j])));
						}
						selectList.add(map);
						mCursor.moveToNext();
					}
				}
				// 关闭游标
				mCursor.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		} 
		finally
		{
			close();
		}
		return selectList;
	}

	// 执行查询SQL语句返回hash
	public HashMap<String, String> SelectHash(String strSql)
	{
		HashMap<String, String> mapMsg = new HashMap<String, String>();
		try
		{
			open();
			mCursor = null;
			mCursor = mDB.rawQuery(strSql, null);
			if(mCursor != null)
			{
				int nCount = mCursor.getCount();
				Log.e("", "<------SelectHash查询数据库记录数------>" + nCount);
				if(nCount != 0)
				{
					mCursor.moveToFirst();
					// 获取每条记录列名
					String[] colName = mCursor.getColumnNames();
					while (!mCursor.isAfterLast())
					{
						mapMsg.put(mCursor.getString(mCursor.getColumnIndex(colName[0])), mCursor.getString(mCursor.getColumnIndex(colName[1])));
						mCursor.moveToNext();
					}
				}
				// 关闭游标
				mCursor.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		} 
		finally
		{
			close();
		}
		return mapMsg;
	}
	
	// 执行更新语句，包括增、删、改
	public boolean executeUpdateSQL(String sql)
	{
		return executeUpdateSQL(sql, null);
	}
	
	// 执行更新语句，包括增、删、改
	// sql		: 	sql语句
	// params	: 	参数
	public boolean executeUpdateSQL(String sql, String[] params)
	{
		boolean flag = false;
		Log.d("", "数据库执行更新语句:" + sql);
		try
		{
			open();
			if(params != null) 
			{
				mDB.execSQL(sql, params);
			}
			else
			{
				mDB.execSQL(sql);
			}
			flag = true;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
		return flag;
	}
	
	/**
	 * 批量执行更新语句，包括增、删、改，采用事务机制，提高速度
	 * @param sqls	sql数组
	 * @return	成功返回true失败返回false
	 * add by  qiufq
	 */
	public boolean executeUpdateSQLWithTransaction(String[] sqls)
	{
		boolean flag = false;
		try
		{
			open();
			if (sqls != null)
			{
				mDB.beginTransaction();
				for (String sql : sqls)
				{
					Log.d("", "数据库执行更新语句(事务):" + sql);
					mDB.execSQL(sql);
				}
				mDB.setTransactionSuccessful();
				mDB.endTransaction();
			}
			flag = true;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			close();
		}
		return flag;
	}
	
	/**
	 * 执行查询语句
	 * @param sql	sql语句
	 * @return	成功返回List<Map>对象，失败返回null
	 */
	public List<Map<String, String>> executeQuerySQL(String sql)
	{
		return executeQuerySQL(sql, null);
	}
	
	/**
	 * 执行查询语句
	 * @param sql	sql语句
	 * @param params	参数
	 * @return	成功返回List<Map>对象，失败返回null
	 */
	public List<Map<String, String>> executeQuerySQL(String sql, String[] params)
	{
		List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		try
		{
			open();
			mCursor = null;
			Log.d("", "数据库执行查询语句: " + sql);
			mCursor = mDB.rawQuery(sql, params);
			String[] columNames = mCursor.getColumnNames();
			
			while (mCursor.moveToNext())
			{
				Map<String, String> map = new HashMap<String, String>();

				for (String columName : columNames)
				{
					map.put(columName, mCursor.getString(mCursor.getColumnIndex(columName)));
				}
				data.add(map);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			close();
		}
		return data;
	}
	
	/**
	 * 
	 * 执行分页查询语句
	 * @param sql	sql语句
	 * @param start 开始位置
	 * @param count 查询条数
	 * @return	成功返回List<Map>对象，失败返回null
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql, int start, int count)
	{
		return executeQuerySQL(sql, null, start, count);
	}

	/**
	 * 
	 * 执行分页查询语句
	 * @param sql	sql语句
	 * @param params	参数
	 * @param start 开始位置
	 * @param count 查询条数
	 * @return	成功返回List<Map>对象，失败返回null
	 * @return
	 */
	public List<Map<String, String>> executeQuerySQL(String sql, String[] params, int start, int count)
	{
		List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		try
		{
			open();
			mCursor = null;
			sql += " limit " + start + "," + count;
			Log.d("", "数据库执行查询语句: " + sql);
			mCursor = mDB.rawQuery(sql, params);
			String[] columNames = mCursor.getColumnNames();

			while (mCursor.moveToNext())
			{
				Map<String, String> map = new HashMap<String, String>();
				for (String columName:columNames)
				{
					map.put(columName, mCursor.getString(mCursor.getColumnIndex(columName)));
				}
				data.add(map);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			close();
		}
		return data;
	}
	
	// 增加方法   20131108 新增
	// t 任意对象，内部成员变量应用注解标志
	public <T> long insert(T t)
	{
		long iResult = 0;
		try
		{
			open();
			String tableName = getTableName(t);
			ContentValues cv = new ContentValues();
			Field[] fields = t.getClass().getDeclaredFields();

			for (Field f : fields)
			{
				FieldInfo fieldInfo = f.getAnnotation(FieldInfo.class);
				if(fieldInfo == null)
				{
					continue;
				}
				String fieldName = fieldInfo.fieldName();
				String fieldType = fieldInfo.fieldType();
				Object objFieldValue = null;
				try
				{
					f.setAccessible(true);
					objFieldValue = f.get(t);
					f.setAccessible(false);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				setContentValues(cv, fieldName, fieldType, objFieldValue);
			}
			iResult = mDB.insert(tableName, null, cv);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			close();
		}
		return iResult;
	}

	// 删除方法
	// t 内部成员变量应用注解标志
	public <T> int delete(T t)
	{
		int iResult = 0;
		try
		{
			open();
			String tableName = getTableName(t);
			Field[] fields = t.getClass().getDeclaredFields();
			StringBuffer sbDeleteCondition = null;
			for (Field f : fields)
			{
				FieldInfo fieldInfo = f.getAnnotation(FieldInfo.class);
				if(fieldInfo == null)
				{
					continue;
				}
				if (!fieldInfo.isPrimaryKey())
				{
					continue;
				}
				String fieldName = fieldInfo.fieldName();
				String fieldType = fieldInfo.fieldType();
				Object objFieldValue = null;
				try
				{
					f.setAccessible(true);
					objFieldValue = f.get(t);
					f.setAccessible(false);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				sbDeleteCondition = getCondition(fieldName, fieldType, objFieldValue);
			}
			iResult = mDB.delete(tableName, sbDeleteCondition.toString(), null);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}
		finally
		{
			close();
		}
		return iResult;
	}
	
	// 精确查询方法	20131108 新增
	// from + count 分页查询，从from查询count条数据
	// orderby 按什么排序
	// order 升序或降序
	public <T> Cursor accurateSearch(T t, int from, int count, String[] orderBy, String[] order)
	{
		try
		{
			open();
			String tableName = getTableName(t);
			StringBuffer strCondtion = new StringBuffer();// where...
			Field[] fields = t.getClass().getDeclaredFields();
			for (Field field : fields)
			{
				FieldInfo fieldInfo = field.getAnnotation(FieldInfo.class);
				if(fieldInfo == null)
				{
					continue;
				}
				String fieldName = fieldInfo.fieldName();
				Object objFieldValue = null;
				try
				{
					field.setAccessible(true);
					objFieldValue = field.get(t);
					field.setAccessible(false);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				if (!valueIsNull(objFieldValue, fieldInfo.fieldType()))
				{
					if (strCondtion.length() == 0)
					{
						strCondtion.append(fieldName + " = " + "'" + objFieldValue + "'");
					}
					else
					{
						strCondtion.append(" and " + fieldName + " = " + "'" + objFieldValue + "'");
					}
				}
			}
			StringBuffer sql = new StringBuffer();
			sql.append("select * from " + tableName);
			
			// 条件
			if(!StringUtil.isEmptyOrNull(strCondtion.toString()))
			{
				sql.append(" where " + strCondtion.toString());
			}
			
			// 排序
			if (orderBy != null && orderBy.length > 0)
			{
				StringBuffer orderCondition = new StringBuffer();
				orderCondition.append(orderBy[0] + " " + order[0]);
				for (int i = 1; i < orderBy.length; i++)
				{
					orderCondition.append("," + orderBy[i] + " " + order[i]);
				}
				sql.append(" order by " + orderCondition.toString());
			}
			
			// 分页
			if (count > 0)
			{
				sql.append(" limit " + from + "," + count);
			}
			System.out.println("SQL: " + sql.toString());
			mCursor = mDB.rawQuery(sql.toString(), null);
			System.out.println("查询条数 ： " + mCursor.getCount());
			/*
			if (cursor.moveToFirst())
			{
				do
				{
					String[] name = cursor.getColumnNames();
					for (int i = 0; i < name.length; i++)
					{
						System.out.println(name[i] + " : " + cursor.getString(i));
					}
				} while (cursor.moveToNext());
			}
			*/
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			close();
		}
		return mCursor;
	}
	

	// 模糊查询方法 20131108 新增
	// from + count 分页查询，从from查询count条数据
	// orderby 按什么排序
	// order 升序或降序
	public <T> Cursor mistySearch(T t, int from, int count, String[] orderBy, String[] order)
	{
		try
		{
			open();
			String tableName = getTableName(t);
			StringBuffer strCondtion = new StringBuffer();// where...
			Field[] fields = t.getClass().getDeclaredFields();
			for (Field field : fields)
			{
				FieldInfo fieldInfo = field.getAnnotation(FieldInfo.class);
				if(fieldInfo == null)
				{
					continue;
				}
				String fieldName = fieldInfo.fieldName();
				String fieldType = fieldInfo.fieldType();
				Object objFieldValue = null;
				try
				{
					field.setAccessible(true);
					objFieldValue = field.get(t);
					field.setAccessible(false);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				if (!valueIsNull(objFieldValue, fieldType))
				{
					getMistySelectCondition(fieldName, objFieldValue, strCondtion, fieldType);
				}
			}
			StringBuffer sql = new StringBuffer();
			sql.append("select * from " + tableName);
			
			// 条件
			if(!StringUtil.isEmptyOrNull(strCondtion.toString()))
			{
				sql.append(" where " + strCondtion.toString());
			}
			
			// 排序
			if (orderBy != null && orderBy.length > 0)
			{
				StringBuffer orderCondition = new StringBuffer();
				orderCondition.append(orderBy[0] + " " + order[0]);
				for (int i = 1; i < orderBy.length; i++)
				{
					orderCondition.append("," + orderBy[i] + " " + order[i]);
				}
				sql.append(" order by " + orderCondition.toString());
			}
			
			// 分页
			if (count > 0)
			{
				sql.append(" limit " + from + "," + count);
			}
			System.out.println("SQL: " + sql.toString());
			mCursor = mDB.rawQuery(sql.toString(), null);
			System.out.println("查询条数 ： " + mCursor.getCount());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			close();
		}
		return mCursor;
	}
	
	// 根据数据类型拼接语句
	private void getMistySelectCondition(String fieldName, Object objFieldValue, StringBuffer strCondtion,
			String fieldType)
	{
		if (fieldType.equals(FieldDataType.DATATYPE_STRING) && ((String) objFieldValue).trim().length() > 0)
		{
			if (strCondtion.length() == 0)
				strCondtion.append(fieldName + " LIKE '%" + (String) objFieldValue + "%'");
			else
				strCondtion.append(" and " + fieldName + " LIKE '%" + (String) objFieldValue + "%'");
		}
		else
		{
			if (strCondtion.length() == 0)
				strCondtion.append(fieldName + " = " + "'" + objFieldValue + "'");
			else
				strCondtion.append(" and " + fieldName + " = " + "'" + objFieldValue + "'");
		}

	}


	// 获取表名
	private <T> String getTableName(T t)
	{
		TableInfo tableInfo = t.getClass().getAnnotation(TableInfo.class);
		String tableName = tableInfo.tableName();
		return tableName;
	}
	
	// 设置字段值
	private void setContentValues(ContentValues cv, String fieldName, String fieldType, Object objFieldValue)
	{
		if (!valueIsNull(objFieldValue, fieldType))
		{
			if (fieldType.equals(FieldDataType.DATATYPE_BOOLEAN))
			{
				cv.put(fieldName, (Boolean) objFieldValue);
			}
			else if (fieldType.equals(FieldDataType.DATATYPE_DOUBLE))
			{
				cv.put(fieldName, (Double) objFieldValue);
			}
			else if (fieldType.equals(FieldDataType.DATATYPE_FLOAT))
			{
				cv.put(fieldName, (Float) objFieldValue);
			}
			else if (fieldType.equals(FieldDataType.DATATYPE_INT))
			{
				cv.put(fieldName, (Integer) objFieldValue);
			}
			else if (fieldType.equals(FieldDataType.DATATYPE_STRING))
			{
				cv.put(fieldName, (String) objFieldValue);
			}
			else if (fieldType.equals(FieldDataType.DATATYPE_LONG))
			{
				cv.put(fieldName, (Long) objFieldValue);
			}
		}
	}
	
	// 判断值是否为空
	private boolean valueIsNull(Object value, String type)
	{
		if (value == null)
			return true;
		if (type.equals(FieldDataType.DATATYPE_STRING) && ((String) value).trim().length() <= 0)
		{
			return true;
		}
		else if (type.equals(FieldDataType.DATATYPE_DOUBLE) && ((Double) value) < 0)
		{
			return true;
		}
		else if (type.equals(FieldDataType.DATATYPE_FLOAT) && (Float) value < 0)
		{
			return true;
		}
		else if (type.equals(FieldDataType.DATATYPE_INT) && (Integer) value < 0)
		{
			return true;
		}
		return false;
	}
	
	// 根据数据类型拼接语句
	private StringBuffer getCondition(String strDBFieldName, String strDBType, Object objValue)
	{
		StringBuffer sbCondition = new StringBuffer();
		// 字符串类型
		if (strDBType.equals(FieldDataType.DATATYPE_STRING))
		{
			sbCondition.append(strDBFieldName).append("='").append((String) objValue).append("'");
		}
		// 双精度浮点型
		else if (strDBType.equals(FieldDataType.DATATYPE_DOUBLE))
		{
			sbCondition.append(strDBFieldName).append("=").append((Double) objValue);
		}
		// 单精度浮点型
		else if (strDBType.equals(FieldDataType.DATATYPE_FLOAT))
		{
			sbCondition.append(strDBFieldName).append("=").append((Float) objValue);
		}
		// 整型
		else if (strDBType.equals(FieldDataType.DATATYPE_INT))
		{
			sbCondition.append(strDBFieldName).append("=").append((Integer) objValue);
		}
		// 长整型
		else if (strDBType.equals(FieldDataType.DATATYPE_LONG))
		{
			sbCondition.append(strDBFieldName).append("=").append((Long) objValue);
		}
		// 布尔类型
		else if (strDBType.equals(FieldDataType.DATATYPE_BOOLEAN))
		{
			sbCondition.append(strDBFieldName).append("=").append((Boolean) objValue);
		}
		return sbCondition;
	}

}
