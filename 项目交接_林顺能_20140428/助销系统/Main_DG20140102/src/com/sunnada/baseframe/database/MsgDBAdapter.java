package com.sunnada.baseframe.database;

import com.sunnada.baseframe.bean.MsgBean;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class MsgDBAdapter extends DBadapter 
{
	private static final String DB_TABLE 		= "MSG";
	private static final String KEY_ID 			= "_id";
	private static final String KEY_TITLE 		= "title";
	private static final String KEY_TEXT 		= "text";

	public MsgDBAdapter(Context context) 
	{
		super(context);
	}

	public long insertData(String title, String text) 
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TITLE, 	title);
		initialValues.put(KEY_TEXT, 	text);

		return mDB.insert(DB_TABLE, KEY_ID, initialValues);
	}
	
	public Cursor fetchAllData() 
	{
		Cursor mCursor = mDB.query(DB_TABLE, new String[] { KEY_ID, KEY_TITLE, KEY_TEXT}, null, null, null, null, null);
		if (mCursor != null) 
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	// 取指定数据的行号
	public String fetchDataByTag(String title) 
	{
		Cursor mCursor = mDB.query(DB_TABLE, new String[] { KEY_TEXT }, KEY_TITLE + " = ?" , new String[] { title }, null, null, null);
		if (mCursor != null && mCursor.getCount() != 0) 
		{
			mCursor.moveToFirst();
			int index = mCursor.getColumnIndex(KEY_TEXT);
			Log.e("", "index = " + index);
			String tempstr = mCursor.getString(index);
			mCursor.close();
			return tempstr;
		}
		return null;
	}
	
	// 删除指定行的数据
	public boolean deleteData(String rowId) 
	{
		return mDB.delete(DB_TABLE, KEY_ID + "=" + rowId, null) > 0;
	}
	
	// 根据标题删除记录
	public boolean deleteDataByTag(String title)
	{
		int iResult = mDB.delete(DB_TABLE, KEY_TITLE + " = ?", new String[]{ title });
		return iResult <= 0? false:true;
	}

	// 获取所有数据
	public static MsgBean[] getAllData(Context context) 
	{
		// 读取偏好设置数据库
		MsgDBAdapter s_MyDataBaseAdapter = new MsgDBAdapter(context);
		MsgBean[] msgBean = null;
		int index = 0;

		try {
			s_MyDataBaseAdapter.open();
			Cursor cur = s_MyDataBaseAdapter.fetchAllData();
			if (null != cur && cur.getCount() != 0) 
			{
				msgBean = new MsgBean[cur.getCount()];
				Log.v("", "找到"+cur.getCount()+"条公告!");
				while (!cur.isAfterLast())
				{
					msgBean[index] = new MsgBean();
					msgBean[index].setID(cur.getString(cur.getColumnIndex(KEY_ID)));
					msgBean[index].setTitle(cur.getString(cur.getColumnIndex(KEY_TITLE)));
					msgBean[index].setText(cur.getString(cur.getColumnIndex(KEY_TEXT)));
					Log.e("消息数据", msgBean[index].getID() + "|" + msgBean[index].getTitle() + "|" 
							+ msgBean[index].getText());
					cur.moveToNext();
					index++;
				}
			}
			if(cur != null) 
			{
				cur.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(s_MyDataBaseAdapter != null) 
			{
				s_MyDataBaseAdapter.close();
			}
		}
		return msgBean;
	}
}
