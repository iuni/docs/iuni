package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;


import android.content.Context;

public class PackageInfoDao extends SqLitBase
{
	public static final String	TABLE_NAME			= "Package_Info";
	public static final String	COLUMN_PLAN			= "p_plan";
	public static final String	COLUMN_FEE			= "p_fee";
	public static final String	COLUMN_CALL_TIME	= "p_call_time";
	public static final String	COLUMN_FLOW1		= "p_flow1";
	public static final String	COLUMN_MSG			= "p_msg";
	public static final String	COLUMN_FREE_CALL	= "p_free_call";
	public static final String	COLUMN_CALL_FEE		= "p_call_fee";
	public static final String	COLUMN_FLOW2		= "p_flow2";
	public static final String	COLUMN_VIDEO_CALL	= "p_video_call";
	public static final String	COLUMN_OTHER		= "p_other";

	public PackageInfoDao(Context con)
	{
		super(con);
	}

	/*
	// 每次开机执行一次建表语句
	public void CreatTable()
	{
		// 建立开机上报更新参数表
		Exec(createSQL);

		if (readPackageInfo("A", 46) == null)
		{
			initData();
		}
	}
	*/

	public void clear()
	{
		Exec("delete from " + TABLE_NAME + " where 1=1");
	}

	public void initData()
	{
		insertData("A", "46", "50", "150MB", "0", "国内", "0.25", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "66", "50", "300MB", "240", "国内", "0.20", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "96", "240", "300MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "126", "320", "400MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "156", "420", "500MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "186", "510", "650MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "226", "700", "750MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "286", "900", "950MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "386", "1250", "1.3GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "586", "1950", "2GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("A", "886", "3000", "3GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");

		insertData("B", "46", "120", "40MB", "0", "国内", "0.25", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("B", "66", "200", "60MB", "0", "国内", "0.20", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("B", "96", "450", "80MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("B", "126", "680", "100MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("B", "156", "920", "120MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("B", "186", "1180", "150MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");

		insertData("C", "46", "260", "40MB", "0", "国内", "0.20", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("C", "66", "380", "60MB", "0", "国内", "0.20", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("C", "96", "550", "80MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");

		insertData("其他", "66", "160", "220MB", "50", "国内", "0.20", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "96", "240", "340MB", "80", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "126", "320", "450MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "156", "420", "570MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "186", "510", "720MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "226", "700", "850MB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "286", "900", "1.1GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "386", "1250", "1.6GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "586", "1950", "2.5GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");
		insertData("其他", "886", "3000", "4GB", "0", "国内", "0.15", "0.0003元/KB", "0.60元/分钟", "执行标准");

	}

	public void insertData(String plan, String fee, String callTime, String flow1, String msg, String freeCall,
			String callFee, String flow2, String videoCall, String other)
	{
		String sql = "INSERT INTO " + TABLE_NAME + " (" + COLUMN_PLAN + "," + COLUMN_FEE + "," + COLUMN_CALL_TIME + ","
				+ COLUMN_FLOW1 + "," + COLUMN_MSG + "," + COLUMN_FREE_CALL + "," + COLUMN_CALL_FEE + "," + COLUMN_FLOW2
				+ "," + COLUMN_VIDEO_CALL + "," + COLUMN_OTHER
				+ ") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
		sql = String.format(sql, plan, fee, callTime, flow1, msg, freeCall, callFee, flow2, videoCall, other);
		Exec(sql);
	}

	public Map<String, String> readPackageInfo(String plan, int fee)
	{
		String sql = "select " + COLUMN_PLAN + "," + COLUMN_FEE + "," + COLUMN_CALL_TIME + "," + COLUMN_FLOW1 + ","
				+ COLUMN_FLOW1 + "," + COLUMN_MSG + "," + COLUMN_FREE_CALL + "," + COLUMN_CALL_FEE + "," + COLUMN_FLOW2
				+ "," + COLUMN_VIDEO_CALL + "," + COLUMN_OTHER + " from " + TABLE_NAME + " where " + COLUMN_PLAN
				+ " = '%s' and " + COLUMN_FEE + " = '%s'";
		sql = String.format(sql, plan, fee);

		List<Map<String, String>> mapList = executeQuerySQL(sql);
		if (mapList == null || mapList.size() == 0)
		{
			return null;
		}
		else
		{
			return mapList.get(0);
		}
	}
}
