package com.sunnada.baseframe.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import sunnada.jni.Commcenter;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.thread.Thread;
import com.sunnada.baseframe.util.CollectionUtil;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;

/**
 * File: NetworkService.java
 * Description: 
 * @author 丘富铨
 * @date 2013-3-7
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class SocketService extends Service implements ISocketService
{
	private static final int	CONNECT_MAX_TRY_COUNT	= 3;					// 最大尝试连接数
	//private static final int	READ_TIMEOUT			= 30*1000;				// 数据读取超时时间
	
	private String				mServerIp1				= null;
	private int					mServerPort1			= -1;
	private String				mServerIp2				= null;
	private int					mServerPort2			= -1;
	private boolean 			mIsFirstIp				= true;
	
	private Socket				mClientSocket			= null;
	private InputStream			mSocketInputStream		= null;
	private OutputStream		mSocketOutputStream		= null;
	
	private RecevieThread		mReceiveThread			= null;							// 数据接收线程
	private DataProcessThread	mDataProcThread			= null;							// 数据处理线程
	
	private String				mLastKnownError			= "Unknow Error";
	private long				mLastSendTime			= System.currentTimeMillis();	// 获取最后一次发送数据的时间戳
	private int					mErrCode				= 0;							// 错误码
	
	public class SocketBinder extends Binder
	{
		public SocketService getService() 
		{
			return SocketService.this;
		}
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.log("SocketService启动");
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.log("SocketService关闭");
	}
	
	@Override
	public IBinder onBind(Intent intent) 
	{
		return new SocketBinder();
	}
	
	// 获取当前网络是否连接
	public boolean isNetworkOk()
	{
		try
		{
			ConnectivityManager connectivity = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null)
			{
				// 获取网络连接管理的对象
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected())
				{
					return true;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	// 连接服务器
	@Override
	public boolean connect(boolean isFirstIp) 
	{
		if (isFirstIp) 
		{
			return tryConnect(mServerIp1, mServerPort1);
		}
		else
		{
			return tryConnect(mServerIp2, mServerPort2);
		}
	}
	
	// 连接服务器
	private synchronized boolean tryConnect(String ip, int port) 
	{
		if (StringUtil.isEmptyOrNull(ip)) 
		{
			setLastknownError(getString(R.string.connect_server_error) + "[" + getString(R.string.ip_empty) + "]");
			Log.log("IP地址为空, 无法连接服务器");
			return false;
		}
		// 断开与服务器的连接
		disConnect();
		
		try
		{
			Log.e("准备创建Socket连接: ", ip + ":" + port);
			mClientSocket = new Socket();
			mClientSocket.connect(new InetSocketAddress(ip, port), 10*1000);
			Log.log("连接服务器成功！");
			// 
			//mSocket.setSoTimeout(READ_TIMEOUT);
			mSocketInputStream = mClientSocket.getInputStream();
			mSocketOutputStream = mClientSocket.getOutputStream();
			// 数据接收线程
			mReceiveThread = new RecevieThread();
			mReceiveThread.start();
			// 数据处理线程
			mDataProcThread = new DataProcessThread();
			mDataProcThread.start();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.log("连接服务器错误！");
			setLastknownError(getString(R.string.connect_server_error));
			mClientSocket = null;
			return false;
		}
	}
	
	// 从接收包队列中删除主命令为id的包
	@Override
	public void removeSameIdPackages(String id)
	{
		if (mDataProcThread != null)
		{
			mDataProcThread.removeSameIdPackages(id);
		}
	}
	
	// 断开与服务器的连接
	@Override
	public void disConnect() 
	{
		// 数据接收线程
		if (mReceiveThread != null && mReceiveThread.isRunning()) 
		{
			Log.d("", "关闭数据接收线程...");
			mReceiveThread.setRunning(false);
			mReceiveThread = null;
		}
		// 数据处理线程
		if (mDataProcThread != null && mDataProcThread.isRunning()) 
		{
			Log.d("", "关闭数据处理线程...");
			mDataProcThread.setRunning(false);
			mDataProcThread = null;
		}
		// 如果套接字没有关闭
		if (mClientSocket != null) 
		{
			try
			{
				Log.log("关闭socket连接...");
				mSocketInputStream.close();
				mSocketOutputStream.close();
				mClientSocket.close();
				mClientSocket = null;
				SystemClock.sleep(1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	// 判断是否已连接
	@Override
	public boolean isConnect() 
	{
		if(mClientSocket == null) 
		{
			return false;
		}
		return mClientSocket.isConnected();
	}

	// 尝试重连
	private boolean tryReconnect() 
	{
		Log.d("", "socket正在进行重连...");
		if(mIsFirstIp == true)
		{
			// 连接备用服务器
			mIsFirstIp = false;
			Log.d("", "准备连接备用服务器...");
		}
		else
		{
			// 连接主服务器
			mIsFirstIp = true;
			Log.d("", "准备连接主服务器...");
		}
		SystemClock.sleep(500);
		return connect(mIsFirstIp);
	}

	@Override
	public void setIP1Adress(String ip) 
	{
		this.mServerIp1 = ip;
	}

	@Override
	public void setPort1(int port) 
	{
		this.mServerPort1 = port;
	}
	
	@Override
	public void setIP2Adress(String ip) 
	{
		this.mServerIp2 = ip;
	}

	@Override
	public void setPort2(int port) 
	{
		this.mServerPort2 = port;
	}
	
	// 获取当前连接的IP
	public String getIp()
	{
		if(mIsFirstIp) 
		{
			return mServerIp1;
		}
		return mServerIp2;
	}
	
	// 获取当前连接的端口
	public int getPort() 
	{
		if(mIsFirstIp) 
		{
			return mServerPort1;
		}
		return mServerPort2;
	}
	
	@Override
	public synchronized int send(byte[] buf, int offset, int len) 
	{
		int flag = ReturnEnum.SUCCESS;
		
		if (!isConnect()) 
		{
			Log.d("", "网络连接已断开, 开始尝试恢复连接");
			// 重新连接
			for(int i=0; i<CONNECT_MAX_TRY_COUNT; i++) 
			{
				if(tryReconnect()) 
				{
					break;
				}
			}
			// 连接失败
			if (!isConnect()) 
			{
				Log.e("", "尝试恢复连接失败！");
				setLastknownError(getString(R.string.server_disconnect));
				flag = ReturnEnum.FAIL;
				return flag;
			}
			Log.d("", "尝试恢复连接成功！");
		}
		
		try
		{
			Log.log("发送密文", buf);
			// 发送数据
			mSocketOutputStream.write(buf, offset, len);
			mSocketOutputStream.flush();
			mLastSendTime = System.currentTimeMillis();
		}
		catch (SocketTimeoutException e)
		{
			Log.log(e);
			flag = ReturnEnum.SEND_TIMEOUT;
		}
		catch (IOException e)
		{
			Log.log(e);
			flag = ReturnEnum.FAIL;
		}
		return flag;
	}
	
	// 解析0142命令单元，判断终端是否可用
	private boolean parseUnit0142(Package pdu) 
	{
		if(!pdu.containsKey("0142")) 
		{
			return true;
		}
		
		byte[] unit = pdu.get("0142");
		boolean ret = false;
		switch (unit[0]) 
		{
			case 0x01:
				Log.log("设备启用");
				ret = true;
				break;
				
			case 0x02:
				Log.log("设备被停用，禁止交易");
				setLastknownError("设备被停用，禁止交易");
				break;
				
			case 0x03:
				Log.log("设备被注销，禁止交易");
				setLastknownError("设备被注销，禁止交易");
				break;
				
			case 0x04:
				Log.log("密码、密钥连续输入异常被锁定，禁止交易");
				setLastknownError("密码、密钥连续输入异常被锁定，禁止交易");
				break;
				
			default:
				Log.log("未知状态，禁止交易");
				setLastknownError("未知状态，禁止交易");
				break;
		}
		
		if (unit.length > 1)
		{
			int len = unit[1];
			if (len > 0)
			{
				// 如果平台有描述，则取平台推送的描述
				byte[] info = new byte[len];
				System.arraycopy(unit, 2, info, 0, len);
				try
				{
					String sErrMsg = new String(info, "GBK");
					Log.log("终端禁用原因(平台描述):" + sErrMsg);
					setLastknownError(sErrMsg);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return ret;
	}
	
	// 接收包
	@Override
	public int receive(String id, List<Package> packages, int timeout) 
	{
		int flag 	  = ReturnEnum.RECIEVE_TIMEOUT;
		long stopTime = System.currentTimeMillis() + timeout;
		
		Log.log("进入接收" + id + "命令循环");
		while (System.currentTimeMillis() < stopTime)
		{
			Queue<Package> packageQueue = null;
			// 如果当前id不是0011的话，则要先收取下0011单元
			if (!"0011".equals(id))
			{
				// 如果未找到该主命令单元的话，则进行获取0011命令用以检测终端是否禁用
				packageQueue = mDataProcThread.getPackageById("0011");
				// 如果包含0011命令单元，则解析
				if (!CollectionUtil.isEmptyOrNull(packageQueue))
				{
					Log.log("检测到返回的数据中包含0011，开始进行0011包解析是否终端停用");
					// 如果终端不可用，则返回false
					if (!parseUnit0142(packageQueue.poll()))
					{
						return ReturnEnum.FAIL;
					}
				}
			}
			
			packageQueue = mDataProcThread.getPackageById(id);
			if (!CollectionUtil.isEmptyOrNull(packageQueue))
			{
				while (packageQueue.size() > 0)
				{
					Package pdata = packageQueue.poll();
					if (CollectionUtil.isEmptyOrNull(pdata.getUnits()))
					{
						Log.log("主命令" + id + "返回的数据包中没有命令单元, 丢弃该包");
						continue;
					}
					
					if (pdata.containsKey("0201") == false)
					{
						setLastknownError(getString(R.string.lost_unit));
						flag = ReturnEnum.FAIL;
					}
					else
					{
						if (check0201(pdata) == false)
						{
							Log.log("0201命令单元校验失败，丢弃该包");
							//setLastknownError("0201命令单元校验失败，丢弃该包");
							flag = ReturnEnum.FAIL;
						}
						else
						{
							packages.add(pdata);
						}
					}
				}
				
				// 只有在至少接收到1个包的情况下才成功
				if (packages.size() > 0)
				{
					flag = ReturnEnum.SUCCESS;
				}
				break;
			}
			
			try
			{
				Thread.sleep(500);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		Log.log("接收" + id + "命令循环结束");
		
		if (flag == ReturnEnum.RECIEVE_TIMEOUT)
		{
			Log.log("接收" + id + "命令超时");
			setLastknownError(getString(R.string.recive_data_timeout));
		}
		return flag;
	}
	
	// 检查0201
	private boolean check0201(Package pdata) 
	{
		boolean flag = false;

		byte[] temp = pdata.get("0201");
		if ((temp[0] != 0) || (temp[1] != 0) || (temp[2] != 0) || (temp[3] != 0))
		{
			mErrCode = Commen.bytes2int(temp);
			Log.log("错误编码:" + mErrCode);
			if (temp.length > 4)
			{
				int ilen = temp[4];
				if (ilen < 0)
				{
					Log.log("数据组包错误");
					return false;
				}

				byte[] qqw = new byte[ilen];
				System.arraycopy(temp, 5, qqw, 0, ilen);
				try
				{
					String errmsg = new String(qqw, "GBK");
					setLastknownError(errmsg.replace("\n", ""));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				flag = false;
			}
			else if ((temp[0] == (byte) 0xB3) && (temp[1] == (byte) 0x22) && (temp[2] == (byte) 0x00) && (temp[3] == (byte) 0x00))
			{
				setLastknownError(getString(R.string.empty_card_not_exsist));
				flag = false;
			}
			else
			{
				setLastknownError(getString(R.string.deal_fail));
				flag = false;
			}
		}
		else
		{
			flag = true;
		}
		return flag;
	}
	
	// socket接收线程
	private class RecevieThread extends Thread
	{
		private final String 	TAG			= "socket接收";
		private boolean			mIsRunning	= true;
		
		@Override
		public void runs() 
		{
			byte[] szRecvBuf = new byte[2048];
			int nRecvLen = 0;
			
			Log.d(TAG, "socket数据接收线程启动");
			while (mIsRunning) 
			{
				try 
				{
					nRecvLen = mSocketInputStream.read(szRecvBuf);
					if (nRecvLen > 0) 
					{
						Log.d(TAG, "接收到" + nRecvLen + "个字节");
						mDataProcThread.addBytes(szRecvBuf, 0, nRecvLen);
					}
				}
				catch (SocketTimeoutException e) 
				{
					// 接收超时，无需理会，因此不打印出异常信息
					//e.printStackTrace();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					Log.e(TAG, "socket数据接收线程出现异常！");
					mIsRunning = false;
					// 断开socket连接
					disConnect();
					break;
				}
			}
			Log.d("", "socket数据接收线程退出...");
		}
		
		public boolean isRunning() 
		{
			return mIsRunning;
		}
		
		public void setRunning(boolean running) 
		{
			this.mIsRunning = running;
		}
	}
	
	// 数据处理线程
	private class DataProcessThread extends com.sunnada.baseframe.thread.Thread
	{
		private final String				TAG			= "socket处理";
		private final byte					START_TAG	= 0x7E;
		private final byte					END_TAG		= 0x7F;

		private Queue<Byte>					dataBytes	= new ConcurrentLinkedQueue<Byte>();				// 线程安全的数据队列
		private boolean						mIsRunning	= true;												// 控制是否结束线程
		
		private Map<String, Queue<Package>>	packageMap	= new ConcurrentHashMap<String, Queue<Package>>();	// 线程安全，数据包Map，存放所有的数据包，子命令主命令--->包队列
		
		@Override
		public void runs()
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Log.d(TAG, "socket数据处理线程启动...");
			
			while (mIsRunning) 
			{
				while (dataBytes.size() > 0) 
				{
					// 获取并移除此队列的头，如果此队列为空，则返回 null
					byte b = dataBytes.poll();
					switch (b)
					{
						// 数据包开始标识
						case START_TAG:
							baos.reset();
							baos.write(b);
							break;
							
						// 数据包结束标识
						case END_TAG:
							baos.write(b);
							
							byte[] buf = baos.toByteArray();
							Log.log("接收密文", buf);
							// 解密
							buf = descrypt(buf);
							
							// 数据接收完成打包
							Package package1 = Package.parse(buf);
							if (package1 == null)
							{
								Log.log("数据组包错误，丢弃该包");
								break;
							}
							Queue<Package> packagesQueue = packageMap.get(package1.getId() + package1.getSubId());
							if (packagesQueue == null)
							{
								packagesQueue = new ConcurrentLinkedQueue<Package>();
							}
							Log.log("往命令单元为" + package1.getId() + package1.getSubId() + "添加数据包");
							packagesQueue.offer(package1);
							packageMap.put(package1.getId() + package1.getSubId(), packagesQueue);
							break;
							
						default:
							baos.write(b);
							break;
					}
				}
				// 延迟500ms
				SystemClock.sleep(500);
			}
			Log.d(TAG, "socket数据处理线程退出...");
		}

		// 通过命令编码获取数据包
		public Queue<Package> getPackageById(String id)
		{
			Queue<Package> packages = packageMap.get(id);
			if (packages != null)
			{
				// 清除数据包
				packageMap.remove(id);
			}
			return packages;
		}
		
		// 删除主命令为id的包
		public void removeSameIdPackages(String id)
		{
			if (packageMap != null)
			{
				packageMap.remove(id);
			}
		}
		
		// 添加字节数组
		private void addBytes(byte[] buf, int offset, int len)
		{
			for (int i = offset; i < offset + len; i++)
			{
				dataBytes.offer(buf[i]);
			}
		}
		
		// 数据解密
		private byte[] descrypt(byte[] data)
		{
			byte dest[] = Commcenter.ReverseTransferredMeaning(data);
			byte temp[] = new byte[dest.length - 4];
			System.arraycopy(dest, 1, temp, 0, dest.length - 4);
			byte recv[] = Commcenter.Decrypt(temp);
			Log.log("接收明文", recv);
			return temp;
		}
		
		public boolean isRunning() 
		{
			return mIsRunning;
		}
		
		public void setRunning(boolean running)
		{
			this.mIsRunning = running;
		}
	}
	
	// 获取错误编码
	@Override
	public int getLastknownErrCode() 
	{
		return mErrCode;
	}
	
	// 设置错误描述
	public void setLastknownError(String lastknownError)
	{
		Log.log("xxxxxxxxxx     lastknownError = " + lastknownError + "     xxxxxxxxxx");
		this.mLastKnownError = lastknownError;
	}
	
	// 获取错误描述
	@Override
	public String getLastKnowError() 
	{
		return mLastKnownError;
	}
	
	// 获取最后一次发送的时间
	@Override
	public long getLastSendTime() 
	{
		return mLastSendTime;
	}
}
