package com.sunnada.baseframe.service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import sunnada.jni.Commcenter;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.PSAM_Data;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.PsamInfo;
import com.sunnada.baseframe.bean.ReturnInfo;
import com.sunnada.baseframe.bean.SystemInfoType;
import com.sunnada.baseframe.database.SystemInfoDao;
import com.sunnada.baseframe.database.SystemParamsDao;
import com.sunnada.baseframe.equipment.IDevice;
import com.sunnada.baseframe.equipment.IntellgentDeviceOpr;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.MD5Util;
import com.sunnada.baseframe.util.MyDES;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.bluetooth.BluetoothClient;

public class EquipmentService extends Service implements IEquipmentService
{
	private static byte 	dataIndex 			= 0x00;
	private boolean 		isSoftEncrypt 		= false;				// 是否软加密
	private boolean 		isTerminateCanUse 	= true;					// 终端是否可用
	private IDevice 		intellgentDeviceOpr = null;
	
	private boolean 		isInit 				= false;
	private PsamInfo 		psamInfo 			= new PsamInfo();

	private String 			localPhoneNumber 	= null;					// 本地SIM卡的手机号码
	private byte[] 			loginKey 			= null;					// 登陆密钥
	private String 			lastknownError 		= "Unknow Error";
	private boolean 		offline 			= false;				// 是否离线

	private String 			deviceType 			= "intelligent";
	private String			mStrAdverInfo		= null;
	
	public class EquipmentBinder extends Binder
	{
		// 获取socket服务
		public EquipmentService getService() 
		{
			return EquipmentService.this;
		}
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.log("EquipmentService启动...");
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.log("EquipmentService关闭...");
	}

	@Override
	public IBinder onBind(Intent intent) 
	{
		return new EquipmentBinder();
	}

	// 初始化设备
	@Override
	public boolean initEquiment() 
	{
		if (!intellgentDeviceOpr.initEquipment())
		{
			setLastknownError(intellgentDeviceOpr.getStrResult());
			return false;
		}
		else
		{
			initVoice();
			isInit = true;
			return true;
		}
	}
	
	// 资源卸载
	@Override
	public boolean releaseEquiment() 
	{
		if(intellgentDeviceOpr.releaseEquipment()) 
		{
			isInit = false;
			return true;
		}
		return false;
	}
	
	// 返回硬件初始化状态
	@Override
	public boolean isInitEquiment() 
	{
		return isInit;
	}
	
	// 握手
	@Override
	public boolean handshake() 
	{
		return intellgentDeviceOpr.handshake();
	}
	
	// 初始化声音
	@Override
	public void initVoice() 
	{
		intellgentDeviceOpr.Mini_audio_init();
		intellgentDeviceOpr.Mini_audio_switch((byte)1);
		Log.d("", "初始化音频设备");
	}
	
	// 获取外设类型
	@Override
	public String getDeviceType() 
	{
		return deviceType;
	}
	
	// 设置外设类型
	@Override
	public boolean setDeviceType(String type) 
	{
		if (intellgentDeviceOpr == null) 
		{
			intellgentDeviceOpr = new IntellgentDeviceOpr(this);
		}
		deviceType = type;
		return true;
	}
	
	// 设置蓝牙设备接口
	@Override
	public boolean setBluetoothClient(BluetoothClient bluetoothClient)
	{
		return intellgentDeviceOpr.setBluetoothClient(bluetoothClient);
	}
	
	// 获取厂家编码
	@Override
	public int getFactoryCode() 
	{
		byte[] factoryCodeBuf = new byte[2];
		intellgentDeviceOpr.getFactoryCode(factoryCodeBuf);
		return factoryCodeBuf[0];
	}
	
	// 获取各类版本号
	@Override
	public String readVersion(byte type) 
	{
		return intellgentDeviceOpr.readVersion(type);
	}
	
	// 获取M3应用版本
	@Override
	public String readM3AppVersion() 
	{
		return readVersion((byte) 0x03);
	}

	// 获取M3驱动版本
	@Override
	public String readM3DriverVersion() 
	{
		return readVersion((byte) 0x02);
	}
	
	// M3升级
	@Override
	public boolean m3Update(String path_name, String file_name, byte file_type, int updata_sum) 
	{
		boolean ret = false;
		ret = intellgentDeviceOpr.m3Update(path_name, file_name, file_type, updata_sum);
		setLastknownError(intellgentDeviceOpr.getStrResult());
		return ret;
	}
	
	// 获取PSAM卡信息
	@Override
	public Map<String, String> getPsamInfo() 
	{
		if (StringUtil.isEmptyOrNull(psamInfo.getAgent())) 
		{
			intellgentDeviceOpr.readOpenPasswdInfo(psamInfo);
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("PSAMID", 		psamInfo.getPsamID());
		map.put("AGENT_NAME", 	psamInfo.getAgent());
		map.put("AGENT_ADDR", 	psamInfo.getAddress());
		map.put("AGENT_TEL", 	psamInfo.getTelno());
		map.put("MAIN_IP", 		psamInfo.getMasterIP());
		map.put("BACKUP_IP", 	psamInfo.getMasterIPBak());
		map.put("MAIN_PORT", 	psamInfo.getMasterSendPort() + "");
		map.put("BACKUP_PORT", 	psamInfo.getMasterSendPortBak() + "");
		map.put("CREATE_DATE", 	psamInfo.getPsamStartDate());
		
		return map;
	}
	
	@Override
	public PsamInfo getPsamInfo(int type)
	{
		return psamInfo;
	}
	
	// 获取主备服务器信息
	// 获取主服务器IP
	@Override
	public String getIp1() 
	{
		return psamInfo.getMasterIP();
	}

	// 获取主服务器端口
	@Override
	public int getPort1() 
	{
		return psamInfo.getMasterSendPort();
	}

	// 获取备用服务器IP
	@Override
	public String getIp2() 
	{
		return psamInfo.getMasterIPBak();
	}

	// 获取备用服务器端口
	@Override
	public int getPort2() 
	{
		return psamInfo.getMasterSendPortBak();
	}
	
	// 获取PSAM卡号码
	@Override
	public String getPsamId() 
	{
		String psamid = null;
		if (psamInfo != null)
		{
			psamid = psamInfo.getPsamID();
		}
		return psamid;
	}
	
	// 设置PSAM卡号码
	@Override
	public void setPsamId(String psamId) 
	{
		if (psamInfo == null)
		{
			psamInfo = new PsamInfo();
		}
		psamInfo.setPsamID(psamId);
	}
	
	@Override
	// 读取PSAMID和PSAM基本信息
	public boolean readPsamInfo() 
	{
		return readPsamInfoFull(true);
	}
	
	// 读取PSAM信息，根据传递参数，可选择从缓存获取，以节省时间
	@Override
	public boolean readPsamInfoFull(boolean isReload) 
	{
		// 蓝牙设备未初始化
		if (isInit == false)
		{
			setLastknownError(getString(R.string.system_init_error_or_not_init));
			return false;
		}
		// 读取PSAM卡卡号失败
		if (intellgentDeviceOpr.readPsamId(psamInfo))
		{
			setLastknownError(getString(R.string.read_psam_error) + "[" + intellgentDeviceOpr.getStrResult() + "]");
			return false;
		}
		// 如果是蓝牙方式，把psaminfo信息重置一下
		if ("blue".equals(this.deviceType)) 
		{
			psamInfo = PSAM_Data.psaminfo;
		}
		// 读取缓存中的PSAM ID
		SystemInfoDao sysinfodao = new SystemInfoDao(this);
		String psamid = sysinfodao.getSystemInfoName(SystemInfoType.PSAMID);
		// 重新加载PSAM卡信息
		if (isReload == true || StringUtil.isEmptyOrNull(psamid) || !psamid.equals(psamInfo.getPsamID()))
		{
			if (intellgentDeviceOpr.readPsamInfo(psamInfo) == false)
			{
				setLastknownError("读取PSAM卡信息失败!");
				return false;
			}
			else
			{
				if ("blue".equals(this.deviceType))
				{
					psamInfo = PSAM_Data.psaminfo;
				}
				// 把信息更新缓存
				sysinfodao.setSystemInfo(SystemInfoType.PSAMID, 			psamInfo.getPsamID());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_AGENT_NAME, 	psamInfo.getAgent());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_AGENT_ADDR, 	psamInfo.getAddress());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_AGENT_TEL, 	psamInfo.getTelno());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_MAIN_IP, 		psamInfo.getMasterIP());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_BACKUP_IP, 	psamInfo.getMasterIPBak());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_MAIN_PORT, 	"" + psamInfo.getMasterSendPort());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_BACKUP_PORT, 	"" + psamInfo.getMasterSendPortBak());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_CREATE_DATE, 	psamInfo.getPsamStartDate());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_END_DATE, 		psamInfo.getPsamEndDate());
				sysinfodao.setSystemInfo(SystemInfoType.PSAM_IS_RELOAD, 	"true");
				return true;
			}
		}
		else
		{
			// 从数据库缓存中获取psam的相关信息
			psamInfo.setAgent(sysinfodao.getSystemInfoName(SystemInfoType.PSAM_AGENT_NAME));
			psamInfo.setAddress(sysinfodao.getSystemInfoName(SystemInfoType.PSAM_AGENT_ADDR));
			psamInfo.setTelno(sysinfodao.getSystemInfoName(SystemInfoType.PSAM_AGENT_TEL));
			psamInfo.setMasterIP(sysinfodao.getSystemInfoName(SystemInfoType.PSAM_MAIN_IP));
			psamInfo.setMasterIPBak(sysinfodao.getSystemInfoName(SystemInfoType.PSAM_BACKUP_IP));
			sysinfodao.setSystemInfo(SystemInfoType.PSAM_IS_RELOAD, "false");
			// 业务服务器连接端口
			String sPort = sysinfodao.getSystemInfoName(SystemInfoType.PSAM_MAIN_PORT);
			if (!StringUtil.isEmptyOrNull(sPort))
			{
				psamInfo.setMasterSendPort(Integer.parseInt(sPort));
			}
			// 备用服务器连接端口
			String sPortBak = sysinfodao.getSystemInfoName(SystemInfoType.PSAM_BACKUP_PORT);
			if (!StringUtil.isEmptyOrNull(sPortBak))
			{
				psamInfo.setMasterSendPort(Integer.parseInt(sPortBak));
			}
			
			// 把信息更新缓存
			sysinfodao.setSystemInfo(SystemInfoType.PSAMID, 			psamInfo.getPsamID());
			sysinfodao.setSystemInfo(SystemInfoType.PSAM_CREATE_DATE, 	psamInfo.getPsamStartDate());
			sysinfodao.setSystemInfo(SystemInfoType.PSAM_END_DATE, 		psamInfo.getPsamEndDate());
			return true;
		}
	}
	
	// 获取PSAM卡加密密钥
	@Override
	public byte[] readPsamExtKey() 
	{
		return PSAM_Data.extKey;
	}
	
	// PSAM卡加密
	@Override
	public boolean psamEncryptByte(byte[] data, byte len, byte type) 
	{
		return intellgentDeviceOpr.psamEncrypt(data, len, type);
	}
	
	// 数据包打包&加密
	@Override
	public byte[] psamEncrypt(Package dataPackage) 
	{
		Log.log("psamEncrypt_dataPackage = " + dataPackage);

		byte[] buf = null;
		byte[] psamIdBytes = Commen.hexstr2byte(psamInfo.getPsamID());
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write((byte) 0x06);											// 通讯模式：以太网方式传输 0x06 Ethernet
			baos.write((byte) 0x20);											// 协议版本
			baos.write((byte) 0x0B);											// 厂商标识
			baos.write(psamIdBytes); 											// PSAM卡号

																				// 低4位 数据压缩算法 0: 未压缩 1: Huffman算法 2~7: 未定义。
																				// 高4位 数据加密算法 0: 未加密 1: DES加密 2: 3DES加密 3~7: 未定义，保留
			baos.write((byte) 0x00); 											// 数据状态码
			baos.write(Commen.hexstr2byte("0000")); 							// 安全数据码：系统分配给各厂家随机密钥随机加密的生成值。未分配的默认为0x0000
			baos.write(dataIndex++); 											// 通信标识
			
			baos.write((byte) 0x01); 											// 1B 通信包总数
			baos.write((byte) 0x01); 											// 1B 通信包序号
			
			baos.write(Commen.hexstr2byte(dataPackage.getServiceCommandID()));	// 业务时序

			baos.write((byte) 0x00); 											// 开机随机验证
			baos.write(Commen.hexstr2byte(dataPackage.getSubId())); 			// 子命令
			baos.write(Commen.hexstr2byte(dataPackage.getId())); 				// 主命令
			baos.write((byte) 0xFF); 											// 应答标识

			baos.write(dataPackage.getPduBytes());								// 命令体：数据长度+子命令标识+命令内容+数据长度+子命令标识+命令内容
			buf = baos.toByteArray();
			baos.close();
			Log.log("发送明文", buf);

			Commcenter.Encrypt(buf);											// 加密
			byte[] crc = Commcenter.CalcCrc16(buf, (char) 0);
			
			baos = new ByteArrayOutputStream();
			baos.write(buf);
			baos.write(crc);
			
			// 生成 鉴权码
			int z = 0;
			for (z = 0; z < 4; z++)
			{
				psamIdBytes[z] ^= crc[0];
			}
			for (z = 4; z < 8; z++)
			{
				psamIdBytes[z] ^= crc[1];
			}
			
			String packageId = dataPackage.getId() + dataPackage.getSubId();
			Log.log("packageId = " + packageId);
			// 签到密钥为空
			if (loginKey == null)
			{
				if ("0000".equals(packageId))
				{
					Log.log("使用固定密钥进行加密！");
					System.arraycopy(MyDES.des_crypt("sunnada0".getBytes(), psamIdBytes), 0, psamIdBytes, 0, 8);
				}
				else
				{
					// PSAM卡加密
					Log.d("鉴权码加密", "使用PSAM卡进行加密！");
					if (!intellgentDeviceOpr.psamEncrypt(psamIdBytes, (byte) 0x08, (byte) 0x00))
					{
						Log.e("socket_send_明文加密", "加密失败[" + intellgentDeviceOpr.getStrResult() + "]..");
						setLastknownError(getString(R.string.data_encrypt_error) + "[" + intellgentDeviceOpr.getStrResult() + "]");
						return null;
					}
				}
			} 
			else
			{
				/*
				if ("0021".equals(packageId))
				{
					Log.log("使用固定密钥进行加密！");
					System.arraycopy(MyDES.des_crypt("sunnada0".getBytes(), psamIdBytes), 0, psamIdBytes, 0, 8);
				} 
				else
				*/
				{
					Log.d("鉴权码加密", "使用签到密钥进行加密！");
					System.arraycopy(MyDES.des_crypt(loginKey, psamIdBytes), 0, psamIdBytes, 0, 8);
				}
			}
			baos.write(psamIdBytes); 			// PSAM鉴权码
			buf = baos.toByteArray();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			setLastknownError(getString(R.string.data_encrypt_error));
		}
		return Commcenter.TransferredMeaning(buf);
	}
	
	// 验证开机密码
	@Override
	public boolean PSAM_CheckPasswd(String pin) 
	{
		ReturnInfo strRes = intellgentDeviceOpr.PSAM_CheckPasswd(pin);
		if(null == strRes)
		{
			this.lastknownError = "密码校验失败";
			return false;
		}
		else
		{
			// 开机密码验证错误
			if(strRes.bResult == false)
			{
				this.lastknownError = String.format("密码校验错误, %s", strRes.desc);
				return false;
			}
			return true;
		}
	}
	
	// 修改开机密码
	@Override
	public boolean PSAM_ModifyPasswd(String oldpin, String newpin)
	{
		ReturnInfo res = intellgentDeviceOpr.PSAM_ModifyPasswd(oldpin, newpin);
		// 开机密码修改错误
		if (false == res.bResult)
		{
			this.lastknownError = res.desc;
			return false;
		} 
		else
		{
			SystemInfoDao sysinfodao = new SystemInfoDao(this);
			String enPwd = MD5Util.bufferToHex(MD5Util.getMD5String(newpin));
			sysinfodao.setSystemInfo(SystemInfoType.LOGIN_PWD, enPwd, enPwd);
			Log.d("", "新密码加入缓存");
			return true;
		}
	}
	
	// 联通写白卡
	// 读取ICCID
	@Override
	public String readIccid() 
	{
		return intellgentDeviceOpr.readIccid();
	}

	// 判断成卡、白卡
	@Override
	public int isWhiteCard(byte szSimType) 
	{
		return intellgentDeviceOpr.isWhiteCard(szSimType);
	}

	// 联通写白卡
	@Override
	public boolean writeSimCard(byte[] szImsi, byte szSimType)
	{
		return intellgentDeviceOpr.writeSimCard(szImsi, szSimType);
	}

	// 写短消息中心号
	@Override
	public boolean writesmscentre(String szSmsc, byte szSimType)
	{
		return intellgentDeviceOpr.writeSmsCentre(szSmsc, szSimType);
	}
	
	// 打印机
	// 打印机状态检测
	public int checkPrinterStatus() 
	{
		return intellgentDeviceOpr.checkPrinterStatus();
	}
		
	// 检测打印机是否有纸
	@Override
	public boolean printerHasPaper() 
	{
		return intellgentDeviceOpr.isPrinterHasPaper();
	}
	
	// 设置打印机字体
	@Override
	public void setPrintFont(int size) 
	{
		intellgentDeviceOpr.setPrintFont(size);
	}

	// 打印文本信息
	@Override
	public void printText(String printContent) 
	{
		intellgentDeviceOpr.printMsg(printContent);
	}
	
	// 打印LOGO
	@Override
	public void printLogo(int[] logoData) 
	{
		intellgentDeviceOpr.printLogo(logoData);
	}
	
	@Override
	public void printLogoFile(String fileName) 
	{
		intellgentDeviceOpr.printLogo(fileName);
	}

	// 开始打印
	@Override
	public void startPrint() 
	{
		intellgentDeviceOpr.startPrint();
	}
	
	// 检测黑标是否存在
	public boolean isBlackLabelExist()
	{
		return intellgentDeviceOpr.isBlackLabelExist();
	}
	
	// 是否打印成功
	@Override
	public boolean isPrintSuccess() 
	{
		return !intellgentDeviceOpr.isPrintError();
	}
	
	// 获取打印错误描述
	@Override
	public String getPrintErrorMsg() 
	{
		return intellgentDeviceOpr.getPrintErrorMsg();
	}
	
	// 获取打印错误描述
	@Override
	public String getPrintErrorMessage() 
	{
		return intellgentDeviceOpr.getPrintErrorMessage();
	}
	
	// 获取广告语
	@Override
	public String getAdverInfo() 
	{
		if(StringUtil.isEmptyOrNull(mStrAdverInfo)) 
		{
			SystemParamsDao dao = new SystemParamsDao(this);
			mStrAdverInfo = dao.getAdverInfo();
		}
		return mStrAdverInfo;
	}
	
	// 移动写卡
	// 读取移动号卡的序列号
	public String readMobilSimSerialNumber()
	{
		return intellgentDeviceOpr.readMobilSimSerialNumber();
	}
	
	// 读取移动号卡的ICCID号码
	@Override
	public String readMobilSimIccidNumber() 
	{
		return intellgentDeviceOpr.readMobilSimIccidNumber();
	}

	// 读取移动号卡的IMSI号码
	@Override
	public String readMobilSimImsiNumber() 
	{
		return intellgentDeviceOpr.readMobilSimImsiNumber();
	}
	
	// 读取移动号卡的短信中心号码
	@Override
	public String readMobilSimSmscNumber() 
	{
		return intellgentDeviceOpr.readMobilSimSmscNumber();
	}
	
	// 是否是移动白卡
	@Override
	public boolean isFJMobileWhiteCard() 
	{
		Log.d("", "判断福州移动白卡");
		String iccid = readFZMobileIccid();
		if (StringUtil.isEmptyOrNull(iccid))
		{
			Log.e("", "读取ICCID失败");
			return false;
		}
		Log.d("", "ICCID = " + iccid);
		
		for (int i = 0; i < iccid.length(); i++)
		{
			if (iccid.charAt(i) != 'F' && iccid.charAt(i) != 'f')
			{
				Log.d("", "成卡");
				return false;
			}
		}
		Log.d("", "白卡");
		return true;
	}
	
	// 读取福州移动卡序列号
	@Override
	public String readFZMobileSearialNumber() 
	{
		return intellgentDeviceOpr.readFZMobileSearialNumber();
	}
	
	// 读取福州移动白卡ICCID
	@Override
	public String readFZMobileIccid() 
	{
		return intellgentDeviceOpr.readFZMobileIccid();
	}

	// 写福州移动白卡ICCID
	@Override
	public boolean writeFZMobileIccid(String iccid) 
	{
		return intellgentDeviceOpr.writeFZMobileIccid(iccid);
	}

	// 读取福州移动白卡IMSI
	@Override
	public String readFZMobileImsi() 
	{
		return intellgentDeviceOpr.readFZMobileImsi();
	}

	// 写福州移动白卡IMSI
	@Override
	public boolean writeFZMobileImsi(String imsi) 
	{
		return intellgentDeviceOpr.writeFZMobileImsi(imsi);
	}

	// 读取福州移动白卡短信中心号码
	@Override
	public String readFZMobileSmsCenter() 
	{
		return intellgentDeviceOpr.readFZMobileSmsCenter();
	}

	// 写福州移动白卡短信中心号码
	@Override
	public boolean writeFZMobileSmsCenter(String smsCenter) 
	{
		return intellgentDeviceOpr.writeFZMobileSmsCenter(smsCenter);
	}

	// 更新福州移动白卡PIN和PUK
	@Override
	public boolean updateFZMobilePinAndPuk(String oldPin1, String newPin1, String oldPin2, String newPin2, 
			String oldPuk1, String newPuk1, String oldPuk2, String newPuk2) 
	{
		return intellgentDeviceOpr.updateFZMobilePinAndPuk(oldPin1, newPin1, oldPin2, newPin2, oldPuk1, newPuk1, oldPuk2, newPuk2);
	}

	// 写福州移动白卡
	@Override
	public boolean writeFZMobileWhiteCard(String iccid, String imsi, String smsCenter, String oldPin1, String newPin1, 
			String oldPin2, String newPin2, String oldPuk1, String newPuk1, String oldPuk2, String newPuk2, String ki) 
	{
		return intellgentDeviceOpr.writeFZMobileWhiteCard(iccid, imsi, smsCenter, oldPin1, newPin1, oldPin2, newPin2, 
				oldPuk1, newPuk1, oldPuk2, newPuk2, ki);
	}
	
	// 其他
	// 获取随机密钥
	public byte[] getLoginKey()
	{
		return loginKey;
	}
	
	// 设置随机密钥
	public void setLoginKey(byte[] loginKey) 
	{
		/*
		if (loginKey != null)
		{
			try
			{
				byte[] data = new byte[loginKey.length];
				System.arraycopy(loginKey, 0, data, 0, loginKey.length);
				Log.log("签到秘钥密文:", loginKey);
				psamEncryptByte(data, (byte) data.length, (byte) 0x01);
				Log.log("签到秘钥明文:", data);
				
				this.loginKey = data;
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				this.loginKey = null;
			}
		}
		*/
		this.loginKey = loginKey;
	}
	
	// 获取IMSI
	@Override
	public String getIMSI() 
	{
		try 
		{
			TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			return tm.getSubscriberId();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// 获取IMEI号码
	@Override
	public String getIMEI() 
	{
		try 
		{
			TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			String imei = telephonyManager.getDeviceId();
			if(imei == null) 
			{
				imei = "000000000000000";
			}
			Log.d("", "imei = " + imei);
			return imei;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "000000000000000";
		}
	}
	
	// 获取小区信息
	@Override
	public String[] getLacCi() 
	{
		String[] result = new String[2];
		result[0] = "0000";
		result[1] = "0000";
		
		try
		{
			TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			GsmCellLocation gcl = (GsmCellLocation)telephonyManager.getCellLocation();
			// 
			result[0] = String.valueOf(gcl.getLac());
			result[1] = String.valueOf(gcl.getCid());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	// 获取本地手机号码
	public String getLocalPhoneNumber()
	{
		if (localPhoneNumber == null)
		{
			try
			{
				TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
				localPhoneNumber = tm.getLine1Number();
				Log.log("手机号码", localPhoneNumber + "");
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			if (localPhoneNumber == null)
			{
				localPhoneNumber = "";
			}
		}
		return localPhoneNumber;
	}

	// 设置本地手机号码
	public void setLocalPhoneNumber(String localPhoneNumber)
	{
		this.localPhoneNumber = localPhoneNumber;
	}
	
	// 获取程序版本
	@Override
	public String getFrameVersion() 
	{
		try
		{
			PackageManager pm = getPackageManager();
			PackageInfo pi = pm.getPackageInfo(getPackageName(), 0);
			// 
			String versionName = pi.versionName;
			if (StringUtil.isEmptyOrNull(versionName)) 
			{
				versionName = "0.0.0";
			}
			return versionName;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "0.0.0";
		}
	}
	
	// 是否软加密
	@Override
	public boolean isSoftEncrypt() 
	{
		return isSoftEncrypt;
	}
	
	// 检测当前终端是否可用
	@Override
	public boolean isTerminateCanUse()
	{
		return isTerminateCanUse;
	}

	// 设置终端是否可用
	@Override
	public void setTerminateCanUse(boolean isTerminateCanUse)
	{
		this.isTerminateCanUse = isTerminateCanUse;
	}
	
	// 设置注销
	@Override
	public void setOffline(boolean enable) 
	{
		offline = enable;
	}

	// 检测是否注销
	@Override
	public boolean isOffline() 
	{
		return offline;
	}
	
	public String getLastknownError()
	{
		return lastknownError;
	}

	public void setLastknownError(String lastknownError)
	{
		this.lastknownError = lastknownError;
	}

	@Override
	public byte[] calcCrc16(byte[] data, char c) 
	{
		return Commcenter.CalcCrc16(data, c);
	}
}
