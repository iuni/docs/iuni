package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

// 手机信息查询的dialog
public class PhoneInfoDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private TextView			mTextQueryPhone;					// 手机号码文本框
	private LinearLayout        mLayUserName;						// 用户名布局
	private TextView			mTextUserName;						// 用户名文本框
	private TextView			mTextMoney;							// 可用余额
	
	private String				mStrTelNum;							// 手机号码
	private String				mStrUserName;						// 用户名
	private String				mStrMoney;							// 可用余额
	
	public PhoneInfoDialog(Context context, String strTelNum, String strUserName, String strMoney) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_phone_info);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_phone_info_orange);
		}
		// 
		mStrTelNum 	= strTelNum;
		mStrUserName = strUserName;
		mStrMoney	= strMoney;
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mTextQueryPhone = (TextView) findViewById(R.id.tv_query_number);
		mLayUserName = (LinearLayout) findViewById(R.id.lay_user_name);
		mTextUserName = (TextView) findViewById(R.id.tv_user_name);
		mTextMoney = (TextView) findViewById(R.id.tv_money);
		
		// 设置手机号码
		mTextQueryPhone.setText(mStrTelNum);
		// 用户名
		if(StringUtil.isEmptyOrNull(mStrUserName)) 
		{
			mLayUserName.setVisibility(View.GONE);
		}
		else
		{
			mLayUserName.setVisibility(View.VISIBLE);
			mTextUserName.setText(mStrUserName);
		}
		// 可用余额
		mTextMoney.setText(mStrMoney);
		
		// 关闭对话框
		Button btnClose = (Button) findViewById(R.id.btn_dialog_close);
		btnClose.setOnClickListener(this);
		
		MyCustomButton btnOk = (MyCustomButton) findViewById(R.id.btn_ok);
		btnOk.setImageResource(R.drawable.btn_custom_check);
		btnOk.setTextViewText1("确定");
		btnOk.setOnClickListener(this);
		btnOk.setOnTouchListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 确定
		case R.id.btn_ok:
			dismiss();
			break;
			
		// 关闭对话框
		case R.id.btn_dialog_close:
			dismiss();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
}
