package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ChangeSkinDialog extends Dialog implements android.view.View.OnClickListener
{
	private ImageView 		mCurrSelected;
	private int 	  		mCurSelecteId;
	private Editor			mEditor;
	private LinearLayout 	mSkinBlue;
	private LinearLayout 	mSkinOrange;
	
	public ChangeSkinDialog(Context context)
	{
		super(context);
		mEditor = context.getSharedPreferences(Statics.CONFIG, Context.MODE_PRIVATE).edit();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_change_skin);
		}
		else
		{
			setContentView(R.layout.dlg_change_skin_orange);
		}
		mCurSelecteId = Statics.SKIN_TYPE;
		initView();
		initData();
	}

	private void initView()
	{
		ImageView ivClose = (ImageView) findViewById(R.id.ivClose);
		ivClose.setOnClickListener(this);
		Button btnClose = (Button) findViewById(R.id.btn_cancel);
		btnClose.setOnClickListener(this);
		Button btnOk = (Button) findViewById(R.id.btn_ok);
		btnOk.setOnClickListener(this);
		mSkinBlue = (LinearLayout) findViewById(R.id.skin_blue);
		mSkinBlue.setOnClickListener(this);
		mSkinOrange = (LinearLayout) findViewById(R.id.skin_orange);
		mSkinOrange.setOnClickListener(this);
	}
	
	// ��ʼ��ѡ��״̬
	private void initData()
	{
		if(mCurrSelected != null)
		{
			mCurrSelected.setVisibility(View.GONE);
		}
		if(mCurSelecteId == Statics.SKIN_BLUE)
		{
			mCurrSelected = (ImageView)mSkinBlue.getChildAt(0);
		}
		else if(mCurSelecteId == Statics.SKIN_ORANGE)
		{
			mCurrSelected = (ImageView)mSkinOrange.getChildAt(0);
		}
		mCurrSelected.setVisibility(View.VISIBLE);
	}

	// ��ʾ
	public void show()
	{
		mCurSelecteId = Statics.SKIN_TYPE;
		initData();
		super.show();
	}
	
	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			// ��ɫƤ��
			case R.id.skin_blue:
				if(mCurSelecteId != Statics.SKIN_BLUE)
				{
					setContentView(R.layout.dlg_change_skin);
					initView();
					if(mCurrSelected != null)
					{
						mCurrSelected.setVisibility(View.GONE);
					}
					mCurrSelected = ((ImageView)((LinearLayout)v).getChildAt(0));
					mCurSelecteId = Statics.SKIN_BLUE;
					mCurrSelected.setVisibility(View.VISIBLE);
					initData();
				}
				break;
				
			// ��ɫƤ��
			case R.id.skin_orange:
				if(mCurSelecteId != Statics.SKIN_ORANGE)
				{
					setContentView(R.layout.dlg_change_skin_orange);
					initView();
					if(mCurrSelected != null)
					{
						mCurrSelected.setVisibility(View.GONE);
					}
					mCurrSelected = ((ImageView)((LinearLayout)v).getChildAt(0));
					mCurSelecteId = Statics.SKIN_ORANGE;
					mCurrSelected.setVisibility(View.VISIBLE);
					initData();
				}
				break;
				
			// ȷ������
			case R.id.btn_ok:
				Statics.SKIN_TYPE = mCurSelecteId;
				mEditor.putInt(Statics.STR_SKIN_TYPE, Statics.SKIN_TYPE);
				mEditor.commit();
				dismiss();
				break;
				
			case R.id.btn_cancel:
			case R.id.ivClose:
				dismiss();
				break;

			default:
				break;
		}
	}

}
