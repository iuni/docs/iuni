package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.CollectionUtil;
import com.sunnada.baseframe.util.DimensionUtil;
import com.sunnada.bluetooth.BluetoothClient;
import com.sunnada.bluetooth.BluetoothListener;

// 蓝牙搜索Dialog
public class SearchBluetoothDialog extends Dialog implements OnClickListener, OnTouchListener, BluetoothListener
{
	private final String				TITLE_STR				= "温馨提示";
	private final int					MSG_REFRESH_BLUE_LIST	= 0x01;
	private final int					MSG_CONNECT_BLUE		= 0x02;
	private final int					MSG_SEARCH				= 0x03;
	private final int					MSG_CONNECT_SUCCESS		= 0x04;
	
	private Context						mContext;
	private BluetoothClient				mBluetoothClient		= null;
	private IEquipmentService			mEquipmentService		= null;
	private Handler						mCallHandler;
	private int							mDismissID				= 0x00;
	
	private ListView					mListBluetooth;						// 蓝牙列表
	
	private List<Map<String, String>> 	mListData 				= new ArrayList<Map<String, String>>();
	private String						mStrBlueName			= "";		// 蓝牙设备名称
	private String						mStrBlueMac				= "";		// 蓝牙设备MAC
	private int 						mSelectedPos	 		= -1;
	private View 						mOldView				= null;
	private Timer						mTimerBlue				= null;
	private final int					SEARCH_TIME				= 60000;		// 蓝牙搜索最长时间 60s
	
	private Handler mHandler = new Handler() 
	{
		@Override
		public void handleMessage(Message message) 
		{
			switch(message.what) 
			{
			// 刷新ListView
			case MSG_REFRESH_BLUE_LIST:
				refreshBluetoothList();
				break;
				
			// 连接蓝牙设备
			case MSG_CONNECT_BLUE:
				// 
				mBluetoothClient.setDevice(mStrBlueMac);
				mEquipmentService.setBluetoothClient(mBluetoothClient);
				// 连接蓝牙
				new Thread() 
				{
					public void run() 
					{
						DialogUtil.showProgress("正在连接蓝牙...");
						// 释放与蓝牙设备的资源连接
						mEquipmentService.releaseEquiment();
						SystemClock.sleep(500);
						// 连接蓝牙设备
						boolean result = mEquipmentService.initEquiment();
						DialogUtil.closeProgress();
						// 
						if(result == false) 
						{
							if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap)) 
							{
								Statics.mCurMap.put(Statics.BLUETOOTH_STATUS, "false");
							}
							DialogUtil.MsgBox(TITLE_STR, "连接蓝牙设备失败！");
							mHandler.sendEmptyMessage(MSG_REFRESH_BLUE_LIST);
						}
						else
						{
							DialogUtil.MsgBox(TITLE_STR, "连接蓝牙设备成功！", "确定", MSG_CONNECT_SUCCESS, null, -1, MSG_CONNECT_SUCCESS, mHandler);
						}
					}
				}.start();
				break;
				
			// 开始搜索
			case MSG_SEARCH:
				handleSearch();
				break;
				
			case MSG_CONNECT_SUCCESS:
				// 添加设备到已连接
				Map<String, String> map = new HashMap<String, String>();
				map.put(Statics.BLUETOOTH_NAME, 	mStrBlueName);
				map.put(Statics.BLUETOOTH_MAC, 		mStrBlueMac);
				map.put(Statics.BLUETOOTH_STATUS, 	"true");
				Statics.mCurMap = map;
				// 刷新蓝牙设备列表
				//mHandler.sendEmptyMessage(MSG_REFRESH_BLUE_LIST);
				dismiss();
				if(mCallHandler != null)
				{
					mCallHandler.sendEmptyMessage(mDismissID);
				}
				break;
				
			default:
				break;
			}
		}
	};
	
	public SearchBluetoothDialog(Context context, BluetoothClient bluetoothClient, IEquipmentService equipmentService, 
			Handler handler, int dismissID) 
	{
		super(context, R.style.transparent_dialog);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_search_bluetooth);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_search_bluetooth_orange);
		}
		mContext 			= context;
		mBluetoothClient 	= bluetoothClient;
		mEquipmentService 	= equipmentService;
		mCallHandler 		= handler;
		mDismissID 			= dismissID;
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mTimerBlue = new Timer();
		// 关闭对话框
		Button btnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		btnDialogClose.setOnClickListener(this);
		// 重新搜索
		Button btnSearchAgain = (Button) findViewById(R.id.btn_search_again);
		btnSearchAgain.setOnClickListener(this);
		// 返回
		Button btnBack = (Button) findViewById(R.id.btn_ok);
		btnBack.setOnClickListener(this);
		
		// 列表框控件
		mListBluetooth = (ListView) findViewById(R.id.list_bluetooth);
	}
	
	@Override
	public void show()
	{
		// 开始搜索蓝牙
		mHandler.sendEmptyMessageDelayed(MSG_SEARCH, 50);
		super.show();
	}

	@Override
	public void onClick(View v) 
	{
		// 防止按钮快速触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 500)) 
		{
			return;
		}
		
		switch(v.getId()) 
		{
		// 关闭对话框
		case R.id.btn_dialog_close:
			if(mBluetoothClient != null) 
			{
				mBluetoothClient.cancelDiscovery();
			}
			mSelectedPos = -1;
			dismiss();
			mTimerBlue.cancel();
			if(mCallHandler != null)
			{
				mEquipmentService.releaseEquiment();
				mCallHandler.sendEmptyMessage(mDismissID);
			}
			break;
		
		// 开始连接
		case R.id.btn_ok:
			connect();
			break;
			
		// 刷新
		case R.id.btn_search_again:
			mSelectedPos = -1;
			mHandler.sendEmptyMessage(MSG_SEARCH);
			break;
			
		default:
			break;
		}
	}
	
	// 搜索
	private void handleSearch() 
	{
		try
		{
			if(mBluetoothClient == null)
			{
				return;
			}
			DialogUtil.showProgress("正在搜索蓝牙设备...");
			if(!mBluetoothClient.getBluetoothEnabled())
			{
				DialogUtil.closeProgress();
				DialogUtil.showMessage("当前蓝牙未开启，请开启蓝牙后点击刷新！");
				mEquipmentService.releaseEquiment();
				Statics.mCurMap.put(Statics.BLUETOOTH_STATUS, "false");
				return;
			}
			// 刷新蓝牙列表
			mListData.clear();
			if(mEquipmentService.handshake() == true)
			{
				if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true"))
				{
					mListData.add(Statics.mCurMap);
				}
			}
			mTimerBlue.cancel();
			mTimerBlue = new Timer();
			mTimerBlue.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					if(mBluetoothClient != null)
					{
						mBluetoothClient.cancelDiscovery();
					}
					DialogUtil.closeProgress();
				} 
			}, SEARCH_TIME);
			mBluetoothClient.cancelDiscovery();
			refreshBluetoothList();
			// 搜索蓝牙
			mBluetoothClient.startDiscovery(mContext, this);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 刷新蓝牙列表
	private void refreshBluetoothList() 
	{
		//Log.e("", "重新刷新ListView");
		BaseAdapter adapter = new BluetoothListAdapter(mContext, mListData, R.layout.dlg_search_bluetooth_listitem);
		mListBluetooth.setAdapter(adapter);
		DimensionUtil.setListViewHeightBasedOnChildren(mListBluetooth);
		// 设置选择事件
		mListBluetooth.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) 
			{
				if(mOldView != null) 
				{
					if(mSelectedPos == 0 && !CollectionUtil.isEmptyOrNull(Statics.mCurMap)
					&& Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true"))
					{
						((TextView)(mOldView.findViewById(R.id.tv_name))).setTextColor(Color.RED);
						((TextView)(mOldView.findViewById(R.id.tv_mac))).setTextColor(Color.RED);
					}
					else
					{
						((TextView)(mOldView.findViewById(R.id.tv_name))).setTextColor(Color.GRAY);
						((TextView)(mOldView.findViewById(R.id.tv_mac))).setTextColor(Color.GRAY);
					}
				}
				((TextView)(arg1.findViewById(R.id.tv_name))).setTextColor(Color.BLUE);
				((TextView)(arg1.findViewById(R.id.tv_mac))).setTextColor(Color.BLUE);
				mOldView = arg1;
				mSelectedPos = pos;
			}
		});
	}
	
	// 开始连接蓝牙
	private void connect()
	{
		if(!mBluetoothClient.getBluetoothEnabled())
		{
			DialogUtil.showMessage("当前蓝牙未开启，请开启蓝牙后点击刷新！");
			mEquipmentService.releaseEquiment();
			if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true"))
			{
				Statics.mCurMap.put(Statics.BLUETOOTH_STATUS, "false");
			}
			return;
		}
		if(mSelectedPos == -1)
		{
			DialogUtil.MsgBox("温馨提示", "请先选择想要连接的蓝牙设备！");
			return;
		}
		// 停止搜索蓝牙
		if(mBluetoothClient != null) 
		{
			mBluetoothClient.cancelDiscovery();
		}
		// 获取蓝牙设备的MAC地址
		mStrBlueName = mListData.get(mSelectedPos).get(Statics.BLUETOOTH_NAME);
		mStrBlueMac  = mListData.get(mSelectedPos).get(Statics.BLUETOOTH_MAC);
		Log.e("", "蓝牙设备名称为: " + mStrBlueName + ", 蓝牙MAC地址为: " + mStrBlueMac);
		// 当前蓝牙设备已连接
		if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true")) 
		{
			if(mStrBlueMac.equals(Statics.mCurMap.get(Statics.BLUETOOTH_MAC))) 
			{
				new Thread() 
				{
					public void run() 
					{
						DialogUtil.showProgress("正在检测蓝牙设备...");
						boolean result = mEquipmentService.handshake();
						DialogUtil.closeProgress();
						// 当前蓝牙设备已连接
						if(result) 
						{
							DialogUtil.MsgBox(TITLE_STR, "当前蓝牙设备已连接！");
							return;
						}
						//mHandler.sendEmptyMessage(MSG_CONNECT_BLUE);
					}
				}.start();
				return;
			}
		}
		mHandler.sendEmptyMessage(MSG_CONNECT_BLUE);
	}
	
	// 蓝牙列表适配器
	private class BluetoothListAdapter extends BaseAdapter 
	{
		private LayoutInflater 				mInflater;
		private List<Map<String, String>> 	mListData;
		private int							mResource;
		
		public BluetoothListAdapter(Context context, List<Map<String, String>> listData, int resource)
		{
			this.mListData = listData;
			this.mResource = resource;
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			TextView tvName 	= null;
			TextView tvMac 		= null;
			ImageView ivStatus 	= null;
			ViewHolder holder 	= null;
			if(convertView == null)
			{
				convertView = mInflater.inflate(mResource, null);
				tvName = ((TextView)convertView.findViewById(R.id.tv_name));
				tvMac = ((TextView)convertView.findViewById(R.id.tv_mac));
				ivStatus = ((ImageView)convertView.findViewById(R.id.iv_bt_status));
				holder = new ViewHolder(tvName, tvMac, ivStatus);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
				tvName = holder.tvName;
				tvMac  = holder.tvMac;
				ivStatus = holder.ivStatus;
			}
			Log.e("", "当前索引为: " + position);
			if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) 
			  && mListData.get(position).get(Statics.BLUETOOTH_MAC).equals(Statics.mCurMap.get(Statics.BLUETOOTH_MAC))
			  && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true"))
			{
				tvName.setTextColor(Color.RED);
				tvMac.setTextColor(Color.RED);
				ivStatus.setImageResource(R.drawable.icon_bt_connected);
			}
			else
			{
				tvName.setTextColor(Color.GRAY);
				tvMac.setTextColor(Color.GRAY);
				ivStatus.setImageResource(R.drawable.icon_bt);
			}
			tvName.setText(mListData.get(position).get(Statics.BLUETOOTH_NAME));
			tvMac.setText(mListData.get(position).get(Statics.BLUETOOTH_MAC));
			return convertView;
		}

		@Override
		public int getCount()
		{
			return mListData.size();
		}

		@Override
		public Object getItem(int position)
		{
			return mListData.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
	
	// 搜寻到蓝牙设备
	@Override
	public void onBluetoothDeviceFound(BluetoothDevice device) 
	{
		//DialogUtil.closeProgress();
		Log.d("", "搜索到蓝牙设备, 蓝牙名称: " + device.getName() + ", mac地址: " + device.getAddress());
		
		// 蓝牙列表里有没有相同的蓝牙
		int nTotal = mListData.size();
		for(int index=0; index<nTotal; index++) 
		{
			if(mListData.get(index).get(Statics.BLUETOOTH_MAC).equals(device.getAddress())) 
			{
				Log.e("", "蓝牙列表已有此设备, 不再添加");
				return;
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put(Statics.BLUETOOTH_NAME, device.getName());
		map.put(Statics.BLUETOOTH_MAC, device.getAddress());
		mListData.add(map);
		// 刷新蓝牙列表
		refreshBluetoothList();
	}
	
	@Override
	public void onBluetoothDiscoveryFinished() 
	{
		DialogUtil.closeProgress();
		mTimerBlue.cancel();
		// 取消搜索
		if(mBluetoothClient != null) 
		{
			mBluetoothClient.cancelDiscovery();
		}
		// 弹出提示
		if(CollectionUtil.isEmptyOrNull(mListData)) 
		{
			//DialogUtil.MsgBox(TITLE_STR, "未搜索到蓝牙设备, 请确认打开蓝牙设备的电源！");
		}
	}
	
	class ViewHolder
	{
		TextView tvName 	= null;
		TextView tvMac 		= null;
		ImageView ivStatus 	= null;
		public ViewHolder(TextView tvName, TextView tvMac, ImageView ivStatus)
		{
			this.tvName = tvName;
			this.tvMac = tvMac;
			this.ivStatus = ivStatus;
		}
	}
}
