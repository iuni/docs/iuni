package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BaseBusiness;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

// 手机账户查询的Dialog
public class PhoneQueryDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private TextView			mTextQueryTitle;					// 标题文本框
	private TextView			mTextQueryPhone;
	private EditText			mEditRandom;						// 验证码输入框
	private LinearLayout        mLayRandom;                      	// 验证码信息
	
	private String				mStrTelNum;							// 手机号码
	private String				mStrRandom;							// 验证码
	private Handler				mHandler;							// 消息处理函数
	private BaseBusiness		mBusiness;							// 协议
	private TextView			mTvTipRandom;						// 短信验证码获取成功提示
	private boolean				mIsGetRandomOk	= false;			// 获取验证码OK?
	private TextView			mTvCountDown;						// 短信验证码倒计时
	private CountDownTimer		mTimer;		
	
	public PhoneQueryDialog(Context context, String strTelNum, Handler handler, BaseBusiness business) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_phone_query);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_phone_query_orange);
		}
		// 
		mStrTelNum 	= strTelNum;
		mHandler 	= handler;
		mBusiness 	= business;
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mTextQueryTitle = (TextView) findViewById(R.id.tv_query_title);
		mTextQueryPhone = (TextView) findViewById(R.id.tv_query_number);	
		mLayRandom   	= (LinearLayout) findViewById(R.id.random_layout);
		mEditRandom 	= (EditText) findViewById(R.id.et_random);
		mTvTipRandom    = (TextView) findViewById(R.id.tv_tip_random);
		mTvCountDown 	= (TextView) findViewById(R.id.tv_countdown);
		mTvTipRandom.setVisibility(View.GONE);
		// 设置手机号码
		mTextQueryPhone.setText(mStrTelNum);
		
		Button btnGetRandom = (Button) findViewById(R.id.btn_get_random);
		btnGetRandom.setOnClickListener(this);
		Button btnClose = (Button) findViewById(R.id.btn_dialog_close);
		btnClose.setOnClickListener(this);
		
		MyCustomButton btnQuery = (MyCustomButton) findViewById(R.id.btn_query);
		btnQuery.setImageResource(R.drawable.ic_action_search);
		btnQuery.setTextViewText1("查询");
		btnQuery.setOnClickListener(this);
		btnQuery.setOnTouchListener(this);
		// 设置查询标题
		if(mBusiness.mTranceType == 0x01 || mBusiness.mTranceType == 0x04) 
		{
			mTextQueryTitle.setText("查询手机账户余额");
		}
		else if(mBusiness.mTranceType == 0x02 || mBusiness.mTranceType == 0x05) 
		{
			mTextQueryTitle.setText("查询固话账户余额");
			mLayRandom.setVisibility(View.GONE);
			
		}
		else if(mBusiness.mTranceType == 0x0A || mBusiness.mTranceType == 0x0D) 
		{
			mTextQueryTitle.setText("查询小灵通账户余额");
			mLayRandom.setVisibility(View.GONE);
		
		}
		else if(mBusiness.mTranceType == 0x0B || mBusiness.mTranceType == 0x0E) 
		{
			mTextQueryTitle.setText("查询宽带ADSL账户余额");
			mLayRandom.setVisibility(View.GONE);
			
		}
		else if(mBusiness.mTranceType == 0x0C || mBusiness.mTranceType == 0x0F) 
		{
			mTextQueryTitle.setText("查询宽带LAN账户余额");
			mLayRandom.setVisibility(View.GONE);
		}
		mTimer = new CountDownTimer(180 * 1000, 1000)
		{
			
			@Override
			public void onTick(long millisUntilFinished)
			{
				String tempstr = String.format("剩余%d秒", millisUntilFinished/1000);
				mTvCountDown.setText(tempstr);
			}
			
			@Override
			public void onFinish()
			{
				mIsGetRandomOk = false;
				mTvTipRandom.setText("短信验证码已失效，请重新获取！");
				mTvCountDown.setVisibility(View.GONE);
			}
		};
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 获取验证码
		case R.id.btn_get_random:
			handleGetRandom();
			break;
			
		// 查询
		case R.id.btn_query:
			handleQuery();
			break;
			
		// 关闭对话框
		case R.id.btn_dialog_close:
			dismiss();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
	
	// 获取验证码
	private void handleGetRandom() 
	{
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取验证码...");
				boolean ret = mBusiness.bll0101(mStrTelNum);
				DialogUtil.closeProgress();
				if(ret == true)
				{
					mIsGetRandomOk = true;
					//DialogUtil.showToast("获取验证码成功", 0x00);
					mTvTipRandom.post(new Runnable()
					{
						@Override
						public void run()
						{
							mTvCountDown.setVisibility(View.VISIBLE);
							mTvTipRandom.setVisibility(View.VISIBLE);		
							mTvTipRandom.setText("获取成功，请在180秒内输入短信验证码！");
							mTimer.start();
						}
					});
				}
			}
		}.start();
	}
	
	// 校验验证码
	private void handleQuery() 
	{
		if(mLayRandom.getVisibility() == View.VISIBLE) 
		{
			if(mIsGetRandomOk == false) 
			{
				DialogUtil.showMessage("温馨提示", "请先获取验证码！");
				return;
			}
			mStrRandom = mEditRandom.getText().toString();
			if(StringUtil.isEmptyOrNull(mStrRandom)) 
			{
				DialogUtil.showMessage("温馨提示", "请输入验证码！");
				return;
			}
			if(mStrRandom.length() != 6) 
			{
				DialogUtil.showMessage("温馨提示", "请输入6位验证码！");
				return;
			}
			// 校验验证码
			new Thread()  
			{
				public void run() 
				{
					DialogUtil.showProgress("正在校验验证码...");
					boolean ret = mBusiness.bll0102(mStrTelNum, mStrRandom);
					DialogUtil.closeProgress();
					if(ret == true) 
					{
						mHandler.sendEmptyMessage(Statics.MSG_VERIFY_OK);
					}
					else
					{
						mEditRandom.post(new Runnable()
						{
							@Override
							public void run()
							{
								mEditRandom.setText("");
							}
						});
					}
				}
			}.start();
		}
		else
		{
			mHandler.sendEmptyMessage(Statics.MSG_VERIFY_OK);
		}
	}

	@Override
	public void dismiss()
	{
		if(mTimer != null)
		{
			mTimer.cancel();
		}
		super.dismiss();
	}
	
}
