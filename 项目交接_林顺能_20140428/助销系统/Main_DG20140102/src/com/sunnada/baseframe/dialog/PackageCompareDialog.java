package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.CompareAdapter;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.bean.Statics;

public class PackageCompareDialog extends Dialog implements android.view.View.OnClickListener
{
	LinearLayout	lay_content	= null;
	ImageView		iv_phone1	= null;
	TextView		tv_phone1	= null;
	TextView		tv_price1	= null;

	ImageView		iv_phone2	= null;
	TextView		tv_phone2	= null;
	TextView		tv_price2	= null;

	ImageView		iv_phone3	= null;
	TextView		tv_phone3	= null;
	TextView		tv_price3	= null;
	Context			m_context	= null;
	
	CompareAdapter  adapter     = null;
	int 			m_item_resource;
	ListView		lv_content  = null;
	List<Map<String, String>> 	mBaseData;
	List<Map<String, String>> 	mEntertainData;
	List<Map<String, String>> 	mCameraData;
	List<Map<String, String>> 	mOtherData;
	ImageView 		btn_base    = null;
	ImageView 		btn_entertainment =  null;
	ImageView 		btn_camera	= null;
	ImageView		btn_other	= null;
	View 			lay_buttons = null;

	public PackageCompareDialog(Context context)
	{
		super(context);
	}

	public PackageCompareDialog(Context context, RecommendPackage[] compareModels, boolean isMostIn)
	{
		super(context);
		this.m_context = context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_compare_phones);
			m_item_resource = R.layout.dlg_compare_item;
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_compare_phones_orange);
			m_item_resource = R.layout.dlg_compare_item_orange;
		}
		
		iv_phone1 = (ImageView) findViewById(R.id.iv_phone1);
		tv_phone1 = (TextView) findViewById(R.id.tv_phone1);
		tv_price1 = (TextView) findViewById(R.id.tv_price1);

		iv_phone2 = (ImageView) findViewById(R.id.iv_phone2);
		tv_phone2 = (TextView) findViewById(R.id.tv_phone2);
		tv_price2 = (TextView) findViewById(R.id.tv_price2);

		iv_phone3 = (ImageView) findViewById(R.id.iv_phone3);
		tv_phone3 = (TextView) findViewById(R.id.tv_phone3);
		tv_price3 = (TextView) findViewById(R.id.tv_price3);
		
		if (compareModels[0] != null)
		{
			compareModels[0].showImageView(iv_phone1);
			tv_phone1.setText(compareModels[0].getTitle());
			tv_price1.setText(compareModels[0].getmProduct().getmPriceDes());
		}
		if (compareModels[1] != null)
		{
			compareModels[1].showImageView(iv_phone2);
			tv_phone2.setText(compareModels[1].getTitle());
			tv_price2.setText(compareModels[1].getmProduct().getmPriceDes());
		}
		if (compareModels[2] != null)
		{
			compareModels[2].showImageView(iv_phone3);
			tv_phone3.setText(compareModels[2].getTitle());
			tv_price3.setText(compareModels[2].getmProduct().getmPriceDes());
		}
		
		lay_buttons = findViewById(R.id.lay_buttons);
		Button btn_dlg_close = (Button) this.findViewById(R.id.btn_dlg_close);
		btn_dlg_close.setOnClickListener(this);
		
		if(isMostIn)
		{
			PhoneModel[] compareModel = new PhoneModel[3];
			if(compareModels[0] != null)
			{
				compareModel[0] = compareModels[0].getmPhoneModel();
			}
			if(compareModels[1] != null)
			{
				compareModel[1] = compareModels[1].getmPhoneModel();
			}
			if(compareModels[2] != null)
			{
				compareModel[2] = compareModels[2].getmPhoneModel();
			}
			// 最IN机型比较手机参数信息
			initPhoneData(compareModel);
			adapter = new CompareAdapter(mBaseData, m_item_resource, context);
			lv_content.setAdapter(adapter);
		}
		else
		{
			ProductInfo[] compareModel = new ProductInfo[3];
			if(compareModels[0] != null)
			{
				compareModel[0] = compareModels[0].getmProduct();
			}
			if(compareModels[1] != null)
			{
				compareModel[1] = compareModels[1].getmProduct();
			}
			if(compareModels[2] != null)
			{
				compareModel[2] = compareModels[2].getmProduct();
			}
			// 套餐靓号信息
			initPackageData(compareModel);
		}
	}

	// 套餐靓号比较套餐详情信息
	private void initPackageData(ProductInfo[] compareModels)
	{
		lay_buttons.setVisibility(View.GONE);
		if (compareModels[0] != null)
		{
			// iv_phone1.setImageBitmap(compareModels[0].getPicBitmap());
			//compareModels[0].showInImageView(iv_phone1);
			//tv_phone1.setText(compareModels[0].getTitle());
			//tv_price1.setText(compareModels[0].getmPriceDes());
		}
		else
		{
			compareModels[0] = new ProductInfo();
		}
		if (compareModels[1] != null)
		{
			// iv_phone2.setImageBitmap(compareModels[1].getPicBitmap());
			//compareModels[1].showInImageView(iv_phone2);
			//tv_phone2.setText(compareModels[1].getTitle());
			//tv_price2.setText(compareModels[1].getmPriceDes());
		}
		else
		{
			compareModels[1] = new ProductInfo();
		}
		if (compareModels[2] != null)
		{
			// iv_phone3.setImageBitmap(compareModels[2].getPicBitmap());
			//compareModels[2].showInImageView(iv_phone3);
			//tv_phone3.setText(compareModels[2].getTitle());
			//tv_price3.setText(compareModels[2].getmPriceDes());
		}
		else
		{
			compareModels[2] = new ProductInfo();
		}
		lay_content = (LinearLayout) findViewById(R.id.lay_content);
		lv_content = (ListView) findViewById(R.id.list_content);
		
		ArrayList<Map<String,String>> packageData = new ArrayList<Map<String,String>>();
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("title", "国内语音拨打分钟数");
		map1.put("content1", compareModels[0].getmCallTime());
		map1.put("content2", compareModels[1].getmCallTime());
		map1.put("content3", compareModels[2].getmCallTime());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "国内流量");
		map1.put("content1", compareModels[0].getmFlow1());
		map1.put("content2", compareModels[1].getmFlow1());
		map1.put("content3", compareModels[2].getmFlow1());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "国内短信发送条数");
		map1.put("content1", compareModels[0].getmMsgCount());
		map1.put("content2", compareModels[1].getmMsgCount());
		map1.put("content3", compareModels[2].getmMsgCount());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "接听免费");
		map1.put("content1", compareModels[0].getmFree());
		map1.put("content2", compareModels[1].getmFree());
		map1.put("content3", compareModels[2].getmFree());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "国内语音拨打(元/分钟)");
		map1.put("content1", compareModels[0].getmCallFee());
		map1.put("content2", compareModels[1].getmCallFee());
		map1.put("content3", compareModels[2].getmCallFee());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "国内流量(元/KB)");
		map1.put("content1", compareModels[0].getmFlow2());
		map1.put("content2", compareModels[1].getmFlow2());
		map1.put("content3", compareModels[2].getmFlow2());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "国内可视电话拨打(元/分钟)");
		map1.put("content1", compareModels[0].getmVideoCall());
		map1.put("content2", compareModels[1].getmVideoCall());
		map1.put("content3", compareModels[2].getmVideoCall());
		packageData.add(map1);

		map1 = new HashMap<String, String>();
		map1.put("title", "其他业务");
		map1.put("content1", compareModels[0].getmOther());
		map1.put("content2", compareModels[1].getmOther());
		map1.put("content3", compareModels[2].getmOther());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "赠送1");
		map1.put("content1", compareModels[0].getmGift1());
		map1.put("content2", compareModels[1].getmGift1());
		map1.put("content3", compareModels[2].getmGift1());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "赠送2");
		map1.put("content1", compareModels[0].getmGift2());
		map1.put("content2", compareModels[1].getmGift2());
		map1.put("content3", compareModels[2].getmGift2());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "赠送3");
		map1.put("content1", compareModels[0].getmGift3());
		map1.put("content2", compareModels[1].getmGift3());
		map1.put("content3", compareModels[2].getmGift3());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "赠送4");
		map1.put("content1", compareModels[0].getmGift4());
		map1.put("content2", compareModels[1].getmGift4());
		map1.put("content3", compareModels[2].getmGift4());
		packageData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "赠送5");
		map1.put("content1", compareModels[0].getmGift5());
		map1.put("content2", compareModels[1].getmGift5());
		map1.put("content3", compareModels[2].getmGift5());
		packageData.add(map1);
		
		adapter = new CompareAdapter(packageData, m_item_resource, m_context);
		lv_content.setAdapter(adapter);
	}

	private void initPhoneData(PhoneModel[] compareModels) 
	{
		lay_buttons.setVisibility(View.VISIBLE);
		btn_base = (ImageView) findViewById(R.id.btn_base);
		btn_base.setOnClickListener(this);
		btn_entertainment = (ImageView) findViewById(R.id.btn_entertainment);
		btn_entertainment.setOnClickListener(this);
		btn_camera = (ImageView) findViewById(R.id.btn_camera);
		btn_camera.setOnClickListener(this);
		btn_other = (ImageView) findViewById(R.id.btn_other);
		btn_other.setOnClickListener(this);
		if (compareModels[0] != null)
		{
			//compareModels[0].showInImageView(iv_phone1);
			//tv_phone1.setText(compareModels[0].getNameDes());
			//tv_price1.setText(compareModels[0].getPriceDes());
		}
		else
		{
			compareModels[0] = new PhoneModel();
		}
		if (compareModels[1] != null)
		{
			//compareModels[1].showInImageView(iv_phone2);
			//tv_phone2.setText(compareModels[1].getNameDes());
			//tv_price2.setText(compareModels[1].getPriceDes());
		}
		else
		{
			compareModels[1] = new PhoneModel();
		}
		if (compareModels[2] != null)
		{
			//compareModels[2].showInImageView(iv_phone3);
			//tv_phone3.setText(compareModels[2].getNameDes());
			//tv_price3.setText(compareModels[2].getPriceDes());
		}
		else
		{
			compareModels[2] = new PhoneModel();
		}

		lay_content = (LinearLayout) findViewById(R.id.lay_content);
		lv_content = (ListView) findViewById(R.id.list_content);
		
		// 基础参数
		mBaseData = new ArrayList<Map<String,String>>();
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("title", "颜色");
		map1.put("content1", compareModels[0].getColor());
		map1.put("content2", compareModels[1].getColor());
		map1.put("content3", compareModels[2].getColor());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "屏幕尺寸");
		map1.put("content1", compareModels[0].getScreenSize());
		map1.put("content2", compareModels[1].getScreenSize());
		map1.put("content3", compareModels[2].getScreenSize());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "主屏参数");
		map1.put("content1", compareModels[0].getScreenParam());
		map1.put("content2", compareModels[1].getScreenParam());
		map1.put("content3", compareModels[2].getScreenParam());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "系统");
		map1.put("content1", compareModels[0].getOp());
		map1.put("content2", compareModels[1].getOp());
		map1.put("content3", compareModels[2].getOp());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "网络模式");
		map1.put("content1", compareModels[0].getNet());
		map1.put("content2", compareModels[1].getNet());
		map1.put("content3", compareModels[2].getNet());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "CPU");
		map1.put("content1", compareModels[0].getCpu());
		map1.put("content2", compareModels[1].getCpu());
		map1.put("content3", compareModels[2].getCpu());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "机身外观");
		map1.put("content1", compareModels[0].getSize());
		map1.put("content2", compareModels[1].getSize());
		map1.put("content3", compareModels[2].getSize());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "机身内存ROM");
		map1.put("content1", compareModels[0].getRom());
		map1.put("content2", compareModels[1].getRom());
		map1.put("content3", compareModels[2].getRom());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "运行内存RAM");
		map1.put("content1", compareModels[0].getRam());
		map1.put("content2", compareModels[1].getRam());
		map1.put("content3", compareModels[2].getRam());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "最大扩展内存");
		map1.put("content1", compareModels[0].getExtendRom());
		map1.put("content2", compareModels[1].getExtendRom());
		map1.put("content3", compareModels[2].getExtendRom());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "电池类型");
		map1.put("content1", compareModels[0].getBattery());
		map1.put("content2", compareModels[1].getBattery());
		map1.put("content3", compareModels[2].getBattery());
		mBaseData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "标配容量");
		map1.put("content1", compareModels[0].getBatteryStorage());
		map1.put("content2", compareModels[1].getBatteryStorage());
		map1.put("content3", compareModels[2].getBatteryStorage());
		mBaseData.add(map1);
		
		// 娱乐功能
		mEntertainData = new ArrayList<Map<String,String>>();
		map1 = new HashMap<String, String>();
		map1.put("title", "音乐格式");
		map1.put("content1", compareModels[0].getMusicFormat());
		map1.put("content2", compareModels[1].getMusicFormat());
		map1.put("content3", compareModels[2].getMusicFormat());
		mEntertainData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "视频格式");
		map1.put("content1", compareModels[0].getVedioFormat());
		map1.put("content2", compareModels[1].getVedioFormat());
		map1.put("content3", compareModels[2].getVedioFormat());
		mEntertainData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "JAVA");
		map1.put("content1", 
		compareModels[0].getJava().equals("1")? "支持"  : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content2", 
		compareModels[1].getJava().equals("1")? "支持"  : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content3",  
		compareModels[2].getJava().equals("1")? "支持"  : compareModels[0].getJava().equals("0") ? "不支持":"");
		mEntertainData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "收音机");
		map1.put("content1", 
		compareModels[0].getRadio().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content2", 
		compareModels[1].getRadio().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content3", 
		compareModels[2].getRadio().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		mEntertainData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "录音");
		map1.put("content1", 
		compareModels[0].getRecord().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content2", 
		compareModels[1].getRecord().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content3", 
		compareModels[2].getRecord().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		mEntertainData.add(map1);
		
		// 拍照功能
		mCameraData = new ArrayList<Map<String,String>>();
		map1 = new HashMap<String, String>();
		map1.put("title", "主摄像头像素");
		map1.put("content1", compareModels[0].getCamera1());
		map1.put("content2", compareModels[1].getCamera1());
		map1.put("content3", compareModels[2].getCamera1());
		mCameraData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "副摄像头像素");
		map1.put("content1", compareModels[0].getCamera2());
		map1.put("content2", compareModels[1].getCamera2());
		map1.put("content3", compareModels[2].getCamera2());
		mCameraData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "摄像头变焦模式");
		map1.put("content1", compareModels[0].getZoom());
		map1.put("content2", compareModels[1].getZoom());
		map1.put("content3", compareModels[2].getZoom());
		mCameraData.add(map1);
		
		// 其他功能
		mOtherData = new ArrayList<Map<String,String>>();
		map1 = new HashMap<String, String>();
		map1.put("title", "数据传输");
		map1.put("content1", compareModels[0].getDataTrans());
		map1.put("content2", compareModels[1].getDataTrans());
		map1.put("content3", compareModels[2].getDataTrans());
		mOtherData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "GPS");
		map1.put("content1", 
		compareModels[0].getGps().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content2", 
		compareModels[1].getGps().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		map1.put("content3", 
		compareModels[2].getGps().equals("1")? "支持" : compareModels[0].getJava().equals("0") ? "不支持":"");
		mOtherData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "理论通话时间");
		map1.put("content1", compareModels[0].getCallTime());
		map1.put("content2", compareModels[1].getCallTime());
		map1.put("content3", compareModels[2].getCallTime());
		mOtherData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "理论待机时间");
		map1.put("content1", compareModels[0].getWaitTime());
		map1.put("content2", compareModels[1].getWaitTime());
		map1.put("content3", compareModels[2].getWaitTime());
		mOtherData.add(map1);
		
		map1 = new HashMap<String, String>();
		map1.put("title", "相关配件");
		map1.put("content1", compareModels[0].getAccess());
		map1.put("content2", compareModels[1].getAccess());
		map1.put("content3", compareModels[2].getAccess());
		mOtherData.add(map1);
	}


	@Override
	public void show()
	{
		super.show();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btn_base:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base_pressed));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other));
				} 
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base_pressed_orange));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera_orange));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment_orange));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other_orange));
				}
				adapter = new CompareAdapter(mBaseData, m_item_resource, m_context);
				lv_content.setAdapter(adapter);
				break; 
				
			case R.id.btn_entertainment:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment_pressed));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other));
				} 
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base_orange));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera_orange));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment_pressed_orange));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other_orange));
				}
				adapter = new CompareAdapter(mEntertainData, m_item_resource, m_context);
				lv_content.setAdapter(adapter);
				break; 
				
			case R.id.btn_camera:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camere_pressed));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other));
				} 
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base_orange));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camere_pressed_orange));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment_orange));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other_orange));
				}
				adapter = new CompareAdapter(mCameraData, m_item_resource, m_context);
				lv_content.setAdapter(adapter);
				break; 
	
			case R.id.btn_other:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other_pressed));
				} 
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					btn_base.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_base_orange));
					btn_camera.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_camera_orange));
					btn_entertainment.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_entertainment_orange));
					btn_other.setImageDrawable(m_context.getResources().getDrawable(R.drawable.btn_other_pressed_orange));
				}
				adapter = new CompareAdapter(mOtherData, m_item_resource, m_context);
				lv_content.setAdapter(adapter);
				break; 
				
			case R.id.btn_dlg_close:
				PackageCompareDialog.this.dismiss();
				break;

			default:
				break;
		}
	}

}
