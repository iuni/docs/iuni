package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ModifySrvDialog extends Dialog implements View.OnClickListener
{
	private Business_00				mBusiness;				// 协议交互类
	private Handler					mParentHandler;			// 父消息处理器
	
	private ImageView				mImageClose;			// 关闭密码修改对话框
	private ImageView				mImageSrvModify;		// 交易密码修改
	private Button					mModifyPswd;			// 修改密码
	
	private EditText				mEditOldSrvPswd;		// 旧交易密码输入框
	private EditText				mEditNewSrvPswd;		// 新交易密码输入框
	private EditText				mEditEnsureSrvPswd;		// 新交易密码确认框
	
	public ModifySrvDialog(Context context, Business_00 business, Handler parentHandler) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_modify_srv_pswd);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_modify_srv_pswd_orange);
		}
		this.mBusiness		= business;
		this.mParentHandler = parentHandler;
		// 注册监听事件
		registerListener();
	}
	
	// 注册监听事件
	private void registerListener() 
	{
		// 关闭密码修改对话框
		mImageClose = (ImageView)findViewById(R.id.pswd_close);
		mImageClose.setOnClickListener(this);
		// 修改交易密码提示
		mImageSrvModify = (ImageView)findViewById(R.id.srv_pswd_tips);
		mImageSrvModify.setOnClickListener(this);
		// 修改密码
		mModifyPswd = (Button)findViewById(R.id.btn_modifyPswd);
		mModifyPswd.setOnClickListener(this);
		
		// 工号密码输入框
		mEditOldSrvPswd		= (EditText)findViewById(R.id.et_old_pswd);
		mEditNewSrvPswd 	= (EditText)findViewById(R.id.et_new_pswd);
		mEditEnsureSrvPswd 	= (EditText)findViewById(R.id.et_ensure_pswd);
	}
	
	// 清除编辑框
	private void clearEdit() 
	{
		mEditOldSrvPswd.setText("");
		mEditNewSrvPswd.setText("");
		mEditEnsureSrvPswd.setText("");
	}
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		// 关闭密码修改对话框
		case R.id.pswd_close:
			dismiss();
			break;
			
		// 切换到修改登陆密码
		case R.id.srv_pswd_tips:
			procSwitch();
			break;
			
		// 修改密码
		case R.id.btn_modifyPswd:
			modifyPswd();
			break;
			
		default:
			break;
		}
	}
	
	// 消息处理器
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch(message.what)
			{
			case 0x01:
				// 清除编辑框
				clearEdit();
				// 销毁
				dismiss();
				break;
				
			case 0x02:
				clearEdit();
				break;
				
			default:
				break;
			}
		}
	};
	
	// 切换到修改交易密码
	private void procSwitch() 
	{
		new Thread()
		{
			public void run() 
			{
				try
				{
					DialogUtil.showProgress("正在获取数据...");
					// 获取交易流水, 密码再次设置, 登录密码修改
					if(mBusiness.bll000A((byte)0x02, (byte)0x02)) 
					{
						mParentHandler.sendEmptyMessage(Statics.MSG_GET_PSWD_SCENE_OK);
					}
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x01);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					DialogUtil.closeProgress();
				}
			}
		}.start();
	}
	
	// 修改密码
	private boolean modifyPswd() 
	{
		// 检测旧交易密码
		final String strOldSrvPswd = mEditOldSrvPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strOldSrvPswd) || strOldSrvPswd.length() != 6) 
		{
			mEditOldSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位旧交易密码！");
			return false;
		}
		// 检测新交易密码
		final String strNewSrvPswd = mEditNewSrvPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strNewSrvPswd) || strNewSrvPswd.length() != 6) 
		{
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位新交易密码！");
			return false;
		}
		// 检测确认交易密码
		String strEnsureSrvPswd = mEditEnsureSrvPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strEnsureSrvPswd) || strEnsureSrvPswd.length() != 6) 
		{
			mEditEnsureSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位确认交易密码！");
			return false;
		}
		// 检测交易密码是否一致
		if(strNewSrvPswd.equals(strEnsureSrvPswd) == false) 
		{
			mEditNewSrvPswd.setText("");
			mEditEnsureSrvPswd.setText("");
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "两次输入的新交易密码不一致！");
			return false;
		}
		// 判断新交易密码是否过于简单
		if(InputRules.isSimple(strNewSrvPswd)) 
		{
			mEditNewSrvPswd.setText("");
			mEditEnsureSrvPswd.setText("");
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "新交易密码过于简单, 请重新设置！");
			return false;
		}
		
		Log.d("xxxx", "旧交易密码为: " + strOldSrvPswd);
		Log.d("xxxx", "新交易密码为: " + strNewSrvPswd);
		
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取交易流水号...");
				if(mBusiness.bll000A((byte)0x02, (byte)0x03) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				DialogUtil.showProgress("正在修改交易密码...");
				if(mBusiness.bll0008(strOldSrvPswd, strNewSrvPswd, null) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				else
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(Business_00.TITLE_STR, "交易密码修改成功!");
					mHandler.sendEmptyMessage(0x01);
				}
			}
		}.start();
		return true;
	}
}
