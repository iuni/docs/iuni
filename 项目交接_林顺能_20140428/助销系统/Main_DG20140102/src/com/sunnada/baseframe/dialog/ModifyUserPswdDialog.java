package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ModifyUserPswdDialog extends Dialog implements View.OnClickListener
{
	private Business_00				mBusiness;				// Э�齻����
	private Handler					mParentHandler;			// ����Ϣ������
	
	private ImageView				mImageClose;			// �ر������޸ĶԻ���
	private ImageView				mImageSrvModify;		// ���������޸�
	private Button					mModifyPswd;			// �޸�����
	
	private EditText				mEditOldUserPswd;		// �¹������������
	private EditText				mEditNewUserPswd;		// �¹������������
	private EditText				mEditEnsureUserPswd;	// �¹�������ȷ�Ͽ�
	
	public ModifyUserPswdDialog(Context context, Business_00 business, Handler parentHandler) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_modify_user_pswd);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_modify_user_pswd_orange);
		}
		this.mBusiness		= business;
		this.mParentHandler = parentHandler;
		// ע������¼�
		registerListener();
	}
	
	// ע������¼�
	private void registerListener() 
	{
		// �ر������޸ĶԻ���
		mImageClose = (ImageView)findViewById(R.id.pswd_close);
		mImageClose.setOnClickListener(this);
		// �޸Ľ���������ʾ
		mImageSrvModify = (ImageView)findViewById(R.id.user_pswd_tips);
		mImageSrvModify.setOnClickListener(this);
		// �޸�����
		mModifyPswd = (Button)findViewById(R.id.btn_modifyPswd);
		mModifyPswd.setOnClickListener(this);
		
		// �������������
		mEditOldUserPswd	= (EditText)findViewById(R.id.et_old_pswd);
		mEditNewUserPswd 	= (EditText)findViewById(R.id.et_new_pswd);
		mEditEnsureUserPswd = (EditText)findViewById(R.id.et_ensure_pswd);
	}
	
	// ����༭��
	private void clearEdit() 
	{
		mEditOldUserPswd.setText("");
		mEditNewUserPswd.setText("");
		mEditEnsureUserPswd.setText("");
	}
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		// �ر������޸ĶԻ���
		case R.id.pswd_close:
			dismiss();
			break;
			
		// �л����޸Ľ�������
		case R.id.user_pswd_tips:
			procSwitch();
			break;
			
		// �޸�����
		case R.id.btn_modifyPswd:
			modifyPswd();
			break;
			
		default:
			break;
		}
	}
	
	// ��Ϣ������
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch(message.what)
			{
			case 0x01:
				// ����༭��
				clearEdit();
				// ����
				dismiss();
				break;
				
			case 0x02:
				clearEdit();
				break;
				
			default:
				break;
			}
		}
	};
	
	// �л����޸Ľ�������
	private void procSwitch() 
	{
		new Thread()
		{
			public void run() 
			{
				try
				{
					DialogUtil.showProgress("���ڻ�ȡ����...");
					// ��ȡ������ˮ, �����ٴ�����, ���������޸�
					if(mBusiness.bll000A((byte)0x02, (byte)0x03)) 
					{
						mParentHandler.sendEmptyMessage(Statics.MSG_GET_PSWD_SCENE_OK);
					}
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x01);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					DialogUtil.closeProgress();
				}
			}
		}.start();
	}
	
	// �޸�����
	private boolean modifyPswd() 
	{
		// ���ɵ�¼����
		final String strOldUserPswd = mEditOldUserPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strOldUserPswd) || strOldUserPswd.length() != 6) 
		{
			mEditOldUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "������6λ�ɵ�¼���룡");
			return false;
		}
		// ����µ�¼����
		final String strNewUserPswd = mEditNewUserPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strNewUserPswd) || strNewUserPswd.length() != 6) 
		{
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "������6λ�µ�¼���룡");
			return false;
		}
		// ���ȷ�ϵ�¼����
		String strEnsureUserPswd = mEditEnsureUserPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strEnsureUserPswd) || strEnsureUserPswd.length() != 6) 
		{
			mEditEnsureUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "������6λȷ�ϵ�¼���룡");
			return false;
		}
		// ����¼�����Ƿ�һ��
		if(strNewUserPswd.equals(strEnsureUserPswd) == false) 
		{
			mEditNewUserPswd.setText("");
			mEditEnsureUserPswd.setText("");
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "����������µ�¼���벻һ�£�");
			return false;
		}
		// �ж��������Ƿ���ڼ�
		if(InputRules.isSimple(strNewUserPswd)) 
		{
			mEditNewUserPswd.setText("");
			mEditEnsureUserPswd.setText("");
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "�µ�¼������ڼ�, ���������ã�");
			return false;
		}
		
		Log.d("xxxx", "�ɵ�½����Ϊ: " + strOldUserPswd);
		Log.d("xxxx", "�µ�½����Ϊ: " + strNewUserPswd);
		
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("���ڻ�ȡ������ˮ��...");
				if(mBusiness.bll000A((byte)0x02, (byte)0x02) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				DialogUtil.showProgress("�����޸ĵ�¼����...");
				if(mBusiness.bll0008(strOldUserPswd, strNewUserPswd, null) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				else
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(Business_00.TITLE_STR, "��¼�����޸ĳɹ�!");
					mHandler.sendEmptyMessage(0x01);
				}
			}
		}.start();
		return true;
	}
}
