package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.FrameActivity;

import android.os.Message;

public class CallBackChildHome implements ICallBack
{
	public CallBackChildHome()
	{
	}
	
	@Override
	public void back() 
	{
		Message msg = Message.obtain();
		msg.what = FrameActivity.FINISH;
		FrameActivity.mHandler.sendMessage(msg);
	}

	@Override
	public void cancel() 
	{
	}

	@Override
	public void dismiss() 
	{
		
	}
}
