package com.sunnada.baseframe.dialog;

import android.app.Activity;
import android.content.Context;

public class CallBackChild implements ICallBack
{
	private Context context;
	
	public CallBackChild(Context context)
	{
		this.context = context;
	}
	
	@Override
	public void back() 
	{
		((Activity)context).finish();
	}

	@Override
	public void cancel() 
	{
	}

	@Override
	public void dismiss() 
	{
		
	}
}
