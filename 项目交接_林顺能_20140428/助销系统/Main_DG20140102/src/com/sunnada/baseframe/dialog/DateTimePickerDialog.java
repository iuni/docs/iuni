package com.sunnada.baseframe.dialog;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

// 自定义日期控件
public class DateTimePickerDialog extends AlertDialog
{
	private DatePicker 					datePicker = null;					// 日期控件
	private TimePicker 					timePicker = null;					// 时间控件
	private OnDateTimeSelectListener 	onDateTimeSelectListener = null;	// 时间选择监听器
	private OnCancelListener 			onCancelListener = null;			// 取消监听
	
	protected DateTimePickerDialog(Context context)
	{
		super(context);
		this.init();
	}
	
	// 初始化日期控件
	private void init() 
	{
		datePicker = new DatePicker(getContext());
		timePicker = new TimePicker(getContext());
		timePicker.setIs24HourView(true);
		
		LinearLayout layout = new LinearLayout(getContext());
		layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.addView(datePicker);
		layout.addView(timePicker);
		
		this.setTitle("请选择");
		this.setCalendar(Calendar.getInstance());
		this.setButton("确定", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// 这里需要把焦点去除，如何编辑框选中的情况下，datapicker的值是不会变化的
				datePicker.clearFocus();
				timePicker.clearFocus();
				
				Calendar c = Calendar.getInstance();
				c.set(Calendar.YEAR, 			datePicker.getYear());
				c.set(Calendar.MONTH, 			datePicker.getMonth());
				c.set(Calendar.DAY_OF_MONTH, 	datePicker.getDayOfMonth());
				c.set(Calendar.HOUR_OF_DAY, 	timePicker.getCurrentHour());
				c.set(Calendar.MINUTE, 			timePicker.getCurrentMinute());
				
				System.out.println(datePicker.getDayOfMonth());
				
				if(onDateTimeSelectListener != null)
				{
					onDateTimeSelectListener.onDateTimeSelected(c);
				}
			}
		});
		
		this.setButton2("取消", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if(onCancelListener != null)
				{
					onCancelListener.onCancel(DateTimePickerDialog.this);
				}
			}
		});
		
		this.setView(layout);
	}
	
	// 设定时间
	public void setDate(Date date)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		this.setCalendar(c);
	}
	
	// 设定时间
	public void setTime(long time)
	{
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		this.setCalendar(c);
	}
	
	// 设定日期
	public void setCalendar(Calendar c)
	{
		this.setYear(c.get(Calendar.YEAR));
		this.setMonth(c.get(Calendar.MONTH));
		this.setDayOfMonth(c.get(Calendar.DAY_OF_MONTH));
		this.setYear(c.get(Calendar.YEAR));
		this.setHour(c.get(Calendar.HOUR_OF_DAY));
		this.setMinute(c.get(Calendar.MINUTE));
	}
	
	// 设定日
	public void setDayOfMonth(int dayOfMonth)
	{
		datePicker.init(datePicker.getYear(), datePicker.getMonth(), dayOfMonth, null);
	}
	
	// 设定月
	public void setMonth(int month)
	{
		datePicker.init(datePicker.getYear(), month, datePicker.getDayOfMonth(), null);
	}
	
	// 设定年
	public void setYear(int year)
	{
		datePicker.init(year, datePicker.getMonth(), datePicker.getDayOfMonth(), null);
	}
	
	// 设定分钟
	public void setMinute(int minute)
	{
		timePicker.setCurrentMinute(minute);
	}
	
	// 设定小时
	public void setHour(int hour)
	{
		timePicker.setCurrentHour(hour);
	}
	
	// 是否显示年
	public void setYearVisible(boolean isVisible)
	{
		try
		{
			if(isVisible)
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(0).setVisibility(View.VISIBLE);
			}
			else
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(0).setVisibility(View.GONE);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 是否显示月
	public void setMonthVisible(boolean isVisible)
	{
		/*
		DatePicker dp = findDatePicker((ViewGroup) this.getWindow().getDecorView());
		if (dp != null) 
		{
		    ((ViewGroup) dp.getChildAt(0)).getChildAt(0).setVisibility(View.GONE);
		} 
		*/
		if(datePicker.getChildAt(0) != null)
		{
			if(isVisible)
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(1).setVisibility(View.VISIBLE);
			}
			else
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(1).setVisibility(View.GONE);
			}
		}
	}
	
	// 是否显示日
	public void setDateVisible(boolean isVisible) 
	{
		/*
		ViewGroup view = (ViewGroup) datePicker.getChildAt(0);
		while(view.getChildCount()>0&&view.getChildAt(0) instanceof ViewGroup)
		{
			if(view.getChildAt(2)instanceof NumberPicker)
			{
				view = (ViewGroup) view.getChildAt(2);
				break;
			}
			else
			{
				view = (ViewGroup) view.getChildAt(0);
			}
		}
		
		if(v) 
		{
			view.setVisibility(View.VISIBLE);
		}
		else
		{
			view.setVisibility(View.GONE);
		}
		return;
		*/
		Class<? extends DatePicker> c = datePicker.getClass();
		Field f;
		try 
		{
			if(getAndroidSDKVersion() > 4.0)
			{
				f = c.getDeclaredField("mDaySpinner");
				f.setAccessible(true );  
				LinearLayout l= (LinearLayout)f.get(datePicker);   
				if(isVisible)
				{
					l.setVisibility(View.VISIBLE);
				}
				else
				{
					l.setVisibility(View.GONE);
				}
			}
			else
			{
				f = c.getDeclaredField("mDayPicker");
				f.setAccessible(true );  
				LinearLayout l= (LinearLayout)f.get(datePicker);   
				if(isVisible)
				{
					l.setVisibility(View.VISIBLE);
				}
				else
				{
					l.setVisibility(View.GONE);
				}
			}
		}
		catch (SecurityException e) 
		{
			e.printStackTrace();
		}
		catch (NoSuchFieldException e) 
		{
			e.printStackTrace();
		} catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		}  
		
		/*
		try
		{
			if(isVisible) 
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(2).setVisibility(View.VISIBLE);
			}
			else
			{
				((ViewGroup) datePicker.getChildAt(0)).getChildAt(2).setVisibility(View.GONE);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		*/
	}
	
	public static float getAndroidSDKVersion()
	{
        float version = (float) 0.0;
        try 
        {
            version = (float)Integer.valueOf(android.os.Build.VERSION.SDK);
        }
        catch (NumberFormatException e) 
        {
            e.printStackTrace();
        }
        return version;
	}
	
	// 是否显示时
	public void setHourVisible(boolean isVisible)
	{
		if(isVisible)
		{
			 ((ViewGroup) timePicker.getChildAt(0)).getChildAt(0).setVisibility(View.VISIBLE);
		}
		else
		{
			 ((ViewGroup) timePicker.getChildAt(0)).getChildAt(0).setVisibility(View.GONE);
		}
	}
	
	// 是否显示分
	public void setMinuteVisible(boolean isVisible)
	{
		if(isVisible)
		{
			 ((ViewGroup) timePicker.getChildAt(0)).getChildAt(1).setVisibility(View.VISIBLE);
		}
		else
		{
			 ((ViewGroup) timePicker.getChildAt(0)).getChildAt(1).setVisibility(View.GONE);
		}
	}
	

	@Override
	public void setOnCancelListener(OnCancelListener listener)
	{
		this.onCancelListener = listener;
		super.setOnCancelListener(listener);
	}
	
	public OnDateTimeSelectListener getOnDateTimeSelectListener()
	{
		return onDateTimeSelectListener;
	}

	public void setOnDateTimeSelectListener(OnDateTimeSelectListener onDateTimeSelectListener)
	{
		this.onDateTimeSelectListener = onDateTimeSelectListener;
	}

	public DatePicker getDatePicker()
	{
		return datePicker;
	}

	public void setDatePicker(DatePicker datePicker)
	{
		this.datePicker = datePicker;
	}

	public TimePicker getTimePicker()
	{
		return timePicker;
	}

	public void setTimePicker(TimePicker timePicker)
	{
		this.timePicker = timePicker;
	}
	
	/**
	 * 时间选择监听器
	 * @author qfq
	 *
	 */
	public interface OnDateTimeSelectListener
	{
		public void onDateTimeSelected(Calendar c);
	}
	
	/**
	 * 获取年月控件实例
	 * @param context
	 * @return
	 */
	public static DateTimePickerDialog getYearMonthDialog(Context context)
	{
		DateTimePickerDialog dateTimePickerDialog = new DateTimePickerDialog(context);
		dateTimePickerDialog.setYearVisible(true);
		dateTimePickerDialog.setMonthVisible(true);
		dateTimePickerDialog.setDateVisible(false);
		dateTimePickerDialog.setHourVisible(false);
		dateTimePickerDialog.setMinuteVisible(false);
		
		return dateTimePickerDialog;
	}
	
	/**
	 * 获取年月控件实例
	 * @param context
	 * @return
	 */
	public static DateTimePickerDialog getYearMonthDateDialog(Context context)
	{
		DateTimePickerDialog dateTimePickerDialog = new DateTimePickerDialog(context);
		dateTimePickerDialog.setYearVisible(true);
		dateTimePickerDialog.setMonthVisible(true);
		dateTimePickerDialog.setDateVisible(true);
		dateTimePickerDialog.setHourVisible(false);
		dateTimePickerDialog.setMinuteVisible(false);
		//timePicker.setVisibility(View.GONE);
		return dateTimePickerDialog;
	}
	
}
