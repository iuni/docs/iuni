package com.sunnada.baseframe.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;

public class MyProgressDialog extends Dialog
{
	private static TextView		mTextMsgStr	= null;
	
	protected MyProgressDialog(Context context, String msg) 
	{
		super(context, R.style.CustomProgressDialog);
		setContentView(R.layout.dlg_myprogress);
		setCanceledOnTouchOutside(false);
		
		// 初始化控件
		initViews();
	}
	
	private MyProgressDialog(Context context, int theme, String msg)
	{
		super(context, theme);
		setContentView(R.layout.dlg_myprogress);
		setCanceledOnTouchOutside(false);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews()
	{
		mTextMsgStr = (TextView) findViewById(R.id.tv_msg);
	}
	
	public void setText(String msgStr) 
	{
		mTextMsgStr.setText(msgStr);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void dismiss() 
	{
		super.dismiss();
	}
}
