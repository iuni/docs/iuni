package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.InvoiceData;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0F;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class InvoicePrintDialog extends Dialog implements View.OnClickListener, OnTouchListener 
{
	private Business_0F                 mBussiness0F;
	
	private ImageView					mDlgClose;
	private TextView                    mTvInvoiceNum;
	private TextView                    mTvInvoiceCode;
	private TextView                    mTvTime;
	private TextView                    mTvNum;
	private TextView                    mTvMoney;
	private Button                      mBtnInvoiceValidate;
	private MyCustomButton              mBtnCancel;
	private MyCustomButton              mBtnPrint;
	
	private Handler                     mHandler;
	private int                         mSelectedPos;
	
	public InvoicePrintDialog(Context context, Business_0F bussiness0F, Handler handler, int pos) 
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mBussiness0F = bussiness0F;
		mHandler = handler;
		mSelectedPos = pos;
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_invoice_print_orange);
		}
		else
		{
			setContentView(R.layout.dlg_invoice_print);
		}
		
		// 初始化数据
		initData();
		// 初始化控件
		initView();
		registerListener();
	}

	// 初始化控件
	public void initView()
	{	 
		// 关闭对话框按钮
		mDlgClose = (ImageView) findViewById(R.id.iv_detail_close);
		// 机打发票号
		mTvInvoiceNum = (TextView) findViewById(R.id.tv_invoicenum);
		// 发票代码
		mTvInvoiceCode = (TextView) findViewById(R.id.tv_invoice_code);
		// 交易时间
		mTvTime = (TextView) findViewById(R.id.tv_time);
		// 交易号码
		mTvNum = (TextView) findViewById(R.id.tv_num);
		// 交易金额
		mTvMoney = (TextView) findViewById(R.id.tv_money);
		// 调整到调整发票号界面
		mBtnInvoiceValidate = (Button) findViewById(R.id.btn_invoice_validate);
		// 取消按钮
		mBtnCancel = (MyCustomButton) findViewById(R.id.btn_cancel);
		mBtnCancel.setTextViewText1("取消");
		mBtnCancel.setOnTouchListener(this);
		// 打印按钮
		mBtnPrint = (MyCustomButton) findViewById(R.id.btn_print);
		mBtnPrint.setTextViewText1("打印");
		mBtnPrint.setOnTouchListener(this);
	}
	
	// 初始化数据
	public void initData()
	{
		if(mBussiness0F.mInvoiceNum != null)
		{
			mTvInvoiceNum.setText(mBussiness0F.mInvoiceNum);
		}
		
		if(mBussiness0F.mBatchNum != null)
		{
			mTvInvoiceCode.setText(mBussiness0F.mBatchNum);
		}
		
		if((mBussiness0F.mInvoiceDatas != null) && mBussiness0F.mInvoiceDatas.size() > 0)
		{
			InvoiceData invoiceData = mBussiness0F.mInvoiceDatas.get(mSelectedPos);
			mTvTime.setText(invoiceData.getBusiTime());
			mTvNum.setText(mBussiness0F.mQueryNum);
			mTvMoney.setText(invoiceData.getMoney());
		}
	}
	
	// 注册监听事件
	private void registerListener() 
	{
		// 关闭对话框
		mDlgClose.setOnClickListener(this);
		// 取消按钮
		mBtnCancel.setOnClickListener(this);
		// 打印按钮
		mBtnPrint.setOnClickListener(this);
		// 调整发票号按钮
		mBtnInvoiceValidate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
		
		// 关闭对话框
		dismiss();
		switch (v.getId()) 
		{
	
		case R.id.iv_detail_close:
			break;
			
		case R.id.btn_cancel:
			break;
			
		case R.id.btn_print:
			Message message = Message.obtain();
			message.what = Statics.MSG_INVOICE_PRINT;
			mHandler.sendMessage(message);
			break;
		
		case R.id.btn_invoice_validate:
			Message msg = Message.obtain();
			msg.what = Statics.MSG_GOTO_INVOICE_VALIDATE;
			mHandler.sendMessage(msg);
			break;
			
		default:
			break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		((MyCustomButton) v).onTouch(event);
		return false;
	}
}
