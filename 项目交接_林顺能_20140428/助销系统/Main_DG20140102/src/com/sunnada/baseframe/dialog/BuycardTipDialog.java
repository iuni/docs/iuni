package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class BuycardTipDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private Button					mBtnClose;					// 关闭对话框
	private TextView				mTextFaceValue;				// 电子卡面额
	private TextView				mTextCount;
	private MyCustomButton			mBtnBuycardCancel;			// 取消购买电子卡
	private MyCustomButton			mBtnBuycardOk;				// 购买电子卡
	
	private String					mStrEcardFaceValue;			// 电子卡面值
	private String					mStrEcardCount;				// 电子卡张数
	private Handler					mHandler;					// 消息Handler
	
	public BuycardTipDialog(Context context, String strFaceValue, String strCount, Handler handler) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_buycard_tip);
		setCanceledOnTouchOutside(false);
		
		mStrEcardFaceValue 	= strFaceValue;
		mStrEcardCount 		= strCount;
		mHandler 			= handler;
		
		// 初始化控件
		initViews();
	}

	// 初始化控件
	private void initViews() 
	{
		// 关闭对话框
		mBtnClose = (Button) findViewById(R.id.btn_dialog_close);
		mBtnClose.setOnClickListener(this);
		// 
		mTextFaceValue = (TextView) findViewById(R.id.tv_buycard_facevalue);
		mTextFaceValue.setText(mStrEcardFaceValue);
		mTextCount = (TextView) findViewById(R.id.tv_buycard_count);
		mTextCount.setText(mStrEcardCount);
		
		// 不购买
		mBtnBuycardCancel = (MyCustomButton) findViewById(R.id.btn_buycard_cancel);
		mBtnBuycardCancel.setImageResource(R.drawable.btn_reselect);
		mBtnBuycardCancel.setTextViewText1("不购买");
		mBtnBuycardCancel.setOnClickListener(this);
		mBtnBuycardCancel.setOnTouchListener(this);
		// 确定购买
		mBtnBuycardOk = (MyCustomButton) findViewById(R.id.btn_buycard_ok);
		mBtnBuycardOk.setImageResource(R.drawable.btn_reselect);
		mBtnBuycardOk.setTextViewText1("确定购买");
		mBtnBuycardOk.setOnClickListener(this);
		mBtnBuycardOk.setOnTouchListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 关闭对话框
		case R.id.btn_dialog_close:
			dismiss();
			break;
			
		// 取消购买
		case R.id.btn_buycard_cancel:
			mHandler.sendEmptyMessage(Statics.MSG_BUYCARD_CANCEL);
			dismiss();
			break;
			
		// 确定购买
		case R.id.btn_buycard_ok:
			mHandler.sendEmptyMessage(Statics.MSG_BUYCARD_OK);
			dismiss();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
}
