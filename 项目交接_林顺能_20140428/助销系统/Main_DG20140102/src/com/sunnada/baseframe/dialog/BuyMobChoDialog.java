package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;

public class BuyMobChoDialog extends Dialog implements android.view.View.OnClickListener 
{
	String strMobTypeArr[]={"Apple"," 三星","化为","索尼"," 三星","化为","索尼"," 三星","化为","索尼"," 三星","化为","索尼"," 三星","化为","索尼"};
	String strSystemArr[]={"不限"," Ios","Android","WindowsPhone"," 非智能手机","其他只能手机"};

	LayoutInflater inflater;
	View dialogView;
	LinearLayout lay_mobchotype;
	LinearLayout lay_system;
	Context m_context = null;	
	Button bt_close = null;
	Button bt_cpu1,bt_cpu2,bt_cpu3,bt_cpu4;
	TextView pricetv1,pricetv2,pricetv3,pricetv4,pricetv5,pricetv6,pricetv7,pricetv8;
	View pricevi1, pricevi2, pricevi3, pricevi4, pricevi5, pricevi6, pricevi7;
	TextView screentv1,screentv2,screentv3,screentv4,screentv5,screentv6,screentv7,screentv8;
	View screenvi1,screenvi2,screenvi3,screenvi4,screenvi5,screenvi6,screenvi7;
    List<Button> btmobcholist=new ArrayList<Button>();
    List<Button> btsystemlist=new ArrayList<Button>();  
    Button btn_reelect;
    Button btn_select;
    
    View oldPriceView;   
    View oldSceenView; 
    TextView oldPriceTv;
    TextView oldSceenTv;
    Button oldbt_CPU;
    Button oldbt_mobType;
    Button oldbt_system;
    
    int []arrSelcet={-1,-1,-1,-1,-1};

	public BuyMobChoDialog(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_buymbcho);
		m_context = context;
	    dlgInit();
	
	
	}
	
	
	public void dlgInit(){
		bt_close = (Button) findViewById(R.id.bt_close);
		bt_close.setOnClickListener(this);		
		btn_reelect = (Button) findViewById(R.id.btn_reelect);
		btn_reelect.setOnClickListener(this);
		btn_select = (Button) findViewById(R.id.btn_select);
		btn_select.setOnClickListener(this);
//		inflater = (LayoutInflater) m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
//		dialogView= inflater.inflate(R.layout.dlg_buymbcho, null);
		
		lay_mobchotype=(LinearLayout)findViewById(R.id.lay_mobchotype);
		mobTypebtInit();
		
		lay_system=(LinearLayout)findViewById(R.id.lay_system);
		systembtInit();
		
		cpuInit();	
		screenInit();
		priceInit();
		}
	

	public void screenInit(){
		screentv1=(TextView)findViewById(R.id.screentv1);
		screentv2=(TextView)findViewById(R.id.screentv2);
		screentv3=(TextView)findViewById(R.id.screentv3);
		screentv4=(TextView)findViewById(R.id.screentv4);
		screentv5=(TextView)findViewById(R.id.screentv5);
		screentv6=(TextView)findViewById(R.id.screentv6);
		screentv7=(TextView)findViewById(R.id.screentv7);
		screentv8=(TextView)findViewById(R.id.screentv8);
		screenvi1=(View)findViewById(R.id.screenvi1);
		screenvi1.setOnClickListener(this);
		screenvi2=(View)findViewById(R.id.screenvi2);
		screenvi2.setOnClickListener(this);
		screenvi3=(View)findViewById(R.id.screenvi3);
		screenvi3.setOnClickListener(this);
		screenvi4=(View)findViewById(R.id.screenvi4);
		screenvi4.setOnClickListener(this);
		screenvi5=(View)findViewById(R.id.screenvi5);
		screenvi5.setOnClickListener(this);
		screenvi6=(View)findViewById(R.id.screenvi6);
		screenvi6.setOnClickListener(this);
		screenvi7=(View)findViewById(R.id.screenvi7);
		screenvi7.setOnClickListener(this);
		
	}
	
	
	public void priceInit(){
		pricetv1=(TextView)findViewById(R.id.pricetv1);
		pricetv2=(TextView)findViewById(R.id.pricetv2);
		pricetv3=(TextView)findViewById(R.id.pricetv3);
		pricetv4=(TextView)findViewById(R.id.pricetv4);
		pricetv5=(TextView)findViewById(R.id.pricetv5);
		pricetv6=(TextView)findViewById(R.id.pricetv6);
		pricetv7=(TextView)findViewById(R.id.pricetv7);
		pricetv8=(TextView)findViewById(R.id.pricetv8);
		pricevi1=(View)findViewById(R.id.pricevi1);
		pricevi1.setOnClickListener(this);
		pricevi2=(View)findViewById(R.id.pricevi2);
		pricevi2.setOnClickListener(this);
		pricevi3=(View)findViewById(R.id.pricevi3);
		pricevi3.setOnClickListener(this);
		pricevi4=(View)findViewById(R.id.pricevi4);
		pricevi4.setOnClickListener(this);
		pricevi5=(View)findViewById(R.id.pricevi5);
		pricevi5.setOnClickListener(this);
		pricevi6=(View)findViewById(R.id.pricevi6);
		pricevi6.setOnClickListener(this);
		pricevi7=(View)findViewById(R.id.pricevi7);
		pricevi7.setOnClickListener(this);
	}
	public void cpuInit(){
		bt_cpu1=(Button)findViewById(R.id.bt_cpu1);
		bt_cpu1.setOnClickListener(this);		
		bt_cpu2=(Button)findViewById(R.id.bt_cpu2);
		bt_cpu2.setOnClickListener(this);		
		bt_cpu3=(Button)findViewById(R.id.bt_cpu3);
		bt_cpu3.setOnClickListener(this);		
		bt_cpu4=(Button)findViewById(R.id.bt_cpu4);
		bt_cpu4.setOnClickListener(this);		
	}
	
	
	public void mobTypebtInit(){
			String []arr=getStrMobTypeArr();		
		    for(int i=0;i<arr.length;i++){
			Button button=new Button(m_context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.leftMargin=10;
			button.setLayoutParams(lp);
			button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			button.setText(arr[i]);
			button.setTextSize(22);	
			button.setId(i);
			button.setTextColor(Color.parseColor("#FFFFFF"));
			lay_mobchotype.addView(button);
			btmobcholist.add(button);
			button.setOnClickListener(new View.OnClickListener()
			{				
				@Override
				public void onClick(View arg0)
				{
					oldbt_mobType=(Button) arg0;
					arrSelcet[0]=oldbt_mobType.getId();
					selectButton(btmobcholist,arg0);
					
				}
			});
		}
	}
	public void systembtInit(){
		String []arr=getStrSystemArr();		
	    for(int i=0;i<arr.length;i++){
		Button button=new Button(m_context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.leftMargin=10;
		button.setLayoutParams(lp);
		button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		button.setText(arr[i]);		
		button.setTextSize(22);		
		button.setId(i);
		button.setTextColor(Color.parseColor("#FFFFFF"));
		lay_system.addView(button);
		btsystemlist.add(button);
		button.setOnClickListener(new View.OnClickListener()
		{				
			@Override
			public void onClick(View arg0)
			{
				oldbt_system=(Button) arg0;
				arrSelcet[1]=oldbt_system.getId();
				selectButton(btsystemlist,arg0);
			}
		});
	}
}
	
	
	
	//按钮选中后改变状态
	public void selectButton(List<Button> crrlist,View arg0){
		arg0.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
	
        for(int i=0;i<crrlist.size();i++){
	       Button button=crrlist.get(i);
	       if(button.getId()!=arg0.getId()){
	    	   button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
	       }
          }    
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_close:
			this.dismiss();
			break;
		case R.id.bt_cpu1:
			bt_cpu1.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			bt_cpu2.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu3.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu4.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			oldbt_CPU=bt_cpu1;
			arrSelcet[4]=0;
			break;
		case R.id.bt_cpu2:
			bt_cpu1.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu2.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			bt_cpu3.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu4.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			oldbt_CPU=bt_cpu2;
			arrSelcet[4]=1;
			break;
		case R.id.bt_cpu3:
			bt_cpu1.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu2.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu3.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			bt_cpu4.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			oldbt_CPU=bt_cpu3;
			arrSelcet[4]=2;
			break;
		case R.id.bt_cpu4:
			bt_cpu1.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu2.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu3.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			bt_cpu4.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			oldbt_CPU=bt_cpu4;
			arrSelcet[4]=3;
			break;
		case R.id.screenvi1:
			selectSceenView(v);
			screentv1.setTextColor(m_context.getResources().getColor(R.color.red));
			oldSceenTv=screentv1;
			arrSelcet[3]=0;
			break;
		case R.id.screenvi2:
			selectSceenView(v);
			screentv2.setTextColor(m_context.getResources().getColor(R.color.red));
			oldSceenTv=screentv2;
			arrSelcet[3]=1;
			break; 
		case R.id.screenvi3:
			selectSceenView(v);
			screentv3.setTextColor(Color.parseColor("#FF0000"));
			oldSceenTv=screentv3;
			arrSelcet[3]=2;
			break;
		case R.id.screenvi4:
			selectSceenView(v);
			screentv4.setTextColor(Color.parseColor("#FF0000"));
			oldSceenTv=screentv4;
			arrSelcet[3]=3;
			break;
		case R.id.screenvi5:
			selectSceenView(v);
			screentv5.setTextColor(Color.parseColor("#FF0000"));
			oldSceenTv=screentv5;
			arrSelcet[3]=4;
			break;
		case R.id.screenvi6:
			selectSceenView(v);
			screentv6.setTextColor(Color.parseColor("#FF0000"));
			oldSceenTv=screentv6;	
			arrSelcet[3]=5;
			break;
		case R.id.screenvi7:
			selectSceenView(v);
			screentv7.setTextColor(Color.parseColor("#FF0000"));
			oldSceenTv=screentv7;
			arrSelcet[3]=6;
			break;
		
		case R.id.pricevi1:
			selectPriceView(v);
			pricetv1.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv1;
			arrSelcet[2]=0;
			break;
		case R.id.pricevi2:
			selectPriceView(v);
			pricetv2.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv2;
			arrSelcet[2]=1;
			break;
		case R.id.pricevi3:
			selectPriceView(v);
			pricetv3.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv3;
			arrSelcet[2]=2;
			break;
		case R.id.pricevi4:
			selectPriceView(v);
			pricetv4.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv4;
			arrSelcet[2]=3;
			break;
		case R.id.pricevi5:
			selectPriceView(v);
			pricetv5.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv5;
			arrSelcet[2]=4;
			break;
		case R.id.pricevi6:
			selectPriceView(v);
			pricetv6.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv6;
			arrSelcet[2]=5;
			break;
		case R.id.pricevi7:
			selectPriceView(v);
			pricetv7.setTextColor(Color.parseColor("#FF0000"));
			oldPriceTv=pricetv7;
			arrSelcet[2]=6;
			break;
		case R.id.btn_reelect:
			cleanSelect();
			break;
		case R.id.btn_select:
			getArrSelcet();
			this.cancel();
			break;
		default:
			break;
		}
	}
	public void selectPriceView(View view){
	
		if(oldPriceView!=null){
		oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldPriceTv!=null){
			oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
		}
		oldPriceView=view;
		view.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumlvse));
	}
	
	
	public void selectSceenView(View view){
	
		if(oldSceenView!=null){
		oldSceenView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldSceenTv!=null){
			oldSceenTv.setTextColor(Color.parseColor("#00A6EC"));
		}
		oldSceenView=view;
		view.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumlvse));
	}
	
	
	public int[] getArrSelcet(){
 System.out.println(arrSelcet[0]);
 System.out.println(arrSelcet[1]);
 System.out.println(arrSelcet[2]);
 System.out.println(arrSelcet[3]);
 System.out.println(arrSelcet[4]);
	    return arrSelcet;
	}
	
	public void cleanSelect(){
		if(oldSceenView!=null){
			oldSceenView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldSceenTv!=null){
		oldSceenTv.setTextColor(Color.parseColor("#00A6EC"));
		}
		if(oldPriceView!=null){
		oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldPriceTv!=null){
		oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
		}
		if(oldbt_CPU!=null){
			oldbt_CPU.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldbt_system!=null){
			oldbt_system.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		if(oldbt_mobType!=null){
			oldbt_mobType.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
		}
		arrSelcet[0]=-1;
		arrSelcet[1]=-1;
		arrSelcet[2]=-1;
		arrSelcet[3]=-1;
		arrSelcet[4]=-1;
	}
	public String[] getStrMobTypeArr(){
		return strMobTypeArr;
	}
	public void setStrMobTypeArr(String []arr){
		strMobTypeArr=arr;
		}
	public String[] getStrSystemArr(){
		return strSystemArr;
	}
	public void setStrSystemArr(String []arr){
		strSystemArr=arr;
		}

}
