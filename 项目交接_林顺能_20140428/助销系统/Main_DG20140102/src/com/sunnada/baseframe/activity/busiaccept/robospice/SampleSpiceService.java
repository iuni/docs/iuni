package com.sunnada.baseframe.activity.busiaccept.robospice;

import android.app.Application;

import com.octo.android.robospice.GoogleHttpClientSpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.googlehttpclient.json.JacksonObjectPersisterFactory;

/**
 * Simple service
 * 
 * @author sni
 * 
 */
public class SampleSpiceService extends GoogleHttpClientSpiceService 
{
    @Override
    public CacheManager createCacheManager(Application application) 
    {
    	// init
    	CacheManager cacheManager = new CacheManager();
        JacksonObjectPersisterFactory jacksonObjectPersisterFactory;
		try {
			jacksonObjectPersisterFactory = new JacksonObjectPersisterFactory(application);
			cacheManager.addPersister(jacksonObjectPersisterFactory);
		} 
		catch (CacheCreationException e) 
		{
			e.printStackTrace();
		}
        return cacheManager;
    }
}
