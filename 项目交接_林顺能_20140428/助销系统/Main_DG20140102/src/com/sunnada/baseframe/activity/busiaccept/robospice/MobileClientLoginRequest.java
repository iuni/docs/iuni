package com.sunnada.baseframe.activity.busiaccept.robospice;

import java.util.ArrayList;
import java.util.List;

import com.sunnada.baseframe.activity.busiaccept.base.DES;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;

public class MobileClientLoginRequest extends BaseRequest 
{
	private String 		agentId;
	private String 		agentPasswd;
	private String 		sendSMSflag = "0";
	private String 		lac;
	private String 		ci;

	public MobileClientLoginRequest(String agentId, String agentPasswd, String lac, String ci) 
	{
		super();
		
		this.agentId 		= agentId;
		this.agentPasswd 	= agentPasswd;
		this.lac			= lac;
		this.ci				= ci;
	}

	public MobileClientLoginRequest(String agentId, String agentPasswd, String sendSMSflag, String lac, String ci) 
	{
		super();
		
		this.agentId 		= agentId;
		this.agentPasswd 	= agentPasswd;
		this.sendSMSflag 	= sendSMSflag;
		this.lac 			= lac;
		this.ci				= ci;
	}
	
	@Override
	protected String getMethod() 
	{
		return "mobileClientLogin";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "mobileClientLoginResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(SEND_SMS_FLAG, 	sendSMSflag));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(AGENT_PASSWORD, 	DES.encryptDES(agentPasswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		DES.encryptDES("01", mEncryptKey)));
		params.add(new Param(VERSION_CODE, 		DES.encryptDES("1." + DeviceUtils.getVersionCode(), mEncryptKey)));
		params.add(new Param(VERSION_NAME, 		DES.encryptDES(DeviceUtils.getVersionName(), mEncryptKey)));
		params.add(new Param(LAC, 				lac));
		params.add(new Param(CI, 				ci));
		return params;
	}
}
