package com.sunnada.baseframe.activity.busiaccept;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.UIHandler;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.activity.busiaccept.base.DES;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;
import com.sunnada.baseframe.activity.busiaccept.base.Global;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.activity.busiaccept.robospice.BaseRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.MobileClientLogOffRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.TelCheckRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.TelGetRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.UploadCertificateInfoRequest;
import com.sunnada.baseframe.bean.IdentifyMsg;
import com.sunnada.baseframe.bean.KeyTextBean;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.identify.IdentifyScan;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.StringUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class BusiPhoneRegisterMain extends BaseActivity implements OnClickListener, ViewGetter
{
	private static final String		TITLE_STR				= "实名制返档温馨提示";
	private static final int		BARCODE_RECOGNISE		= 0xF001;
	private static final int		IDCARD_SCAN_DONE		= 0xF002;
	
	private static final int		MSG_GET_TELNO_SUCC		= 0x01;
	private static final int		MSG_GET_TELNO_FAIL		= 0x02;
	private static final int		MSG_GET_TELNO_TIMEOUT	= 0x03;
	
	private static final int		MSG_CHECK_TEL_SUC		= 0x05;
	private static final int 		MSG_CHECK_SUC_OLD 		= 0x06;
	private static final int		MSG_CHECK_TEL_FAIL		= 0x07;
	private static final int		MSG_CHECK_TEL_TIMEOUT	= 0x08;
	
	private static final int 		MSG_UPLOAD_SUC 			= 0x09;
	private static final int 		MSG_UPLOAD_FAIL 		= 0x0A;
	private static final int		MSG_UPLOAD_TIMEOUT		= 0x0B;
	private static final int		MSG_LOGIN_OFF			= 0x0C;
	
	// 第一步-号码登记
	private LinearLayout			mLinearBacktoBusiAccept	= null;
	private LinearLayout			mLinearRegisterStep1	= null;				// 返档第一步
	private ButtonGroupUtil			mBtnGroupUtil 			= null;				// 按钮组
	private LinearLayout			mLinearInputTel 		= null;				// 号码返档布局
	private LinearLayout			mLinearInputIccid		= null;				// ICCID返档布局
	private EditText				mEditTel;									// 手机号码输入框
	private EditText				mEditIccid;									// ICCID号码输入框
	private EditText				mEditTelLast4;								// 手机号码尾4位输入框
	private boolean					mIsInputTelHide;							// 号码返档布局是否是隐藏状态
	
	private String					mStrTel;
	private TelCheckRequest 		mTelCheckRequest;
	private String					mCommunicationID;							// 业务时序
	private String					mUploadType;								// 证件图片上传开关
	private String					mCertifyFlag			= "1111111";		// 证件类型开关
	
	private String					mStrIccid;									// 手机卡ICCID
	private String					mStrTelLast4;								// 手机号码末4位
	private TelGetRequest			mTelGetRequest			= null;				// 号码获取请求
	
	// 第二步-录入信息
	private LinearLayout			mLinearRegisterStep2	= null;				// 返档第二步
	//private Spinner				mSpCustType				= null;				// 证件类型下拉框
	private TextView               	mTvCustType;
	// private ImageView            mIvCustType;
	private LinearLayout            mLayCust;
	private LinearLayout            mLayLvSp;
	private ListView              	mLvCustType;
	
	private LinearLayout			mLinearScanIdcard		= null;				// 扫描身份证的布局
	private Button					mBtnScanIdcard			= null;				// 扫描身份证的按钮
	private TextView				mTextTel				= null;				// 返档号码
	private EditText				mEditUserName			= null;				// 客户姓名
	private ImageView				mImageUserName			= null;
	private EditText				mEditUserNo				= null;				// 证件号码
	private ImageView				mImageUserNo			= null;
	private EditText				mEditUserAddr			= null;				// 证件地址
	private ImageView				mImageUserAddr			= null;
	private EditText				mEditContactName		= null;				// 联系人
	//private ImageView				mImageContactName		= null;
	private EditText				mEditContactTel			= null;				// 联系电话
	//private ImageView				mImageContactTel		= null;
	private EditText				mEditContactAddr		= null;				// 通信地址
	//private ImageView				mImageContactAddr		= null;
	
	private String					mStrCustType			= null;				// 证件类型
	private IdentifyMsg				mIdentifyMsg			= null;				// 身份证信息
	private String					mStrUserName			= null;
	private String					mStrUserNo				= null;
	private String					mStrUserAddr			= null;
	private String					mStrContactName			= null;
	private String					mStrContactTel			= null;
	private String					mStrContactAddr			= null;
	
	private ArrayAdapter<KeyTextBean>   mAdapter            = null;
	private boolean                 mIsIvCustHited          = false;
	private List<KeyTextBean>       mCustTypeList           = new ArrayList<KeyTextBean>();
	
	// 第三步-信息确认
	private LinearLayout			mLinearRegisterStep3	= null;				// 返档第三步
	private TextView				mTextEnsureTel			= null;
	private TextView				mTextEnsureUserName		= null;
	private TextView				mTextEnsureCustType		= null;
	private TextView				mTextEnsureUserNo		= null;
	private TextView				mTextEnsureUserAddr		= null;
	private TextView				mTextEnsureContactName	= null;
	private TextView				mTextEnsureContactTel	= null;
	private TextView				mTextEnsureContactAddr	= null;
	private TextView				mTvLoginOff				= null;				// 变更发展人
	
	private UploadCertificateInfoRequest	mUploadRequest	= null;
	
	private Business_00				mBusiness				= null;
	
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			DialogUtil.closeProgress();
			switch (message.what)
			{
			// 获取号码成功，进行号码校验
			case MSG_GET_TELNO_SUCC:
				DialogUtil.showProgress("正在校验号码...");
				mTelCheckRequest = new TelCheckRequest(App.getAgentId(), mStrTel, App.getDesKey(), "02");
				executeRequest(mHandler, MSG_CHECK_TEL_SUC, BaseRequest.PROGRESS_TIMEOUT, mTelCheckRequest, new TelCheckRequestListener());
				break;
				
			// 获取手机号码失败
			case MSG_GET_TELNO_FAIL:
				String desc1 = (String)message.obj;
				if(StringUtil.isEmptyOrNull(desc1) || "anyType{}".equals(desc1)) 
				{
					DialogUtil.MsgBox(TITLE_STR, "服务器响应超时！该手机卡未验证通过，请勿销售。（序号：1997）");
				} 
				else 
				{
					DialogUtil.MsgBox(TITLE_STR, desc1+"");
				}
				
				if(mIsInputTelHide)
				{
					// 清空ICCID输入框和手机号码尾4位输入框
					mEditIccid.setText("");
					mEditTelLast4.setText("");
				}
				break;
				
			// 获取手机号码超时
			case MSG_GET_TELNO_TIMEOUT:
				DialogUtil.MsgBox(TITLE_STR, "服务器响应超时！该手机卡未验证通过，请勿销售。（序号：19003）");
				if(mIsInputTelHide) 
				{
					// 清空ICCID输入框和手机号码尾4位输入框
					mEditIccid.setText("");
					mEditTelLast4.setText("");
				}
				break;
				
			// 号码校验成功
			case MSG_CHECK_TEL_SUC:
				initCustTypeListData();
				// 初始化证件类型
				// initCustType();
				// 显示第二页
				showPage(0x02);
				// 显示返档号码
				mTextTel.setText(mStrTel);
				break;
			
			// 老用户
			case MSG_CHECK_SUC_OLD:
				back(
						new ICallBack() 
						{
							@Override
							public void back() 
							{
								mEditTel.setText("");
							}
							
							@Override
							public void cancel() 
							{
							}
		
							@Override
							public void dismiss() 
							{
							}
						}, 
						TITLE_STR, 
						mStrTel + "用户为老用户，请用户登录中国联通网上营业厅或手机营业厅或发送彩信至10010进行补登记。", 
						"确定", null, true, false, false);
				break;
			
			// 号码校验失败
			case MSG_CHECK_TEL_FAIL:
				String desc2 = (String)message.obj;
				if(StringUtil.isEmptyOrNull(desc2)  || "anyType{}".equals(desc2)) 
				{
					DialogUtil.MsgBox(TITLE_STR, "服务器响应超时！该手机号未验证通过，请勿销售。（序号：29003）");
				}
				else 
				{
					DialogUtil.MsgBox(TITLE_STR, desc2+"");
				}
				break;
			
			// 号码校验超时
			case MSG_CHECK_TEL_TIMEOUT:
				DialogUtil.MsgBox(TITLE_STR, "服务器响应超时！该手机号未验证通过，请勿销售。（序号：29003）");
				break;
				
			// 扫描身份证成功
			case IDCARD_SCAN_DONE:
				mEditUserName.setText(mIdentifyMsg.name);
				mEditUserNo.setText(Global.checkIDNo(mIdentifyMsg.identify_no));
				mEditUserAddr.setText(mIdentifyMsg.address);
				break;
				
			// 证件信息上传成功
			case MSG_UPLOAD_SUC:
				back(
						new ICallBack()
						{
							@Override
							public void back() 
							{
								// 输入框重置
								resetInput();
								// 显示第一页
								showPage(0x01);
								// 提交登记信息到助销平台
								uploadUserInfo(0x01);
							}
		
							@Override
							public void cancel() 
							{
							}
		
							@Override
							public void dismiss() 
							{
							}
						}, 
						TITLE_STR, 
						(String)message.obj, 
						"确定", null, true, false, false);
				break;
				
			// 证件信息上传失败
			case MSG_UPLOAD_FAIL:
				String desc3 = (String)message.obj;
				if(StringUtil.isEmptyOrNull(desc3) || "anyType{}".equals(desc3)) 
				{
					desc3 = "服务器响应超时！返档失败，请勿销售。（序号：39003）";
				}
				DialogUtil.showMessage(TITLE_STR, 
						desc3, 
						"确定", 
						new View.OnClickListener() 
						{
							public void onClick(View v) 
							{
								// 输入框重置
								resetInput();
								// 显示第一页
								showPage(0x01);
								// 提交登记信息到助销平台
								uploadUserInfo(0x00);
							}
						});
				break;
				
			// 证件信息上传超时
			case MSG_UPLOAD_TIMEOUT:
				System.out.println("========CONFIRM_SUBMIT=========");
				DialogUtil.MsgBox(TITLE_STR, "服务器响应超时！返档失败，请勿销售。（序号：39003）");
				break;
				
			// 注销
			case MSG_LOGIN_OFF:
				MobileClientLogOffRequest request = new MobileClientLogOffRequest(App.getAgentId());
				mSpiceManager.execute(request, null);	
				App.setAccessToken(null);
				Intent intent = new Intent(BusiPhoneRegisterMain.this, BusiPhoneRegisterLogin.class);
				intent.putExtra(Statics.IS_AUTO_LOGIN, false);
				startActivity(intent);
				finish();
				break;
				
			default:
				break;
			}
		}
	};
	
	// 提交登记信息到助销平台
	private void uploadUserInfo(final int szResult) 
	{
		new Thread()
		{
			public void run()
			{
				DialogUtil.showProgress("正在处理, 请稍候...");
				Map<String, String> result = mUploadRequest.getResult();
				// 提交登记信息
				mBusiness.bll000D((byte)szResult, mStrTel, result.get(BaseRequest.TRADE_STATE), result.get(BaseRequest.DESCRIPTION));
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_busiaccept_phone_register);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_busiaccept_phone_register_orange);
		}
		App.init(this);
		mBusiness = new Business_00(this, equipmentService, socketService, dataBaseService);
		// 初始化布局
		initViews();
		// 显示第一页
		showPage(0x01);
		hideInputTel(false);
	}
	
	// 初始化布局
	private void initViews() 
	{
		// 初始化第一页
		initStep1();
		// 初始化第2页
		initStep2();
		// 初始化第三步
		initStep3();
	}
	
	// 初始化第一页
	private void initStep1()
	{
		// 返回到业务受理
		mLinearBacktoBusiAccept = (LinearLayout) findViewById(R.id.linear_backto_busiaccept);
		mLinearBacktoBusiAccept.setOnClickListener(this);
		
		mTvLoginOff = (TextView) findViewById(R.id.tv_login_off);
		mTvLoginOff.setOnClickListener(this);
		mTvLoginOff.setText("< 变更发展人");
		
		// 返档第一页
		mLinearRegisterStep1 = (LinearLayout) findViewById(R.id.register_step1);
		mLinearInputTel = (LinearLayout) findViewById(R.id.input_tel);
		mLinearInputIccid = (LinearLayout) findViewById(R.id.input_iccid);
		mEditTel = (EditText) findViewById(R.id.et_tel);
		mEditIccid = (EditText) findViewById(R.id.et_iccid);
		mEditTelLast4 = (EditText) findViewById(R.id.et_tel_last4);
		
		try 
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBtnGroupUtil = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_phone_register, R.id.btn_iccid_register}, 
						new String[] {"手机号码登记", "ICCID登记"}, 
						R.drawable.btn_tip_select,
						R.drawable.btn_tip_selected);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnGroupUtil = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_phone_register, R.id.btn_iccid_register}, 
						new String[] {"手机号码登记", "ICCID登记"}, 
						R.drawable.btn_tip_select_orange,
						R.drawable.btn_tip_selected_orange);
			}
			
			mBtnGroupUtil.setSelected(0);
			mBtnGroupUtil.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					if(newId == R.id.btn_phone_register)
					{
						hideInputTel(false);
					}
					else if(newId == R.id.btn_iccid_register)
					{
						hideInputTel(true);
					}
				}
			});
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		// 扫描ICCID
		Button btnScanIccid = (Button) findViewById(R.id.btn_scan_iccid);
		btnScanIccid.setOnClickListener(this);
		// 下一步
		Button btnPage1Next = (Button) findViewById(R.id.btn_page1_next);
		btnPage1Next.setOnClickListener(this);
	}
	
	// 隐藏号码返档布局
	private void hideInputTel(boolean bHide)
	{
		if(bHide == false)
		{
			mIsInputTelHide = false;
			// 显示号码返档布局
			mLinearInputTel.setVisibility(View.VISIBLE);
			// 隐藏ICCID返档布局
			mLinearInputIccid.setVisibility(View.GONE);
			// 清空手机号码输入框
			mEditTel.setText("");
		}
		else
		{
			mIsInputTelHide = true;
			// 隐藏号码返档布局
			mLinearInputTel.setVisibility(View.GONE);
			// 显示ICCID返档布局
			mLinearInputIccid.setVisibility(View.VISIBLE);
			// 清空ICCID输入框和手机号码尾4位输入框
			mEditIccid.setText("");
			mEditTelLast4.setText("");
		}
	}
	
	// 初始化第2页
	private void initStep2()
	{
		mLinearRegisterStep2 = (LinearLayout) findViewById(R.id.register_step2);
		//mSpCustType			= (Spinner) findViewById(R.id.sp_custtype);
		mTvCustType         = (TextView) findViewById(R.id.tv_custtype);
		// mIvCustType         = (ImageView) findViewById(R.id.iv_custtype);
		mLayCust            = (LinearLayout) findViewById(R.id.lay_cust);
		mLayLvSp            = (LinearLayout) findViewById(R.id.lay_lv_sp);
		mLvCustType         = (ListView) findViewById(R.id.lv_custtype);
		
		mLinearScanIdcard	= (LinearLayout) findViewById(R.id.layout_idcard);
		mBtnScanIdcard		= (Button) findViewById(R.id.btn_scan_idcard);
		mTextTel			= (TextView) findViewById(R.id.tv_tel);
		mEditUserName		= (EditText) findViewById(R.id.et_name);
		mImageUserName		= (ImageView) findViewById(R.id.iv_name);
		mEditUserNo			= (EditText) findViewById(R.id.et_no);
		mImageUserNo		= (ImageView) findViewById(R.id.iv_no);
		mEditUserAddr		= (EditText) findViewById(R.id.et_addr);
		mImageUserAddr		= (ImageView) findViewById(R.id.iv_addr);
		mEditContactName	= (EditText) findViewById(R.id.et_contact_name);
		mEditContactTel		= (EditText) findViewById(R.id.et_contact_tel);
		mEditContactAddr	= (EditText) findViewById(R.id.et_contact_addr);
		
		// mIvCustType.setOnClickListener(this);
		mLayCust.setOnClickListener(this);
		// mLayLvSp.setOnClickListener(this);
		
		mLvCustType.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) 
			{
				mIsIvCustHited = false;
				// 获取证件类型
				KeyTextBean keyTextBean = (KeyTextBean) mLvCustType.getItemAtPosition(position);
				mStrCustType = keyTextBean.getKey().trim();
				mTvCustType.setText(keyTextBean.getText().trim());
				mLvCustType.setVisibility(View.GONE);
				// setEditText(mStrCustType);
				if("01".equals(mStrCustType))
				{ 
					mLinearScanIdcard.setVisibility(View.VISIBLE);
				}
				else
				{
					mLinearScanIdcard.setVisibility(View.GONE);
				}
				// 重置第二步的输入
				resetStepTwoInput();
			}
		});
		
		// 初始化证件类型选择框
		// initCustType();
		// 显示返档号码
		mTextTel.setText(mStrTel);
		
		// 姓名
		mEditUserName.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				mImageUserName.setVisibility(View.VISIBLE);
				if(!mStrCustType.equals("07"))
				{
					if (StringUtil.isEmptyOrNull(s.toString()) || !isNameValid(mStrCustType, s.toString())) 
					{
						
						mImageUserName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mImageUserName.setImageResource(R.drawable.ic_correct);
					}
				}
				else
				{
					if (StringUtil.isEmptyOrNull(s.toString()) || !isNameValid(mStrCustType, s.toString())||s.toString().length()<3) 
					{
						
						mImageUserName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mImageUserName.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		// 证件号
		mEditUserNo.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				mImageUserNo.setVisibility(View.VISIBLE);
				if (StringUtil.isEmptyOrNull(s.toString()) || !isNoValid(mStrCustType, s.toString())) 
				{
					mImageUserNo.setImageResource(R.drawable.ic_error);
				}
				else
				{
					mImageUserNo.setImageResource(R.drawable.ic_correct);
				}
			}

					@Override
					public void afterTextChanged(Editable s)
					{
						int pos = s.length()-1;
						if(s.length()>0)
						{
							char mCustType = s.charAt(pos);
							// 身份证
							if(mStrCustType.endsWith("01")||mStrCustType.endsWith("02"))
							{
								if(mCustType!='X'&& mCustType!='0'&& mCustType!='1'&& mCustType!='2'&& mCustType!='3'&& mCustType!='4'&& 
								   mCustType!='5'&&mCustType!='6'&&mCustType!='7' &&mCustType!='8'&& mCustType!='9'|| s.length()>18)
								{
									s.delete(pos, pos+1);
								}
							}
							// 港澳居民来往内地通行证
							if(mStrCustType.endsWith("05"))
							{
								if(mCustType!='H' && mCustType!='M' && mCustType!='0'&& mCustType!='1' && mCustType!='2' && mCustType!='3' && mCustType!='4' 
								   && mCustType!='5' && mCustType!='6' && mCustType!='7'&& mCustType!='8' && mCustType!='9'&& mCustType!='(' && mCustType!=')'|| s.length()>11)
								{
									s.delete(pos, pos+1);
								}
							}
							// 台湾居民来往内地通行证
							if(mStrCustType.endsWith("06"))
							{
								if(s.length()>13)
								{
									s.delete(pos, pos+1);
								}
							}
						}
					}
				});
		
		// 地址
		mEditUserAddr.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				mImageUserAddr.setVisibility(View.VISIBLE);
				if (StringUtil.isEmptyOrNull(s.toString()) || isAddressValid(mStrCustType, s.toString()) != 0)
				{
					mImageUserAddr.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mImageUserAddr.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 扫描身份证
		mBtnScanIdcard.setOnClickListener(this);
		// 上一步
		Button btnPage2Prev = (Button) findViewById(R.id.btn_page2_prev);
		btnPage2Prev.setOnClickListener(this);
		// 下一步
		Button btnPage2Next = (Button) findViewById(R.id.btn_page2_next);
		btnPage2Next.setOnClickListener(this);
	}
	
	public void initCustTypeListData()
	{
		String[] names  = getResources().getStringArray(R.array.type_card_name);
		String[] values = getResources().getStringArray(R.array.type_card_value);
		
		mCustTypeList.clear();
		// 计算有效的证件类型
		for(int i=0; i<mCertifyFlag.length(); i++)
		{
			// 当前证件类型有效
			if("1".equals(mCertifyFlag.substring(i, i+1)))
			{
				KeyTextBean bean = new KeyTextBean(values[i], names[i]);
				mCustTypeList.add(bean);
			}
		}
		mTvCustType.setText(mCustTypeList.get(0).getText().trim());
		mStrCustType = mCustTypeList.get(0).getKey().trim();
		mLinearScanIdcard.setVisibility(View.VISIBLE);
	}
	
	// 筛选有效的证件类型
	private void initCustType() 
	{
		if(mIsIvCustHited == false)
		{
			mLvCustType.setVisibility(View.VISIBLE);
			mLayLvSp.setVisibility(View.VISIBLE);
			mAdapter = new ArrayAdapter<KeyTextBean>(
					BusiPhoneRegisterMain.this,
					R.layout.lay_register_busilist_item, R.id.tv_text, mCustTypeList);
			mLvCustType.setAdapter(mAdapter);
			mLvCustType.invalidate();
			mIsIvCustHited = true;
		}
		else
		{
			mLvCustType.setVisibility(View.GONE);
			mLayLvSp.setVisibility(View.GONE);
			mIsIvCustHited = false;
		}
	}
	
	// 判断姓名是否合法
	private boolean isNameValid(String type, String val)
	{
		if (!"07".equals(type)) 
		{
			return (val.matches("[\u4E00-\u9FA5.·()（）]+") && InputRules.getChineseCount(val) >= 2);
		}
		else 
		{
			try 
			{
				// 判断是否有SQL字符
				if(InputRules.hasSQLKey(val, true))
				{
					return false;
				}
				return val.getBytes("GBK").length >= 3 && !val.matches("\\d*");
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}
	}
	
	// 判断证件号码是否合法
	private boolean isNoValid(String type, String val) 
	{
		if ("01".equals(type) || "02".equals(type)) 
		{
			/*
			if(val.length() < 18)
			{
				return false;
			}
			if(checkIDEndTime(val) == false)
			{
				return false;
			}
			return true;
			*/
			//return val.matches("[0-9]{17}[0-9Xx]{1}");
			return InputRules.check18IdentifyID(val);
		} 
		else if ("05".equals(type)) 
		{
			return val.matches("^[H,M](\\d{8}|\\d{10})");
		} 
		else if ("06".equals(type)) 
		{
			/*
			if(val.length() == 11)
			{
				char c = val.charAt(10);
				System.out.println("c :" + c);
				if(c < 'A' ||(c >'Z'&& c <'a'|| c > 'z'))
				{
					return false;
				}
			}
			*/
			return val.matches("(\\d{10}[0-9a-zA-Z])|(\\d{10}[(][0-9a-zA-Z][)])|(\\d{10}[（][0-9a-zA-Z][）])|(\\d{8})");
			//return val.matches("(\\d{10}[0-9a-zA-Z])|(\\d{8})|");
		} 
		else if ("03".equals(type) || "04".equals(type))
		{
			return val.matches("[0-9a-zA-Z]{6,}");
		}
		else if("07".equals(type))
		{
			// 判断是否有SQL字符
			if(InputRules.hasSQLKey(val, true))
			{
				return false;
			}
			return val.matches("[0-9a-zA-Z]{6,}");
		}
		return false;
	}
	
	/*
	private boolean isPattern(String str, String pattern)
	{
		// /^[-|+]?\\d*([.]\\d{0,2})?$
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.find();
	}
	*/
	
	// 判断地址是否合法
	private int isAddressValid(String type, String val) 
	{
		// 防SQL注入
		if(InputRules.hasSQLKey(val, true))
		{
			return 1;
		}
		// 
		if ("01".equals(type) || "02".equals(type)) 
		{
			if(InputRules.getChineseCount(val) < 8)
			{
				return 2;
			}
			return 0;
		} 
		else if ("03".equals(type) || "04".equals(type) || "05".equals(type)) 
		{
			if(InputRules.getChineseCount(val) < 8)
			{
				return 2;
			}
			return 0;
		}
		else if ("06".equals(type)) 
		{
			if(InputRules.getChineseCount(val) < 3)
			{
				return 2;
			}
			return 0;
		}
		else if ("07".equals(type)) 
		{
			if(InputRules.getChineseCount(val) < 2)
			{
				return 2;
			}
			return 0;
		}
		return 2;
	}
	
	// 初始化第三页
	private void initStep3()
	{
		mLinearRegisterStep3    = (LinearLayout) findViewById(R.id.register_step3);
		mTextEnsureTel			= (TextView) findViewById(R.id.tv_ensure_tel);
		mTextEnsureUserName		= (TextView) findViewById(R.id.tv_ensure_name);
		mTextEnsureCustType		= (TextView) findViewById(R.id.tv_ensure_custtype);
		mTextEnsureUserNo		= (TextView) findViewById(R.id.tv_ensure_no);
		mTextEnsureUserAddr		= (TextView) findViewById(R.id.tv_ensure_addr);
		mTextEnsureContactName	= (TextView) findViewById(R.id.tv_ensure_contact_name);
		mTextEnsureContactTel	= (TextView) findViewById(R.id.tv_ensure_contact_tel);
		mTextEnsureContactAddr	= (TextView) findViewById(R.id.tv_ensure_contact_addr);
		
		// 上一步
		Button btnPage3Prev = (Button) findViewById(R.id.btn_page3_prev);
		btnPage3Prev.setOnClickListener(this);
		// 提交
		Button btnPage3Submit = (Button) findViewById(R.id.btn_page3_submit);
		btnPage3Submit.setOnClickListener(this);
	}
	
	// 刷新第三页的内容
	private void refreshPage3()
	{
		mTextEnsureTel.setText(mStrTel);
		mTextEnsureUserName.setText(mStrUserName);
		mTextEnsureCustType.setText(mTvCustType.getText().toString());
		mTextEnsureUserNo.setText(mStrUserNo);
		mTextEnsureUserAddr.setText(mStrUserAddr);
		mTextEnsureContactName.setText(mStrContactName);
		mTextEnsureContactTel.setText(mStrContactTel);
		mTextEnsureContactAddr.setText(mStrContactAddr);
	}
	
	// 显示当前页
	private void showPage(int nPageIdx)
	{
		if(nPageIdx == 0x01)
		{
			mLinearRegisterStep1.setVisibility(View.VISIBLE);
			mLinearRegisterStep2.setVisibility(View.GONE);
			mLinearRegisterStep3.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x02)
		{
			mLinearRegisterStep1.setVisibility(View.GONE);
			mLinearRegisterStep2.setVisibility(View.VISIBLE);
			mLinearRegisterStep3.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x03)
		{
			mLinearRegisterStep1.setVisibility(View.GONE);
			mLinearRegisterStep2.setVisibility(View.GONE);
			mLinearRegisterStep3.setVisibility(View.VISIBLE);
		}
	}
	
	// 输入框重置
	private void resetInput()
	{
		// 第一步
		mEditTel.setText("");
		mEditIccid.setText("");
		mEditTelLast4.setText("");
		// 第二步
		mTextTel.setText("");
		mEditUserName.setText("");
		mEditUserNo.setText("");
		mEditUserAddr.setText("");
		mEditContactName.setText("");
		mEditContactTel.setText("");
		mEditContactAddr.setText("");
	}
	
	// 重置第二步输入
	private void resetStepTwoInput()
	{
		mEditUserName.setText("");
		mEditUserNo.setText("");
		mEditUserAddr.setText("");
		mEditContactName.setText("");
		mEditContactTel.setText("");
		mEditContactAddr.setText("");
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		App.init(this);
		mSpiceManager.start(this);
	}
	
	@Override
	public void onStop() 
	{
		super.onStop();
		mSpiceManager.shouldStop();
	}
	
	@Override
	protected void onDestroy()
	{
		File file = new File(Statics.PATH_IMAGE + "/test.jpg");
		if(file.exists())
		{
			file.delete();
			Log.e("", "用户信息照片已删除...");
		}
		super.onDestroy();
	}

	// 处理点击事件
	@Override
	public void onClick(View v) 
	{
		/*
		if(ButtonUtil.isFastDoubleClick(v.getId(), Global.CLICK_TIME))
		{
			return;
		}
		*/
		switch(v.getId())
		{
		// 返回到业务办理
		case R.id.linear_backto_busiaccept:
			backToHome();
			break;
			
		// 注销
		case R.id.tv_login_off:
			loginOff();
			break;
		 
		case R.id.lay_cust:
			initCustType();
			break;
			
		// 扫描ICCID
		case R.id.btn_scan_iccid:
			Intent intent = new Intent(BusiPhoneRegisterMain.this, CaptureActivity.class);
			startActivityForResult(intent, BARCODE_RECOGNISE);
			break;
			
		// 号码登记下一步
		case R.id.btn_page1_next:
			procStep1();
			break;
			
		// 扫描身份证
		case R.id.btn_scan_idcard:
			procIdcardScan(Statics.PATH_IMAGE + "/test.jpg");
			break;
			
		// 第二步上一步
		case R.id.btn_page2_prev:
			showPage(0x01);
			break;
			
		// 第二步下一步
		case R.id.btn_page2_next:
			// 获取用户输入
			if(getUserInput())
			{
				// 显示确认界面
				showPage(0x03);
				// 初始化第三页
				refreshPage3();
			}
			break;
			
		// 第三步上一步
		case R.id.btn_page3_prev:
			showPage(0x02);
			break;
			
		// 第三步提交
		case R.id.btn_page3_submit:
			procStep3();
			break;
			
		default:
			break;
		}
	}
	
	// 注销
	private void loginOff()
	{
		DialogUtil.MsgBox(TITLE_STR, "确定要变更发展人重新登录吗？", "确定", MSG_LOGIN_OFF, "取消", -1, -1, mHandler);
	}

	// 回退
	private void backToHome()
	{
		DialogUtil.MsgBox(TITLE_STR, 
				"是否要退出实名制返档业务?",
				"确定",
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						// 注销实名制返档
						MobileClientLogOffRequest request = new MobileClientLogOffRequest(App.getAgentId());
						mSpiceManager.execute(request, null);
						// 
						finish();
					}
				},	"取消", null, null);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToHome();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// Activity回调
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		//
		if(requestCode == BARCODE_RECOGNISE)
		{
			switch(resultCode)
			{
				case RESULT_OK:
					Log.i("", "一维码识别成功");
					if(UIHandler.mBarCodeStr.trim().length() != 19)
					{
						back(null, TITLE_STR, "手机卡条形码扫描失败，请重新扫描或选择手动输入！", "确定", null, false, false, false);
					}
					else 
					{
						mEditIccid.setText(UIHandler.mBarCodeStr);
						mEditIccid.setSelection(UIHandler.mBarCodeStr.length());
					}
					break;
					
				case RESULT_CANCELED:
					Log.i("", "一维码识别取消");
					break;
					
				default:
					break;
			}
		}
	}
	
	// 第一步处理
	private void procStep1()
	{
		if(mIsInputTelHide == false)
		{
			// 手机号码返档
			procStep1Phone();
		}
		else
		{
			// ICCID返档
			procStep1Iccid();
		}
	}
	
	// 校验号码
	private void procStep1Phone()
	{
		mStrTel = mEditTel.getText().toString().trim();
		// 判断号码是否为空
		if(StringUtil.isEmptyOrNull(mStrTel)) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入手机号！");
			return;
		}
		// 判断号码长度
		if(mStrTel.length() != 11) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入11位手机号码！");
			return;
		}
		// 判断号码里面是否是纯数字
		byte[] bytes = mStrTel.getBytes();
		for(int i=0; i<11; i++)
		{
			if(bytes[i] < 0x30 || bytes[i] > 0x39)
			{
				DialogUtil.MsgBox(TITLE_STR, "您输入的号码有误，请重新输入！");
				return;
			}
		}
		// 非联通号码
		if(Global.isUnicomNumber(mStrTel) == false) 
		{
			DialogUtil.MsgBox(TITLE_STR, "非联通号码，请重新输入！");
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在校验号码...");
		mTelCheckRequest = new TelCheckRequest(App.getAgentId(), mStrTel, App.getDesKey(), "01");
		executeRequest(mHandler, MSG_CHECK_TEL_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mTelCheckRequest, new TelCheckRequestListener());
	}
	
	public final class TelCheckRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			DialogUtil.closeProgress();
			// 
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL).sendToTarget();
			}
			else
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				DialogUtil.closeProgress();
				// 
				Map<String, String> result = mTelCheckRequest.getResult();
				// 检测公告
				Global.checkMail(BusiPhoneRegisterMain.this, result);
				// 检测交易状态
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if(BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 业务时序
					mCommunicationID = result.get(BaseRequest.COMMUNICATION_ID);
					// 证件照片上传开关
					String type = result.get(BaseRequest.UPLOAD_TYPE);
					Log.e("", "uploadType: " + mUploadType);
					mUploadType = DES.decryptDES(type, App.getDesKey());
					// 证件类型开关
					mCertifyFlag = result.get(BaseRequest.CERTIFY_FLAG);
					if(StringUtil.isEmptyOrNull(mCertifyFlag))
					{
						mCertifyFlag = "1111111";
					}
					Log.e("", "certifyFlag: " + mCertifyFlag);
					// 
					mHandler.obtainMessage(MSG_CHECK_TEL_SUC).sendToTarget();
				} 
				//else if("0001".equals(tradeState)) 
				//{
				//	mHandler.sendEmptyMessage(MSG_CHECK_SUC_OLD);
				//} 
				else 
				{
					// 获取错误指示
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHECK_TEL_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				DialogUtil.closeProgress();
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
			}
		}
	}
	
	// 通过ICCID号码和手机号码尾4位获取手机号码
	private void procStep1Iccid()
	{
		// 校验ICCID号码
		mStrIccid = mEditIccid.getText().toString().trim();
		if(StringUtil.isEmptyOrNull(mStrIccid))
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入19位ICCID号码！");
			return;
		}
		if(mStrIccid.length() < 19)
		{
			DialogUtil.MsgBox(TITLE_STR, "您输入的ICCID号码长度有误, 请重新输入！");
			return;
		}
		// 校验手机号码
		mStrTelLast4 = mEditTelLast4.getText().toString().trim();
		if(StringUtil.isEmptyOrNull(mStrTelLast4))
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入手机号码尾4位！");
			return;
		}
		if(mStrTelLast4.length() < 4)
		{
			DialogUtil.MsgBox(TITLE_STR, "您输入的手机号码尾4位长度有误, 请重新输入！");
			return;
		}
		
		DialogUtil.showProgress("正在获取号码...");
		mTelGetRequest = new TelGetRequest(App.getAgentId().trim(), mStrIccid, mStrTelLast4);
		executeRequest(mHandler, MSG_GET_TELNO_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mTelGetRequest, new GetTelListener());
	}
	
	// 获取号码的回调事件
	public final class GetTelListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			DialogUtil.closeProgress();
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL).sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售(序号：19001)").sendToTarget();
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				DialogUtil.closeProgress();
				// 检测交易状态
				Map<String, String> result = mTelGetRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mStrTel = DES.decryptDES(result.get("cardnumber"), BaseRequest.mEncryptKey);
					Log.e("", "获取到的手机号码为: " + mStrTel);
					mHandler.sendEmptyMessage(MSG_GET_TELNO_SUCC);
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_GET_TELNO_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				DialogUtil.closeProgress();
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售(序号：19001)").sendToTarget();
			}
		}
	}
	
	// 启动拍照
	private void procIdcardScan(final String path)
	{
		new Thread()
		{
			public void run()
			{
				IdentifyScan photo = new IdentifyScan(BusiPhoneRegisterMain.this);
				int ret = photo.takePhoto(path);
				if (ret != 1)
				{
					mIdentifyMsg = null;
					return;
				}
				else
				{
					Log.e("", "拍照成功, 开始识别...");
					mIdentifyMsg = photo.scanIdentifySigal(path);
					mHandler.sendEmptyMessage(IDCARD_SCAN_DONE);
				}
			}
		}.start();
	}
	
	// 获取用户输入
	private boolean getUserInput() 
	{
		// 判断证件姓名
		mStrUserName = mEditUserName.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrUserName)) 
		{
			mEditUserName.requestFocus();
			mImageUserName.setVisibility(View.VISIBLE);
			mImageUserName.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "请输入证件姓名！");
			return false;
		}
		if (!isNameValid(mStrCustType, mStrUserName)) 
		{
			mEditUserName.requestFocus();
			mImageUserName.setVisibility(View.VISIBLE);
			mImageUserName.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "您输入的证件姓名不正确, 请重新输入！");
			return false;
		}
		
		// 判断证件号码
		mStrUserNo = mEditUserNo.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrUserNo)) 
		{
			mEditUserNo.requestFocus();
			mImageUserNo.setVisibility(View.VISIBLE);
			mImageUserNo.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "请输入证件号码！");
			return false;
		}
		if (!isNoValid(mStrCustType, mStrUserNo)) 
		{
			mEditUserNo.requestFocus();
			mImageUserNo.setVisibility(View.VISIBLE);
			mImageUserNo.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "您输入的证件号码不合法, 请重新输入！");
			return false;
		}
		
		// 判断证件地址
		mStrUserAddr = mEditUserAddr.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrUserAddr)) 
		{
			mEditUserAddr.requestFocus();
			mImageUserAddr.setVisibility(View.VISIBLE);
			mImageUserAddr.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "请输入证件地址！");
			return false;
		}
		int result = isAddressValid(mStrCustType, mStrUserAddr);
		if (result != 0) 
		{
			mEditUserAddr.requestFocus();
			mImageUserAddr.setVisibility(View.VISIBLE);
			mImageUserAddr.setImageResource(R.drawable.ic_error);
			// 
			String tempstr = (result==1? "您输入的证件地址不合法, 请重新输入！":"您输入的证件地址长度太短, 请重新输入！");
			DialogUtil.MsgBox(TITLE_STR, tempstr);
			return false;
		}
		
		// 联系人信息
		// 联系人姓名
		mStrContactName = mEditContactName.getText().toString().trim();
		if(StringUtil.isEmptyOrNull(mStrContactName))
		{
			mStrContactName = "";
		}
		// 联系人电话
		mStrContactTel = mEditContactTel.getText().toString().trim();
		if(StringUtil.isEmptyOrNull(mStrContactTel))
		{
			mStrContactTel = "";
		}
		// 联系人通信地址
		mStrContactAddr = mEditContactAddr.getText().toString().trim();
		if(StringUtil.isEmptyOrNull(mStrContactAddr))
		{
			mStrContactAddr = "";
		}
		Log.e("", "客户姓名为: " 	+ mStrUserName);
		Log.e("", "证件号码为: " 	+ mStrUserNo);
		Log.e("", "证件地址为: " 	+ mStrUserAddr);
		Log.e("", "联系人为: " 		+ mStrContactName);
		Log.e("", "联系电话为: " 	+ mStrContactTel);
		Log.e("", "通信地址为: " 	+ mStrContactAddr);
		return true;
	}
	
	// 提交用户证件信息
	private void procStep3() 
	{
		if(!DeviceUtils.hasInternet())
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在提交证件信息...");
		mUploadRequest = new UploadCertificateInfoRequest(mCommunicationID, App.getAgentId(), App.getDesKey(), 
				mStrTel, mStrUserName, mStrUserNo, mStrCustType, mStrUserAddr, 
				mStrContactName, mStrContactTel, mStrContactAddr);
		executeRequest(mHandler, MSG_UPLOAD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mUploadRequest, new UploadRequestListener());
	}
	
	public final class UploadRequestListener implements RequestListener<String>
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_UPLOAD_FAIL).sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售(序号：39001)").sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0)
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				Map<String,String> result = mUploadRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				// 文本上传成功
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 上传图片
					if("1".equals(mUploadType))
					{
						mHandler.obtainMessage(MSG_UPLOAD_SUC, desc).sendToTarget();
					} 
					else
					{
						mHandler.obtainMessage(MSG_UPLOAD_SUC, desc).sendToTarget();
				    }
				}
				else 
				{
					mHandler.obtainMessage(MSG_UPLOAD_FAIL, desc).sendToTarget();
				} 
			}
			catch (Exception e)
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售(序号：39001)").sendToTarget();
			}
		}
	}
}
