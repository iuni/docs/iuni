package com.sunnada.baseframe.activity.busiaccept.robospice;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.sunnada.baseframe.activity.busiaccept.base.DES;

public class UploadCertificateInfoRequest extends BaseRequest 
{
	private String 		mCommunicaID;
	private String 		mAgentId;
	private String 		mDesKey;
	private String 		mTelphone;
	private String 		certificateName;
	private String 		certificateNum;
	private String 		certificateType;
	private String 		certificateAdd;
	
	private String		mContactName;			// 联系人姓名
	private String		mContactTel;			// 联系人电话
	private String		mContactAddr;			// 联系人通信地址

	public UploadCertificateInfoRequest(String communicaID, String agentId,
			String desKey, String telphone, String certificateName,
			String certificateNum, String certificateType, String certificateAdd,
			String contactName, String contactTel, String contactAddr) 
	{
		Log.e("", "证件类型为: " + certificateType);
		
		this.mCommunicaID 		= communicaID;
		this.mAgentId 			= agentId;
		this.mDesKey 			= desKey;
		this.mTelphone 			= telphone;
		
		this.certificateName 	= certificateName;
		this.certificateNum 	= certificateNum;
		this.certificateType 	= certificateType;
		this.certificateAdd 	= certificateAdd;
		
		this.mContactName		= contactName;
		this.mContactTel		= contactTel;
		this.mContactAddr		= contactAddr;
	}

	@Override
	protected String getMethod() 
	{
		return "uploadCertificateInfo";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "uploadCertificateInfoResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		
		params.add(new Param(COMMUNICATION_ID, 	mCommunicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentId, 		mDesKey)));
		params.add(new Param(USER_TEL, 			DES.encryptDES(mTelphone, 		mDesKey)));
		params.add(new Param(USER_NAME, 		DES.encryptDES(certificateName, mDesKey)));
		params.add(new Param(USER_TYPE, 		DES.encryptDES(certificateType, mDesKey)));
		params.add(new Param(USER_NO, 			DES.encryptDES(certificateNum, 	mDesKey)));
		params.add(new Param(USER_ADDR, 		DES.encryptDES(certificateAdd, 	mDesKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		
		params.add(new Param(CONTACT_NAME, 		DES.encryptDES(mContactName, 	mDesKey)));
		params.add(new Param(CONTACT_TEL, 		DES.encryptDES(mContactTel, 	mDesKey)));
		params.add(new Param(CONTACT_ADDR, 		DES.encryptDES(mContactAddr, 	mDesKey)));
		return params;
	}
}
