package com.sunnada.baseframe.activity.busiaccept;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_08;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButtonOrange;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.DimensionUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;

// 增值业务办理
public class BusiAddValue extends BaseActivity implements OnClickListener
{
	private LinearLayout			  mLinearBacktoBusiAccept;		// 返回到业务受理菜单
	private int						  mAddValueBusiType = 0x01;		// 0x01-增值业务订购 0x02-增值业务退订
	
	// 左边布局
	private LinearLayout              mLinearLeft;                  
	private ImageView                 mIvLeftIcon;
	
	private LinearLayout			  mLinearUserInput;				// 用户输入布局
	private EditText				  mEditPhone;					// 手机号输入框
	private EditText				  mEditRandom;					// 验证码输入框
	private Button					  mBtnGetRandom;				// 获取验证码
	private TextView                  mTvGetRandomResult;           // 获取验证码成功时给出的提示信息
	private MyCustomButton			  mBtnQuery;					// 查询按钮
	private TextView                  mTvCountDown;                 // 倒计时
	
	private Business_08				  mBusiness08;
	private String					  mStrTelNum;					// 手机号码
	private String					  mStrRandom;					// 随机码
	private boolean					  mIsGetRandomOk;				// 获取验证码OK?
	
	// 增值业务订购界面
	private LinearLayout              mLinearOrderTitle;            // 增值订购标题
	private LinearLayout			  mLinearAddValueOrder;			// 增值业务订购界面
	private ListView				  mListAddValueCanOrder;	    // 可订购的增值业务列表
	private TextView				  mTextTelNumOrder;				// 查询号码
	private ImageView				  mImageGotoExit;				// 跳转到增值业务退订
	private MyCustomButtonOrange	  mBtnOrderBack;				// 增值业务订购返回到验证码输入界面
	
	// 增值业务退订界面
	private LinearLayout              mLinearExitTitle;             // 增值业务退订标题
	private LinearLayout			  mLinearAddValueExit;			// 增值业务退订界面
	private ListView				  mListAddValueOrdered;			// 已经订购的增值业务列表
	private TextView				  mTextTelNumExit;				// 查询号码
	private ImageView				  mImageGotoOrder;				// 跳转到增值业务订购
	private MyCustomButtonOrange	  mBtnExitBack;					// 增值业务退订界面返回到密码输入界面
	
	private String                    mStrBusiName;                 // 正在操作业务名字
	private int                       mBusiCode;                    // 正在操作业务编码
	private int                       mBusiPosition;                // 正在操作业务位置

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_busiaccept_addvalue_orange);
		}
		else
		{
			setContentView(R.layout.lay_busiaccept_addvalue);
		}
		
		mBusiness08 = new Business_08(this, equipmentService, socketService, dataBaseService);
		// 初始化布局
		initViews();
		// 初始化数据
		// initData();
	}

	// 初始化布局
	private void initViews() 
	{
		// 初始化左边界面
		initLeftUI();
		// 初始化第一步
		initStep1();
		// 初始化增值业务订购界面
		initStep2();
		// 初始化增值业务退订界面
		initStep3();
		
		// 显示第一页
		showPage(0x01);
	}
	
	// 初始化左边界面
	private void initLeftUI()
	{
	    mLinearLeft = (LinearLayout) this.findViewById(R.id.linear_left);
	    mIvLeftIcon = (ImageView) this.findViewById(R.id.iv_left_icon);
	}
	
	// 初始化第一步
	private void initStep1() 
	{
		mLinearBacktoBusiAccept 	= (LinearLayout) findViewById(R.id.linear_backto_busiaccept);
		mLinearUserInput 			= (LinearLayout) findViewById(R.id.linear_random);
		
		mEditPhone 		= (EditText) findViewById(R.id.et_phone_no);
		mEditRandom 	= (EditText) findViewById(R.id.et_random);
		mBtnGetRandom 	= (Button) findViewById(R.id.btn_get_random);
		mTvGetRandomResult = (TextView) findViewById(R.id.tv_get_random_result);
		mTvGetRandomResult.setVisibility(View.GONE);
		mTvCountDown    = (TextView) findViewById(R.id.tv_countdown);
		mTvCountDown.setVisibility(View.GONE);
		
		mLinearBacktoBusiAccept.setOnClickListener(this);
		mBtnGetRandom.setOnClickListener(this);
		
		mBtnQuery = (MyCustomButton) findViewById(R.id.btn_query);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query_orange, (float)0.8, (float)0.8);
		}
		else
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query, (float)0.8, (float)0.8);
		}
		mBtnQuery.setTextViewText1("查询");
		mBtnQuery.setOnClickListener(this);
		mBtnQuery.setOnTouchListener(this);
	}
	
	// 初始化增值业务订购界面
	private void initStep2() 
	{
		mLinearAddValueOrder = (LinearLayout) findViewById(R.id.linear_addvalue_order);
		mListAddValueCanOrder = (ListView) findViewById(R.id.list_addvalue_can_order);
		mLinearOrderTitle = (LinearLayout) findViewById(R.id.linear_order_title);
		mTextTelNumOrder = (TextView) findViewById(R.id.tv_phone_number_order);
		
		// 跳转到增值业务退订
		mImageGotoExit = (ImageView) findViewById(R.id.image_addvalue_goto_exit);
		mImageGotoExit.setOnClickListener(this);
		// 返回到验证码输入界面
		mBtnOrderBack = (MyCustomButtonOrange) findViewById(R.id.btn_addvalue_order_back);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnOrderBack.setImageResource(R.drawable.btn_reselect_orange);
		}
		else
		{
			mBtnOrderBack.setImageResource(R.drawable.btn_reselect);
		}
		mBtnOrderBack.setTextViewText1("返回");
		mBtnOrderBack.setOnClickListener(this);
		mBtnOrderBack.setOnTouchListener(this);
	}
	
	// 初始化增值业务退订界面
	private void initStep3() 
	{
		mLinearAddValueExit = (LinearLayout) findViewById(R.id.linear_addvalue_exit);
		mListAddValueOrdered = (ListView) findViewById(R.id.list_addvalue_ordered);
		mLinearExitTitle = (LinearLayout) findViewById(R.id.linear_exit_title);
		mTextTelNumExit = (TextView) findViewById(R.id.tv_phone_number_exit);
		
		// 跳转到增值业务订购
		mImageGotoOrder = (ImageView) findViewById(R.id.image_addvalue_goto_order);
		mImageGotoOrder.setOnClickListener(this);
		// 返回到验证码输入界面
		mBtnExitBack = (MyCustomButtonOrange) findViewById(R.id.btn_addvalue_exit_back);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnExitBack.setImageResource(R.drawable.btn_reselect_orange);
		}
		else
		{
			mBtnExitBack.setImageResource(R.drawable.btn_reselect);
		}
		mBtnExitBack.setTextViewText1("返回");
		mBtnExitBack.setOnClickListener(this);
		mBtnExitBack.setOnTouchListener(this);
	}
	
	// 显示第几页
	private void showPage(int nPage) 
	{
		if(nPage == 0x01)
		{
			mLinearUserInput.setVisibility(View.VISIBLE);
			mLinearAddValueOrder.setVisibility(View.GONE);
			mLinearAddValueExit.setVisibility(View.GONE);
		}
		else if(nPage == 0x02)
		{
			mLinearUserInput.setVisibility(View.GONE);
			mLinearAddValueOrder.setVisibility(View.VISIBLE);
			mLinearAddValueExit.setVisibility(View.GONE);
		}
		else if(nPage == 0x03)
		{
			mLinearUserInput.setVisibility(View.GONE);
			mLinearAddValueOrder.setVisibility(View.GONE);
			mLinearAddValueExit.setVisibility(View.VISIBLE);
		}
	}
	
	public void initData()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			int argbBlue = ARGBUtil.getArgb(1);
			int argbOrange = ARGBUtil.getArgb(2);
		    int argbBlue2 = ARGBUtil.getArgb(3);
		    
		    mLinearLeft.setBackgroundColor(argbOrange);
		    mIvLeftIcon.setImageResource(R.drawable.icon_addvalue_orange);
			// 获取验证码按钮
			mBtnGetRandom.setBackgroundColor(argbBlue);
			// 号码
			mTextTelNumExit.setTextColor(argbBlue2);
			mTextTelNumOrder.setTextColor(argbBlue2);     
			// 增值业务标题
			mLinearOrderTitle.setBackgroundColor(argbOrange);
			mLinearExitTitle.setBackgroundColor(argbOrange);
			// 退订按钮
			mImageGotoExit.setImageResource(R.drawable.addvalue_exit_orange);
			// 订购按钮
			mImageGotoOrder.setImageResource(R.drawable.addvalue_order_orange);
		}
	}
	
	// 计时器
	private CountDownTimer mCountDownTimer = new CountDownTimer(180*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			String tempstr = String.format("剩余%d秒", millisUntilFinished/1000);
			mTvCountDown.setText(tempstr);
		}
		
		@Override
		public void onFinish() 
		{
			mTvCountDown.setVisibility(View.GONE);
			mTvGetRandomResult.setText("短信验证码已失效，请重新获取!");
			mIsGetRandomOk = false;
		}
	};
	
	@Override
	public void onInitSuccess() 
	{
	}

	@Override
	public void onInitFail() 
	{
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			// 连接蓝牙设备
			case Statics.MSG_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiAddValue.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						print();
					}
				}.start();
				break;
				
			// 取消蓝牙或蓝牙连接失败
			case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				sendMsgChoice();
				break;
				
			default:
				break;
			}
		}
	};
		
	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
					
		switch(v.getId()) 
		{
		// 返回到业务受理
		case R.id.linear_backto_busiaccept:
			backToPrevious();
			break;
			
		// 获取验证码
		case R.id.btn_get_random:
			handleGetRandom();
			break;
			
		// 查询
		case R.id.btn_query:
			handleQuery();
			break;
			
		// 调转到增值业务退订流程
		case R.id.image_addvalue_goto_exit:
			mAddValueBusiType = 0x02;
			// 重新获取验证码
			showPage(0x01);
			// 设置手机号码
			mEditPhone.setText(mStrTelNum);
			break;
			
		// 跳转到增值业务订购流程
		case R.id.image_addvalue_goto_order:
			mAddValueBusiType = 0x01;
			// 重新获取验证码
			showPage(0x01);
			// 设置手机号码
			mEditPhone.setText(mStrTelNum);
			break;
			
		// 增值业务订购流程返回到验证码输入界面
		case R.id.btn_addvalue_order_back:
		// 增值业务退订六层返回到验证码输入界面
		case R.id.btn_addvalue_exit_back:
			showPage(0x01);
			break;
		}
	}
	
	public void backToPrevious()
	{
		back(new ICallBack() 
		{
			@Override
			public void back() 
			{
				mCountDownTimer.cancel();
				BusiAddValue.this.finish();
			}
			
			@Override
			public void cancel() 
			{
			}
			
			@Override
			public void dismiss()
			{
			}
		}, null, null, "确定", "取消 ", true ,false, false);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
		
	// 处理获取验证码的请求
	private void handleGetRandom() 
	{
		// 校验手机号码
		mStrTelNum = mEditPhone.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrTelNum)) 
		{
			back(null, Business_08.TITLE_STR, "请输入手机号码！", "确定", null, false, false, false);
			return;
		}
		if(mStrTelNum.length() != 11)
		{
			back(null, Business_08.TITLE_STR, "请输入11位手机号码！", "确定", null, false, false, false);
			return;
		}
		
		// 获取随机码
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取随机码");
				if(mBusiness08.bll0101(mStrTelNum)) 
				{
					mIsGetRandomOk = true;
					DialogUtil.showToast("获取随机码成功！", 0x00);
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							mTvGetRandomResult.setVisibility(View.VISIBLE);
							mTvGetRandomResult.setText("获取成功，请在180秒内输入短信验证码！");
							mTvCountDown.setVisibility(View.VISIBLE);
							mCountDownTimer.start();
						}
					});
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 处理查询请求
	private void handleQuery() 
	{
		// 是否已经获取过验证码
		if(mIsGetRandomOk == false)
		{
			back(null, Business_08.TITLE_STR, "请先获取验证码！", "确定", null, false, false, false);
			return;
		}
		
		// 校验手机号码
		String strTelNum = mEditPhone.getText().toString();
		if(StringUtil.isEmptyOrNull(strTelNum)) 
		{
			back(null, Business_08.TITLE_STR, "请输入手机号码！", "确定", null, false, false, false);
			return;
		}
		if(strTelNum.length() != 11)
		{
			back(null, Business_08.TITLE_STR, "请输入11位手机号码！", "确定", null, false, false, false);
			return;
		}
		// 判断手机号码是否一致
		if(mStrTelNum.equals(strTelNum) == false)
		{
			mIsGetRandomOk = false;
			back(null, Business_08.TITLE_STR, "检测到手机号码发生更改, 请重新获取验证码！", "确定", null, false, false, false);
			return;
		}
		
		// 校验验证码
		mStrRandom = mEditRandom.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrRandom))
		{
			back(null, Business_08.TITLE_STR, "请输入验证码！", "确定", null, false, false, false);
			return;
		}
		if(mStrRandom.length() != 6)
		{
			back(null, Business_08.TITLE_STR, "请输入6位验证码！", "确定", null, false, false, false);
			return;
		}
		
		
		// 清空部分数据
		clearParticalData();
		
		// 校验随机码
		new Thread() 
		{
			public void run() 
			{
				// 校验验证码
				DialogUtil.showProgress("正在校验验证码");
				if(mBusiness08.bll0102(mStrTelNum, mStrRandom) == false)
				{
					DialogUtil.closeProgress();
					return;
				}
				
				// 清空编辑框
				clearEdit();
				
				// 获取增值业务列表
				mBusiness08.mListBusi.clear();
				DialogUtil.showProgress("正在获取增值业务列表");
				if(mBusiness08.bll0801(mAddValueBusiType, mStrTelNum) == false) 
				{
					DialogUtil.closeProgress();
					// debug
					// 刷新增值业务列表
					//refreshAddValueList();
					// debug end
					return;
				}
				DialogUtil.closeProgress();
				// 刷新增值业务列表
				refreshAddValueList();
			}
		}.start();
	}
	
	// 清空编辑框
	private void clearEdit() 
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				mEditPhone.setText("");
				mEditRandom.setText("");
				mIsGetRandomOk = false;
				mTvGetRandomResult.setVisibility(View.GONE);
			}
		});
	}
	
	// 清空部分编辑框数据
	private void clearParticalData()
	{
		mCountDownTimer.cancel();
		mTvCountDown.setVisibility(View.GONE);
		mTvGetRandomResult.setVisibility(View.GONE);
		mEditRandom.setText("");
	}
	
	// 刷新增值业务列表
	private void refreshAddValueList() 
	{
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{	
				// 按编码升序排列
				for(int i = 0;i < mBusiness08.mListBusi.size();i++)
				{
					Map<String,String> map = new HashMap<String, String>();
					map.put(Business_08.ADDVALUE_ID, String.format("%02d", (i+1)));
					map.put(Business_08.ADDVALUE_CODE, mBusiness08.mListBusi.get(i).get(Business_08.ADDVALUE_CODE));
					map.put(Business_08.ADDVALUE_NAME, mBusiness08.mListBusi.get(i).get(Business_08.ADDVALUE_NAME));
					mBusiness08.mListBusi.set(i, map);
				}
				
				if(mAddValueBusiType == 0x01) 
				{
					SimpleAdapter adapter = new AddValueOrderAdapter(BusiAddValue.this, 
							mBusiness08.mListBusi, 
							R.layout.lay_busiaccept_addvalue_listitem, 
							new String[] {Business_08.ADDVALUE_ID, Business_08.ADDVALUE_NAME}, 
							new int[] {R.id.tv_listitem_id, R.id.tv_listitem_name});
					mListAddValueCanOrder.setAdapter(adapter);
					DimensionUtil.setListViewHeightBasedOnChildren(mListAddValueCanOrder);
					// 显示增值业务订购界面
					showPage(0x02);
					// 设置手机号码
					mTextTelNumOrder.setText(mStrTelNum);
				}
				else
				{
					SimpleAdapter adapter = new AddValueExitAdapter(BusiAddValue.this, 
							mBusiness08.mListBusi, 
							R.layout.lay_busiaccept_addvalue_listitem, 
							new String[] {Business_08.ADDVALUE_ID, Business_08.ADDVALUE_NAME}, 
							new int[] {R.id.tv_listitem_id, R.id.tv_listitem_name});
					mListAddValueOrdered.setAdapter(adapter);
					DimensionUtil.setListViewHeightBasedOnChildren(mListAddValueOrdered);
					// 显示增值业务退订界面
					showPage(0x03);
					// 设置手机号码
					mTextTelNumExit.setText(mStrTelNum);
				}
			}
		});
	}
	
	// 增值业务订购适配器
	private class AddValueOrderAdapter extends SimpleAdapter 
	{
		private List<? extends Map<String, ?>> dataList = new ArrayList<Map<String,String>>();
		public AddValueOrderAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
			dataList = data;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				convertView = super.getView(position, convertView, parent);
			}
			
			// 增值业务详情TextView
			TextView tvListitemDetail =(TextView)convertView.findViewById(R.id.tv_listitem_detail);
			// 增值业务订购TextView
			TextView tvListitemOp =(TextView)convertView.findViewById(R.id.tv_listitem_op);
			// 增值业务编码TextView
			TextView tvListitemId =(TextView)convertView.findViewById(R.id.tv_listitem_id);
			// 增值业务名称TextView
			TextView tvListitemName =(TextView)convertView.findViewById(R.id.tv_listitem_name);
			
			tvListitemDetail.setText(Html.fromHtml("<u>详情</u>"));
			tvListitemOp.setText(Html.fromHtml("<u>订购</u>"));
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				int argbOrange = ARGBUtil.getArgb(2);
				int argbBlue2 = ARGBUtil.getArgb(3);
				int argbGreen = ARGBUtil.getArgb(4);
				tvListitemDetail.setTextColor(argbOrange);
				tvListitemOp.setTextColor(argbBlue2);
				tvListitemOp.setBackgroundColor(argbGreen);
			}
			tvListitemId.setText((String)dataList.get(position).get(Business_08.ADDVALUE_ID));
			tvListitemName.setText((String)dataList.get(position).get(Business_08.ADDVALUE_NAME));
			
			// 获取增值业务详情
			tvListitemDetail.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					getAddValueBusiDetail(position);
				}
			});
			// 增值业务订购
			tvListitemOp.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					// 订购增值业务
					orderAddValueBusi(position);
				}
			});
			return convertView;
		}
	}
	
	// 增值业务退订适配器
	private class AddValueExitAdapter extends SimpleAdapter 
	{
		private List<? extends Map<String, ?>> dataList = new ArrayList<Map<String,String>>();
		public AddValueExitAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
			this.dataList = data;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if (convertView == null) 
			{
				convertView = super.getView(position, convertView, parent);
			}
			
			// 增值业务详情TextView
			TextView tvListitemDetail =(TextView)convertView.findViewById(R.id.tv_listitem_detail);
			// 增值业务退订TextView
			TextView tvListitemOp =(TextView)convertView.findViewById(R.id.tv_listitem_op);
			// 增值业务编码TextView
			TextView tvListitemId =(TextView)convertView.findViewById(R.id.tv_listitem_id);
			// 增值业务名称TextView
			TextView tvListitemName =(TextView)convertView.findViewById(R.id.tv_listitem_name);
			
			tvListitemDetail.setText(Html.fromHtml("<u>详情</u>"));
			tvListitemOp.setText(Html.fromHtml("<u>退订</u>"));
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				int argbOrange = ARGBUtil.getArgb(2);
				int argbBlue2 = ARGBUtil.getArgb(3);
				int argbGreen = ARGBUtil.getArgb(4);
				tvListitemDetail.setTextColor(argbOrange);
				tvListitemOp.setTextColor(argbBlue2);
				tvListitemOp.setBackgroundColor(argbGreen);
			}
			tvListitemId.setText((String)dataList.get(position).get(Business_08.ADDVALUE_ID));
			tvListitemName.setText((String)dataList.get(position).get(Business_08.ADDVALUE_NAME));
			
			// 获取增值业务详情
			tvListitemDetail.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					getAddValueBusiDetail(position);
				}
			});
			// 增值业务退订
			tvListitemOp.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					// 增值业务退订
					exitAddValueBusi(position);
				}
			});
			return convertView;
		}
	}
	
	// 获取增值业务详情
	private void getAddValueBusiDetail(final int position) 
	{
		final String strAddName = (String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_NAME);
		final int nAddCode = Integer.parseInt((String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_CODE));
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取详情");
				if(mBusiness08.bll0406(nAddCode))
				{
					back(null, strAddName + "详情", mBusiness08.mStrDetail, "确定", null, false, false, false);
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 订购增值业务
	private void orderAddValueBusi(final int position) 
	{
		final String strAddName = (String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_NAME);
		final int nAddCode = Integer.parseInt((String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_CODE));
		
		// 业务名称
		mStrBusiName = strAddName;
		// 业务编码
		mBusiCode = nAddCode;
		// 业务位置
		mBusiPosition = position;
		
		new Thread() 
		{
			public void run() 
			{
				// 订购增值业务
				DialogUtil.showProgress("正在订购" + strAddName);
				if(mBusiness08.bll0802(mStrTelNum, nAddCode)) 
				{
					DialogUtil.closeProgress();
					
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							print();
						}
						
						@Override
						public void cancel()
						{
							orderSendMsgChoice();
						}

						@Override
						public void dismiss() 
						{
							orderSendMsgChoice();
						}
					}, null, "订购" + mStrBusiName + "成功！是否打印凭据?", "确定", "取消", true, true, true);
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 退订增值业务
	private void exitAddValueBusi(final int position) 
	{
		final String strAddName = (String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_NAME);
		final int nAddCode = Integer.parseInt((String)mBusiness08.mListBusi.get(position).get(Business_08.ADDVALUE_CODE));
		
		// 业务名称
		mStrBusiName = strAddName;
		// 业务编码
		mBusiCode = nAddCode;
		// 业务位置
		mBusiPosition = position;
		
		new Thread() 
		{
			public void run() 
			{
				// 订购增值业务
				DialogUtil.showProgress("正在退订" + strAddName);
				if(mBusiness08.bll0803(mStrTelNum, nAddCode)) 
				{
					DialogUtil.closeProgress();
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							print();
						}
						
						@Override
						public void cancel() 
						{
							exitSendMsgChoice();
						}

						@Override
						public void dismiss()
						{
							exitSendMsgChoice();
						}
					}, null, "退订" + mStrBusiName + "成功！"+"是否打印凭据?", "打印", "取消", true, true, true);
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 订购成功发送短信通知
	private void sendOrderOkSms(final int nAddCode, final int position) 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在发送短信通知");
				if(mBusiness08.bll0804(mStrTelNum, nAddCode)) 
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							mBusiness08.mListBusi.remove(position);
							// 刷新可订购增值业务列表
							refreshAddValueList();
						}
						
						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, Business_08.TITLE_STR, "发送短信通知成功!", "确定", null, true, false, false);
				}
				else
				{
					mBusiness08.mListBusi.remove(position);
					// 刷新可订购增值业务列表
					refreshAddValueList();
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 退订成功发送短信通知
	private void sendExitOkSms(final int nAddCode, final int position) 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在发送短信通知");
				if(mBusiness08.bll0805(mStrTelNum, nAddCode)) 
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							mBusiness08.mListBusi.remove(position);
							// 刷新可退订增值业务列表
							refreshAddValueList();
						}
						
						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, Business_08.TITLE_STR, "发送短信通知成功!", "确定", null, true, false, false);
				}
				else
				{
					mBusiness08.mListBusi.remove(position);
					// 刷新可退订增值业务列表
					refreshAddValueList();
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 订购业务是否发送短信选择框
	private void orderSendMsgChoice()
	{
		back(new ICallBack()
		{
			@Override
			public void back() 
			{
				// 发送短信通知
				sendOrderOkSms(mBusiCode, mBusiPosition);
			}
			
			@Override
			public void cancel() 
			{
				mBusiness08.mListBusi.remove(mBusiPosition);
				// 刷新可订购增值业务列表
				refreshAddValueList();
			}

			@Override
			public void dismiss() 
			{
				mBusiness08.mListBusi.remove(mBusiPosition);
				// 刷新可订购增值业务列表
				refreshAddValueList();
			}
		}, Business_08.TITLE_STR, "是否发送短信通知?", "确定", "取消", true, true, true);
	}
	
	// 选择
	public void sendMsgChoice()
	{
		if(mAddValueBusiType == 1)
		{
			orderSendMsgChoice();
		}
		else
		{
			exitSendMsgChoice();
		}
	}
	
	// 退订业务是否发送短信选择框
	private void exitSendMsgChoice()
	{
		back(new ICallBack()
		{
			@Override
			public void back() 
			{
				// 发送短信通知
				sendExitOkSms(mBusiCode, mBusiPosition);
			}
			
			@Override
			public void cancel() 
			{
				mBusiness08.mListBusi.remove(mBusiPosition);
				// 刷新可退订增值业务列表
				refreshAddValueList();
			}

			@Override
			public void dismiss() 
			{
				mBusiness08.mListBusi.remove(mBusiPosition);
				// 刷新可退订增值业务列表
				refreshAddValueList();
			}
		}, Business_08.TITLE_STR, "是否发送短信通知?", "确定", "取消", true, true, true);
	}
	
	// 打印
	private void print() 
	{
		DialogUtil.showProgress("正在打印,请稍候");
		new Thread() 
		{
			public void run() 
			{
				try 
				{
					// 检测蓝牙设备
					if(equipmentService.handshake() == false) 
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("请连接蓝牙设备", 
								"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
								"立即搜索", 
								Statics.MSG_CONNECT_BLUETOOTH, 
								"取消", 
								Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
								Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
								mHandler);
						return;
					}
					
					// 开始打印
					mBusiness08.print(printMsg(), new String(mBusiness08.bllnum, "ASCII"));
				} 
				catch (Exception e) 
				{
					DialogUtil.closeProgress();
					e.printStackTrace();
				}
				
				if(equipmentService.isPrintSuccess())
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							mBusiness08.mListBusi.remove(mBusiPosition);
							// 刷新可退订增值业务列表
							refreshAddValueList();
							DialogUtil.closeProgress();
						}
						
						@Override
						public void dismiss() 
						{
							mBusiness08.mListBusi.remove(mBusiPosition);
							// 刷新可退订增值业务列表
							refreshAddValueList();
							DialogUtil.closeProgress();
						}
						
						@Override
						public void cancel() 
						{
						}
					}, 
					PrintUtil.STR_SUCCESS_TITLE, 
					PrintUtil.STR_SUCCESS_MSG, 
					PrintUtil.STR_SUCCESS_OK, 
					null , true, false, true);
				}
				else
				{
					DialogUtil.closeProgress();
					showPrintDialog();
				}
			};
		}.start();
	}
	
	// 显示打印对话框
	private void showPrintDialog()
	{
		back(new ICallBack()
		{
			@Override
			public void back() 
			{
				print();
			}
			
			@Override
			public void cancel() 
			{
				sendMsgChoice();
			}

			@Override
			public void dismiss() 
			{
				sendMsgChoice();
			}
		}, 
		PrintUtil.STR_FAIL_TITLE,
		PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()),
		PrintUtil.STR_FAIL_OK, 
		PrintUtil.STR_FAIL_CANCEL, true, true, true);
	}
	
	// 打印信息
	private StringBuffer printMsg()
	{
	    StringBuffer strbuf = new StringBuffer();
		
	    // 增值业务订购
		if(mAddValueBusiType == 1)
		{
			strbuf.append("业务类型: 增值业务订购 ").append("\n");
			strbuf.append("手机号码: "+mStrTelNum).append("\n");
			strbuf.append("已订购: " +mStrBusiName).append("\n");
		}
		
		// 增值业务退订
		else
		{
			strbuf.append("业务类型: 增值业务退订 ").append("\n");
			strbuf.append("手机号码: "+mStrTelNum).append("\n");
			strbuf.append("已退订: " +mStrBusiName).append("\n");
		}
		try
		{
			strbuf.append("交易时间:" +StringUtil.converDateString(new String(mBusiness08.blltime,"ASCII")));
		} 
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		  
		return strbuf;
	}
}
