package com.sunnada.baseframe.activity.charge;

import android.os.Bundle;

public class BusiChargeEPayFix extends BusiChargeEPay 
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mBusinessEPay.mTranceType = 0x02;
		
		// 显示第一步
		showPage(0x01);
		// 刷新第一步
		refreshPage(0x01);
		
		// 检测交易环境 
		checkErrorDeal(new String[] {"02"});
	}
}
