package com.sunnada.baseframe.activity.charge;

import android.os.Bundle;

public class BusiChargeEPayPhone extends BusiChargeEPay 
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		// 交易类型
		mBusinessEPay.mTranceType = (byte)0x01;
		// 显示第一步
		showPage(0x01);
		// 刷新第一步
		refreshPage(0x01);
		
		// 检测交易环境  
		checkErrorDeal(new String[] {"01"});
	}
}
