package com.sunnada.baseframe.activity.busiaccept.base;

import java.io.File;

import com.sunnada.baseframe.activity.busiaccept.robospice.BaseRequest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class App //extends Application 
{
	protected static String 		TAG 						= "实名制返档";
	public static final String 		SDCARD_ROOT_PATH 			= Environment.getExternalStorageDirectory().getPath();
	public static final String 		SDCARD_BASE_PATH 			= SDCARD_ROOT_PATH + "/idcard";
	
	private static final String 	AGENT_ID 					= "key_agent_id";
	private static final String 	AGENT_PWD					= "key_agent_pwd";
	private static final String 	ACCESS_TOKEN 				= "key_access_token";
	private static final String 	FIRST 						= "key_first_run";
	private static final String 	AGENT_NAME 					= "key_agent_name";
	// 自动登录标志
	private static final String		AUTO_LOGIN					= "key_auto_login";
	// 加密密码
	private static final String		DES_KEY						= "des_key";
	private static final String		USER_NO						= "key_user_no";
	private static final String		USER_NAME					= "key_user_name";
	private static final String		SHOP_ADDR					= "shop_addr";				// 网店地址
	private static final String		SHOP_TEL					= "shop_tel";
	private static final String		SHOP_NAME					= "shop_name";
	
	private static Context 			mContext;
	private static Resources 		mResources;

	public App() 
	{
		
	}
	
	public static void init(Context context) 
	{
		mContext = context;
	}
	
	public static Context context() 
	{
		return mContext;
	}
	
	public static SharedPreferences getPreferences() 
	{
		return context().getSharedPreferences("idcard", Activity.MODE_PRIVATE);
	}
	
	public static Resources resources() 
	{
		return mResources;
	}
	
	public static boolean checkExternalMemoryAvailable() 
	{
		if (getExternalMemoryAvailableSize() / 1024L / 1024L < 1L)
		{
			return false;
		}
		return true;
	}

	public static DisplayMetrics getDisplayMetrics() 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((WindowManager) App.context().getSystemService("window")).getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics;
	}
	
	public static float dpToPixel(float dp) 
	{
		return dp * (getDisplayMetrics().densityDpi / 160F);
	}
	
	public static String getBaseDir() 
	{
		return SDCARD_BASE_PATH;
	}

	public static long getExternalMemoryAvailableSize()
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getAvailableBlocks();
		} 
		else 
		{
			return -1L;
		}
	}

	public static long getExternalMemoryTotalSize() 
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getBlockCount();
		} 
		else 
		{
			return -1L;
		}
	}

	public static String getImagePath(String imgName) 
	{
		return SDCARD_BASE_PATH+"/images"+File.separator+imgName;
	}

	public static String getImagesDir() 
	{
		return SDCARD_BASE_PATH+"/.images";
	}
	
	/*
	public void onConfigurationChanged(Configuration configuration)
	{
		super.onConfigurationChanged(configuration);
	}

	@Override
	public void onCreate() 
	{
		super.onCreate();
		Log.e(TAG, "App.onCreate()");
		
		mContext = getApplicationContext();
		if(mContext == null)
		{
			Log.e(TAG, "context = null");
		}
		mResources = getResources();
	}
	
	public void onLowMemory() 
	{
		super.onLowMemory();
	}

	public void onTerminate() 
	{
		super.onTerminate();
	}
	*/
	
	public static boolean hasAccessToken() 
	{
		String token = getAccessToken();
		if (token != null && token.length() > 0)
		{
			return true;
		}
		return false;
	}

	public static String getAccessToken() 
	{
		return getPreferences().getString(ACCESS_TOKEN, null);
	}
	
	public static void setAccessToken(String token) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(ACCESS_TOKEN, token);
		ed.commit();
	}
	
	// 获取加密key
	public static String getDesKey()
	{
		return getPreferences().getString(DES_KEY, BaseRequest.mEncryptKey);
	}
	
	// 设置加密key
	public static void setDesKey(String key)
	{
		Editor ed = getPreferences().edit();
		ed.putString(DES_KEY, key);
		ed.commit();
	}
	
	// 设置是否自动登录
	public static boolean getAutoLoginFlag() 
	{
		return getPreferences().getBoolean(AUTO_LOGIN, true);
	}
	
	// 设置是否自动登录
	public static void setAutoLoginFlag(boolean flag)
	{
		Editor ed = getPreferences().edit();
		ed.putBoolean(AUTO_LOGIN, flag);
		ed.commit();
	}

	public static long getLastPauseTime() 
	{
		return getPreferences().getLong("PAUSE_TIME", 0);
	}
	
	public static void setLastPauseTime(long time) 
	{
		Editor ed = getPreferences().edit();
		ed.putLong("PAUSE_TIME", time);
		ed.commit();
	}
	
	// APP是否第一次运行
	public static boolean isFirstRun() 
	{
		return getPreferences().getBoolean(FIRST, true);
	}

	// 设置APP的运行状态
	public static void setFirstRun(boolean b) 
	{
		Editor ed = getPreferences().edit();
		ed.putBoolean(FIRST, b);
		ed.commit();
	}
	
	// 获取发展人编码
	public static String getAgentId() 
	{
		String strAgentID = getPreferences().getString(AGENT_ID, null);
		//Log.e("", "发展人编码为: " + strAgentID);
		return strAgentID;
	}
	
	// 设置发展人编码
	public static void setAgentId(String agentID)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_ID, agentID);
		ed.commit();
		Log.e("", "更新发展人编码为: " + agentID);
	}
	
	// 获取发展人密码
	public static String getAgentPwd() 
	{
		return getPreferences().getString(AGENT_PWD, null);
	}
	
	// 设置发展人密码
	public static void setAgentPwd(String agentPwd)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_PWD, agentPwd);
		ed.commit();
	}
	
	// 获取发展人姓名
	public static String getAgentName() 
	{
		return getPreferences().getString(AGENT_NAME, null);
	}
	
	// 设置发展人姓名
	public static void setAgentName(String agentName)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_NAME, agentName);
		ed.commit();
		Log.e("", "更新代理商为: " + agentName);
	}
	
	// 获取登录工号
	public static String getUserNo() 
	{
		String strUserNo = getPreferences().getString(USER_NO, null);
		//Log.e("", "营业员工号为: " + strUserNo);
		return strUserNo;
	}
	
	// 设置登录工号
	public static void setUserNo(String strUserNo)
	{
		Editor ed = getPreferences().edit();
		ed.putString(USER_NO, strUserNo);
		ed.commit();
		Log.e("", "更新营业员工号为: " + strUserNo);
	}
	
	// 设置登录营业员姓名
	public static void setUserName(String strUserName)
	{
		Editor ed = getPreferences().edit();
		ed.putString(USER_NAME, strUserName);
		ed.commit();
		Log.e("", "更新营业员姓名为: " + strUserName);
	}
	
	// 获取登录营业员姓名
	public static String getUserName() 
	{
		String strUserName = getPreferences().getString(USER_NAME, null);
		return strUserName;
	}
	
	// 网点名称
	public static void setShopName(String strShopName)
	{
		Editor ed = getPreferences().edit();
		ed.putString(SHOP_NAME, strShopName);
		ed.commit();
		Log.e("", "更新网点名称为: " + strShopName);
	}
	
	public static String getShopName()
	{
		return getPreferences().getString(SHOP_NAME, null);
	}
	
	// 获取网点地址
	public static String getShopAddr() 
	{
		return getPreferences().getString(SHOP_ADDR, null);
	}
	
	// 设置网点地址
	public static void setShopAddr(String strShopAddr) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(SHOP_ADDR, strShopAddr);
		ed.commit();
		Log.e("", "更新网点地址为: " + strShopAddr);
	}
	
	// 获取网点电话
	public static String getShopTel() 
	{
		return getPreferences().getString(SHOP_TEL, null);
	}
	
	// 设置网点电话
	public static void setShopTel(String strShopTel) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(SHOP_TEL, strShopTel);
		ed.commit();
		Log.e("", "更新网点电话为: " + strShopTel);
	}
	
	public static void showToast(int message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(String message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}
	
	public static void saveDisplaySize(Activity activity) 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		SharedPreferences.Editor editor = getPreferences().edit();
		editor.putInt("screen_width", displaymetrics.widthPixels);
		editor.putInt("screen_height", displaymetrics.heightPixels);
		editor.putFloat("density", displaymetrics.density);
		editor.commit();
	}
	
	public static int[] getDisplaySize(Context context) 
	{
		SharedPreferences sp = getPreferences();
		return new int[] { sp.getInt("screen_width", 480), sp.getInt("screen_height", 854)};
	}
}
