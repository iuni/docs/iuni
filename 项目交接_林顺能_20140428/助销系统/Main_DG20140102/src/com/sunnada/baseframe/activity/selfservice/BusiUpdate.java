package com.sunnada.baseframe.activity.selfservice;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.CRCData;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.bluetooth.Commen;

// 业务更新
public class BusiUpdate extends BaseActivity implements OnClickListener
{
	// 左边布局
	private LinearLayout        mLinearLeft;
	
	private TextView 			mTextMenu;				// 菜单版本
	private TextView 			mTextMenuUpdate;
	private TextView			mTextPackage2G;			// 2G套餐版本
	private TextView			mTextPackage2GUpdate;
	private TextView			mTextAddValue;			// 增值业务版本
	private TextView			mTextAddValueUpdate;
	private TextView			mTextSpecial;			// 特服业务版本
	private TextView			mTextSpecialUpdate;
	private TextView			mTextNumber;			// 选号版本
	private TextView			mTextNumberUpdate;
	private TextView			mTextBrand;				// 品牌版本
	private TextView			mTextBrandUpdate;
	//private TextView			mTextWelcome;			// 开机欢迎语版本
	//private TextView			mTextWelcomeUpdate;
	//private TextView			mTextPrint;				// 凭条打印信息版本
	//private TextView			mTextPrintUpdate;
	//private TextView			mTextMail;				// 邮件
	//private TextView			mTextMailUpdate;
	private TextView			mTextPackage3G;			// 3G套餐
	private TextView			mTextPackage3GUpdate;
	
	private MyCustomButton		mBtnUpdate;				// 立即更新
	private LinearLayout		mBacktoSelfService;
	
	private Business_00	mBusiness00;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_busi_update_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_busi_update);
		}
		
		mBusiness00 = new Business_00(this, equipmentService, socketService, dataBaseService);
		// 初始化控件
		initViews();
		// 初始化显示
		// initShow();
		// 刷新界面
		refreshScreen();
		// 获取升级状态
		getUpdateStatus();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 左边布局
		mLinearLeft             = (LinearLayout) findViewById(R.id.linear_left);
		
		// 菜单版本
		mTextMenu	 			= (TextView) findViewById(R.id.tv_ver_menu);
		mTextMenuUpdate 		= (TextView) findViewById(R.id.tv_menu_update);
		// 2G套餐版本
		mTextPackage2G			= (TextView) findViewById(R.id.tv_ver_package2g);
		mTextPackage2GUpdate 	= (TextView) findViewById(R.id.tv_package2g_update);
		// 增值业务版本
		mTextAddValue			= (TextView) findViewById(R.id.tv_ver_addvalue);
		mTextAddValueUpdate		= (TextView) findViewById(R.id.tv_addvalue_update);
		// 特服业务版本
		mTextSpecial			= (TextView) findViewById(R.id.tv_ver_special);
		mTextSpecialUpdate		= (TextView) findViewById(R.id.tv_special_update);
		// 选号规则版本
		mTextNumber				= (TextView) findViewById(R.id.tv_ver_number);
		mTextNumberUpdate		= (TextView) findViewById(R.id.tv_number_update);
		// 品牌版本
		mTextBrand				= (TextView) findViewById(R.id.tv_ver_brand);
		mTextBrandUpdate		= (TextView) findViewById(R.id.tv_brand_update);
		// 3G套餐版本
		mTextPackage3G			= (TextView) findViewById(R.id.tv_ver_package3g);
		mTextPackage3GUpdate	= (TextView) findViewById(R.id.tv_package3g_update);
		
		mBtnUpdate = (MyCustomButton) findViewById(R.id.btn_busi_update);
		mBtnUpdate.setTextViewText1("立即更新");
		mBtnUpdate.setOnClickListener(this);
		mBtnUpdate.setOnTouchListener(this);
		
		mBacktoSelfService = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		mBacktoSelfService.setOnClickListener(this);
		
		mTextMenuUpdate.setOnClickListener(this);
		mTextPackage2GUpdate.setOnClickListener(this);
		mTextAddValueUpdate.setOnClickListener(this);
		mTextSpecialUpdate.setOnClickListener(this);
		mTextNumberUpdate.setOnClickListener(this);
		mTextBrandUpdate.setOnClickListener(this);
		mTextPackage3GUpdate.setOnClickListener(this);
	}
	
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			ImageView mIvBusiUpdate = (ImageView) findViewById(R.id.iv_busi_update);
			mIvBusiUpdate.setImageDrawable(getResources().getDrawable(R.drawable.icon_busi_update_orange));
	
			ImageView ivReturn = (ImageView) findViewById(R.id.btn_return);
			ivReturn.setImageDrawable(getResources().getDrawable(R.drawable.icon_return_orange));
		}
	}
	
	// 刷新界面
	private void refreshScreen() 
	{
		mTextMenu.setText(getVersionStr(CRCData.crcmenu));
		mTextPackage2G.setText(getVersionStr(CRCData.crcmeal));
		mTextAddValue.setText(getVersionStr(CRCData.crcincrement));
		mTextSpecial.setText(getVersionStr(CRCData.crcespeciallyservice));
		mTextNumber.setText(getVersionStr(CRCData.crcselnorule));
		mTextBrand.setText(getVersionStr(CRCData.crcbrand));
		mTextPackage3G.setText(getVersionStr(CRCData.crc3gmeal));
	}
	
	// 版本显示的时候，一个数字一个点
	private String getVersionStr(String crc)
	{
		// 如果为空
		if(StringUtil.isEmptyOrNull(crc))
		{
			return "0.0.0.0";
		}
		char[] chars = crc.toCharArray();
		StringBuffer result = new StringBuffer();
		for(char c:chars)
		{
			result.append(c);
			result.append(".");
		}
		return result.substring(0, result.length()-1).toString();
	}
	
	// 获取升级状态
	private void getUpdateStatus() 
	{
		new Thread()
		{
			public void run()
			{
				byte[] crc = new byte[2];
				// 菜单版本
				DialogUtil.showProgress("正在获取菜单版本...");
				if(mBusiness00.bll0021((byte)0x01, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcmenu.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x01, true);
					}
					else
					{
						refreshUpdateStatus(0x01, false);
					}
				}

				// 2G套餐版本
				DialogUtil.showProgress("正在获取2G套餐版本...");
				if(mBusiness00.bll0021((byte)0x02, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcmeal.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x02, true);
					}
					else
					{
						refreshUpdateStatus(0x02, false);
					}
				}
				
				// 增值业务版本
				DialogUtil.showProgress("正在获取增值业务版本...");
				if(mBusiness00.bll0021((byte)0x03, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcincrement.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x03, true);
					}
					else
					{
						refreshUpdateStatus(0x03, false);
					}
				}
				
				// 特服业务版本
				DialogUtil.showProgress("正在获取特服业务版本...");
				if(mBusiness00.bll0021((byte)0x04, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcespeciallyservice.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x04, true);
					}
					else
					{
						refreshUpdateStatus(0x04, false);
					}
				}
				
				// 选号规则版本
				DialogUtil.showProgress("正在获取选号规则版本...");
				if(mBusiness00.bll0021((byte)0x05, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcselnorule.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x05, true);
					}
					else
					{
						refreshUpdateStatus(0x05, false);
					}
				}
				
				// 品牌版本
				DialogUtil.showProgress("正在获取品牌版本...");
				if(mBusiness00.bll0021((byte)0x06, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crcbrand.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x06, true);
					}
					else
					{
						refreshUpdateStatus(0x06, false);
					}
				}
				else
				{
					DialogUtil.closeProgress();
					back(null, null, "系统更新失败...", "确定", null, false, false, false);
					return;
				}
				
				// 3G套餐版本
				DialogUtil.showProgress("正在获取3G套餐版本...");
				if(mBusiness00.bll0021((byte)0x0A, crc, false) == ReturnEnum.SUCCESS)
				{
					if(!CRCData.crc3gmeal.equalsIgnoreCase(Commen.hax2str(crc)))
					{
						refreshUpdateStatus(0x0A, true);
					}
					else
					{
						refreshUpdateStatus(0x0A, false);
					}
				}
				
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 刷新升级状态
	private void refreshUpdateStatus(final int type, final boolean status)
	{
		BusiUpdate.this.runOnUiThread(new Runnable()
		{
			public void run()
			{
				switch(type)
				{
				case 0x01:
					mTextMenuUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x02:
					mTextPackage2GUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x03:
					mTextAddValueUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x04:
					mTextSpecialUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x05:
					mTextNumberUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x06:
					mTextBrandUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				case 0x0A:
					mTextPackage3GUpdate.setVisibility(status? View.VISIBLE:View.GONE);
					break;
					
				default:
					break;
				}
			}
		});
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}
	
	@Override
	public void onInitFail() 
	{
		
	}

	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
					
		switch(v.getId())
		{
		// 菜单版本更新
		case R.id.tv_menu_update:
			procBusiSingleUpdate(0x01);
			break;
		
		// 2G套餐版本更新
		case R.id.tv_package2g_update:
			procBusiSingleUpdate(0x02);
			break;
			
		// 增值业务版本更新
		case R.id.tv_addvalue_update:
			procBusiSingleUpdate(0x03);
			break;
			
		// 特服业务版本更新
		case R.id.tv_special_update:
			procBusiSingleUpdate(0x04);
			break;
			
		// 选号规则更新
		case R.id.tv_number_update:
			procBusiSingleUpdate(0x05);
			break;
			
		// 品牌版本更新
		case R.id.tv_brand_update:
			procBusiSingleUpdate(0x06);
			break;
			
		// 3G套餐版本更新
		case R.id.tv_package3g_update:
			procBusiSingleUpdate(0x0A);
			break;
			
		// 立即更新
		case R.id.btn_busi_update:
			procBusiUpdate();
			break;
			
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	// 处理单步更新
	private void procBusiSingleUpdate(final int type)
	{
		new Thread()
		{
			public void run()
			{
				byte[] crc = new byte[2];
				DialogUtil.showProgress("正在更新...");
				if(mBusiness00.bll0021((byte)type, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(type, false);
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 处理更新
	private void procBusiUpdate() 
	{
		new Thread()
		{
			public void run()
			{
				byte[] crc = new byte[2];
				// 菜单版本
				DialogUtil.showProgress("正在更新菜单...");
				if(mBusiness00.bll0021((byte)0x01, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x01, false);
				}
				
				// 2G套餐版本
				DialogUtil.showProgress("正在更新2G套餐...");
				if(mBusiness00.bll0021((byte)0x02, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x02, false);
				}
				
				// 增值业务版本
				DialogUtil.showProgress("正在更新增值业务...");
				if(mBusiness00.bll0021((byte)0x03, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x03, false);
				}
				
				// 特服业务版本
				DialogUtil.showProgress("正在更新特服业务...");
				if(mBusiness00.bll0021((byte)0x04, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x04, false);
				}
				
				// 选号规则版本
				DialogUtil.showProgress("正在更新选号规则...");
				if(mBusiness00.bll0021((byte)0x05, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x05, false);
				}
					
				// 品牌版本
				DialogUtil.showProgress("正在更新品牌...");
				if(mBusiness00.bll0021((byte)0x06, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x06, false);
				}
				
				// 收件箱
				DialogUtil.showProgress("正在更新收件箱...");
				mBusiness00.bll0021((byte)0x09, crc, true);
				
				// 3G套餐版本
				DialogUtil.showProgress("正在更新3G套餐...");
				if(mBusiness00.bll0021((byte)0x0A, crc, true) == ReturnEnum.SUCCESS)
				{
					refreshUpdateStatus(0x0A, false);
				}
				
				DialogUtil.closeProgress();
				back(null, null, "系统更新完毕!", "确定", null, false, false, false);
			}
		}.start();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
