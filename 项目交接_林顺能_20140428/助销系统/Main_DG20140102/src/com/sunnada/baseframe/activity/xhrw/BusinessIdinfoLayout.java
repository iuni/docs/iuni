package com.sunnada.baseframe.activity.xhrw;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.bean.IdentifyMsg;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.identify.IdentifyScan;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.PictureShowUtils;
import com.sunnada.baseframe.util.FileOpr;
import com.sunnada.baseframe.util.StringUtil;

public class BusinessIdinfoLayout extends UILogic implements OnClickListener, ViewGetter,OnTouchListener
{
	public  int			        ICCID_LENGTH        = 19;
	
	private TextView			mTvIdType;					// 证件类型
	private View				mLayCover;					// 覆盖屏幕
	private ListView			mLvIdType;					// 证件类型
	
	private EditText			mEtDate				= null; // 有效期
	private EditText			mEtName				= null; // 姓名
	private EditText			mEtIdNum			= null; // 证件号码
	private EditText			mEtAddress			= null; // 地址
	private EditText			mEtIccid			= null; // 空卡卡号
	
	private ButtonGroupUtil		util;                       // 资费组
	private ButtonGroupUtil		sexUtil;                    // 性别组
	
	private Button				mBtnReadIccid		= null; // 读取空卡信息
	private Button              mBtnPrevious;               // 回退按钮
	private LinearLayout        mLayBack;                   // 回退LinearLayout
	private MyCustomButton      mBtnPre;                    // 上一步
	private MyCustomButton	    mBtnNext;                   // 开户
	
	private ImageView			mIvIdentifyScan1	= null; // 身份证扫描正面
	private ImageView			mIvIdentifyScan2	= null; // 身份证扫描反面
	
	private ImageView			mIvTipName;
	private ImageView			mIvTipDate;
	private ImageView			mIvTipIdNum;
	private ImageView			mIvTipAddress;
	private ImageView			mIvTipIccid;
	
	private RelativeLayout   	mRelativeLayout1;
	private RelativeLayout      mRelativeLayout2;
	
	private String				sPicFolder			= "";	// 暂存图片文件的文件夹名
	private String				filePath1			= null;
	private String				filePath2			= null;
	private String				mIdType				= "02"; // 证件类型编码
	private List<Object>		mListIdType;                // 证件编码列表

	private PickNetBusiness     mPickNetBusiness    = null;
	private Context				context;
	
	// 性别组Id
	private int[]               mSexUtilIds         = {R.id.btn_sex_male, R.id.btn_sex_female};
	// 资费组Id
	private int[]               mUtilIds            = {R.id.btn_standard, R.id.btn_all, R.id.btn_half};

	// 使用ButtonGroupUtil改变字体颜色设置
	private final int[][]       mFontColor          = {{0xff,0xff,0xff,0xff,0xff},{0xff,0x67,0xa7,0xc1}};
	
	BusinessIdinfoLayout(NumberSelectActivity2 numberSelect) 
	{
		super(numberSelect);
	}
	
	protected void onCreate(Bundle savedInstanceState)
	{
		//super.onCreate(savedInstanceState);
		mNumberSelect.setContentView(R.layout.lay_picknetwork_business_idinfo2);

		// 初始化控件
		initView();
		// 注册监听
		setListenner();
		// 初始化数据
		initData();
	}

	private void initView()
	{
		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;
		
		// 回退LinearLayout
		mLayBack = (LinearLayout)mNumberSelect.findViewById(R.id.layBack);
		// 回退按钮
		mBtnPrevious = (Button) mNumberSelect.findViewById(R.id.btn_previous);
		
		// 导航栏
		// mTvSelectedNum = (TextView) mNumberSelect.findViewById(R.id.tv_selected_num);
		// mTvSelectedPackage = (TextView) mNumberSelect.findViewById(R.id.tv_selected_package);
		
		// 证件类型
		// mLayIdType = mNumberSelect.findViewById(R.id.layIdType);
		mTvIdType  = (TextView) mNumberSelect.findViewById(R.id.tvIdType);
		mLvIdType  = (ListView) mNumberSelect.findViewById(R.id.listIdType);
		mLayCover  = mNumberSelect.findViewById(R.id.lay_cover);
		
		// 用户信息
		mEtDate = (EditText) mNumberSelect.findViewById(R.id.et_id_date);
		mEtName = (EditText) mNumberSelect.findViewById(R.id.et_id_name);
		mEtIdNum = (EditText) mNumberSelect.findViewById(R.id.et_id_num); 
		mEtAddress = (EditText) mNumberSelect.findViewById(R.id.et_id_address);
		
		// 正反面拍照
		mIvIdentifyScan1 = (ImageView) mNumberSelect.findViewById(R.id.iv_id_1);
		mIvIdentifyScan2 = (ImageView) mNumberSelect.findViewById(R.id.iv_id_2);
		
		// 读卡相应控件
		mEtIccid = (EditText) mNumberSelect.findViewById(R.id.et_iccid);
		// 不支持手动输入
		mEtIccid.setEnabled(true);
		
		mBtnReadIccid = (Button) mNumberSelect.findViewById(R.id.btn_read_iccid);
		
		mIvTipAddress = (ImageView) mNumberSelect.findViewById(R.id.iv_tip_address);
		mIvTipDate    = (ImageView) mNumberSelect.findViewById(R.id.iv_tip_date);
		mIvTipIccid   = (ImageView) mNumberSelect.findViewById(R.id.iv_tip_iccid);
		mIvTipIdNum   = (ImageView) mNumberSelect.findViewById(R.id.iv_tip_idnum);
		mIvTipName    = (ImageView) mNumberSelect.findViewById(R.id.iv_tip_name);
		
		// 初始化性别组
		try
		{
			sexUtil = ButtonGroupUtil.createBean(this, new int[]
			{ R.id.btn_sex_male, R.id.btn_sex_female }, new String[]
			{ "男", "女" }, R.color.btn_id_common, R.drawable.btn_tip_selected, mFontColor);
			sexUtil.setSelected(0);
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		// 初始化资费组
		try
		{
			String[] strcode = new String[3];
			String[] strtext = new String[3];
			for (int i = 0; i < PickNetBusiness.productZifei.size(); i++)
			{
				Productzifei pz = PickNetBusiness.productZifei.get(i);
				strcode[i] = pz.getCode() + "";
				strtext[i] = pz.getName();
			}
			util = ButtonGroupUtil.createBean(this, new int[]
			{ R.id.btn_standard, R.id.btn_all, R.id.btn_half }, strcode, strtext, R.color.btn_id_common,
					R.drawable.btn_tip_selected, mFontColor);
			util.setSelected(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		// 上一步按钮
		mBtnPre = (MyCustomButton) mNumberSelect.findViewById(R.id.btn_pre);
		mBtnPre.setTextViewText1("返回");
		mBtnPre.setImageResource(R.drawable.btn_back);
		mBtnPre.setOnTouchListener(this);
		
		// 下一步按钮
		mBtnNext = (MyCustomButton) mNumberSelect.findViewById(R.id.btn_next);
		mBtnNext.setTextViewText1("确定");
		mBtnNext.setImageResource(R.drawable.btn_custom_check);
		mBtnNext.setOnTouchListener(this);
		
		mRelativeLayout1 = (RelativeLayout) this.findViewById(R.id.relativeLayout1);
		mRelativeLayout2 = (RelativeLayout) this.findViewById(R.id.relativeLayout2);
		mRelativeLayout1.getBackground().setAlpha(30);
		mRelativeLayout2.getBackground().setAlpha(30);
	}

	private void setListenner()
	{
		//mLayIdType.setOnClickListener(this);
		mLayCover.setOnClickListener(this);
		
		mBtnPre.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
		mBtnPrevious.setOnClickListener(this);
		mLayBack.setOnClickListener(this);
		
		mIvIdentifyScan1.setOnClickListener(this);
		mIvIdentifyScan2.setOnClickListener(this);
		mBtnReadIccid.setOnClickListener(this);
		
		mLvIdType.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView tv = (TextView) view.findViewById(R.id.tv_text);
				mTvIdType.setText(tv.getText().toString());
				mIdType = "0"+Integer.toString((position+2));
				
				mLayCover.setVisibility(View.GONE);
			}
		});
		
		// 性别Util
		sexUtil.setClickListener(new ButtonGroupClickListener() 
		{
			@Override
			public void onClick(int previousId, int newId) 
			{
				for(int i = 0;i < mSexUtilIds.length;i++)
				{
					if(mSexUtilIds[i] == newId)
					{
						mPickNetBusiness.mSelectSexUtilPos = i;
						break;
					}
				}
			}
		});
		
		// 资费Util 
		util.setClickListener(new ButtonGroupClickListener() 
		{
			@Override
			public void onClick(int previousId, int newId) 
			{
				for(int i = 0;i < mUtilIds.length;i++)
				{
					if(mUtilIds[i] == newId)
					{
						mPickNetBusiness.mSelectUtilPos = i;
						break;
					}
				}
			}
		});
		// 对话框输入监听，在对话框后面提示输入是否正确
		addEditextListernner();
	}

	// 对话框输入监听，在对话框后面提示输入是否正确
	private void addEditextListernner()
	{
		mEtName.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipName.setVisibility(View.GONE);
				}
				else
				{
					mIvTipName.setVisibility(View.VISIBLE);
					if (!InputRules.isNameValid(mIdType, s.toString()))
					{
						mIvTipName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipName.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
			
		mEtIdNum.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIdNum.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIdNum.setVisibility(View.VISIBLE);
					if (!InputRules.isNoValid(mIdType, s.toString()))
					{
						mIvTipIdNum.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipIdNum.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
			
		mEtDate.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipDate.setVisibility(View.GONE);
				}
				else
				{
					mIvTipDate.setVisibility(View.VISIBLE);
					if (!InputRules.checkIDEndTime(s.toString()).equals(""))
					{
						mIvTipDate.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipDate.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
			
		mEtAddress.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipAddress.setVisibility(View.GONE);
				}
				else
				{
					mIvTipAddress.setVisibility(View.VISIBLE);
					if (InputRules.isAddressValid(mIdType, s.toString()) != 0)
					{
						mIvTipAddress.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipAddress.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
			
		mEtIccid.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIccid.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIccid.setVisibility(View.VISIBLE);
					if (s.length() != ICCID_LENGTH) 
					{
						mIvTipIccid.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipIccid.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
	}
		
	// 初始化数据
	private void initData()
	{	
		mListIdType = new ArrayList<Object>();
		mListIdType.add("18位身份证");
		//mListIdType.add("驾驶证");
		//mListIdType.add("军官证");
		//mListIdType.add("护照");
		ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(mNumberSelect, R.layout.lay_id_type_list_item,
				R.id.tv_text, mListIdType);
		mLvIdType.setAdapter(adapter);
		
		mTvIdType.setText(mListIdType.get(0).toString());
		resetDate();
	}

	// 重置输入数据
	public void resetDate()
	{
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.IdentityEndtime))
		{
			mEtDate.setText(mPickNetBusiness.IdentityEndtime);
		}
		
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.username))
		{
			mEtName.setText(mPickNetBusiness.username);
		}
		
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.Identity))
		{
			mEtIdNum.setText(mPickNetBusiness.Identity);
		}
		
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.address))
		{
			mEtAddress.setText(mPickNetBusiness.address);
		}
		
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.iccid))
		{
			mEtIccid.setText(mPickNetBusiness.iccid);
		}
		
		sexUtil.setSelected(mPickNetBusiness.mSelectSexUtilPos);
		util.setSelected(mPickNetBusiness.mSelectUtilPos);
		
		
		mIvIdentifyScan1.setImageResource(R.drawable.iv_id_1);
		mIvIdentifyScan2.setImageResource(R.drawable.iv_id_2);
		
		sPicFolder = FileOpr.getSdcardPath() + mNumberSelect.getString(R.string.identify_photo_path)
				+ String.valueOf(System.currentTimeMillis());
		filePath1 = sPicFolder + "/identify_1.jpg";
		filePath2 = sPicFolder + "/identify_2.jpg";
	}
	
	@Override
	public void onClick(View v)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 500)) 
		{
			return;
		}
				
		Message msg = new Message();

		switch (v.getId())
		{
			case R.id.iv_id_1: // 身份证扫描正面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath1);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath1, 206, 130);

							mIvIdentifyScan1.post(new Runnable()
							{
								@Override
								public void run()
								{
									if (!StringUtil.isEmptyOrNull(msg.identify_no))
									{
										mEtIdNum.setText(msg.identify_no);
									}
									if (!StringUtil.isEmptyOrNull(msg.sex))
									{
										if ("01".equals(msg.sex))
										{
											sexUtil.setSelected(1);
										}
										else
										{
											sexUtil.setSelected(0);
										}
									}
									if (!StringUtil.isEmptyOrNull(msg.name))
									{
										mEtName.setText(msg.name);
									}
									if (!StringUtil.isEmptyOrNull(msg.end_time))
									{
										mEtDate.setText(msg.end_time);
									}
									if (!StringUtil.isEmptyOrNull(msg.address))
									{
										mEtAddress.setText(msg.address);
									}
									if (bitmap2 != null)
									{
										mIvIdentifyScan1.setImageBitmap(bitmap2);
									}
								}
							});
						}
					}
				}.start();
				break;
				
			case R.id.iv_id_2:// 身份证扫描反面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath2);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath2, 206, 130);

							mIvIdentifyScan2.post(new Runnable()
							{
								@Override
								public void run()
								{
									if (msg != null)
									{
										if (!StringUtil.isEmptyOrNull(msg.identify_no))
										{
											mEtIdNum.setText(msg.identify_no);
										}
										if (!StringUtil.isEmptyOrNull(msg.sex))
										{
											if ("01".equals(msg.sex))
											{
												sexUtil.setSelected(1);
											}
											else
											{
												sexUtil.setSelected(0);
											}
										}
										if (!StringUtil.isEmptyOrNull(msg.name))
										{
											mEtName.setText(msg.name);
										}
										if (!StringUtil.isEmptyOrNull(msg.end_time))
										{
											mEtDate.setText(msg.end_time);
										}
										if (!StringUtil.isEmptyOrNull(msg.address))
										{
											mEtAddress.setText(msg.address);
										}
										if (bitmap2 != null)
										{
											mIvIdentifyScan2.setImageBitmap(bitmap2);
										}
									}
								}
							});
						}
					}
				}.start();
				break;
				
			case R.id.btn_pre:
				backToPrevious();
				break;
				
			case R.id.btn_next:
				if(Statics.IS_DEBUG)
				{
					//bundle.putString("className", PickNetWorkNetAccessPermitsActivity.class.getName());
					msg.what = FrameActivity.DIRECTION_NEXT;
					//msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					return;
				}
				
				String checkResult = checkAllInput();
				
				if(StringUtil.isEmptyOrNull(checkResult))
				{
					getData();

					//bundle.putString("className", PickNetWorkNetAccessPermitsActivity.class.getName());
					//bundle.putString("name", mEtName.getText().toString());
					//bundle.putString("id_no", mEtIdNum.getText().toString());
					msg.what = FrameActivity.DIRECTION_NEXT;
					//msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
				}
				else
				{
					/*
					back(null, "温馨提示", "开户信息未填写完整，请补充以下信息：\n"+checkResult,
							"确定", null, false, false, false);
							*/
					return;
				}
				break;

			case R.id.btn_read_iccid:
				if (Statics.IS_DEBUG)
				{
					DialogUtil.showProgress("正在读取卡号,请稍候");
					new Thread()
					{
						public void run()
						{
							try
							{
								final String iccid = NumberSelectActivity2.equipmentService.readIccid();
								mNumberSelect.runOnUiThread(new Runnable()
								{
									public void run()
									{
										mEtIccid.setText(iccid);
									}
								});
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
							DialogUtil.closeProgress();
						};
					}.start();
				}
				else
				{
					DialogUtil.showProgress("正在读取卡号,请稍候");
					readIccid();
				}
				break;
				
			// 证件类型
			case R.id.layIdType:
				mLayCover.setVisibility(View.VISIBLE);
				break;
				
			// 覆盖全局
			case R.id.lay_cover:
				mLayCover.setVisibility(View.GONE);
				break;
				
			case R.id.btn_previous:
				//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
				break;
			
			case R.id.layBack:
				//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
				break;
			default:
				break;
		}
	}

	public void readIccid()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					int iswhiteCard = NumberSelectActivity2.equipmentService.isWhiteCard((byte) 1);
					// 白卡
					if (iswhiteCard == 1)
					{
						mPickNetBusiness.openCardType = 1;
						final String iccid = NumberSelectActivity2.equipmentService.readIccid();
						mPickNetBusiness.iccid = iccid;
						if (iccid != null)
						{
							mNumberSelect.runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									ICCID_LENGTH = 20;
									mEtIccid.setText(iccid);
									Toast.makeText(context, "当前为白卡，读卡成功", Toast.LENGTH_SHORT).show();
								}
							});
						}
						else
						{
							mNumberSelect.runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									//back(null, null, "读取失败", "确定", null, false, false, false);
								}
							});
						}
					}
					else if (iswhiteCard == 2)
					{
						// 成卡
						mPickNetBusiness.openCardType = 0;
						final String iccid = NumberSelectActivity2.equipmentService.readIccid();
						mPickNetBusiness.iccid = iccid.substring(0, 19);
						if (iccid != null)
						{
							mNumberSelect.runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									ICCID_LENGTH = 19;
									mEtIccid.setText(mPickNetBusiness.iccid);
									Toast.makeText(context, "当前为成卡，读卡成功", Toast.LENGTH_SHORT).show();
								}
							});
						}
					}
					else
					{
						mNumberSelect.runOnUiThread(new Runnable()
						{
							public void run()
							{
								DialogUtil.closeProgress();
								//back(null, null, "读取失败", "确定", null, false, false, false);
							}
						});
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					mNumberSelect.runOnUiThread(new Runnable()
					{
						public void run()
						{
							DialogUtil.closeProgress();
							Toast.makeText(context, "当前为白卡，读卡成功", Toast.LENGTH_SHORT).show();
						}
					});
				}
			};
		}.start();
	}

	public void getData()
	{
		// 证件类型
		mPickNetBusiness.cust_type = mIdType;
		
		// 用户信息
		mPickNetBusiness.IdentityEndtime = mEtDate.getText().toString();
		mPickNetBusiness.username = mEtName.getText().toString();
		mPickNetBusiness.Identity = mEtIdNum.getText().toString();//  证件号码
		mPickNetBusiness.address = mEtAddress.getText().toString();

		// 卡号
		mPickNetBusiness.iccid = mEtIccid.getText().toString();
		mPickNetBusiness.sPicFolder = sPicFolder;
		
		// 性别信息收集
		try
		{
			if (sexUtil.getValue().equals("男"))
			{
				mPickNetBusiness.sex = "02";
			}
			else
			{
				mPickNetBusiness.sex = "01";
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		
		// 资费信息收集
		try
		{
			mPickNetBusiness.zife = Integer.parseInt(util.getValue());
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private String checkAllInput() 
	{
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append(checkMainInput());
		sBuffer.append(checkUserInfoInput());
		return sBuffer.toString();
	}
	
	// 主要的信息校验
	private String checkMainInput() 
	{
		StringBuffer sBuffer = new StringBuffer();

		if (mPickNetBusiness.openCardType == 0) 
		{
			if ("".equals(mEtIccid.getText().toString())) 
			{
				sBuffer.append("请获取卡号\n");
			}

			if (mEtIccid.getText().toString().length() < 19
					&& !mEtIccid.getText().toString().equals("")) 
			{
				sBuffer.append("输入的卡号长度不正确\n");
			}
		}
		else if(mPickNetBusiness.openCardType == 1)
		{
			if ("".equals(mEtIccid.getText().toString())) 
			{
				sBuffer.append("请获取卡号\n");
			}

			if (mEtIccid.getText().toString().length() < 20
					&& !mEtIccid.getText().toString().equals("")) 
			{
				sBuffer.append("输入的卡号长度不正确\n");
			}
		}
		return sBuffer.toString();
	}

	 // 检查用户信息的录入
	private String checkUserInfoInput() 
	{
		StringBuffer sBuffer = new StringBuffer();
		
		if (StringUtil.isEmptyOrNull(mEtIdNum.getText().toString())) 
		{
			sBuffer.append("证件号码为空，请正确输入证件号码\n");
		} 
		else 
		{
			if ("01".equals(mIdType) && (mEtIdNum.getText().toString().length() != 15)) 
			{
				sBuffer.append("证件号码输入有误，请输入15位身份证号\n");
			} 
			else if ("02".equals(mIdType) && (mEtIdNum.getText().toString().length() != 18)) 
			{
				sBuffer.append("证件号码输入有误，请输入18位身份证号\n");
			}
		}

		if (!InputRules.checkIDEndTime(mEtDate.getText().toString()).equals(""))
		{
			sBuffer.append(InputRules.checkIDEndTime(mEtDate.getText().toString())+"\n");
		} 
		
		if (StringUtil.isEmptyOrNull(mEtName.getText().toString())) 
		{
			sBuffer.append("请输入开户姓名\n");
		}
		
		if (StringUtil.isEmptyOrNull(mEtAddress.getText().toString())) 
		{
			sBuffer.append("请输入开户地址\n");
		}
		return sBuffer.toString();
	}
	
	private IdentifyMsg takePhoto(String path)
	{
		IdentifyScan photo = new IdentifyScan(mNumberSelect);
		int ret = photo.takePhoto(path);
		IdentifyMsg msg = null;
		if (ret == 1)
		{
			msg = photo.scanIdentifySigal(path);
		}
		return msg;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void backToPrevious()
	{
		getData();
		//bundle.putString("className", PickNetWorkPackageSelectActivity.class.getName());
		
		Message msg = Message.obtain();
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		//msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}

	protected void onNewIntent(Intent intent)
	{
		/*
		String selectedNum = bundle.getString("num");
		mTvSelectedNum.setText(selectedNum);	
		mTvSelectedPackage.setText(setStepTwoTvShow());
		initData();
		*/
	}

	protected void onResume()
	{
		/*
		String selectedNum = bundle.getString("num");
		mTvSelectedNum.setText(selectedNum);	
		mTvSelectedPackage.setText(setStepTwoTvShow());
		*/
	}

	public String setStepTwoTvShow()
	{
		String show = "";
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.packageName))
		{
			if(!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractName))
			{
				show = mPickNetBusiness.packageName+","+mPickNetBusiness.mContractName;
			}else
			{
				show = mPickNetBusiness.packageName;
			}
		}
		
		if(show != null)
		{
			if(show.length() > 12)
			{
				show = show.substring(0,12)+"...";
			}
		}
		else
		{
			show = "";
		}
		return show;
	}

	@Override
	public View findViewById(int id) 
	{
		return null;
	}

	@Override
	public void go() 
	{
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		return false;
	}
}	
