package com.sunnada.baseframe.activity.dztj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.HistoryPackageAdapter;
import com.sunnada.baseframe.adapter.MyGridViewAdapter;
import com.sunnada.baseframe.adapter.MyNewTelAdapter;
import com.sunnada.baseframe.bean.ConstantData;
import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0E;
import com.sunnada.baseframe.database.HistoryProductDao;
import com.sunnada.baseframe.database.HistoryRecommendPackageDao;
import com.sunnada.baseframe.database.ProductInfoDao;
import com.sunnada.baseframe.database.RecommendPackageDao;
import com.sunnada.baseframe.dialog.DetailParamDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ActivityDetailsDialog;
import com.sunnada.baseframe.dialog.PackageCompareDialog;
import com.sunnada.baseframe.dialog.PackageDetailsDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.BitmapPoolUtil.onImageGotListener;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.CollectionUtil;
import com.sunnada.baseframe.util.DimensionUtil;
import com.sunnada.baseframe.util.StringUtil;

public class RecommendActivity extends DgActivity implements OnClickListener,OnScrollListener
{
	public static final String		TITLE_STR			= "店长推荐业务办理提示";
	private LayoutInflater			mInflater;
	private Business_0E				mHaoka;
	private RecommendPackageDao		mPackageParamsDao;
	private ProductInfoDao			mProductInfoDao;
	private View					mLayBack;			// 返回按钮
	private List<RecommendPackage>	mCurDataList;		// 当前数据
	private RecommendPackage		mCurPackage;		// 当前选中机型
	
	// 主界面
	private Button 					mBtnPrevious;		// 返回
	private GridView				mGridContent;		// 网格布局
	private MyGridViewAdapter		mGridAdapter;		// 网格适配器
	private int						mCompareCount;		// 加入对比机型数量 
	private TextView				mTvCompareCount;	// 加入对比机型数量显示
	private Animation				mCountAnim;			// 添加新机型到对比栏动画提示
	private ImageView				mBead;				// 指示球
	private TextView				mTvMostIn;			// 最IN机型
	private TextView				mTvProduct;			// 套餐靓号
	private boolean 				mIsMostIn;			// 当前是否处于最IN机型

	// 手机详细参数
	private String[] 				mLocalPaths;		// 手机图片本地路径
	private String[] 				mServerPaths;		// 手机图片服务器路径
	private View					mLayPhoneDetails;	// 手机详细参数界面
	private MyCustomButton			mBtnBack;			// 返回
	private MyCustomButton			mBtnPurchase;		// 立即购买
	private Button					mBtnAddToCompare;	// 加入对比
	private ImageView				mCurrImg;			// 当前手机大图
	//private LinearLayout			mLayImgcontainer;	// 展示手机大图片布局
	private int						mCurrIndex;			// 当前下标
	private List<LinearLayout> 		mIconList;			// 缩略图List
	private LinearLayout			mLayIcons;			// 缩略图布局
	private TextView				mTvTelNum;			// 选择手机号码
	private CheckBox				mCheckNum;			// 确定选中的号码
	private TextView 				mTvMarkTag;			// 营销口号
	private TextView				mTvPrice;			// 价格
	private TextView				mTvSoldCount;		// 已出售
	private	LinearLayout 			mLayCountSold;		// 已出售整行布局
	private TextView				mTvNumBelong;		// 号码归属地
	private TextView				mTvActivity;		// 活动
	private TextView				mTvPackage;			// 套餐
	private TextView				mTvPhoneDetails; 	// 手机参数详情
	private TextView				mTvActivityDetails; // 活动详情
	private TextView				mTvPackageDetails;	// 套餐详情
	
	private DetailParamDialog 		mDlgPhoneDetails;    // 手机参数信息对话框
	private ActivityDetailsDialog	mDlgActivityDetails;// 活动详情对话框
	private PackageDetailsDialog 	mDlgPackageDetails;	// 套餐详情对话框
	
	// 号码选择框
	//private	PopupWindow			mPopNumChoose;		// 号码选择框
	private View					mLayNumChoose;		// 号码选择界面
	private ImageView				mIvClose;			// 关闭选择号码对话框
	private ImageView 				mBtnSearch;			// 搜索
	private ImageView				mIvDropPayType;		// 付费类型
	private ImageView				mIvDropNumRules;	// 靓号规则下拉按钮
	private ImageView				mIvDropFee;			// 话费区间下拉按钮
	//private EditText				mEtLast4;			// 号码后四位输入框
	private View					mLayChooseNumCover;	// 选择号码覆盖全局界面
	private ListView				mLvPayType;			// 付费类型ListView
	private ListView				mLvNumRules;		// 靓号规则ListView
	private ListView				mLvFee;				// 费用区间ListView
	private ArrayAdapter<Object>	mArrAdapter;		// String 适配器
	private TextView				mTvPayType;			// 付费类型
	private TextView				mTvNumRules;		// 靓号规则
	private TextView				mTvFee;				// 费用区间
	private ListView           	 	mLvNum;             // 号码ListView
	private MyCustomButton2     	mBtnSelect;         // 确定按钮
	private MyNewTelAdapter     	mTelAdapter;        // 号码列表适配器
	private View					mMoreView;          // 加载更多页面
	private int                 	mLastItem;  		// 上次选中项
	private boolean 				mIsScrollOver;		// 号码列表下拉滚动是否结束
	// 号码数据
	private List<String>        	mNumList;           // 号码列表
	private List<String>        	mFeeList;           // 费用列表       
	
	// 历史记录
	private Button 					mBtnHistory;		// 历史记录按钮
	private PopupWindow				mPopHistory;		// 历史记录PopupWindow
	private ListView				mLvHistory;			// 历史浏览ListView
	private Button					mBtnClearHistory;	// 清除历史记录按钮
	private Button					mBtnHistory_hide;	// 隐藏历史记录
	private TextView				mTvNoHistory;		// 没有历史浏览记录
	private List<RecommendPackage>	mCurHistoryData;	// 当前浏览记录
	private HistoryRecommendPackageDao mHistoryPackDao;	// 推荐包历史浏览数据库
	private HistoryProductDao		mHistoryProductDao;	// 套餐靓号历史浏览记录
	private BaseAdapter				mHistoryAdapter;	// 历史浏览适配器
	
	// 对比框
	private RecommendPackage[]		mCurCompares;
	private RecommendPackage[]		mCompareProducts;
	private RecommendPackage[]		mComparePackages;
	private PopupWindow				mPopCompare;		// 对比框PopupWindow
	private Button					mBtnShowCompare;	// 弹出对比框
	private Button					mBtnHideCompare;	// 关闭对比框
	private TextView				mTvCompareCount2;	// 弹出对比框后的加入对比机型数量显示
	private LinearLayout			mLayCompare1;		// 对比框1
	private LinearLayout			mLayCompare2;		// 对比框2
	private LinearLayout			mLayCompare3;		// 对比框3
	private Button					mBtnCompareClose1;	// 关闭对比框1按钮
	private Button					mBtnCompareClose2;	// 关闭对比框2按钮
	private Button					mBtnCompareClose3;	// 关闭对比框3按钮
	
	private int						mPayType 	= 0;	// 付费类型
	private int						mSelRule1 	= 0;	// 选号规则1, 靓号规则
	private int						mSelRule2	= 0;	// 选号规则2, 号段
	private int						mSelRule3	= 0;	// 选号规则3, 话费区间
	private int						mNoRuleCode	= 0;	// 靓号规则编码
	private String					mStrLast4	= "";	// 号码后四位
	private String					mStrFirst3	= "";	// 三位号首
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_recommend_activity);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_recommend_activity_orange);
		}
		mHaoka = new Business_0E(this, equipmentService, socketService, dataBaseService);
		FrameActivity.mBaseBusiness = mHaoka;
		System.gc();
		
		new Thread()
		{
			public void run() 
			{
				Log.d("", "检测异常交易...");
				if(errorDealCheck(new String[] {"09"}) == false) 
				{
					// 获取靓号规则
					getBeautyRules();
				}
			}
		}.start();
	}
	
	// 获取靓号规则
	private void getBeautyRules() 
	{
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获靓号规则, 请稍候...");
				SystemClock.sleep(100);
				// 获取靓号规则
				boolean result = mHaoka.bll0E01();
				DialogUtil.closeProgress();
				if(result == false) 
				{
					DialogUtil.MsgBox(TITLE_STR, mHaoka.getLastknownError());
				}
				// 初始化界面
				runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						// 初始化控件
						initView();
						// 初始显示最IN机型
						inMostIn();
					}
				});
			}
		}.start();
	}
	
	// 异常交易处理完成的回调
	@Override
	protected void errorDealCheckCallBack() 
	{
		// 获取靓号规则
		getBeautyRules();
	}
	
	// 初始化控件
	private void initView() 
	{
		mInflater = LayoutInflater.from(this);
		mPackageParamsDao = new RecommendPackageDao(this);
		mProductInfoDao = new ProductInfoDao(this);
		mComparePackages = new RecommendPackage[3];
		mCompareProducts =  new RecommendPackage[3];
		mCurCompares = new RecommendPackage[3];
		mCountAnim = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
		mCurrIndex = 0;
		// 重置每个推荐包的比较选中状态
		resetCompareStatus();
		// 初始化号码选择框
		initPopNumChoose(); 
		// 初始化历史记录界面
		initPopHistory();
		// 初始化手机详情信息界面
		initPhoneDetailsView();
		// 初始化对比界面
		initPopCompare();
		// 初始化浏览记录
		//updateHistory();
		
		// 主界面
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		mGridContent = (GridView) findViewById(R.id.layContent);
		mGridContent.setVerticalFadingEdgeEnabled(true);
		mGridContent.setFadingEdgeLength(60);
		mTvCompareCount = (TextView) findViewById(R.id.tvCompare_count);
		mBead = (ImageView) findViewById(R.id.bead);
		mTvMostIn = (TextView) findViewById(R.id.tvMostIn);
		mTvProduct = (TextView) findViewById(R.id.tvProduct);
		mTvMostIn.setOnClickListener(this);
		mTvProduct.setOnClickListener(this);
	}
	
	// 重置所有选中状态
	private void resetCompareStatus()
	{
		if(!CollectionUtil.isEmptyOrNull(ConstantData.mRecommendPackgeList))
		{
			for(RecommendPackage pack : ConstantData.mRecommendPackgeList)
			{
				pack.setmIsSelected(false);
			}
		}
		
		if(!CollectionUtil.isEmptyOrNull(ConstantData.mProductList))
		{
			for(RecommendPackage pack : ConstantData.mProductList)
			{
				pack.setmIsSelected(false);
			}
		}
	}

	// 最IN机型界面
	private void inMostIn() 
	{
		DialogUtil.showProgress("正在加载数据...");
		if(mLayPhoneDetails.isShown()) 
		{
			mLayPhoneDetails.setVisibility(View.GONE);
			mGridContent.setVisibility(View.VISIBLE);
		}
		if(!mIsMostIn)
		{
			mIsMostIn = true;
			mTvMostIn.setTextColor(Color.WHITE);
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mTvProduct.setTextColor(Color.parseColor("#9abac9"));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mTvProduct.setTextColor(Color.parseColor("#c7c7c7"));
			}
			// 若内存中为空，从数据库读取数据
			if(CollectionUtil.isEmptyOrNull(ConstantData.mRecommendPackgeList))
			{
				ConstantData.mRecommendPackgeList = mPackageParamsDao.getAllRecommendPackages();
			}
			// 切换之前保留套餐靓号的对比数据
			mCurDataList = ConstantData.mRecommendPackgeList;
			mCompareProducts = mCurCompares.clone();
			clearCompares();
			mCurCompares = mComparePackages;
			initGridView("推荐包");
		}
		else
		{
			DialogUtil.closeProgress();
		}
	}
	
	// 套餐靓号界面
	private void inProduct() 
	{
		DialogUtil.showProgress("正在加载数据...");
		if(mLayPhoneDetails.isShown())
		{
			mLayPhoneDetails.setVisibility(View.GONE);
			mGridContent.setVisibility(View.VISIBLE);
		}
		if(mIsMostIn)
		{
			mIsMostIn = false;
			mTvProduct.setTextColor(Color.WHITE);
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mTvMostIn.setTextColor(Color.parseColor("#9abac9"));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mTvMostIn.setTextColor(Color.parseColor("#c7c7c7"));
			}
			// 若内存中为空，从数据库读取数据
			if(CollectionUtil.isEmptyOrNull(ConstantData.mProductList))
			{
				ConstantData.mProductList = mProductInfoDao.getAllProducts();
			}
			mCurDataList = ConstantData.mProductList;
			// 切换之前保留推荐包的对比数据
			mComparePackages = mCurCompares.clone();
			clearCompares();
			mCurCompares = mCompareProducts;
			initGridView("套餐靓号");
		}
		else
		{
			DialogUtil.closeProgress();
		}
	}
	
	// 情况对比框
	private void clearCompares()
	{
		closeCompare(0);
		closeCompare(1);
		closeCompare(2);
		mCompareCount = 0;
		mTvCompareCount.setText(0 + "");
		mTvCompareCount2.setText(0 + "");
	}

	// 初始主界面网格数据
	private void initGridView(final String type)
	{
		if(mCurDataList == null || mCurDataList.size() <= 0)
		{
			DialogUtil.closeProgress();
			DialogUtil.MsgBox(TITLE_STR, type +"数据未下发，请联系管理员！", 
			"确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(type.equals("推荐包"))
					{
						mTvProduct.performClick();
					}
					else
					{
						finishAct();
					}
				}
			},
			"", null, 
			new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(type.equals("推荐包"))
					{
						mTvProduct.performClick();
					}
					else
					{
						finishAct();
					}
				}
			});
			return;
		}
		// 恢复上次选中状态
		if(mCurCompares[0] != null)
		{
			mCurDataList.get(mCurDataList.indexOf(mCurCompares[0])).setmIsSelected(true);
		}
		if(mCurCompares[1] != null)
		{
			mCurDataList.get(mCurDataList.indexOf(mCurCompares[1])).setmIsSelected(true);
		}
		if(mCurCompares[2] != null)
		{
			mCurDataList.get(mCurDataList.indexOf(mCurCompares[2])).setmIsSelected(true);
		}
		
		if(mGridAdapter != null)
		{
			mGridAdapter.resycleImage();
		}
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mGridAdapter = new MyGridViewAdapter(this, R.layout.dztj_item, mCurDataList);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mGridAdapter = new MyGridViewAdapter(this, R.layout.dztj_item_orange, mCurDataList);
		}
		mGridContent.setAdapter(mGridAdapter);
		mCurPackage = null;
		mCurrImg.setImageBitmap(null);
		mCurHistoryData = null; 
		mTvTelNum.setText(Html.fromHtml("<u>" + "点击选择号码" + "</u>"));
		mCheckNum.setVisibility(View.GONE);
		// 更新对比框内容
		updateCompareView();
		// 更新历史记录
		updateHistory();
		DialogUtil.closeProgress();
	}
	
	// 更新对比框内容
	private void updateCompareView()
	{
		mCompareCount = 0;
		if (mCurCompares[0] != null)
		{
			mLayCompare1.setBackgroundResource(R.drawable.bg_compare_on);
			mBtnCompareClose1.setVisibility(View.VISIBLE);
			mLayCompare1.addView(getModelView(mCurCompares[0], R.layout.lay_compare_item_pack));
			mCompareCount ++;
		}
		if (mCurCompares[1] != null)
		{
			mLayCompare2.setBackgroundResource(R.drawable.bg_compare_on);
			mBtnCompareClose2.setVisibility(View.VISIBLE);
			mLayCompare2.addView(getModelView(mCurCompares[1], R.layout.lay_compare_item_pack));
			mCompareCount ++;
		}
		if (mCurCompares[2] != null)
		{
			mLayCompare3.setBackgroundResource(R.drawable.bg_compare_on);
			mBtnCompareClose3.setVisibility(View.VISIBLE);
			mLayCompare3.addView(getModelView(mCurCompares[2], R.layout.lay_compare_item_pack));
			mCompareCount ++;
		}
		// 更新小球上显示的数量
		mTvCompareCount.setText(mCompareCount + "");
		mTvCompareCount2.setText(mCompareCount + "");
	}

	// 初始化对比界面
	private void initPopCompare()
	{
		mBtnShowCompare = (Button) findViewById(R.id.btnShowCompare);
		mBtnShowCompare.setOnClickListener(this);
		
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView  = mInflater.inflate(R.layout.pop_compare, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = mInflater.inflate(R.layout.pop_compare_orange, null);
		}
		mPopCompare = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mPopCompare.setFocusable(true);
		mPopCompare.setTouchable(true);
		mPopCompare.setBackgroundDrawable(new BitmapDrawable());
		mPopCompare.setAnimationStyle(R.style.AnimationRight);
		
		mBtnHideCompare = (Button) contentView.findViewById(R.id.btnHideCompare);
		mBtnHideCompare.setOnClickListener(this);
		mTvCompareCount2 = (TextView) contentView.findViewById(R.id.tvCompareCount);
		
		mLayCompare1 = (LinearLayout) contentView.findViewById(R.id.layCompare1);
		mLayCompare2 = (LinearLayout) contentView.findViewById(R.id.layCompare2);
		mLayCompare3 = (LinearLayout) contentView.findViewById(R.id.layCompare3);
		mLayCompare1.setOnClickListener(this);
		mLayCompare2.setOnClickListener(this);
		mLayCompare3.setOnClickListener(this);
		mBtnCompareClose1 = (Button) contentView.findViewById(R.id.btn_compare_close1);
		mBtnCompareClose2 = (Button) contentView.findViewById(R.id.btn_compare_close2);
		mBtnCompareClose3 = (Button) contentView.findViewById(R.id.btn_compare_close3);
		mBtnCompareClose1.setOnClickListener(this);
		mBtnCompareClose2.setOnClickListener(this);
		mBtnCompareClose3.setOnClickListener(this);
		Button btnToCompare = (Button) contentView.findViewById(R.id.btnToCompare);
		btnToCompare.setOnClickListener(this);
	}

	// 初始化历史记录  
	private void initPopHistory()
	{
		mBtnHistory = (Button) findViewById(R.id.btnHistory);
		mBtnHistory.setOnClickListener(this);
		mHistoryPackDao = new HistoryRecommendPackageDao(this);
		mHistoryProductDao = new HistoryProductDao(this);
		mHistoryPackDao.clear();
		mHistoryProductDao.clear();
		mCurHistoryData = new ArrayList<RecommendPackage>();
		
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView  = mInflater.inflate(R.layout.pop_history, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = mInflater.inflate(R.layout.pop_history_orange, null);
		}
		mPopHistory = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mPopHistory.setFocusable(true);
		mPopHistory.setTouchable(true);
		mPopHistory.setBackgroundDrawable(new BitmapDrawable());
		mPopHistory.setAnimationStyle(R.style.AnimationRight); 

		mLvHistory = (ListView) contentView.findViewById(R.id.lv_history);
		mLvHistory.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				mCurPackage = mCurHistoryData.get(position);
				closePopWindow(mPopHistory); 
				showView(mCurPackage);
			}
		});
		mBtnClearHistory = (Button) contentView.findViewById(R.id.btn_clear_history);
		mBtnClearHistory.setOnClickListener(this);
		mBtnHistory_hide = (Button) contentView.findViewById(R.id.btn_history_hide);
		mBtnHistory_hide.setOnClickListener(this); 
		mTvNoHistory = (TextView) contentView.findViewById(R.id.tvNoHistory);
	}

	// 初始化号码选择框
	private void initPopNumChoose()
	{
		mLayNumChoose = findViewById(R.id.layNumChoose);
		mIvClose = (ImageView) findViewById(R.id.ivClose);
		mIvClose.setOnClickListener(this);
		mBtnSearch = (ImageView)findViewById(R.id.btnSearch);
		//mBtnSearch.setTextViewText1("搜索");
		//mBtnSearch.setTextViewText2("");
		//mBtnSearch.setTextViewTextSize(1,25);
		//mBtnSearch.setImageResource(R.drawable.ic_action_search,(float)0.9,(float)0.9);
		//mBtnSearch.setOnTouchListener(this);
		mBtnSearch.setOnClickListener(this);
		
		mBtnSelect = (MyCustomButton2)findViewById(R.id.btn_select);
		mBtnSelect.setTextViewText1("确定");
		mBtnSelect.setTextViewText2("");
		mBtnSelect.setTextViewTextSize(1, 35);
		mBtnSelect.setImageResource(R.drawable.btn_custom_check,(float)0.9,(float)0.9);
		mBtnSelect.setOnClickListener(this);
		mBtnSelect.setOnTouchListener(this);
		
		//mEtLast4 = (EditText) findViewById(R.id.et_customnum); 
		mIvDropPayType = (ImageView) findViewById(R.id.iv_drop_pay_type);
		mIvDropPayType.setOnClickListener(this);
		mIvDropNumRules = (ImageView)findViewById(R.id.iv_drop_num_rules); 
		mIvDropNumRules.setOnClickListener(this);
		mIvDropFee = (ImageView) findViewById(R.id.iv_drop_price);
		mIvDropFee.setOnClickListener(this);
		mLayChooseNumCover = findViewById(R.id.layCover);
		mLayChooseNumCover.setOnClickListener(this);
		mLvPayType = (ListView) findViewById(R.id.lv_pay_type);
		mLvNumRules = (ListView) findViewById(R.id.lv_no_rules);
		mLvFee = (ListView) findViewById(R.id.lv_fee);
		mTvPayType = (TextView) findViewById(R.id.tv_pay_type);
		mTvNumRules = (TextView) findViewById(R.id.tv_num_rule);
		mTvFee = (TextView) findViewById(R.id.tv_price);
		
		mLvPayType.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView item = (TextView) view.findViewById(R.id.tv_text);
				mLayChooseNumCover.setVisibility(View.GONE);
				mLvPayType.setVisibility(View.GONE);
				mTvPayType.setText(item.getText().toString());
				mPayType = position + 1;// 1预付 2后付
			}
		});
		
		mLvNumRules.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView item = (TextView) view.findViewById(R.id.tv_text);
				mLayChooseNumCover.setVisibility(View.GONE);
				mLvNumRules.setVisibility(View.GONE);
				mTvNumRules.setText(item.getText().toString());
				mNoRuleCode = position;
				if(position != 0)
				{
					mSelRule1 = 1;
				}
			}
		});
		
		mLvFee.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView item = (TextView) view.findViewById(R.id.tv_text);
				mLayChooseNumCover.setVisibility(View.GONE);
				mLvFee.setVisibility(View.GONE);
				mTvFee.setText(item.getText().toString());
				mSelRule3 = position;
			}
		});
		
		mMoreView = getLayoutInflater().inflate(R.layout.load, null);
		mMoreView.setVisibility(View.GONE);
		
		mLvNum = (ListView)this.findViewById(R.id.lv_num);
		mLvNum.setVerticalFadingEdgeEnabled(true);
		mLvNum.setFadingEdgeLength(60);
		mLvNum.setDrawingCacheEnabled(false);
		mLvNum.setVerticalScrollBarEnabled(false);
		
		initAdapter();
		// 添加底部view，一定要在setAdapter之前添加，否则会报错。
		mLvNum.addFooterView(mMoreView); 	
		mLvNum.setAdapter(mTelAdapter);
		// 设置listview的滚动事件
		mLvNum.setOnScrollListener(this); 	
	}
	
	// 初始化号码适配器
	private void initAdapter()
	{
		mNumList = new ArrayList<String>();
		mFeeList = new ArrayList<String>();
		mTelAdapter = new MyNewTelAdapter(this, mNumList, mFeeList, R.layout.lay_newtelnum_listitem);
	}

	// 初始化手机详情信息界面 
	private void initPhoneDetailsView()
	{
		mLayCountSold = (LinearLayout) findViewById(R.id.layCountSold);
		mCurrImg = (ImageView)findViewById(R.id.iv_Cur); 
		
		// 右侧手机详情 
		mTvTelNum = (TextView) findViewById(R.id.tvTelNum);
		mTvTelNum.setText(Html.fromHtml("<u>" + "点击选择号码" + "</u>"));
		
		mTvPhoneDetails = (TextView)findViewById(R.id.tvPhoneDetails);
		mTvActivityDetails	= (TextView) findViewById(R.id.tvActivityDetails);
		mTvPackageDetails = (TextView) findViewById(R.id.tvPackageDetails);
		mTvTelNum.setOnClickListener(this);
		
		// 活动详情
		mTvPhoneDetails.setOnClickListener(this);
		mTvActivityDetails.setOnClickListener(this);
		mDlgActivityDetails = new ActivityDetailsDialog(this);
		// 套餐详情
		mTvPackageDetails.setOnClickListener(this);
		mDlgPackageDetails = new PackageDetailsDialog(this);
		
		// 左侧手机图片预览
		//mCurrImg = new ImageView(this);
		mIconList = new ArrayList<LinearLayout>();
		//mLayImgcontainer = (LinearLayout) findViewById(R.id.layShowPic);
		mLayIcons = (LinearLayout) findViewById(R.id.layIcons);
		// 营销口号
		mTvMarkTag = (TextView) findViewById(R.id.tvTitle);
		// 价格
		mTvPrice = (TextView) findViewById(R.id.tvPrice);
		// 已售出
		mTvSoldCount = (TextView) findViewById(R.id.tvCountSold);
		// 归属地
		mTvNumBelong = (TextView) findViewById(R.id.tvNumBelong);
		// 活动内容
		mTvActivity = (TextView) findViewById(R.id.tvSpacial);
		// 套餐内容
		mTvPackage = (TextView) findViewById(R.id.tvPackage);
		// 按钮
		mCheckNum = (CheckBox) findViewById(R.id.cbNumCheck);
		mLayPhoneDetails = findViewById(R.id.layPhoneDetails);
		// 返回
		mBtnBack = (MyCustomButton) findViewById(R.id.btnBack);
		mBtnBack.setTextViewText1("返回");
		mBtnBack.setImageResource(R.drawable.btn_back);
		mBtnBack.setOnTouchListener(this);
		mBtnBack.setOnClickListener(this);
		// 立即购买
		mBtnPurchase = (MyCustomButton) findViewById(R.id.btnPurchase);
		mBtnPurchase.setTextViewText1("立即购买");
		mBtnPurchase.setOnTouchListener(this);
		mBtnPurchase.setOnClickListener(this);
		mBtnAddToCompare  = (Button) findViewById(R.id.btnAddToCompare);
		mBtnAddToCompare.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		if (ButtonUtil.isFastDoubleClick(v.getId(), 500))
		{
			//showErrorMsg("您的操作速度过快！");
			return;
		}
		switch (v.getId())
		{
			// 返回
			case R.id.layBack:
			case R.id.btn_previous:
				DialogUtil.MsgBox(TITLE_STR, "确定要放弃该次业务吗?",
				"确定", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						finishAct();
					}
				}, "取消", null, null);
				break;
				
			// 最IN机型
			case R.id.tvMostIn:
				inMostIn(); 
				break;
				
			// 套餐靓号
			case R.id.tvProduct:
				inProduct();
				break;
				
			// 手机详细信息返回
			case R.id.btnBack:
				// 弹出详情框时 屏蔽按钮点击事件，暂时先这样做
				if(mLayNumChoose.isShown())
				{
					return;
				}
				if(mMapBitmap != null)
				{
					Iterator<Entry<String, Bitmap>> itr = mMapBitmap.entrySet().iterator();
					while(itr.hasNext()) 
					{
						Entry<String, Bitmap> entry = itr.next();
						Bitmap bitmap = entry.getValue();
						if(bitmap != null && !bitmap.isRecycled()) 
						{
							Log.e("", "正在释放缩略影图片...");
							bitmap.recycle();
							bitmap = null;
							System.gc();
						}
						entry = null;
					}
					mMapBitmap.clear();
				}
				mCurrImg.setImageBitmap(null);
				mGridContent.setVisibility(View.VISIBLE);
				mLayPhoneDetails.setVisibility(View.GONE);
				mCheckNum.setVisibility(View.GONE);
				mTvTelNum.setText(Html.fromHtml("<u>" + "点击选择号码" + "</u>"));
				break;

			// 手机参数信息
			case R.id.tvPhoneDetails:
				// 弹出详情框时 屏蔽按钮点击事件，暂时先这样做
				if(mLayNumChoose.isShown())
				{
					return;
				}
				mDlgPhoneDetails = new DetailParamDialog(this, mCurPackage);
				mDlgPhoneDetails.show();
				break;
				
			// 活动详情
			case R.id.tvActivityDetails:
				mDlgActivityDetails.show(mCurPackage.getmProduct().getmActivityContent(), mCurPackage.getmProduct().getmActivityDetails());
				break;
				
			// 套餐详情
			case R.id.tvPackageDetails:
				mDlgPackageDetails.show(mCurPackage);
				break;

			// 立即购买
			case R.id.btnPurchase:
				// 弹出详情框时 屏蔽按钮点击事件，暂时先这样做
				if(mLayNumChoose.isShown())
				{
					return;
				}
				if(mCheckNum.isShown() && mCheckNum.isChecked())
				{
					purchase();
				}
				else
				{
					DialogUtil.MsgBox(TITLE_STR, "请选择号码!");
					//DialogUtil.showMessage("请选中号码!");
				}
				break;
			
			// 加入对比
			case R.id.btnAddToCompare:
				if(compareModel(mCurPackage))
				{
					playAnimation(v);
				}
				break;
			
			// 选择号码
			case R.id.tvTelNum:
				// 显示选择号码对话
				showNumChooseDialog();
				break;
				
			// 付费类型下拉按钮
			case R.id.iv_drop_pay_type:
				mLvPayType.setVisibility(View.VISIBLE);
				mLvNumRules.setVisibility(View.GONE);
				mLvFee.setVisibility(View.GONE);
				mLayChooseNumCover.setVisibility(View.VISIBLE);
				break;
				
			// 选择号码下拉按钮
			case R.id.iv_drop_num_rules:
				mLvPayType.setVisibility(View.GONE);
				mLvNumRules.setVisibility(View.VISIBLE);
				mLvFee.setVisibility(View.GONE);
				mLayChooseNumCover.setVisibility(View.VISIBLE);
				break;
			
			// 选择价格区间下拉按钮
			case R.id.iv_drop_price:
				mLvPayType.setVisibility(View.GONE);
				mLvNumRules.setVisibility(View.GONE);
				mLvFee.setVisibility(View.VISIBLE);
				mLayChooseNumCover.setVisibility(View.VISIBLE);
				break;
			
			// 搜索
			case R.id.btnSearch:
				searchNum();
				break;
				
			// 号码选择框确定按钮
			case R.id.btn_select:
				if(StringUtil.isEmptyOrNull(getChoseNum()))
				{
					DialogUtil.MsgBox("温馨提示", "请选择号码！");
				}
				else
				{
					selectNum();
				}
				break;
			
			// 覆盖全局阴影
			case R.id.layCover:
				mLvPayType.setVisibility(View.GONE);
				mLvNumRules.setVisibility(View.GONE);
				mLvFee.setVisibility(View.GONE);
				mLayChooseNumCover.setVisibility(View.GONE);
				break;
			
			// 关闭选择号码
			case R.id.ivClose:
				mLayNumChoose.setVisibility(View.GONE);
				break;
				
			// 历史浏览
			case R.id.btnHistory:
				// 弹出详情框时 屏蔽按钮点击事件，暂时先这样做
				if(mLayNumChoose.isShown())
				{
					return;
				}
				mPopHistory.showAtLocation(v, Gravity.RIGHT, 0, 0);
				break;

			// 关闭历史浏览
			case R.id.btn_history_hide:
				closePopWindow(mPopHistory);
				break;

			// 清空历史记录
			case R.id.btn_clear_history:
				clearHistory();	
				break;
				
			// 对比框
			case R.id.btnShowCompare:
				// 弹出详情框时 屏蔽按钮点击事件，暂时先这样做
				if(mLayNumChoose.isShown())
				{
					return;
				}
				mPopCompare.showAtLocation(v, Gravity.RIGHT, 0, 0);
				break;
				
			// 点击对比框中的第一项
			case R.id.layCompare1:
				if(mCurCompares[0] != null)
				{
					selectItem(mCurCompares[0]);
					closePopWindow(mPopCompare);
				}
				break;
				
			// 点击对比框中的第二项
			case R.id.layCompare2:
				if(mCurCompares[1] != null)
				{
					selectItem(mCurCompares[1]);
					closePopWindow(mPopCompare);
				}
				break;
				
			// 点击对比框中的第三项
			case R.id.layCompare3:
				if(mCurCompares[2] != null)
				{
					selectItem(mCurCompares[2]);
					closePopWindow(mPopCompare);
				}
				break;
			
			// 进行对比
			case R.id.btnToCompare:
				compare();
				break;
			
			// 删除对比机型1
			case R.id.btn_compare_close1:
				closeCompare(0);
				break;

			// 删除对比机型2
			case R.id.btn_compare_close2:
				closeCompare(1);
				break;
							
			// 删除对比机型3
			case R.id.btn_compare_close3:
				closeCompare(2);		
				break;

			// 关闭对比框
			case R.id.btnHideCompare:
				closePopWindow(mPopCompare);
				break;
			default:
				break;
		}
	}
	
	// 选中号码
	private void selectNum()
	{
		//mTelAdapter.removeFocus();
		mLayNumChoose.setVisibility(View.GONE);
		if(mTelAdapter != null)
		{
			if (!StringUtil.isEmptyOrNull(getChoseNum()))
			{
				runOnUiThread(new Runnable() 
				{	
					@Override
					public void run() 
					{
						if(mTvTelNum.getText().toString().equals("点击选择号码"))
						{
							mCheckNum.setVisibility(View.VISIBLE);
						}
						mTvTelNum.setText(getChoseNum());
					}
				});
			}
			else
			{
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						if(mTvTelNum.getText().toString().equals("点击选择号码"))
						{
							mCheckNum.setVisibility(View.GONE);
						}
					}
				});
			}
		}
	}

	// 获取
	private void searchNum()
	{
		// 查询前先清空原有数据
		mTelAdapter.clear();
		if(Statics.IS_DEBUG)
		{
			loadNewData();
			return;
		}
		else
		{
			DialogUtil.showProgress("正在获号码列表,请稍候...");
			new Thread()
			{
				@Override
				public void run()
				{ 
					//mStrLast4 = mEtLast4.getText().toString();
					if(!mIsMostIn)
					{
						// 套餐靓号没有0E11，需另起一流程
						mHaoka.getReceivePackage().setServiceCommandID("FFFF");
					}
					if(mHaoka.bll0E02(mPayType, mSelRule1, mNoRuleCode, mStrLast4, mSelRule2, mStrFirst3, mSelRule3, (short)1, true))
					{
						// 获取号码列表成功
						mHandler.sendEmptyMessage(1);
					}
					else
					{
						DialogUtil.MsgBox(TITLE_STR, mHaoka.getLastknownError());
					}
					DialogUtil.closeProgress();
				}
			}.start();
		}
	}

	// 立即购买
	private void purchase()
	{
		// 18610000111
		final String selectNum = mTvTelNum.getText().toString();
		DialogUtil.showProgress("正在进行号码预占,请稍候...");
		new Thread()
		{
			@Override
			public void run()
			{ 
				int type = 1;
				if(StringUtil.isEmptyOrNull(mHaoka.mSelectNum))
				{
					mHaoka.mSelectNum = selectNum;
				}
				if(!selectNum.equals(mHaoka.mSelectNum))
				{
					Log.i("号码预占", "变更预占");
					type = 5;
				}
				// 付费类型 1 为后付 2为预付，与之前查询号码相反
				if(mHaoka.bll0E03(selectNum, mPayType == 1? 2: 1, 0x04, type, 1))
				{
					mHaoka.mSelectNum = selectNum;
					mHandler.sendEmptyMessage(3);
				}
				else
				{
					mHaoka.mSelectNum = "";
					DialogUtil.MsgBox(TITLE_STR, mHaoka.getLastknownError());
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 输入终端串号并提交
	private void showIMEIDialog()
	{ 
		View view = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			view  = getLayoutInflater().inflate(R.layout.lay_input, null); 
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			view = getLayoutInflater().inflate(R.layout.lay_input_orange, null); 
		}
		final EditText et_input = (EditText) view.findViewById(R.id.editText);
		DialogUtil.showCustomerDialog("请输入售卖机型串号", view, "提交", new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				final String termainalID = et_input.getText().toString().trim();
				if(termainalID == null || termainalID.equals(""))
				{
					showErrorMsg("售卖机型串号不能为空，请重新输入！");
					showIMEIDialog();
					return;
				}
				occupyConcract(termainalID);
			}
		},"取消", null); 
	}

	// 预占合约计划
	private void occupyConcract(final String termainalID)
	{
		DialogUtil.showProgress("正在预占合约计划资源,请稍候...");
		new Thread()
		{
			@Override
			public void run()
			{
				try 
				{
					ProductInfo product = mCurPackage.getmProduct();
					int contractDate = Integer.parseInt(product.getmContractDate());
					int actType = Integer.parseInt(product.getmActivityType());
					String packageID = product.getmProductID();	// 20131105
					String contractID = product.getmContractID();
					//if(mHaoka.bll0E04(12, 2, "37", "1232123", termainalID))
					System.out.println("ActType :" + actType);
					if(mHaoka.bll0E04(contractDate, actType, packageID, contractID, termainalID))
					{
						Bundle bundle = new Bundle();
						bundle.putSerializable("package", mCurPackage.getmProduct());
						Message msg = new Message();
						bundle.putString("className", UserIdinfoActivity.class.getName());
						msg.what = FrameActivity.DIRECTION_NEXT;
						msg.setData(bundle);
						FrameActivity.mHandler.sendMessage(msg);
					}
					else
					{
						// 套餐禁用 20131128
						if(socketService.getLastknownErrCode() == 2)
						{
							String ss = "预占合约计划资源失败!\n";
							if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
							{
								ss += "失败原因: [" + socketService.getLastKnowError() + "]";
							}
							DialogUtil.MsgBox(TITLE_STR, ss, "确定", 6, "", -1, 6, mHandler);
						}
						else
						{
							DialogUtil.MsgBox(TITLE_STR, mHaoka.getLastknownError());
						}
					}
					DialogUtil.closeProgress();
				}
				catch (Exception e) 
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, "终端上传数据异常，请确认数据是否完整!", 
					"确定", new OnClickListener()
					{
						
						@Override
						public void onClick(View v)
						{
							finishAct();
						}
					}, "", null, null);
					e.printStackTrace();
					mHaoka.bll0E16();
				}
			};
		}.start();
	} 

	// 显示选择号码对话
	private void showNumChooseDialog()
	{
		//mPopNumChoose.setWidth(mTvCompareCount.getLeft() - mBtnPrevious.getRight());
		//mPopNumChoose.showAtLocation(mLayPhoneDetails, Gravity.CENTER, 0, 0);
		mLayNumChoose.setVisibility(View.VISIBLE);
		if(Statics.IS_DEBUG)
		{
			// 虚拟靓号规则
			Rule rule = new Rule(0, "全部");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(1, "ABC");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(2, "AA");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(3, "AABB");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(4, "ABAB");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(5, "AAA");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(6, "ABCD");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(7, "AAAA");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(8, "ABCDE");
			Business_0E.mListNoRules.add(rule);
			rule = new Rule(9, "AAAAA");
			Business_0E.mListNoRules.add(rule);
			
			// 重置选择号码ListView
			mTelAdapter.clear();
			mTelAdapter.notifyDataSetChanged();
			// 因此选择号码框选择按钮
			mBtnSelect.setVisibility(View.GONE);
			return;
		}
		List<Object> ruleList = new ArrayList<Object>();
		ruleList.add("不限");
		ruleList.addAll(Business_0E.mListNoRules);
		mArrAdapter = new ArrayAdapter<Object>(this, R.layout.num_rule_list_item, R.id.tv_text, ruleList);
		mLvNumRules.setAdapter(mArrAdapter);
		
		ArrayAdapter<String> tempAdapter = new ArrayAdapter<String>(this, R.layout.num_rule_list_item, R.id.tv_text);
		tempAdapter.add("不限");
		tempAdapter.add("0-50");
		tempAdapter.add("50-100");
		tempAdapter.add("100-300");
		tempAdapter.add("300-500");
		tempAdapter.add("500-5000");
		tempAdapter.add("5000以上");
		mLvFee.setAdapter(tempAdapter);
		 
		// 付费类型
		tempAdapter = new ArrayAdapter<String>(this, R.layout.num_rule_list_item, R.id.tv_text);
		tempAdapter.add("预付费");
		tempAdapter.add("后付费");
		mLvPayType.setAdapter(tempAdapter);
		// 重置选择号码ListView
		mTelAdapter.clear();
		mTelAdapter.notifyDataSetChanged();
		// 隐藏选择号码框选择按钮
		mBtnSelect.setVisibility(View.GONE);
		mTvPayType.setText("预付费");
		mTvNumRules.setText("不限");
		mTvFee.setText("不限");
		mPayType 	= 1;
		mSelRule1 	= 0;
		mSelRule2	= 0;	
		mSelRule3	= 0;	
		mNoRuleCode	= 0;
		mStrLast4	= "";	
		mStrFirst3	= "";	
		// 首次获取号码
		searchNum();
		//mEtLast4.setText("");
	}

	// 关闭弹出框
	private void closePopWindow(PopupWindow popWindow)
	{
		if (popWindow != null && popWindow.isShowing())
		{
			popWindow.dismiss();
		}
	}
	
	// 清除历史浏览记录
	private void clearHistory()
	{
		if(mCurHistoryData != null && mCurHistoryData.size() > 0)
		{
			DialogUtil.MsgBox(TITLE_STR,"确定删除所有浏览记录?", "确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(mIsMostIn)
					{
						mHistoryPackDao.clear();
					}
					else
					{
						mHistoryProductDao.clear();
					}
					updateHistory();
				}
			}, "取消", null, null);
		}
		else
		{
			DialogUtil.MsgBox(TITLE_STR, "当前浏览记录为空");
		}
	}
	
	// 更新浏览历史
	private void updateHistory()
	{
		// 最IN机型
		if (mCurHistoryData != null)
		{
			mCurHistoryData.clear();
		}
		if(mIsMostIn)
		{
			mCurHistoryData = mHistoryPackDao.getData();
		}
		else
		{
			mCurHistoryData = mHistoryProductDao.getData();
		}
		
		if (mCurHistoryData != null && mCurHistoryData.size() > 0)
		{
			mHistoryAdapter = new HistoryPackageAdapter(this, mCurHistoryData, R.layout.lay_compare_item_pack, this);
			mLvHistory.setAdapter(mHistoryAdapter);
			mTvNoHistory.setVisibility(View.GONE);
			mLvHistory.setVisibility(View.VISIBLE);
		}
		else
		{
			mLvHistory.setAdapter(null);
			mTvNoHistory.setVisibility(View.VISIBLE);
			mLvHistory.setVisibility(View.GONE);
		}
	}

	//加载数据
	private void loadNewData()
	{  
		mTelAdapter.clear();
		mIsScrollOver = true;
		if(Statics.IS_DEBUG)
		{
			mNumList = new ArrayList<String>();
			mFeeList = new ArrayList<String>();
			mNumList.add("18600001111");
			mNumList.add("18600001112");
			mNumList.add("18600001113");
			mNumList.add("18600001114");
			mNumList.add("18600001115");
			mNumList.add("18600001116");
			mNumList.add("18600001117");
			
			mFeeList.add("50");
			mFeeList.add("51");
			mFeeList.add("52");
			mFeeList.add("53");
			mFeeList.add("54");
			mFeeList.add("55");
			mFeeList.add("56");
			mTelAdapter.addData(mNumList,mFeeList);
			mBtnSelect.setVisibility(View.VISIBLE);
		}
		else
		{
			mTelAdapter.addData(getNumberList(), getFeeList());
			if(getNumberList().size() > 0)
			{
				mBtnSelect.setVisibility(View.VISIBLE);
			}
			else
			{
				mBtnSelect.setVisibility(View.GONE);
			}
		}
	}
	
	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}

	// 点击某项，在主界面点击
	public void selectItem(int position)
	{
		mCurPackage = mCurDataList.get(position);
		showView(mCurPackage);
	}
	
	// 点击某项
	public void selectItem(RecommendPackage pack)
	{
		mTvTelNum.setText(Html.fromHtml("<u>" + "点击选择号码" + "</u>"));
		mCheckNum.setVisibility(View.GONE);
		mCurPackage = pack;
		showView(mCurPackage);
		closePopWindow(mPopHistory);
	}
	
	String count = "";
	// 显示手机详情
	private void showView(final RecommendPackage pack)
	{
		mGridContent.setVisibility(View.GONE);
		mLayPhoneDetails.setVisibility(View.VISIBLE);
		if(mIsMostIn)
		{
			// 最IN机型显示出售数量
			mTvPhoneDetails.setVisibility(View.VISIBLE);
			mLayCountSold.setVisibility(View.VISIBLE);
			count = "";
			DialogUtil.showProgress("正在刷新数据,请稍候...");
			new Thread()
			{
				@Override
				public void run()
				{
					count = mHaoka.bll0E11(pack.getmPhoneModel().getId());
					// count = mHaoka.bll0E11("21");
					if(!StringUtil.isEmptyOrNull(count))
					{
						Message msg = new Message();
						msg.obj = pack;
						msg.what = 4;
						mHandler.sendMessage(msg);
					}
					else
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox(TITLE_STR, "获取已售出数量失败！");
						Message msg = new Message();
						msg.what = 6;
						mHandler.sendMessage(msg);
					}
				}
			}.start();
			mHistoryPackDao.deleteRecommendPack(mCurPackage.getmId());	// 删除历史记录中重复的记录
			mHistoryPackDao.insertData(mCurPackage); 					// 添加到历史浏览记录中
		}
		else
		{
			// 套餐靓号不显示出售数量
			mLayCountSold.setVisibility(View.GONE);
			mTvPhoneDetails.setVisibility(View.GONE);
			mHistoryProductDao.deleteProduct(mCurPackage.getmId());
			mHistoryProductDao.insertData(mCurPackage);
			Message msg = new Message();
			msg.obj = pack;
			msg.what = 4;
			mHandler.sendMessage(msg);
		}
		//initFileList();
		initIcons();
		// 更新历史浏览记录界面
		updateHistory();
	}
	
	// 去掉某个对比机型
	private void closeCompare(int index)
	{
		switch (index)
		{
			case 0:
				mLayCompare1.removeAllViews();
				mLayCompare1.setBackgroundResource(R.drawable.bg_compare);
				if(mCurCompares[0] != null)
				{
					mCurCompares[0].setmIsSelected(false);
				}
				mCurCompares[0] = null;
				mBtnCompareClose1.setVisibility(View.GONE);
				break;
			case 1:
				mLayCompare2.removeAllViews();
				mLayCompare2.setBackgroundResource(R.drawable.bg_compare);
				if(mCurCompares[1] != null)
				{
					mCurCompares[1].setmIsSelected(false);
				}
				mCurCompares[1] = null;
				mBtnCompareClose2.setVisibility(View.GONE);
				break;
			case 2:
				mLayCompare3.removeAllViews();
				mLayCompare3.setBackgroundResource(R.drawable.bg_compare);
				if(mCurCompares[2] != null)
				{
					mCurCompares[2].setmIsSelected(false);
				}
				mCurCompares[2] = null;
				mBtnCompareClose3.setVisibility(View.GONE);
				break;
			default:
				break;
		}
		mCompareCount --;
		if(mCompareCount <= 0)
		{
			mCompareCount = 0;
		}
		mTvCompareCount.setText(mCompareCount + "");
		mTvCompareCount2.setText(mCompareCount + "");
		if(mGridAdapter != null)
		{
			mGridAdapter.notifyDataSetChanged();
		}
	}

	// 对比
	private void compare()
	{
		if (mCurCompares[0] == null && mCurCompares[1] == null && mCurCompares[2] == null)
		{
			if(mIsMostIn)
			{
				showErrorMsg("对比栏里没有机型");
			}
			else
			{
				showErrorMsg("对比栏里没有套餐");
			}
			return;
		}
		if (mCurCompares[0] != null)
		{
			if (!mCurCompares[0].ismIsParamLoaded())
			{
				RecommendPackage temp = getModelInfoFromDb(mCurCompares[0]);
				// if (!temp.isParamLoaded) 
				//{
				//  temp = getModelInfoFromServer(compareModels[0]);
				//}
				if (temp.ismIsParamLoaded())
				{
					mCurCompares[0].setParams(temp);
				}
			}
		}
		if (mCurCompares[1] != null)
		{
			if (!mCurCompares[1].ismIsParamLoaded())
			{
				RecommendPackage temp = getModelInfoFromDb(mCurCompares[1]);
				// if (!temp.isParamLoaded) {
				// temp = getModelInfoFromServer(compareModels[1]);
				// }
				if (temp.ismIsParamLoaded())
				{
					mCurCompares[1].setParams(temp);
				}
			}
		}
		if (mCurCompares[2] != null)
		{
			if (!mCurCompares[2].ismIsParamLoaded())
			{
				RecommendPackage temp = getModelInfoFromDb(mCurCompares[2]);
				// if (!temp.isParamLoaded) {
				// temp = getModelInfoFromServer(compareModels[2]);
				// }
				if (temp.ismIsParamLoaded())
				{
					mCurCompares[2].setParams(temp);
				}
			}
		}

		String ids = "";
		if (mCurCompares[0] != null && !mCurCompares[0].ismIsParamLoaded())
		{
			ids += mCurCompares[0].getmId() + ",";
		}
		if (mCurCompares[1] != null && !mCurCompares[1].ismIsParamLoaded())
		{
			ids += mCurCompares[1].getmId() + ",";
		}
		if (mCurCompares[2] != null && !mCurCompares[2].ismIsParamLoaded())
		{
			ids += mCurCompares[2].getmId() + ",";
		}

		if (ids.length() > 0)
		{
			ids = ids.substring(0, ids.length() - 1);
			loadCompareInfoFromServer(ids);
		}
		else
		{
			RecommendPackage[] temCompareModels = mCurCompares.clone();
			PackageCompareDialog dlg_compare = new PackageCompareDialog(this, temCompareModels, mIsMostIn);
			dlg_compare.show();
		}
	}
	
	// 从服务器加载比对信息
	private void loadCompareInfoFromServer(String ids)
	{
		// final String[] idArray = ids.split(",");
		// dlg_progress.show();
		new Thread()
		{
			public void run()
			{
				// List<PhoneModel> list = getModelInfoFromServer(idArray);
				DialogUtil.closeProgress();
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						RecommendPackage[] temCompareModels = mCurCompares.clone();
						PackageCompareDialog dlg_compare = new PackageCompareDialog(RecommendActivity.this, temCompareModels, mIsMostIn);
						dlg_compare.show();
					}
				});
			};
		}.start();
	}
	
	// 从数据库获取信息
	private RecommendPackage getModelInfoFromDb(RecommendPackage model)
	{
		RecommendPackage dbPackage = null;
		if(mIsMostIn)
		{
			dbPackage = mPackageParamsDao.getPacakgeByPhoneID(model.getmId());
		}
		else
		{
			dbPackage = mProductInfoDao.getPacakgeByPhoneID(model.getmId());
		}
		
		if (dbPackage != null)
		{
			return dbPackage; 
		}
		else
		{
			return model;
		}
	}

	// 点击某项对比
	public void addToCompare(final int position, View view)
	{
		if (!mAnimEnd)// 1s
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		// 添加数据
		if(compareModel(mCurDataList.get(position)))
		{
			// 播放动画效果
			playAnimation(view);
			mGridAdapter.notifyDataSetChanged();
		}
	}
	
	// 加入对比
	private boolean compareModel(RecommendPackage pack)
	{
		if(pack == null)
		{
			return false;
		}
		if(pack.equals(mCurCompares[0]) || pack.equals(mCurCompares[1]) ||pack.equals(mCurCompares[2]))
		{
			if(mIsMostIn)
			{
				showErrorMsg("该机型已加入对比!");
			}
			else
			{
				showErrorMsg("该套餐已加入对比!");
			}
			return false;
		}
		
		LinearLayout target = null;
		int index = 0;
		if (mCurCompares[0] == null)
		{
			target = mLayCompare1;
			mBtnCompareClose1.setVisibility(View.VISIBLE);
			index = 0;
		}
		else if (mCurCompares[1] == null)
		{
			target = mLayCompare2;
			mBtnCompareClose2.setVisibility(View.VISIBLE);
			index = 1;
		}
		else if (mCurCompares[2] == null)
		{
			target = mLayCompare3;
			mBtnCompareClose3.setVisibility(View.VISIBLE);
			index = 2;
		}
		else
		{
			showErrorMsg("产品对比栏已满!");
			return false;
		}
		
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			target.setBackgroundResource(R.drawable.bg_compare_on);
		} 
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			target.setBackgroundResource(R.drawable.bg_compare_on_orange);
		}
		View compareView = getModelView(pack, R.layout.lay_compare_item_pack);
		target.addView(compareView);
		mCurCompares[index] = pack;
		pack.setmIsSelected(true);
		mGridAdapter.notifyDataSetChanged();
		return true;
	}
	
	// 获取机型显示view
	private View getModelView(RecommendPackage pack, int resourceId)
	{
		View view = View.inflate(getBaseContext(), resourceId, null);
		ImageView ivPhonePic = (ImageView) view.findViewById(R.id.ivPreview);
		TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		TextView tvDescribe = (TextView) view.findViewById(R.id.tvDescribe);
		TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);

		// iv_phonePic.setImageBitmap(model.getPicBitmap());
		if(mIsMostIn)
		{
			//pack.getmPhoneModel().showInImageView(ivPhonePic);
			//tvPrice.setText(pack.getmPhoneModel().getPriceDes());
			/*
			if(StringUtil.isEmptyOrNull(pack.getmProduct().getmBigPic()))
			{
				// 推荐包图片为空时展示机型图片
				pack.getmPhoneModel().showInImageView(ivPhonePic);
			}
			else
			{
				// 推荐包图片不为空时展示推荐包图片
				pack.getmProduct().showInImageView(ivPhonePic);
			}
			*/
			tvTitle.setText(StringUtil.adjustStringLength(pack.getTitle(), 18));
			tvDescribe.setText(StringUtil.adjustStringLength(pack.getmProduct().getmDescribe(), 20));
		}
		else
		{
			//pack.getmProduct().showInImageView(ivPhonePic);
			tvTitle.setText(StringUtil.adjustStringLength(pack.getTitle(), 18));
			tvDescribe.setText(StringUtil.adjustStringLength(pack.getmProduct().getmDescribe(), 20));
		}
		pack.showImageView(ivPhonePic);
		tvPrice.setText(pack.getmProduct().getmPriceDes());
		return view;
	}
	
	private boolean	mAnimEnd = true;

	// 播放动画效果
	private void playAnimation(View view)
	{
		int[] location = new int[2];
		view.getLocationInWindow(location);
		int start_x = location[0] + 10;	// 让球的位置居中一点
		int start_y = location[1] + 5; 
		location = new int[2]; 
		mTvCompareCount.getLocationOnScreen(location);
		int end_x = location[0];
		int end_y = location[1];
		//System.out.println("start_x : " + start_x + " start_y :" + start_y);
		//System.out.println("to_x : " + end_x + " to_y :" + end_y);
		
		MarginLayoutParams params = (MarginLayoutParams) mBead.getLayoutParams();
		params.leftMargin = start_x;
		params.topMargin = start_y;
		mBead.setVisibility(View.VISIBLE);
		mBead.setLayoutParams(params);
		
		TranslateAnimation anim = new TranslateAnimation(0, end_x - start_x, 0, end_y - start_y);
		anim.setStartOffset(100);
		anim.setDuration(1000);
		final AlphaAnimation alphaAnimation = new AlphaAnimation(100, 0);
		mBead.startAnimation(alphaAnimation); 
		alphaAnimation.setDuration(1000);
		alphaAnimation.setStartOffset(100);
		//final Animation anim2 = AnimationUtils.loadAnimation(RecommendActivity.this, R.anim.anim_disappear);
		//anim2.setStartOffset(100);
		//mBead.startAnimation(anim2);// 渐渐透明动画
		anim.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{ 
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				mBead.setVisibility(View.GONE);
			}
		});
		AnimationSet animSet = new AnimationSet(true);
		animSet.addAnimation(anim);
		animSet.addAnimation(alphaAnimation);
		mBead.startAnimation(animSet);
		// view = mLayContent.getChildAt(position - mLayContent.getFirstVisiblePosition());
		// mData.remove(position);
		// mAdapter.notifyDataSetInvalidated();
		// mLayContent.setFastScrollEnabled(false);
		// System.out.println("position :" + position + " count :" +
		// mLayContent.getChildCount() + " position :" + position % 8);
		mCountAnim.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
				mAnimEnd = false; 
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				
				updateCompareView();
				mAnimEnd = true; 
				/*
				mCompareCount++;
				if(mCompareCount >= 3)
				{
					mCompareCount = 3;
				}
				mTvCompareCount.setText(mCompareCount + "");
				mTvCompareCount2.setText(mCompareCount + "");// 弹出对话框后的计数球
				*/
			}
		});
		mCountAnim.setStartOffset(1100);
		mTvCompareCount.startAnimation(mCountAnim);
	}

	/*
	// 显示当前图片
	private ImageView getPic(int index)
	{
		int sWidth, sHeight;
		int width, height;

		sWidth = 300;
		sHeight = 400;
		width = DimensionUtil.getPxFromDip(getResources(), sWidth);
		height = DimensionUtil.getPxFromDip(getResources(), sHeight);
 
		mCurrImg = new ImageView(this);
		LayoutParams params = new LayoutParams(width, height);
		mCurrImg.setLayoutParams(params);
		mLayImgcontainer.addView(mCurrImg); 
		Bitmap bitmap = PictureShowUtils.decodeFile(mLocalPaths[index], width, height);
		if (bitmap != null)
		{
			mCurrImg.setImageBitmap(bitmap);
		}
		else
		{
			mCurrImg.setImageResource(R.drawable.bg_model_loading);
		}
		System.gc();
		return mCurrImg;
	}
	*/
	
	// 初始化缩略图
	private void initIcons()
	{
		mCurrIndex = 0;
		mIconList.clear();
		mLayIcons.removeAllViews();
		/*
		if(mFileNameList.size() <= 0)
		{
			return;
		}
		*/
		if(mIsMostIn)
		{
			// 最In机型显示手机图片
			mLocalPaths = mCurPackage.getmPhoneModel().getPicPaths();
			mServerPaths = mCurPackage.getmPhoneModel().getServerPicPaths();
		}
		else
		{
			// 套餐靓号显示套餐图片
			mLocalPaths = mCurPackage.getmProduct().getPicPaths();
			mServerPaths = mCurPackage.getmProduct().getServerPicPaths();
		}
		
		//for(String fileName:mFileNameList)
		for(int i = 0; i < mLocalPaths.length; i++)
		{ 
			//Bitmap bitmap = PictureShowUtils.decodeFile(fileName, icon_width, icon_width);
			ImageView img = addIcons();
			showImageView(img, mLocalPaths[i], mServerPaths[i], i);
		}
	}
 
	private Map<String, Bitmap> mMapBitmap = new HashMap<String, Bitmap>();
	// 下载图片并显示
	private void showImageView(final ImageView img, final String mLocalPath, String mServerPath, final int index)
	{
		if(StringUtil.isEmptyOrNull(mLocalPath))
		{
			return;
		}
		Bitmap bitmap = BitmapPoolUtil.getBitmap(mLocalPath, mServerPath, 
		new onImageGotListener() 
		{
			@Override
			public void onImageGot(boolean result) 
			{
				if(result == true)
				{
					img.post(new Runnable() 
					{  
						@Override
						public void run() 
						{
							System.out.println("从网络...");
							Bitmap bitmap = BitmapPoolUtil.getBitmap(mLocalPath, null, null,new int[] {210, 280}, false);
							img.setImageBitmap(bitmap);
							mMapBitmap.put(mLocalPath, bitmap);
							if(index == mCurrIndex)
							{
								selectIcon(mCurrIndex, false);
							}
						}
					});
				}
				else
				{
					img.post(new Runnable() 
					{  
						@Override
						public void run() 
						{
							img.setImageResource(R.drawable.bg_no_model);
						}
					});
					mCurrImg.post(new Runnable()
					{
						@Override
						public void run()
						{
							mCurrImg.setImageResource(R.drawable.bg_no_model);
						}
					});
				}
			}
		}, new int[]{210, 280}, false);
		
		if(bitmap == null) 
		{
			img.setImageResource(R.drawable.bg_model_loading);
		}
		else
		{
			System.out.println("从本地....");
			img.setImageBitmap(bitmap);
			mMapBitmap.put(mLocalPath, bitmap);
		}
		if(index == mCurrIndex)
		{
			selectIcon(mCurrIndex, false);
		}
	}

	// 添加缩略图
	private ImageView addIcons()
	{
		int lay_width;
		int margin;
		int padding;
		int icon_width;
		int icon_height;
		lay_width 	= DimensionUtil.getPxFromDip(getResources(), 80);				// 布局长宽各设为75
		margin 		= DimensionUtil.getPxFromDip(getResources(), 2);				// 边距3
		padding 	= DimensionUtil.getPxFromDip(getResources(), 5);
		icon_width 	= DimensionUtil.getPxFromDip(getResources(), 78);				// 缩略图长宽70
		icon_height = DimensionUtil.getPxFromDip(getResources(), 60);				// 缩略图长宽70
		// 缩览图外部边框
		LinearLayout layout = new LinearLayout(RecommendActivity.this);
		layout.setBackgroundColor(Color.WHITE);
		LayoutParams params = new LayoutParams(lay_width, lay_width);
		params.gravity = Gravity.CENTER;
		params.setMargins(margin, margin, margin, margin);
		layout.setLayoutParams(params);
		layout.setPadding(padding, padding*2, padding, padding);
		layout.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				int index = mIconList.indexOf(v);
				if(index != mCurrIndex)
				{
					selectIcon(index, true);
				}
			}
		});
		LinearLayout clayout = new LinearLayout(RecommendActivity.this);
		clayout.setBackgroundColor(Color.WHITE);
		params = new LayoutParams(icon_width, icon_height);
		clayout.setLayoutParams(params);
		layout.addView(clayout);
		ImageView img = new ImageView(RecommendActivity.this);
		img.setImageResource(R.drawable.bg_model_loading);
		img.setLayoutParams(params);
		clayout.addView(img);
		mLayIcons.addView(layout);
		mIconList.add(layout); 
		return img;
	}
	
	// 选中缩略图
	private void setIconSelected(int index)
	{
		for(int i = 0; i<mIconList.size(); i++)
		{
			LinearLayout layout = mIconList.get(i);
			if(i == index)
			{
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					layout.setBackgroundResource(R.drawable.pic_selected);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					layout.setBackgroundResource(R.drawable.pic_selected_orange);
				}
			}
			else 
			{
				layout.setBackgroundResource(R.drawable.pic_no_selected);
			}
		}
	}
	
	// 选中图标
	private void selectIcon(int index, boolean isAnim)
	{
		boolean direction = mCurrIndex > index;// true--right, false--left
		mCurrIndex = index;
		//String fileName = mFileNameList.get(mCurrIndex);
		if(isAnim)
		{
			if(direction) 
			{
				mCurrImg.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_out));
				//mLayImgcontainer.removeAllViews();
				if(mMapBitmap.get(mLocalPaths[index]) != null)
				{
					mCurrImg.setImageBitmap(mMapBitmap.get(mLocalPaths[index]));
				}
				else
				{
					mCurrImg.setImageResource(R.drawable.bg_model_loading);
				}
				//mCurrImg = getPic(index);
				mCurrImg.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_in));
			}
			else
			{
				mCurrImg.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_out));
				//mLayImgcontainer.removeAllViews();
				if(mMapBitmap.get(mLocalPaths[index]) != null)
				{
					mCurrImg.setImageBitmap(mMapBitmap.get(mLocalPaths[index]));
				}
				else
				{
					mCurrImg.setImageResource(R.drawable.bg_model_loading);
				}
				//mCurrImg = getPic(index);
				mCurrImg.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_in));
			}
		}
		else
		{
			if(mMapBitmap.get(mLocalPaths[index]) != null)
			{
				mCurrImg.setImageBitmap(mMapBitmap.get(mLocalPaths[index]));
			}
			else
			{
				mCurrImg.setImageResource(R.drawable.bg_model_loading);
			}
			//mLayImgcontainer.removeAllViews();
			//mCurrImg.setImageBitmap(mMapBitmap.get(mLocalPaths[index]));
			//mCurrImg = getPic(index);
		}
		setIconSelected(mCurrIndex);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) 
	{
		// 减1是因为上面加了个addFooterView
		mLastItem = firstVisibleItem + visibleItemCount - 1; 
	}


	// 号码ListView响应Scroll事件
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) 
	{
		if(mIsScrollOver)
		{
			if (scrollState == RecommendActivity.SCROLL_STATE_IDLE && mLastItem == mTelAdapter.getCount())
			{
				mIsScrollOver = false;
				Log.i("TAG", "拉到最底部");
				if(Statics.IS_DEBUG)
				{
					loadMoreData();
					return;
				}
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							if(mTelAdapter != null)
							{
								if(Business_0E.mNumberTotal > 12)
								{
									if (mTelAdapter.getCount() * 2 < Business_0E.mNumberTotal)
									{
										runOnUiThread(new Runnable()
										{
											@Override
											public void run()
											{
												mMoreView.setVisibility(View.VISIBLE);
											}
										});
										//mStrLast4 = mEtLast4.getText().toString();
										boolean result = mHaoka.bll0E02(mPayType, mSelRule1, mNoRuleCode, mStrLast4,
												mSelRule2, mStrFirst3, mSelRule3,(short) (mTelAdapter.getCount() * 2 + 1), false);
										mHandler.sendEmptyMessage(5);
										if (!result)
										{ 
											DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定", new OnClickListener()
											{
												@Override
												public void onClick(View v)
												{
													mIsScrollOver = true;
												}
											}, "", null, null);
										}
										else
										{
											mHandler.sendEmptyMessage(0);
										}
									}
									else
									{
										mHandler.sendEmptyMessage(5);
										DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定", new OnClickListener()
										{
											@Override
											public void onClick(View v)
											{
												mIsScrollOver = true;
											}
										}, "", null, null);
									}
								}
							}
						}
						catch (Exception e)
						{
							e.printStackTrace();
							mHandler.sendEmptyMessage(5);
							DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定",new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mIsScrollOver = true;
								}
							}, "", null, null);
						}
					}
				}).start();
			}
		}
	}
	
	// 加载更多数据
	private void loadMoreData()
	{ 
		if(Statics.IS_DEBUG)
		{
			new Thread(new Runnable() 
			{	
				@Override
				public void run() 
				{
					try 
					{
						Thread.sleep(1000);
						runOnUiThread(new Runnable() 
						{	
							@Override
							public void run() 
							{
								DialogUtil.MsgBox(TITLE_STR, "没有更多的号码！");
							}
						});
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
			}).start();
		}
		else
		{
			mTelAdapter.addData(getNumberList(), getFeeList());
		}
	}

	// 获取号码列表
	private List<String> getNumberList()
	{
		mNumList = new ArrayList<String>();
		if(Business_0E.mNumberList != null)
		{
			mNumList.addAll(Business_0E.mNumberList);
		}
		return mNumList;
	} 
	
	// 获取费用列表
	private List<String> getFeeList()
	{
		mFeeList = new ArrayList<String>();
		if(Business_0E.mNumberDetailList != null)
		{
			mFeeList.addAll(Business_0E.mNumberDetailList);
		}
		return mFeeList;
	}
	
	// 获取被选定号码
	public String getChoseNum()
	{
		return mTelAdapter.getSelectedNumber();
	}
	
	// 声明Handler
	private Handler	mHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				// 加载更多数据，这里可以使用异步加载
				case 0:
					mIsScrollOver = true;
					loadMoreData(); 
					Log.i("TAG", "加载更多数据");
					break;
				
				// 加载新的号码
				case 1:
					loadNewData();
					break;
				
				// 预占号码成功，提示输入终端串号
				case 3:
					if(mIsMostIn)
					{
						// 最IN机型需要输入终端串号
						showIMEIDialog();
					}
					else
					{
						occupyConcract("");
					}
					break;
				
				// 获取已售出数量
				case 4:
					RecommendPackage pack = (RecommendPackage) msg.obj;
					DialogUtil.closeProgress();
					mTvMarkTag.setText(StringUtil.adjustStringLength(pack.getmProduct().getmDescribe(), 35));
					if(mIsMostIn)
					{
						mTvPrice.setText(pack.getmPhoneModel().getPriceDes());
					}
					else
					{
						mTvPrice.setText(pack.getmProduct().getmPriceDes());
					}
					mTvNumBelong.setText(pack.getmProduct().getmAreaLong());
					mTvSoldCount.setText(count + "  台");
					mTvActivity.setText(pack.getmProduct().getmActivityContent());
					mTvPackage.setText(pack.getmProduct().getmProductName());
					break;
					
				case 5:
					mMoreView.setVisibility(View.GONE);
					break;
				
				//  获取已出售数量失败
				case 6:
					mGridContent.setVisibility(View.VISIBLE);
					mLayPhoneDetails.setVisibility(View.GONE);
					break;
					
				default:
					break;
			}
		};
	};
	
	// 按键监听
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			if(mLayNumChoose != null && mLayNumChoose.isShown())
			{
				mLayNumChoose.setVisibility(View.GONE);
				return true;
			}
			else if(mLayPhoneDetails != null && mLayPhoneDetails.isShown())
			{
				// 相当于响应返回按钮事件
				mBtnBack.performClick();
				return true;
			}
			else
			{
				DialogUtil.MsgBox(TITLE_STR, "确定要放弃该次业务吗?", "确定", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						finishAct();
					}
				}, "取消", null, null);
				/*
				DialogUtil.showSelectDialog("确定要放弃该次业务吗?", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						finish();
					}
				}, null);
				*/
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onNewIntent(Intent intent) 
	{
		super.onNewIntent(intent);
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			mLayPhoneDetails.setVisibility(View.GONE);
			mGridContent.setVisibility(View.VISIBLE);
			return;
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			mLayPhoneDetails.setVisibility(View.GONE);
			mGridContent.setVisibility(View.VISIBLE);
			return;
		}
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		BitmapPoolUtil.clear();
		System.gc();
		Log.e("", "店长推荐onDestroy()");
	}
	
	private void finishAct()
	{
		System.gc();
		Message msg = FrameActivity.mHandler.obtainMessage();
		msg.what = FrameActivity.FINISH;
		FrameActivity.mHandler.sendMessage(msg);
	}
}
