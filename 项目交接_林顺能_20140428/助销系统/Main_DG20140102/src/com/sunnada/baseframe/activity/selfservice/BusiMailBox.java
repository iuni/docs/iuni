package com.sunnada.baseframe.activity.selfservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.SystemParamsDao;
import com.sunnada.baseframe.dialog.CallBackChild;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.DimensionUtil;
import com.sunnada.baseframe.util.StringUtil;

// 收件箱
public class BusiMailBox extends BaseActivity implements OnClickListener
{
	private static final String			TITLE_STR				= "收件箱温馨提示";
	private static final String			MAIL_RES				= "mail_res";
	
	private LinearLayout				mBacktoSelfService		= null;
	
	private LinearLayout                mLinearLeft;
	private ImageView                   mIvMailbox;
	private LinearLayout                mLinearMailboxTitle;
	private TextView                    mTvStatus;
	private TextView                    mTvDate;
	private TextView                    mTvContent;
	private TextView                    mTvOper;
	private ListView					mListMail				= null;
	
	// private LinearLayout				mMailOperations			= null;
	private MyCustomButton				mBtnMailPrev			= null;
	private MyCustomButton				mBtnMailNext			= null;
	
	private List<Map<String, String>>	mListMailData			= null;
	private List<Map<String, String>>	mListMailSubData		= new ArrayList<Map<String, String>>();
	private int 						mMailTotal				= 0;
	private int 						mMailStartIdx			= 0;
	// private int							mMailCur				= 0;
	
	private SystemParamsDao				mSystemParams			= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_mailbox_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_mailbox);
		}
		
		mSystemParams = new SystemParamsDao(this);
		// 初始化控件
		initViews();
		// 初始化显示
		// initShow();
		// 初始化邮箱
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在初始化邮箱...");
				initMailData();
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mBacktoSelfService = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		mBacktoSelfService.setOnClickListener(this);
		
		mLinearLeft = (LinearLayout) this.findViewById(R.id.linear_left);
		mIvMailbox = (ImageView) this.findViewById(R.id.iv_mailbox);
		mLinearMailboxTitle = (LinearLayout) this.findViewById(R.id.linear_mailbox_title);
		mTvStatus = (TextView) this.findViewById(R.id.tv_status);
		mTvDate = (TextView) this.findViewById(R.id.tv_date);
		mTvContent = (TextView) this.findViewById(R.id.tv_content);
		mTvOper = (TextView) this.findViewById(R.id.tv_oper);
		
		mListMail = (ListView) findViewById(R.id.mail_list);
		mListMail.setVerticalFadingEdgeEnabled(true);
		mListMail.setFadingEdgeLength(100);
		// mMailOperations = (LinearLayout) findViewById(R.id.mail_operations);
		
		// 上一页
		mBtnMailPrev = (MyCustomButton) findViewById(R.id.btn_mail_prev);
		mBtnMailPrev.setImageResource(R.drawable.btn_reselect);
		mBtnMailPrev.setTextViewText1("上一页");
		mBtnMailPrev.setOnClickListener(this);
		mBtnMailPrev.setOnTouchListener(this);
		// 下一页
		mBtnMailNext = (MyCustomButton) findViewById(R.id.btn_mail_next);
		mBtnMailNext.setImageResource(R.drawable.btn_custom_check);
		mBtnMailNext.setTextViewText1("下一页");
		mBtnMailNext.setOnClickListener(this);
		mBtnMailNext.setOnTouchListener(this);
	}
	
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			mIvMailbox.setImageDrawable(getResources().getDrawable(R.drawable.icon_mailbox_orange));
			ImageView ivReturn = (ImageView) findViewById(R.id.btn_return);
			ivReturn.setImageDrawable(getResources().getDrawable(R.drawable.icon_return_orange));
			mLinearMailboxTitle.setBackgroundColor(ARGBUtil.getArgb(7));
			mTvStatus.setTextColor(ARGBUtil.getArgb(2));
			mTvDate.setTextColor(ARGBUtil.getArgb(2));
			mTvContent.setTextColor(ARGBUtil.getArgb(2));
			mTvOper.setTextColor(ARGBUtil.getArgb(2));
		}
	}
	
	// 初始化邮箱
	private void initMailData() 
	{
		mListMailData = mSystemParams.selectMailData(App.getUserNo());
		//mListMailData = mSystemParams.selectMailData("555555");
		if(mListMailData == null) 
		{
			back(new CallBackChild(this), TITLE_STR, "收件箱为空!", "确定", null, true, false, false);
			/*
			for(int i=0; i<40; i++) 
			{
				if(i < 20) 
				{
					mSystemParams.InsertParamsValue(
							"09", 
							String.format("201310%02d160744", i), 
							String.format("测试收件箱%d", i), 
							"0", 
							App.getUserNo());
				}
				else
				{
					mSystemParams.InsertParamsValue(
							"09", 
							String.format("201310%02d160744", i), 
							String.format("测试收件箱%d", i), 
							"0", 
							"555555");
				}
			}
			*/
		}
		else
		{
			// 判断邮件总数是否小于10，如果小于10则隐藏上下页按钮
			/*
			mMailTotal = mListMailData.size();
			if(mMailTotal <= 10) 
			{
				mMailOperations.setVisibility(View.INVISIBLE);
			}
			else
			{
				mMailOperations.setVisibility(View.VISIBLE);
			}
			*/
			// 添加
			procCore();
		}
	}
	
	// 核心处理
	private void procCore() 
	{
		/*
		if(mMailStartIdx + 10 < mMailTotal) 
		{
			mMailCur = 10;
		}
		else
		{
			mMailCur = mMailTotal-mMailStartIdx;
		}
		*/
		// 先清空
		mListMailSubData.clear();
		// 再添加
		// for(int j=mMailStartIdx; j<mMailStartIdx+mMailCur; j++) 
		for(int j=0; j<mListMailData.size(); j++) 
		{
			Map<String, String> map = mListMailData.get(j);
			map.put(MAIL_RES, StringUtil.adjustStringLength(map.get("s_value"), 15));
			mListMailSubData.add(map);
		}
		
		// 刷新界面
		refreshScreen();
	}
	
	// 处理上一页
	private void procPrev() 
	{
		if(mMailStartIdx <= 0) 
		{
			back(null, TITLE_STR, "已经是第一页了！", "确定", null, false, false, false);
			return;
		}
		else
		{
			mMailStartIdx -= 10;
			// 核心处理
			procCore();
		}
	}
	
	// 处理下一页
	private void procNext() 
	{
		if(mMailStartIdx + 10 >= mMailTotal) 
		{
			back(null, TITLE_STR, "已经是最后一页了！", "确定", null, false, false, false);
			return;
		}
		else
		{
			mMailStartIdx += 10;
			// 核心处理
			procCore();
		}
	}
	
	// 刷新界面
	private void refreshScreen() 
	{
		BusiMailBox.this.runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				BaseAdapter adapter = new FusionAdapter(BusiMailBox.this, mListMailSubData, R.layout.lay_selfservice_mailbox_listitem);
				mListMail.setAdapter(adapter);
				DimensionUtil.setListViewHeightBasedOnChildren(mListMail);
			}
		});
	}
	
	// 为了触发按钮需要触发的事件
	class FusionAdapter extends BaseAdapter 
	{
		private int resource;
		private LayoutInflater inflater;
		private List<Map<String, String>> dataList;
		
		public FusionAdapter(Context context, List<Map<String, String>> dataList, int resource) 
		{
			this.dataList = dataList;
			this.resource = resource;
			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() 
		{
			return dataList.size();
		}

		@Override
		public Object getItem(int position) 
		{
			return dataList.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			ViewHolder viewHolder;
			TextView  tvMailStatus;
			TextView  tvMailDate;
			TextView  tvMailRes;
			TextView  tvMailOp;
			
			if(convertView == null)
			{
				viewHolder = new ViewHolder();
				convertView = inflater.inflate(resource, null);
				tvMailStatus = (TextView)convertView.findViewById(R.id.tv_mail_status);
				tvMailDate = (TextView)convertView.findViewById(R.id.tv_mail_date);
				tvMailRes = (TextView)convertView.findViewById(R.id.tv_mail_res);
				tvMailOp = (TextView)convertView.findViewById(R.id.tv_mail_op);
				
				viewHolder.setTvMailStatus(tvMailStatus);
				viewHolder.setTvMailDate(tvMailDate);
				viewHolder.setTvMailRes(tvMailRes);
				viewHolder.setTvMailOp(tvMailOp);
				
				convertView.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder)convertView.getTag();
				tvMailStatus = viewHolder.getTvMailStatus();
				tvMailDate = viewHolder.getTvMailDate();
				tvMailRes = viewHolder.getTvMailRes();
				tvMailOp = viewHolder.getTvMailOp();
			}
			
			// 邮件状态
			final Map<String, String> map = dataList.get(position);
			String tempstr = map.get("s_relationcode");
			
			if(tempstr.equals("0")) 
			{
				tvMailStatus.setText("未读");
				tvMailStatus.setTextColor(R.color.red);
			}
			else
			{
				tvMailStatus.setText("已读");
			}
			
			// 邮件日期
			tvMailDate.setText(map.get("s_code"));
			
			// 邮件内容
			tvMailRes.setText(map.get(MAIL_RES));
			
			// 查看详情
			tvMailStatus.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					back(null, "邮件详情", dataList.get(position).get("s_value"),
							"确定", null, false, false, false);
					// 获取邮件编码
					String code = map.get("_id");
					Log.e("", "邮件编码为: " + code);
					// 更新邮件状态
					mSystemParams.updateMailStatus(code, "1");
					// 更新界面
					map.put("s_relationcode", "1");
					refreshScreen();
				}
			});
			
			// 查看详情
			tvMailRes.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					back(null, "邮件详情", dataList.get(position).get("s_value"),
							"确定", null, false, false, false);
					// 获取邮件编码
					String code = map.get("_id");
					Log.e("", "邮件编码为: " + code);
					// 更新邮件状态
					mSystemParams.updateMailStatus(code, "1");
					// 更新界面
					map.put("s_relationcode", "1");
					refreshScreen();
				}
			});
			
			// 删除
			tvMailOp.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					new Thread()
					{
						public void run()
						{
							DialogUtil.showProgress("正在删除邮件...");
							back(new ICallBack() 
							{
								@Override
								public void back() 
								{
									// 删除当前邮件
									mSystemParams.DeleteParamsValue("09", dataList.get(position).get("_id"));
									// 重新初始化邮件
									initMailData();
								}

								@Override
								public void cancel() 
								{
								}

								@Override
								public void dismiss() 
								{
								}
							}, null, "确定要删除当前邮件?", "确定", "取消", true, false, false);
							DialogUtil.closeProgress();
						}
					}.start();
				}
			});
			return convertView;
		}
	}
	
	class ViewHolder
	{
		private TextView tvMailStatus;
		private TextView tvMailDate;
		private TextView tvMailRes;
		private TextView tvMailOp;
		
		public TextView getTvMailStatus() 
		{
			return tvMailStatus;
		}
		
		public void setTvMailStatus(TextView tvMailStatus) 
		{
			this.tvMailStatus = tvMailStatus;
		}
		
		public TextView getTvMailDate()
		{
			return tvMailDate;
		}
		
		public void setTvMailDate(TextView tvMailDate) 
		{
			this.tvMailDate = tvMailDate;
		}
		
		public TextView getTvMailRes() 
		{
			return tvMailRes;
		}
		public void setTvMailRes(TextView tvMailRes)
		{
			this.tvMailRes = tvMailRes;
		}

		public TextView getTvMailOp()
		{
			return tvMailOp;
		}

		public void setTvMailOp(TextView tvMailOp) 
		{
			this.tvMailOp = tvMailOp;
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}

	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
		
		switch(v.getId()) 
		{
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			finish();
			break;
			
		// 上一页
		case R.id.btn_mail_prev:
			procPrev();
			break;
			
		// 下一页
		case R.id.btn_mail_next:
			procNext();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
