package com.sunnada.baseframe.activity.busiaccept.base;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Log;

import com.sunnada.baseframe.activity.busiaccept.robospice.BaseRequest;
import com.sunnada.baseframe.database.MsgDBAdapter;

public class Global 
{
	public static final int				CLICK_TIME			= 1000;				// 单击超时
	
	public static final String			HELP_TYPE 			= "help_type";
	public static final String			FIRST_LAUNCH 		= "first_launch";
	public static char[] 				symbol_char 		= new char[] {'-', '<', '>', '.', '(', ')', '!', '@', 
																		'#', '$', '%', '^', '&', '*',
																		'+', '/', ',', ';', ':', '[', 
																		']', '{', '}', '|', '、'};

	public static String[] 				symbol_string 		= new String[] {"-", "<", ">", ".", "(", ")", 
																		"!", "@", "#", "$", "%", "^", "&", "*",
																		"+", "/", ",", ";", ":", "[", "]", "{", 
																		"}", "|", "、"};
	public static boolean				mHasMail			= false;
	public static String				mMailTitle			= null;
	public static String				mMailText			= null;
	
	public static boolean isSymbol(CharSequence s) 
	{
		if(s.length() > 0)
		{
			char c = s.charAt(s.length() - 1);
			for(int i = 0; i < symbol_char.length; i++)
			{
				if(c == symbol_char[i])
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isSymbol(String s) 
	{
		if(s.length() > 0)
		{
			try
			{
				String regEx = "[1234567890-`~!@#$%^&*+=|{}':;',\\[\\]<>/?~@#￥%……&*——+|{}【】‘；：”“’。，、？！]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(s);
				if(m.find())
				{
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
			/*
			String ss = s.substring(s.length() - 1);
			for(int i = 0; i < symbol_string.length; i++)
			{
				if(ss.equals(symbol_string[i]))
				{
					return true;
				}
			}
			*/
		}
		return false;
	}
	
	private static String[] mSimplePwds = new String[] 
	{
		"000000", "111111", "222222", "333333",
		"444444", "555555", "666666", "777777", 
		"888888" ,"999999", "123456", "234567", 
		"345678", "456789", "567890", "012345",
		"987654", "876543", "765432", "654321", 
		"543210"
	};

	// 判断密码是否过于简单
	public static boolean isSimple(String pwd) 
	{
		for(int i=0; i<mSimplePwds.length; i++)
		{
			if(pwd.equals(mSimplePwds[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	// 校验是否是联通手机号码
	public static boolean isUnicomNumber(String telstr)
	{
		try
		{
			telstr = telstr.substring(0, 3);
			if(telstr.equals("130") || telstr.equals("131") || telstr.equals("132") || telstr.equals("155") 
					|| telstr.equals("156") || telstr.equals("185") || telstr.equals("186") || telstr.equals("145"))
			{
				return true;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	// 调用垃圾回收器
	public static void causeGC()
	{
		try 
		{
			System.gc();
			Thread.sleep(100);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 检测公告
	public static void checkMail(Context context, Map<String, String> result)
	{
		try 
		{
			String noticeFlag = result.get(BaseRequest.NOTICE_FLAG);
			if("0".equals(noticeFlag))
			{
				mHasMail = false;
				return;
			}
			else
			{
				mHasMail = true;
				// 公告标题
				mMailTitle = result.get(BaseRequest.NOTICE_TITLE);
				// 公告内容
				mMailText = result.get(BaseRequest.NOTICE_MSG);
				// 更新数据库
				MsgDBAdapter adapter = null;
				try 
				{
					adapter = new MsgDBAdapter(context);
					adapter.open();
					// 看数据库里面是不是有相同的记录
					if(adapter.fetchDataByTag(mMailTitle) == null)
					{
						Log.e("", "insert msg data");
						adapter.insertData(mMailTitle, mMailText);
					}
					else
					{
						Log.e("", "msg already exists!");
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					if(adapter != null)
					{
						adapter.close();
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 去掉身份证识别结果号码里面的(wrong number)
	public static String checkIDNo(String idno)
	{
		try 
		{
			if(idno != null)
			{
				for(int i=0; i<26; i++)
				{
					String tempstr = String.format("%d", 0x61+i);
					idno.replace(tempstr, "");
				}
				idno.replace("(", "");
				idno.replace(")", "");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return idno;
	}
}
