package com.sunnada.baseframe.activity.busiaccept;

import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;
import com.sunnada.baseframe.activity.busiaccept.base.Global;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.activity.busiaccept.robospice.AgentActivateRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.AlterAgentPasswdRequest2;
import com.sunnada.baseframe.activity.busiaccept.robospice.BaseRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.GetIdentifyIDRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.GetIdentifyIDRequest2;
import com.sunnada.baseframe.activity.busiaccept.robospice.MobileClientLoginRequest;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.CallBackChild;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.identify.Interface;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

@SuppressLint("HandlerLeak")
public class BusiPhoneRegisterLogin extends BaseActivity implements OnClickListener 
{
	private static final String			TITLE_STR						= "实名制返档温馨提示";
	private static final int			MSG_LOGIN_SEND_CONFIG_OK		= 0x01;
	private static final int 			MSG_LOGIN_SUC 					= 0x02;
	private static final int 			MSG_LOGIN_FAIL 					= 0x03;
	private static final int			MSG_LOGIN_TIMEOUT				= 0x04;
	
	private static final int 			MSG_GET_IDENTIFY_ID_SUC 		= 0x05;
	private static final int 			MSG_GET_IDENTIFY_ID_FAIL 		= 0x06;
	private static final int			MSG_GET_IDENTIFY_ID_TIMEOUT 	= 0x07;

	private static final int 			MSG_MODIFY_PSWD_SUC 			= 0x08;
	private static final int 			MSG_MODIFY_PSWD_FAIL 			= 0x09;
	private static final int 			MSG_MODIFY_PSWD_TIMEOUT 		= 0x0A;
	
	private	int							mPageIdx						= 0x00;
	private Business_00					mBusiness						= null;
	private byte[] 						mIsShow 						= new byte[1];
	// 返回到业务办理
	private LinearLayout				mLinearBacktoBusiAccept			= null;
	// 登录页
	private LinearLayout				mLinearLogin;
	private EditText 					mEditLoginAgentNo;
	private ImageView					mImageLoginAgentNo;
	private String						mStrAgentNo;
	private EditText					mEditLoginAgentPwd;
	private ImageView 					mImageLoginAgentPswd;
	private String						mStrAgentPswd;
	
	private String						mLac;
	private String						mCi;
	private MobileClientLoginRequest 	mRequest;
	
	// 激活页面
	private LinearLayout				mLinearActivate;
	private TextView					mTextActivateTitle;
	private EditText					mEditActivateAgentNo;
	private ImageView					mImageActivateAgentNo;
	
	private GetIdentifyIDRequest		mGetIdentifyIDRequest;
	private String						mCommunicationID;
	private GetIdentifyIDRequest2		mGetIdentifyIDRequest2;
	
	// 设定密码界面
	private LinearLayout				mLinearSetPswd;
	private EditText					mEditSetIdentify;
	private ImageView					mImageSetIdentify;
	private String						mStrIdentify;
	private EditText					mEditSetNewPswd;
	private ImageView					mImageSetNewPswd;
	private EditText					mEditSetEnsureNewPswd;
	private ImageView					mImageSetEnsureNewPswd;
	private TextView					mTextSetRemainTime;
	
	private AgentActivateRequest		mAgentActivateRequest;
	private AlterAgentPasswdRequest2	mAlterAgentPasswdRequest2;
	
	Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			DialogUtil.closeProgress();
			switch (msg.what) 
			{
			// 更新发展人编码和密码到平台OK
			case MSG_LOGIN_SEND_CONFIG_OK:
				Intent intent = new Intent(BusiPhoneRegisterLogin.this, BusiPhoneRegisterMain.class);
				startActivity(intent);
				finish();
				break;
				
			// 登陆成功
			case MSG_LOGIN_SUC:
				App.setFirstRun(false);
				App.setAgentId(mStrAgentNo);
				App.setAgentPwd(mStrAgentPswd);
				// 当前是平台下发的发展人编码和密码, 不需要重复提交
				if(mIsShow[0] == 0x00)
				{
					mHandler.sendEmptyMessage(MSG_LOGIN_SEND_CONFIG_OK);
					break;
				}
				else
				{
					new Thread()
					{
						public void run() 
						{
							DialogUtil.showProgress("正在提交发展人信息....");
							if(mBusiness.bll000C(App.getAgentId(), App.getAgentPwd()) == false) 
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox(TITLE_STR, mBusiness.getLastknownError());
								return;
							}
							// 跳转到主界面
							mHandler.sendEmptyMessage(MSG_LOGIN_SEND_CONFIG_OK);
						}
					}.start();
				}
				break;
				
			// 登陆失败
			case MSG_LOGIN_FAIL:
				String desc1 = (String)msg.obj;
				if (TextUtils.isEmpty(desc1) || "anyType{}".equals(desc1)) 
				{
					DialogUtil.MsgBox(TITLE_STR, "登录失败, 请重试！");
				}
				else 
				{
					DialogUtil.MsgBox(TITLE_STR, desc1 + "");
				}
				mEditLoginAgentPwd.setText("");
				break;
				
			// 登陆超时
			case MSG_LOGIN_TIMEOUT:
				DialogUtil.MsgBox(TITLE_STR, "登录请求超时, 请重试！");
				mEditLoginAgentPwd.setText("");
				break;
				
			// 获取验证码成功
			case MSG_GET_IDENTIFY_ID_SUC:
				// 显示修改密码的布局
				showPage(0x03);
				mEditSetIdentify.setText("");
				mEditSetNewPswd.setText("");
				mEditSetEnsureNewPswd.setText("");
				// 验证码输入框获得输入焦点
				mEditSetIdentify.requestFocus();
				// 开启倒计时
				startCountDownTimer();
				// 显示提示信息
				back(null , TITLE_STR, (String)msg.obj, "确定", null, false, false, false);
				break;
				
			// 获取验证码失败
			case MSG_GET_IDENTIFY_ID_FAIL:
				String desc2 = (String)msg.obj;
				if(StringUtil.isEmptyOrNull(desc2)  || "anyType{}".equals(desc2)) 
				{
					DialogUtil.MsgBox(TITLE_STR, "您本次获取验证码失败, 请重新获取！");
				} 
				else 
				{
					DialogUtil.MsgBox(TITLE_STR, desc2+"");
				}
				break;
			
			// 获取验证码超时
			case MSG_GET_IDENTIFY_ID_TIMEOUT:
				DialogUtil.MsgBox(TITLE_STR, "随机码请求超时, 请重试！");
				break;
				
			// 修改密码成功
			case MSG_MODIFY_PSWD_SUC:
				// 取消倒计时
				mCountDownTimer.cancel();
				// 保存发展人编码和密码
				App.setAgentId(mStrAgentNo);
				App.setAgentPwd(mStrAgentPswd);
				back(
						new ICallBack() 
						{
							@Override
							public void back() 
							{
								// 显示登录布局
								showPage(0x01);
								// 设置发展人编码
								String strAgentNo = App.getAgentId();
								mEditLoginAgentNo.setText(strAgentNo);
								if(!StringUtil.isEmptyOrNull(strAgentNo)) 
								{
									mEditLoginAgentNo.setSelection(strAgentNo.length());
								}
								// 设置发展人密码
								String strAgentPswd = App.getAgentPwd();
								mEditLoginAgentPwd.setText(strAgentPswd);
								if(!StringUtil.isEmptyOrNull(strAgentPswd)) 
								{
									mEditLoginAgentPwd.setSelection(strAgentPswd.length());
								}
							}
							
							@Override
							public void cancel() 
							{
								// 显示登录布局
								showPage(0x01);
								// 设置发展人编码
								String strAgentNo = App.getAgentId();
								mEditLoginAgentNo.setText(strAgentNo);
								if(!StringUtil.isEmptyOrNull(strAgentNo)) 
								{
									mEditLoginAgentNo.setSelection(strAgentNo.length());
								}
								// 设置发展人密码
								String strAgentPswd = App.getAgentPwd();
								mEditLoginAgentPwd.setText(strAgentPswd);
								if(!StringUtil.isEmptyOrNull(strAgentPswd)) 
								{
									mEditLoginAgentPwd.setSelection(strAgentPswd.length());
								}
							}
		
							@Override
							public void dismiss() 
							{
								// 显示登录布局
								showPage(0x01);
								// 设置发展人编码
								String strAgentNo = App.getAgentId();
								mEditLoginAgentNo.setText(strAgentNo);
								if(!StringUtil.isEmptyOrNull(strAgentNo)) 
								{
									mEditLoginAgentNo.setSelection(strAgentNo.length());
								}
								// 设置发展人密码
								String strAgentPswd = App.getAgentPwd();
								mEditLoginAgentPwd.setText(strAgentPswd);
								if(!StringUtil.isEmptyOrNull(strAgentPswd)) 
								{
									mEditLoginAgentPwd.setSelection(strAgentPswd.length());
								}
							}
						}, 
						TITLE_STR, 
						"您的登录密码已修改成功，请您妥善保存修改后的密码！", 
						"确定", null, true, true, true);
				break;
				
			// 修改密码失败
			case MSG_MODIFY_PSWD_FAIL:
				// 取消倒计时
				mCountDownTimer.cancel();
				// 
				String desc3 = (String)msg.obj;
				if(StringUtil.isEmptyOrNull(desc3) || "anyType{}".equals(desc3))
				{
					DialogUtil.MsgBox(TITLE_STR, "密码修改请求失败, 请重试！");
				}
				else
				{
					DialogUtil.MsgBox(TITLE_STR, desc3+"");
				}
				mEditSetNewPswd.setText("");
				mEditSetEnsureNewPswd.setText("");
				// 隐藏修改密码的布局
				showPage(0x02);
				break;
			
			// 修改密码超时
			case MSG_MODIFY_PSWD_TIMEOUT:
				// 取消倒计时
				mCountDownTimer.cancel();
				// 
				DialogUtil.MsgBox(TITLE_STR, "服务请求超时, 请重试！");
				mEditSetNewPswd.setText("");
				mEditSetEnsureNewPswd.setText("");
				// 隐藏修改密码的布局
				showPage(0x02);
				break;
				
			default:
				break;
			}
		}
	};
	
	// 开启倒计时
	private void startCountDownTimer() 
	{
		mCountDownTimer.cancel();
		mCountDownTimer.start();
	}
	
	private CountDownTimer mCountDownTimer = new CountDownTimer(180*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			String tempstr = String.format("剩余%d秒", millisUntilFinished/1000);
			mTextSetRemainTime.setText(tempstr);
		}
		
		@Override
		public void onFinish() 
		{
			// 隐藏修改密码的布局
			showPage(0x02);
		}
	};

	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_busiaccept_phone_register_login);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_busiaccept_phone_register_login_orange);
		}
		App.init(this);
		mBusiness = new Business_00(this, equipmentService, socketService, dataBaseService);
		//初始化回调接口子类
		mChild = new CallBackChild(this);
		// 初始化控件
		initViews();
		// 显示登录布局
		showPage(0x01);
		
		boolean isAutoLogin = getIntent().getBooleanExtra(Statics.IS_AUTO_LOGIN, true);
		if(isAutoLogin)
		{ 
			// 处理自动登录
			procAutoLogin();
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	// 初始化布局
	private void initViews() 
	{
		// 初始化登录布局
		initLoginView();
		// 初始化激活布局
		initActivateView();
		// 初始化密码修改布局
		initPswdView();
	}
	
	// 初始化登录布局
	private void initLoginView() 
	{
		// 返回到业务办理
		mLinearBacktoBusiAccept = (LinearLayout) findViewById(R.id.linear_backto_busiaccept);
		mLinearBacktoBusiAccept.setOnClickListener(this);
		
		// 登录布局
		mLinearLogin = (LinearLayout) findViewById(R.id.register_login);
		// 发展人编码
		mImageLoginAgentNo = (ImageView) findViewById(R.id.iv_login_agent_no);
		mEditLoginAgentNo = (EditText) findViewById(R.id.et_login_agent_no);
		mEditLoginAgentNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mImageLoginAgentNo.setVisibility(View.VISIBLE);
				if (StringUtil.isEmptyOrNull(s.toString())) 
				{
					mImageLoginAgentNo.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mImageLoginAgentNo.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		String strAgentID = App.getAgentId();
		mEditLoginAgentNo.setText(strAgentID);
		if(!StringUtil.isEmptyOrNull(strAgentID)) 
		{
			mEditLoginAgentNo.setSelection(strAgentID.length());
		}
		
		// 密码
		mImageLoginAgentPswd = (ImageView) findViewById(R.id.iv_login_agent_pswd);
		mEditLoginAgentPwd = (EditText) findViewById(R.id.et_login_agent_pswd);
		mEditLoginAgentPwd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mImageLoginAgentPswd.setVisibility(View.VISIBLE);
				if (StringUtil.isEmptyOrNull(s.toString()) || s.length() != 6) 
				{
					mImageLoginAgentPswd.setImageResource(R.drawable.ic_error);
				}
				else
				{
					mImageLoginAgentPswd.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		String strAgentPswd = App.getAgentPwd();
		mEditLoginAgentPwd.setText(strAgentPswd);
		if(!StringUtil.isEmptyOrNull(strAgentPswd)) 
		{
			mEditLoginAgentPwd.setSelection(strAgentPswd.length());
		}
		//Log.e("", "发展人密码为: " + strAgentPswd);
		
		// 登录
		Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(this);
		// 激活
		Button btnLoginActivate = (Button) findViewById(R.id.btn_login_activate);
		btnLoginActivate.setOnClickListener(this);
		// 忘记密码
		Button btnLoginForgetPswd = (Button) findViewById(R.id.btn_login_forget_pswd);
		btnLoginForgetPswd.setOnClickListener(this);
	}
	
	// 初始化激活布局
	private void initActivateView() 
	{
		// 激活布局
		mLinearActivate = (LinearLayout) findViewById(R.id.register_activate);
		// 标题栏
		mTextActivateTitle = (TextView) findViewById(R.id.tv_activate_title);
		
		// 发展人编码
		mImageActivateAgentNo = (ImageView) findViewById(R.id.iv_activate_agent_no);
		mEditActivateAgentNo = (EditText) findViewById(R.id.et_activate_agent_no);
		mEditActivateAgentNo.setText(mEditLoginAgentNo.getText().toString().trim());
		mEditActivateAgentNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mImageActivateAgentNo.setVisibility(View.VISIBLE);
				if (StringUtil.isEmptyOrNull(s.toString())) 
				{
					mImageActivateAgentNo.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mImageActivateAgentNo.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 获取验证码
		Button btnActivateGetIdentify = (Button) findViewById(R.id.btn_activate_getrandom);
		btnActivateGetIdentify.setOnClickListener(this);
	}
	
	// 初始化密码修改布局
	private void initPswdView() 
	{
		// 密码修改布局
		mLinearSetPswd = (LinearLayout) findViewById(R.id.register_set_new_pswd);
		// 随机码
		mImageSetIdentify = (ImageView) findViewById(R.id.iv_set_random);
		mEditSetIdentify = (EditText) findViewById(R.id.et_set_random);
		mEditSetIdentify.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				// 
				String tempstr = s.toString();
				if (StringUtil.isEmptyOrNull(tempstr)) 
				{
					mImageSetIdentify.setVisibility(View.INVISIBLE);
					return;
				}
				// 
				mImageSetIdentify.setVisibility(View.VISIBLE);
				if(tempstr.length() != 6)
				{
					mImageSetIdentify.setImageResource(R.drawable.ic_error);
				}
				else 
				{
					mImageSetIdentify.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 新密码
		mImageSetNewPswd = (ImageView) findViewById(R.id.iv_set_new_pswd);
		mEditSetNewPswd = (EditText) findViewById(R.id.et_set_new_pswd);
		mEditSetNewPswd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				// 
				String tempstr = s.toString();
				if (StringUtil.isEmptyOrNull(tempstr)) 
				{
					mImageSetNewPswd.setVisibility(View.INVISIBLE);
					return;
				}
				// 
				mImageSetNewPswd.setVisibility(View.VISIBLE);
				if (tempstr.length() != 6) 
				{
					mImageSetNewPswd.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mImageSetNewPswd.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 重复新密码
		mImageSetEnsureNewPswd = (ImageView) findViewById(R.id.iv_set_ensure_new_pswd);
		mEditSetEnsureNewPswd = (EditText) findViewById(R.id.et_set_ensure_new_pswd);
		mEditSetEnsureNewPswd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				String tempstr = s.toString();
				if(StringUtil.isEmptyOrNull(tempstr)) 
				{
					mImageSetEnsureNewPswd.setVisibility(View.INVISIBLE);
					return;
				}
				// 
				mImageSetEnsureNewPswd.setVisibility(View.VISIBLE);
				if (tempstr.length() != 6) 
				{
					mImageSetEnsureNewPswd.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mImageSetEnsureNewPswd.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 倒计时
		mTextSetRemainTime = (TextView) findViewById(R.id.tv_set_remain_time);
		
		// 确认修改
		Button btnSetModify = (Button) findViewById(R.id.btn_set_modify);
		btnSetModify.setOnClickListener(this);
	}
	
	// 显示布局
	// 0x01-登录布局 0x02-激活布局 0x03-设置密码布局
	private void showPage(int nPageIdx)
	{
		mPageIdx = nPageIdx;
		// 显示登录布局
		if(nPageIdx == 0x01)
		{
			mLinearLogin.setVisibility(View.VISIBLE);
			mLinearActivate.setVisibility(View.GONE);
			mLinearSetPswd.setVisibility(View.GONE);
		}
		// 显示激活布局
		else if(nPageIdx == 0x02)
		{
			mLinearLogin.setVisibility(View.GONE);
			mLinearActivate.setVisibility(View.VISIBLE);
			mLinearSetPswd.setVisibility(View.GONE);
		}
		// 显示密码设置布局
		else if(nPageIdx == 0x03)
		{
			mLinearLogin.setVisibility(View.GONE);
			mLinearActivate.setVisibility(View.GONE);
			mLinearSetPswd.setVisibility(View.VISIBLE);
		}
	}
	
	// 处理自动登录
	private void procAutoLogin()
	{
		/*
		// 判断是否自动登录
		if(App.getAutoLoginFlag()) 
		{
			// 获取发展人工号和密码
			mStrAgentNo = App.getAgentId();
			mStrAgentPswd = App.getAgentPwd();
			if(StringUtil.isEmptyOrNull(mStrAgentNo) || StringUtil.isEmptyOrNull(mStrAgentPswd)) 
			{
				return;
			}
			// 
			Log.e("", "自动登录");
			DialogUtil.showProgress("正在登录...");
			if(StringUtil.isEmptyOrNull(mLac) || StringUtil.isEmptyOrNull(mCi))
			{
				getLacci();
			}
			mRequest = new MobileClientLoginRequest(mStrAgentNo.trim(), mStrAgentPswd.trim(), "1", mLac, mCi);
			executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mRequest, new MobileClientLoginListener());
		}
		*/
		
		new Thread() 
		{
			public void run() 
			{
				int[] 	nAgentNoLen = new int[1];
				byte[] 	szAgentNo = new byte[64];
				int[] 	nAgentPswdLen = new int[1];
				byte[] 	szAgentPswd = new byte[6];
				
				// 获取登录信息
				DialogUtil.showProgress("正在获取登录信息...");
				if(mBusiness.bll000B(mIsShow, nAgentNoLen, szAgentNo, nAgentPswdLen, szAgentPswd) == false) 
				{
					DialogUtil.closeProgress();
					return;
				}
				// 需要显示登录界面
				if(mIsShow[0] == 0x01) 
				{
					DialogUtil.closeProgress();
					return;
				}
				// 更新配置
				if(nAgentNoLen[0] != 0 && nAgentPswdLen[0] != 0) 
				{
					mStrAgentNo = Interface.getSubString(new String(szAgentNo));
					mStrAgentPswd = Interface.getSubString(new String(szAgentPswd));
					App.setAgentId(mStrAgentNo);
					App.setAgentPwd(mStrAgentPswd);
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							mEditLoginAgentNo.setText(mStrAgentNo);
							mEditLoginAgentPwd.setText(mStrAgentPswd);
						}
					});
				}
				// 自动登录
				DialogUtil.showProgress("正在登录...");
				if(StringUtil.isEmptyOrNull(mLac) || StringUtil.isEmptyOrNull(mCi)) 
				{
					getLacci();
				}
				mRequest = new MobileClientLoginRequest(App.getAgentId(), App.getAgentPwd(), "1", mLac, mCi);
				executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mRequest, new MobileClientLoginListener());
			}
		}.start();
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
		App.init(this);
		mSpiceManager.start(this);
	}
	
	@Override
	public void onStop() 
	{
		super.onStop();
		mSpiceManager.shouldStop();
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
	}

	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Global.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		// 返回到业务办理
		case R.id.linear_backto_busiaccept:
			back(mChild, null, null, "确定", "取消", true, false, false);
			break;
			
		// 登录
		case R.id.btn_login:
			handleLogin();
			break;
			
		// 第一次登录激活
		case R.id.btn_login_activate:
			// 显示激活页
			showPage(0x02);
			// 更新标题
			mTextActivateTitle.setText("激        活");
			// 设置发展人编码
			//String strAgentNo = App.getAgentId();
			String strAgentNo = mEditLoginAgentNo.getText().toString().trim();
			mEditActivateAgentNo.setText(strAgentNo);
			if(!StringUtil.isEmptyOrNull(strAgentNo)) 
			{
				mEditActivateAgentNo.setSelection(strAgentNo.length());
			}
			break;
			
		// 忘记密码
		case R.id.btn_login_forget_pswd:
			// 显示激活页
			showPage(0x02);
			// 更新标题
			mTextActivateTitle.setText("找回密码");
			// 设置发展人编码
			mEditActivateAgentNo.setText(mEditLoginAgentNo.getText().toString().trim());
			break;
			
		// 获取验证码
		case R.id.btn_activate_getrandom:
			String strTitle = mTextActivateTitle.getText().toString();
			if(strTitle.equals("找回密码"))
			{
				// 找回密码获取随机码
				handleForgetPswdGetRandom();
			}
			else
			{
				// 激活获取验证码
				handleActivateGetRandom();
			}
			break;
		
		// 设定新密码
		case R.id.btn_set_modify:
			String strType = mTextActivateTitle.getText().toString();
			if(strType.equals("找回密码"))
			{
				// 找回密码设定密码
				handleForgetPswdModifyPswd();
			}
			else
			{
				// 激活设定密码
				handleActivateModifyPswd();
			}
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			if(mPageIdx == 0x01)
			{
				back(mChild, null, null, "确定", "取消", true, false, false);
				return true;
			}
			// 取消激活或者取消找回密码
			else if(mPageIdx == 0x02)
			{
				// 显示登录布局
				showPage(0x01);
				// 设置发展人编码和密码
				/*
				String strAgentID = App.getAgentId();
				mEditLoginAgentNo.setText(strAgentID);
				if(!StringUtil.isEmptyOrNull(strAgentID)) 
				{
					mEditLoginAgentNo.setSelection(strAgentID.length());
				}
				*/
				String strAgentPswd = App.getAgentPwd();
				mEditLoginAgentPwd.setText(strAgentPswd);
				if(!StringUtil.isEmptyOrNull(strAgentPswd)) 
				{
					mEditLoginAgentPwd.setSelection(strAgentPswd.length());
				}
				return true;
			}
			// 取消设定密码
			else if(mPageIdx == 0x03)
			{
				// 显示获取验证码的布局
				showPage(0x02);
				// 设置发展人
				String strAgentID = App.getAgentId();
				mEditActivateAgentNo.setText(strAgentID);
				if(!StringUtil.isEmptyOrNull(strAgentID)) 
				{
					mEditActivateAgentNo.setSelection(strAgentID.length());
				}
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// 获取小区
	public void getLacci()
	{
		try
		{
			TelephonyManager mainTel =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			GsmCellLocation gcl = (GsmCellLocation) mainTel.getCellLocation();
			mLac = String.valueOf(gcl.getLac());
			mCi = String.valueOf(gcl.getCid());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			mLac = "";
			mCi = "";
		}
	}
	
	// 处理登陆
	private void handleLogin() 
	{
		// 校验发展人编码
		mStrAgentNo = mEditLoginAgentNo.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrAgentNo)) 
		{
			mImageLoginAgentNo.setVisibility(View.VISIBLE);
			mImageLoginAgentNo.setImageResource(R.drawable.ic_error);
			mEditLoginAgentNo.requestFocus();
			DialogUtil.MsgBox(TITLE_STR, "请输入发展人编码！");
			return;
		}
		// 校验发展人密码
		mStrAgentPswd = mEditLoginAgentPwd.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrAgentPswd)) 
		{
			mImageLoginAgentPswd.setVisibility(View.VISIBLE);
			mImageLoginAgentPswd.setImageResource(R.drawable.ic_error);
			mEditLoginAgentPwd.requestFocus();
			DialogUtil.MsgBox(TITLE_STR, "请输入密码！");
			return;
		}
		if(mStrAgentPswd.length() != 6) 
		{
			mImageLoginAgentPswd.setVisibility(View.VISIBLE);
			mImageLoginAgentPswd.setImageResource(R.drawable.ic_error);
			mEditLoginAgentPwd.requestFocus();
			DialogUtil.MsgBox(TITLE_STR, "请输入6位密码！");
			return;
		}
		
		if(!DeviceUtils.hasInternet())
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		// 更改标志位0x01以保证登录成功后更新发展人信息
		mIsShow[0] = 0x01;
		
		DialogUtil.showProgress("正在登录...");
		if(StringUtil.isEmptyOrNull(mLac) || StringUtil.isEmptyOrNull(mCi))
		{
			getLacci();
		}
		mRequest = new MobileClientLoginRequest(mStrAgentNo.trim(), mStrAgentPswd.trim(), "1", mLac, mCi);
		executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mRequest, new MobileClientLoginListener());
	}
	
	public final class MobileClientLoginListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_LOGIN_FAIL, "登录请求超时, 请重试！").sendToTarget();
			}
			else 
			{
				mHandler.sendEmptyMessage(MSG_LOGIN_FAIL);
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String, String> result = mRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 保存登录状态
					App.setAccessToken("login");
					// 保存密钥
					String key = result.get(BaseRequest.REGISTER_KEY);
					App.setDesKey(key);
					
					/*
					// FTP服务器参数
					// FTP服务器IP
					String ftpSerIP = result.get("ftpSerIP");
					ftpSerIP = DES.decryptDES(ftpSerIP, ApiClient.mEncryptKey);
					ApiClient.FTP_URL = ftpSerIP;
					// FTP服务器端口
					String ftpSerPort = result.get("ftpSerPort");
					ftpSerPort = DES.decryptDES(ftpSerPort, ApiClient.mEncryptKey);
					ApiClient.FTP_PORT = ftpSerPort;
					// FTP服务器用户名
					String ftpSerUser = result.get("ftpSerUser");
					ftpSerUser = DES.decryptDES(ftpSerUser, ApiClient.mEncryptKey);
					ApiClient.FTP_USER_NAME = ftpSerUser;
					// FTP服务器密码
					String ftpSerPass = result.get("ftpSerPass");
					ftpSerPass = DES.decryptDES(ftpSerPass, ApiClient.mEncryptKey);
					ApiClient.FTP_USER_PWD = ftpSerPass;
					// 上传路径
					String upLoadPATH = result.get("upLoadPATH");
					upLoadPATH = DES.decryptDES(upLoadPATH, ApiClient.mEncryptKey);
					ApiClient.FTP_DIR = upLoadPATH;
					*/
					
					// 检查公告
					Global.checkMail(BusiPhoneRegisterLogin.this, result);
					mHandler.sendEmptyMessage(MSG_LOGIN_SUC);
				}
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_LOGIN_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				mHandler.sendEmptyMessage(MSG_LOGIN_FAIL);
			}
		}
	}
	
	// 激活获取验证码
	private void handleActivateGetRandom()
	{
		// 校验发展人密码
		mStrAgentNo = mEditActivateAgentNo.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrAgentNo)) 
		{
			mImageActivateAgentNo.setVisibility(View.VISIBLE);
			mImageActivateAgentNo.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "请输入发展人编码！");
			mEditActivateAgentNo.requestFocus();
			return;
		}
		
		if(!DeviceUtils.hasInternet()) 
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在获取校验码...");
		mGetIdentifyIDRequest = new GetIdentifyIDRequest(mStrAgentNo, null);
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mGetIdentifyIDRequest, new ActiavteGetIdentifyIDListener());
	}
	
	// 激活获取验证码回调
	private final class ActiavteGetIdentifyIDListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, "验证码请求超时, 请重试！").sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mGetIdentifyIDRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					//String key = result.get("registerKey");
					//App.setAccessToken(key);
					mCommunicationID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_SUC, desc).sendToTarget();
				} 
				else 
				{
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}
	}
	
	// 校验用户输入
	private boolean checkPswdInput() 
	{
		// 检测校验码
		mStrIdentify = mEditSetIdentify.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrIdentify)) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入验证码！");
			return false;
		}
		if (mStrIdentify.length() != 6) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请正确输入验证码！");
			return false;
		}
		// 检测密码
		final String strNewPswd = mEditSetNewPswd.getText().toString();
		if (StringUtil.isEmptyOrNull(strNewPswd)) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入新密码！");
			return false;
		}
		if (strNewPswd.length() != 6) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请正确输入新密码！");
			return false;
		}
		// 检测确认密码
		final String strEnsureNewPswd = mEditSetEnsureNewPswd.getText().toString();
		if (StringUtil.isEmptyOrNull(strEnsureNewPswd)) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请输入确认密码！");
			return false;
		}
		if (strEnsureNewPswd.length() != 6) 
		{
			DialogUtil.MsgBox(TITLE_STR, "请正确输入确认密码！");
			return false;
		}
		// 检测新密码和确认密码的一致性
		if(strNewPswd.equals(strEnsureNewPswd) == false)
		{
			DialogUtil.MsgBox(TITLE_STR, "新密码与确认密码不一致, 请重新输入！");
			return false;
		}
		if(Global.isSimple(strNewPswd)) 
		{
			DialogUtil.MsgBox(TITLE_STR, "您输入的新密码过于简单，请重新输入！");
			return false;
		}
		mStrAgentPswd = strNewPswd;
		return true;
	}
	
	// 激活设定密码
	private void handleActivateModifyPswd() 
	{
		// 检测密码输入
		if(checkPswdInput() == false)
		{
			return;
		}
		
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在修改登录密码...");
		mAgentActivateRequest = new AgentActivateRequest(mCommunicationID, mStrAgentNo, mStrIdentify, mStrAgentPswd);
		executeRequest(mHandler, MSG_MODIFY_PSWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mAgentActivateRequest, new ActiveteModifyPswdListener());
	}
	
	// 修改密码返回
	private final class ActiveteModifyPswdListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, "修改密码请求超时, 请重试！").sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mAgentActivateRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mHandler.obtainMessage(MSG_MODIFY_PSWD_SUC).sendToTarget();
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();	
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}
	}
	
	// 找回密码获取验证码
	// 找回密码
	private void handleForgetPswdGetRandom() 
	{
		// 校验发展人编码
		mStrAgentNo = mEditActivateAgentNo.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrAgentNo)) 
		{
			mImageActivateAgentNo.setVisibility(View.VISIBLE);
			mImageActivateAgentNo.setImageResource(R.drawable.ic_error);
			DialogUtil.MsgBox(TITLE_STR, "请输入发展人编码！");
			mEditActivateAgentNo.requestFocus();
			return;
		}
		// 保存发展人编码
		App.setAgentId(mStrAgentNo);
		
		if(!DeviceUtils.hasInternet()) 
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在获取校验码...");
		mGetIdentifyIDRequest2 = new GetIdentifyIDRequest2(mStrAgentNo, null);
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mGetIdentifyIDRequest2, new GetIdentifyID2Listener());
	}
	
	// 找回密码获取验证码
	public final class GetIdentifyID2Listener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, "验证码请求超时, 请重试！").sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mGetIdentifyIDRequest2.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					//String key = result.get("registerKey");
					//App.setAccessToken(key);
					mCommunicationID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_SUC, desc).sendToTarget();
				} 
				else 
				{
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}
	}
	
	// 找回密码设定新密码
	private void handleForgetPswdModifyPswd() 
	{
		// 检测密码输入
		if(checkPswdInput() == false)
		{
			return;
		}
		
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_exception));
			return;
		}
		
		DialogUtil.showProgress("正在修改登录密码...");
		mAlterAgentPasswdRequest2 = new AlterAgentPasswdRequest2(mStrAgentNo, null, mStrAgentPswd, mStrIdentify, mCommunicationID);
		executeRequest(mHandler, MSG_MODIFY_PSWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mAlterAgentPasswdRequest2, new ForgetPswdModifyPswdListener());
	}
	
	// 修改密码返回
	public final class ForgetPswdModifyPswdListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, "修改密码请求超时, 请重试！").sendToTarget();
			} else {
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mAlterAgentPasswdRequest2.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mHandler.obtainMessage(MSG_MODIFY_PSWD_SUC).sendToTarget();
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				// 取消定时器
				cancelTimer();
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}
	}
}
