package com.sunnada.baseframe.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Rect;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sunnada.baseframe.service.DataBaseService.DataBaseBinder;
import com.sunnada.baseframe.service.EquipmentService.EquipmentBinder;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.service.SocketService.SocketBinder;

import com.sunnada.baseframe.activity.busiaccept.robospice.BaseRequest;
import com.sunnada.baseframe.activity.busiaccept.robospice.SampleSpiceService;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.SystemParamsDao;
import com.sunnada.baseframe.dialog.CallBackChild;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.listener.OnExceptionListener;
import com.sunnada.baseframe.thread.Thread;
//import com.sunnada.livetercbs.ui.UIHandle;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.ui.MyCustomButtonOrange;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.Rotate3d;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.bluetooth.BluetoothClient;

/**
 * File: BaseActivity.java
 * Description: 
 * @author 丘富铨
 * @date 2013-3-7
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public abstract class BaseActivity extends Activity implements ServiceConnection, OnTouchListener
{
	public static final String DESCRIPTOR_EUIPMENTSERVICE 	= "com.sunnada.baseframe.service.EquipmentService";
	public static final String DESCRIPTOR_SOCKETSERVICE 	= "com.sunnada.baseframe.service.SocketService";
	public static final String DESCRIPTOR_DATABASEERVICE 	= "com.sunnada.baseframe.service.DataBaseService";

	public static IEquipmentService 	equipmentService 	= null;		// 设备服务
	public static ISocketService 		socketService 		= null;		// 网络基础服务
	public static IDataBaseService 		dataBaseService 	= null;		// 数据库基础服务
	public static BluetoothClient		mBluetoothClient	= new BluetoothClient();
	
	protected InputMethodManager 		inputMethodManager 		= null;	// 输入法管理器
	private boolean 					autoReleaseInputMethod 	= true;	// 是否自动影藏输入法
	
	private Timer 						checkServiceBindTimer 	= null;
	private int 						checkServiceBindCount 	= 10;	// 检测服务绑定次数
	
	protected SpiceManager 				mSpiceManager 		= new SpiceManager(SampleSpiceService.class);
	private Timer						mRequestTimer 		= null;
	private Toast						mErrorToast			= null;
	private SystemParamsDao				mSystemParams		= null;
	
	public CallBackChild                mChild;                        // 回调接口子类  
	
	protected void onCreate(android.os.Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		Thread.setDefaultOnExceptionListener(new OnExceptionListener()
		{
			@Override
			public void onException(Exception e)
			{
				Log.log("xxxxxxxxxxxxxxxxxxxxxx系统出现未知异常xxxxxxxxxxxxxxxxxxxxxx");
				Log.log(e);
			}
		});
		
		// 绑定服务
		serviceBind();
		// 检测服务绑定
		startInitCheck();
		// 获得输入法服务
		inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		mSystemParams = new SystemParamsDao(this);
	}
	
	@Override
	protected void onDestroy() 
	{
		unbindService(this);
		super.onDestroy();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// 自动隐藏输入法
		if(autoReleaseInputMethod)
		{
			autoReleaseInputMethod(event);
		}
		return super.onTouchEvent(event);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else if(v instanceof MyCustomButton2)
		{
			((MyCustomButton2)v).onTouch(event);
		}
		else if(v instanceof MyCustomButtonOrange)
		{
			((MyCustomButtonOrange)v).onTouch(event);
		}
		return false;
	}
	
	// 启动服务
	protected void serviceStart() 
	{
		startService(new Intent(DESCRIPTOR_EUIPMENTSERVICE));
		startService(new Intent(DESCRIPTOR_SOCKETSERVICE));
		startService(new Intent(DESCRIPTOR_DATABASEERVICE));
	}
	
	// 绑定服务
	protected void serviceBind() 
	{
		bindService(new Intent(DESCRIPTOR_EUIPMENTSERVICE), this, Context.BIND_AUTO_CREATE);
		bindService(new Intent(DESCRIPTOR_SOCKETSERVICE),  this, Context.BIND_AUTO_CREATE);
		bindService(new Intent(DESCRIPTOR_DATABASEERVICE), this, Context.BIND_AUTO_CREATE);
	}
	
	// 停止服务
	protected void serviceStop() 
	{
		stopService(new Intent(DESCRIPTOR_EUIPMENTSERVICE));
		stopService(new Intent(DESCRIPTOR_SOCKETSERVICE));
		stopService(new Intent(DESCRIPTOR_DATABASEERVICE));
	}
	
	// 使用计时器来轮询检测服务是否绑定
	private void startInitCheck()
	{
		if(checkServiceBindTimer != null)
		{
			checkServiceBindTimer.cancel();
		}
		
		checkServiceBindTimer = new Timer();
		checkServiceBindTimer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				if(checkServiceBindCount == 0)
				{
					if(equipmentService == null || socketService == null || dataBaseService == null)
					{
						// 只要有一个服务绑定失败则失败
						runOnUiThread(new Runnable() 
						{
							public void run() 
							{
								onInitFail();
							}
						});
					}
					cancel();
				}
				
				if(equipmentService != null && socketService != null && dataBaseService != null)
				{
					// 所有的服务绑定完毕
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							onInitSuccess();
						}
					});
					cancel();
				}
				checkServiceBindCount--;
			}
		}, 1000, 1000);
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder binder)
	{
		if(name.getClassName().equals(DESCRIPTOR_EUIPMENTSERVICE))
		{
			equipmentService = (IEquipmentService)(((EquipmentBinder)binder).getService());
		}
		else if(name.getClassName().equals(DESCRIPTOR_SOCKETSERVICE))
		{
			socketService = (ISocketService)(((SocketBinder)binder).getService());
		}
		else if(name.getClassName().equals(DESCRIPTOR_DATABASEERVICE))
		{
			dataBaseService = (IDataBaseService)(((DataBaseBinder)binder).getService());
		}
		Log.log("绑定服务成功ComponentName = "+name.getClassName());
	}
	
	@Override
	public void onServiceDisconnected(ComponentName name)
	{
		Log.log("xxxxxxxxxxxxxxxxxxxxxxxxxx绑定服务失败ComponentName = "+name.getClassName()+"xxxxxxxxxxxxxxxxxxxxxxxxxx");
	};
	
	// 输入框失去焦点后自动隐藏输入法
	public void autoReleaseInputMethod(MotionEvent event)
	{
		View  v = this.getCurrentFocus();
		
		int x = (int) event.getX();
		int y = (int) event.getY();
		
		if(v != null && (v instanceof EditText))
		{
			Rect rect = new Rect();
			v.getGlobalVisibleRect(rect);
			if(!rect.contains(x, y))
			{
				inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
	}
	
	public boolean isAutoReleaseInputMethod()
	{
		return autoReleaseInputMethod;
	}
	
	public void setAutoReleaseInputMethod(boolean autoReleaseInputMethod)
	{
		this.autoReleaseInputMethod = autoReleaseInputMethod;
	}
	
	public abstract void onInitSuccess();
	public abstract void onInitFail();
	
	// 启动翻转效果
	protected void startRotation(View v)
	{
		Rotate3d rotate3D = new Rotate3d(0, 360, v.getWidth()/2.0f, v.getHeight()/2.0f, 10.0f, false);
		rotate3D.setDuration(150);
		rotate3D.setFillAfter(true);
		//rotate3D.setInterpolator(new OvershootInterpolator());
		rotate3D.setInterpolator(new DecelerateInterpolator());
		v.startAnimation(rotate3D);
	}
	
	// 请求服务
	public void executeRequest(final Handler handler, final int msgID, long timeout, BaseRequest request, RequestListener<String> requestListener) 
	{
		if(request != null) 
		{
			mSpiceManager.execute(request, requestListener);
		}
		mRequestTimer = null;
		mRequestTimer = new Timer();
		mRequestTimer.schedule(new TimerTask() 
		{
			@Override
			public void run() 
			{
				Log.e("", "请求超时!");
				DialogUtil.closeProgress();
				handler.sendEmptyMessage(msgID);
				mRequestTimer = null;
			}
		}, timeout);
	}
	
	// 取消定时器
	public void cancelTimer()
	{
		if(mRequestTimer != null) 
		{
			mRequestTimer.cancel();
			mRequestTimer = null;
		}
	}
	
	public void showErrorMsg(String text) 
	{
		if (mErrorToast == null)
		{
			mErrorToast = new Toast(getBaseContext());
			mErrorToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
			mErrorToast.setDuration(Toast.LENGTH_SHORT);
		}
		else
		{
			// errorToast.cancel();
		}
		View msgView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			 msgView  = getLayoutInflater().inflate(R.layout.lay_toast_compareerror, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			 msgView = getLayoutInflater().inflate(R.layout.lay_toast_compareerror_orange, null);
		}
		TextView tv_msg = (TextView) msgView.findViewById(R.id.tv_msg);
		tv_msg.setText(text);
		mErrorToast.setView(msgView);
		mErrorToast.show();
	}
	
	// 回退
	public void back(final ICallBack callBack, String title, String msg, String strOk, String strCancle,
			boolean isUseOkListener, boolean isUseCancleListener, boolean isUseDismissListener)
	{
		String tempTitle = "提示信息";
		String tempMsg = Statics.EXIT_TIP;
		
		if(!StringUtil.isEmptyOrNull(title)) 
		{
			tempTitle = title;
		}
		
		if(!StringUtil.isEmptyOrNull(msg)) 
		{
			tempMsg = msg;
		}
		
		DialogUtil.MsgBox(tempTitle, 
			tempMsg, 
			strOk, 
			(isUseOkListener == false? null:new View.OnClickListener() 
			{	
				public void onClick(View v) 
				{
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							callBack.back();
						}
					});
				}
			}), 
			strCancle,
			(isUseCancleListener == false ? null:new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							callBack.cancel();
						}
					});
				}
			}), 
			(isUseDismissListener == false ? null:new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							callBack.dismiss();
						}
					}); 
				}
			}));
	}
	
	// 获取菜单权限
	protected boolean getMenuAuthority(String authority)
	{
		if(mSystemParams.SelectParamsValues(authority) != null)
		{
			return true;
		}
		return false;
	}

}
