package com.sunnada.baseframe.activity.charge;

import java.io.File;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessBuycard;
import com.sunnada.baseframe.business.BusinessEPay;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.BuycardTipDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;

public class BusiChargeBuycard extends BusiCharge implements OnClickListener, ViewGetter
{
	// 第一步(充值信息输入)
	private ButtonGroupUtil			mBtnGroupFaceValue;
	private ButtonGroupUtil			mBtnGroupCount;
	private EditText				mEditPswd;										// 交易密码输入框
	
	private BuycardTipDialog		mBuycardTipDialog		= null;
	private String 					mStrEcardFaceValue 		= "20";
	private String					mStrEcardCount			= "1";
	private String					mStrPswd;
	
	private BusinessBuycard			mBuycard				= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_charge_buycard);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_charge_buycard_orange);
		}
		// 初始化控件
		initViews();
	    // 检测异常交易
		mBuycard = new BusinessBuycard(this, equipmentService, socketService, dataBaseService, mHandler);
		checkErrorDeal(new String[] {"07"});
	}
	
	// 初始化控件
	protected void initViews() 
	{
		// 返回到充值中心二级菜单
		LinearLayout linearBacktoCharge = (LinearLayout) findViewById(R.id.linear_backto_recharge);
		linearBacktoCharge.setOnClickListener(this);
		
		// 电子卡面值
		try
		{
			mBtnGroupFaceValue = ButtonGroupUtil.createBean(this, 
					new int[] {R.id.btn_20, R.id.btn_30, R.id.btn_50, R.id.btn_100, R.id.btn_300}, 
					new String[] {"20", "30", "50", "100", "300"},  
					R.drawable.btn_tip_select, 
					R.drawable.btn_tip_selected);
			mBtnGroupFaceValue.setSelected(0);
			mBtnGroupFaceValue.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					if(newId == R.id.btn_20) 
					{
						mBuycard.mFaceIdx = 0x01;
						mStrEcardFaceValue = "20";
					}
					else if(newId == R.id.btn_30) 
					{
						mBuycard.mFaceIdx = 0x02;
						mStrEcardFaceValue = "30";
					}
					else if(newId == R.id.btn_50) 
					{
						mBuycard.mFaceIdx = 0x03;
						mStrEcardFaceValue = "50";
					}
					else if(newId == R.id.btn_100) 
					{
						mBuycard.mFaceIdx = 0x04;
						mStrEcardFaceValue = "100";
					}
					else if(newId == R.id.btn_300) 
					{
						mBuycard.mFaceIdx = 0x05;
						mStrEcardFaceValue = "300";
					}
				}
			});
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		// 电子卡张数
		try
		{
			mBtnGroupCount = ButtonGroupUtil.createBean(this, 
					new int[] {R.id.btn_1, R.id.btn_2, R.id.btn_3, R.id.btn_4, R.id.btn_5}, 
					new String[] {"1", "2", "3", "4", "5"}, 
					R.drawable.btn_tip_select, 
					R.drawable.btn_tip_selected);
			mBtnGroupCount.setSelected(0);
			mBtnGroupCount.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					if(newId == R.id.btn_1) 
					{
						mBuycard.mEcardNum = 0x01;
						mStrEcardCount = "1";
					}
					else if(newId == R.id.btn_2) 
					{
						mBuycard.mEcardNum = 0x02;
						mStrEcardCount = "2";
					}
					else if(newId == R.id.btn_3) 
					{
						mBuycard.mEcardNum = 0x03;
						mStrEcardCount = "3";
					}
					else if(newId == R.id.btn_4) 
					{
						mBuycard.mEcardNum = 0x04;
						mStrEcardCount = "4";
					}
					else if(newId == R.id.btn_5) 
					{
						mBuycard.mEcardNum = 0x05;
						mStrEcardCount = "5";
					}
				}
			});
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		// 交易密码
		mEditPswd = (EditText) findViewById(R.id.et_password);
		
		// 提交
		Button btnSubmit = (Button) findViewById(R.id.btn_step1_submit);
		btnSubmit.setOnClickListener(this);
	}
	
	/*
	// 检测电子卡交易异常状态
	private void checkEcardStatus() 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在检测打印机状态...");
				if(checkPrinter(Statics.MSG_CHECK_PRINTER, Statics.MSG_CHECK_PRINTER_CANCEL) == false) 
				{
					return;
				}
				// 检测电子卡异常文件
				DialogUtil.showProgress("正在检测异常电子卡信息...");
				SystemClock.sleep(500);
				if(checkEcardFile()) 
				{
					return;
				}
				// 检测异常交易
				DialogUtil.showProgress("正在检测异常交易...");
				SystemClock.sleep(500);
				
				DialogUtil.closeProgress();
			}
		}.start();
	}
	*/
	
	/*
	// 检测电子卡卡密文件
	private boolean checkEcardFile() 
	{
		File file = new File(BusinessBuycard.mEcardFile);
		if(file.exists() == false) 
		{
			return false;
		}
		try {
			FileInputStream in = new FileInputStream(file);
			if(in.available() < 80) 
			{
				file.delete();
				Log.e(BusinessBuycard.TAG, "电子卡卡密文件长度非法");
				return false;
			}
			if(mBuycard.mEcardInfo == null) 
			{
				mBuycard.mEcardInfo = new StringBuffer();
			}
			if(mBuycard.mEcardInfo.length() != 0) 
			{
				mBuycard.mEcardInfo.delete(0, mBuycard.mEcardInfo.length());
			}

			// 电子卡卡密信息
			byte[] cardnum = new byte[1];
			byte[] cardno  = new byte[20];
			byte[] cardpw  = new byte[20];
			byte[] faceIdx = new byte[1];
			byte[] endtime = new byte[8];
			// 读取卡密信息
			in.read(cardnum, 0, 1);
			for(byte i=0; i<cardnum[0]; i++) 
			{
				in.read(cardno, 0, 20);
				in.read(cardpw, 0, 20);
				in.read(faceIdx,0, 1);
				in.read(endtime,0, 8);

				mBuycard.mEcardInfo.append("\n账号:")
				 	.append(new String(cardno), 0, Commen.endofstr(cardno))
				 	.append("\n密码:")
				 	.append(new String(cardpw), 0, Commen.endofstr(cardpw))
				 	.append("\n面值:")
				 	.append(Statics.DZK_Type[faceIdx[0]-1][1] + "元")
				 	.append("\n有效期:")
				 	.append(StringUtil.converDateString(new String(endtime))).append("\n");
			}
			// 获取流水
			byte[] bllnum = new byte[30];
			in.read(bllnum, 0, 30);
			mBuycard.mStrBillNum = new String(bllnum, "ASCII");
			
			// 银联签购单信息
			//byte temp = (byte)in.read();
			//if(temp == 0x01) 
			//{
			//	mBuyCard.postype = 2;
			//	BaseBll.readPosDealInfo(in, mBuyCard.mPosDealTime, mBuyCard.mPosDealMoney);
			//}
			in.close();
			in = null;
			
			DialogUtil.closeProgress();
			DialogUtil.MsgBox(
					BusinessBuycard.TITLE_STR, 
					"上笔电子卡未打印成功,卡密列表如下\n" + mBuycard.mEcardInfo.toString(), 
					"确定", 
					Statics.MSG_PRINT, 
					"取消", 
					Statics.MSG_PRINT_CANCEL, 
					Statics.MSG_PRINT_CANCEL, 
					mHandler);
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	*/
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回二级菜单
		case R.id.linear_backto_recharge:
			back2Main();
			break;
			
		// 提交购买
		case R.id.btn_step1_submit:
			handleBuycard();
			break;
			
		default:
			break;
		}
	}
	
	// 清空所有填写框
	protected void clearStep() 
	{
		// 电子卡面值
		mBtnGroupFaceValue.setSelected(0);
		mStrEcardFaceValue = "20";
		mBuycard.mFaceIdx = 0x01;
		// 电子卡张数
		mBtnGroupCount.setSelected(0);
		mStrEcardCount = "1";
		mBuycard.mEcardNum = 0x01;
		// 交易密码
		mEditPswd.setText("");
	}
	
	// 处理充值请求
	private void handleBuycard() 
	{
		// 校验交易密码
		mStrPswd = mEditPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrPswd)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入交易密码！");
			return;
		}
		if(mStrPswd.length() != 6) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入6位交易密码！");
			return;
		}
		if(!Statics.HAD_PWD)
		{
			if(Statics.MODIFY_TYPE == 1)
			{
				DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
				"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
			}
			else if(Statics.MODIFY_TYPE == 2)
			{
				DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
						"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
			}
		}
		else
		{
			mBuycard.mStrPswd = mStrPswd;
			
			// 显示购买电子卡的提示框
			if(mBuycardTipDialog != null) 
			{
				mBuycardTipDialog = null;
			}
			mBuycardTipDialog = new BuycardTipDialog(this, mStrEcardFaceValue, mStrEcardCount, mHandler);
			mBuycardTipDialog.show();
		}
	}
	
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
				
			// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				new ModifyPswdDialog(BusiChargeBuycard.this, 
				new Business_00(BusiChargeBuycard.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				new ResetSrvDialog(BusiChargeBuycard.this, 
				new Business_00(BusiChargeBuycard.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 修改交易密码成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mEditPswd.setText("");
				break;
					
			// 检测打印机状态
			case Statics.MSG_CHECK_PRINTER:
			case Statics.MSG_CHECK_PRINTER_CANCEL:
				finish();
				break;
				
			// 取消购买
			case Statics.MSG_BUYCARD_CANCEL:
				clearStep();
				break;
				
			// 确定购买
			case Statics.MSG_BUYCARD_OK:
				// 发起购买电子卡流程
				new Thread() 
				{
					public void run() 
					{
						mBuycard.doBuyCard();
						runOnUiThread(new Runnable() 
						{
							public void run() 
							{
								clearStep();
							}
						});
					}
				}.start();
				break;
				
			// 获取卡密成功, 打印卡密
			case Statics.MSG_PRINT:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙设备
						DialogUtil.showProgress("正在检测蓝牙设备...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("请连接蓝牙设备", 
									"检测到当前无蓝牙打印设备, 是否连接蓝牙?", 
									"立即搜索", 
									Statics.MSG_BUYCARD_OK_CONNECT_BLUETOOTH, 
									"取消", 
									Statics.MSG_PRINT_CANCEL, 
									Statics.MSG_PRINT_CANCEL, 
									mHandler);
							return;
						}
						// 打印卡密信息
						printEcardInfo();
					};
				}.start();
				break;
				
			// 连接蓝牙设备
			case Statics.MSG_BUYCARD_OK_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiChargeBuycard.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_BUYCARD_OK_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_BUYCARD_OK_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙设备
						DialogUtil.showProgress("正在检测蓝牙设备状态...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
							return;
						}
						// 打印卡密信息
						printEcardInfo();
					}
				}.start();
				break;
				
			// 打印失败, 取消打印
			case Statics.MSG_PRINT_CANCEL:
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！",
				"确定", Statics.MSG_BUYCARD_REPORT, "", 0x00, Statics.MSG_BUYCARD_REPORT, mHandler);
				break;
				
			// 打印成功, 取消重新打印
			case Statics.MSG_PRINT_OK:
				//DialogUtil.showToast("打印成功", 0x00);
				// 删除卡密信息文件
				File file = new File(BusinessBuycard.mEcardFile);
				if(file.exists()) 
				{
					file.delete();
				}
				mHandler.sendEmptyMessage(Statics.MSG_BUYCARD_OK_REPORT);
				break;
				
			// 卡密打印OK上报
			case Statics.MSG_BUYCARD_OK_REPORT:
				reportEcardBill(0x01, 
						Statics.MSG_BUYCARD_OK_REPORT, 
						Statics.MSG_BUYCARD_OK_REPORT_FAIL, 
						Statics.MSG_BUYCARD_OK_REPORT_OK);
				break;
				
			case Statics.MSG_BUYCARD_REPORT:
				reportEcardBill(0x02, Statics.MSG_BUYCARD_REPORT, 0x00, Statics.MSG_BUYCARD_OK_REPORT_OK);
				break;
				
			// 卡密打印OK上报成功
			case Statics.MSG_BUYCARD_OK_REPORT_OK:
			// 卡密打印OK上报失败
			case Statics.MSG_BUYCARD_OK_REPORT_FAIL:
				finish();
				break;
			
			// 退出
			case Statics.MSG_FINISH:
				finish();
				break;
				
			default:
				break;
			}
		}
	};
	
	// 卡密上报
	// nReportResult: 0x01-打印成功 0x02-打印失败
	// msgOkId: 上报失败, 按下确定按钮发出去的消息
	// msgID1：上报失败, 按下取消按钮发出去的消息
	// msgID2：上报成功发出去的消息
	private void reportEcardBill(final int nReportResult, final int msgOkID, final int msgID1, final int msgID2) 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在处理...");
				boolean result = mBuycard.bll030A(nReportResult);
				DialogUtil.closeProgress();
				if(result == false) 
				{
					DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
							mBuycard.getLastknownError() + "\n是否重试?", 
							"确定", 
							msgOkID, 
							"取消", 
							msgID1, 
							msgID1, 
							mHandler);
				}
				else
				{
					if(msgID2 != 0x00) 
					{
						mHandler.sendEmptyMessage(msgID2);
					}
				}
			}
		}.start();
	}
	
	// 打印卡密信息
	private void printEcardInfo() 
	{
		// 检测蓝牙打印机状态
		DialogUtil.showProgress("正在检测蓝牙打印机状态...");
		if(checkPrinter(Statics.MSG_PRINT, Statics.MSG_PRINT_CANCEL, mHandler) == false) 
		{
			DialogUtil.closeProgress();
			return;
		}
		// 打印
		DialogUtil.showProgress("正在打印...");
		mBuycard.print();
		DialogUtil.closeProgress();
		
		if (!equipmentService.isPrintSuccess()) 
		{
			DialogUtil.MsgBox(PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.STR_FAIL_MSG,
					PrintUtil.STR_FAIL_OK,
					Statics.MSG_PRINT, 
					PrintUtil.STR_FAIL_CANCEL, 
					Statics.MSG_PRINT_CANCEL, 
					Statics.MSG_PRINT_CANCEL, 
					mHandler);
		}
		else
		{
			DialogUtil.MsgBox(PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.STR_SUC_MSG_REPRINT,
					PrintUtil.STR_FAIL_OK, 
					Statics.MSG_PRINT, 
					PrintUtil.STR_FAIL_CANCEL, 
					Statics.MSG_PRINT_OK, 
					Statics.MSG_PRINT_OK,
					mHandler);
		}
	}
}
