package com.sunnada.baseframe.activity.yygj;

import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;

public class NetAccessPermitsActivity extends DgActivity implements OnClickListener
{
	private Button			mBtnCheck;
	private View			mLayBack;
	private Button 			mBtnPrevious;
	private MyCustomButton	mBtnNetWrite;
	private TextView		mTvContent;
	private String			mStr;
	private boolean			mIsCheck		= false;
	private LinearLayout	mLayCheck	= null;
	private Business_0C		mHaoka		= null;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_netaccesspermits);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_netaccesspermits_orange);
		}
		mHaoka = (Business_0C) FrameActivity.mBaseBusiness;
		// 初始化控件
		init();
	}

	// 初始化控件
	public void init()
	{
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		mLayCheck = (LinearLayout) findViewById(R.id.lay_check);
		mLayCheck.setOnClickListener(this);
		
		mBtnNetWrite = (MyCustomButton) findViewById(R.id.net_write);
		mBtnNetWrite.setTextViewText1("签       名");
		mBtnNetWrite.setImageResource(R.drawable.net_write);
		mBtnNetWrite.setOnTouchListener(this);
		mBtnNetWrite.setOnClickListener(this);
		
		mTvContent = (TextView) findViewById(R.id.tv_content);
		//mStr = "中国联通移动业务客户入网服务协议\n根据相关法律条例规定,双方达成协议如下:\n第一条乙方明确费用标准和费用交纳相关规定,按约定为甲方提供网络服务和客户服务\n第二条甲方按照入网要求提交真实资料,乙方对信息资料负有保密义务,甲乙双方须遵循风险控制相关规定\n第三条乙方须遵循号码变更与转让相关规定\n第四条涉及到协议变更/解除以及争议方式的内容,甲乙双方须遵循协议相关规定解决 \n第五条本协议自双方签字盖章且业务开通之日起生效,有效期一年";
		mStr = mHaoka.getNetworkProtocol();
		mTvContent.setText(mStr);

		mBtnCheck = (Button) findViewById(R.id.bt_check);
		mBtnCheck.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		if (ButtonUtil.isFastDoubleClick(v.getId(), 500))
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		switch (v.getId())
		{
			// 点击打钩框或者文字时都相应
			case R.id.lay_check:
			case R.id.bt_check:
				if (mIsCheck == true)
				{
					mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netcheck));
					mIsCheck = false;
				}
				else
				{
					mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netchecked));
					mIsCheck = true;
				}
				break;
			
			// 签名
			case R.id.net_write:
				write();
				break;
				
			// 返回
			case R.id.btn_previous:
			case R.id.layBack:
			DialogUtil.MsgBox("温馨提示", Statics.EXIT_TIP, 
			"确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					exit();
				}
			}, "取消", null , null);
				break;
			default:
				break; 
		}
	}

	// 退出
	private void exit()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				mHaoka.bll0C16();// 通知服务器释放资源
			}
		}.start();
		Message msg = new Message();
		msg.what = FrameActivity.FINISH;
		FrameActivity.mHandler.sendMessage(msg);
	}
	
	// 签名
	private void write()
	{
		final Bundle bundle = this.getIntent().getBundleExtra("_data");
		final Message msg = new Message();
		if (Statics.IS_DEBUG)
		{
			bundle.putString("className", SignatureActivity.class.getName());
			msg.what = FrameActivity.DIRECTION_NEXT;
			msg.setData(bundle);
			FrameActivity.mHandler.sendMessage(msg);
			return;
		}
		if (mIsCheck == true)
		{
			DialogUtil.showProgress("正在获取费用, 请稍候...");
			System.out.println("协议界面的haoka:" + mHaoka);
			new Thread()
			{
				@Override
				public void run()
				{
					boolean ret;
					try
					{
						ret = mHaoka.bll0C0B();
						if (!ret)
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									DialogUtil.MsgBox("温馨提示", mHaoka.getLastknownError(), 
									"确定", new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											exit();
										}
									}, "", null, new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											exit();
										}
									});
								}
							});
							return;
						}
						else
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									bundle.putString("className", SignatureActivity.class.getName());
									msg.what = FrameActivity.DIRECTION_NEXT;
									msg.setData(bundle);
									FrameActivity.mHandler.sendMessage(msg);
								}
							});
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox("温馨提示", mHaoka.getLastknownError(), 
								"确定", new OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										exit();
									}
								}, "", null, null);
							}
						});
					}
				};
			}.start();
		}
		else
		{
			DialogUtil.MsgBox("温馨提示", "同意入网协议后方可继续开户！");
		}
	}

	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			DialogUtil.MsgBox("温馨提示", Statics.EXIT_TIP, 
			"确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					exit();
				}
			}, "取消", null , null);;
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netchecked));
		mIsCheck = true;
	}

	/*
	// 单击按钮监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.net_write:
				net_write.onTouch(event);
				break;

			default:
				break;
		}
		return false;
	}
	*/
}
