package com.sunnada.baseframe.activity.xhrw;

import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.PickNetBusiness;

import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccessPermitsLayout extends Busi_frame implements OnClickListener, OnTouchListener
{
	private   Button				bt_check;
	private   MyCustomButton	    mBtnNetWrite;
	private   TextView			    tv_content;
	private   String				str;
	private   boolean				ischeck		     = false;
	private   PickNetBusiness       mPickNetBusiness = null;//选号入网业务类
	private   Button                mBtnPrevious     = null;//回退按钮
	private   LinearLayout          mLaytBack;              //回退LinearLayout
	
	public AccessPermitsLayout(NumberSelectActivity2 numberSelect)
	{
		super(numberSelect);
	}

	protected void onCreate(Bundle savedInstanceState)
	{
		mNumberSelect.setContentView(R.layout.lay_netaccesspermits);
		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;
		init();
	}

	public void init()
	{
		bt_check = (Button) mNumberSelect.findViewById(R.id.bt_check);
		bt_check.setOnClickListener(this);
		
		tv_content = (TextView) mNumberSelect.findViewById(R.id.tv_content);
		str = "中国联通移动业务客户入网服务协议\n根据相关法律条例规定,双方达成协议如下:\n第一条乙方明确费用标准和费用交纳相关规定,按约定为甲方提供网络服务和客户服务\n第二条甲方按照入网要求提交真实资料,乙方对信息资料负有保密义务,甲乙双方须遵循风险控制相关规定\n第三条乙方须遵循号码变更与转让相关规定\n第四条涉及到协议变更/解除以及争议方式的内容,甲乙双方须遵循协议相关规定解决 \n第五条本协议自双方签字盖章且业务开通之日起生效,有效期一年";
		tv_content.setText(str);

		//回退LinearLayout
		mLaytBack = (LinearLayout) mNumberSelect.findViewById(R.id.layBack);
		mLaytBack.setOnClickListener(this);
		
		mBtnPrevious = (Button) mNumberSelect.findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		
		mBtnNetWrite = (MyCustomButton) mNumberSelect.findViewById(R.id.net_write);
		mBtnNetWrite.setTextViewText1("签       名");
		mBtnNetWrite.setImageResource(R.drawable.net_write);
		mBtnNetWrite.setOnTouchListener(this);
		mBtnNetWrite.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 500)) 
		{
			return;
		}
				
		final Message msg = Message.obtain();

		switch (v.getId())
		{
			case R.id.bt_check:
				if (ischeck == true)
				{
					bt_check.setBackgroundDrawable(mNumberSelect.getResources().getDrawable(R.drawable.bt_netcheck));
					ischeck = false;
				}
				else
				{
					bt_check.setBackgroundDrawable(mNumberSelect.getResources().getDrawable(R.drawable.bt_netchecked));
					ischeck = true;
				}
				break;
			case R.id.net_write:
				if(Statics.IS_DEBUG)
				{
					//bundle.putString("className", PickNetSignatureActivity.class.getName());
					msg.what = FrameActivity.DIRECTION_NEXT;
					//msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					return;
				}
				
				if (ischeck == true)
				{
					DialogUtil.showProgress("正在获取费用,请稍候");
					new Thread()
					{
						@Override
						public void run()
						{
							int ret = 0;
							try
							{
								ret = mPickNetBusiness.bll0D0B();

								if (ret != ReturnEnum.SUCCESS)
								{
									mNumberSelect.runOnUiThread(new Runnable()
									{
										public void run()
										{
											/*
											back(new ICallBack() 
											{
												@Override
												public void back() 
												{
													Message msg = new Message();
													msg.what = FrameActivity.FINISH;
													FrameActivity.mHandler.sendMessage(msg);
												}
												
												@Override
												public void cancel() 
												{
												}

												@Override
												public void dismiss() 
												{
												}
											}, null, "获取费用失败,将退出当前流程.失败原因:[" + mPickNetBusiness.getLastknownError() + "]",
											"确定", null, true, false, false);
											*/
										}
									});
									mNumberSelect.runOnUiThread(new Runnable()
									{
										public void run()
										{
											DialogUtil.closeProgress();
										}
									});
									return;
								}
								else
								{
									mNumberSelect.runOnUiThread(new Runnable()
									{
										public void run()
										{
											DialogUtil.closeProgress();
										}
									});
									//bundle.putString("className", PickNetSignatureActivity.class.getName());
									msg.what = FrameActivity.DIRECTION_NEXT;
									//msg.setData(bundle);
									FrameActivity.mHandler.sendMessage(msg);
								}
							}
							catch (Exception e)
							{
								e.printStackTrace();
								mNumberSelect.runOnUiThread(new Runnable()
								{
									public void run()
									{
										DialogUtil.closeProgress();
										/*
										back(new ICallBack() 
										{
											@Override
											public void back() 
											{
												Message msg = Message.obtain();
												msg.what = FrameActivity.FINISH;
												FrameActivity.mHandler.sendMessage(msg);
											}
											
											@Override
											public void cancel() 
											{
											}

											@Override
											public void dismiss() 
											{
											}
										}, null, "获取费用失败,将退出当前流程.失败原因:[" + mPickNetBusiness.getLastknownError() + "]",
										"确定", null, true, false, false);
										*/
									}
								});
							}
						};
					}.start();
				}
				else
				{
					//back(null, null, "同意入网协议后方可继续开户！", "确定", null, false, false, false);
				}
				break;
			case R.id.lay_check:
				break;
				
			case R.id.btn_previous:
				//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			    break;
			
			case R.id.layBack:
				//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
				break;
			default:
				break;
		}
	}

	private void backToPrevious()
	{
		Message msg = Message.obtain();
		//bundle.putString("className", PickNetWorkBusinessIdinfoActivity.class.getName());
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		//msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume()
	{
		bt_check.setBackgroundDrawable(mNumberSelect.getResources().getDrawable(R.drawable.bt_netchecked));
		ischeck = true;
	}

	@Override
	public void go() 
	{
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		return false;
	}
}
