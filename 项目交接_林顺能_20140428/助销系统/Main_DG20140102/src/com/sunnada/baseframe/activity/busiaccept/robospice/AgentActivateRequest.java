package com.sunnada.baseframe.activity.busiaccept.robospice;

import java.util.ArrayList;
import java.util.List;

import com.sunnada.baseframe.activity.busiaccept.base.DES;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;

public class AgentActivateRequest extends BaseRequest 
{
	private String 			mCommunicaID;
	private String 			mAgentId;
	private String 			mIdentifyId;
	private String			mAgentPswd;

	public AgentActivateRequest(String communicaID, String agentId, String identifyId, String agentPswd) 
	{
		super();
		
		this.mCommunicaID 	= communicaID;
		this.mAgentId 		= agentId;
		this.mIdentifyId 	= identifyId;
		this.mAgentPswd		= agentPswd;
	}

	@Override
	protected String getMethod() 
	{
		return "agentActivate";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "agentActivateResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	mCommunicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentId, mEncryptKey)));
		params.add(new Param(IDENTIFY_ID, 		DES.encryptDES(mIdentifyId, mEncryptKey)));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		params.add(new Param(AGENT_PASSWORD, 	DES.encryptDES(mAgentPswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
