package com.sunnada.baseframe.activity.selfservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

// 网点信息查询
public class BusiShopInfo extends BaseActivity implements OnClickListener 
{
	private LinearLayout		mLinearBacktoSelfService;			// 返回到自服务
	// private LinearLayout        mLinearLeft;
	
	private TextView			mTextIpAddr;						// IP地址
	private TextView			mTextPort;							// 端口
	private TextView			mTextShopName;						// 网点名称
	private TextView			mTextShopAddr;						// 网点地址
	private TextView			mTextAgentName;						// 代理商姓名
	private TextView			mTextAgentTel;						// 代理商电话
	private String[]			mStrShopName	= new String[1];	// 网点地址
	private String[]			mStrShopAddr	= new String[1];	// 网点地址
	private String[]			mStrAgentName	= new String[1];	// 网点联系人
	private String[]			mStrAgentTel	= new String[1];	// 联系电话
	
	private ListView			mListAgents;						// 营业员列表
	private List<Map<String, String>> mListData	= null;
	private LinearLayout        mLayStaffListTitle;
	// private TextView            mTvName;
	// private TextView            mTvTel;
	// private TextView            mTvNumber;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_shopinfo_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_shopinfo);
		}
		
		// 初始化控件
		initViews();
		// 初始化显示
		initShow();
		// 初始化网点信息
		initAgentList();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 返回自服务
		mLinearBacktoSelfService = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		mLinearBacktoSelfService.setOnClickListener(this);
		
		// 左边布局
		// mLinearLeft     = (LinearLayout) findViewById(R.id.linear_left);
		
		mTextIpAddr 	= (TextView) findViewById(R.id.tv_ip_address);
		mTextPort		= (TextView) findViewById(R.id.tv_port);
		mTextShopName  	= (TextView) findViewById(R.id.tv_shop_name);
		mTextShopAddr 	= (TextView) findViewById(R.id.tv_shop_addr);
		mTextAgentName	= (TextView) findViewById(R.id.tv_agent_name);
		mTextAgentTel	= (TextView) findViewById(R.id.tv_agent_tel);
		
		mListAgents		= (ListView) findViewById(R.id.list_agent);
		mLayStaffListTitle = (LinearLayout) findViewById(R.id.lay_staff_list_title);
		// mTvName         = (TextView) findViewById(R.id.tv_name);
		// mTvTel          = (TextView) findViewById(R.id.tv_tel);
		// mTvNumber       = (TextView) findViewById(R.id.tv_number);
	}
	
	private void initShow()
	{
		mTextShopName.setText("");
		mTextShopAddr.setText("");
		mTextAgentName.setText("");
		mTextAgentTel.setText("");
		mLayStaffListTitle.setVisibility(View.GONE);
		
		/*
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			
			ImageView mIvBusiShopInfo = (ImageView) findViewById(R.id.iv_busi_shopinfo);
			mIvBusiShopInfo.setImageDrawable(getResources().getDrawable(R.drawable.icon_busi_shopinfo_orange));
			
			ImageView ivReturn = (ImageView) findViewById(R.id.btn_return);
			ivReturn.setImageDrawable(getResources().getDrawable(R.drawable.icon_return_orange));
			
			mLayStaffListTitle.setBackgroundColor(ARGBUtil.getArgb(2));
		}
		*/
	}
	
	// 初始化营业员列表
	private void initAgentList() 
	{
		// 设置IP地址和端口
		mTextIpAddr.setText(socketService.getIp());
		mTextPort.setText(String.valueOf(socketService.getPort()));
		
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取网点信息");
				Business_00 business = new Business_00(BusiShopInfo.this, equipmentService, socketService, dataBaseService);
				// 获取网点信息
				mListData = business.bll000F(App.getUserNo(), mStrShopName, mStrShopAddr, mStrAgentName, mStrAgentTel);
				DialogUtil.closeProgress();
				if(mListData == null) 
				{
					DialogUtil.MsgBox("温馨提示", business.getLastknownError());
					return;
				}
				// 刷新网点信息
				runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						// 刷新基本信息
						mTextShopName.setText(mStrShopName[0]);
						mTextShopAddr.setText(mStrShopAddr[0]);
						mTextAgentName.setText(mStrAgentName[0]);
						mTextAgentTel.setText(mStrAgentTel[0]);
						mLayStaffListTitle.setVisibility(View.VISIBLE);
						
						List<Map<String,String>> tempList = new ArrayList<Map<String,String>>();
						int loginUserPos = -1;
						
						// 取得登录用户在当前mListData列表中的位置
						for(int i=0;i<mListData.size();i++)
						{
							if(!StringUtil.isEmptyOrNull(App.getUserNo()))
							{
								if(App.getUserNo().equals(mListData.get(i).get(Statics.KEY_USER_NO)))
								{
									loginUserPos = i;
									break;
								}
							}
						}
						
						if(loginUserPos != -1)
						{
							tempList.add(mListData.get(loginUserPos));
							for(int i = 0;i < mListData.size();i++)
							{
								if(i != loginUserPos)
								{
									tempList.add(mListData.get(i));
								}
							}
						}
						else
						{
							tempList = mListData;
						}
						// 刷新营业员列表
						AgentInfoAdapter adapter = new AgentInfoAdapter(BusiShopInfo.this, tempList);
						mListAgents.setAdapter(adapter);
						// DimensionUtil.setListViewHeightBasedOnChildren(mListAgents);
					}
				});
			}
		}.start();
	}
	
	// 营业员信息
	private class AgentInfoAdapter extends BaseAdapter 
	{
		private List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
		private Context mContext;
		private LayoutInflater inflater;
		
		public AgentInfoAdapter(Context context, List<Map<String, String>> data) 
		{
			mContext = context;
			inflater = LayoutInflater.from(mContext);
			this.dataList = data;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			ViewHolder viewHolder = new ViewHolder();
			if(convertView == null) 
			{
				 convertView = inflater.inflate(R.layout.lay_selfservice_shopinfo_listitem, null);
				
				TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
				TextView tv_tel = (TextView)convertView.findViewById(R.id.tv_tel);
				TextView tv_no = (TextView)convertView.findViewById(R.id.tv_no);
				
				viewHolder.setTv_name(tv_name);
				viewHolder.setTv_tel(tv_tel);
				viewHolder.setTv_no(tv_no);
				convertView.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder) convertView.getTag();
			}
			
			if(position == 0)
			{
				if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					viewHolder.getTv_name().setTextColor(ARGBUtil.getArgb(8));
					viewHolder.getTv_tel().setTextColor(ARGBUtil.getArgb(8));
					viewHolder.getTv_no().setTextColor(ARGBUtil.getArgb(8));
					
					convertView.setBackgroundColor(ARGBUtil.getArgb(7));
				}
				else
				{
					convertView.setBackgroundColor(Color.argb(0xff, 0xae, 0xdf, 0xff));
					viewHolder.getTv_name().setTextColor(Color.argb(0xff, 0x24, 0x7A, 0xAB));
					viewHolder.getTv_tel().setTextColor(Color.argb(0xff, 0x24, 0x7A, 0xAB));
					viewHolder.getTv_no().setTextColor(Color.argb(0xff, 0x24, 0x7A, 0xAB));
				}
			}							
			else
			{
				convertView.setBackgroundColor(Color.argb(0xff, 0xee, 0xf2, 0xf5));
				viewHolder.getTv_name().setTextColor(Color.argb(0xff, 0x6e, 0x88, 0x99));
				viewHolder.getTv_tel().setTextColor(Color.argb(0xff, 0x6e, 0x88, 0x99));
				viewHolder.getTv_no().setTextColor(Color.argb(0xff, 0x6e, 0x88, 0x99));
			}
			viewHolder.getTv_name().setText(dataList.get(position).get(Statics.KEY_USER_NAME));
			viewHolder.getTv_tel().setText(dataList.get(position).get(Statics.KEY_USER_TEL));
			viewHolder.getTv_no().setText(dataList.get(position).get(Statics.KEY_USER_NO));
			return convertView;
		}

		@Override
		public int getCount() 
		{
			return dataList.size();
		}

		@Override
		public Object getItem(int position) 
		{
			return dataList.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}
	}
	
	class ViewHolder
	{
		TextView tv_name;
		TextView tv_tel;
		TextView tv_no;
		
		public TextView getTv_name() 
		{
			return tv_name;
		}
		
		public void setTv_name(TextView tv_name)
		{
			this.tv_name = tv_name;
		}
		
		public TextView getTv_tel() 
		{
			return tv_tel;
		}
		
		public void setTv_tel(TextView tv_tel) 
		{
			this.tv_tel = tv_tel;
		}
		
		public TextView getTv_no() 
		{
			return tv_no;
		}
		
		public void setTv_no(TextView tv_no) 
		{
			this.tv_no = tv_no;
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}
	
	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
					
		switch(v.getId()) 
		{
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
