package com.sunnada.baseframe.activity.selfservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.BusiCheckDetailDialog;
import com.sunnada.baseframe.dialog.DateTimePickerDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.DimensionUtil;
import com.sunnada.baseframe.util.StringUtil;

public class BusiCheck extends BaseActivity implements OnClickListener
{
	private static final String				TITLE_STR			= "对账业务办理提示";
	
	// 时间设定布局(左边)
	private LinearLayout                    mLinearLeft;                    // 左边布局
	private LinearLayout					mLinearDateSet;					// 时间设定布局
	private TextView						mTextDateSet;
	private TextView                        mTextLeftStartDate;             // 起始时间
	private TextView                        mTextLeftEndDate;               // 结束时间
	private LinearLayout					mBacktoSelfService;
	
	private ImageView                       mIvBusiCheck;
	// 用户输入布局
	private LinearLayout					mLinearUserInput;				// 用户输入布局
	// 对账类型
	private TextView                        mTvCheckType;
	private ImageView                       mIvCheckType;
	private ListView                        mLvCheckType;
	private String[]                        mCheckTypeStrArr    = {"日对账","月对账","历史对账"};
	// 查询时间
	private LinearLayout                    mLaySearchTime;
	private EditText                        mEtSearchTime;
	// 开始时间 
	private LinearLayout                    mLayBeiginTime;
	private EditText                        mEtBeiginTime;
	// 结束时间
	private LinearLayout                    mLayEndTime;
	private EditText                        mEtEndTime;
	// 对账范围
	private TextView						mTvCheckScope;
	private ImageView						mIvCheckScope;
	private ListView                        mLvCheckScope;
	private String[]                        mCheckScopeStrArr   = {"工号","营业厅"};

	private EditText						mEditPassword;					// 交易密码输入框
	private MyCustomButton				    mBtnQuery;						// 查询
	
	// 对账列表(第二页)
	private LinearLayout                    mLinearDZTitle;
	private TextView                        mTvCode;
	private TextView                        mTvNumber;
	private TextView                        mTvMoney;
	private TextView                        mTvDetail;
	
	private int 							mBillType           = 0x01;		// 当前交易类型
	private static final int 				BUSI_CHECK_DAY 		= 0x01;
	private static final int 				BUSI_CHECK_MONTH 	= 0x02;
	private static final int 				BUSI_CHECK_DATE 	= 0x03;
	
	private Calendar						mCalendar			= Calendar.getInstance();
	private Business_00						mBusiness00;
	private String 							mStrBeginTime 		= "";
	private String 							mStrEndTime			= "";
	private String							mStrPswd			= null;
	
	// 对账详细信息布局
	private LinearLayout					mLinearCheckDetail;				// 对账详细信息布局
	private ListView						mListCheck;						// 对账ListView
	private List<Map<String, Object>>		mListCheckData;
	private int[]							mCheckTotal			= new int[1];
	private int[]							mCheckCur			= new int[1];
	
	private String							mStrCheckName;					// 对账分类名称
	private String							mStrCheckCode;					// 对账分类编码
	private List<Map<String, String>>		mListCheckDetail    = new ArrayList<Map<String,String>>(); // 对账详情
	
	private boolean                         mIsFirstIn          = true;
	private ArrayAdapter<String>            mAdapter            = null;
	
	private View                            mOldTypeItemView    = null;
	private View                            mOldScopeItemView   = null;
	
	private boolean                         mIsTypeListHide     = true;     // 对账类型ListView是否已隐藏
	private boolean                         mIsScopeListHide    = true;     // 对账范围ListView是否已隐藏
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_busi_check_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_busi_check);
		}
		
		mBusiness00 = new Business_00(this, equipmentService, socketService, dataBaseService);
		// 初始化布局
		initViews();
		// 显示用户输入布局
		showUserInputView(true);
		// 注册监听
		setListener();
		// 初始化数据
		// initData();
	}
	
	// 初始化布局
	private void initViews() 
	{
		// 初始化时间设定布局
		initDateSetView();
		// 初始化对账详细信息布局
		initCheckView();
		// 显示当前日期
		showCurDate();
		// 初始化第二步
		initStepTwo();
	}
	
	// 初始化时间设定布局
	private void initDateSetView() 
	{
		// 左边布局UI初始化
		mLinearLeft = (LinearLayout) findViewById(R.id.linear_left);
		mLinearDateSet 	= (LinearLayout) findViewById(R.id.linear_set_date);
		mTextDateSet	= (TextView) findViewById(R.id.tv_date_set);
		mTextLeftStartDate = (TextView)findViewById(R.id.tv_left_start_date);
		mTextLeftEndDate = (TextView)findViewById(R.id.tv_left_end_date);
		
		// 回退按钮
		mBacktoSelfService = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		
		mIvBusiCheck = (ImageView) this.findViewById(R.id.iv_busi_check);
		// 右边布局UI初始化
		mLinearUserInput = (LinearLayout) findViewById(R.id.linear_user_input);
		// 对账类型
		mTvCheckType = (TextView)findViewById(R.id.tv_checktype);
		mIvCheckType = (ImageView)findViewById(R.id.iv_checktype);
		mLvCheckType = (ListView)findViewById(R.id.lv_checktype);
		mLvCheckType.setVisibility(View.GONE);
		
		// 收搜时间
		mLaySearchTime = (LinearLayout)findViewById(R.id.linear_search_time);
		mEtSearchTime = (EditText)findViewById(R.id.et_search_time);
		
		// 开始时间
		mLayBeiginTime = (LinearLayout)findViewById(R.id.linear_begin_time);
		mEtBeiginTime = (EditText)findViewById(R.id.et_begin_time);
		// 结束时间
		mLayEndTime = (LinearLayout)findViewById(R.id.linear_end_time);
		mEtEndTime = (EditText)findViewById(R.id.et_end_time);
		// 对账范围
		mTvCheckScope = (TextView)findViewById(R.id.tv_check_scope);
		mIvCheckScope = (ImageView)findViewById(R.id.iv_check_scope);
		mLvCheckScope = (ListView)findViewById(R.id.lv_check_scope);
		mLvCheckScope.setVisibility(View.GONE);
		
		mEditPassword = (EditText) findViewById(R.id.et_password);
		
		// 查询按钮
		mBtnQuery = (MyCustomButton) findViewById(R.id.btn_query);
		mBtnQuery.setTextViewText1("查询");
		mBtnQuery.setOnTouchListener(this);
		
		//mViewType = (View)this.findViewById(R.id.view_type);
		//mViewScope = (View)this.findViewById(R.id.view_scope);
		
		//mViewType.setVisibility(View.GONE);
		//mViewScope.setVisibility(View.GONE);
		// 初始化对账类型
		//initCheckType();
		// 初始化对账范围
		//initCheckScope();
		// 初始化时间选择框
		initTimeInput();
 	}
	
	// 初始化第二步
	public void initStepTwo()
	{
		mLinearDZTitle = (LinearLayout) this.findViewById(R.id.linear_dz_title);
		mTvCode = (TextView) this.findViewById(R.id.tv_code);
		mTvNumber = (TextView) this.findViewById(R.id.tv_number);
		mTvMoney = (TextView) this.findViewById(R.id.tv_money);
		mTvDetail = (TextView) this.findViewById(R.id.tv_detail);
	}
	
	// 初始化对账类型
	private void initCheckType() 
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mAdapter = new ArrayAdapter<String>(
					BusiCheck.this,
					R.layout.lay_dz_typelist_item_orange, R.id.tv_text,mCheckTypeStrArr);
			mLvCheckType.setAdapter(mAdapter);
		}
		else
		{
			mAdapter = new ArrayAdapter<String>(
					BusiCheck.this,
					R.layout.lay_dz_typelist_item, R.id.tv_text,mCheckTypeStrArr);
			mLvCheckType.setAdapter(mAdapter);
		}
	}
	
	// 初始化对账范围
	private void initCheckScope() 
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mAdapter = new ArrayAdapter<String>(
					BusiCheck.this,
					R.layout.lay_dz_scopelist_item_orange, R.id.tv_text,mCheckScopeStrArr);
			mLvCheckScope.setAdapter(mAdapter);
		}
		else
		{
			mAdapter = new ArrayAdapter<String>(
					BusiCheck.this,
					R.layout.lay_dz_scopelist_item, R.id.tv_text,mCheckScopeStrArr);
			mLvCheckScope.setAdapter(mAdapter);
		}
	}
	
	public void setListener()
	{
		// 回退按钮
		mBacktoSelfService.setOnClickListener(this);
		// 重置回初始界面
		mLinearDateSet.setOnClickListener(this);
		// 查询按钮
		mBtnQuery.setOnClickListener(this);
		
		// 对账类型ImageView
		mIvCheckType.setOnClickListener(this);
		// 对账范围ImageView
		mIvCheckScope.setOnClickListener(this);
		
		mLvCheckType.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) 
			{
				// 控制LvCheckType是否显示的开关变量
				mIsTypeListHide = true;
				mEditPassword.setEnabled(true);
				mEditPassword.setText("");
				//mViewType.setVisibility(View.GONE);
				if(mOldTypeItemView == null)
				{
					mOldTypeItemView = view;
				}
				else
				{
					mOldTypeItemView.setBackgroundColor(Color.parseColor("#E8F9FF"));
					mOldTypeItemView = view;
				}
				
				mTvCheckType.setText(mCheckTypeStrArr[position]);
				mLvCheckType.setVisibility(View.GONE);
				switch(position)
				{
				case 0:
					mBillType = BUSI_CHECK_DAY;
					// 左边布局设定
					mTextDateSet.setText("设定日期");
					// 开始时间 
					mTextLeftStartDate.setVisibility(View.VISIBLE);
					mTextLeftStartDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mCalendar.getTime()));
					// 结束时间
					mTextLeftEndDate.setVisibility(View.GONE);
					
					// 显示搜索时间布局
					mLaySearchTime.setVisibility(View.VISIBLE);
					// 隐藏开始时间布局
					mLayBeiginTime.setVisibility(View.GONE);
					// 隐藏结束时间布局
					mLayEndTime.setVisibility(View.GONE);
					// 设置显示日期的长度
					mEtSearchTime.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
					mEtSearchTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(mCalendar.getTime()));
					mEtSearchTime.setSelection(10);
					break;
				
				case 1:
					mBillType = BUSI_CHECK_MONTH;
					// 左边布局设定
					mTextDateSet.setText("设定月份");
					// 开始时间 
					mTextLeftStartDate.setVisibility(View.VISIBLE);
					mTextLeftStartDate.setText(new SimpleDateFormat("yyyy-MM").format(mCalendar.getTime()));
					// 结束时间
					mTextLeftEndDate.setVisibility(View.GONE);
					
					// 显示搜索时间布局
					mLaySearchTime.setVisibility(View.VISIBLE);
					// 隐藏开始时间布局
					mLayBeiginTime.setVisibility(View.GONE);
					// 隐藏结束时间布局
					mLayEndTime.setVisibility(View.GONE);
					// 设置显示月份的长度
					mEtSearchTime.setFilters(new InputFilter[] { new InputFilter.LengthFilter(7) });
					mEtSearchTime.setText(new SimpleDateFormat("yyyy-MM").format(mCalendar.getTime()));
					mEtSearchTime.setSelection(7);
					break;
				
				case 2:
					mBillType = BUSI_CHECK_DATE;
					
					String date = new SimpleDateFormat("yyyy-MM").format(mCalendar.getTime());
					String[] sdate = date.split("-");
					int year = Integer.parseInt(sdate[0]);
					int month = Integer.parseInt(sdate[1]);
					sdate = getYearAndMoanth(year, month);
					
					// 左边布局设定
					mTextDateSet.setText("设定日期区间");
					// 开始时间 
					mTextLeftStartDate.setVisibility(View.VISIBLE);
					mTextLeftStartDate.setText(sdate[0] + "-" + sdate[1]  +"-"+ "01");
					// 结束时间
					mTextLeftEndDate.setVisibility(View.VISIBLE);
					mTextLeftEndDate.setText(sdate[0] + "-" + sdate[1]  +"-"+ getDay(year, month));
					
					// 显示搜索时间布局
					mLaySearchTime.setVisibility(View.GONE);
					// 隐藏开始时间布局
					mLayBeiginTime.setVisibility(View.VISIBLE);
					// 隐藏结束时间布局
					mLayEndTime.setVisibility(View.VISIBLE);
					
					mEtBeiginTime.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
					mEtBeiginTime.setText(sdate[0] + "-" + sdate[1]  +"-"+ "01"); 
					mEtBeiginTime.setSelection(10);
					
					mEtEndTime.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
					mEtEndTime.setText(sdate[0] + "-" + sdate[1]  +"-"+ getDay(year, month));
					mEtEndTime.setSelection(10);
					break;
				}
			}
		});
		
		mLvCheckScope.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) 
			{
				mIsScopeListHide = true;
				//mViewScope.setVisibility(View.GONE);
				mEditPassword.setEnabled(true);
				view.setBackgroundColor(Color.parseColor("#91E2FF"));
				if(mOldScopeItemView == null)
				{
					mOldScopeItemView = view;
				}
				else
				{
					mOldScopeItemView.setBackgroundColor(Color.parseColor("#E8F9FF"));
					mOldScopeItemView = view;
				}
				
				mTvCheckScope.setText(mCheckScopeStrArr[position]);
				mLvCheckScope.setVisibility(View.GONE);
				
				if(position == 0)
				{
					mBusiness00.mCheckScope = 0x01;
				}
				else if(position == 1)
				{
					mBusiness00.mCheckScope = 0x00;
				}
			}
		});
		
	}
	
	// 根据年份，获取月份的天数
	private int getDay(int year, int month)
	{
		switch (month - 1)
		{
			case 0:
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
			case 2:
				 if((year%4==0&&year%100!=0)||year%400==0) 
				 {
					 return 29;
				 }
				 else
				 {
					 return 28;
				 }
			default:
				break;
		}
		return -1;
	}

	// 获取年份和月份
	private String[] getYearAndMoanth(int year, int month)
	{
		String[] ss = new String[2];
		if(month == 1)
		{ 
			year = year - 1;
			month = 12;
			ss[0] = year + "";
			ss[1] = month + "";
		}
		else
		{
			month = month - 1;
			ss[0] = year + "";
			if(month < 10)
			{
				ss[1] = "0"+month;
			}
			else
			{
				ss[1] = month + "";
			}
		}
		return ss;
	}
	
	// 初始化时间选择框
	private void initTimeInput() 
	{
		// 查询时间
		mEtSearchTime.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{				
				if (hasFocus == true) 
				{
					mEditPassword.requestFocus();
					if(mIsFirstIn)
					{
						mIsFirstIn = false;
					}
					else
					{
						// 输入日期
						if(mBillType == BUSI_CHECK_MONTH)
						{
							showDateDialog(mEtSearchTime, 1);
						}
						else
						{
							showDateDialog(mEtSearchTime, 0);
						}
					}
				}
			}
		});
		// 开始时间
		mEtBeiginTime.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if (hasFocus == true) 
				{
					showDateDialog(mEtBeiginTime, 2);
					mEditPassword.requestFocus();
				}
			}
		});
		// 结束时间
		mEtEndTime.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if (hasFocus == true) 
				{
					showDateDialog(mEtEndTime, 3);
					mEditPassword.requestFocus();
				}
			}
		});
		
		mEtSearchTime.clearFocus();
		mEtBeiginTime.clearFocus();
		mEtEndTime.clearFocus();
	}
	
	// 显示日期对话框
    protected void showDateDialog(final EditText editText, final int dateType) 
    {
    	if(ButtonUtil.isFastDoubleClick(editText.getId())) 
    	{
    		return;
    	}
    	
    	DateTimePickerDialog date = DateTimePickerDialog.getYearMonthDateDialog(this);
		if(dateType == 1)
		{
			date.setDateVisible(false);
		}
    	
		date.setOnDateTimeSelectListener(new DateTimePickerDialog.OnDateTimeSelectListener()
		{	
			@Override
			public void onDateTimeSelected(Calendar c)
			{
    			if(dateType == 1)
    			{
    				editText.setText(new StringBuilder()
    					.append(c.get(Calendar.YEAR))
    					.append("-")
    					.append((c.get(Calendar.MONTH)+1)<10? "0"+(c.get(Calendar.MONTH)+1):(c.get(Calendar.MONTH)+1)));
    				
    				// 设置设定月份显示日期
    				mTextLeftStartDate.setText(editText.getText().toString());
    			}
    			else
    			{
    				editText.setText(new StringBuilder()
    					.append(c.get(Calendar.YEAR))
    					.append("-")
    					.append((c.get(Calendar.MONTH)+1)<10? "0"+(c.get(Calendar.MONTH)+1):(c.get(Calendar.MONTH)+1))
    					.append("-")
    					.append((c.get(Calendar.DATE)<10)? "0"+c.get(Calendar.DATE):c.get(Calendar.DATE)));
    				
    				if(dateType != 3)
    				{
    					mTextLeftStartDate.setText(editText.getText().toString());
    				}
    				else
    				{
    					mTextLeftEndDate.setText(editText.getText().toString());
    				}
    			}
			}
		});
    	date.show();	
    }
	
	// 初始化对账详细信息布局
	private void initCheckView() 
	{
		mLinearCheckDetail = (LinearLayout) findViewById(R.id.linear_check_detail);
		mListCheck = (ListView) findViewById(R.id.check_list);
	}
	
	// 显示布局
	private void showUserInputView(boolean isShow)
	{
		if(isShow) 
		{
			mLinearUserInput.setVisibility(View.VISIBLE);
			mLaySearchTime.setVisibility(View.VISIBLE);
			mLayBeiginTime.setVisibility(View.GONE);
			mLayEndTime.setVisibility(View.GONE);
			
			// 左边布局设定
			mTextDateSet.setText("设定日期");
			// 显示当前日期
			showCurDate();
			mTextLeftEndDate.setVisibility(View.GONE);
			mLinearCheckDetail.setVisibility(View.GONE);
			
			// 初始化对账类型信息
			mTvCheckType.setText(mCheckTypeStrArr[0]);
			// 当前交易类型
			mBillType = BUSI_CHECK_DAY;	
			
			// 初始化对账范围信息
			mTvCheckScope.setText(mCheckScopeStrArr[0]);
			mBusiness00.mCheckScope = 0x01;
			
			mEditPassword.setText("");
		}
		else
		{
			mLinearUserInput.setVisibility(View.GONE);
			mLinearCheckDetail.setVisibility(View.VISIBLE);
		}
	}
	
	// 显示当前日期
	private void showCurDate() 
	{
		Calendar calendar = Calendar.getInstance();
		String timeStr = String.format("%04d-%02d-%02d", calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.DATE));
		mTextLeftStartDate.setText(timeStr);
		
		// 设置显示日期的长度
		mEtSearchTime.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
		mEtSearchTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(mCalendar.getTime()));
		mEtSearchTime.setSelection(10);
	}
	
	public void initData()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			mTextLeftStartDate.setTextColor(ARGBUtil.getArgb(5));
			mTextLeftEndDate.setTextColor(ARGBUtil.getArgb(5));
			
			mLinearUserInput.setBackgroundColor(ARGBUtil.getArgb(2));
			
			// 对账类型
			mIvCheckType.setBackgroundColor(ARGBUtil.getArgb(2));
			mIvCheckScope.setBackgroundColor(ARGBUtil.getArgb(2));
			
			// 对账范围
			mIvBusiCheck.setImageResource(R.drawable.icon_busi_check_orange);
			
			// 设置第二步显示
			mLinearDZTitle.setBackgroundColor(ARGBUtil.getArgb(7));
			mTvCode.setTextColor(ARGBUtil.getArgb(2));
			mTvNumber.setTextColor(ARGBUtil.getArgb(2));
			mTvMoney.setTextColor(ARGBUtil.getArgb(2));
			mTvDetail.setTextColor(ARGBUtil.getArgb(2));
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		if((v.getId() != R.id.iv_checktype) && (v.getId() != R.id.iv_check_scope))
		{
			// 防止按钮多次触发
			if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
			{
				return;
			}
		}
		
		switch(v.getId())
		{
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			back(new ICallBack() 
			{
				@Override
				public void back() 
				{
					backToPrevious();
				}
				
				@Override
				public void cancel() 
				{
				}
				
				@Override
				public void dismiss() 
				{
				}
			}, null, null, "确定", "取消", true, false, false);
			break;
			
		// 设定日期
		case R.id.linear_set_date:
			showUserInputView(true);
			// 初始化对账类型
			initCheckType();
			// 初始化对账范围
			initCheckScope();
			break;
			
		// 查询
		case R.id.btn_query:
			if(Statics.HAD_PWD)
			{
				handleQuery();
			}
			else
			{
				if(Statics.MODIFY_TYPE == 1)
				{
					DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
					"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
				}
				else if(Statics.MODIFY_TYPE == 2)
				{
					DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
							"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
				}
			}
			break;
		
		case R.id.iv_checktype:
			if(mIsTypeListHide)
			{
				mIsTypeListHide = false;
				mLvCheckType.setVisibility(View.VISIBLE);
				//mViewType.setVisibility(View.VISIBLE);
				mEditPassword.setEnabled(false);
				// 初始化对账类型
				initCheckType();
			}
			else
			{
				mIsTypeListHide = true;
				mLvCheckType.setVisibility(View.GONE);
				mEditPassword.setEnabled(true);
				//mViewType.setVisibility(View.GONE);
			}
			mLvCheckScope.setVisibility(View.GONE);
			break;
		
		case R.id.iv_check_scope:
			if(mIsScopeListHide)
			{
				mIsScopeListHide = false;
				mLvCheckScope.setVisibility(View.VISIBLE);
				//mViewScope.setVisibility(View.VISIBLE);
				mEditPassword.setEnabled(false);
				// 初始化对账范围
				initCheckScope();
			}
			else
			{
				mIsScopeListHide = true;
				mLvCheckScope.setVisibility(View.GONE);
				mEditPassword.setEnabled(true);
				//mViewScope.setVisibility(View.GONE);
			}
			mLvCheckType.setVisibility(View.GONE);
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	// 处理对账请求
	private void handleQuery() 
	{
		String strCurTime 	= new SimpleDateFormat("yyyyMMdd").format(mCalendar.getTime());
		
		mStrPswd = mEditPassword.getText().toString();
		if (StringUtil.isEmptyOrNull(mStrPswd) || mStrPswd.length() != 6) 
		{
			back(null, TITLE_STR, "请输入6位交易密码！", "确定", null, false, false, false);
			return;
		}
		mEditPassword.setText("");
		// 日对账
		if(mBillType == BUSI_CHECK_DAY)
		{
			mStrBeginTime = mEtSearchTime.getText().toString();
			mStrBeginTime = mStrBeginTime.replace("-", "");
			if(mStrBeginTime.compareTo(strCurTime) > 0)
			{
				back(null, TITLE_STR, "查询时间不能大于当前时间！", "确定", null, false, false, false);
				return;
			}
		}
		// 月对账
		else if(mBillType == BUSI_CHECK_MONTH)
		{
			mStrBeginTime = mEtSearchTime.getText().toString();
			mStrBeginTime = mStrBeginTime.replace("-", "");
			if(mStrBeginTime.compareTo(strCurTime) > 0)
			{
				back(null, TITLE_STR, "查询时间不能大于当前时间！", "确定", null, false, false, false);
				return;
			}
		}
		// 历史对账
		else if(mBillType == BUSI_CHECK_DATE)
		{
			mStrBeginTime = mEtBeiginTime.getText().toString();
			mStrBeginTime = mStrBeginTime.replace("-", "");
			mStrEndTime = mEtEndTime.getText().toString();
			mStrEndTime = mStrEndTime.replace("-", "");
			
			if(StringUtil.isEmptyOrNull(mStrBeginTime)) 
			{
				back(null, TITLE_STR, "开始时间不能为空！", "确定", null, false, false, false);
				return;
			}
			if(StringUtil.isEmptyOrNull(mStrEndTime)) 
			{
				back(null, TITLE_STR, "结束时间不能为空！", "确定", null, false, false, false);
				return;
			}
			if(!mStrBeginTime.substring(0, 6).equals(mStrEndTime.substring(0, 6)))
			{
				back(null, TITLE_STR, "查询时间不能跨月！", "确定", null, false, false, false);
				return;
			}				
			if(mStrBeginTime.compareTo(mStrEndTime) > 0)
			{	
				back(null, TITLE_STR, "开始时间不能大于结束时间！", "确定", null, false, false, false);
				return;
			}
			if(mStrBeginTime.compareTo(strCurTime) > 0 || mStrEndTime.compareTo(strCurTime) > 0)
			{
				back(null, TITLE_STR, "查询时间不能大于当前时间！", "确定", null, false, false, false);
				return;
			}
		}
		
		if(mStrBeginTime.length() < 8)
		{
			mStrBeginTime += "00";
		}
		
		DialogUtil.showProgress("正在获取对账详情");
		new Thread() 
		{
			public void run() 
			{
				// 获取对账详情
				mListCheckData = mBusiness00.bll002A(mBillType, mStrBeginTime, mStrEndTime, mStrPswd);
				// 获取对账详情失败
				if(mListCheckData == null)
				{
					DialogUtil.closeProgress();
					return;
				}
				else
				{
					DialogUtil.closeProgress();
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							// 显示对账详情界面
							showUserInputView(false);
							// 
							SimpleAdapter adapter = new CheckDetailAdapter(
									BusiCheck.this, 
									mListCheckData, 
									R.layout.lay_selfservice_busi_check_listitem,
									new String[] {Statics.CHECK_CATEGORY_SEQ, Statics.CHECK_CATEGORY_TITLE, Statics.CHECK_CATEGORY_CONTENT}, 
									new int[]{R.id.tv_check_id, R.id.tv_check_count, R.id.tv_check_money});
							mListCheck.setAdapter(adapter);
							DimensionUtil.setListViewHeightBasedOnChildren(mListCheck);
							
							mCheckTotal[0] = 0x00;
							mCheckCur[0] = 0x01;
						}
					});
				}
			}
		}.start();
	}
	
	// 为了触发按钮需要触发的事件
	private class CheckDetailAdapter extends SimpleAdapter 
	{
		public CheckDetailAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null)
			{
				convertView = super.getView(position, convertView, parent);
			}
			
			Map<String, Object> map = mListCheckData.get(position);
			String strDetail = (String)map.get(Statics.CHECK_CATEGORY_DETAIL);
			final String strCode = (String)map.get(Statics.CHECK_CATEGORY_CODE);
			final String strName = (String)map.get(Statics.CHECK_CATEGORY_TITLE);
			
			if(strDetail.equals("--"))
			{
				convertView.findViewById(R.id.tv_check_no_detail).setVisibility(View.VISIBLE);
				convertView.findViewById(R.id.tv_check_has_detail).setVisibility(View.GONE);
			}
			else
			{
				convertView.findViewById(R.id.tv_check_no_detail).setVisibility(View.GONE);
				convertView.findViewById(R.id.tv_check_has_detail).setVisibility(View.VISIBLE);
				convertView.findViewById(R.id.lay_check_has_detail).setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						// 获取分类详情并显示
						getCateDetail(strCode, strName,1);
					}
				});
			}
			return convertView;
		}
	}
	
	// 获取分类详情并显示
	private void getCateDetail(final String strCode, final String strName,final int way) 
	{
		new Thread() 
		{
			public void run() 
			{
				if(way == 1)
				{
					mCheckTotal[0] = 0x00;
					mCheckCur[0] = 0x01;
				}
			
				mStrCheckCode = strCode;
				mStrCheckName = strName;
				// 
				DialogUtil.showProgress("正在获取分类详情...");
				Log.e("", "分类编码为: " + strCode);
				mListCheckDetail = mBusiness00.bll002A(mBillType, mStrBeginTime, mStrEndTime, mStrPswd, 
						Integer.parseInt(strCode), mCheckTotal, mCheckCur);
			
				if(mListCheckDetail == null)
				{
					DialogUtil.closeProgress();
					return;
				}
			
				// 显示详情
				if(mListCheckDetail.size() != 0) 
				{
					showCateDetail(strCode, strName);
				}
				
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 显示分类详情
	private void showCateDetail(final String strCode, final String strName) 
	{
		// 显示对账详情的界面
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				BusiCheckDetailDialog dialog = new BusiCheckDetailDialog(BusiCheck.this, strName+"业务交易明细", mListCheckDetail, mHandler);
				dialog.show();
			}
		});
	}
	
	// 消息处理器
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch(message.what) 
			{
			// 上一页
			case Statics.MSG_BUSI_CHECK_PREV:
				if((mCheckCur[0]-10) < 1)
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							showCateDetail(mStrCheckCode, mStrCheckName);
						}
						
						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, TITLE_STR, "已经是第一页了！", "确定", null, true, false, false);
					return;
				}
				else
				{
					mCheckCur[0] -= 10;
					// 获取分类详情并显示
					getCateDetail(mStrCheckCode, mStrCheckName,2);
				}
				break;
			
			// 下一页
			case Statics.MSG_BUSI_CHECK_NEXT:
				if((mCheckCur[0]+10) > mCheckTotal[0]) 
				{
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							showCateDetail(mStrCheckCode, mStrCheckName);
						}

						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, TITLE_STR, "已经是最后一页了！", "确定", null, true, false, false);
					return;
				}
				else
				{
					mCheckCur[0] += 10;
					// 获取分类详情并显示
					getCateDetail(mStrCheckCode, mStrCheckName,2);
				}
				break;
			
				// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				ModifyPswdDialog dialog1 = new ModifyPswdDialog(BusiCheck.this, 
				new Business_00(BusiCheck.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog1.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				ResetSrvDialog dialog2 = new ResetSrvDialog(BusiCheck.this, 
				new Business_00(BusiCheck.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog2.show();
				break;
				
			// 交易密码修改成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mEditPassword.setText("");
				break;
			
			default:
				break;
			}
		}
	};
	
	// 回到前面去
	public void backToPrevious()
	{
		BusiCheck.this.finish();
	}
	
	@Override
	public void onInitSuccess() 
	{
	}
	
	@Override
	public void onInitFail() 
	{
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			back(new ICallBack() 
			{
				@Override
				public void back() 
				{
					backToPrevious();
				}
				
				@Override
				public void cancel() 
				{
				}
				
				@Override
				public void dismiss() 
				{
				}
			}, null, null, "确定", "取消", true, false, false);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
