package com.sunnada.baseframe.activity.yygj;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.bean.ContractData;
import com.sunnada.baseframe.bean.IdentifyMsg;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.identify.IdentifyScan;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.FileOpr;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.PictureShowUtils;
import com.sunnada.baseframe.util.StringUtil;

public class BusinessIdinfoActivity extends DgActivity implements OnClickListener, ViewGetter
{
	public static int			ICCID_LENGTH		= 19;
	private View				mLayBack;					// 返回
	private Button 				mBtnPrevious;				// 返回
	private View				mLayIdType;					// 证件类型
	private TextView			mTvIdType;					// 证件类型
	private View				mLayCover;					// 覆盖屏幕
	private ListView			mLvIdType;					// 证件类型
	
	private EditText			mEtIdNum			= null;
	private EditText			mEtName				= null;
	private EditText			mEtDate				= null;
	private EditText			mEtAddress			= null;
	private EditText			mEtIccid			= null;

	private TextView			mTvPhoneInfo		= null;
	private TextView			mTvPackageInfo		= null;
	private TextView			mTvNumInfo			= null;

	private ImageView			mIvIdentifyScan1	= null; // 身份证扫描正面
	private ImageView			mIvIdentifyScan2	= null; // 身份证扫描反面

	private String				sPicFolder			= "";	// 暂存图片文件的文件夹名
	private String				filePath1			= null;
	private String				filePath2			= null;

	//private Button			mBtnStandar			= null;
	//private Button			mBtnAll				= null;
	//private Button			mBtnHalf			= null;
	//private Button			mBtnWriredCrad		= null;
	private Button				mBtnReadIccid		= null; 
	private MyCustomButton		mBtnNext;
	private MyCustomButton		mBtnPre;
	private ButtonGroupUtil		util;
	private ButtonGroupUtil		sexUtil;
	private Business_0C			haoka;
	// MyPorgressDialog 		dlg_progress = null;
	// private CommonDialog 	dialog = null;
	private String				mIdType				 = "02"; 	// 证件类型
	private RadioButton			mRbKongka;				 		// 开卡类型--成卡
	private RadioButton			mRbBaika;						// 开卡类型--白卡
	private TextView			mTvKongka;
	private TextView			mTvBaika;
	
	private ImageView			mIvTipName;
	private ImageView			mIvTipDate;
	private ImageView			mIvTipIdNum;
	private ImageView			mIvTipAddress;
	private ImageView			mIvTipIccid;
	private List<Object>		mListIdType;
	
	private final int[][]       mFontColor          = {{0xff,0xff,0xff,0xff,0xff},{0xff,0x6f,0xae,0xc8}};
	private final int[][]       mFontColorOrange    = {{0xff,0xff,0xff,0xff,0xff},{0xff,0xff,0x96,0x00}};
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_business_idinfo);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_business_idinfo_orange);
		}
		// 初始化控件
		initView();
		// 注册监听 
		setListenner();
		// 初始化数据
		initData();
	}

	// 初始化控件
	private void initView()
	{
		// 下一步按钮
		mBtnNext = (MyCustomButton) findViewById(R.id.btn_next);
		mBtnNext.setTextViewText1("确认");
		mBtnNext.setImageResource(R.drawable.btn_custom_check);
		mBtnNext.setOnTouchListener(this);
		mBtnNext.setOnClickListener(this);
		
		mBtnPre = (MyCustomButton) findViewById(R.id.btn_pre);
		mBtnPre.setTextViewText1("返回");
		mBtnPre.setImageResource(R.drawable.btn_back);
		mBtnPre.setOnTouchListener(this);
		mBtnPre.setOnClickListener(this);
		// 证件类型
		mLayIdType = findViewById(R.id.layIdType);
		mTvIdType  = (TextView) findViewById(R.id.tvIdType);
		mLvIdType  = (ListView) findViewById(R.id.listIdType);
		mLayCover  = findViewById(R.id.lay_cover);
		// 开卡类型
		mRbKongka = (RadioButton) findViewById(R.id.rbKongka);
		mRbBaika = (RadioButton) findViewById(R.id.rbBaika);
		mTvKongka = (TextView) findViewById(R.id.tvKongka);
		mTvBaika = (TextView) findViewById(R.id.tvbaika);
		
		mLayBack = findViewById(R.id.layBack);
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mTvPhoneInfo = (TextView) findViewById(R.id.tv_phone_info);
		mTvPackageInfo = (TextView) findViewById(R.id.tv_package_info); 
		mTvNumInfo = (TextView) findViewById(R.id.tv_num_info);
		mEtIdNum = (EditText) findViewById(R.id.et_id_num);
		mEtName = (EditText) findViewById(R.id.et_id_name);
		mEtDate = (EditText) findViewById(R.id.et_id_date);
		mEtAddress = (EditText) findViewById(R.id.et_id_address);
		mEtIccid = (EditText) findViewById(R.id.et_iccid);
		mIvIdentifyScan1 = (ImageView) findViewById(R.id.iv_id_1);
		mIvIdentifyScan2 = (ImageView) findViewById(R.id.iv_id_2);
		mBtnReadIccid = (Button) findViewById(R.id.btn_read_iccid);
		
		mIvTipAddress = (ImageView) findViewById(R.id.iv_tip_address);
		mIvTipDate    = (ImageView) findViewById(R.id.iv_tip_date);
		mIvTipIccid   = (ImageView) findViewById(R.id.iv_tip_iccid);
		mIvTipIdNum   = (ImageView) findViewById(R.id.iv_tip_idnum);
		mIvTipName    = (ImageView) findViewById(R.id.iv_tip_name);
		
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				sexUtil = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_sex_male, R.id.btn_sex_female }, new String[]
						{ "男", "女" }, R.color.text_blue_4, R.drawable.btn_tip_selected, 
						mFontColor);
			}
			else
			{
				sexUtil = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_sex_male, R.id.btn_sex_female }, new String[]
						{ "男", "女" }, R.drawable.btn_tip_select_orange, R.drawable.btn_tip_selected_orange,
						mFontColorOrange);
			}
			sexUtil.setSelected(0);
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		haoka = (Business_0C) FrameActivity.mBaseBusiness;
		try
		{
			String[] strcode = new String[3];
			String[] strtext = new String[3];
			for (int i = 0; i < Business_0C.productZifei.size(); i++)
			{
				Productzifei pz = Business_0C.productZifei.get(i);
				strcode[i] = pz.getCode() + "";
				strtext[i] = pz.getName();
			}
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				util = ButtonGroupUtil.createBean(this, new int[]
				{ R.id.btn_standard, R.id.btn_all, R.id.btn_half }, strcode, strtext, R.color.text_blue_4,
				R.drawable.btn_tip_selected, mFontColor);
			}
			else
			{
				util = ButtonGroupUtil.createBean(this, new int[]
				{ R.id.btn_standard, R.id.btn_all, R.id.btn_half }, strcode, strtext,  R.drawable.btn_tip_select_orange,
				R.drawable.btn_tip_selected_orange, mFontColorOrange);
			}
			util.setSelected(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	// 注册监听
	private void setListenner()
	{
		mLayIdType.setOnClickListener(this);
		mLayCover.setOnClickListener(this);
		mLayBack.setOnClickListener(this);
		mBtnPrevious.setOnClickListener(this);
		mIvIdentifyScan1.setOnClickListener(this);
		mIvIdentifyScan2.setOnClickListener(this);
		mBtnReadIccid.setOnClickListener(this);
		mTvKongka.setOnClickListener(this);
		mTvBaika.setOnClickListener(this);
		
		// 成卡
		mRbKongka.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if(isChecked)
				{
					selectCardType(0); 
				}
			}
		});
		
		// 白卡
		mRbBaika.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if(isChecked)
				{
					selectCardType(1); 
				}
			}
		});
		
		mLvIdType.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				selectIdType(position);
			}
		});
		// 对话框输入监听，在对话框后面提示输入是否正确
		addEditextListernner();
	}
	
	// 选择号卡类型  0成卡  1白卡
	private void selectCardType(int type)
	{
		if(type == 1)
		{
			haoka.openCardType = 1;
			mEtIccid.setEnabled(false);
			mEtIccid.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});  
			ICCID_LENGTH = 20;
			mRbBaika.setChecked(true);
		}
		else
		{
			haoka.openCardType = 0;
			ICCID_LENGTH = 19;
			mEtIccid.setFilters(new InputFilter[]{new InputFilter.LengthFilter(19)}); 
			mEtIccid.setEnabled(true);
			mRbKongka.setChecked(true);
		}
		mEtIccid.setText("");
	}
	
	// 选择证件类型
	private void selectIdType(int position)
	{
		switch (position)
		{
			case 0:
				mTvIdType.setText("18位身份证");
				mIdType = "02";
				break;
				
			case 1:
				mTvIdType.setText("15位身份证");
				mIdType = "01";
				break;
				
			case 2:
				mTvIdType.setText("驾驶证");
				mIdType = "03";
				break;
				
			case 3:
				mTvIdType.setText("军官证");
				mIdType = "04";
				break;
				
			case 4:
				mTvIdType.setText("护照");
				mIdType = "08";
				break;

			default:
				break;
		}
		mLayCover.setVisibility(View.GONE);
	}
	
	// 对话框输入监听，在对话框后面提示输入是否正确
	private void addEditextListernner()
	{
		mEtName.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				
				if(InputRules.isNull(s.toString()))
				{
					mIvTipName.setVisibility(View.GONE);
				}
				else
				{
					mIvTipName.setVisibility(View.VISIBLE);
					if (!InputRules.isNameValid(mIdType, s.toString()))
					{
						mIvTipName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipName.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		mEtIdNum.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIdNum.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIdNum.setVisibility(View.VISIBLE);
					if (!InputRules.isNoValid(mIdType, s.toString()))
					{
						mIvTipIdNum.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipIdNum.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		mEtDate.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipDate.setVisibility(View.GONE);
				}
				else
				{
					mIvTipDate.setVisibility(View.VISIBLE);
					if (!InputRules.checkIDEndTime(s.toString()).equals(""))
					{
						mIvTipDate.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipDate.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		mEtAddress.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipAddress.setVisibility(View.GONE);
				}
				else
				{
					mIvTipAddress.setVisibility(View.VISIBLE);
					if (InputRules.isAddressValid(mIdType, s.toString()) != 0)
					{
						mIvTipAddress.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipAddress.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		mEtIccid.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIccid.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIccid.setVisibility(View.VISIBLE);
					if (s.length() != ICCID_LENGTH) 
					{
						mIvTipIccid.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipIccid.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
	}
	
	// 初始化数据
	private void initData()
	{ 
		Bundle bundle = this.getIntent().getBundleExtra("_data");
		if (haoka.state)
		{
			try
			{
				PhoneModel model = (PhoneModel) bundle.getSerializable("model");
				if (model != null)
				{
					mTvPhoneInfo.setText(model.getNameDes() + "\n" + model.getPriceDes());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				ContractData contract = (ContractData) bundle.getSerializable("package");
				if (contract != null)
				{
					mTvPackageInfo.setText(contract.getContractDes());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				String num = bundle.getString("num");
				if (!StringUtil.isEmptyOrNull(num))
				{
					mTvNumInfo.setText(num);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			try
			{
				String timeLimit = bundle.getString("timeLimit");
				if (!StringUtil.isEmptyOrNull(timeLimit))
				{
					mTvNumInfo.setText(timeLimit);
				}
				else
				{
					mTvNumInfo.setText("无合约计划");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				ContractData contract = (ContractData) bundle.getSerializable("package");
				if (contract != null)
				{
					mTvPackageInfo.setText(contract.getPackageDes());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				String num = bundle.getString("num");
				if (!StringUtil.isEmptyOrNull(num))
				{
					mTvPhoneInfo.setText(num);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		mListIdType = new ArrayList<Object>();
		mListIdType.add("18位身份证");
		//mListIdType.add("15位身份证");
		//mListIdType.add("驾驶证");
		//mListIdType.add("军官证");
		//mListIdType.add("护照");
		ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(this, R.layout.lay_id_type_list_item,
				R.id.tv_text, mListIdType);
		mLvIdType.setAdapter(adapter);
		
		mEtAddress.setText("");
		mEtDate.setText("");
		mEtName.setText("");
		mEtIdNum.setText("");
		mEtIccid.setText("");
		sexUtil.setSelected(0);
		// 默认为成卡
		selectCardType(0);
		//mIvIdentifyScan1.setImageResource(R.drawable.iv_id_1);
		//mIvIdentifyScan2.setImageResource(R.drawable.iv_id_2);

		sPicFolder = FileOpr.getSdcardPath() + getString(R.string.identify_photo_path)
				+ String.valueOf(System.currentTimeMillis());
		filePath1 = sPicFolder + "/identify_1.jpg";
		filePath2 = sPicFolder + "/identify_2.jpg";
	}

	private Handler mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case Statics.MSG_CONNECT_BLUETOOTH:
					SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusinessIdinfoActivity.this, mBluetoothClient, equipmentService, 
							mHandler, Statics.MSG_CONNECT_DONE);
					dialog.show();
					break; 
				
				case Statics.MSG_CONNECT_DONE:
					/*
					DialogUtil.showProgress("正在检测蓝牙设备状态...");
					if(equipmentService.handshake()) 
					{
						DialogUtil.showProgress("正在读取卡号,请稍候...");
						readIccid();
					}
					else
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("温馨提示", "蓝牙连接失败，请重试连接蓝牙或者选择手动输入！"); 
					}
					*/
					break;
					
				case Statics.MSG_READ_CARD:
					DialogUtil.showProgress("正在读取卡号,请稍候...");
					readIccid();
					break;
					
				default:
					break;
			}
		}
	};
	
	@Override
	public void onClick(View v)
	{
		Bundle bundle = this.getIntent().getBundleExtra("_data");
		Message msg = new Message();
		if (ButtonUtil.isFastDoubleClick(v.getId(), 500))
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		switch (v.getId())
		{
			case R.id.iv_id_1: // 身份证扫描正面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath1);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath1, mIvIdentifyScan1.getWidth(), mIvIdentifyScan1.getHeight());
							mIvIdentifyScan1.post(new Runnable()
							{
								@SuppressWarnings("unused")
								@Override
								public void run()
								{
									if (msg == null)
										return;
									if (!StringUtil.isEmptyOrNull(msg.identify_no))
									{
										mEtIdNum.setText(msg.identify_no);
									}
									if (!StringUtil.isEmptyOrNull(msg.sex))
									{
										if ("01".equals(msg.sex))
										{
											sexUtil.setSelected(1);
										}
										else
										{
											sexUtil.setSelected(0);
										}
									}
									if (!StringUtil.isEmptyOrNull(msg.name))
									{
										mEtName.setText(msg.name);
									}
									if (!StringUtil.isEmptyOrNull(msg.end_time))
									{
										mEtDate.setText(msg.end_time);
									}
									if (!StringUtil.isEmptyOrNull(msg.address))
									{
										mEtAddress.setText(msg.address);
									}
									if (bitmap2 != null)
									{
										mIvIdentifyScan1.setImageBitmap(bitmap2);
										// bitmap2.recycle();
									}
								}
							});
						}
					}
				}.start();
				break;
				
			case R.id.iv_id_2:// 身份证扫描反面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath2);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath2, mIvIdentifyScan2.getWidth(), mIvIdentifyScan2.getHeight());
							mIvIdentifyScan2.post(new Runnable()
							{
								@Override
								public void run()
								{
									if (msg != null)
									{
										if (!StringUtil.isEmptyOrNull(msg.identify_no))
										{
											mEtIdNum.setText(msg.identify_no);
										}
										if (!StringUtil.isEmptyOrNull(msg.sex))
										{
											if ("01".equals(msg.sex))
											{
												sexUtil.setSelected(1);
											}
											else
											{
												sexUtil.setSelected(0);
											}
										}
										if (!StringUtil.isEmptyOrNull(msg.name))
										{
											mEtName.setText(msg.name);
										}
										if (!StringUtil.isEmptyOrNull(msg.end_time))
										{
											mEtDate.setText(msg.end_time);
										}
										if (!StringUtil.isEmptyOrNull(msg.address))
										{
											mEtAddress.setText(msg.address);
										}
										if (bitmap2 != null)
										{
											mIvIdentifyScan2.setImageBitmap(bitmap2);
										}
									}
								}
							});
						}
					}
				}.start();
				break;

			case R.id.btn_next:
				if(Statics.IS_DEBUG)
				{
					bundle.putString("className", NetAccessPermitsActivity.class.getName());
					bundle.putString("name", mEtName.getText().toString());
					bundle.putString("id_no", mEtIdNum.getText().toString());
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					return;
				}
				// 信息校驗
				String strResult = checkIdinfo();
				if("".equals(strResult.trim()))
				{
					getData();
					bundle.putString("className", NetAccessPermitsActivity.class.getName());
					bundle.putString("name", mEtName.getText().toString());
					bundle.putString("id_no", mEtIdNum.getText().toString());
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
				}
				else
				{
					DialogUtil.MsgBox("温馨提示", "以下开户信息未正确填写：\n"+ strResult);
				}
				break;

			case R.id.btn_read_iccid:
				if (Statics.IS_DEBUG)
				{
					DialogUtil.showProgress("正在读取卡号,请稍候...");
					new Thread()
					{
						public void run()
						{
							try
							{
								//final String iccid = equipmentService.readIccid();
								final String iccid = "8986999999999100003278";
								runOnUiThread(new Runnable()
								{
									public void run()
									{
										mEtIccid.setText(iccid);
									}
								});
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
							DialogUtil.closeProgress();
						};
					}.start();
				}
				else
				{
					new Thread() 
					{
						public void run() 
						{
							// 检测蓝牙设备是否存在
							DialogUtil.showProgress("正在检测蓝牙设备...");
							if(equipmentService.handshake() == false) 
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox("请连接蓝牙设备", 
										"检测到当前无蓝牙读卡设备, 是否连接蓝牙?", 
										"立即搜索", 
										Statics.MSG_CONNECT_BLUETOOTH, 
										"取消", 
										Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
										Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
										mHandler);
								return;
							}
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_READ_CARD);
						};
					}.start();
				}
				break;
				
			// 证件类型
			case R.id.layIdType:
				if(mListIdType.size() > 1)
				{
					mLayCover.setVisibility(View.VISIBLE);
				}
				break;
				
			// 覆盖全局
			case R.id.lay_cover:
				mLayCover.setVisibility(View.GONE);
				break;
				
			// 成卡
			case R.id.tvKongka:
				selectCardType(0);
				break;
				
			// 白卡
			case R.id.tvbaika:
				selectCardType(1);
				break;
			
			// 返回上一步
			case R.id.btn_pre:
				back();
				break;
				
			// 返回
			case R.id.btn_previous:
			case R.id.layBack:
				exit();
				break;

			default:
				break;
		}
	}
	
	// 退出
	private void exit()
	{
		DialogUtil.MsgBox("温馨提示", Statics.EXIT_TIP, 
		"确定", new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new Thread()
				{
					@Override
					public void run()
					{
						haoka.bll0C16();// 通知服务器释放资源
					}
				}.start();
				Message msg = new Message();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
				
			}
		}, "取消", null , null);
	}

	// 返回
	private void back()
	{
		Bundle bundle = getIntent().getBundleExtra("_data");
		Message msg = new Message();
		bundle.putString("className", MobileOrderActivity.class.getName());
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}

	private String checkIdinfo()
	{
		StringBuffer sb = new StringBuffer();
		String name = mEtName.getText().toString();
		String id	= mEtIdNum.getText().toString();
		String iccid = mEtIccid.getText().toString();
		String address = mEtAddress.getText().toString();
		String date  = mEtDate.getText().toString();
		if (StringUtil.isEmptyOrNull(name))
		{
			//sb.append("用户名不能为空！");
			//sb.append("\n");
			sb.append("用户名、");
		} 
		else if(!InputRules.isNameValid(mIdType, name))
		{
			//sb.append("用户名输入不合法！");
			//sb.append("\n");
			sb.append("用户名、");
		}
		
		if (StringUtil.isEmptyOrNull(id))
		{
			//sb.append("身份证号码不能为空！");
			//sb.append("\n");
			sb.append("证件号码、");
		}
		else if(!InputRules.isNoValid(mIdType, id))
		{
			//sb.append("证件号码输入不合法！");
			//sb.append("\n");
			sb.append("证件号码、");
		}
		
		if (StringUtil.isEmptyOrNull(date))
		{
			//sb.append("有效期不能为空！");
			//sb.append("\n");
			sb.append("有效期、");
		}
		else if(!StringUtil.isEmptyOrNull(InputRules.checkIDEndTime(date)))
		{
			//sb.append(InputRules.checkIDEndTime(date));
			//sb.append("\n");
			sb.append("有效期、");
		}
		
		if (StringUtil.isEmptyOrNull(address))
		{
			//sb.append("地址不能为空！");
			//sb.append("\n");
			sb.append("地址、");
		}
		else if(InputRules.isAddressValid(mIdType, address) != 0)
		{
			//sb.append("地址输入不合法！");
			//sb.append("\n");
			sb.append("地址、");
		}
		
		if (StringUtil.isEmptyOrNull(iccid))
		{
			//sb.append("卡号不能为空！");
			//sb.append("\n");
			sb.append("卡号");
		}
		else if(iccid.getBytes().length != ICCID_LENGTH)
		{
			//sb.append("卡号输入不合法！");
			//sb.append("\n");
			sb.append("卡号");
		}
		
		if(!StringUtil.isEmptyOrNull(sb.toString()))
		{
			if(sb.toString().endsWith("、"))
			{
				return sb.toString().trim().substring(0, sb.toString().trim().length() - 1)+"。";
			}
			else
			{
				return sb.toString().toString().trim()+"。";
			}
		}
		return sb.toString();
	}

	public void readIccid()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					final String iccid = equipmentService.readIccid();
					if (iccid != null)
					{
						int iswhiteCard = equipmentService.isWhiteCard((byte)1);
						// 白卡
						if(haoka.openCardType == 1)
						{
							DialogUtil.closeProgress();
							if(iswhiteCard != 1)
							{
								DialogUtil.MsgBox("温馨提示", "当前为白卡开卡，请插入白卡！");
								mEtIccid.post(new Runnable()
								{
									@Override
									public void run()
									{
										mEtIccid.setText("");
										haoka.iccid = "";
									}
								});
							}
							else
							{
								haoka.iccid = iccid;
								runOnUiThread(new Runnable()
								{
									@Override
									public void run()
									{
										mEtIccid.setText(haoka.iccid);
									}
								});
								DialogUtil.showToast("当前为白卡，读卡成功", 0x00);
							}
						}// 成卡
						else 
						{
							DialogUtil.closeProgress();
							if(iswhiteCard != 2)
							{
								DialogUtil.MsgBox("温馨提示", "当前为成卡开卡，请插入成卡！");
								mEtIccid.post(new Runnable()
								{
									@Override
									public void run()
									{
										mEtIccid.setText("");
										haoka.iccid = "";
									}
								});
							}
							else
							{
								haoka.iccid = iccid.substring(0, 19);
								runOnUiThread(new Runnable()
								{
									@Override
									public void run()
									{
										mEtIccid.setText(haoka.iccid);
									}
								});
								DialogUtil.showToast("当前为成卡，读卡成功", 0x00);
							}
						}
					}
					else
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("温馨提示", "读卡失败，请正确插入SIM卡，芯片朝上！");
						haoka.iccid = "";
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("温馨提示", "读卡失败，请正确插入SIM卡，芯片朝上！");
					haoka.iccid = "";
				}
			};
		}.start();
	}

	// 获取用户录入信息
	public void getData()
	{
		haoka.cust_type = mIdType;
		haoka.username = mEtName.getText().toString();
		haoka.address = mEtAddress.getText().toString();
		haoka.iccid = mEtIccid.getText().toString();// 20130930 aso
		try
		{
			if (sexUtil.getValue().equals("男"))
			{
				haoka.sex = "02";
			}
			else
			{
				haoka.sex = "01";
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		haoka.IdentityEndtime = mEtDate.getText().toString();
		haoka.Identity = mEtIdNum.getText().toString();
		haoka.sPicFolder = sPicFolder;
		try
		{
			haoka.zife = Integer.parseInt(util.getValue());
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private IdentifyMsg takePhoto(String path)
	{
		IdentifyScan photo = new IdentifyScan(this);
		int ret = photo.takePhoto(path);
		IdentifyMsg msg = null;
		if (ret == 1)
		{
			msg = photo.scanIdentifySigal(path);
		}
		return msg;

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");

		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
		try
		{
			PhoneModel model = (PhoneModel) bundle.getSerializable("model");
			if (model != null)
			{
				mTvPhoneInfo.setText(model.getNameDes() + "\n" + model.getPriceDes());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			ContractData contract = (ContractData) bundle.getSerializable("package");
			if (contract != null)
			{
				mTvPackageInfo.setText(contract.getContractDes());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			String num = bundle.getString("num");
			if (!StringUtil.isEmptyOrNull(num))
			{
				mTvNumInfo.setText(num);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		initData();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction != FrameActivity.DIRECTION_NEXT)
		{
			return;
		}
	}

	@Override
	public void onInitSuccess()
	{
		
	}

	@Override
	public void onInitFail()
	{
		
	}

	/*
	// 单击按钮监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.btn_next:
				mBtnNext.onTouch(event);
				break;

			default:
				break;
		}
		return false;
	}
	*/
}
