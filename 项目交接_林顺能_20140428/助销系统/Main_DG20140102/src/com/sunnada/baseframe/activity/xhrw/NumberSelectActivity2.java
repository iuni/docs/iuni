package com.sunnada.baseframe.activity.xhrw;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.PickNetWorkTelAdapter;
import com.sunnada.baseframe.dialog.CallBackChildHome;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.dialog.NumberChoiceDialog;
import com.sunnada.baseframe.dialog.NumberChoiceDialog.onChoiceGetListener;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.PickNetBusiness;

public class NumberSelectActivity2 extends DgActivity implements OnClickListener,OnScrollListener
{
	// 界面逻辑类
	UILogic                            mUILogic;
	
	private LinearLayout               mLayBack;                     // 回退LinearLayout
	private Button                     mBtnPrevious;                 // 回退按钮
	private MyCustomButton			   mBtnSelNum;
    private MyCustomButton             mBtnNext;
	private ListView			       telNumList;
	private PickNetWorkTelAdapter      telNumAdapter;
	private View				       moreView;// 加载更多页面
	private int					       lastItem;

	private int						   mFirstType			= 0; // 选号规则1(靓号)
	private String					   mFirstStr			= "";
	private int						   mSecondType			= 0; // 选号规则2(号段)
	private String					   mSecondStr			= "";
	private int						   mThirdType			= 0; // 选号规则3(价格)
	private int                        mPayType             = 0; // 付费类型

	public  PickNetBusiness            mPickNetBusiness     = null;//选号入网业务类
	private List<String>		       numList;
	private List<String>		       feeCList;  
	
	private List<String>               mTelFeeLevelsList;            // 号码费用档次列表
	
	private boolean                    mIsScrollOver        = true;  // 判断滚动翻页查询是否结束

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lay_numselect);
	
		mPickNetBusiness = new PickNetBusiness(this, equipmentService, socketService, dataBaseService);
		mPickNetBusiness.state = false;
		mPickNetBusiness.m0D04AddQueryTerm = 1;

		FrameActivity.mBaseBusiness = mPickNetBusiness;
		init();
		
		if(Statics.IS_DEBUG)
		{
			numInit();
			new Thread(new Runnable() 
			{
				@Override
				public void run()
				{
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							NumberChoiceDialog bm = new NumberChoiceDialog(NumberSelectActivity2.this,
									new onChoiceGetListener()
									{
										@Override
										public void onChoiceGot(int[] arrSelcet, String[] arrStr)
										{
											loadConditionData(arrSelcet, arrStr);
										}
									});
							bm.show();
						}
					});
				}
			}).start();
			return;
		}
		
		DialogUtil.showProgress("获取靓号中,请稍候");
		new Thread()
		{
			public void run()
			{
				int ret = mPickNetBusiness.bll0D05();

				if (ret != ReturnEnum.SUCCESS)
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							DialogUtil.closeProgress();
							back(null , null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
						}
					});
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							numInit();
							NumberChoiceDialog bm = new NumberChoiceDialog(NumberSelectActivity2.this,
									new onChoiceGetListener()
									{
										@Override
										public void onChoiceGot(int[] arrSelcet, String[] arrStr)
										{
											loadConditionData(arrSelcet, arrStr);
										}
									});
							bm.show();
						}
					});
					DialogUtil.closeProgress();
				}
			}
		}.start();
	}

	// 主界面初始化
	public void init()
	{
		// 确定按钮
		mBtnNext = (MyCustomButton) this.findViewById(R.id.moblie_next);
		mBtnNext.setTextViewText1("确定");
		mBtnNext.setImageResource(R.drawable.btn_custom_check);
		mBtnNext.setOnTouchListener(this);
		mBtnNext.setOnClickListener(this);
		
		// 筛选号码按钮
		mBtnSelNum = (MyCustomButton) this.findViewById(R.id.bt_selnum);
		mBtnSelNum.setTextViewText1("筛选号码");
		mBtnSelNum.setImageResource(R.drawable.ic_action_search);
		mBtnSelNum.setOnTouchListener(this);
		mBtnSelNum.setOnClickListener(this);
		
		// 回退按钮
		mBtnPrevious = (Button)this.findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		
		// 回退LinearLayout
		mLayBack = (LinearLayout)this.findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
	}
	
	// 号码界面初始化
	public void numInit()
	{
		if(telNumList != null)
		{
			try
			{
				telNumList.removeAllViews();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		moreView = getLayoutInflater().inflate(R.layout.load, null);
		moreView.setVisibility(View.GONE);

		telNumList = (ListView) this.findViewById(R.id.telnumList);
		telNumList.setDivider(null);
		telNumList.setDrawingCacheEnabled(false);
		telNumList.setVerticalScrollBarEnabled(false);

		List<String> numberList = getNumberList();
		List<String> feeList = getFeeList();
		List<String> feeLevelList = getFeeLevelList();
		telNumAdapter = new PickNetWorkTelAdapter(this, numberList, feeList,feeLevelList);

		// 添加底部view，一定要在setAdapter之前添加，否则会报错。
		telNumList.addFooterView(moreView); 	
		telNumList.setAdapter(telNumAdapter);
		// 设置listview的滚动事件
		telNumList.setOnScrollListener(this); 	
	}

	public String getChoseNum()
	{
		return telNumAdapter.getSelectedNumber();
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
	{
		// 减1是因为上面加了个addFooterView
		lastItem = firstVisibleItem + visibleItemCount - 1; 
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState)
	{
		if(Statics.IS_DEBUG)
		{
			loadMoreData();
			return;
		}
		
		if(mIsScrollOver)
		{	
			if (scrollState == SCROLL_STATE_IDLE && lastItem == telNumAdapter.getCount())
			{
				// 滚动翻页查询进行中
				mIsScrollOver = false;
				
				Log.i("TAG", "拉到最底部");
				moreView.setVisibility(View.VISIBLE);

				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							if(telNumAdapter != null)
							{

								if (telNumAdapter.getTelNumSize() < mPickNetBusiness.telnumTotal)
								{
									int ret = mPickNetBusiness.bll0D03(mFirstType, mFirstStr, mSecondType, mSecondStr,
											mThirdType, mPayType, (short) (telNumAdapter.getTelNumSize()+1), false);
									if (ret != ReturnEnum.SUCCESS)
									{
										runOnUiThread(new Runnable()
										{
											public void run()
											{
												back(new ICallBack() 
												{
													@Override
													public void back() 
													{
														mIsScrollOver = true;
													}
													
													@Override
													public void cancel() 
													{
													}

													@Override
													public void dismiss() 
													{
													}
												}, null, "未查询到符合条件的号码 !", "确定", null, true, false, false);
												moreView.setVisibility(View.GONE);
											}
										});
									}
									else
									{
										mHandler.sendEmptyMessage(0);
									}
								}
								else
								{
									runOnUiThread(new Runnable()
									{
										public void run()
										{
											back(new ICallBack() 
											{
												@Override
												public void back() 
												{
													mIsScrollOver = true;
												}
												
												@Override
												public void cancel() 
												{
												}
												
												@Override
												public void dismiss() 
												{
												}
											}, null, "没有更多的号码.", "确定", null, true, false, false);
											moreView.setVisibility(View.GONE);
										}
									});
								}
							}
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}).start();
			}
		}
	}

	// 声明Handler
	private Handler	mHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case 0:
					// 加载更多数据，这里可以使用异步加载
					loadMoreData(); 
					moreView.setVisibility(View.GONE);
					
					// 滚动ListView翻页查询结束
					mIsScrollOver = true;
					Log.i("TAG", "加载更多数据");
					break;

				default:
					break;
			}
		};
	};

	//加载数据
	private void loadNewData()
	{  
		if(Statics.IS_DEBUG)
		{
			List<String> numberList = new ArrayList<String>();
			List<String> feeList = new ArrayList<String>();
			List<String> feeLevelList = new ArrayList<String>();
			
			for(int i=0;i<6;i++)
			{
				numberList.add("12345678912");
				feeList.add("这是号码详情");
				feeLevelList.add("12");
			}
			telNumAdapter.clear();
			telNumAdapter.addData(numberList, feeList, feeLevelList);
			return;
		}
		telNumAdapter.clear();
		telNumAdapter.addData(getNumberList(), getFeeList(), getFeeLevelList());
	}

	// 加载更多数据
	private void loadMoreData()
	{ 
		if(Statics.IS_DEBUG)
		{
			new Thread(new Runnable() 
			{	
				@Override
				public void run() 
				{
					try 
					{
						moreView.setVisibility(View.VISIBLE);
						Thread.sleep(1000);
						moreView.setVisibility(View.GONE);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					back(null , null, "没有更多的号码!", "确定", null, false, false, false);
				}
			});
			return;
		}
		telNumAdapter.addData(getNumberList(), getFeeList(),getFeeLevelList());
	}

	private List<String> getNumberList()
	{
		numList = new ArrayList<String>();
		numList.addAll(mPickNetBusiness.resnum);
		return numList;
	}
	

	private List<String> getFeeList()
	{
		feeCList = new ArrayList<String>();
		feeCList.addAll(mPickNetBusiness.telPrintBuffer);
		return feeCList;
	}

	private List<String> getFeeLevelList()
	{
		mTelFeeLevelsList = new ArrayList<String>();
		mTelFeeLevelsList.addAll(mPickNetBusiness.mTelFeeLevelsList);
		return mTelFeeLevelsList;
	}
	
	@Override
	public void onClick(View v)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 500)) 
		{
			return;
		}
				
		final Bundle bundle = new Bundle();
		final Message msg = Message.obtain();
		switch (v.getId())
		{
			case R.id.moblie_next:
				if(Statics.IS_DEBUG)
				{
					bundle.putString("className", PickNetWorkPackageSelectActivity.class.getName());
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					return;
				}
				
				if(telNumAdapter != null)
				{
					if(!StringUtil.isEmptyOrNull(getChoseNum()))
					{
						try
						{
							Integer.parseInt(telNumAdapter.getSelectedTelFeeLevel());
						}catch (Exception e) {
							back(null, null, "该号码费用档次有误，请换个号码!", "确定", null, false, false, false);
							return;
						}
						
						DialogUtil.showProgress("正在预占号码,请稍候");
						new Thread()
						{
							public void run()
							{
								mPickNetBusiness.slectenum = getChoseNum();
								int ret = mPickNetBusiness.bll0D04(telNumAdapter.getSelectedTelFeeLevel());

								if (ret != ReturnEnum.SUCCESS)
								{
									DialogUtil.closeProgress();

									runOnUiThread(new Runnable()
									{
										public void run()
										{
											DialogUtil.closeProgress();
											back(null, null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
										}
									});
									return;
								}
								else
								{		
									goto_Layout(R.layout.lay_picknetwork_package_select);
								}
							};
						}.start();
					}
					else
					{
						back(null, null, "还未选定号码!", "确定", null, false, false, false);
					}
				}
				break;

			case R.id.bt_selnum:
				NumberChoiceDialog bm = new NumberChoiceDialog(this, new onChoiceGetListener()
				{
					@Override
					public void onChoiceGot(int[] arrSelcet, String[] arrStr)
					{
						loadConditionData(arrSelcet, arrStr);
					}
				});
				bm.show();
				break;
			
			case R.id.btn_previous:
				backToPrevious();
				break;
			
			case R.id.layBack:
				backToPrevious();
				break;
				
			default:
				break;
		}
	}

	//回退
	private void backToPrevious()
	{
		back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	// 加载查询条件
	public void loadConditionData(int[] arr, String[] arrStr)
	{
		if(Statics.IS_DEBUG)
		{
			loadNewData();
			return;
		}
		// arr { -1, -1, -1, -1 } 付费类型、号段、价格、靓号规则
		// arrStr { "", "" } 号段、靓号规则
		// 付费类型
		if (arr[0] != -1)
		{
			mPayType = arr[0];
		}

		// 号段
		if (arr[1] != -1 && arr[1] != 0)
		{
			mSecondType = 1;
			mSecondStr = arrStr[0];
		}
		else
		{
			mSecondType = 0;
			mSecondStr = "";
		}

		// 价格
		if (arr[2] == -1)
		{
			mThirdType = 0;
		}
		else
		{
			 mThirdType = arr[2];
		}

		// 靓号规则
		if (arr[3] != -1 && arr[3] != 0)
		{
			 mFirstType = 1;
			 mFirstStr = arr[3] + "";
		}
		else
		{
			 mFirstType = 0;
			 mFirstStr = "";
		}
		
		DialogUtil.showProgress("获取号码中,请稍候");
		new Thread()
		{
			public void run()
			{
				int ret = mPickNetBusiness.bll0D03(mFirstType, mFirstStr, mSecondType, mSecondStr,
						mThirdType, mPayType, (short) 1, true);
				
				if (ret != ReturnEnum.SUCCESS)
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							back(null , null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
							DialogUtil.closeProgress();
						}
					});
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							loadNewData();
							moreView.setVisibility(View.GONE);
							DialogUtil.closeProgress();
						}
					});
				}
			}
		}.start();
	}

	public void goto_Layout(int id) 
	{
		// 先把原来的指针清空
		mUILogic = null;

		mUILogic = UILogic.getInstance(id, this);
		mUILogic.go();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
	}

	
	
	@Override
	protected void onDestroy() 
	{
		System.gc();
		super.onDestroy();
	}

	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}	
}
