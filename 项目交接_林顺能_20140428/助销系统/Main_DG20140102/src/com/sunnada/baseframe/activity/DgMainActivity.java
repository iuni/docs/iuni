package com.sunnada.baseframe.activity;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.sunnada.baseframe.activity.busiaccept.BusiAcceptActivity;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.activity.charge.BusiQuickCharge;
import com.sunnada.baseframe.activity.charge.RechargeActivity;
import com.sunnada.baseframe.activity.dztj.RecommendActivity;
import com.sunnada.baseframe.activity.selfservice.BusiSelfService;
import com.sunnada.baseframe.activity.xhrw.NumberSelectActivity;
import com.sunnada.baseframe.activity.yygj.MobileIntroduceActivity;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ModifyPswdInfoDialog;
import com.sunnada.baseframe.dialog.ModifySrvDialog;
import com.sunnada.baseframe.dialog.ModifyUserPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.update.CheckVersionTask;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class DgMainActivity extends DgActivity implements OnClickListener
{
	private String				mStrBusiType;			// 模块类型
	private Business_00			mBusiness;

	private TextView			mImageHead;				// 点击用户头像弹出操作布局
	private ImageView			mImageHeadClose;		// 关闭用户操作布局
	private TextView			mTextOperationMoney;	// 当前营业额
	private PopupWindow			mPopUserInfo;			// 用户信息
	private CheckVersionTask	mCheckVersion;			// 程序升级
	
	private String[]			mStrSalesMoney	= new String[1];
	private TextView			mTvName1;
	private TextView			mTvAddress1;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 自助服务中软件强制升级失败后，先跳转到主界面，清空栈，最后退出程序
		if(getIntent().getBooleanExtra("isToExit", false))
		{
			finish();
		}
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dgmainactivity);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dgmainactivity_orange);
		}
		mBusiness = new Business_00(this, equipmentService, socketService, dataBaseService);
		DialogUtil.init(this);
		// 初始化控件
		initViews();
		// 显示代理商信息
		// showAgentInfo();
		// 密码修改相关
		checkModifyPswd();
	}

	// 初始化控件
	public void initViews()
	{
		// 优惠购机
		Button btnPreferentialBuy = (Button) findViewById(R.id.preferentialbuy);
		btnPreferentialBuy.setOnClickListener(this);
		// 选号入网
		Button btnChooseNumNet = (Button) findViewById(R.id.choosenumnet);
		btnChooseNumNet.setOnClickListener(this);
		// 套餐办理
		Button btnDealMeal = (Button) findViewById(R.id.dealmeal);
		btnDealMeal.setOnClickListener(this);
		// 快捷充值
		Button btnQuickPay = (Button) findViewById(R.id.installbd);
		btnQuickPay.setOnClickListener(this);
		// 充值中心
		Button btnChargeCenter = (Button) findViewById(R.id.installphone);
		btnChargeCenter.setOnClickListener(this);
		// 业务受理
		Button btnBusiAccept = (Button) findViewById(R.id.treasurebox);
		btnBusiAccept.setOnClickListener(this);
		// 店长推荐
		Button btnRecommend = (Button) findViewById(R.id.sel_recommend);
		btnRecommend.setOnClickListener(this);
		// 自服务
		Button btnSelfService = (Button) findViewById(R.id.smallcr);
		btnSelfService.setOnClickListener(this);
		
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView = LayoutInflater.from(this).inflate(R.layout.pop_user_info, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = LayoutInflater.from(this).inflate(R.layout.pop_user_info_orange, null);
		}
		mPopUserInfo = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mPopUserInfo.setFocusable(true);
		mPopUserInfo.setTouchable(true);
		mPopUserInfo.setBackgroundDrawable(new BitmapDrawable());
		mPopUserInfo.setOnDismissListener(new OnDismissListener()
		{
			@Override
			public void onDismiss()
			{
				mTvName1.setVisibility(View.VISIBLE);
				mTvAddress1.setVisibility(View.VISIBLE);
			}
		});
		
		// 头像1
		mImageHead = (TextView) findViewById(R.id.headm);
		mImageHead.setOnClickListener(this);
		// 头像2
		mImageHeadClose = (ImageView) contentView.findViewById(R.id.operation_head);
		mImageHeadClose.setOnClickListener(this);
		// 用户当前营业额
		mTextOperationMoney = (TextView) contentView.findViewById(R.id.operation_money);
		// 修改密码
		LinearLayout layModifyPswd = (LinearLayout) contentView.findViewById(R.id.operation_modify_pswd);
		layModifyPswd.setOnClickListener(this);
		// 软件升级
		LinearLayout laySoftUpdate = (LinearLayout) contentView.findViewById(R.id.operation_soft_update);
		laySoftUpdate.setOnClickListener(this);
		// 系统帮助
		LinearLayout laySystemHelp = (LinearLayout) contentView.findViewById(R.id.operation_system_help);
		laySystemHelp.setOnClickListener(this);
		// 注销系统
		LinearLayout layLogoff = (LinearLayout) contentView.findViewById(R.id.operation_logoff);
		layLogoff.setOnClickListener(this);
		
		// 先掉落套餐办理
		Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		btnDealMeal.startAnimation(anim);
		// 后掉落百宝箱
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(100);
		btnBusiAccept.startAnimation(anim);
		// 接着小课堂
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(200);
		btnSelfService.startAnimation(anim);
		// 接着宽带
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(300);
		btnQuickPay.startAnimation(anim);
		// 接着固话
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(400);
		btnChargeCenter.startAnimation(anim);
		// 优惠购机
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(500);
		btnPreferentialBuy.startAnimation(anim);
		// 选号入网
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(600);
		btnChooseNumNet.startAnimation(anim);
		// 应用下载
		anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.dropdown);
		anim.setStartOffset(700);
		btnRecommend.startAnimation(anim);
		
		// 显示代理商信息
		showAgentInfo(contentView);
	}
	
	// 显示代理商信息
	private void showAgentInfo(View contentView) 
	{
		try
		{
			String agentName = App.getUserName();
			if (StringUtil.isEmptyOrNull(agentName)) 
			{
				agentName = "xxx"; 
			}
			// 营业员姓名
			mTvName1 = (TextView) findViewById(R.id.tv_name);
			mTvName1.setText(agentName);
			TextView tvName2 = (TextView) contentView.findViewById(R.id.tv_name2);
			tvName2.setText(agentName);
			// 网点名称
			String agentAddr = App.getShopName();
			if (StringUtil.isEmptyOrNull(agentAddr)) 
			{
				agentAddr = "xxxxxxxx";
			}
			mTvAddress1 = (TextView) findViewById(R.id.tv_address);
			mTvAddress1.setText(agentAddr);
			TextView tvAddress2 = (TextView) contentView.findViewById(R.id.tv_address2);
			tvAddress2.setText(agentAddr);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	// 密码修改相关
	private void checkModifyPswd()
	{
		// 设置初始密码
		if (1 == mBusiness.getModifyUserPasswordFlag()) 
		{
			ModifyPswdInfoDialog dialog = new ModifyPswdInfoDialog(this, mHandler);
			dialog.show();
		}
		// 重设工号密码
		else if(2 == mBusiness.getModifyUserPasswordFlag()) 
		{
			ModifyUserPswdDialog dialog = new ModifyUserPswdDialog(this, mBusiness, mHandler);
			dialog.show();
		}
		// 重设交易密码
		else if(2 == mBusiness.getModifySrvPasswordFlag()) 
		{
			ResetSrvDialog dialog = new ResetSrvDialog(this, mBusiness);
			dialog.show();
		}
		// 搜索蓝牙
		else
		{
			//SearchBluetoothDialog dialog = new SearchBluetoothDialog(this, mBluetoothClient, equipmentService);
			//dialog.show();
		}
	}
	
	// 消息处理器
	private Handler	mHandler	= new Handler()
	{
		public void handleMessage(Message message)
		{
			switch (message.what)
			{
				// 修改初始密码
				case Statics.MSG_MODIFY_PSWD:
					Log.d("xxxx", "接收到密码修改的消息");
					ModifyPswdDialog dialog = new ModifyPswdDialog(DgMainActivity.this, mBusiness);
					dialog.show();
					break;

				// 不修改初始密码
				case Statics.MSG_MODIFY_PSWD_CANCEL:
					break;

				// 获取密码场景ok
				case Statics.MSG_GET_PSWD_SCENE_OK:
					// 初始密码需要修改
					if (Business_00.mPswdScene == 0x01)
					{
						ModifyPswdDialog dialog1 = new ModifyPswdDialog(DgMainActivity.this, mBusiness);
						dialog1.show();
					}
					// 登录密码需要修改
					else if (Business_00.mPswdScene == 0x02)
					{
						ModifyUserPswdDialog dialog1 = new ModifyUserPswdDialog(DgMainActivity.this, mBusiness, mHandler);
						dialog1.show();
					}
					// 交易密码修改
					else if (Business_00.mPswdScene == 0x03)
					{
						ModifySrvDialog dialog1 = new ModifySrvDialog(DgMainActivity.this, mBusiness, mHandler);
						dialog1.show();
					}
					// 重设交易密码
					else if(Business_00.mPswdScene == 0x04) 
					{
						ResetSrvDialog dialog1 = new ResetSrvDialog(DgMainActivity.this, mBusiness);
						dialog1.show();
					}
					break;
					
				// 升级
				case Statics.MSG_UPDATE:
					// 下载
					new Thread()
					{
						public void run()
						{
							DialogUtil.showProgress("正在下载升级文件...");
							mCheckVersion.checkBreakDownLoad(Statics.PATH_APK + "/Main_DG.apk");
						}
					}.start();
					break;

				// 用户取消升级
				case Statics.MSG_UPDATE_CANCEL:
					if(Statics.IS_FORCED_UPDATE)
					{
						// 取消强制升级
						finish();
					}
					break;
					
				// 正在升级
				case Statics.MSG_UPDATING:
					try
					{
						DialogUtil.showProgress(String.format("下载中(%d/%d KB)", mCheckVersion.getFileCurrentLength() / 1024,
								mCheckVersion.getFileTotalLength() / 1024));
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					break;

				// 升级失败
				case Statics.MSG_UPDATE_FAILED:
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("软件升级提示", mCheckVersion.getLastErrorStr(), 
					"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
					break;

				// 升级成功
				case Statics.MSG_UPDATE_OK:
					DialogUtil.closeProgress();
					// 跳转到APK安装界面
					CheckVersionTask.installApkRequestCode(new File(Statics.PATH_APK + "/Main_DG.apk"),
							DgMainActivity.this, Statics.INSTALL_APK_REQUEST);
					break;
					
				// 软件安装失败
				case Statics.MSG_INSTALL_FAILED:
					if(Statics.IS_FORCED_UPDATE)
					{
						finish();
					}
					break;
					
				default:
					break;
			}
		}
	};

	@Override
	public void onClick(final View v)
	{
		if (ButtonUtil.isFastDoubleClick(500))
		{
			showErrorMsg("您的操作速度太快了!");
			return;
		}
		
		switch (v.getId())
		{
			case R.id.preferentialbuy:
			case R.id.dealmeal:   
			case R.id.choosenumnet:
			case R.id.installbd:
			case R.id.installphone:
			case R.id.treasurebox:
			case R.id.sel_recommend:
			case R.id.smallcr:
				procMainMenu(v);
				break;

			// 弹出用户操作布局
			case R.id.headm:
				new Thread()
				{
					public void run()
					{
						DialogUtil.showProgress("正在获取当日营业额...");
						//BusinessNum businessNum = new BusinessNum(DgMainActivity.this);
						//final String tempstr = businessNum.successOrFailMoney("交易成功", App.getUserNo());
						Business_00 business = new Business_00(DgMainActivity.this, equipmentService, socketService, dataBaseService);
						mStrSalesMoney[0] = "";
						boolean ret = business.bll0015(mStrSalesMoney);
						DialogUtil.closeProgress();
						if(!ret)
						{
							DialogUtil.MsgBox("温馨提示", business.getLastknownError());
						}
						// 弹出用户信息界面
						runOnUiThread(new Runnable() 
						{
							public void run() 
							{
								mPopUserInfo.showAtLocation(v, Gravity.TOP|Gravity.RIGHT, 0, 0);
								mTvName1.setVisibility(View.GONE);
								mTvAddress1.setVisibility(View.GONE);
								if (StringUtil.isEmptyOrNull(mStrSalesMoney[0])) 
								{
									mTextOperationMoney.setText("0.00元");
								}
								else
								{
									mTextOperationMoney.setText(mStrSalesMoney[0] + "元");
								}
							}
						});
					}
				}.start();
				break;

			// 隐藏用户操作布局
			case R.id.operation_head:
				closePopWindow(mPopUserInfo);
				break;

			// 修改密码
			case R.id.operation_modify_pswd:
				closePopWindow(mPopUserInfo);
				procModifyPswd();
				break;

			// 系统帮助
			case R.id.operation_system_help:
				closePopWindow(mPopUserInfo);
				break;

			// 注销系统
			case R.id.operation_logoff:
				closePopWindow(mPopUserInfo);
				procLogOff();
				break;
			
			// 软件升级
			case R.id.operation_soft_update:
				closePopWindow(mPopUserInfo);
				new Thread()
				{
					public void run()
					{
						// 检测升级
						mCheckVersion = new CheckVersionTask(DgMainActivity.this, mHandler);
						// checkupdate的参数为false代表"不需要升级时不提示任何信息"
						DialogUtil.showProgress("正在检测更新...");
						if (mCheckVersion.checkupdate(true) == false)
						{
							// 关闭进度条
							DialogUtil.closeProgress();
						}
					}
				}.start();
				break;

			default:
				break;
		}
	}

	// 关闭弹出框
	private void closePopWindow(PopupWindow popWindow)
	{
		if (popWindow != null && popWindow.isShowing()) 
		{
			popWindow.dismiss();
		}
	}
	
	// 处理基本菜单
	private void procMainMenu(View v) 
	{
		// 启动翻转的效果
		startRotation(v);
		String authority = "";
		switch (v.getId())
		{
		// 优惠购机
		case R.id.preferentialbuy:
			mStrBusiType = MobileIntroduceActivity.class.getName();
			authority = "0100";
			break;
			
		// 选号入网
		case R.id.choosenumnet:
			mStrBusiType = NumberSelectActivity.class.getName();
			authority = "0200";
			break;
		
		// 套餐办理
		case R.id.dealmeal:
			mStrBusiType = TestActivity1.class.getName();
			authority = "0300";
			break;	
			
		// 快捷充值
		case R.id.installbd:
			// mStrBusiType = RechargeNetPayActivity.class.getName();
			mStrBusiType = BusiQuickCharge.class.getName();
			authority = "0400";
			break;
			
		// 充值中心
		case R.id.installphone:
			mStrBusiType = RechargeActivity.class.getName();
			authority = "0500";
			break;
			
		// 店长推荐
		case R.id.sel_recommend: 
			mStrBusiType = RecommendActivity.class.getName();
			authority = "0600";
			break;
			
		// 业务受理
		case R.id.treasurebox:
			mStrBusiType = BusiAcceptActivity.class.getName();
			authority = "0700";
			break;
			
		// 自服务
		case R.id.smallcr:
			mStrBusiType = BusiSelfService.class.getName();
			authority = "0800";
			break;
		}
		boolean result = getMenuAuthority(authority); 
		// 自助服务固化在本地
		if(authority.equals("0800"))
		{
			result = true;
		}
		if(!result)
		{
			DialogUtil.MsgBox("温馨提示", "当前菜单功能未授权开通，请联系管理员！");
			return;
		}
		else
		{
			TimerTask task = new TimerTask() 
			{
				@Override
				public void run() 
				{
					synchronized (Statics.INIT_OK_LOCK) 
					{
						if(mStrBusiType.equals(BusiQuickCharge.class.getName())) 
						{
							Intent intent = new Intent(DgMainActivity.this, BusiQuickCharge.class);
							startActivity(intent);
						}
						else
						{
							Intent intent = new Intent(DgMainActivity.this, FrameActivity.class);
							intent.putExtra("type", mStrBusiType);
							startActivity(intent);
						}
						// 手动调用垃圾回收器
						System.gc();
					}
				}
			};
			Timer timer = new Timer(); 
			timer.schedule(task, 300);
		}
	}
	
	@Override
	public void onInitSuccess()
	{

	}

	@Override
	public void onInitFail()
	{

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			procLogOff();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Statics.INSTALL_APK_REQUEST)
		{
			if(resultCode == RESULT_CANCELED)
			{
				DialogUtil.MsgBox("温馨提示", "软件安装失败，请进行手动安装！\nAPK路径：SD卡/sunnada/apks/Main_DG.apk", 
						"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
			}
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		// 手动调用垃圾回收器
		System.gc();
		Log.e("", "主界面onDestroy()");
	}
	
	// 处理密码修改
	private void procModifyPswd()
	{
		new Thread()
		{
			public void run()
			{
				try
				{
					DialogUtil.showProgress("正在获取数据...");
					// 获取交易流水, 密码再次设置, 默认为工号密码修改
					if (mBusiness.bll000A((byte) 0x02, (byte) 0x02) == false) 
					{
						DialogUtil.closeProgress();
						//DialogUtil.MsgBox("温馨提示", mBusiness.getLastknownError());
						return;
					}
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(Statics.MSG_GET_PSWD_SCENE_OK);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					DialogUtil.closeProgress();
				}
			}
		}.start();
	}

	// 处理注销
	private void procLogOff()
	{
		DialogUtil.MsgBox("提示信息", 
				"确定要注销本系统吗?", 
				"注销", 
				new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						// 调转到登录activity
						Intent intent = new Intent(DgMainActivity.this, LaunchActivity.class);
						startActivity(intent);
						finish();
					}
				}, 
				"取消", 
				null, 
				null);
	}
}