package com.sunnada.baseframe.activity.yygj;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ContractData;
import com.sunnada.baseframe.bean.PackageInfo;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class PackageSelectActivity extends DgActivity implements OnClickListener
{
	private static final String	TITLE_STR		= "选择号码业务办理提示";
	//private LinearLayout		lay_type		= null;
	//private LinearLayout		lay_ending		= null;
	//private LinearLayout		lay_fee			= null;
	//private LinearLayout		lay_plan		= null;		
	//private View				mLayBack;	
	//private Button 			mBtnPrevious;				// 返回按钮
	
	private TextView			tv_type			= null;
	private TextView			tv_ending		= null;
	private TextView			tv_fee			= null;
	private TextView			tv_plan			= null;

	private RelativeLayout		lay_cover		= null;

	private ImageView			iv_type			= null;
	private ImageView			iv_ending		= null;
	private ImageView			iv_fee			= null;
	private ImageView			iv_plan			= null;

	private ListView			lv_type			= null;
	private ListView			lv_ending		= null;
	private ListView			lv_fee			= null;
	private ListView			lv_plan			= null;

	//private MyCustomButton 	mbtnNext		= null;
	//private MyCustomButton 	mbtnPre			= null;
	private TextView			tv_phone_info	= null;
	private TextView			tv_package_info	= null;

	private List<ContractData>	dataList		= new ArrayList<ContractData>();

	private List<ContractData>	currentList		= null;
	private ContractData		currentContract	= null;

	private ArrayAdapter<Object>adapter			= null;
	//private PackageInfoDao	dao				= null;
	private TextView			tv_call_time	= null;
	private TextView			tv_flow1		= null;
	private TextView			tv_flow2		= null;
	private TextView			tv_video_call	= null;
	private TextView			tv_other		= null;
	private TextView			tv_msg_count	= null;
	private TextView			tv_free			= null;
	private TextView			tv_call_fee		= null;
	
	private TextView			tv_label_local	= null;
	private Business_0C			haoka			= null;
	private boolean				mIsSelect		= false;
	private int					mOldProID;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_package_select);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_package_select_orange);
		}
		haoka = (Business_0C) FrameActivity.mBaseBusiness;
		// 初始化控件
		initViews();
		//dao = new PackageInfoDao(this);
		//dao.CreatTable();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 返回上一级
		View layBack = findViewById(R.id.layBack);
		layBack.setOnClickListener(this);
		Button btnPrevious = (Button) findViewById(R.id.btn_previous);
		btnPrevious.setOnClickListener(this);
		
		LinearLayout lay_type = (LinearLayout) findViewById(R.id.lay_type);
		lay_type.setOnClickListener(this);
		LinearLayout lay_ending = (LinearLayout) findViewById(R.id.lay_ending);
		lay_ending.setOnClickListener(this);
		LinearLayout lay_fee = (LinearLayout) findViewById(R.id.lay_fee);
		lay_fee.setOnClickListener(this);
		LinearLayout lay_plan = (LinearLayout) findViewById(R.id.lay_plan);
		lay_plan.setOnClickListener(this);
		
		tv_type = (TextView) findViewById(R.id.tv_type);
		tv_ending = (TextView) findViewById(R.id.tv_ending);
		tv_fee = (TextView) findViewById(R.id.tv_fee);
		tv_plan = (TextView) findViewById(R.id.tv_plan);
		
		lay_cover = (RelativeLayout) findViewById(R.id.lay_cover);
		lay_cover.setOnClickListener(this);
		
		iv_type = (ImageView) findViewById(R.id.iv_type);
		iv_ending = (ImageView) findViewById(R.id.iv_ending);
		iv_fee = (ImageView) findViewById(R.id.iv_fee);
		iv_plan = (ImageView) findViewById(R.id.iv_plan);
	
		lv_type = (ListView) findViewById(R.id.lv_type);
		lv_ending = (ListView) findViewById(R.id.lv_ending);
		lv_fee = (ListView) findViewById(R.id.lv_fee);
		lv_plan = (ListView) findViewById(R.id.lv_plan);
		
		lv_type.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				currentContract = currentList.get(arg2);
				tv_type.setText(currentContract.actName);
				tv_ending.setText(currentContract.timeLimit);
				tv_fee.setText(String.valueOf(currentContract.proMoney));
				tv_plan.setText(currentContract.planType);
				lay_cover.setVisibility(View.GONE);
				Business_0C.mPayType = currentContract.proType;
				haoka.ProductCode = currentContract.proID;
				showConstractInfo();
			}
		});
		
		lv_ending.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				currentContract = currentList.get(arg2);
				tv_ending.setText(currentContract.timeLimit);
				tv_fee.setText(String.valueOf(currentContract.proMoney));
				tv_plan.setText(currentContract.planType);
				lay_cover.setVisibility(View.GONE);
				showConstractInfo();
			}
		});

		lv_fee.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				currentContract = currentList.get(arg2);
				tv_fee.setText(String.valueOf(currentContract.proMoney));
				tv_plan.setText(currentContract.planType);
				lay_cover.setVisibility(View.GONE);
				showConstractInfo();
			}
		});

		lv_plan.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				currentContract = currentList.get(arg2);
				tv_plan.setText(currentContract.planType);
				lay_cover.setVisibility(View.GONE);
				showConstractInfo();
			}
		});
		
		tv_phone_info = (TextView) findViewById(R.id.tv_phone_info);
		tv_phone_info.setOnClickListener(this);
		
		tv_package_info = (TextView) findViewById(R.id.tv_package_info);
		//Bundle bundle = this.getIntent().getBundleExtra("_data");
		// PhoneModel model = (PhoneModel) bundle.getSerializable("model");
		// if (model != null) {
		// tv_phone_info.setText(model.getNameDes() + "\n"
		// + model.getPriceDes());
		// }
		tv_call_time = (TextView) findViewById(R.id.tv_call_time);
		tv_flow1 = (TextView) findViewById(R.id.tv_flow1);
		tv_flow2 = (TextView) findViewById(R.id.tv_flow2);
		tv_video_call = (TextView) findViewById(R.id.tv_video_call);
		tv_other = (TextView) findViewById(R.id.tv_other);
		tv_msg_count = (TextView) findViewById(R.id.tv_msg_count);
		tv_free = (TextView) findViewById(R.id.tv_free);
		tv_call_fee = (TextView) findViewById(R.id.tv_call_fee);
		tv_label_local = (TextView) findViewById(R.id.tv_label_local);
		
		MyCustomButton btnNext = (MyCustomButton) findViewById(R.id.moblie_next);
		btnNext.setTextViewText1("确认");
		btnNext.setImageResource(R.drawable.btn_custom_check);
		btnNext.setOnTouchListener(this);
		btnNext.setOnClickListener(this);
		
		MyCustomButton btnPre = (MyCustomButton) findViewById(R.id.moblie_pre);
		btnPre.setTextViewText1("返回");
		btnPre.setImageResource(R.drawable.btn_back);
		btnPre.setOnTouchListener(this);
		btnPre.setOnClickListener(this);
		Log.e(TITLE_STR, "初始化控件成功");
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		Bundle bundle = getIntent().getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		try
		{
			PhoneModel model = (PhoneModel) bundle.getSerializable("model");
			if (model != null)
			{
				tv_phone_info.setText(model.getNameDes() + "\n" + model.getPriceDes());
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		initData();
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		System.gc();
		Log.e("", TITLE_STR + "onDestroy");
	}
	
	// 消息处理器
	private Handler mHandler = new  Handler() 
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				// 查询成功
				case 0:
					PackageInfo packageInfo = haoka.mPackageInfo; 
					tv_call_time.setText(packageInfo.getmCall());
					tv_flow1.setText(packageInfo.getmFlow());
					tv_flow2.setText(packageInfo.getmFeeFlow());
					tv_video_call.setText(packageInfo.getmFeeVedioCall());
					tv_other.setText(packageInfo.getmOther());
					tv_msg_count.setText(packageInfo.getmMsgCount());
					tv_free.setText(packageInfo.getmFreeIncomingCall());
					tv_call_fee.setText(packageInfo.getmFeeCall());
					
					if (currentContract.planType.equals("C"))
					{
						tv_label_local.setText("本地语音拨打分钟数");
					}
					else
					{
						tv_label_local.setText("国内语音拨打分钟数");
					}
					mIsSelect = true;
					DialogUtil.closeProgress();
					break;

				// 查询失败
				case 1:
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, haoka.getLastknownError());
					Log.e("", "获取合约计划详情失败!");
					
					tv_call_time.setText("——");
					tv_flow1.setText("——");
					tv_flow2.setText("——");
					tv_video_call.setText("——");
					tv_other.setText("——");
					tv_msg_count.setText("——");
					tv_free.setText("——");
					tv_call_fee.setText("——");
					mIsSelect = false; 
					break;
					
				case 3:
					// 失败后释放资源
					backToPrevious();
					break;
					
				default:
					break;
			}
		}
	};
	
	// 初始化数据
	private void initData()
	{
		if (Statics.IS_DEBUG)
		{
			ContractData data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "存话费送手机";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "12";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "24";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 46;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "A";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "B";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 66;
			data.planType = "C";
			//data.actMoney = 200;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "A";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "B";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);

			data = new ContractData();
			data.actName = "购机入网";
			data.timeLimit = "36";
			data.proMoney = 96;
			data.planType = "C";
			//data.actMoney = 100;
			//data.phoneMoney = 500;
			dataList.add(data);
		}
		else
		{
			dataList = Business_0C.contractList;
		}
		currentContract = dataList.get(0);
		tv_type.setText(currentContract.actName);
		tv_ending.setText(currentContract.timeLimit);
		tv_fee.setText(String.valueOf(currentContract.proMoney));
		tv_plan.setText(currentContract.planType);
		Business_0C.mPayType = currentContract.proType;
		haoka.ProductCode = currentContract.proID;
		
		showConstractInfo();
		Log.e(TITLE_STR, "初始化数据成功");
	}
	
	@Override
	public void onInitSuccess()
	{

	}

	@Override
	public void onInitFail()
	{

	}

	@Override
	public void onClick(View v)
	{
		Bundle bundle = this.getIntent().getBundleExtra("_data");
		Message msg = new Message();

		if (ButtonUtil.isFastDoubleClick(v.getId(), 500))
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		switch (v.getId())
		{
			/*
			case R.id.tv_phone_info:
				backToPrevious();
				bundle.putString("className", MobileIntroduceActivity.class.getName());
				msg.what = FrameActivity.DIRECTION_PREVIOUS;
				msg.setData(bundle);
				FrameActivity.mHandler.sendMessage(msg);
				break; 
			*/
				
			case R.id.moblie_next:
				if(mIsSelect)
				{
					bundle.putString("className", MobileOrderActivity.class.getName());
					bundle.putSerializable("package", currentContract);
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
				}
				else
				{
					DialogUtil.MsgBox("温馨提示", "请选择套餐！");
				}
				break;

			case R.id.lay_type:
				showList(0);
				break;

			case R.id.lay_ending:
				showList(1);
				break;

			case R.id.lay_fee:
				showList(2);
				break;

			case R.id.lay_plan:
				showList(3);
				break;

			case R.id.lay_cover:
				lay_cover.setVisibility(View.GONE);
				break;
				
			// 返回上一步
			case R.id.moblie_pre:
				backToPrevious();
				break;
				
			// 返回主界面
			case R.id.btn_previous:
			case R.id.layBack:
				exit();
				break;

			default:
				break;
		}
	}

	// 退出
	private void exit()
	{
		DialogUtil.MsgBox(TITLE_STR, 
				Statics.EXIT_TIP, 
				"确定", 
				new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						new Thread()
						{
							@Override
							public void run()
							{
								// 通知服务器释放资源
								haoka.bll0C16();
							}
						}.start();				
						FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
					}
				}, 
				"取消", 
				null , 
				null);
	}

	// 显示下拉菜单
	private void showList(int index)
	{
		lay_cover.setVisibility(View.VISIBLE);
		currentList = new ArrayList<ContractData>();
		List<Object> tempList = new ArrayList<Object>();

		switch (index)
		{
			// 合约类型
			case 0:
				String lastType = null;
				for (int i = 0; i < dataList.size(); i++)
				{
					ContractData contract = dataList.get(i);
					String type = contract.actName;
					if (!type.equals(lastType) && !tempList.contains(type))
					{
						currentList.add(contract);
						tempList.add(type);
						lastType = type;
					}
				}
				adapter = new ArrayAdapter<Object>(PackageSelectActivity.this, R.layout.lay_package_list_item,
						R.id.tv_text, tempList);
				lv_type.setAdapter(adapter);

				iv_type.setVisibility(View.VISIBLE);
				lv_type.setVisibility(View.VISIBLE);

				iv_ending.setVisibility(View.GONE);
				lv_ending.setVisibility(View.GONE);
				iv_fee.setVisibility(View.GONE);
				lv_fee.setVisibility(View.GONE);
				iv_plan.setVisibility(View.GONE);
				lv_plan.setVisibility(View.GONE);
				break;

			// 合约期限
			case 1:
				String lastTime = null;
				for (int i = 0; i < dataList.size(); i++)
				{
					ContractData contract = dataList.get(i);
					String type = contract.actName;
					String time = contract.timeLimit;
					if (!type.equals(currentContract.actName))
					{
						continue;
					}
					if (!time.equals(lastTime) && !tempList.contains(time))
					{
						currentList.add(contract);
						tempList.add(time);
						lastTime = time;
					}
				}

				adapter = new ArrayAdapter<Object>(PackageSelectActivity.this, R.layout.lay_package_list_item,
						R.id.tv_text, tempList);
				lv_ending.setAdapter(adapter);
				iv_ending.setVisibility(View.VISIBLE);
				lv_ending.setVisibility(View.VISIBLE);

				iv_type.setVisibility(View.GONE);
				lv_type.setVisibility(View.GONE);
				iv_fee.setVisibility(View.GONE);
				lv_fee.setVisibility(View.GONE);
				iv_plan.setVisibility(View.GONE);
				lv_plan.setVisibility(View.GONE);
				break;

			// 套餐档次
			case 2:
				int lastFee = -1;
				for (int i = 0; i < dataList.size(); i++)
				{
					ContractData contract = dataList.get(i);
					String type = contract.actName;
					int fee = contract.proMoney;
					String time = contract.timeLimit;
					if (!type.equals(currentContract.actName))
					{
						continue;
					}
					if (!time.equals(currentContract.timeLimit))
					{
						continue;
					}
					if (lastFee != fee && !tempList.contains(fee))
					{
						currentList.add(contract);
						tempList.add(fee);
						lastFee = fee;
					}
				}
				adapter = new ArrayAdapter<Object>(PackageSelectActivity.this, R.layout.lay_package_list_item,
						R.id.tv_text, tempList);
				lv_fee.setAdapter(adapter);

				iv_type.setVisibility(View.GONE);
				lv_type.setVisibility(View.GONE);
				iv_ending.setVisibility(View.GONE);
				lv_ending.setVisibility(View.GONE);

				iv_fee.setVisibility(View.VISIBLE);
				lv_fee.setVisibility(View.VISIBLE);

				iv_plan.setVisibility(View.GONE);
				lv_plan.setVisibility(View.GONE);
				break;

			// 套餐计划
			case 3:
				String lastPlan = null;
				for (int i = 0; i < dataList.size(); i++)
				{
					ContractData contract = dataList.get(i);
					String type = contract.actName;
					int fee = contract.proMoney;
					String time = contract.timeLimit;
					String planType = contract.planType;
					if (!type.equals(currentContract.actName))
					{
						continue;
					}
					if (!time.equals(currentContract.timeLimit))
					{
						continue;
					}
					if (fee != currentContract.proMoney)
					{
						continue;
					}
					if (!planType.equals(lastPlan) && !tempList.contains(planType))
					{
						currentList.add(contract);
						tempList.add(planType);
						lastPlan = planType;
					}
				}

				adapter = new ArrayAdapter<Object>(PackageSelectActivity.this, R.layout.lay_package_list_item,
						R.id.tv_text, tempList);
				lv_plan.setAdapter(adapter);
				iv_type.setVisibility(View.GONE);
				lv_type.setVisibility(View.GONE);
				iv_ending.setVisibility(View.GONE);
				lv_ending.setVisibility(View.GONE);
				iv_fee.setVisibility(View.GONE);
				lv_fee.setVisibility(View.GONE);

				iv_plan.setVisibility(View.VISIBLE);
				lv_plan.setVisibility(View.VISIBLE);
				break;

			default:
				break;
		}
	}

	private boolean	mIsSucceesed = false;
	// 获取合约计划信息
	private void showConstractInfo()
	{
		/*
		tv_package_info.setText(currentContract.proMoney
				+ "元合约:套包总价"
				+ currentContract.phoneMoney
				+ "元,含手机款"
				+ (currentContract.phoneMoney > currentContract.actMoney ? currentContract.phoneMoney
						- currentContract.actMoney : 0) + "元,含预存款" + currentContract.actMoney + "元");
		*/
		// 20131029 协议修改了 
		tv_package_info.setText(currentContract.planContent);
		// 20131009 aso
		if(currentContract.proID != mOldProID)
		{
			mOldProID = currentContract.proID;
			new Thread()
			{
				@Override
				public void run()
				{
					boolean bRelease = true;
					// 若之前已经有预占，先释放资源
					if(mIsSucceesed)
					{
						DialogUtil.showProgress("正在释放预占资源...");
						bRelease = haoka.bll0C16((byte)0x00);
					}
					if(bRelease == false)
					{
						DialogUtil.MsgBox("温馨提示", 
								"服务器释放资源失败，即将退出该流程！", 
								"确定", 
								new OnClickListener() 
								{
									@Override
									public void onClick(View v) 
									{
										finish();
									}
								}, 
								"", 
								null, 
								new OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										finish();
									}
								});
					}
					else
					{
						DialogUtil.showProgress("正在获取合约计划详情...");
						int ret = haoka.bll0C02(currentContract.timeLimit, currentContract.actType, currentContract.proID, currentContract.planID);
						if (ret != ReturnEnum.SUCCESS) 
						{
							DialogUtil.closeProgress();
							// 20131109  接入返回 1 或者超时 都当成终端串号预占失败
							if(haoka.getLastknownErrorCode() == 1 || ret == ReturnEnum.RECIEVE_TIMEOUT) 
							{
								String ss = "终端串号预占失败!";
								if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
								{
									ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
								}
								haoka.setLastknownError(ss);
								DialogUtil.MsgBox("温馨提示", haoka.getLastknownError(), "确定", 3, "", -1, 3, mHandler);
							}
							else
							{
								mIsSucceesed = true;
								// 查询套餐详情失败
								mHandler.sendEmptyMessage(1);
							}
						}
						else 
						{
							// 成功
							mHandler.sendEmptyMessage(0);
							mIsSucceesed = true;
						}
					}
				}
			}.start();
		}
		
		/*
		Map<String, String> packageInfo = dao.readPackageInfo(currentContract.planType, currentContract.proMoney);
		if (packageInfo != null)
		{
			tv_call_time.setText(packageInfo.get(PackageInfoDao.COLUMN_CALL_TIME));
			tv_flow1.setText(packageInfo.get(PackageInfoDao.COLUMN_FLOW1));
			tv_flow2.setText(packageInfo.get(PackageInfoDao.COLUMN_FLOW2));
			tv_video_call.setText(packageInfo.get(PackageInfoDao.COLUMN_VIDEO_CALL));
			tv_other.setText(packageInfo.get(PackageInfoDao.COLUMN_OTHER));
			tv_msg_count.setText(packageInfo.get(PackageInfoDao.COLUMN_MSG));
			tv_free.setText(packageInfo.get(PackageInfoDao.COLUMN_FREE_CALL));
			tv_call_fee.setText(packageInfo.get(PackageInfoDao.COLUMN_CALL_FEE));
		}

		if (currentContract.planType.equals("C"))
		{
			tv_label_local.setText("本地语音拨打分钟数");
		}
		else
		{
			tv_label_local.setText("国内语音拨打分钟数");
		}
		*/
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// 返回到上一界面
	private void backToPrevious()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				haoka.bll0C16();// 退出流程，释放资源 20131023
			}
		}.start();
		Bundle bundle = getIntent().getBundleExtra("_data");
		Message msg = new Message();
		bundle.putString("className", MobileIntroduceActivity.class.getName());
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
	}

	/*
	// 按钮单击监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.moblie_next:
				mbtnNext.onTouch(event);
				break;

			default:
				break;
		}
		return false;
	}
	*/

}
