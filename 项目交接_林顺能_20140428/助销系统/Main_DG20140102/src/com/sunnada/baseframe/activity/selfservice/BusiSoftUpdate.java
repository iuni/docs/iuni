package com.sunnada.baseframe.activity.selfservice;

import java.io.File;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.DgMainActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.Global;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.update.CheckVersionTask;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;

public class BusiSoftUpdate extends BaseActivity implements OnClickListener
{
	private static final String		TITLE_STR 		= "终端软件升级提示";
	
	private LinearLayout			mBacktoSelfService;	// 返回自服务
	private LinearLayout            mLinearLeft;
	private MyCustomButton		    mBtnSoftUpdate;
	
	private CheckVersionTask		mCheckVersion	= null;
	// 版本UI
	private TextView                mTvCurrVersion;
	
	private Handler	mHandler = new Handler()
	{
		public void handleMessage(Message message)
		{
			switch(message.what)
			{
			// 升级
			case Statics.MSG_UPDATE:
				// 下载
				new Thread() 
				{
					public void run() 
					{
						DialogUtil.showProgress("正在下载升级文件...");
						mCheckVersion.checkBreakDownLoad(Statics.PATH_APK+"/Main_DG.apk");
					}
				}.start();
				break;
				
			// 用户取消升级
			case Statics.MSG_UPDATE_CANCEL:
				if(Statics.IS_FORCED_UPDATE)
				{
					// 取消强制升级
					exitApp();
				}
				break; 
				
			// 正在升级
			case Statics.MSG_UPDATING:
				try
				{
					DialogUtil.showProgress(String.format("下载中(%d/%d KB)", 
							mCheckVersion.getFileCurrentLength()/1024, mCheckVersion.getFileTotalLength()/1024));
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				break;
			
			// 升级失败
			case Statics.MSG_UPDATE_FAILED:
				DialogUtil.closeProgress();
				DialogUtil.MsgBox(TITLE_STR, mCheckVersion.getLastErrorStr(), 
				"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
				break;
			
			// 升级成功
			case Statics.MSG_UPDATE_OK:
				DialogUtil.closeProgress();
				// 跳转到APK安装界面
				CheckVersionTask.installApkRequestCode(
						new File(Statics.PATH_APK+"/Main_DG.apk"), 
						BusiSoftUpdate.this,
						Statics.INSTALL_APK_REQUEST);
				break;
				
			// 软件安装失败
			case Statics.MSG_INSTALL_FAILED:
				if(Statics.IS_FORCED_UPDATE)
				{
					exitApp();
				}
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_soft_update_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_soft_update);
		}
		
		// 初始化布局
		initViews();
		// 初始化数据
		initData();
		// 初始化显示
		// initShow();
	}
	
	// 初始化布局
	private void initViews() 
	{
		mBacktoSelfService = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		mBacktoSelfService.setOnClickListener(this);
		
		// 左边布局
		mLinearLeft = (LinearLayout) findViewById(R.id.linear_left);
		
		// 版本UI
		mTvCurrVersion = (TextView) findViewById(R.id.tv_curr_version);
		
		// 更新按钮
		mBtnSoftUpdate = (MyCustomButton) findViewById(R.id.btn_softawre_update);
		mBtnSoftUpdate.setTextViewText1("终端软件升级");
		mBtnSoftUpdate.setOnClickListener(this);
		mBtnSoftUpdate.setOnTouchListener(this);
	}
	
	public void initData()
	{
		if(mCheckVersion == null)
		{
			mCheckVersion = new CheckVersionTask(BusiSoftUpdate.this, mHandler);
		}
		mTvCurrVersion.setText(mCheckVersion.getVersionName());
	}
	
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			ImageView mIvSoftUpdate = (ImageView) findViewById(R.id.iv_soft_update);
			mIvSoftUpdate.setImageDrawable(getResources().getDrawable(R.drawable.icon_soft_update_orange));
		
			ImageView ivReturn = (ImageView) findViewById(R.id.btn_return);
			ivReturn.setImageDrawable(getResources().getDrawable(R.drawable.icon_return_orange));
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onClick(View v) 
	{
		if(ButtonUtil.isFastDoubleClick(v.getId(), Global.CLICK_TIME))
		{
			return;
		}
		
		switch(v.getId())
		{
		// 终端软件升级
		case R.id.btn_softawre_update:
			handleSoftUpdate();
			break;
			
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
		
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	// 退出应用程序
	private void exitApp()
	{
		// 先跳转到主界面，清空栈，最后退出应用程序
		Intent intent = new Intent(this, DgMainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("isToExit", true);
		this.startActivity(intent);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Statics.INSTALL_APK_REQUEST)
		{
			if(resultCode == RESULT_CANCELED)
			{
				DialogUtil.MsgBox(TITLE_STR, "软件安装失败，请进行手动安装！\nAPK路径：SD卡/sunnada/apks/Main_DG.apk", 
						"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
			}
		}
	}
	
	// 处理终端软件升级
	private void handleSoftUpdate() 
	{
		new Thread()
		{
			public void run() 
			{
				// 检测升级
				if(mCheckVersion == null) 
				{
					mCheckVersion = new CheckVersionTask(BusiSoftUpdate.this, mHandler);
				}
				// checkupdate的参数为false代表"不需要升级时不提示任何信息"
				DialogUtil.showProgress("正在检测更新...");
				if(mCheckVersion.checkupdate(true) == false) 
				{
					// 关闭进度条
					DialogUtil.closeProgress();
				}
				else
				{
					mTvCurrVersion.post(new Runnable()
					{
						@Override
						public void run()
						{
							mTvCurrVersion.setText(mCheckVersion.getVersionName());
						}
					});
				}
			}
		}.start();
	}
}
