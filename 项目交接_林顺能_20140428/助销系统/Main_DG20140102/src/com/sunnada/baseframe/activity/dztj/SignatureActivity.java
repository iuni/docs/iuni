package com.sunnada.baseframe.activity.dztj;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.business.Business_0E;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.DialogUtil.OnPasswordGetListener;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.signature.WritePadDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;

public class SignatureActivity extends DgActivity implements OnClickListener
{
	private static final 	String	TITLE_STR			= "温馨提示";
	private View			mLayBack;	
	private Button			mBtnPreVious;
	private WritePadDialog	dialog;					// 签名Dialog
	private ImageView		iv_sign			= null;
	private TextView		tv_content		= null;
	private MyCustomButton	btn_cancel		= null;
	private MyCustomButton	moblie_next		= null;
	private Business_0E		mHaoka			= null;
	//private PackageInfoDao	dao			= null;

	private StringBuffer	sPrintBuffer	= null;
	//private File[]		files			= null; // 要上传的图片
	//private ImgLogDBAdapter imgLogAdapter	= null;
	// private CommonDialog cdialog = null;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_signature);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_signature_orange);
		}
		mHaoka = (Business_0E) FrameActivity.mBaseBusiness;
		//dao = new PackageInfoDao(this);
		//imgLogAdapter = new ImgLogDBAdapter(this);
		// 初始化控件
		init();
	}

	// 初始化控件
	private void init()
	{
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPreVious = (Button) findViewById(R.id.btn_previous);
		mBtnPreVious.setOnClickListener(this);

		moblie_next = (MyCustomButton) findViewById(R.id.moblie_next);
		moblie_next.setTextViewText1("完      成");
		moblie_next.setImageResource(R.drawable.btn_custom_check);
		moblie_next.setOnTouchListener(this);
		moblie_next.setOnClickListener(this);

		btn_cancel = (MyCustomButton) findViewById(R.id.btn_cancel);
		btn_cancel.setTextViewText1("退      出");
		btn_cancel.setImageResource(R.drawable.quit);
		btn_cancel.setOnTouchListener(this);
		btn_cancel.setOnClickListener(this);
		tv_content = (TextView) findViewById(R.id.tv_content);
		iv_sign = (ImageView) findViewById(R.id.iv_sign);
		iv_sign.setOnClickListener(this);
	}
	

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			// 返回
			case R.id.layBack:
			case R.id.btn_previous:
			case R.id.btn_cancel:	
				exit();
				break;
			
			// 下一步
			case R.id.moblie_next:
				DialogUtil.MsgBox(TITLE_STR, "确认提交吗？", "确定", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if(Statics.HAD_PWD)
						{
							showPwdDialog();
						}
						else
						{
							if(Statics.MODIFY_TYPE == 1)
							{
								DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
								"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
							}
							else if(Statics.MODIFY_TYPE == 2)
							{
								DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
										"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
							}
						}
					}
				}, "取消", null, null);
				break;
				
			// 签名
			case R.id.iv_sign:
				if (dialog == null)
				{
					dialog = new WritePadDialog(SignatureActivity.this);
					dialog.setOnBitmapGetListener(new WritePadDialog.OnBitmapGetListener()
					{
						@Override
						public void onBitmapGet(Bitmap bitmap)
						{
							// Bitmap newBitmap = BitmapUtil.rotate(bitmap,
							// 0,false); //这里不能回收原图,否则重新签名时会出错
							iv_sign.setImageBitmap(bitmap);
							iv_sign.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_signatured));
							// int w = newBitmap.getWidth();
							// int h = newBitmap.getHeight();
							// Log.i("signature", "w:"+w+" h:"+h);
						}
					});
				}
				dialog.show();
				break;
			default:
				break;
		}
	}
	
	// 显示密码输入框
	private void showPwdDialog()
	{
		DialogUtil.showPwdDialog(new OnPasswordGetListener()
		{
			@Override
			public void onPasswordGet(String password)
			{
				if(password == null || password.trim().length() != 6)
				{
					showErrorMsg("请输入6位密码！");
					showPwdDialog();
					return;
				}
				checkPwd(password);
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			exit();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
	}

	// 退出
	private void exit()
	{
		DialogUtil.MsgBox(TITLE_STR, Statics.EXIT_TIP, 
		"确定", new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new Thread()
				{
					@Override
					public void run()
					{
						// 通知服务器释放资源
						mHaoka.bll0E16();
					}
				}.start();
				Message msg = new Message();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
				
			}
		}, "取消", null , null);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");
	
		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		ProductInfo contract = (ProductInfo) bundle.getSerializable("package");
		//String name = bundle.getString("name");
		//String id_no = bundle.getString("id_no");
		//String number = bundle.getString("num");
		sPrintBuffer = new StringBuffer();
		
		sPrintBuffer.append("业务类型：3G号卡销售\n")
					.append("【费用信息】\n")
					.append(Business_0E.mStrFeeDetail)
					.append("【用户信息】\n")
					.append("客户名称:")
					.append(Business_0E.mStrIdcardName)
					.append("\n")
					.append("证件号码:")
					.append(Business_0E.mStrIdcardNo)
					.append("\n")
					.append("证件地址:")
					.append(Business_0E.mStrIdcardAddr)
					.append("\n")
					.append("【套餐名称】\n")
					.append(contract.getmProductName())
					.append("\n")
					.append("【套餐信息】\n")
					.append(StringUtil.isEmptyOrNull(contract.getPackDetails()) ? "无套餐信息":contract.getPackDetails())
					.append("\n")
					.append("【活动类型】\n")
					.append(StringUtil.isEmptyOrNull(contract.getmActivityContent())?"无活动信息":contract.getmActivityContent())
					.append("\n")
					.append("【合约计划名称】\n")
					.append(StringUtil.isEmptyOrNull(contract.getmContractName())?"无合约计划信息":contract.getmContractName())
					.append("\n")
					.append("【合约计划详情】\n")
					.append(StringUtil.isEmptyOrNull(Business_0E.mContractContent)?"无此详情":Business_0E.mContractContent);
		tv_content.setText(sPrintBuffer.toString());
		if (dialog != null)
		{
			dialog.clear();
		}}

	private void checkPwd(final String pwd)
	{
		mHaoka.mPassword = pwd;
		DialogUtil.showProgress("正在提交,请稍候...");
		new Thread()
		{
			public void run()
			{
				boolean ret = false;
				ret = mHaoka.doOpenCard(mHandler);
				if (!ret)
				{
					DialogUtil.closeProgress();
					if(mHaoka.getLastknownErrorCode() == -4)
					{
						String ss = "开户失败!\n失败原因: [密码错误]";
						DialogUtil.MsgBox(TITLE_STR, ss, "重试", Statics.MSG_NOTIFY, "放弃", Statics.MSG_FINISH, mHandler);
					}
					else
					{
						String ss = "开户失败!\n失败原因: [" + mHaoka.getLastknownError()+ "]";
						DialogUtil.MsgBox(TITLE_STR, ss, "确定", Statics.MSG_FINISH, "", 0, Statics.MSG_FINISH, mHandler);
						mHaoka.bll0E16();
					}
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox(TITLE_STR, "开户成功,是否打印凭条？", 
							"打印", Statics.MSG_PRINT, "取消", Statics.MSG_FINISH, Statics.MSG_FINISH, mHandler);
						}
					});
				}
			}
		}.start();
	}
	
	// 打印失败
	/*
	private void printFail()
	{
		DialogUtil.MsgBox(TITLE_STR, "打印失败。\n失败原因:[" + equipmentService.getPrintErrorMsg() + "]",
		"确定" , Statics.MSG_REPRINT, "", 0, Statics.MSG_REPRINT, mHandler);
	};
	*/
	
	// 重新打印
	private void showRePrintDialog()
	{
		SignatureActivity.this.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				DialogUtil.closeProgress();
				DialogUtil.MsgBox(PrintUtil.STR_FAIL_TITLE, 
								  PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()), 
								  PrintUtil.STR_FAIL_OK,
								  Statics.MSG_PRINT,
								  PrintUtil.STR_FAIL_CANCEL,
						          Statics.MSG_FINISH, 
						          Statics.MSG_FINISH, 
						          mHandler);
			}
		});
	}
	
	@Override
	public void onInitSuccess()
	{

	}

	@Override
	public void onInitFail()
	{

	}
	
	private Handler mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				// 密码对话框
				case Statics.MSG_NOTIFY:
					showPwdDialog();
					break;
				
				// 打印
				case Statics.MSG_PRINT:
					new Thread() 
					{
						public void run() 
						{
							// 检测蓝牙设备是否存在
							DialogUtil.showProgress("正在检测蓝牙设备...");
							if(equipmentService.handshake() == false) 
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox("请连接蓝牙设备", 
										"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
										"立即搜索", 
										Statics.MSG_CONNECT_BLUETOOTH, 
										"取消", 
										Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
										Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
										mHandler);
								return;
							}
							mHandler.sendEmptyMessage(Statics.MSG_CONNECT_DONE);
						};
					}.start();
					break;
					
				// 连接蓝牙
				case Statics.MSG_CONNECT_BLUETOOTH:
					SearchBluetoothDialog dialog = new SearchBluetoothDialog(SignatureActivity.this, mBluetoothClient, equipmentService, 
							mHandler, Statics.MSG_CONNECT_DONE);
					dialog.show();
					break; 
				
				// 打印
				case Statics.MSG_CONNECT_DONE:
					new Thread()
					{
						@Override
						public void run()
						{
							// 检测蓝牙设备是否存在
							DialogUtil.showProgress("正在检测蓝牙设备...");
							if(equipmentService.handshake() == false) 
							{
								DialogUtil.closeProgress();
								mHandler.sendEmptyMessage(Statics.MSG_CONNECT_BLUETOOTH_CANCEL);
								//DialogUtil.MsgBox("温馨提示", "打印失败。\n失败原因[蓝牙设备连接失败]", 
								//"确定", Statics.MSG_REPRINT, "", 0, Statics.MSG_REPRINT, mHandler);
							}
							else
							{
								DialogUtil.showProgress("正在打印,请稍候...");
								String strPrint;
								if(!StringUtil.isEmptyOrNull(mHaoka.mTranceTime));
								{
									strPrint = sPrintBuffer.toString() + "\n【交易时间】\n"+mHaoka.mTranceTime;
								}
								mHaoka.print(new StringBuffer(strPrint), mHaoka.mStrBllnum);
								if(!equipmentService.isPrintSuccess())
								{
									// 提示是否重新打印
									showRePrintDialog();
								}
								else
								{
									DialogUtil.closeProgress();
									DialogUtil.MsgBox(PrintUtil.STR_SUCCESS_TITLE, 
											  PrintUtil.STR_SUCCESS_MSG, 
											  PrintUtil.STR_SUCCESS_OK,
											  Statics.MSG_FINISH,
											  "",
									          0, 
									          Statics.MSG_FINISH, 
									          mHandler);
								}
							}
						}
					}.start();
					break;
				
				// 重新打印
				case Statics.MSG_REPRINT:
					showRePrintDialog();
					break;
					
				// 初始化交易密码
				case Statics.MSG_MODIFY_PSWD:
					ModifyPswdDialog dialog1 = new ModifyPswdDialog(SignatureActivity.this, 
					new Business_00(SignatureActivity.this, equipmentService, socketService, dataBaseService), mHandler);
					dialog1.show();
					break;
					
				// 重置交易密码
				case Statics.MSG_RESET_PSWD:
					ResetSrvDialog dialog2 = new ResetSrvDialog(SignatureActivity.this, 
					new Business_00(SignatureActivity.this, equipmentService, socketService, dataBaseService), mHandler);
					dialog2.show();
					break;
					
				// 交易密码修改成功
				case Statics.MSG_MODIFY_PSWD_SUCCESS:
					showPwdDialog();
					break;
					
				// 退出
				// 取消连接蓝牙
				case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				case Statics.MSG_FINISH:
					new Thread()
					{
						@Override
						public void run()
						{
							mHaoka.bll0E16();
						}
					}.start();
					msg = new Message();
					msg.what = FrameActivity.FINISH;
					FrameActivity.mHandler.sendMessage(msg);
					break;
				
				default:
					break;
			}
		}
	};
	/*
	private void afterOpenCard()
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				DialogUtil.showProgress("正在上传图片");
			}
		});

		// 开卡成功!此时处理照片上传的事件
		try
		{
			String bllNum = "";
			if (LaunchActivity.isDebuge)
			{
				// bllNum = debugBllNum;
			}
			else
			{
				bllNum = haoka.sBllnum; // 流水号
			}
			// 此时文件已经被转移到流水号目录下了
			File jpgFolder = new File(Environment.getExternalStorageDirectory().toString()
					+ this.getString(R.string.identify_photo_path) + haoka.sPicFolder);
			File newFolder = new File(Environment.getExternalStorageDirectory().toString()
					+ this.getString(R.string.identify_photo_path) + bllNum);
			jpgFolder.renameTo(newFolder); // 这里把时间戳的文件名更改为流水号的文件名
			// Log.i("imageUploader", "照片目录:" + jpgFolder);
			if (jpgFolder != null && jpgFolder.exists())
			{
				//
				// if (info != null) { // 使用识别模式
				// // 保存本地身份证读取信息的二进制文件
				// File binFile = new File(
				// Environment.getExternalStorageDirectory()
				// .toString()
				// + main_activity
				// .getString(R.string.identify_photo_path)
				// + bllNum
				// + "/"
				// + bllNum
				// + "_"
				// + idCardLength[0] + ".bin");
				// // 把身份证信息写入文件
				// FileOpr.writeFile(binFile, idCardData);
				// }

				files = jpgFolder.listFiles();

				int photoCount = files.length;
				StringBuffer fileName = new StringBuffer(); // 图片名的拼接

				// 拼装所需要的各种参数
				for (int i = 0; i < files.length; i++)
				{
					if (!files[i].getName().endsWith(".jpg"))
					{ // 跳过bin文件
						continue;
					}
					if (i != 0)
					{
						fileName.append(",");
					}
					fileName.append(files[i].getName());
				}
				// Log.i("imageUploader", "照片文件:" + fileName);
				uploadImgs();
				updateImgLogInfo(bllNum, photoCount, fileName.toString());
				files = null;
			}
		}
		catch (Exception e)
		{

			e.printStackTrace();
		}
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				DialogUtil.closeProgress();
			}
		});
	}
	*/

	/*
	// 把要上传的文件路径等加入到本地数据库,等待上传
	private void uploadImgs()
	{
		final String textMark = "仅供中国联通开户专用";
		String psamId = "";
		if (LaunchActivity.isDebuge)
		{
			psamId = "51000000000001";
		}
		else
		{
			try
			{
				psamId = equipmentService.getPsamId();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		// psamId="51000000000001";
		StringBuffer remotePath = new StringBuffer(getString(R.string.ftp_image_upload_folder)); // 要提交的目录
		remotePath.append("/" + psamId.substring(0, 2));
		remotePath.append("/" + psamId.substring(2, 4));
		remotePath.append("/" + psamId);
		if (LaunchActivity.isDebuge)
		{
			remotePath.append("/" + "201304301111222233334444" + (int) (Math.random() * 899999 + 100000)); // 目录是
			// "/Photos/省份/地市/psamid/流水号"
			// 的格式
		}
		else
		{
			remotePath.append("/" + haoka.sBllnum); // 目录是
													// "/Photos/省份/地市/psamid/流水号"
													// 的格式
		}

		// Log.i("PHOTO_UPLOAD", "照片提交目录:" + remotePath);
		for (int i = 0; i < files.length; i++)
		{
			File img = files[i];
			if (img.isFile() && img.getName().endsWith(".jpg"))
			{ // 图像文件
				try
				{
					PictureShowUtils.addTextMark(img.getAbsolutePath(), textMark); // 身份证照片打水印
					imgLogAdapter.addUploadLog(img.getAbsolutePath(), remotePath + "/" + img.getName());
					// Log.i("PHOTO_UPLOAD", img.getName() + "照片插入数据库成功");
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}

			}
			else if (img.isFile() && img.getName().endsWith(".bin"))// 身份证识别
			{ 
				 // 导购系统暂时不会用到这一块代码
				 //try { imgLogAdapter .addUploadLog( img.getAbsolutePath(),
				 //getString(R.string.ftp_image_upload_folder) + "/idfile/" +
				 //img.getName()); // Log.i("PHOTO_UPLOAD", img.getName() +
				 //"bin文件插入数据库成功"); } catch (Exception e1) {
				 //e1.printStackTrace(); }
			}
		}
		System.out.println("上传信息保存完毕");
	}
	*/
	
	/*
	// 本地缓存并上传照片日志信息
	private void updateImgLogInfo(final String bllNum, final int photoCount, final String fileName)
	{
		synchronized (this)
		{
			// 本地缓存
			imgLogAdapter.addImgLog(bllNum, String.valueOf(photoCount), fileName);

			ImgLogReport report = new ImgLogReport(this, equipmentService, socketService, dataBaseService);
			// Log.i("PHOTO_UPLOAD", "照片日志上传:" + bllNum + "--" + photoCount +
			// "张:" + fileName);

			int flag = 0;
			if (LaunchActivity.isDebuge)
			{
				flag = ReturnEnum.SUCCESS;
			}
			else
			{
				flag = report.bll0F80(bllNum, photoCount, fileName);
			}
			System.out.println("信息上报:" + flag);
			if (flag == ReturnEnum.SUCCESS)
			{
				imgLogAdapter.deleteImgLog(bllNum);
				// Log.i("PHOTO_UPLOAD", "照片日志删除:" + result);
			}
		}
	}
	*/
}
