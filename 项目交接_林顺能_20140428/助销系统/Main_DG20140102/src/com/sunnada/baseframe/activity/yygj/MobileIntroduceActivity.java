package com.sunnada.baseframe.activity.yygj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.HistoryModelAdapter;
import com.sunnada.baseframe.adapter.ImageAdapter;
import com.sunnada.baseframe.adapter.MeauListAdapter;
import com.sunnada.baseframe.bean.ConstantData;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.database.HistoryPhoneYhgjDao;
import com.sunnada.baseframe.database.PhoneParamsDao;
import com.sunnada.baseframe.dialog.DetailParamDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ImageBrowseDialog;
import com.sunnada.baseframe.dialog.PhoneCompareDialog;
import com.sunnada.baseframe.ui.GalleryFlow;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.CollectionUtil;
import com.sunnada.baseframe.util.HttpURLConnectionUtil;
import com.sunnada.baseframe.util.StringUtil;

public class MobileIntroduceActivity extends DgActivity implements OnClickListener, OnTouchListener
{
	private static final 					String	TITLE_STR	= "优惠购机业务办理提示";
	public static int						MATCH_PARENT		= LinearLayout.LayoutParams.MATCH_PARENT;
	public static int						WRAP_CONTENT		= LinearLayout.LayoutParams.WRAP_CONTENT;
	private Business_0C						mHaoka				= null;
	private PhoneParamsDao					mPhoneParamsDao		= null;
	
	// 主界面
	private GalleryFlow						mGallery;								// 自定义画廊
	private View							mLayBack;								// 返回
	private Button 							mBtnPrevious;							// 返回按钮
	private ImageAdapter					mImageAdapter;
	private Timer							mTimer				= new Timer();
	private Timer							mTipTimer 			= new Timer();
	private RelativeLayout					mLayShadowCover		= null;				// 长按时覆盖整个屏幕的阴影
	private ImageView						mIvDrag				= null;				// 长按时显示的可拖动的手机小图片
	private int								mIvDrag_x			= -1;									
	private int								mIvDrag_y			= -1;									
	private ImageView						mIvNoModel			= null;				// 没有相应的手机预览时显示的图片
	private boolean							mIsImageDraging		= false;			// 是否处于拖动状态
	private float							mLastx;									// 拖动时记录x的值
	private float							mLasty;									// 拖动时记录y的值
	private RelativeLayout					mLayCompare			= null;				// 准备进行对比手机总
	private Button							mBtnCompare			= null;				// 对比按钮
	// private LinearLayout 				mLayCompareShow 	= null; 			// 准备进行对比手机1
	private LinearLayout					mLayCompare1		= null;				// 准备进行对比手机1
	private LinearLayout					mLayCompare2		= null;				// 准备进行对比手机2
	private LinearLayout					mLayCompare3		= null;				// 准备进行对比手机3
	private Button							mBtnCompareClose1	= null;				// 删除已选对比手机1
	private Button							mBtnCompareClose2	= null;				// 删除已选对比手机2
	private Button							mBtnCompareClose3	= null;				// 删除已选对比手机3
	// private int 							mLayCompareShow_x 	= -1;
	// private int 							mLayCompareShow_y 	= -1; 
	private int								mLayCompare1_x		= -1;				// 准备进行对比手机1在屏幕上的x坐标
	private int								mLayCompare1_y		= -1;				// 准备进行对比手机1在屏幕上的y坐标
	private int								mLayCompare2_x		= -1;				// 准备进行对比手机2在屏幕上的x坐标
	private int								mLayCompare2_y		= -1;				// 准备进行对比手机2在屏幕上的y坐标
	private int								mLayCompare3_x		= -1;				// 准备进行对比手机3在屏幕上的x坐标
	private int								mLayCompare3_y		= -1;				// 准备进行对比手机3在屏幕上的y坐标
	
	private RelativeLayout					mLayModelTips		= null;				// 手机信息
	private Animation						mModelTipAnm		= null;				// 手机部分信息提示动画
	private TextView						mTvTipName			= null;				// 手机名称
	private TextView						mTvTipOp			= null;				// 手机操作系统
	private TextView						mTvTipPrice			= null;				// 手机价格
	private TextView						mTvTipScreen		= null;				// 手机屏幕信息
	private List<PhoneModel>				mModelData			= null;
	private PhoneModel						mCurrentModel		= null;
	private PhoneModel[]					mCompareModels		= new PhoneModel[3];
		
	// 手机详细信息
	// private LinearLayout lay_detail = null; 
	private ImageView						mIvDetail			= null;				// 手机详情-图片
	private TextView						mTvDetailTitle;							// 手机详情-标题
	private TextView						mTvDetailNet;							// 手机详情-网络模式
	private TextView						mTvDetailScreen;						// 手机详情-屏幕尺寸信息
	private TextView						mTvDetailCpu;							// 手机详情-CPU主频
	private TextView						mTvDetailPrice;							// 手机详情-价格
	//private Button						mBtnDetail			= null;				// 手机详情-详细参数按钮
	//private Button						mBtnAddCompare		= null;				// 手机详情-进行对比按钮
	//private Button						mBtnPurchase		= null;				// 手机详情-立即购买按钮
	private LinearLayout					mLayRecommened_1	= null;				// 可能喜欢手机1
	private LinearLayout					mLayRecommened_2	= null;				// 可能喜欢手机2
	private LinearLayout					mLayRecommened_3	= null;				// 可能喜欢手机3
	//private Button						mBtnDetailClose		= null;				// 手机详情-关闭详情
	private PhoneModel						mDetailCurrModel	= null;				// 手机详情当前显示机型

	private List<PhoneModel>				mRecommenedModels	= null;
	
	// 历史浏览菜单
	private Button							mBtnHistory;							// 历史浏览按钮
	private Button							mBtnHistory_hide;						// 关闭历史浏览按钮
	private Button							mBtnClearHistory;		
	private ListView						mLvHistory;								// 历史浏览记录
	private List<PhoneModel>				mListHistoryModel;
	private HistoryPhoneYhgjDao				mHistoryDao;							// 历史浏览数据库
	private BaseAdapter						mHistoryAdapter;
	private TextView 						mTvNoHistory;
	
	// 筛选
	private LinearLayout layListone; 
	// private MeauListAdapter mltAdapter;
	private MeauListAdapter					mLoAdapter;
	private LinearLayout					mLayListtwo;							// 点击筛选弹出的菜单，二级菜单
	private ListView						mLvOne;
	private ListView						mLvTwo;
	public int								mFirListOldPosition	= -1;
	private View							mFirOldItem			= null;
	public int								mSecListOldPosition	= -1;
	private View							mSecListOldItem		= null;
	private List<? extends Map<String, ?>>	mListone;
	private List<? extends Map<String, ?>>	mListtwo;
	private boolean							mLaytwoishow		= false;			// 二级菜单是否显示
	private boolean							mIsFirstIn			= true;
	private Button							mBtnModelQuery		= null;				// 筛选按钮
	
	// 筛选条件提示
	private Map<String, String>				mConditionMap;							// 查询条件数字
	private Map<Integer, String>			mStrConditionMap;						// 查询条件
	private TableRow						mTableRow;
	private TableLayout						mTableLayout;
	private ImageButton						mIbCloseConditon;						// 关闭筛选对话框
	private ImageButton						mIbClearCondition;						// 清除筛选条件提示
	private View							mLayConditionTip;						// 筛选条件提示界面
	private int[]							mCondition;
	private List<PhoneModel>				mRelativePhones;
	
	// 搜索
	// private RelativeLayout mLaySearch;
	private Button							mBtnToSearch;
	private String[]						mStrIndicatorArr;						// 风向标数组
	private LinearLayout					mLayFasters;
	private EditText						mEtSearch;
	private Button 							mBtnSearch;
	
	private LinearLayout					mLayTips;								// 模糊搜索条件提示
	private View							mLine;									// 模糊搜索条件提示分割线
	private ListView						mListDimSearch;							// 模糊查询下拉列表
	private Button							mBtnOldIndic;							// 上次选中的风向标
	private View							mOldSelectItem;							// 上次选中的模糊查询提示
	
	// popWindow
	private View							mParent;
	private PopupWindow						mPopHistory, mPopChoose, mPopDetails, mPopSearch;
	private LayoutInflater					mInflater;
	
	//private Animation						mAnim_left_out;							// 动画左出
	//private Animation						mAnim_left_in;							// 动画左进
	//private Animation						mAnim_right_out;						// 动画右出
	//private Animation						mAnim_right_in;							// 动画右进
	//private Animation						mAnim_bottom_out;						// 动画从底部进
	//private Animation						mAnim_bottom_in;						// 动画从底部出
	//private int 							lastId = -1;
	//private Thread 						thread1;
	//private Timer 						anm_timer = null;
	//private Animation 					anm_image = null;
	//private String 						lastModelId = null;
	private float							mOrigX;
	private float							mOrigY;
	private boolean							mIsGalleryOnMove = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mPhoneParamsDao = new PhoneParamsDao(getBaseContext());
		if (CollectionUtil.isEmptyOrNull(ConstantData.modelList))  
		{
			ConstantData.modelList = mPhoneParamsDao.getRelativePhones("");
			if (CollectionUtil.isEmptyOrNull(ConstantData.modelList))  
			{
				DialogUtil.MsgBox(TITLE_STR, 
				"机型未下发，请联系管理员！", 
				"确定", 
				new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
					}
				}, 
				"", 
				null, 
				new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
					}
				});
				return;
			}
		}
		
		Log.i("Display", "加载布局: " + System.currentTimeMillis());
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.mobileintroduceactivity_blue);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.mobileintroduceactivity_orange);
		}
		
		//DialogUtil.showProgress("正在初始化，请稍候...");
		//mHandler.sendEmptyMessageDelayed(0x01, 200);
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在初始化, 请稍候...");
				SystemClock.sleep(100);
				// 异常交易检测
				if(errorDealCheck(new String[] {"09"}) == false) 
				{
					// 初始化界面
					mHandler.sendEmptyMessage(0x01);
				}
			}
		}.start();
	}
	
	// 异常交易处理完成的回调
	@Override
	protected void errorDealCheckCallBack() 
	{
		// 初始化界面
		mHandler.sendEmptyMessage(0x01);
	}
	
	// 消息处理器
	private Handler mHandler = new Handler() 
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch(msg.what) 
			{
			case 0x01:
				mHaoka = new Business_0C(MobileIntroduceActivity.this, equipmentService, socketService, dataBaseService);
				mHaoka.state = true;
				FrameActivity.mBaseBusiness = mHaoka;
				
				// 初始控件
				initView();
				// 历史浏览数据库操作
				mHistoryDao = new HistoryPhoneYhgjDao(MobileIntroduceActivity.this);
				mHistoryDao.clear();
				mListHistoryModel = new ArrayList<PhoneModel>();
				mModelData = ConstantData.modelList;
				// 初始化浏览记录
				updateHistory();
				// 展示机型
				mHandler.sendEmptyMessage(0x02);
				break;
				
			case 0x02:
				// 展示所有的机型
				Log.i("Display", "展示机型1: " + System.currentTimeMillis());
				displayModels(mModelData);
				Log.i("Display", "展示机型2: " + System.currentTimeMillis());
				DialogUtil.closeProgress();
				break;
			}
		}
	};
	
	// 注册监听事件
	public void initView() 
	{
		mParent = findViewById(R.id.parent);
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		mInflater = LayoutInflater.from(this);
		// 初始化机型选择界面
		initGalleryFlow();
		// 初始化ImageDrag
		initImageDrag();
		// 历史浏览
		initPopHistory();
		// 筛选
		initPopChoose();
		// 详情
		initPopDetails();
		// 搜索
		initPopSearch();
		// 机型对比布局
		initCompareLay();
	}

	// 机型对比布局
	private void initCompareLay()
	{
		mIvNoModel = (ImageView) findViewById(R.id.iv_no_model);
		// 对比框总
		mLayCompare = (RelativeLayout) findViewById(R.id.lay_compare);
		/*
		mLayCompare.setVisibility(View.GONE); //对比框 初始显示
		mLayCompareShow = (LinearLayout) findViewById(R.id.lay_compare_show);
		mLayCompareShow.setBackgroundResource(R.drawable.bg_compare_none);
		mLayCompareShow.setVisibility(View.VISIBLE);
		*/
		// 对比框1
		mLayCompare1 = (LinearLayout) findViewById(R.id.lay_compare1);
		mLayCompare1.setOnClickListener(this);
		// 对比框2
		mLayCompare2 = (LinearLayout) findViewById(R.id.lay_compare2);
		mLayCompare2.setOnClickListener(this);
		// 对比框3
		mLayCompare3 = (LinearLayout) findViewById(R.id.lay_compare3);
		mLayCompare3.setOnClickListener(this);
		// 清除对比框1的内容
		mBtnCompareClose1 = (Button) findViewById(R.id.btn_compare_close1);
		mBtnCompareClose1.setOnClickListener(this);
		// 清除对比框2的内容
		mBtnCompareClose2 = (Button) findViewById(R.id.btn_compare_close2);
		mBtnCompareClose2.setOnClickListener(this);
		// 清除对比框3的内容
		mBtnCompareClose3 = (Button) findViewById(R.id.btn_compare_close3);
		mBtnCompareClose3.setOnClickListener(this);
		// 对比
		mBtnCompare = (Button) findViewById(R.id.btn_compare);
		mBtnCompare.setOnClickListener(this);
		// 筛选
		mBtnModelQuery = (Button) findViewById(R.id.btn_model_query);
		mBtnModelQuery.setOnClickListener(this);

		// 手机信息布局
		mLayModelTips = (RelativeLayout) findViewById(R.id.lay_model_tips);
		// 手机名称
		mTvTipName = (TextView) findViewById(R.id.tv_tip_name);
		// 手机操作系统
		mTvTipOp = (TextView) findViewById(R.id.tv_tip_op);
		// 手机价格
		mTvTipPrice = (TextView) findViewById(R.id.tv_tip_price);
		// 手机屏幕
		mTvTipScreen = (TextView) findViewById(R.id.tv_tip_screen);
		// 长按时显示 覆盖屏幕
		mLayShadowCover = (RelativeLayout) findViewById(R.id.lay_show_cover);
		mLayShadowCover.setOnTouchListener(this);
		// 部分手机参数信息动画
		mModelTipAnm = new AlphaAnimation(0, 1f);
		mModelTipAnm.setDuration(400);
	}

	// 搜索
	private void initPopSearch()
	{
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			 contentView = mInflater.inflate(R.layout.pop_search, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			 contentView = mInflater.inflate(R.layout.pop_search_orange, null);
		}
		mPopSearch = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mPopSearch.setFocusable(true);
		mPopSearch.setTouchable(true);
		mPopSearch.setBackgroundDrawable(new BitmapDrawable());
		// setTouchListenner(contentView, mPopSearch);
		mPopSearch.setAnimationStyle(R.style.AnimationUp);
		mPopSearch.setOnDismissListener(new OnDismissListener()
		{
			@Override
			public void onDismiss() 
		 	{
				//inputMethodManager.hideSoftInputFromWindow(mEtSearch.getApplicationWindowToken(), 0);
				// 关闭输入法
				//((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
				//.hideSoftInputFromWindow(contentView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				
				InputMethodManager imm = (InputMethodManager) (MobileIntroduceActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE));
				imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
				/*
				int flags = getWindow().getAttributes().flags;
				getWindow().addFlags(flags|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
				mBtnToSearch.clearFocus();
				mEtSearch.requestFocus();
				InputMethodManager mInputMethod = (InputMethodManager) MobileIntroduceActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
				mInputMethod.hideSoftInputFromInputMethod(mEtSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				*/
			}
		});

		// 搜索
		// mLaySearch = (RelativeLayout)
		// contentView.findViewById(R.id.lay_search);
		mBtnSearch = (Button) contentView.findViewById(R.id.btnSearch);
		mLayFasters = (LinearLayout) contentView.findViewById(R.id.layButtonFaster);
		mEtSearch = (EditText) contentView.findViewById(R.id.etSearch);
		mListDimSearch = (ListView) contentView.findViewById(R.id.lvDimSerch);
		mLayTips = (LinearLayout) contentView.findViewById(R.id.layTips);
		mLine = contentView.findViewById(R.id.line);
		// 初始化风向标界面
		indicatorInit();
		setListenner();
	}

	// 手机详情
	private void initPopDetails()
	{
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView = mInflater.inflate(R.layout.pop_details, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = mInflater.inflate(R.layout.pop_details_orange, null);
		}
		mPopDetails = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mPopDetails.setFocusable(true);
		mPopDetails.setBackgroundDrawable(new BitmapDrawable());
		mPopDetails.setAnimationStyle(R.style.AnimationDown);

		// 手机详情布局
		// lay_detail = (LinearLayout)
		// contentView.findViewById(R.id.lay_detail);
		// 手机详情-图片
		mIvDetail = (ImageView) contentView.findViewById(R.id.iv_detail);
		mIvDetail.setOnClickListener(this);
		// 手机详情-标题
		mTvDetailTitle = (TextView) contentView.findViewById(R.id.tv_detail_name);
		// 手机详情-网络模式
		mTvDetailNet = (TextView) contentView.findViewById(R.id.tv_detail_net);
		// 手机详情-主屏尺寸
		mTvDetailScreen = (TextView) contentView.findViewById(R.id.tv_detail_sreen);
		// 手机详情-CPU主频
		mTvDetailCpu = (TextView) contentView.findViewById(R.id.tv_detail_cpu);
		// 手机详情-操作系统
		mTvDetailPrice = (TextView) contentView.findViewById(R.id.tv_detail_price);
		
		// 手机详情-详细参数
		Button btnDetail = (Button) contentView.findViewById(R.id.btn_detail);
		btnDetail.setOnClickListener(this);
		// 手机详情-进行对比
		Button btnAddCompare = (Button) contentView.findViewById(R.id.btn_add_compare);
		btnAddCompare.setOnClickListener(this);
		// 手机详情-立即购买
		Button btnPurchase = (Button) contentView.findViewById(R.id.btn_purchase);
		btnPurchase.setOnClickListener(this);
		//
		// 手机详情-为你推荐1
		mLayRecommened_1 = (LinearLayout) contentView.findViewById(R.id.lay_recommened_1);
		mLayRecommened_1.setOnClickListener(this);
		// 手机详情-为你推荐2
		mLayRecommened_2 = (LinearLayout) contentView.findViewById(R.id.lay_recommened_2);
		mLayRecommened_2.setOnClickListener(this);
		// 手机详情-为你推荐3
		mLayRecommened_3 = (LinearLayout) contentView.findViewById(R.id.lay_recommened_3);
		mLayRecommened_3.setOnClickListener(this);
		// 手机详情-关闭按钮
		Button btnDetailClose = (Button) contentView.findViewById(R.id.btn_detail_close);
		btnDetailClose.setOnClickListener(this);
	}

	// 筛选菜单
	private void initPopChoose()
	{
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView = mInflater.inflate(R.layout.pop_choose, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = mInflater.inflate(R.layout.pop_choose_orange, null);
		}
		mPopChoose = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mPopChoose.setFocusable(true);
		mPopChoose.setOutsideTouchable(true);
		mPopChoose.setBackgroundDrawable(new BitmapDrawable());
		mPopChoose.setAnimationStyle(R.style.AnimationRight);
		setTouchListenner(contentView, mPopChoose);
		// 筛选页面<确定> 
		mBtnToSearch = (Button) contentView.findViewById(R.id.btnToSearch);
		Button bt_rightok = (Button) contentView.findViewById(R.id.bt_rightok);
		bt_rightok.setOnClickListener(this);
		// 筛选页面<清除条件>
		Button bt_rightclean = (Button) contentView.findViewById(R.id.bt_rightclean);
		bt_rightclean.setOnClickListener(this);
		// 筛选页面<关闭>
		// Button bt_rightclose = (Button)
		// contentView.findViewById(R.id.bt_rightclose);
		// bt_rightclose.setOnClickListener(this);

		layListone = (LinearLayout)contentView.findViewById(R.id.layListone);
		mLvOne = (ListView) contentView.findViewById(R.id.sxlistone);
		mLayListtwo = (LinearLayout) contentView.findViewById(R.id.layListtwo);
		mLvTwo = (ListView) contentView.findViewById(R.id.sxlisttwo);
		mConditionMap = new HashMap<String, String>();
		mStrConditionMap = new HashMap<Integer, String>();
		mIbClearCondition = (ImageButton) findViewById(R.id.ibClearCondition);
		mIbClearCondition.setOnClickListener(this);
		mLayConditionTip = findViewById(R.id.lay_condition);
		
		//mAnim_left_out = AnimationUtils.loadAnimation(this, R.anim.left_out);
		//mAnim_left_in  = AnimationUtils.loadAnimation(this, R.anim.left_in);
		//mAnim_right_out = AnimationUtils.loadAnimation(this, R.anim.right_out);
		//mAnim_right_in = AnimationUtils.loadAnimation(this, R.anim.right_in);
		//mAnim_bottom_in = AnimationUtils.loadAnimation(this, R.anim.bottom_in);
		//mAnim_bottom_out = AnimationUtils.loadAnimation(this, R.anim.bottom_out);
	}

	// 历史浏览
	private void initPopHistory()
	{
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView = mInflater.inflate(R.layout.pop_history, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			contentView = mInflater.inflate(R.layout.pop_history_orange, null);
		}
		mPopHistory = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		mPopHistory.setFocusable(true);
		mPopHistory.setTouchable(true);
		mPopHistory.setBackgroundDrawable(new BitmapDrawable());
		mPopHistory.setAnimationStyle(R.style.AnimationRight);
		//setTouchListenner(contentView, mPopHistory);

		mLvHistory = (ListView) contentView.findViewById(R.id.lv_history);
		// mLayHistory = (LinearLayout)
		// contentView.findViewById(R.id.lay_history);
		mBtnHistory = (Button) findViewById(R.id.btn_history);
		mBtnClearHistory = (Button) contentView.findViewById(R.id.btn_clear_history);
		mBtnClearHistory.setOnClickListener(this);
		mBtnHistory.setOnClickListener(this);
		mBtnHistory_hide = (Button) contentView.findViewById(R.id.btn_history_hide);
		mBtnHistory_hide.setOnClickListener(this);
		mTvNoHistory = (TextView) contentView.findViewById(R.id.tvNoHistory);
	}

	// 设置弹出框touch监听， 点击外部消失
	private void setTouchListenner(View contentView, final PopupWindow pop)
	{
		contentView.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int x = (int) event.getX();
				int y = (int) event.getY();
				int left = layListone.getLeft();
				int right = layListone.getRight();
				int top = layListone.getTop();
				int bottom = layListone.getBottom();
				Log.e("", "x :" + x + " y:" + y);
				Log.e("", "left :" + left + " right :" + right + " top :" + top + " bottom :" + bottom);
				if(x > left && x < right && y < bottom && y > top)
				{
					// 防止点击一级菜单空白处时关闭
					return false;
				}
				if (pop != null && pop.isShowing())
				{
					pop.dismiss();
				}
				return false;
			}
		});
	}

	// 注册监听
	private void setListenner()
	{
		mBtnToSearch.setOnClickListener(this);
		mBtnSearch.setOnClickListener(this);
		mEtSearch.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (s.length() > 0)
				{
					// 模糊查询
					List<String> relative = getRelativePhone(s.toString());
					if (relative != null && relative.size() > 0)
					{
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
								R.layout.search_list_tip_item, relative);
						mListDimSearch.setAdapter(adapter);
						mLayTips.setVisibility(View.VISIBLE);
						mLine.setVisibility(View.GONE);
						mLayFasters.setVisibility(View.GONE);
					}
					else
					{
						mListDimSearch.setAdapter(null);
						mLayTips.setVisibility(View.GONE);
						mLine.setVisibility(View.VISIBLE);
						mLayFasters.setVisibility(View.VISIBLE);
					}
				}
				else
				{ 
					mListDimSearch.setAdapter(null);
					mLayTips.setVisibility(View.GONE);
					mLine.setVisibility(View.VISIBLE);
					mLayFasters.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{

			}

			@Override
			public void afterTextChanged(Editable s)
			{

			}
		});

		mListDimSearch.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				view.setBackgroundDrawable(MobileIntroduceActivity.this.getResources().getDrawable(R.drawable.selected));
				if (mOldSelectItem != null)
				{
					mOldSelectItem.setBackgroundDrawable(null);
				}
				mOldSelectItem = view;
				mEtSearch.setText(((TextView) view).getText().toString());
				// 将光标移到文字末尾
				CharSequence text = mEtSearch.getText();
				if(text instanceof Spannable)
				{
					Spannable spanText = (Spannable) text;
					Selection.setSelection(spanText, text.length());
				}
			}
		});
	}

	// 初始化风向标
	public void indicatorInit()
	{
		// 从服务器获取风向标信息
		mEtSearch.setText("");
		mLayTips.setVisibility(View.GONE);
		mLayFasters.removeAllViews();
		getIndicatorFromServer();
		if (mStrIndicatorArr != null && mStrIndicatorArr.length > 0)
		{
			LinearLayout line = null;
			for (int i = 0; i < mStrIndicatorArr.length; i++)
			{
				if (line == null)
				{
					line = new LinearLayout(this);
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
					lp.topMargin = 10;
					line.setLayoutParams(lp);
				}
				Button button = new Button(this);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)getResources().getDimension(R.dimen.INDICATOR_LENGTH), WRAP_CONTENT);
				lp.leftMargin = 5;
				button.setLayoutParams(lp);
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					button.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_tip_select));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					button.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.btn_tip_select_orange));
				}
				
				button.setText(mStrIndicatorArr[i]);
				button.setSingleLine(true);
				button.setTextSize((int)getResources().getDimension(R.dimen.INDICATOR_TEXT_SIZE));
				button.setTextColor(R.color.text_orange);
				line.addView(button);
				button.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						Button btn = (Button) view;
						mEtSearch.setText(btn.getText().toString());
						// 将光标移到文字末尾
						CharSequence text = mEtSearch.getText();
						if(text instanceof Spannable)
						{
							Spannable spanText = (Spannable) text;
							Selection.setSelection(spanText, text.length());
						}
						if(btn != mBtnOldIndic)
						{
							btn.setTextColor(Color.WHITE);
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								btn.setBackgroundDrawable(getBaseContext().getResources()
								.getDrawable(R.drawable.btn_tip_selected));
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								btn.setBackgroundDrawable(getBaseContext().getResources()
										.getDrawable(R.drawable.btn_tip_selected_orange));
							}
							
							if (mBtnOldIndic != null)
							{
								mBtnOldIndic.setTextColor(R.color.text_orange);
								if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
								{
									mBtnOldIndic.setBackgroundDrawable(getBaseContext().getResources().getDrawable(
											R.drawable.btn_tip_select));
								}
								else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
								{
									mBtnOldIndic.setBackgroundDrawable(getBaseContext().getResources().getDrawable(
											R.drawable.btn_tip_select_orange));
								}
							}
							mBtnOldIndic = btn;
						}
					}
				});
				// 三个一行
				if ((i + 1) % 3 == 0)
				{
					mLayFasters.addView(line);
					line = null;
				}
			}
			if (line != null)
			{
				mLayFasters.addView(line);
				line = null;
			}
		}
	}

	// 从数据库中模糊查询相关信息
	private List<String> getRelativePhone(String band) 
	{
		mRelativePhones = mPhoneParamsDao.getRelativePhones(band);
		List<String> res = null;
		if(mRelativePhones != null && mRelativePhones.size() > 0)
		{
			res = getRelativeTip(mRelativePhones);
		}
		return res;
	} 

	// 获取模糊查询下拉框数据    band + name
	private List<String> getRelativeTip(List<PhoneModel> relativePhones)
	{
		List<String> relative = new ArrayList<String>();
		for(int i = 0; i < relativePhones.size(); i++)
		{
			StringBuffer sb = new StringBuffer();
			sb.append(relativePhones.get(i).getBand())
			  .append(" ")
			  .append(relativePhones.get(i).getName());
			relative.add(sb.toString());
		}
		return relative;
	}

	// 从服务器获取风向标信息
	private void getIndicatorFromServer()
	{
		if(Statics.IS_DEBUG)
		{
			mStrIndicatorArr = new String[]
			{ "三星", "APPLE", "SONY", "小米","诺基亚", "HTC" };
		}
		else
		{
			mStrIndicatorArr = ConstantData.mIndicator;
		}
	}

	// 初始化机型选择界面
	private void initGalleryFlow()
	{
		mGallery = (GalleryFlow) findViewById(R.id.gallery);
		mGallery.setCallbackDuringFling(false);
		mGallery.setSpacing(30);
		mGallery.setMaxRotationAngle((int)getResources().getDimension(R.dimen.GALLERY_FLOW_SIZE));
		mGallery.setFadingEdgeLength(60);
		mGallery.setHorizontalFadingEdgeEnabled(true);
		mGallery.setOnTouchListener(this);
		mGallery.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, final View view, final int position, long id)
			{
				Log.e("pic", "item selected... position:" + position);
				mImageAdapter.getBigBitmap((ImageView)view, position, new int[]{mImageAdapter.BIG_WIDTH, mImageAdapter.BIG_HEIGHT});
				mTipTimer.cancel();
				mTipTimer = new Timer();
				mTipTimer.schedule(new TimerTask()
				{
					@Override
					public void run()
					{
						runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								mGallery.setSelection(mGallery.getSelectedItemPosition());
								if(!mLayModelTips.isShown())
								{
									mCurrentModel = mModelData.get(mGallery.getSelectedItemPosition());
									mLayModelTips.setVisibility(View.VISIBLE);
									mLayModelTips.startAnimation(mModelTipAnm);
									// 展示手机信息
									mTvTipName.setText(mCurrentModel.getNameDes());
									mTvTipOp.setText(mCurrentModel.getOp());
									mTvTipPrice.setText(mCurrentModel.getPriceDes());
									mTvTipScreen.setText(mCurrentModel.getScreenSize() + "寸屏");
								}
							}
						});
					}
				}, 500);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{
				Log.e("", "nothing selected!!");
			}
		});
		
		mGallery.setOnScrollListener(new Runnable() 
		{
			@Override
			public void run()
			{
				mLayModelTips.setVisibility(View.GONE);
				mModelTipAnm.cancel();
				mTipTimer.cancel();
				mTimer.cancel();
				mTimer = new Timer();
				mTimer.schedule(new TimerTask() 
				{
					@Override
					public void run()
					{
						runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								// 滑动时释放掉不可见的ImageView
								mImageAdapter.recycleImage(mGallery.getFirstVisiblePosition(), mGallery.getLastVisiblePosition());
								mImageAdapter.clearBigImages();
							}
						});
					}
				}, 200);
			}
		});
		Log.i("Display", "gallery:" + System.currentTimeMillis());
		// 搜索条件
		mTableLayout = (TableLayout) findViewById(R.id.tableLayout_condition);
		mTableRow = new TableRow(this);
	}

	// 初始化ImageDrag
	private void initImageDrag()
	{
		mIvDrag = (ImageView) findViewById(R.id.iv_drag);
		mIvDrag.setOnTouchListener(this);
		mIvDrag.setOnClickListener(this);
		mIvDrag.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				// 防止出现无匹配机型时，仍然响应长按事件
				if (!mIvNoModel.isShown())
				{
					if (mCurrentModel == null)
					{
						return false;
					}
					// 判断当前机型是否已经在选择机型中
					if (mCurrentModel.equals(mCompareModels[0]) || mCurrentModel.equals(mCompareModels[1])
							|| mCurrentModel.equals(mCompareModels[2]))
					{
						showErrorMsg("当前机型已经加入对比");
						return true;
					}
					mIvDrag.setVisibility(View.VISIBLE);
					mLayShadowCover.setVisibility(View.VISIBLE);
					mIsImageDraging = true;
					mCurrentModel.showInImageView(mIvDrag);
				}
				// return true 才能屏蔽掉单击事件
				return true;
			}
		});
	}

	private boolean mIsShowing = false;
	@Override
	public void onClick(View v)
	{
		try
		{
			if (ButtonUtil.isFastDoubleClick(v.getId(), 1000))
			{
				showErrorMsg("您的操作速度过快！");
				return;
			}
			switch (v.getId())
			{
				// 图示手机信息
				case R.id.iv_detail:
					if(mDetailCurrModel != null)
					{
						if(!mIsShowing)
						{
							mIsShowing = true;
							ImageBrowseDialog dialog = new ImageBrowseDialog(MobileIntroduceActivity.this,
									mDetailCurrModel.getPicPaths(), mDetailCurrModel.getServerPicPaths());
							dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
							{
								@Override
								public void onDismiss(DialogInterface dialog)
								{
									mIsShowing = false;									
								}
							});
							dialog.show();
						}
					}
					break;

				// 对比框1关闭
				case R.id.btn_compare_close1: 
					closeCompare(0);
					break;

				// 对比框2关闭
				case R.id.btn_compare_close2:
					closeCompare(1);
					break;

				// 对比框3关闭
				case R.id.btn_compare_close3:
					closeCompare(2);
					break;

				// 机型加入对比
				case R.id.btn_add_compare:
					addToCompare();
					break;

				// 立即购买
				case R.id.btn_purchase:
					purchase();
					break;

				// 查看详细参数
				case R.id.btn_detail:
					detail();
					break;

				// 对比
				case R.id.btn_compare:
					compare();
					break;

				// 单击弹出详情
				case R.id.iv_drag:
					if (!mIvNoModel.isShown())
					{
						mCurrentModel = mModelData.get(mGallery.getSelectedItemPosition());
						showModelInfo(mCurrentModel);
					}
					break;

				// 关闭详情
				case R.id.btn_detail_close:
					closePopWindow(mPopDetails);
					break;

				// 单击对比框1
				case R.id.lay_compare1:
					if (mCompareModels[0] != null)
					{
						showModelInfo(mCompareModels[0]);
					}
					break;

				// 单击对比框2
				case R.id.lay_compare2:
					if (mCompareModels[1] != null)
					{
						showModelInfo(mCompareModels[1]);
					}
					break;

				// 单击对比框3
				case R.id.lay_compare3:
					if (mCompareModels[2] != null)
					{
						showModelInfo(mCompareModels[2]);
					}
					break;

				// 可能喜欢1
				case R.id.lay_recommened_1:
					if(mRecommenedModels.size() > 0)
					{
						mDetailCurrModel = mRecommenedModels.get(0);
						showModelInfo(mRecommenedModels.get(0));
					}
					break;

				// 可能喜欢2
				case R.id.lay_recommened_2: 
					if(mRecommenedModels.size() > 1)
					{
						mDetailCurrModel = mRecommenedModels.get(1);
						showModelInfo(mRecommenedModels.get(1));
					}
					break;

				// 可能喜欢3
				case R.id.lay_recommened_3:
					if(mRecommenedModels.size() > 2)
					{
						mDetailCurrModel = mRecommenedModels.get(2);
						showModelInfo(mRecommenedModels.get(2));
					}
					break;

				// 筛选
				case R.id.btn_model_query:
					mPopChoose.showAtLocation(v, Gravity.RIGHT, 0, 0);
					if (mIsFirstIn == true)
					{
						initSxlistone();
					}
					else
					{
						mListone = getSxlistoneList();
						if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
						{
							mLoAdapter = new MeauListAdapter(MobileIntroduceActivity.this, mListone, R.layout.rightmenulist, new String[]
									{ "tv_typerml", "tv_staterml", "listid" }, new int[]
									{ R.id.tv_typerml, R.id.tv_staterml, R.id.listid });
						}
						else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
						{
							mLoAdapter = new MeauListAdapter(MobileIntroduceActivity.this, mListone, R.layout.rightmenulist_orange, new String[]
									{ "tv_typerml", "tv_staterml", "listid" }, new int[]
									{ R.id.tv_typerml, R.id.tv_staterml, R.id.listid });
						}
						mLvOne.setAdapter(mLoAdapter);
						if (mLaytwoishow)
						{
							mLoAdapter.setSelectItem(mFirListOldPosition);
						}
					}
					break;

				// 筛选 确定
				case R.id.bt_rightok:
					mCondition = getData();
					List<PhoneModel> models = queryModels(mCondition);
					showErrorMsg("已匹配符合条件的机型" + models.size()+ "款");
					initConditionView(mCondition);
					displayModels(models);
					if (mLaytwoishow == true)
					{
						mLayListtwo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_out));
						mLayListtwo.setVisibility(View.GONE);
					}
					else
					{
						if(mFirOldItem != null)
						{
							mFirOldItem.setBackgroundDrawable(null);
						}
					}
					closePopWindow(mPopChoose);
					break;

				// 筛选 清除条件
				case R.id.bt_rightclean:
					mLaytwoishow = false;
					if (mLayListtwo.isShown())
					{
						//mLayListtwo.startAnimation(mAnim_right_out);
						mLayListtwo.setVisibility(View.GONE);
					}
					cleanData();
					// 如果主界面的筛选条件界面显示则隐藏
					if(mLayConditionTip.isShown())
					{
						clearCondition(); 
					}
					break;

				// 清除筛选条件提示
				case R.id.ibClearCondition:
					/*
					for(int i = 0; i < mCondition.length; i++)
					{
						mCondition[i] = 0;
					}
					*/
					clearCondition(); 
					break;
					
				/*
				// 关闭筛选
				case R.id.bt_rightclose: closePopWindow(mPopChoose);
				 	break;
				*/

				// 历史浏览
				case R.id.btn_history:
					mPopHistory.showAtLocation(v, Gravity.RIGHT, 0, 0);
					break;
				
				// 历史浏览中的查看详情
				case R.id.btnDetail:
					PhoneModel pm = (PhoneModel) v.getTag();
					showModelInfo(pm);
					break;
					
				// 关闭历史浏览
				case R.id.btn_history_hide:
					closePopWindow(mPopHistory);
					break;

				// 清空历史记录
				case R.id.btn_clear_history:
					clearHistory();
					break;

				// 搜索界面
				case R.id.btnToSearch:
					mPopSearch.showAtLocation(mParent, Gravity.RIGHT | Gravity.TOP, -10, 10);
					InputMethodManager imm = (InputMethodManager) (MobileIntroduceActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE));
					imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
					indicatorInit();
					break;
					
				// 开始搜索
				case R.id.btnSearch:
					startSearch();
					break;

				// 搜索条件
				case R.id.ivClose:
					mTableRow.removeView((View) v.getTag());
					mTableRow.invalidate();
					break;
				
				// 返回
				case R.id.btn_previous:
				case R.id.layBack:
					exit();
					break;
				default:
					break;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			}

	// 清除主界面提示
	private void clearCondition()
	{
		cleanData();
		initConditionView(mCondition);
		List<PhoneModel> models = queryModels(mCondition);
		displayModels(models);
		mLayConditionTip.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bottom_out));
		mLayConditionTip.setVisibility(View.GONE);
	}

	// 清除历史浏览记录
	private void clearHistory()
	{
		if(mListHistoryModel != null && mListHistoryModel.size() > 0)
		{
			DialogUtil.MsgBox(TITLE_STR, "确定删除所有浏览记录？", "确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					mHistoryDao.clear();
					updateHistory();
				}
			}, "取消", null, null);
		}
		else
		{
			DialogUtil.MsgBox(TITLE_STR, "当前浏览记录为空！", "确定", 0, "", 0, null);
		}
	}

	// 开始搜索
	private void startSearch()
	{
		cleanData();
		mLayConditionTip.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bottom_out));
		mLayConditionTip.setVisibility(View.GONE);
		// 将输入用空格切开
		String strCon = mEtSearch.getText().toString();
		String[] split = strCon.split(" ");
		String band = split[0];
		String name = strCon.substring(band.length());
		mRelativePhones = new ArrayList<PhoneModel>();
		mRelativePhones.addAll(mPhoneParamsDao.getRelativePhones(band, name));
		// 用没有用空格切开的关键字再查询一遍
		List<PhoneModel> tempList = mPhoneParamsDao.getRelativePhones(strCon);
		for(PhoneModel model : tempList)
		{
			if(!mRelativePhones.contains(model))
			{
				mRelativePhones.add(model);
			}
		}
		tempList.clear();
		tempList = null;
		closePopWindow(mPopSearch);
		closePopWindow(mPopChoose);
		displayModels(mRelativePhones);
	}

	// 初始化主界面中筛选条件提示界面
	private void initConditionView(int[] condition)
	{
		mTableRow.removeAllViews();
		mTableLayout.removeAllViews();
		
		/*
		// 最新活动
		if (condition[0] != 0)
		{
			// String brand = ConstantData.brands[condition[1] - 1];
			addToTableRow("活动：", "店长推荐", 0);
		}
		*/
		
		// 品牌
		if (condition[0] != 0)
		{
			String brand = ConstantData.mBrands[condition[0] - 1];
			addToTableRow("品牌：", brand, 0);
		}

		// 价格
		if (condition[1] != 0)
		{
			String price = null;
			// 价格
			switch (condition[1])
			{
				case 0:
					break;
					
				case 1: // 1-499
					price = "500以下";
					break;
					
				case 2: // 500-999
					price = "500-999";
					break;
					
				case 3: // 1000-1999
					price = "1000-1999";
					break;
					
				case 4: // 2000-2999
					price = "2000-2999";
					break;
					
				case 5: // 3000-4999
					price = "3000-4999";
					break;
					
				case 6: // 5000以上
					price = "5000以上";
					break;
					
				default:
					break;
			}
			if (price != null)
			{
				addToTableRow("价格范围:", price, 1);
			}
		}

		// 系统
		if (condition[2] != 0)
		{
			String system = null;
			switch (condition[2])
			{
				case 0:
					break;
					
				case 1: // IOS
					system = "IOS";
					break;
					
				case 2: // 安卓
					system = "Android";
					break;
					
				case 3: // WinPhone
					system = "WinPhone";
					break;
					
				case 4: // windowsmobile
					system = "WindowsMobile";
					break;
					
				case 5: // 塞班
					system = "塞班";
					break;
					
				case 6: // 其他智能系统
					system = "其他智能系统";
					break;
					
				case 7: // 其他非智能系统
					system = "其他非智能系统";
					break;
					
				default:
					break;
			}
			if (system != null)
			{
				addToTableRow("操作系统：", system, 2);
			}
		}

		// 屏幕尺寸
		if (condition[3] != 0)
		{
			String screnSize = null;
			switch (condition[3])
			{
				case 0:
					break;
					
				case 1: // 2.0及以下
					screnSize = "2.0以下";
					break;
					
				case 2: // 2.1-2.5
					screnSize = "2.1-2.5";
					break;
					
				case 3: // 2.6-3.0
					screnSize = "2.6-3.0";
					break;
					
				case 4: // 3.1-3.5
					screnSize = "3.1-3.5";
					break;
					
				case 5: // 3.6-4.0
					screnSize = "3.6-4.0";
					break;
					
				case 6: // 4.1-4.9
					screnSize = "4.1-4.9";
					break;
					
				case 7: // 5.0以上
					screnSize = " 5.0以上";
					break;
					
				default:
					break;
			}
			if (screnSize != null)
			{
				addToTableRow("屏幕尺寸：", screnSize, 3);
			}
		}

		// CPU
		if (condition[4] != 0)
		{
			String strCpu = null;
			switch (condition[4])
			{
				case 0:
					break;
					
				case 1: // 单核1G以下
					strCpu = "单核1G以下";
					break;
					
				case 2: // 单核1G以上
					strCpu = "单核1G以上";
					break;
					
				case 3: // 双核
					strCpu = "双核";
					break;
					
				case 4: // 四核
					strCpu = "四核";
					break;
				default:
					break;
			}
			if (strCpu != null)
			{
				addToTableRow("CPU:", strCpu, 4);
			}
		}
		if (mTableRow != null)
		{
			mTableLayout.addView(mTableRow);
			mTableRow.invalidate();
			mTableLayout.invalidate();
		}
		if (!isEmpty(condition))
		{
			if (!mLayConditionTip.isShown())
			{
				mLayConditionTip.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bottom_in));
				mLayConditionTip.setVisibility(View.VISIBLE);
			}
		}
		else
		{
			if (mLayConditionTip.isShown())
			{
				mLayConditionTip.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bottom_out));
				mLayConditionTip.setVisibility(View.GONE);
			}
		}
	}

	// 判断数组是否全为0
	private boolean isEmpty(int[] condition)
	{
		boolean isEmpty = true;
		for (int i = 0; i < mCondition.length; i++)
		{
			if (mCondition[i] != 0)
			{
				isEmpty = false;
				break;
			}
		}
		return isEmpty;
	}

	// 添加至界面
	private void addToTableRow(String key, String value, int index)
	{
		// 显示查询条件 20131013
		View view = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			view = mInflater.inflate(R.layout.horizontalscrollview_item, null);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			view = mInflater.inflate(R.layout.horizontalscrollview_item_orange, null);
		}
		
		TextView title = (TextView) view.findViewById(R.id.title);
		TextView content = (TextView) view.findViewById(R.id.content);
		mIbCloseConditon = (ImageButton) view.findViewById(R.id.ivClose);
		title.setText(key);
		content.setText(value);
		content.setOnClickListener(new OnConditionClickListener(view, index));
		mIbCloseConditon.setOnClickListener(new OnConditionClickListener(view, index));
		view.setOnClickListener(new OnConditionClickListener(view, index));
		mTableRow.addView(view);
	}

	// 选择条件点击监听器
	class OnConditionClickListener implements OnClickListener
	{
		private View	mView;
		private int		mIndex;

		public OnConditionClickListener(View view, int index)
		{
			this.mView = view;
			this.mIndex = index;
		}

		@Override
		public void onClick(View v)
		{
			mTableRow.removeView(mView);
			mTableRow.invalidate();
			// 修改对应的下标
			mCondition[mIndex] = 0;
			// 20131120 修改 ：删掉屏幕下方的某个筛选参数，相应的也应当在筛选参数栏中清除掉此参数
			mConditionMap.put(mIndex + "", "0");
			mStrConditionMap.remove(mIndex);
			List<PhoneModel> models = queryModels(mCondition);
			initConditionView(mCondition);
			displayModels(models);
		}
	}

	// 加入对比
	private void addToCompare()
	{
		if (mDetailCurrModel.equals(mCompareModels[0]) || mDetailCurrModel.equals(mCompareModels[1])
				|| mDetailCurrModel.equals(mCompareModels[2]))
		{
			showErrorMsg("当前机型已经加入对比");
			return;
		}
		if (mCompareModels[0] == null)
		{
			compareModel(0, mDetailCurrModel);
		}
		else if (mCompareModels[1] == null)
		{
			compareModel(1, mDetailCurrModel);
		}
		else if (mCompareModels[2] == null)
		{
			compareModel(2, mDetailCurrModel);
		}
		else
		{
			showErrorMsg("产品对比栏已满");
		}
	}

	// 立即购买
	private void purchase()
	{
		if (Statics.IS_DEBUG == false)
		{
			showIMEIDialog();
		}
		else
		{
			DialogUtil.showProgress("正在获取合约信息, 请稍候...");
			new Thread()
			{
				public void run()
				{
					SystemClock.sleep(1500);
					Bundle bundle = new Bundle();
					bundle.putSerializable("model", mDetailCurrModel);
					bundle.putString("className", PackageSelectActivity.class.getName());
					Message msg = new Message();
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					// dlg_progress.dismiss();
					DialogUtil.closeProgress();
				};
			}.start();
			closePopWindow(mPopDetails);
		}
		// showIMEIDialog();
	}
	
	// 输入终端串号并提交
	private void showIMEIDialog()
	{ 
		View view = getLayoutInflater().inflate(R.layout.lay_input, null);
		final EditText et_input = (EditText) view.findViewById(R.id.editText);
		DialogUtil.showCustomerDialog("请输入售卖机型串号", 
				view, 
				"提交", 
				new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						DialogUtil.dismiss();
						final String number = et_input.getText().toString().trim();
						if(StringUtil.isEmptyOrNull(number)) 
						{
							showErrorMsg("售卖机型串号不能为空，请重新输入！");
							showIMEIDialog();
							return;
						}
						DialogUtil.showProgress("正在获取合约信息, 请稍候...");
						new Thread() 
						{
							@Override
							public void run() 
							{
								boolean ret = mHaoka.bll0C01(mDetailCurrModel.getId(), number);// 359769031404905
								// int ret = mHaoka.bll0C01("21", number);// 359769031404905
								if (!ret)
								{
									DialogUtil.closeProgress();
									DialogUtil.MsgBox(TITLE_STR, mHaoka.getLastknownError());
								}
								else
								{
									DialogUtil.closeProgress();
									// 点击立即购买后关闭 手机详情
									runOnUiThread(new Runnable()
									{
										public void run()
										{
											closePopWindow(mPopDetails);
										}
									});
									Bundle bundle = new Bundle();
									bundle.putSerializable("model", mDetailCurrModel);
									bundle.putString("className", PackageSelectActivity.class.getName());
									
									Message msg = FrameActivity.mHandler.obtainMessage();
									msg.what = FrameActivity.DIRECTION_NEXT;
									msg.setData(bundle);
									FrameActivity.mHandler.sendMessage(msg);
								}
							};
						}.start();
					} 
				}, 
				"取消", 
				new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
							.hideSoftInputFromWindow(DialogUtil.getDialog().getCurrentFocus().getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
						DialogUtil.dismiss();
					}
				}, 
				new OnClickListener() 
				{
					@Override
					public void onClick(View v)
					{
						((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
						.hideSoftInputFromWindow(DialogUtil.getDialog().getCurrentFocus().getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
						DialogUtil.dismiss();
					}
				}, 
				false);
	}

	// 详细参数
	private void detail()
	{
		if (!mDetailCurrModel.isParamLoaded)
		{
			PhoneModel temp = getModelInfoFromDb(mDetailCurrModel);
			if (temp.isParamLoaded)
			{
				mDetailCurrModel.setParams(temp);
			}
		}
		if (mDetailCurrModel.isParamLoaded)
		{
			DetailParamDialog dlg_detail = new DetailParamDialog(this, mDetailCurrModel);
			dlg_detail.show();
		}
		else
		{
			//loadDetailInfoFromServer(mCurrentModel.getId());
			DialogUtil.showProgress("正在加载详细信息, 请稍候...");
			new Thread()
			{
				public void run()
				{
					List<PhoneModel> list = getModelInfoFromServer(new String[]
					{ mDetailCurrModel.getId() });
					DialogUtil.closeProgress();
					if (list == null)
					{
						DialogUtil.MsgBox("温馨提示", "详细信息加载失败！");
						//DialogUtil.showMessage("详细信息加载失败");
					}
					else
					{
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								DetailParamDialog dlg_detail = new DetailParamDialog(MobileIntroduceActivity.this,
										mDetailCurrModel);
								dlg_detail.show();
							}
						});
					}
				};
			}.start();
		}
	}

	// 关闭弹出框
	private void closePopWindow(PopupWindow popWindow)
	{
		if (popWindow != null && popWindow.isShowing())
		{
			popWindow.dismiss();
		}
	}

	// 对比
	private void compare()
	{
		if (mCompareModels[0] == null && mCompareModels[1] == null && mCompareModels[2] == null)
		{
			showErrorMsg("对比栏里没有机型");
			return;
		}
		if (mCompareModels[0] != null)
		{
			if (!mCompareModels[0].isParamLoaded)
			{
				PhoneModel temp = getModelInfoFromDb(mCompareModels[0]);
				// if (!temp.isParamLoaded) {
				// temp = getModelInfoFromServer(compareModels[0]);
				// }
				if (temp.isParamLoaded)
				{
					mCompareModels[0].setParams(temp);
				}
			}
		}
		if (mCompareModels[1] != null)
		{
			if (!mCompareModels[1].isParamLoaded)
			{
				PhoneModel temp = getModelInfoFromDb(mCompareModels[1]);
				// if (!temp.isParamLoaded) {
				// temp = getModelInfoFromServer(compareModels[1]);
				// }
				if (temp.isParamLoaded)
				{
					mCompareModels[1].setParams(temp);
				}
			}
		}
		if (mCompareModels[2] != null)
		{
			if (!mCompareModels[2].isParamLoaded)
			{
				PhoneModel temp = getModelInfoFromDb(mCompareModels[2]);
				// if (!temp.isParamLoaded) {
				// temp = getModelInfoFromServer(compareModels[2]);
				// }
				if (temp.isParamLoaded)
				{
					mCompareModels[2].setParams(temp);
				}
			}
		}

		String ids = "";
		if (mCompareModels[0] != null && !mCompareModels[0].isParamLoaded)
		{
			ids += mCompareModels[0].getId() + ",";
		}
		if (mCompareModels[1] != null && !mCompareModels[1].isParamLoaded)
		{
			ids += mCompareModels[1].getId() + ",";
		}
		if (mCompareModels[2] != null && !mCompareModels[2].isParamLoaded)
		{
			ids += mCompareModels[2].getId() + ",";
		}

		if (ids.length() > 0)
		{
			ids = ids.substring(0, ids.length() - 1);
			loadCompareInfoFromServer(ids);
		}
		else
		{
			PhoneCompareDialog dlg_compare = new PhoneCompareDialog(this, mCompareModels.clone());
			dlg_compare.show();
		}
	}

	// 去掉某个对比机型
	private void closeCompare(int index)
	{
		switch (index)
		{
			case 0:
				mLayCompare1.removeAllViews();
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mLayCompare1.setBackgroundResource(R.drawable.bg_compare_out);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mLayCompare1.setBackgroundResource(R.drawable.bg_compare_out_orange);
				}
				mCompareModels[0] = null;
				mBtnCompareClose1.setVisibility(View.GONE);
				break;
				
			case 1:
				mLayCompare2.removeAllViews();
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mLayCompare2.setBackgroundResource(R.drawable.bg_compare_out);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mLayCompare2.setBackgroundResource(R.drawable.bg_compare_out_orange);
				}
				mCompareModels[1] = null;
				mBtnCompareClose2.setVisibility(View.GONE);
				break;
				
			case 2:
				mLayCompare3.removeAllViews();
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mLayCompare3.setBackgroundResource(R.drawable.bg_compare_out);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mLayCompare3.setBackgroundResource(R.drawable.bg_compare_out_orange);
				}
				mCompareModels[2] = null;
				mBtnCompareClose3.setVisibility(View.GONE);
				break;
				
			default:
				break;
		}
		/*
		if (compareModels[0] == null && compareModels[1] == null &&
		compareModels[2] == null) {
		mLayCompare.startAnimation(AnimationUtils.
		LoadAnimation(getBaseContext(), R.anim.left_out));
		mLayCompare.setVisibility(View.GONE);
		mLayCompareShow.setVisibility(View.VISIBLE); }
		*/
	}

	public void showModelInfo(PhoneModel model)
	{
		if (model == null)
		{
			return;
		}
	 	if (!model.isParamLoaded)
	 	{
	 		PhoneModel temp = getModelInfoFromDb(model); 
	 		if (!temp.isParamLoaded)
	 		{ 
	 			temp = getModelInfoFromServer(model); 
	 		} 
	 		if (temp.isParamLoaded) 
	 		{
	 			model.setParams(temp); 
	 		} 
	 	}
	 	//mIvDetail.setImageBitmap(model.getPicBitmap());
		if (mPopHistory != null && mPopHistory.isShowing())
		{
			mPopHistory.dismiss();
		}
		model.showInImageView(mIvDetail);
		mTvDetailTitle.setText(model.getName()); 
		mTvDetailNet.setText(model.getNet());
		mTvDetailScreen.setText(model.getScreenSize() + "英寸" + " " + (model.getScreenParam().equals("无")?"":model.getScreenParam()));
		mTvDetailCpu.setText(model.getCpu());
		mTvDetailPrice.setText(model.getPriceDes());

		mLayRecommened_1.removeAllViews();
		mLayRecommened_2.removeAllViews();
		mLayRecommened_3.removeAllViews();
		mRecommenedModels = getRecommenedModel(model);

		int recommendedCount = mRecommenedModels.size();
		switch (recommendedCount)
		{
			case 3:
				View recommenedView3 = getModelView(mRecommenedModels.get(2), R.layout.lay_recommened_item);
				mLayRecommened_3.addView(recommenedView3);

			case 2:
				View recommenedView2 = getModelView(mRecommenedModels.get(1), R.layout.lay_recommened_item);
				mLayRecommened_2.addView(recommenedView2);

			case 1:
				View recommenedView1 = getModelView(mRecommenedModels.get(0), R.layout.lay_recommened_item);
				mLayRecommened_1.addView(recommenedView1);

			case 0:
				break;

			default:
				break;
		}
		mPopDetails.setWidth((mBtnCompare.getRight() - mLayCompare1.getLeft()));
		mPopDetails.showAtLocation(mParent, Gravity.BOTTOM, -25, 25);
		//mCurrentModel = model;
		mDetailCurrModel = model;
		// 删除历史记录中重复的记录
		mHistoryDao.deletePhone(model.getId());
		// 添加到历史浏览记录中
		mHistoryDao.insertData(model); 
		// 更新历史浏览记录界面
		updateHistory();
	}
	
	// 更新浏览历史
	private void updateHistory()
	{
		if (mListHistoryModel != null)
		{
			mListHistoryModel.clear();
		}
		mListHistoryModel = mHistoryDao.getData();
		if (!CollectionUtil.isEmptyOrNull(mListHistoryModel)) 
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mHistoryAdapter = new HistoryModelAdapter(this, mListHistoryModel, R.layout.pop_hisroty_item_model, this);
			}
			else
			{
				mHistoryAdapter = new HistoryModelAdapter(this, mListHistoryModel, R.layout.pop_hisroty_item_model_orange, this);
			}
			mLvHistory.setAdapter(mHistoryAdapter);
			mTvNoHistory.setVisibility(View.GONE);
			mLvHistory.setVisibility(View.VISIBLE);
		}
		else
		{
			mLvHistory.setAdapter(null);
			mTvNoHistory.setVisibility(View.VISIBLE);
			mLvHistory.setVisibility(View.GONE);
		}
	}
	
	// 这里的推荐算法(算价格最接近的)效率很低,以后要优化
	private List<PhoneModel> getRecommenedModel(PhoneModel model)
	{
		List<PhoneModel> list = new ArrayList<PhoneModel>();
		Float price = Float.valueOf(model.getPrice());

		for (int i = 0; i < ConstantData.modelList.size(); i++)
		{
			PhoneModel temp = ConstantData.modelList.get(i);
			if(temp == null)
			{
				continue;
			}
			if (temp.equals(model))
			{
				continue;
			}
			if (list.size() < 3)
			{
				list.add(temp);
			}
			else
			{
				Float price1 = Float.valueOf(temp.getPrice());

				for (int j = 0; j < list.size(); j++)
				{
					PhoneModel temp2 = list.get(j);
					Float price2 = Float.valueOf(temp2.getPrice());
					if (Math.abs(price2 - price) > Math.abs(price1 - price))
					{
						list.remove(temp2);
						list.add(temp);
						break;
					}
				}
			}
		}
		return list;
	}

	private PhoneModel getModelInfoFromDb(PhoneModel model)
	{
		PhoneModel dbModel = mPhoneParamsDao.getModelParamById(model.getId());
		if (dbModel != null)
		{
			return dbModel;
		}
		else
		{
			return model;
		}
	}

	private PhoneModel getModelInfoFromServer(PhoneModel model) 
	{ 
		PhoneModel dbModel2 = mPhoneParamsDao.getModelParamById(model.getId()); 
		if (dbModel2 != null)
		{
			return dbModel2; 
		}
		else 
		{ 
			return model;
		} 
	}

	// 从服务器接口上获取机型的详细参数
	private List<PhoneModel> getModelInfoFromServer(String[] modelIds)
	{
		int size = modelIds.length;
		Map<String, String> param = new HashMap<String, String>();
		switch (size)
		{
			case 3:
				param.put("model_id3", modelIds[2]);

			case 2:
				param.put("model_id2", modelIds[1]);

			case 1:
				param.put("model_id1", modelIds[0]);
				break;

			default:
				return null;
		}

		try
		{
			if (param.isEmpty())
			{
				return null;
			}
			else
			{
				param.put("psamid", equipmentService.getPsamId());
				if (Statics.IS_DEBUG)
				{
					param.put("psamid", "3401100000000008");
				}
			}

			String json = HttpURLConnectionUtil.doGet(ConstantData.getServerPath() + "/getMobileDetail.shtml", param,
					null, 10000);
			JSONObject jobj = new JSONObject(json);
			System.out.println(jobj);
			List<PhoneModel> models = ConstantData.getModelList(jobj);
			if (models == null || models.size() == 0)
			{
				return null;
			}
			for (int i = 0; i < ConstantData.modelList.size(); i++)
			{
				PhoneModel model = ConstantData.modelList.get(i);
				for (int j = 0; j < models.size(); j++)
				{
					PhoneModel model1 = models.get(j);
					if (model.getId().equals(model1.getId()))
					{
						model1.isParamLoaded = true;
						model.setParams(model1);
						mPhoneParamsDao.insertData(model);
					}
				}
			}
			return models;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public void onInitSuccess()
	{
		/*
		haoka = new Haoka3GIndependent(this, equipmentService, socketService,
		dataBaseService); int ret = haoka.bll0C05(); dlg_progress.dismiss();
		if (ret == ReturnEnum.SUCCESS) {
		
		} else { String errorMsg = haoka.getLastKnownError();
		showMessage(errorMsg); }
		*/
		System.out.println("成功");
	}

	@Override
	public void onInitFail()
	{
		System.out.println("失败");
	}

	float mOldX;
	public boolean onTouch(View v, MotionEvent event)
	{
		float rawX = event.getRawX();
		float rawY = event.getRawY();
		
		if(v.getId() ==  R.id.iv_drag) 
		{
			switch (event.getAction()) 
			{
				case MotionEvent.ACTION_DOWN:
					if(mIvDrag_x == -1) 
					{
						mIvDrag_x = mIvDrag.getLeft();
						mIvDrag_y = mIvDrag.getTop();
					}
					mLastx = rawX - mIvDrag_x;
					mLasty = rawY - mIvDrag_y;
					//System.out.println("left :" + mIvDrag.getLeft() + " top :" + mIvDrag.getTop());
					if (mLayCompare.isShown() && mLayCompare1_x == -1)
					{
						int[] location = new int[2];
						mLayCompare1.getLocationOnScreen(location);
						mLayCompare1_x = location[0];
						mLayCompare1_y = location[1];
						
						location = new int[2];
						mLayCompare2.getLocationOnScreen(location);
						mLayCompare2_x = location[0];
						mLayCompare2_y = location[1];
						
						location = new int[2];
						mLayCompare3.getLocationOnScreen(location);
						mLayCompare3_x = location[0];
						mLayCompare3_y = location[1];
					}
					mGallery.onTouchEvent(event);
					//Log.d("", "action down, x=" + event.getRawX() + ", y=" + event.getRawY());
					mIsGalleryOnMove = false;
					mOrigX = rawX;
					mOrigY = rawY;
					break;
					
				case MotionEvent.ACTION_MOVE:
					if (mIsImageDraging) 
					{
						int newx = (int) (rawX - mLastx);
						int newy = (int) (rawY - mLasty);
						//System.out.println("newx :" + newx + " newy:" + newy);
						MarginLayoutParams params = (MarginLayoutParams) v.getLayoutParams();
						params.leftMargin = newx;
						params.topMargin = newy;
						v.setLayoutParams(params);
						if (mLayCompare.isShown())
						{
							if (mCompareModels[0] == null)
							{
								if (rawX > mLayCompare1_x && rawX < mLayCompare1_x + mLayCompare1.getWidth()
										&& rawY > mLayCompare1_y && rawY < mLayCompare1_y + mLayCompare1.getHeight())
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare1.setBackgroundResource(R.drawable.bg_compare_in);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare1.setBackgroundResource(R.drawable.bg_compare_in_orange);
									}
								}
								else
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare1.setBackgroundResource(R.drawable.bg_compare_out);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare1.setBackgroundResource(R.drawable.bg_compare_out_orange);
									}
								}
							}
							if (mCompareModels[1] == null)
							{
								if (rawX > mLayCompare2_x && rawX < mLayCompare2_x + mLayCompare2.getWidth()
										&& rawY > mLayCompare2_y && rawY < mLayCompare2_y + mLayCompare2.getHeight())
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare2.setBackgroundResource(R.drawable.bg_compare_in);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare2.setBackgroundResource(R.drawable.bg_compare_in_orange);
									}
								}
								else
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare2.setBackgroundResource(R.drawable.bg_compare_out);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare2.setBackgroundResource(R.drawable.bg_compare_out_orange);
									}
								}
							}
							if (mCompareModels[2] == null)
							{
								if (rawX > mLayCompare3_x && rawX < mLayCompare3_x + mLayCompare3.getWidth()
										&& rawY > mLayCompare3_y && rawY < mLayCompare3_y + mLayCompare3.getHeight())
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare3.setBackgroundResource(R.drawable.bg_compare_in);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare3.setBackgroundResource(R.drawable.bg_compare_in_orange);
									}
								}
								else
								{
									if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
									{
										mLayCompare3.setBackgroundResource(R.drawable.bg_compare_out);
									}
									else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
									{
										mLayCompare3.setBackgroundResource(R.drawable.bg_compare_out_orange);
									}
								}
							}
						}
					}
					else
					{
						Log.d("", "action move, x=" + event.getRawX() + ", y=" + event.getRawY());
						if(Math.sqrt((rawX-mOrigX)*(rawX-mOrigX) + (rawY-mOrigY)*((rawY-mOrigY))) > 20) 
						{
							Log.d("", "gallery on move");
							mIsGalleryOnMove = true;
							mGallery.onTouchEvent(event);
						}
					}
					break;
					
				case MotionEvent.ACTION_UP:
					if (mIsImageDraging)
					{
						if (mLayCompare.isShown())
						{
							if (rawX > mLayCompare1_x && rawX < mLayCompare1_x + mLayCompare1.getWidth()
									&& rawY > mLayCompare1_y && rawY < mLayCompare1_y + mLayCompare1.getHeight())
							{
								if (mCompareModels[0] == null)
								{
									compareModel(0, mCurrentModel);
								}
								else
								{
									showErrorMsg("对比栏已有产品,请删除以加入对比");
								}
							}
	
							if (rawX > mLayCompare2_x && rawX < mLayCompare2_x + mLayCompare2.getWidth()
									&& rawY > mLayCompare2_y && rawY < mLayCompare2_y + mLayCompare2.getHeight())
							{
								if (mCompareModels[1] == null)
								{
									compareModel(1, mCurrentModel);
								}
								else
								{
									showErrorMsg("对比栏已有产品,请删除以加入对比");
								}
							}
	
							if (rawX > mLayCompare3_x && rawX < mLayCompare3_x + mLayCompare3.getWidth()
									&& rawY > mLayCompare3_y && rawY < mLayCompare3_y + mLayCompare3.getHeight())
							{
								if (mCompareModels[2] == null)
								{
									compareModel(2, mCurrentModel);
								}
								else
								{
									showErrorMsg("对比栏已有产品,请删除以加入对比");
								}
							}
						}
						
						MarginLayoutParams params = (MarginLayoutParams) v.getLayoutParams();
						params.leftMargin = mIvDrag_x;
						params.topMargin = mIvDrag_y;
						v.setLayoutParams(params);
						mIvDrag.setImageDrawable(null);
						//mIvDrag.setVisibility(View.GONE);
						//Log.e("", "mIvDrag.setVisibility(View.GONE)");
						mLayShadowCover.setVisibility(View.GONE);
						mIsImageDraging = false;
					}
					else
					{
						Log.d("", "action up, x=" + event.getRawX() + ", y=" + event.getRawY());
						if(mIsGalleryOnMove == true) 
						{
							mGallery.onTouchEvent(event);
						}
					}
					break;
					
				default:
					break;
			}
		}
		
		if(event.getAction() == MotionEvent.ACTION_UP)
		{
			// 过滤当前无匹配机型的情况
			if(!mIvNoModel.isShown())
			{
				mGallery.setSelection(mGallery.getSelectedItemPosition());
				mTipTimer.cancel();
				mTipTimer = new Timer();
				// 在手指离开屏幕一段时间后再次显示手机参数信息，改善手机信息有时候不显示的情况
				mTipTimer.schedule(new TimerTask()
				{
					@Override
					public void run()
					{
						runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								mGallery.setSelection(mGallery.getSelectedItemPosition());
								if(!mLayModelTips.isShown())
								{
									mCurrentModel = mModelData.get(mGallery.getSelectedItemPosition());
									mLayModelTips.setVisibility(View.VISIBLE);
									mLayModelTips.startAnimation(mModelTipAnm);
									// 展示手机信息
									mTvTipName.setText(mCurrentModel.getNameDes());
									mTvTipOp.setText(mCurrentModel.getOp());
									mTvTipPrice.setText(mCurrentModel.getPriceDes());
									mTvTipScreen.setText(mCurrentModel.getScreenSize() + "寸屏");
								}
							}
						});
					}
				}, 1200);
			}
		}
		return false;
	}
	
	// 加入对比
	private void compareModel(int index, PhoneModel model)
	{
		LinearLayout target = null;

		switch (index)
		{
			case 0:
				target = mLayCompare1;
				mBtnCompareClose1.setVisibility(View.VISIBLE);
				break;

			case 1:
				target = mLayCompare2;
				mBtnCompareClose2.setVisibility(View.VISIBLE);
				break;

			case 2:
				target = mLayCompare3;
				mBtnCompareClose3.setVisibility(View.VISIBLE);
				break;

			default:
				break;
		}
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			target.setBackgroundResource(R.drawable.bg_compare_on);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			target.setBackgroundResource(R.drawable.bg_compare_on_orange);
		}
		
		View compareView = getModelView(model, R.layout.lay_compare_item_model);
		target.addView(compareView);

		mCompareModels[index] = model;
	}

	private View getModelView(PhoneModel model, int resourceId)
	{
		View view = View.inflate(getBaseContext(), resourceId, null);
		ImageView iv_phonePic = (ImageView) view.findViewById(R.id.iv_phonepic);
		TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
		TextView tv_price = (TextView) view.findViewById(R.id.tv_price);

		// iv_phonePic.setImageBitmap(model.getPicBitmap());
		model.showInImageView(iv_phonePic);
		tv_name.setText(model.getNameDes());
		tv_price.setText(model.getPriceDes());
		return view;
	}

	// 初始化一级菜单并监听
	public void initSxlistone()
	{
		mListone = getSxlistoneList();
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mLvOne.setDivider(this.getResources().getDrawable(R.drawable.menulistline));
			mLvOne.setVerticalScrollBarEnabled(false);
			mLoAdapter = new MeauListAdapter(this, mListone, R.layout.rightmenulist, new String[]
					{ "tv_typerml", "tv_staterml", "listid" }, new int[]
					{ R.id.tv_typerml, R.id.tv_staterml, R.id.listid });
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLvOne.setDivider(this.getResources().getDrawable(R.drawable.menulistline_orange));
			mLvOne.setVerticalScrollBarEnabled(false);
			mLoAdapter = new MeauListAdapter(this, mListone, R.layout.rightmenulist_orange, new String[]
					{ "tv_typerml", "tv_staterml", "listid" }, new int[]
					{ R.id.tv_typerml, R.id.tv_staterml, R.id.listid });
		}
		
		mLvOne.setAdapter(mLoAdapter);
		mIsFirstIn = false;
		mLvOne.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				mLoAdapter.setSelectItem(position);
				if (mFirListOldPosition != position)
				{
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_1selected));
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_1selected_orange));
					}
					if (mFirListOldPosition != -1 && mFirOldItem != null)
					{
						mFirOldItem.setBackgroundDrawable(null);
					}
				}
				else
				{
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_1selected));
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_1selected_orange));
					}
					/*
					if (view.getBackground() != null)
					{
						view.setBackgroundDrawable(null);
					}
					else
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_1selected));
					}
					*/
				}
				mFirOldItem = view;
				mFirListOldPosition = position;
				mLaytwoishow = true;
				mLayListtwo.startAnimation(AnimationUtils.loadAnimation(MobileIntroduceActivity.this, R.anim.right_in));
				mLayListtwo.setVisibility(View.VISIBLE);
				initSxlisttwo(position);
			}
		});
	}

	// 初始化二级菜单并监听
	public void initSxlisttwo(int select)
	{
		if (select == 0)
		{
			mListtwo = getSxlisttwoList(select);
		}
		else if (select == 1)// 价格
		{
			mListtwo = getSxlisttwoList(select);
		}
		else if (select == 2)// 系统
		{
			mListtwo = getSxlisttwoList(select);
		}

		else if (select == 3)// 屏幕尺寸
		{
			mListtwo = getSxlisttwoList(select);
		}
		else if (select == 4)// CPU
		{
			mListtwo = getSxlisttwoList(select);
		}
		else if (select == 5)// 最新活动
		{
			mListtwo = getSxlisttwoList(select);
		}

		mListtwo = getSxlisttwoList(select);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mLvTwo.setDivider(this.getResources().getDrawable(R.drawable.menulistline));
			mLvTwo.setVerticalScrollBarEnabled(false);
			mLoAdapter = new MeauListAdapter(this, 
					mListtwo, 
					R.layout.rightmenutlist, 
					new String[] { "tv_menutwo" }, 
					new int[] { R.id.tv_menutwo});
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLvTwo.setDivider(this.getResources().getDrawable(R.drawable.menulistline_orange));
			mLvTwo.setVerticalScrollBarEnabled(false);
			mLoAdapter = new MeauListAdapter(this, 
					mListtwo, 
					R.layout.rightmenutlist_orange, 
					new String[] { "tv_menutwo" }, 
					new int[] { R.id.tv_menutwo});
		}
		mLvTwo.setAdapter(mLoAdapter);
		mLvTwo.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				mLoAdapter.setSelectItem(position);
				if (mSecListOldPosition != position)
				{
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_2selected));
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						view.setBackgroundDrawable(getResources().getDrawable(R.drawable.layright_2selected_orange));
					}
					
					if (mSecListOldPosition != -1 && mSecListOldItem != null)
					{
						mSecListOldItem.setBackgroundDrawable(null);
					}
				}
				mSecListOldItem = view;
				mSecListOldPosition = position;

				mFirOldItem.setBackgroundDrawable(null);
				TextView crrtv = (TextView) view.findViewById(R.id.tv_menutwo);
				TextView tv = (TextView) mFirOldItem.findViewById(R.id.tv_staterml);
				TextView tv_id = (TextView) mFirOldItem.findViewById(R.id.listid);
				tv.setText(crrtv.getText().toString());
				//tv.setTextColor(Color.parseColor("#FF8530"));
				tv_id.setText("" + position);
				mLaytwoishow = false; 
				mLayListtwo.startAnimation(AnimationUtils.loadAnimation(MobileIntroduceActivity.this, R.anim.left_out));
				mLayListtwo.setVisibility(View.GONE);
				// 保存选中的条件下标
				if (mConditionMap.containsKey("" + mFirListOldPosition))
				{
					mConditionMap.remove(mFirListOldPosition + "");
				}
				mConditionMap.put(mFirListOldPosition + "", position + "");
				// 保存选中的条件内容
				if (mStrConditionMap.containsKey(mFirListOldPosition))
				{
					mStrConditionMap.remove(mFirListOldPosition);
				} 
				mStrConditionMap.put(mFirListOldPosition, crrtv.getText().toString());
				System.out.println("value :" + mFirListOldPosition + " :" +crrtv.getText().toString());
				System.out.println(mConditionMap.get(""+mFirListOldPosition)+"   "+mFirListOldPosition);
			}
		});
	}

	// 获取筛选条件
	public int[] getData()
	{
		if (mListone != null)
		{
			for (int i = 0; i < mListone.size(); i++)
			{
				if (!mConditionMap.containsKey("" + i))
				{
					mConditionMap.put("" + i, "0");
				}
			}

			int arr[] = new int[mConditionMap.size()];
			for (int i = 0; i < mConditionMap.size(); i++)
			{
				arr[i] = Integer.parseInt(mConditionMap.get("" + i));
			}
			return arr;
		}
		return null;
	}

	// 清楚查询条件
	public void cleanData()
	{
		mConditionMap.clear();
		mStrConditionMap.clear();
		mCondition = getData();
		initSxlistone();
		if (mTableRow != null)
		{
			mTableRow.invalidate();
		}
	}

	// 获取二级菜单的显示列表
	public List<Map<String, Object>> getSxlisttwoList(int select)
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		switch (select)
		{
			/*
			case 0:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "店长推荐");
				list.add(map);
				break;
			*/

			case 0:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				for (int i = 0; i < ConstantData.mBrands.length; i++)
				{
					String brand = ConstantData.mBrands[i];
					map = new HashMap<String, Object>();
					map.put("tv_menutwo", brand);
					list.add(map);
				}
				break;

			case 1:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "1-499");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "500-999");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "1000-1999");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "2000-2999");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "3000-4999");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "5000以上");
				list.add(map);
				break;

			case 2:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "IOS");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "安卓");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "WINPHONE");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "WINDOWS MOBILE");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "塞班");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "其他智能系统");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "其他非智能系统");
				list.add(map);
				break;

			case 3:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "2.0英寸及以下");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "2.1-2.5英寸");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "2.6-3.0英寸");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "3.1-3.5英寸");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "3.6-4.0英寸");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "4.1-4.9英寸");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "5.0英寸及以上");
				list.add(map);
				break;

			case 4:
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "全部");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "单核1GHz以下CPU");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "单核1GHz及以上CPU");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "双核CPU");
				list.add(map);
				map = new HashMap<String, Object>();
				map.put("tv_menutwo", "四核CPU"); 
				list.add(map);
				break;
		}
		return list;
	}

	// 获取一级菜单的显示列表
	public List<Map<String, Object>> getSxlistoneList()
	{
		String tempStr;
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		/*
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tv_typerml", "最新活动");
		tempStr = mStrConditionMap.get(0);
		if (tempStr != null && !tempStr.equals(""))
		{
			map.put("tv_staterml", tempStr);
		}
		else
		{
			map.put("tv_staterml", "全部");
		}
		list.add(map);
		*/

		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("tv_typerml", "品牌");
		tempStr = mStrConditionMap.get(0);
		if (tempStr != null && !tempStr.equals(""))
		{
			map1.put("tv_staterml", tempStr);
		}
		else
		{
			map1.put("tv_staterml", "全部");
		}
		list.add(map1);

		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("tv_typerml", "价格");
		tempStr = mStrConditionMap.get(1);
		if (tempStr != null && !tempStr.equals(""))
		{
			map2.put("tv_staterml", tempStr);
		}
		else
		{
			map2.put("tv_staterml", "全部");
		}
		list.add(map2);

		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("tv_typerml", "系统");
		tempStr = mStrConditionMap.get(2);
		if (tempStr != null && !tempStr.equals(""))
		{
			map3.put("tv_staterml", tempStr);
		}
		else
		{
			map3.put("tv_staterml", "全部");
		}
		list.add(map3);

		Map<String, Object> map4 = new HashMap<String, Object>();
		map4.put("tv_typerml", "屏幕尺寸");
		tempStr = mStrConditionMap.get(3);
		if (tempStr != null && !tempStr.equals(""))
		{
			map4.put("tv_staterml", tempStr);
		}
		else
		{
			map4.put("tv_staterml", "全部");
		}
		list.add(map4);

		Map<String, Object> map5 = new HashMap<String, Object>();
		map5.put("tv_typerml", "CPU");
		tempStr = mStrConditionMap.get(4);
		if (tempStr != null && !tempStr.equals(""))
		{
			map5.put("tv_staterml", tempStr);
		}
		else
		{
			map5.put("tv_staterml", "全部");
		}
		list.add(map5);
		return list;
	}

	// 查询符合条件的机型
	public List<PhoneModel> queryModels(int[] condition)
	{
		System.out.println("condition : "+Arrays.toString(condition));
		List<PhoneModel> result = new ArrayList<PhoneModel>();
		for (int i = 0; i < ConstantData.modelList.size(); i++)
		{
			PhoneModel model = ConstantData.modelList.get(i);
			/*
			switch (condition[0])
			{
				case 0:
					break;

				case 1:
					if (!model.getIsRecommened().equals("1"))
					{
						continue;
					}
					break;

				default:
					break;
			}
			*/
			
			// 品牌
			if (condition[0] != 0)
			{
				switch (condition[0])
				{
					case 0:
						break;

					default:
						String brand = ConstantData.mBrands[condition[0] - 1];
						if (!model.getBand().equals(brand))
						{
							continue;
						}
						break;
				}
			}
			
			// 价格
			switch (condition[1])
			{
				case 0:
					break;
				case 1: // 1-499
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 > 499 || price1 < 0)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}

					break;
				case 2: // 500-999
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 > 999 || price1 < 500)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 3: // 1000-1999
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 > 1999 || price1 < 1000)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}

					break;
				case 4: // 2000-2999
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 > 2999 || price1 < 2000)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 5: // 3000-4999
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 > 4999 || price1 < 3000)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 6: // 5000以上
					try
					{
						float price1 = Float.parseFloat(model.getPrice());
						if (price1 < 5000)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				default:
					break;
			}

			// 系统
			switch (condition[2])
			{
				case 0:

					break;
				case 1: // IOS
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("ios"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 2: // 安卓
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("安卓"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 3: // WinPhone
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("winphone") && !op.equalsIgnoreCase("windowsphone"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 4: // windowsmobile
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("windowsmobile") && !op.equalsIgnoreCase("winmobile"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 5: // 塞班
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("塞班"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 6: // 其他智能系统
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("其他智能系统"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 7: // 其他非智能系统
					try
					{
						String op = model.getOp().replaceAll(" ", "");
						if (!op.equalsIgnoreCase("其他非智能系统"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				default:
					continue;
			}

			// 屏幕尺寸
			switch (condition[3])
			{
				case 0:

					break;
				case 1: // 2.0及以下
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 2.0)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 2: // 2.1-2.5
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 2.5 || screen < 2.1)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 3: // 2.6-3.0
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 3.0 || screen < 2.6)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 4: // 3.1-3.5
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 3.5 || screen < 3.1)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 5: // 3.6-4.0
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 4.0 || screen < 3.6)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 6: // 4.1-4.9
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen > 4.9 || screen < 4.1)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 7: // 5.0以上
					try
					{
						float screen = Float.parseFloat(model.getScreenSize());
						if (screen < 5.0)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				default:
					continue;
			}

			// CPU
			switch (condition[4])
			{
				case 0:

					break;
				case 1: // 单核1G以下
					try
					{
						String coreNum = model.getCoreNum().trim();
						float cpu = Float.parseFloat(model.getCpu().toLowerCase().replace("ghz", "").replace("g", ""));
						if (!coreNum.equals("单核") || cpu >= 1)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					continue;
				case 2: // 单核1G以上
					try
					{
						String coreNum = model.getCoreNum().trim();
						float cpu = Float.parseFloat(model.getCpu().toLowerCase().replace("ghz", "").replace("g", ""));
						if (!coreNum.equals("单核") || cpu < 1)
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 3: // 双核
					try
					{
						String coreNum = model.getCoreNum().trim();
						if (!coreNum.equals("双核"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				case 4: // 四核
					try
					{
						String coreNum = model.getCoreNum().trim();
						if (!coreNum.equals("四核"))
						{
							continue;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						continue;
					}
					break;
				default:
					continue;
			}
			result.add(model);
		}
		return result;
	}
	
	// 展示所有的机型
	private void displayModels(List<PhoneModel> modelList) 
	{
		if (mImageAdapter != null) 
		{
			mImageAdapter.clearImages();
			mImageAdapter = null;
		}
		mModelData = modelList;
		mImageAdapter = new ImageAdapter(this, modelList);
		//Log.i("Display", "adapter: " + System.currentTimeMillis());
		//mImageAdapter.createReflectedImages();
		//Log.i("Display", "创建倒影: " + System.currentTimeMillis());
		mGallery.setAdapter(mImageAdapter);
		mGallery.setSelection((mImageAdapter.getCount() - 1) / 2);
		
		if (mModelData != null && mModelData.size() > 0) 
		{
			mCurrentModel = mModelData.get((mImageAdapter.getCount() - 1) / 2);
			mTvTipName.setText(mCurrentModel.getNameDes());
			mTvTipOp.setText(mCurrentModel.getOp());
			mTvTipPrice.setText(mCurrentModel.getPriceDes());
			mTvTipScreen.setText(mCurrentModel.getScreenSize() + "寸屏");
			mLayModelTips.setVisibility(View.VISIBLE);
		}
		else
		{
			mLayModelTips.setVisibility(View.GONE);
		}

		// 无符合机型时显示
		if (modelList == null || modelList.size() == 0)
		{
			mIvNoModel.setVisibility(View.VISIBLE);
		}
		else
		{
			mIvNoModel.setVisibility(View.GONE);
		}
	}

	// 从服务器加载比对信息
	private void loadCompareInfoFromServer(String ids)
	{
		// final String[] idArray = ids.split(",");
		// dlg_progress.show();
		new Thread()
		{
			public void run()
			{
				// List<PhoneModel> list = getModelInfoFromServer(idArray);
				DialogUtil.closeProgress();
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						PhoneCompareDialog dlg_compare = new PhoneCompareDialog(MobileIntroduceActivity.this,
								mCompareModels.clone());
						dlg_compare.show();
					}
				});
			};
		}.start();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			exit();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	/*
	@Override
	public void onPause() 
	{
		super.onPause();
		// 清除图片缓冲池
		BitmapPoolUtil.clear();
		// 清除ImageAdapter
		if(mImageAdapter != null) 
		{
			mImageAdapter.clearImages(false);
		}
		// 手动调用垃圾回收器
		System.gc();
		Log.e("", "优惠购机onPause()");
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		// 重新展示机型数据
		mHandler.sendEmptyMessageDelayed(0x02, 200);
		Log.e("", "优惠购机onResume()");
	}
	*/
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		// 清除图片缓冲池
		BitmapPoolUtil.clear();
		if(mImageAdapter != null) 
		{
			mImageAdapter.clearImages();
		}
		// 手动调用垃圾回收器
		System.gc();
		Log.e("", "优惠购机onDestroy()");
	}
	
	// 退出
	private void exit() 
	{
		DialogUtil.MsgBox(TITLE_STR, 
				Statics.EXIT_TIP, 
				"确定", 
				new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
					}
				}, 
				"取消", 
				null , 
				null);
	}

	
	/*
	private void loadDetailInfoFromServer(final String id)
	{ 
		//dlg_progress.show();
		new Thread() 
		{
			public void run()
			{
				List<PhoneModel> list = getModelInfoFromServer(new String[] { id });
				DialogUtil.closeProgress(); 
				runOnUiThread(new Runnable()
				{ 
					public void run() 
					{ 
						DetailParamDialog dlg_detail = new DetailParamDialog(MobileIntroduceActivity.this, mCurrentModel);
						dlg_detail.show(); 
					} 
				});
			}; 
		}.start(); 
	}
	*/
}
