package com.sunnada.baseframe.activity.charge;

import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessCharge;
import com.sunnada.baseframe.business.BusinessEPay;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.PhoneInfoDialog;

public class BusiCharge extends DgActivity 
{
	protected String 				mStrTelNum 				= "";					// 用户号码
	protected String[]				mStrUserInfo			= new String[1];		// 用户信息
	
	@Override
	public void onInitSuccess() 
	{
		
	}
	
	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		System.gc();
		Log.e("", "BusiCharge onDestroy()");
	}
	
	// 查询用户信息
	protected void queryUserInfo(final BusinessCharge business) 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在查询手机账户信息...");
				boolean result = business.bll0105(mStrUserInfo);
				DialogUtil.closeProgress();
				// 查询用户信息失败
				if(result == false) 
				{
					DialogUtil.MsgBox(BusinessCharge.TITLE_STR, business.getLastknownError());
					return;
				}
				else
				{
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							String[] strUsers = mStrUserInfo[0].split(",");
							if(null == strUsers || strUsers.length < 3) 
							{
								PhoneInfoDialog dialog = new PhoneInfoDialog(BusiCharge.this, mStrTelNum, "", "");
								dialog.show();
							}
							else
							{
								PhoneInfoDialog dialog = new PhoneInfoDialog(BusiCharge.this, mStrTelNum, strUsers[1], strUsers[2]);
								dialog.show();
							}
						}
					});
				}
			}
		}.start();
	}
	
	// 检测打印机
	protected boolean checkPrinter(int confirmID, int cancelID, Handler handler) 
	{
		int result = equipmentService.checkPrinterStatus();
		if(result == 0x62) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
					"温馨提醒: 打印机缺纸, 请更换打印纸！", 
					"确定",
					confirmID, 
					"取消", 
					cancelID,
					cancelID,
					handler);
			return false;
		}
		else if(result == 0x63) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
					"温馨提醒: 打印电压过低, 请接入适配器！", 
					"确定",
					confirmID, 
					"取消", 
					cancelID,
					cancelID,
					handler);
			return false;
		}
		else if(result == 0x64) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
					"温馨提醒: 打印机过热, 请稍候打印！", 
					"确定",
					confirmID, 
					"取消", 
					cancelID,
					cancelID,
					handler);
			return false;
		}
		else if(result != 0) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
					"温馨提醒: 检测打印机状态失败！", 
					"确定",
					confirmID, 
					"取消", 
					cancelID,
					cancelID,
					handler);
			return false;
		}
		return true;
	}
	
	// 判断是否含有2个以上的非数字
	protected boolean isIllegalMoney(String money) 
	{
		int n = 0;
		for (int i = 0; i < money.length(); i++)
		{
			if (money.charAt(i) == '.')
			{
				n++;
			}
		}
		if (n > 1)
		{
			return true;
		}
		if(money.endsWith(".")) 
		{
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			// 是否返回到主菜单?
			back2Main();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// 是否返回到主菜单?
	protected void back2Main() 
	{
		DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
				Statics.EXIT_TIP, 
				"确定", 
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View arg0) 
					{
						finish();
					}
				}, 
	            "取消", 
	            null, 
	            null);
	}
}
