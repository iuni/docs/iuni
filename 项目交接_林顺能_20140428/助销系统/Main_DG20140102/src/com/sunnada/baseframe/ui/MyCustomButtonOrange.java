package com.sunnada.baseframe.ui;

import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.ARGBUtil;

import android.content.Context;
import android.util.AttributeSet;

public class MyCustomButtonOrange extends MyCustomButton
{
	public MyCustomButtonOrange(Context context)
	{
		super(context);
	}

	public MyCustomButtonOrange(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setCustomBgColor();
	}

	public void setCustomBgColor()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			int argb = ARGBUtil.getArgb(2);
			this.setBackgroundColor(argb);
		}
	}
}
