package com.sunnada.baseframe.ui;

import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.ARGBUtil;

import android.content.Context;
import android.util.AttributeSet;

public class MyCustomButtonBlue extends MyCustomButton
{
	public MyCustomButtonBlue(Context context)
	{
		super(context);
	}

	public MyCustomButtonBlue(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setCustomBgColor();
	}

	
	public void setCustomBgColor()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			int argb = ARGBUtil.getArgb(1);
			this.setBackgroundColor(argb);
		}
	}

}
