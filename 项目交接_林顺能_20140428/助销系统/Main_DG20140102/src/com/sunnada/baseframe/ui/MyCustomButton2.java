package com.sunnada.baseframe.ui;

import com.sunnada.baseframe.activity.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyCustomButton2 extends LinearLayout
{
	private ImageView	iv;
	private TextView	tv1;
	private TextView	tv2;
	private int			mOldHeight;
	private int			mOldWidth;
	private Context     mContext;

	public MyCustomButton2(Context context)
	{
		this(context, null);
	}

	public MyCustomButton2(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// 导入布局
		LayoutInflater.from(context).inflate(R.layout.btn_custom2, this, true);
		iv = (ImageView) findViewById(R.id.iv);
		tv1 = (TextView) findViewById(R.id.tv1);
		tv2 = (TextView) findViewById(R.id.tv2);
		mContext = context;
	}

	// 设置图片资源
	public void setImageResource(int resId)
	{
		iv.setImageDrawable(mContext.getResources().getDrawable(resId));
	}

	// 设置文本1显示的文字
	public void setTextViewText1(String text)
	{
		tv1.setText(text);
	}

	// 设置文本2显示的文字
	public void setTextViewText2(String text)
	{
		tv2.setText(text);
	}

	// 设置文本显示的文字大小
	public void setTextViewTextSize(int textViewId,int textSize)
	{
		switch(textViewId)
		{
		case 1:
			tv1.setTextSize(textSize);
			break;
			
		case 2:
			tv2.setTextSize(textSize);
			break;
		}
	}
	
	// 设置图片大小
	public void setImageResource(int resourceId,float scaleW,float scaleH)
	{
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),resourceId);
		bitmap = scalePicture(bitmap,scaleW,scaleH);
		iv.setImageDrawable(new BitmapDrawable(bitmap));
	}
	
	/**
	 * @created 2013-10-03
	 * @description 调整图片的像素
	 */
	public Bitmap scalePicture(Bitmap bitmap, float scaleW, float scaleH)
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		// Matrix比例
		//float scaleWidth = w / width;
		//float scaleHeight = h / height;
		System.out.println("width:"+width);
		System.out.println("height:"+height);

		Matrix matrix = new Matrix();
		// 使用Matrix.postScale设置维度Resize
		matrix.postScale(scaleW, scaleH);
		
		// Resize图片文件至指定大小分辨率
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
		return resizedBitmap;
	}
	
	public void hideWidget(int widgetId)
	{
		switch (widgetId)
		{
			case 0:
				iv.setVisibility(View.GONE);
				break;
			case 1:
				tv1.setVisibility(View.GONE);
				break;
			case 2:
				tv2.setVisibility(View.GONE);
				break;
		}
	}

	public void showWidget(int widgetId)
	{
		switch (widgetId)
		{
			case 0:
				iv.setVisibility(View.VISIBLE);
				break;
			case 1:
				tv1.setVisibility(View.VISIBLE);
				break;
			case 2:
				tv2.setVisibility(View.VISIBLE);
				break;
		}
	}

	// 按钮单击监听
	public void onTouch(MotionEvent event)
	{
		switch (event.getAction()) 
		{
			case MotionEvent.ACTION_DOWN:
				mOldHeight = this.getHeight(); 
				mOldWidth = this.getWidth();
				(this.getLayoutParams()).height = (int) (mOldHeight * 0.9);
				(this.getLayoutParams()).width = (int) (mOldWidth);
				this.setLayoutParams(this.getLayoutParams());
				break;
				
			case MotionEvent.ACTION_UP:
				(this.getLayoutParams()).width = mOldWidth;
				(this.getLayoutParams()).height = mOldHeight;
				 setLayoutParams(this.getLayoutParams());
				break;
		}
	}
}
