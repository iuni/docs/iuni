package com.sunnada.baseframe.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

public class DropDownInfoUtil
{
	private Context			mContext;
	private String			mTitle		= null; // ����
	private String[]		mLabels		= null;
	private String[][]		mContents	= null;
	private LinearLayout	mLayTitle	= null;
	private ScrollView		mLayBody	= null;
	private TextView		mTvTitle	= null;
	//private	LinearLayout	mParent;
	

	public void setLabels(String[] labels)
	{
		this.mLabels = labels;
	}

	public void setContents(String[][] contents)
	{
		this.mContents = contents;
	}

	public void setTitle(String title)
	{
		this.mTitle = title;
	}

	public DropDownInfoUtil(Context context)
	{
		mContext = context;
		mLabels = new String[]
		{ "1111111", "2222222", "1111111", "2222222", "1111111", "2222222" };
		mContents = new String[][]
		{
			{ "23123131231231231231312312312312313123123123123131231231", "4123214421312","4123123123141231231241321312" },
			{ "2312313123123123123131231231", 	 "4123214421312", "4123123123141231231241321312" },
			{ "23123131231231", "4123214421312", "4123123123141231231241321312" },
			{ "23123131231231", "4123214421312", "4123123123141231231241321312" },
			{ "23123131231231", "4123214421312", "4123123123141231231241321312" },
			{ "23123131231231", "4123214421312", "4123123123141231231241321312" } };
	}

	public void create(boolean isExpanded)
	{
		mLayTitle = new LinearLayout(mContext);
		mLayTitle.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		mTvTitle = new TextView(mContext);
		mTvTitle.setLayoutParams(new LayoutParams(227, 45));
		if (isExpanded)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_expanded);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_expanded_orange);
			}
		}
		else
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_folded);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_folded_orange);
			}
			
		}

		mTvTitle.setText(mTitle);
		mTvTitle.setPadding(30, 0, 0, 0);
		mTvTitle.setTextColor(Color.WHITE);
		mTvTitle.setTextSize(22);
		mLayTitle.addView(mTvTitle);

		mLayBody = new ScrollView(mContext);
		mLayBody.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1));
		LinearLayout lay_content = new LinearLayout(mContext);
		lay_content.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		lay_content.setOrientation(LinearLayout.VERTICAL);

		for (int i = 0; i < mLabels.length; i++)
		{
			String label = mLabels[i];
			LinearLayout row = new LinearLayout(mContext);
			int top = (int) mContext.getResources().getDimension(R.dimen.DROP_DOWN_TEXT_PADDINT);
			int bottom = (int) mContext.getResources().getDimension(R.dimen.DROP_DOWN_TEXT_PADDINT);
			row.setPadding(50, top, 0, bottom);
			row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			TextView tv_label = new TextView(mContext);
			tv_label.setLayoutParams(new LayoutParams(177, 45));
			tv_label.setTextSize((int) mContext.getResources().getDimension(R.dimen.DROP_DOWN_TEXT_SIZE_TITLE));
			tv_label.setTextColor(Color.parseColor("#A4A4A4"));
			tv_label.setText("- " + label);
			row.addView(tv_label);
			View devider = new View(mContext);
			devider.setLayoutParams(new LayoutParams(1, LayoutParams.MATCH_PARENT));
			devider.setBackgroundColor(Color.parseColor("#E1E1E1"));
			row.addView(devider);

			String[] rowValue = mContents[i];
			for (int j = 0; j < rowValue.length; j++)
			{
				String modelValue = rowValue[j];
				TextView tv_value = new TextView(mContext);
				tv_value.setLayoutParams(new LayoutParams((int) mContext.getResources().getDimension(R.dimen.DETAIL_WIDTH), LayoutParams.WRAP_CONTENT));
				tv_value.setPadding(20, 0, 20, 0);
				tv_value.setTextColor(Color.parseColor("#A4A4A4"));
				tv_value.setTextSize((int) mContext.getResources().getDimension(R.dimen.DROP_DOWN_TEXT_SIZE_CONTENT));
				tv_value.setGravity(Gravity.CENTER);
				tv_value.setText(modelValue);

				if (j != 0)
				{
					devider = new View(mContext);
					devider.setLayoutParams(new LayoutParams(1, LayoutParams.MATCH_PARENT));
					devider.setBackgroundColor(Color.parseColor("#E1E1E1"));
					row.addView(devider);
				}
				row.addView(tv_value);
			}

			devider = new View(mContext);
			devider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
			devider.setBackgroundColor(Color.parseColor("#E1E1E1"));
			lay_content.addView(devider);
			lay_content.addView(row);
			if (i == mLabels.length - 1)
			{
				devider = new View(mContext);
				devider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
				devider.setBackgroundColor(Color.parseColor("#E1E1E1"));
				lay_content.addView(devider);
			}
		}
		mLayBody.addView(lay_content);
		if (!isExpanded)
		{
			mLayBody.setVisibility(View.GONE);
		}
		return;
	}

	public LinearLayout getTitle()
	{
		return mLayTitle;
	}

	public ScrollView getBody()
	{
		return mLayBody;
	}

	public void expand()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_expanded);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_expanded_orange);
		}
		mLayBody.setVisibility(View.VISIBLE);
	}

	public void fold()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_folded);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mTvTitle.setBackgroundResource(R.drawable.bg_compare_title_folded_orange);
		}
		mLayBody.setVisibility(View.GONE);
	}

	private int	index	= 0;
	public void createGroup(final DropDownInfoUtil... util1)
	{
		for (int i = 0; i < util1.length; i++)
		{
			final int temp = i;
			LinearLayout title = util1[i].getTitle();
			title.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (index == temp)
					{
						return;
					}
					if(index < util1.length)
					{
						util1[index].fold();
					}
					util1[temp].expand();

					index = temp;
				}
			});
		}
	}
}
