package com.sunnada.baseframe.bean;
/**
 * File: Number.java
 * Description: 规则
 * @author 丘富铨
 * @date 2013-1-17
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class Rule
{
	private int id = -1;//编号
	private String content  = null;//内容
	
	public Rule()
	{
		super();
	}
	
	public Rule(int id, String content)
	{
		super();
		this.id = id;
		this.content = content;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}

	@Override
	public String toString()
	{
		return this.content;
	}
	
}
