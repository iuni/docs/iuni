package com.sunnada.baseframe.bean;

/**
 * 
 * @author class CRCData 全局各类CRC
 * 
 */
public class CRCData
{

	/**
	 * 0x01 业务菜单
	 */
	public static String crcmenu = "0000";

	/**
	 * 0x02:套餐信息(产品包信息)
	 */
	public static String crcmeal = "0000";

	/**
	 * 0x03:增值业务列表
	 */
	public static String crcincrement = "0000";

	/**
	 * 0x04:特服业务列表
	 */
	public static String crcespeciallyservice = "0000";

	/**
	 * 0x05:选号规则列表
	 */
	public static String crcselnorule = "0000";

	/**
	 * 0x06:品牌列表
	 */
	public static String crcbrand = "0000";

	/**
	 * 0x07:开机欢迎语更新
	 */
	public static String crcwelcome = "0000";

	/**
	 * 0x08:打印凭条广告信息
	 */
	public static String crcprint = "0000";

	/**
	 * 0x09:终端收件箱内容更新
	 */
	public static String crcinbox = "0000";

	/**
	 * 0x0A:3G套餐信息
	 */
	public static String crc3gmeal = "0000";

}
