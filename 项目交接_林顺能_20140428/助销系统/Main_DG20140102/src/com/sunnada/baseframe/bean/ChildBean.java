package com.sunnada.baseframe.bean;

public class ChildBean
{
	private  String                   mSubject;
	private  String                   mDetail;
	
	public String getmSubject() 
	{
		return mSubject;
	}
	
	public void setmSubject(String mSubject) 
	{
		this.mSubject = mSubject;
	}
	
	public String getmDetail() 
	{
		return mDetail;
	}
	
	public void setmDetail(String mDetail) 
	{
		this.mDetail = mDetail;
	}
}
