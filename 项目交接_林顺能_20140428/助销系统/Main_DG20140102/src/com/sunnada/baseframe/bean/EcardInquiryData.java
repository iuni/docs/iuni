package com.sunnada.baseframe.bean;

public class EcardInquiryData 
{
	private int                   mMoney;
	private String                mTime;
	private String                mECardKey;
	private String                mFlowNo;
	
	public int getmMoney() 
	{
		return mMoney;
	}
	
	public void setmMoney(int mMoney) 
	{
		this.mMoney = mMoney;
	}
	
	public String getmTime() 
	{
		return mTime;
	}
	
	public void setmTime(String mTime)
	{
		this.mTime = mTime;
	}
	
	public String getmECardKey() 
	{
		return mECardKey;
	}
	
	public void setmECardKey(String mECardKey) 
	{
		this.mECardKey = mECardKey;
	}

	public String getmFlowNo() 
	{
		return mFlowNo;
	}

	public void setmFlowNo(String mFlowNo) 
	{
		this.mFlowNo = mFlowNo;
	}
}
