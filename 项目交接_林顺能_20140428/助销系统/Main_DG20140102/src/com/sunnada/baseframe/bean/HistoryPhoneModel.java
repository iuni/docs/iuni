package com.sunnada.baseframe.bean;

import com.sunnada.baseframe.database.HistoryPhoneYhgjDao;
import com.sunnada.baseframe.database.reflect.FieldDataType;
import com.sunnada.baseframe.database.reflect.FieldInfo;
import com.sunnada.baseframe.database.reflect.TableInfo;

@TableInfo(tableName = HistoryPhoneYhgjDao.TABLE_NAME, describe = "历史手机参数信息", alias = "")
public class HistoryPhoneModel extends PhoneModel
{
	private static final long	serialVersionUID	= 1L;
	@FieldInfo(fieldName = HistoryPhoneYhgjDao.EDIT_TIME, fieldType = FieldDataType.DATATYPE_STRING, describe = "最后编辑时间")
	private String edittime			= "";			// 最后操作时间,保存历史记录时使用
	
	
	public String getEdittime()
	{
		return edittime;
	}
	
	public void setEdittime(String edittime)
	{
		this.edittime = edittime;
	}
}
