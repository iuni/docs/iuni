package com.sunnada.baseframe.bean;

public class PackageInfo
{
	private String	mCall				= "";	// 国内语音拨打分钟数
	private String	mFlow				= "";	// 国内流量
	private String	mMsgCount			= "";	// 国内短信发送条数
	private String	mFreeIncomingCall	= "";	// 接听免费
	private String	mFeeCall			= "";	// 国内语音拨打费用（元/分钟）
	private String	mFeeFlow			= "";	// 国内流量费用
	private String	mFeeVedioCall		= "";	// 国内可视电话费用
	private String	mOther				= "";	// 其他业务

	
	public PackageInfo()
	{
	}

	public PackageInfo(String mCall, String mFlow, String mMsgCount, String mFreeIncomingCall, String mFeeCall,
			String mFeeFlow, String mFeeVedioCall, String mOther)
	{
		this.mCall = mCall;
		this.mFlow = mFlow;
		this.mMsgCount = mMsgCount;
		this.mFreeIncomingCall = mFreeIncomingCall;
		this.mFeeCall = mFeeCall;
		this.mFeeFlow = mFeeFlow;
		this.mFeeVedioCall = mFeeVedioCall;
		this.mOther = mOther;
	}

	public String getmCall()
	{
		return mCall;
	}

	public void setmCall(String mCall)
	{
		this.mCall = mCall;
	}

	public String getmFlow()
	{
		return mFlow;
	}

	public void setmFlow(String mFlow)
	{
		this.mFlow = mFlow;
	}

	public String getmMsgCount()
	{
		return mMsgCount;
	}

	public void setmMsgCount(String mMsgCount)
	{
		this.mMsgCount = mMsgCount;
	}

	public String getmFreeIncomingCall()
	{
		return mFreeIncomingCall;
	}

	public void setmFreeIncomingCall(String mFreeIncomingCall)
	{
		this.mFreeIncomingCall = mFreeIncomingCall;
	}

	public String getmFeeCall()
	{
		return mFeeCall;
	}

	public void setmFeeCall(String mFeeCall)
	{
		this.mFeeCall = mFeeCall;
	}

	public String getmFeeFlow()
	{
		return mFeeFlow;
	}

	public void setmFeeFlow(String mFeeFlow)
	{
		this.mFeeFlow = mFeeFlow;
	}

	public String getmFeeVedioCall()
	{
		return mFeeVedioCall;
	}

	public void setmFeeVedioCall(String mFeeVedioCall)
	{
		this.mFeeVedioCall = mFeeVedioCall;
	}

	public String getmOther()
	{
		return mOther;
	}

	public void setmOther(String mOther)
	{
		this.mOther = mOther;
	}

}
