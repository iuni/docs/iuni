package com.sunnada.baseframe.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MsgBean implements Serializable
{
	private String			mKeyID;
	private String 			mKeyTitle;
	private String 			mKeyText;

	public MsgBean() 
	{
		
	}

	public MsgBean(String id, String title, String text) 
	{        
		super();
		
		this.mKeyID = id;        
		this.mKeyTitle = title;
		this.mKeyText = text;
	}    

	@Override
	public String toString() 
	{
		return "key_id = " + mKeyID + ", key_title = " + mKeyTitle + ", key_text" + mKeyText;
	}
	
	// 获取行号
	public String getID() 
	{
		return mKeyID;
	}

	// 设置行号
	public void setID(String id) 
	{
		this.mKeyID = id;
	}

	// 获取标题
	public String getTitle() 
	{
		return mKeyTitle;
	}

	// 设置标题
	public void setTitle(String key) 
	{
		this.mKeyTitle = key;
	}

	// 获取内容
	public String getText() 
	{
		return mKeyText;
	}

	// 设置内容
	public void setText(String text) 
	{
		this.mKeyText = text;
	}
}
