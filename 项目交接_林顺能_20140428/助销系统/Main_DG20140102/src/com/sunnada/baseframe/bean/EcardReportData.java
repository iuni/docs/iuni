package com.sunnada.baseframe.bean;

public class EcardReportData 
{
	public int           mQueryType; // 查询类型
	public String        mCardKey;   // 卡密
	public String        mFlowNo;    // 流水号
	
	public int getQueryType() 
	{
		return mQueryType;
	}
	
	public void setQueryType(int queryType) 
	{
		this.mQueryType = queryType;
	}
	
	public String getCardKey() 
	{
		return mCardKey;
	}
	
	public void setCardKey(String cardKey) 
	{
		this.mCardKey = cardKey;
	}
	
	public String getFlowNo() 
	{
		return mFlowNo;
	}
	
	public void setFlowNo(String flowNo) 
	{
		this.mFlowNo = flowNo;
	}
}
