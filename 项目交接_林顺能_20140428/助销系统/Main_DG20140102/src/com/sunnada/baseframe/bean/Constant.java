package com.sunnada.baseframe.bean;

public class Constant 
{
 
	public   static final String                                PAY_FIX                ="fix";   //固话sdk文件名称  
	public   static final String                                PAY_PHS                ="phs";   //小灵通sdk文件名称                     
	public   static final String                                PAY_LAN                ="lan";   //宽带充值sdka文件名称     
	public   static final String                                PAY_PHONE              ="phone"; //手机充值sdka文件名称
	public   static final String                                BUYCARD                ="electricard";
	public   static final byte                                  RCHARGE=0x01;   // 手机充值
	public   static final byte                                  PAYMENT=0x04;   // 手机缴费
}
