package com.sunnada.baseframe.bean;

import android.util.Log;
import android.widget.ImageView;

import com.sunnada.baseframe.util.StringUtil;

public class RecommendPackage
{
	private String 		mId;			// 推荐包ID
	private ProductInfo	mProduct;		// 套餐包
	private PhoneModel 	mPhoneModel;	// 手机信息
	private boolean 	mIsBindPhone;	// 是否有绑定手机
	private boolean  	mIsParamLoaded;	// 信息是否已下载
	private boolean 	mIsSelected;	// 是否被选中

	public RecommendPackage()
	{
		mPhoneModel = new PhoneModel();
		mProduct    = new ProductInfo();
	}

	public String getmId()
	{
		return mId;
	}

	public void setmId(String mId)
	{
		this.mId = mId;
	}

	public String getTitle()
	{
		return mProduct.getmActivityContent() + "  " + mProduct.getmProductName();
	}

	public ProductInfo getmProduct()
	{
		return mProduct;
	}

	public void setmProduct(ProductInfo mProduct)
	{
		this.mProduct = mProduct;
	}

	public PhoneModel getmPhoneModel()
	{
		return mPhoneModel;
	}

	public void setmPhoneModel(PhoneModel mPhoneModel)
	{
		this.mPhoneModel = mPhoneModel;
		mIsBindPhone = true;
	}

	public boolean ismIsBindPhone()
	{
		return mIsBindPhone;
	}

	public void setmIsBindPhone(boolean mIsBindPhone)
	{
		this.mIsBindPhone = mIsBindPhone;
	}

	public boolean ismIsParamLoaded()
	{
		return mIsParamLoaded;
	}

	public void setmIsParamLoaded(boolean mIsParamLoaded)
	{
		this.mIsParamLoaded = mIsParamLoaded;
	}

	public void setParams(RecommendPackage temp)
	{
		mPhoneModel.setParams(temp.getmPhoneModel());
		mProduct.setParams(temp.getmProduct());
	}
	
	public void showImageView(ImageView imageView)
	{
		if(!StringUtil.isEmptyOrNull(getmProduct().getmBigPic()))
		{
			Log.e("3", "product");
			getmProduct().showInImageView(imageView);
		}
		else 
		{
			Log.e("3", "phone");
			getmPhoneModel().showInImageView(imageView);
		} 
	}
	
	@Override
	public boolean equals(Object o) 
	{
		if (this == o) 
		{
			return true;
		} 
		else if (o == null) 
		{
			return false;
		}
		
		if (o instanceof RecommendPackage) 
		{
			if (mId.equals(((RecommendPackage) o).getmId())) 
			{
				return true;
			}
		}
		return false;
	}

	public boolean ismIsSelected()
	{
		return mIsSelected;
	}

	public void setmIsSelected(boolean mIsSelected)
	{
		this.mIsSelected = mIsSelected;
	}
}
