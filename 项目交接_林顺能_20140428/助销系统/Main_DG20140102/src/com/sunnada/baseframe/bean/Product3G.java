package com.sunnada.baseframe.bean;

public class Product3G {
	private int code;
	private int haveProCode;
	private int brand;
	private String name;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public int getHaveProCode() {
		return haveProCode;
	}
	public void setHaveProCode(int haveProCode) {
		this.haveProCode = haveProCode;
	}
	public int getBrand() {
		return brand;
	}
	public void setBrand(int brand) {
		this.brand = brand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return  name ;
	}
	
	
	
}
