package com.sunnada.baseframe.bean;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.BitmapPoolUtil.onImageGotListener;

public class ProductInfo implements Serializable
{
	private static final long	serialVersionUID	= 1L;
	
	private String 	id;					// ID号
	private String  mPrice;				// 价格
	private String 	mDescribe;  		// 描述，宣传口号
	private String 	mAreaLong; 			// 归属地  如：全国-福建省-福州
	private String 	mAreaShort; 		// 归属地  如：福州
	private String 	mAreaCode;	 		// 区域编码
	
	private	String  mContractID;		// 合约ID
	private String  mContractCode;		// 合约编码
	private String  mContractName;		// 合约名称
	private String  mContractDate;		// 合约期限
	
	private String  mProductID;			// 套餐ID   原来用套餐编码，长度过长，web后来改用套餐ID
	private String  mProductCode;		// 套餐编码
	private String 	mProductName;		// 套餐名称
	private String  mProductPrice;		// 套餐档次
	private String 	mProductType;		// 套餐类型
	
	private	String  mActivityType;		// 活动类型
	private String  mActivityContent;  	// 活动内容
	private String  mActivityDetails;   // 活动详情
	private String	mMarktingTag;		// 营销标签 -- 1 热销 3 特惠  5 新品
	
	private String  mPicPath; 			// 本地图片路径
	private String 	mBigPic;			// 大图
	
	// 套餐详情
	private String	mFlow1;				// 国内流量
	private String	mCallTime;			// 国内拨打分钟数
	private String	mMsgCount;			// 国内短信发送条数
	private String	mFree;				// 接听免费
	private String	mCallFee;			// 国内拨打费用
	private String	mFlow2;				// 国内流量费用
	private String	mVideoCall;			// 视频通话分钟数
	private String	mOther;				// 其他业务
	
	private String	mIsGift;			// 是否有赠品
	private String  mGiftInfo;			// 赠品说明
	private String 	mGift1;				// 赠品1
	private String 	mGift2;				// 赠品2
	private String 	mGift3;				// 赠品3
	private String 	mGift4;				// 赠品4
	private String 	mGift5;				// 赠品5
	
	private boolean  mIsParamLoaded;	// 信息是否已下载

	
	public String getTitle()
	{
		return mActivityContent + "  " + mProductName;
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getmDescribe()
	{
		return mDescribe;
	}

	public void setmDescribe(String mDescribe)
	{
		this.mDescribe = mDescribe;
	}

	public String getmAreaLong()
	{
		return mAreaLong;
	}

	public void setmAreaLong(String mBelongArea)
	{
		this.mAreaLong = mBelongArea;
	}

	public String getmProductCode()
	{
		return mProductCode;
	}

	public void setmProductCode(String mPackageCode)
	{
		this.mProductCode = mPackageCode;
	}
 
	public String getmProductName()
	{
		return mProductName;
	}
	
	public String getmProductID()
	{
		return mProductID;
	}

	public void setmProductID(String mProductID)
	{
		this.mProductID = mProductID;
	}

	public String getmProductPrice()
	{
		return mProductPrice;
	}

	public void setmProductPrice(String mProductPrice)
	{
		this.mProductPrice = mProductPrice;
	}

	public String getmProductType()
	{
		return mProductType;
	}

	public void setmProductType(String mProductType)
	{
		this.mProductType = mProductType;
	}

	public void setmProductName(String mPackage)
	{
		this.mProductName = mPackage;
	}

	public String getmFlow1()
	{
		return mFlow1;
	}

	public void setmFlow1(String mFlow1)
	{
		this.mFlow1 = mFlow1;
	}

	public String getmCallTime()
	{
		return mCallTime;
	}

	public void setmCallTime(String mCallTime)
	{
		this.mCallTime = mCallTime;
	}

	public String getmMsgCount()
	{
		return mMsgCount;
	}

	public void setmMsgCount(String mMsgCount)
	{
		this.mMsgCount = mMsgCount;
	}

	public String getmFree()
	{
		return mFree;
	}

	public void setmFree(String mFree)
	{
		this.mFree = mFree;
	}

	public String getmCallFee()
	{
		return mCallFee;
	}

	public void setmCallFee(String mCallFee)
	{
		this.mCallFee = mCallFee;
	}

	public String getmFlow2()
	{
		return mFlow2;
	}

	public void setmFlow2(String mFlow2)
	{
		this.mFlow2 = mFlow2;
	}

	public String getmVideoCall()
	{
		return mVideoCall;
	}

	public void setmVideoCall(String mVideoCall)
	{
		this.mVideoCall = mVideoCall;
	}

	public String getmOther()
	{
		return mOther;
	}

	public void setmOther(String mOther)
	{
		this.mOther = mOther;
	}

	public String getmActivityType()
	{
		return mActivityType;
	}

	public void setmActivityType(String mActivityType)
	{
		this.mActivityType = mActivityType;
		// 01存话费送话费 02购手机入网送话费 03预存话费送手机
		if(mActivityType.endsWith("01"))
		{
			this.setmActivityContent("存话费送话费");
		}
		else if(mActivityType.endsWith("02"))
		{
			this.setmActivityContent("购手机入网送话费");
		}
		else if(mActivityType.endsWith("03"))
		{
			this.setmActivityContent("预存话费送手机");
		}
	}

	public String getmActivityContent()
	{
		return mActivityContent;
	}
 
	public void setmActivityContent(String mActivityContent)
	{
		this.mActivityContent = mActivityContent;
	}
	
	public String getmActivityDetails()
	{
		return mActivityDetails;
	}

	public void setmActivityDetails(String mActivityDetails)
	{
		this.mActivityDetails = mActivityDetails;
	}

	public String getmMarktingTag()
	{
		return mMarktingTag;
	}

	public void setmMarktingTag(String mMarktingTag)
	{
		this.mMarktingTag = mMarktingTag;
	}

	public String getmContractDate()
	{
		return mContractDate;
	}

	public void setmContractDate(String mSpacialDate)
	{
		this.mContractDate = mSpacialDate;
	}

	public boolean ismIsParamLoaded()
	{
		return mIsParamLoaded;
	}

	public void setmIsParamLoaded(boolean mIsParamLoaded)
	{
		this.mIsParamLoaded = mIsParamLoaded;
	}

	public void setParams(ProductInfo temp)
	{
		
	}
	
	public String getmPrice()
	{
		return mPrice;
	}
	
	public String getmPriceDes()
	{
		return "￥" + mPrice;
	}

	public void setmPrice(String mPrice)
	{
		this.mPrice = mPrice;
	}
	
	public String getmPicPath()
	{
		return mPicPath;
	}

	public void setmPicPath(String mPicPath)
	{
		this.mPicPath = mPicPath;
	}
	
	public String getmBigPic()
	{
		return mBigPic;
	}

	public void setmBigPic(String mBigPic)
	{
		//String [] bigs = mBigPic.split(",");// web说这个有可能也会推多张图片
		String [] bigs = this.getmPicPath().split(",");
		if(bigs != null && bigs.length > 0)
		{
			this.mBigPic = bigs[0];
		}
		else
		{
			this.mBigPic = "";
		}
	}

	public String getmAreaShort()
	{
		return mAreaShort;
	}

	public void setmAreaShort(String mAreaShort)
	{
		this.mAreaShort = mAreaShort;
	}

	public String getmAreaCode()
	{
		return mAreaCode;
	}

	public void setmAreaCode(String mAreaCode)
	{
		this.mAreaCode = mAreaCode;
	}

	public String getmContractID()
	{
		return mContractID;
	}

	public void setmContractID(String mContractID)
	{
		this.mContractID = mContractID;
	}

	public String getmContractCode()
	{
		return mContractCode;
	}

	public void setmContractCode(String mContractCode)
	{
		this.mContractCode = mContractCode;
	}

	public String getmContractName()
	{
		return mContractName;
	}

	public void setmContractName(String mContractName)
	{
		this.mContractName = mContractName;
	}

	public String getmIsGift()
	{
		return mIsGift;
	}

	public void setmIsGift(String mIsGift)
	{
		this.mIsGift = mIsGift;
	}

	public String getmGiftInfo()
	{
		return mGiftInfo;
	}

	public void setmGiftInfo(String mGiftInfo)
	{
		this.mGiftInfo = mGiftInfo;
	}

	public String getmGift1()
	{
		return mGift1;
	}

	public void setmGift1(String mGift1)
	{
		this.mGift1 = mGift1;
	}

	public String getmGift2()
	{
		return mGift2;
	}

	public void setmGift2(String mGift2)
	{
		this.mGift2 = mGift2;
	}

	public String getmGift3()
	{
		return mGift3;
	}

	public void setmGift3(String mGift3)
	{
		this.mGift3 = mGift3;
	}

	public String getmGift4()
	{
		return mGift4;
	}

	public void setmGift4(String mGift4)
	{
		this.mGift4 = mGift4;
	}

	public String getmGift5()
	{
		return mGift5;
	}

	public void setmGift5(String mGift5)
	{
		this.mGift5 = mGift5;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public void showInImageView(final ImageView view) 
	{
		final Bitmap bitmap = BitmapPoolUtil.getBitmap(Statics.PATH_PIC + mBigPic,
				ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + mBigPic, 
				new onImageGotListener() 
				{
					@Override
					public void onImageGot(boolean result) 
					{
						System.out.println("从网络...");
						if(result == true)
						{
							System.out.println("从网络下载完成后从本地去取...");
							final Bitmap bitmap = BitmapPoolUtil.getBitmap(Statics.PATH_PIC + mBigPic, 
									null, null, new int[] {210, 280});
							view.post(new Runnable() 
							{
								@Override
								public void run() 
								{
									if(bitmap != null)
									{
										view.setImageBitmap(bitmap);
									}
									else
									{
										view.setImageResource(R.drawable.bg_model_loading);
									}
								}
							});
						} 
						else
						{
							view.post(new Runnable() 
							{
								@Override
								public void run() 
								{
									view.setImageResource(R.drawable.bg_no_model);
								}
							});
						}
						
					}
				}, new int[]{200, 300});
				
		if(bitmap != null) 
	 	{
			System.out.println("从本地....");
			view.setImageBitmap(bitmap);
		}
		else
		{
			view.setImageResource(R.drawable.bg_model_loading);
		}
	}

	public String[] getPicPaths() 
	{
		String[] result = this.getmPicPath().split(",");
		for (int i = 0; i < result.length; i++) 
		{
			result[i] = Statics.PATH_PIC + result[i];
		}
		return result;
	}
	
	public String[] getServerPicPaths() 
	{
		String[] result = this.getmPicPath().split(",");
		for (int i = 0; i < result.length; i++) 
		{
			result[i] = ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + result[i];
		}
		return result;
	}

	@Override
	public boolean equals(Object o) 
	{
		if (this == o) 
		{
			return true;
		} 
		else if (o == null) 
		{
			return false; 
		}
		
		if (o instanceof RecommendPackage) 
		{
			if (id.equals(((ProductInfo) o).getId())) 
			{
				return true;
			}
		}
		return false;
	}

	public String getPackDetails()
	{ 
		String s = "套餐包含国内语音拨打分钟数"+ mCallTime + "分钟，国内流量" + mFlow1
				 + "，国内接听免费（含可视电话）。套餐超出后，国内语音拨打"+ mCallFee
				 + "元/分钟，国内流量" + mFlow2+ "元/KB," +"国内可视电话拨打"+ mVideoCall 
				 + "元/分钟。其他执行标准资费。";
		return s;
	}
}
