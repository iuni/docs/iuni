package com.sunnada.baseframe.bean;

// 身份证信息
public class IdentifyMsg 
{
	public String name				= "";
	public String identify_no		= "";
	public String sex				= "";
	public String minzu				= "";  		// 民族
	public String birth_date		= ""; 		// 出生
	public String address			= "";
	public String sign_org			= "";		// 签发机关
	public String begin_time		= "";		// 有效期开始时间
	public String end_time			= "";		// 有效期结束时间
}
