package com.sunnada.baseframe.bean;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.BitmapPoolUtil.onImageGotListener;

@SuppressWarnings("serial")
public class PhoneModel implements Serializable 
{
	private String id 				= "";			// ID号
	private String name 			= "";			// 手机名称
	private String screenSize 		= "";			// 主屏尺寸
	private String op 				= "";			// 操作系统
	private String price 			= "";			// 价格信息
	private String picPath 			= "";			// 本地图片路径
	private String band 			= "";			// 手机品牌
	private String color 			= "";			// 手机颜色
	private String screenParam 		= "";			// 主屏参数
	private String net 				= "";			// 网络模式
	private String coreNum 			= "";			// 核心数量
	private String cpu 				= "";			// CPU频率
	private String size 			= "";			// 机身外观
	private String rom 				= "";			// 机身内存
	private String ram 				= "";			// 运行内存
	private String extendRom 		= "";			// 最大扩展内存
	private String batteryStorage 	= "";			// 电池容量
	private String battery 			= "";			// 电池
	private String musicFormat 		= "";			// 音乐格式
	private String vedioFormat 		= "";			// 视频格式
	private String java 			= "";			// 是否支持JAVA
	private String radio 			= "";			// 收音机
	private String record 			= "";			// 录音功能
	private String camera1 			= "";			// 主摄像头参数
	private String camera2 			= "";			// 副摄像头参数
	private String zoom 			= "";			// 摄像头变焦模式
	private String dataTrans 		= "";			// 数据传输
	private String gps 				= "";			// GPS
	private String callTime 		= "";			// 理论通话时间
	private String waitTime 		= "";			// 理论待机时间
	private String access 			= "";			// 配套设备
	private String bigPic 			= "";			// 大图名称
	
	private String isRecommened 	= "0";
	public boolean isParamLoaded 	= false;
	
	public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}

	public String getNameDes() 
	{
		return band +"/"+ name;
	}
	
	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getScreenSize() 
	{
		return screenSize;
	}

	public void setScreenSize(String screenSize) 
	{
		this.screenSize = screenSize;
	}

	public String getOp() 
	{
		return op;
	}

	public void setOp(String op) 
	{
		this.op = op;
	}

	public String getPrice() 
	{
		return price;
	}

	public String getPriceDes() 
	{
		return "￥" + price;
	}

	public void setPrice(String price) 
	{
		this.price = price;
	}

	public String getPicPath() 
	{
		return picPath;
	}

	public void setPicPath(String picPath)
	{
		this.picPath = picPath;
	}
	
	public void showInImageView(final ImageView view) 
	{
		Bitmap bitmap = BitmapPoolUtil.getBitmap(Statics.PATH_PIC + getId() + "/" + bigPic,
				ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + bigPic, 
				new onImageGotListener() 
				{
					@Override
					public void onImageGot(boolean result) 
					{
						System.out.println("从网络...");
						if(result == true)
						{
							view.post(new Runnable() 
							{
								@Override
								public void run() 
								{
									System.out.println("从网络下载完成后从本地去取...");
									Bitmap bitmap = BitmapPoolUtil.getBitmap(Statics.PATH_PIC + getId() + "/" + bigPic, 
											null, null, null);
									view.setImageBitmap(bitmap);
								}
							});
						}
						else
						{
							view.post(new Runnable() 
							{
								@Override
								public void run() 
								{
									view.setImageResource(R.drawable.bg_no_model);
								}
							});
							
						}
					}
				}, new int[]{210, 280});
		
		if(bitmap == null) 
		{
			view.setImageResource(R.drawable.bg_model_loading);
		}
		else
		{
			System.out.println("从本地....");
			view.setImageBitmap(bitmap);
		}
	}

	public String[] getPicPaths() 
	{
		String[] result = this.getPicPath().split(",");
		for (int i = 0; i < result.length; i++) 
		{
			result[i] = Statics.PATH_PIC + getId() + "/pics/" + result[i];
		}
		return result; 
	}
	
	public String[] getServerPicPaths() 
	{
		String[] result = this.getPicPath().split(",");
		for (int i = 0; i < result.length; i++) 
		{
			result[i] = ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + result[i];
		}
		return result;
	}

	public void setParams(PhoneModel model) 
	{
		this.setAccess(model.getAccess());
		this.setBand(model.getBand());
		this.setBattery(model.getBattery());
		this.setBatteryStorage(model.getBatteryStorage());
		this.setBigPic(model.getBigPic());
		this.setCallTime(model.getCallTime());
		this.setCamera2(model.getCamera2());
		this.setCamera1(model.getCamera1());
		this.setColor(model.getColor());
		this.setCoreNum(model.getCoreNum());
		this.setCpu(model.getCpu());
		this.setDataTrans(model.getDataTrans());
		this.setExtendRom(model.getExtendRom());
		this.setGps(model.getGps());
		this.setJava(model.getJava());
		this.setMusicFormat(model.getMusicFormat());
		this.setName(model.getName());
		this.setNet(model.getNet());
		this.setOp(model.getOp());
		this.setPicPath(model.getPicPath());
		this.setPrice(model.getPrice());
		this.setRadio(model.getRadio());
		this.setRam(model.getRam());
		this.setRecord(model.getRecord());
		this.setRom(model.getRom());
		this.setScreenParam(model.getScreenParam());
		this.setScreenSize(model.getScreenSize());
		this.setSize(model.getSize());
		this.setVedioFormat(model.getVedioFormat());
		this.setWaitTime(model.getWaitTime());
		this.setZoom(model.getZoom());
		this.setIsRecommened(model.getIsRecommened());
		this.isParamLoaded = model.isParamLoaded;
	}

	public String getBand() 
	{
		return band;
	}

	public void setBand(String band) 
	{
		this.band = band;
	}

	public String getColor() 
	{
		return color;
	}

	public void setColor(String color) 
	{
		this.color = color;
	}

	public String getScreenParam() 
	{
		return screenParam;
	}

	public void setScreenParam(String screenParam) 
	{
		this.screenParam = screenParam;
	}

	public String getNet() 
	{
		return net;
	}

	public void setNet(String net) 
	{
		this.net = net;
	}

	public String getCoreNum() 
	{
		return coreNum;
	}

	public void setCoreNum(String coreNum) 
	{
		this.coreNum = coreNum;
	}

	public String getCpu() 
	{
		return cpu;
	}

	public void setCpu(String cpu) 
	{
		this.cpu = cpu;
	}

	public String getSize() 
	{
		return size;
	}

	public void setSize(String size) 
	{
		this.size = size;
	}

	public String getRom() 
	{
		return rom;
	}

	public void setRom(String rom) 
	{
		this.rom = rom;
	}

	public String getRam() 
	{
		return ram;
	}

	public void setRam(String ram) 
	{
		this.ram = ram;
	}

	public String getExtendRom() 
	{
		return extendRom;
	}

	public void setExtendRom(String extendRom) 
	{
		this.extendRom = extendRom;
	}

	public String getBatteryStorage() 
	{
		return batteryStorage;
	}

	public void setBatteryStorage(String batteryStorage) 
	{
		this.batteryStorage = batteryStorage;
	}

	public String getBattery() 
	{
		return battery;
	}

	public void setBattery(String battery) 
	{
		this.battery = battery;
	}

	public String getMusicFormat() 
	{
		return musicFormat;
	}

	public void setMusicFormat(String musicFormat) 
	{
		this.musicFormat = musicFormat;
	}

	public String getVedioFormat() 
	{
		return vedioFormat;
	}

	public void setVedioFormat(String vedioFormat) 
	{
		this.vedioFormat = vedioFormat;
	}

	public String getJava() 
	{
		return java;
	}

	public void setJava(String java) 
	{
		this.java = java;
	}

	public String getRadio() 
	{
		return radio;
	}

	public void setRadio(String radio)
	{
		this.radio = radio;
	}

	public String getRecord() 
	{
		return record;
	}

	public void setRecord(String record) 
	{
		this.record = record;
	}

	public String getCamera1() 
	{
		return camera1;
	}

	public void setCamera1(String camera1) 
	{
		this.camera1 = camera1;
	}

	public String getCamera2() 
	{
		return camera2;
	}

	public void setCamera2(String camera2) 
	{
		this.camera2 = camera2;
	}

	public String getZoom() 
	{
		return zoom;
	}

	public void setZoom(String zoom) 
	{
		this.zoom = zoom;
	}

	public String getDataTrans() 
	{
		return dataTrans;
	}

	public void setDataTrans(String dataTrans) 
	{
		this.dataTrans = dataTrans;
	}

	public String getGps() 
	{
		return gps;
	}

	public void setGps(String gps) 
	{
		this.gps = gps;
	}

	public String getCallTime() 
	{
		return callTime;
	}

	public void setCallTime(String callTime) 
	{
		this.callTime = callTime;
	}

	public String getWaitTime() 
	{
		return waitTime;
	}

	public void setWaitTime(String waitTime) 
	{
		this.waitTime = waitTime;
	}

	public String getAccess() 
	{
		return access;
	}

	public void setAccess(String access) 
	{
		this.access = access;
	}

	public String getBigPic() 
	{
		return bigPic;
	}

	public void setBigPic(String clearPic) 
	{
		String [] bigs = clearPic.split(",");// web说这个有可能也会推多张图片
		this.bigPic = bigs[0];
	}
	
	public String getIsRecommened() 
	{
		return isRecommened;
	}

	public void setIsRecommened(String isRecommened) 
	{
		this.isRecommened = isRecommened;
	}

	@Override
	public boolean equals(Object o) 
	{
		if (this == o) 
		{
			return true;
		} 
		else if (o == null) 
		{
			return false;
		}
		
		if (o instanceof PhoneModel) 
		{
			if (id.equals(((PhoneModel) o).getId())) 
			{
				return true;
			}
		}
		return false;
	}
}
