package com.sunnada.baseframe.bean;

import java.io.Serializable;

import com.sunnada.baseframe.util.StringUtil;

@SuppressWarnings("serial")
public class ContractData implements Serializable 
{
	public String 		actName 			= "";			// 活动名称
	public int 			actType				= 0;			// 活动编码
	public String 		timeLimit 			= "";			// 合约期限
	public int 			proID 				= 0;			// 套餐ID
	public int 			proMoney 			= 0;			// 套餐金额
	public int 			proType 			= 0;			// 套餐付费类型
	public String 		planType 			= "";			// 计划类型
	public String 		planID 				= "";			// 合约ID
	public String 		planContent			= "";			// 合约计划内容
	//public int 		actMoney 			= 0;			// 预存话费
	//public int 		phoneMoney 			= 0;			// 手机款
	
	public String getContractDes() 
	{
		if(StringUtil.isEmptyOrNull(timeLimit) || timeLimit.equals("0")) 
		{
			return planType+"计划"+proMoney+"元套餐";
		}
		else
		{
			return planType+"计划"+proMoney+"元套餐"+timeLimit+"个月";
		}
	}
	
	public String getPackageDes() 
	{
		return planType+"计划"+proMoney+"元套餐";
	}
}
