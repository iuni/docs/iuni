package com.sunnada.baseframe.bean;

/**
 * 系统信息类型
 * 
 * 缓存在sqlitdb数据库中SYSTEM_INFO表的标识。
 * 
 * @author qiuzq.
 *
 */
public class SystemInfoType {
	
	public static final String DEVICETYPE="INFO_DEVICETYPE";//设备类型。一体机/蓝牙
	
	public static final String BLUETOOTH="INFO_BLUETOOTH";//蓝牙设备信息
	
	/* PSAM卡中存放的一些信息  */
	public static final String PSAMID="INFO_PSAMID";//psamid
	public static final String PSAM_AGENT_NAME="INFO_PSAM_AGENT_NAME";//代理商名称
	public static final String PSAM_AGENT_ADDR="INFO_PSAM_AGENT_ADDR";//代理商地址
	public static final String PSAM_AGENT_TEL="INFO_PSAM_AGENT_TEL";//代理商电话
	public static final String PSAM_MAIN_IP="INFO_PSAM_MAIN_IP";//主服务器IP
	public static final String PSAM_BACKUP_IP="INFO_PSAM_BACKUP_IP";//备用服务器IP
	public static final String PSAM_MAIN_PORT="INFO_PSAM_MAIN_PORT";//主服务器端口
	public static final String PSAM_BACKUP_PORT="INFO_PSAM_BACKUP_PORT";//备用服务器端口
	public static final String PSAM_CREATE_DATE = "INFO_PSAM_CREATE_DATE";//psam开卡时间
	public static final String PSAM_END_DATE = "INFO_PSAM_END_DATE";//psam到期时间
	public static final String PSAM_IS_RELOAD = "PSAM_IS_RELOAD";//是否从新加载psam
	
	public static final String LOGIN_PWD="INFO_LOGINPWD";//LOGIN_PWD，加密之后存储
	
	

}
