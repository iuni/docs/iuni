/**
 * $RCSfile: M3.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-22  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <p>
 * Title: M3
 * </p>
 * <p>
 * Description: M3文件升级封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2013
 * </p>
 * 
 * @author 丘富铨
 * @date 2013-4-22
 * @version 1.0
 */

public class M3 implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	private String				name				= null;
	private String				latestVersion		= null;	// 最新版本
	private String				currentVersion		= null;	// 当前版本
	private String				minVersion			= null;	// 最低支持版本
	private String				url					= null;	// apk升级地址
	private long				size				= 0L;	// apk大小,字节为单位
	private String				tips				= null;	// 升级提示信息

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getLatestVersion()
	{
		return latestVersion;
	}

	public void setLatestVersion(String latestVersion)
	{
		this.latestVersion = latestVersion;
	}

	public String getCurrentVersion()
	{
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion)
	{
		this.currentVersion = currentVersion;
	}

	public String getMinVersion()
	{
		return minVersion;
	}

	public void setMinVersion(String minVersion)
	{
		this.minVersion = minVersion;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public long getSize()
	{
		return size;
	}

	public void setSize(long size)
	{
		this.size = size;
	}

	public String getTips()
	{
		return tips;
	}

	public void setTips(String tips)
	{
		this.tips = tips;
	}

	/**
	 * 
	 * 解析M3
	 * 
	 * @param json
	 * @return
	 */
	public static final M3 parse(JSONObject json)
	{
		M3 m3 = new M3();

		try
		{
			m3.setName(json.getString("name"));
			m3.setLatestVersion(json.getString("version"));
			m3.setMinVersion(json.getString("minVersion"));
			m3.setSize(Long.parseLong(json.getString("size")));
			m3.setTips(json.getString("tips"));
			m3.setUrl(json.getString("url"));

		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return m3;
	}
}
