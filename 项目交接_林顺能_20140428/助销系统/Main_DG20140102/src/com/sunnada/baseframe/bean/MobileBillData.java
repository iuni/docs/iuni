package com.sunnada.baseframe.bean;

public class MobileBillData 
{
	private int								mCode            = 0;         // 账单编码
	private String							mContent         = "";        // 账单内容
	private boolean							mIsHasSubItem    = false;     // 是否有子项
	private boolean							mIsHasDetail     = false;     // 是否有详情
	//private List<MobileBillSubItemData>	mSubItemList     = new ArrayList<MobileBillSubItemData>();  // 子项列表数据
	
	public int getmCode() 
	{
		return mCode;
	}
	
	public void setmCode(int mCode) 
	{
		this.mCode = mCode;
	}
	
	public String getmContent() 
	{
		return mContent;
	}
	
	public void setmContent(String mContent) 
	{
		this.mContent = mContent;
	}
	
	public boolean ismIsHasSubItem() 
	{
		return mIsHasSubItem;
	}
	
	public void setmIsHasSubItem(boolean mIsHasSubItem) 
	{
		this.mIsHasSubItem = mIsHasSubItem;
	}
	
	public boolean ismIsHasDetail() 
	{
		return mIsHasDetail;
	}
	
	public void setmIsHasDetail(boolean mIsHasDetail) 
	{
		this.mIsHasDetail = mIsHasDetail;
	}
	
	/*
	public List<MobileBillSubItemData> getmSubItemList() 
	{
		return mSubItemList;
	}
	
	public void setmSubItemList(MobileBillSubItemData subItemData) 
	{
		mSubItemList.add(subItemData);
	}
	*/
}
