package com.sunnada.baseframe.bean;

public class InvoiceData 
{
	private String             mSerialNum;  // 流水号
	private String             mBusiType;   // 交易类型
	private int                mMoney;      // 金额(以分为单位)
	private String             mCMoney;     // 金额(以元为单位)
	private String             mBusiTime;   // 交易时间
	private String             mDeviceType; // 设备类型
	private int                mPrintCount; // 打印次数
	
	public InvoiceData(String serialNum, String busiType, int money,String cMoney, 
			String busiTime, String deviceType, int printCount) 
	{
		this.mSerialNum = serialNum;
		this.mBusiType = busiType;
		this.mMoney = money;
		this.mCMoney = cMoney;
		this.mBusiTime = busiTime;
		this.mDeviceType = deviceType;
		this.mPrintCount = printCount;
	}

	public String getSerialNum() 
	{
		return mSerialNum;
	}
	
	public void setSerialNum(String serialNum)
	{
		this.mSerialNum = serialNum;
	}
	
	public String getBusiType() 
	{
		return mBusiType;
	}
	
	public void setBusiType(String busiType) 
	{
		this.mBusiType = busiType;
	}
	
	public int getMoney() 
	{
		return mMoney;
	}
	
	public void setMoney(int money) 
	{
		this.mMoney = money;
	}
	
	public String getCMoney() 
	{
		return mCMoney;
	}

	public void setCMoney(String cMoney)
	{
		this.mCMoney = cMoney;
	}

	public String getBusiTime() 
	{
		return mBusiTime;
	}
	
	public void setBusiTime(String busiTime) 
	{
		this.mBusiTime = busiTime;
	}
	
	public String getDeviceType() 
	{
		return mDeviceType;
	}
	
	public void setDeviceType(String deviceType) 
	{
		this.mDeviceType = deviceType;
	}
	
	public int getPrintCount() 
	{
		return mPrintCount;
	}
	
	public void setPrintCount(int printCount) 
	{
		this.mPrintCount = printCount;
	}
}
