package com.sunnada.baseframe.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * 图片池.不过现在获取图片大小的算法有问题
 * 现在的功能就是做远程图片下载及回调
 * @author acer
 *
 */
public class BitmapPoolUtil
{
	//private static long				MAX_SIZE		= 1024*1024;						// 最大缓存容量,默认最大为5M
	//private static long				size			= 0;								// 当前缓存容量
	//private static Map<String, Long>	bitmapsSize		= new HashMap<String, Long>();		// 缓存每张图片的字节数
	private static Map<String, Bitmap>	bitmapsPool		= new HashMap<String, Bitmap>();	// 缓存每张图片数据
	private static Queue<String>		bitmapsOrder	= new LinkedList<String>();			// 图片加载的队列
	
	// 回调队列的map
	// 基本思想为:下载一个图片时,将回调方法加入队列.下载同样的图片,不多次调用下载方法.
	// 当图片下载完成时, 取出方法回调队列, 依次回调.
	private static Map<String, List<onImageGotListener >> callBacks = new HashMap<String, List<onImageGotListener>>();
	private static Handler 				callBackHandle;
	
	private BitmapPoolUtil() 
	{
		
	}
	
	public static void init() 
	{
		// 下载成功后, 通知此handler在主线程中调用回调事件
		callBackHandle = new Handler() 
		{
			@Override
			public void handleMessage(Message msg) 
			{
				switch (msg.arg1) 
				{
					case 0:
						// 加入队列
						break;
						
					case 1:
						// 从队列取出处理 
						doloadResult(msg, true);
						break;
					
					case 2:
						doloadResult(msg, false);
						break;
					
				default:
					break;
				}
			}
		};
	}
	
	// 下载结果
	private static void doloadResult(Message msg, boolean result)
	{ 
		synchronized (callBacks)
		{
			String url = msg.getData().getString("url");
			List<onImageGotListener> list = callBacks.get(url);
			removeFromQueue(url);// 将下载完成的任务移除队列 20131028
			if(list == null) 
			{
				Log.e("图片下载完成...", "监听为空...");
				return;
			}
			Log.e("图片下载完成...", "结果：" + result);
			Log.e("图片下载完成...", "监听个数：" + list.size());
			int i = 0;
			while (list.size() > 0) 
			{
				i++;
				onImageGotListener listener = list.remove(0);
				Log.e("图片下载完成...", "调用监听"+ i + " 剩余监听：" + list.size());
				try 
				{
					// 回调一次,不管死活,有异常就catch掉,不做其他处理了
					if(listener != null)
					{
						listener.onImageGot(result);
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Bitmap getBitmap(final String localPath, final String serverPath, final onImageGotListener listener) 
	{
		return getBitmap(localPath, serverPath, listener, null);
	}
	
	public static Bitmap getBitmap(final String localPath, final String serverPath, final onImageGotListener listener, int[] size) 
	{
		return getBitmap(localPath, serverPath, listener, size, true);
	}
	
	/**
	 * 核心方法, 加载图片, 如果本地文件不存在或加载失败, 就从网络获取.
	 * @param localPath		本地文件路径
	 * @param serverPath	网络url路径
	 * @param listener		网络url下载成功后的回调事件.可以在这里面重新加载图片
	 * @param isCache		是否加入缓存	
	 * @return
	 */
	public static Bitmap getBitmap(final String localPath, final String serverPath, final onImageGotListener listener, int[] size, boolean isCache)
	{
		if(size != null && size.length > 1 && size[0] != 0 && size[1] != 0) 
		{
			Log.e(localPath, "size[0] :" + size[0] + "  size[1]:" + size[1] + "\n serverPath:" + serverPath);
		}
		synchronized (callBacks) 
		{
			if(callBacks.containsKey(serverPath)) 
			{
				 //如果已经在下载了,直接加入队列
				addToQueue(serverPath, listener);
				Log.e("加载图片..", "已经在下载，加入队列...");
			}
			else
			{	
				Bitmap bitmap = null;
				// 否则就先判断缓存有木有,木有再加入队列
				File file = new File(localPath);
				boolean bContains;
				if(isCache)
				{
					bContains = containsKey(localPath);
				}
				else
				{
					bContains = false;
				}
				
				if(bContains) 
				{ 
					bitmap = bitmapsPool.get(localPath);
					Log.e("加载图片..", "本地缓存存在..." + bitmap.isRecycled());
					if(bitmap.isRecycled() == false) 
					{ 
						return bitmap;
					}
					remove(localPath);
				}
				else if(file.exists() && file.isFile()) 
				{
					Log.e("加载图片..", "本地图片存在...");
					// 判断本地是否有文件
					try 
					{
						// 按指定长宽解码
						if(size != null && size.length > 1 && size[0] != 0 && size[1] != 0) 
						{
							bitmap = PictureShowUtils.decodeFile(localPath, size[0], size[1]);
						}
						else
						{
							bitmap = BitmapFactory.decodeFile(localPath);
						}
						// 是否加入缓存队列
						if(isCache)
						{
							addBitmap(localPath, bitmap);
						}
						return bitmap;
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				else
				{
					Log.e("加载图片..", "本地图片不存在...");
					addToQueue(serverPath, listener);
					Thread thread = new Thread() 
					{
						@Override
						public void run() 
						{
							try
							{
								// 下载并处理
								// 取消同步锁.但实际上应该考虑用队列, 以免线程同时开销太大
								//synchronized (imageGotLock) 
								{
									FileOpr.downFile(serverPath, localPath);
									Bundle bundle = new Bundle();
									bundle.putString("url", serverPath);
									Message msg = new Message();
									msg.arg1 = 1;
									msg.setData(bundle);
									callBackHandle.sendMessage(msg);
								}
							} 
							catch (Exception e) 
							{
								e.printStackTrace();
								Log.e("", "serverPath :" + serverPath + " localPath:" + localPath);
								Bundle bundle = new Bundle();
								bundle.putString("url", serverPath);
								Message msg = new Message();
								msg.arg1 = 2;
								msg.setData(bundle);
								callBackHandle.sendMessage(msg);
							}
						}
					};
					thread.start();
				}
			}
			return null;
		}
	}
	
	// 添加回调事件到队列
	private static void addToQueue(String serverPath, onImageGotListener listener) 
	{
		List<onImageGotListener> list = callBacks.get(serverPath);
		if(list == null) 
		{
			list = new ArrayList<onImageGotListener>();
			callBacks.put(serverPath, list);
		}
		list.add(listener);
	}
	
	// 从队列里删除回调事件
	private static void removeFromQueue(String serverPath) 
	{
		callBacks.remove(serverPath);
	}
	
	// 添加图片到缓冲池
	// key 图片标识, 为本地存储路径
	public static void addBitmap(String key, Bitmap bitmap) 
	{
		if(bitmapsPool.containsKey(key)) 
		{
			remove(key);
		}
		/*
		long bSize = 0L;
		try 
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			if(key.toLowerCase().endsWith("png")) 
			{
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			}
			else if(key.toLowerCase().endsWith("jpg") || key.toLowerCase().endsWith("jpeg")) 
			{
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			}
			else
			{
				
			}
			baos.flush();
			bSize = baos.toByteArray().length;
			baos.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new RuntimeException("get bitmap size error", e);
		}
		
		// 单张图片超过最大缓存值
		if(bSize > MAX_SIZE) 
		{
			return;
		}
		*/
		
		//while(size+bSize > MAX_SIZE)
		//{
		//	remove(bitmapsOrder.poll());
		//}
		
		//size += bSize;
		//bitmapsSize.put(key, bSize);
		bitmapsOrder.offer(key);
		bitmapsPool.put(key, bitmap);
		Log.e("加载图片..", "图片加入缓存..." + bitmap);
	}
	
	// 删除key对应的位图信息
	// key - 为图片存储的绝对路径
	public static void remove(String key)
	{
		// 从图片大小容器中删除
		//if(bitmapsSize.containsKey(key)) 
		//{ 
		//	size -= bitmapsSize.get(key);
		//	bitmapsSize.remove(key);
		//}
		// 从图片加载容器中删除
		if(bitmapsOrder.contains(key)) 
		{
			bitmapsOrder.remove(key);
		}
		// 从图片缓存容器中移除
		if(bitmapsPool.containsKey(key)) 
		{
			Bitmap bitmap = bitmapsPool.get(key);
			bitmapsPool.remove(key);
			// 
			if(bitmap != null && !bitmap.isRecycled()) 
			{
				bitmap.recycle();
				bitmap = null;
			}
		}
		Log.e("加载图片..", "图片移除缓存...");
	}
	
	/*
	public static void shrink() 
	{
		while(size >= MAX_SIZE) 
		{
			String key = bitmapsOrder.poll();
			if(key == null) 
			{
				bitmapsPool.clear();
				bitmapsSize.clear();
				return;
			}
			Bitmap bitmap = bitmapsPool.get(key);
			if(bitmap != null) 
			{
				bitmap.recycle();
				bitmapsPool.remove(key);
			}
			Long tempSize = bitmapsSize.get(key);
			if(tempSize != null) 
			{
				size -= tempSize;
				bitmapsSize.remove(key);
			}
		}
	}
	*/
	
	public static void clear() 
	{
		while(!bitmapsOrder.isEmpty()) 
		{
			bitmapsOrder.poll();
		}
		Iterator<Entry<String, Bitmap>> itr = bitmapsPool.entrySet().iterator();
		while(itr.hasNext()) 
		{
			Entry<String, Bitmap> entry = itr.next();
			String strLocalPath = entry.getKey();
			Bitmap bitmap = entry.getValue();
			if(bitmap != null && !bitmap.isRecycled()) 
			{
				Log.e("", "正在释放: " + strLocalPath + " 的原始图片...");
				bitmap.recycle();
				bitmap = null;
			}
			strLocalPath = null;
			entry = null;
		}
		bitmapsPool.clear();
		//bitmapsSize.clear();
		Log.e("加载图片..", "清除缓存...");
	}
	
	// 缓冲池中是否包含key的位图
	public static boolean containsKey(String key)
	{
		if(bitmapsPool.containsKey(key) && bitmapsPool.get(key).isRecycled())
		{
			Log.e("加载图片..", "缓存中存在...isRecycled");
			remove(key);
		}
		return bitmapsPool.containsKey(key);
	}
	
	// 返回缓冲池中key对用的位图
	public Bitmap get(String key)
	{
		return bitmapsPool.get(key);
	}
	
	/*
	public long getMaxBufferSize()
	{
		return MAX_SIZE;
	}
	
	public void setMaxBufferSize(long maxSize) 
	{
		MAX_SIZE = maxSize;
	}
	
	// 获取所有的图片大小
	public long getSize() 
	{
		return size;
	}
	*/
	
	// 获取数量
	public int getCount()
	{
		return bitmapsOrder.size();
	}
	
	public interface onImageGotListener 
	{
		// 原先是考虑把bitmap加载好传进去的, 后面想想, 还是丢给逻辑类自己去决定要不要加载好了. --万一人家线程都已经挂掉了呢?
		public void onImageGot(boolean result);
	}; 
}

