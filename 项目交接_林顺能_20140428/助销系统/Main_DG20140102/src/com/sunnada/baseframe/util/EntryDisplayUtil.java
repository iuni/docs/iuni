package com.sunnada.baseframe.util;

import com.sunnada.baseframe.bean.Statics;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EntryDisplayUtil 
{	
	private int 		labelWidth = -2;					// 标题宽度,默认为WRAPCONTENT
	private int 		textWidth = -2;						// 文字宽度,默认为WRAPCONTENT
	
	private int 		horizontalPadding = 0;				// 标题右Padding和内容左Padding
	private int 		verticalPadding = 0;				// 上下Padding
	
	private Context 	context;							// 上下文(Activity)
	private String 		colorString = "#000000";			// 颜色字符串,#000000
	private int 		textSize = 20;

	public EntryDisplayUtil(int labelWidth, int textWidth, int margin,
			int deviderMargin, Context context) 
	{
		super();
		this.labelWidth = labelWidth;
		this.textWidth = textWidth;
		this.context = context;
	}

	public void setLabelWidth(int labelWidth) {
		this.labelWidth = labelWidth;
	}

	public void setTextWidth(int textWidth) {
		this.textWidth = textWidth;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public void setColorString(String colorString) {
		this.colorString = colorString;
	}

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	public EntryDisplayUtil(Context context) {
		super();
		this.context = context;
	}
	
	

	public void setHorizontalPadding(int horizontalPadding) {
		this.horizontalPadding = horizontalPadding;
	}

	public void setVerticalPadding(int verticalPadding) {
		this.verticalPadding = verticalPadding;
	}

	public void displayEntryInfomations(String[][] entrys,LinearLayout parent){
		parent.setOrientation(LinearLayout.VERTICAL);
		for (int i = 0; i < entrys.length; i++) {
			String[] entry = entrys[i];
			LinearLayout child = new LinearLayout(context);
			child.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, -2));
			child.setPadding(0, verticalPadding, 0, verticalPadding);		
			child.setGravity(Gravity.CENTER_VERTICAL);
			TextView label = new TextView(context);
			label.setLayoutParams(new LayoutParams(labelWidth, -2));
			label.setPadding(0, 0, horizontalPadding, 0);
			label.setText(entry[0]);
			label.setGravity(Gravity.RIGHT);
			label.setTextColor(Color.parseColor(colorString));
			label.setTextSize(textSize);
			child.addView(label);
			
			View horizontalDevider = new View(context);
			horizontalDevider.setLayoutParams(new LayoutParams(1, LayoutParams.FILL_PARENT));
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				horizontalDevider.setBackgroundColor(Color.parseColor("#00A6EC"));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				horizontalDevider.setBackgroundColor(Color.parseColor("#FF9600"));
			}
			child.addView(horizontalDevider);
			
			TextView text = new TextView(context);
			text.setPadding(horizontalPadding, 0, 0, 0);
			text.setLayoutParams(new LayoutParams(textWidth, -2));
			text.setTextColor(Color.parseColor(colorString));
			text.setText(entry[1]);
			text.setTextSize(textSize);
			child.addView(text);
			if(i!=0){
				View veticalDevider = new View(context);
				veticalDevider.setLayoutParams(new LayoutParams( LayoutParams.FILL_PARENT,1));
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					veticalDevider.setBackgroundColor(Color.parseColor("#00A6EC"));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					veticalDevider.setBackgroundColor(Color.parseColor("#FF9600"));
				}
				parent.addView(veticalDevider);
			}
			
			parent.addView(child);
		}
		
		
	}
}
