package com.sunnada.baseframe.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import android.util.Log;

import com.sunnada.baseframe.listener.BreakPointDownloadListener;

public class HttpURLConnectionUtil
{
	private static final String 		TAG 				= "HTTP下载";
	public static final int 			CONNECT_TIMEOUT 	= 15*1000;
	public static final int 			READ_TIMEOUT 		= 30*1000;
	
	private HttpURLConnectionUtil() {}

	public static String doGet(String path, Map<String, String> params) throws Exception 
	{
		return doGet(path, params, "UTF-8");
	}
	
	public static  String doGet(String path, Map<String, String> params,String encode) throws Exception 
	{
		String returnValue = null;
		StringBuilder urlPath = new StringBuilder(path);
		
		if(encode == null || encode.length() <= 0)
		{
			encode = "UTF-8";
		}
		urlPath.append('?');
		
		if(params != null && params.size() > 0)
		{
			for(Map.Entry<String, String> entry:params.entrySet())
			{
				urlPath.append(entry.getKey()).append('=').append(URLEncoder.encode(entry.getValue(), encode)).append('&');
			}
			urlPath.deleteCharAt(urlPath.length()-1);
		}
		
		URL url = new URL(urlPath.toString());
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(CONNECT_TIMEOUT);
		connection.setReadTimeout(READ_TIMEOUT);
		
		if(connection.getResponseCode() == 200)
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream is = connection.getInputStream();
			byte[] buf = new byte[1024*10];
			int len = 0;
			while((len = is.read(buf)) > 0)
			{
				baos.write(buf, 0, len);
			}
			
			returnValue = new String(baos.toByteArray(), encode);
			return returnValue;
		}
		else
		{
			throw new Exception("服务器响应错误" + connection.getResponseCode());
		}
	}
	
	public static String doGet(String path, Map<String, String> params, String encode, int timeout) throws Exception 
	{
		String returnValue = null;
		StringBuilder urlPath = new StringBuilder(path);
		
		if(encode == null || encode.length() <= 0)
		{
			encode = "UTF-8";
		}
		urlPath.append('?');

		if(params != null && params.size() > 0)
		{
			for(Map.Entry<String, String> entry:params.entrySet())
			{
				urlPath.append(entry.getKey()).append('=').append(URLEncoder.encode(entry.getValue(), encode)).append('&');
			}
			urlPath.deleteCharAt(urlPath.length()-1);
		}
		
		URL url = new URL(urlPath.toString());
		System.out.println("url :" + url);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(timeout);
		connection.setReadTimeout(timeout);
		
		if(connection.getResponseCode() == 200)
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream is = connection.getInputStream();
			byte[] buf = new byte[1024*10];
			int len = 0;
			while((len=is.read(buf))>0)
			{
				baos.write(buf,0,len);
			}
			
			returnValue = new String(baos.toByteArray(),encode);
			return returnValue;
		}
		else
		{
			throw new Exception("服务器响应错误"+connection.getResponseCode());
		}
	}
	
	public static String doPost(String path, Map<String, String> params) throws Exception
	{
		return doPost(path, params, "UTF-8");
	}
	
	public static String doPost(String path, Map<String, String> params, String encode) throws Exception 
	{
		String returnValue = null;
		StringBuilder body = new StringBuilder();
		
		if(encode == null || encode.length() <= 0)
		{
			encode = "UTF-8";
		}
		
		if(params != null && params.size() > 0)
		{
			for(Map.Entry<String, String> entry:params.entrySet())
			{
				body.append(entry.getKey()).append('=').append(URLEncoder.encode(entry.getValue(), encode)).append('&');
			}
			body.deleteCharAt(body.length()-1);
		}
		
		byte[] bodyContent = body.toString().getBytes();
		
		URL url = new URL(path);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("POST");
		connection.setConnectTimeout(CONNECT_TIMEOUT);
		connection.setReadTimeout(READ_TIMEOUT);
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", bodyContent.length+"");
		
		OutputStream os = connection.getOutputStream();
		os.write(bodyContent);
		os.flush();
		os.close();
		
		if(connection.getResponseCode() == 200)
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream is = connection.getInputStream();
			byte[] buf = new byte[1024*10];
			int len = 0;
			while((len=is.read(buf))>0)
			{
				baos.write(buf,0,len);
			}
			returnValue = new String(baos.toByteArray(),encode);
			return returnValue;
		}
		else
		{
			throw new Exception("服务器响应错误"+connection.getResponseCode());
		}
	}

	public static String doPost(String path, Map<String, String> params, Map<String, File> files) throws IOException
	{
		return doPost(path, params, files, "UTF-8");
	}
		
	public static String doPost(String path, Map<String, String> params,Map<String, File> files, String encode) throws IOException 
	{        
		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--";
		String LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = encode;

		URL uri = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setReadTimeout(READ_TIMEOUT);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", CHARSET);
		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA + ";boundary=" + BOUNDARY);
		StringBuilder sb = new StringBuilder();
		 
		if (params != null) 
		{
			for (Map.Entry<String, String> entry : params.entrySet()) {
				sb.append(PREFIX);
				sb.append(BOUNDARY);
				sb.append(LINEND);
				sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEND);
				sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
				sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
				sb.append(LINEND);
				sb.append(entry.getValue());
				sb.append(LINEND);
			}
		}
		
		DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
		outStream.write(sb.toString().getBytes());
		if (files != null)
			for (Map.Entry<String, File> file : files.entrySet()) 
			{
				StringBuilder sb1 = new StringBuilder();
				sb1.append(PREFIX);
				sb1.append(BOUNDARY);
				sb1.append(LINEND);
				sb1.append("Content-Disposition: form-data; name=\"" + file.getKey() + "\"; filename=\"" + file.getValue().getName() + "\"" + LINEND);
				sb1.append("Content-Type: application/octet-stream; charset=" + CHARSET + LINEND);
				sb1.append(LINEND);
				outStream.write(sb1.toString().getBytes());
				InputStream is = new FileInputStream(file.getValue());
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) 
				{
					outStream.write(buffer, 0, len);
				}
				is.close();
				outStream.write(LINEND.getBytes());
			}
		byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
		outStream.write(end_data);
		outStream.flush();
		
		int res = conn.getResponseCode();
		String data = "";
		if (res == 200) 
		{
			InputStream in = conn.getInputStream();
			InputStreamReader isReader = new InputStreamReader(in);
			BufferedReader bufReader = new BufferedReader(isReader);
			String line = null;
			while ((line = bufReader.readLine()) != null) 
			{
				data += line;
			}
			bufReader.close();
		} 
		else 
		{
			throw new IOException();
		}
		outStream.close();
		conn.disconnect();
		return data;
	}
	
	// 下载文件
	public static boolean downLoadFile(String urlStr, File file) 
	{
		if (!file.getParentFile().exists()) 
		{
			file.getParentFile().mkdirs();
		}
		
		boolean flag = true;
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		try {
			URL url = new URL(urlStr);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(CONNECT_TIMEOUT);
			connection.setReadTimeout(READ_TIMEOUT);
			
			//int fileSize = connection.getContentLength();
			//int currentSize = 0;
			
			inputStream = connection.getInputStream();
			outputStream = new FileOutputStream(file);

			byte buf[] = new byte[1024];
			int len = 0;

			while ((len = inputStream.read(buf)) > 0) 
			{
				outputStream.write(buf, 0, len);
				//currentSize += len;
			}
		}
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
			flag = false;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			flag = false;
		}
		finally 
		{
			try {
				if (inputStream != null) 
				{
					inputStream.close();
					inputStream = null;
				}
				if (outputStream != null) 
				{
					outputStream.close();
					outputStream = null;
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				flag = false;
			}
		}

		// 如果下载不成功，则把失败的文件删除
		if (flag == false && file != null && file.exists()) 
		{
			file.delete();
		}
		return flag;
	}
	
	/**
	 * 断点下载
	 * @param breakPoint 如果为0则重新下载
	 */
	public static void downLoadAPKWithBreakPoint(final String url, final String localPath, final long breakPoint, 
			final BreakPointDownloadListener listener)
	{
		HttpURLConnection 	urlConnection 		= null;
		InputStream 		inputStream 		= null;
		RandomAccessFile	accessFile			= null;
		long 				tempBreakPoint 		= breakPoint;
		long 				totalLength 		= 0;
		File 				file 				= null;

		try
		{
			try
			{
				// 打开HTTP连接
				Log.e(TAG, "下载URL为" + url);
				urlConnection = (HttpURLConnection)new URL(url).openConnection();
				urlConnection.setConnectTimeout(CONNECT_TIMEOUT);
				urlConnection.setReadTimeout(READ_TIMEOUT);
				if(urlConnection.getResponseCode() != 200)
				{
					String errorStr = "服务端响应错误, 升级失败！";
					if(listener != null)
					{
						listener.onDownloadFailed(tempBreakPoint, 0, errorStr);
					}
					Log.e(TAG, errorStr);
					return;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "连接升级服务器失败！";
				if(listener != null)
				{
					listener.onDownloadFailed(tempBreakPoint, 0, errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}
			// 获取文件总大小
			totalLength = urlConnection.getContentLength();
			urlConnection.disconnect();
			urlConnection = null;
			Log.v(TAG, "当前下载文件大小:" + totalLength);
			// 确保该文件目录存在
			file = new File(localPath);
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			// 如果重新下载则先删除文件
			if(file.exists() && (breakPoint<=0 && totalLength != file.length()))
			{
				Log.e(TAG, "文件是否存在: " + file.exists());
				Log.e(TAG, "文件断点是否为0: " + breakPoint);
				Log.e(TAG, "下载文件大小是否和本地文件大小相同:" + totalLength + "==" + file.length());
				Log.e(TAG, "删除文件");

				file.delete();
				tempBreakPoint = 0;
			}
			// 打败本地文件
			try
			{
				accessFile = new RandomAccessFile(file, "rwd");
				if(totalLength != accessFile.length())
				{
					// 设定文件的大小
					accessFile.setLength(totalLength);
				}
				accessFile.seek(tempBreakPoint);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "打开本地缓存文件失败, 请检测SD卡是否存在！";
				if(listener != null)
				{
					listener.onDownloadFailed(tempBreakPoint, 0, errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}
			
			try
			{
				urlConnection = (HttpURLConnection)new URL(url).openConnection();
				urlConnection.setConnectTimeout(CONNECT_TIMEOUT);
				urlConnection.setReadTimeout(READ_TIMEOUT);
				urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-" + totalLength);
				Log.e(TAG, "设定断点: " + tempBreakPoint);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "连接升级服务器失败！";
				if(listener != null)
				{
					listener.onDownloadFailed(tempBreakPoint, 0, errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}

			/*
			SharedPreferences preferences = main_activity.getPreferences(Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putBoolean(KEY_ISDOWNLOADSUCCESS, false);
			editor.putString(KEY_LASTVERSION, mUpdataInfo.getVersion());
			editor.commit();
			*/
			
			byte[] buf = new byte[1024*2];
			int len = 0;
			inputStream = urlConnection.getInputStream();
			// 
			while((len = inputStream.read(buf)) > 0)
			{
				accessFile.write(buf, 0, len);
				tempBreakPoint += len;
				//Log.e(TAG, "当前下载量 = " + tempBreakPoint + ", 文件总长度: " + totalLength);
				// 
				if(listener != null)
				{
					listener.onDownloading(tempBreakPoint, totalLength);
				}
				// 下载完成
				if(tempBreakPoint == totalLength)
				{
					listener.onDownloadSuccess(tempBreakPoint, totalLength);
					break;
				}

				/*
				if(count == 5)
				{
					count = 0;
					// 界面更新
					UIHandle.InitBusiStep("请稍后","正在下载中(单位：KB)...", (int) (tempBreakPoint*100.0f/totalLength));
					android.util.Log.v("当前下载量:", tempBreakPoint+"/"+totalLength);
					editor = preferences.edit();
					editor.putLong(KEY_BREAKPOINT, tempBreakPoint);
					editor.putBoolean(KEY_ISDOWNLOADSUCCESS, false);
					editor.commit();
				}
				*/
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String errorStr = "下载升级文件数据失败！";
			if(listener != null)
			{
				listener.onDownloadFailed(tempBreakPoint, 0, errorStr);
			}
			Log.e(TAG, errorStr);
		}
		finally
		{
			boolean status = (tempBreakPoint == totalLength);
			Log.e(TAG, "当前保存的断点: " + tempBreakPoint);
			Log.e(TAG, "当前保存的是否成功: " + status);

			/*
			SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putLong(KEY_BREAKPOINT, tempBreakPoint);
			editor.putBoolean(KEY_ISDOWNLOADSUCCESS, tempBreakPoint == totalLength ? true:false);
			editor.putString(KEY_LASTVERSION, mUpdataInfo.getVersion());
			editor.commit();
			*/
			
			// 关闭网络输入流
			if(inputStream != null)
			{
				try
				{
					inputStream.close();
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 关闭本地文件流
			if(accessFile != null)
			{
				try
				{
					accessFile.close();
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 关闭HTTP连接
			if(urlConnection != null)
			{
				urlConnection.disconnect();
				urlConnection = null;
			}
		}
	}
}

