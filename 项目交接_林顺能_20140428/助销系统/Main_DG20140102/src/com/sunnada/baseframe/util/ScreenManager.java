package com.sunnada.baseframe.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;   
  
public class ScreenManager 
{   
    private static List<Activity> 	activityList = new ArrayList<Activity>();   
    private static ScreenManager 	instance;
    
    private  ScreenManager() 
    {
    	
    }  
    
    public static ScreenManager getScreenManager() 
    {   
        if(instance == null) 
        {   
            instance = new ScreenManager();
        }   
        return instance;   
    } 
    
    public void popAllActivity()
    { 
    	for(int i=0;i<activityList.size();i++)
    	{
    		if(activityList.get(i)!=null)
			{
				activityList.get(i).finish();
			}
    	}       
    	activityList.clear();
    } 
    
    public void popActivity(Class<?> cls) 
    {
    	int index = -1;
    	for(int i=0;i<activityList.size();i++)
    	{
    		if(((Activity)activityList.get(i)).getClass().equals(cls))
    		{
    			index = i;
    			activityList.get(i).finish();
    			break;
    		}
    	}       
    	if(index != -1)
    	{
    		activityList.remove(index);
    	}
    }
    
    public boolean pushActivity(Activity activity)
    {
    	for(int i=0;i<activityList.size();i++)
    	{
    		if(((Activity)activityList.get(i)).getClass().equals(activity.getClass()))
    		{
    			return false;
    		}
    	}
    	activityList.add(activity);
    	return true;
    }
    
    public boolean getFinishResult(Activity activity)
    {
    	for(int i=0;i<activityList.size();i++)
    	{
    		if(((Activity)activityList.get(i)).getClass().equals(activity.getClass()))
    		{
    			activityList.get(i).finish();
    			activityList.remove(i);
    			return true;
    		}
    	}
    	return false;
    }
}  