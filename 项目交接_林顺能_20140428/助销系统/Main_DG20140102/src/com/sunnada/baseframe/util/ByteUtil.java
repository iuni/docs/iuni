package com.sunnada.baseframe.util;
/**
 * File: ByteUtil.java
 * Description: byte工具类
 * @author 丘富铨
 * @date 2013-1-19
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class ByteUtil
{
	/**
	 * 判断两个byte数组是否相同
	 * @param buf1
	 * @param buf2
	 * @return
	 */
	public static final boolean isEquals(byte[] buf1,byte[] buf2)
	{
		if(buf1.length!=buf2.length)
		{
			return false;
		}
		
		for(int i = 0;i<buf1.length;i++)
		{
			if(buf1[i]!=buf2[i])
			{
				return false;
			}
		}
		
		return true;
	}

	/**
	 * 获取子码流
	 * @param buf
	 * @param index
	 * @param len
	 * @return
	 */
	public static final byte[] subBytesWithFromAndLength(byte[] buf,int index,int len)
	{
		byte[] subBuf = new byte[len];
		System.arraycopy(buf, index, subBuf, 0, len);
		return subBuf;
	}

	/**
	 * 获取子码流
	 * @param buf
	 * @param index
	 * @param len
	 * @return
	 */
	public static final byte[] subBytesWithStartAndEnd(byte[] buf,int start,int end)
	{
		byte[] subBuf = new byte[end-start];
		System.arraycopy(buf, start, subBuf, 0, subBuf.length);
		return subBuf;
	}
}
