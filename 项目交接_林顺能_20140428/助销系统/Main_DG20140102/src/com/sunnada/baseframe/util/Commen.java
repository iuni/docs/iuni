package com.sunnada.baseframe.util;

import java.io.UnsupportedEncodingException;

public final class Commen {
	
	/*
	 * 16进制字符串转byte[]
	 * 如：“F56573”转化后为 ｛0xF5, 0x65, 0x73｝
	 */
	public static byte[] hexstr2byte(String s)
	{
		System.out.println("s=="+s.length());
		if (s.length()%2 != 0) 
		{
			return null;
		}
		
		int len = s.length()/2;
		byte[] arr = new byte[len];
		for(int i = 0; i < len; i++)
		{
			arr[i] = (byte)Integer.parseInt(s.substring(2*i, 2*i+2), 16);
		}
		return arr;
	}
	
	/*
	 *日志输出byte[] 16进制串
	 *data: 要输出的数组
	 *len: 数组长度
	 *tag:日志tag
	 *type: 当type='i' 时用log.i 其它用 log.e 输出
	 */
	public static int printhax(byte[] data, int len, String tag, char type) {
		if (null == data)
			return -1;
		int i;
		StringBuffer buff = new StringBuffer();
		for (i = 0; i < len; i++) {
			String s = Integer.toHexString(data[i] & 0xFF);
			if (s.length() == 1)
				s = '0' + s;
			buff.append(s);
			buff.append(" ");
		}
		if (type == 'i') {

			Log.i(tag, buff.toString());
		} else {
			Log.e(tag, buff.toString());
		}
		return 0;
	}
	
	//转为MISPOS
	public static String mishax(byte[] data, int len) {
		if (null == data)
			return null;
		int i;
		StringBuffer buff = new StringBuffer();
		for (i = 0; i < len; i++) {
			String s = Integer.toHexString(data[i] & 0xFF);
			if (s.length() == 1)
				s = '0' + s;
			buff.append(s);
			buff.append(" ");
		}
		return buff.toString();
	}
	
	
	
	/*
	 *对十六进制数据进行倒换 
	 */
	public static byte[] haxchange(byte[] data)
	{
		if(null == data)
		{
			return null;
		}
		byte temp = 0;
		
		temp = data[1];
		data[1] = data[0];
		data[0] = temp;
		
		return data;
	}
	
	
	/*
	 * byte[] 转16进制串
	 * data: 要转化的字节数组
	 * split: 字节数组连接所用符号
	 * 返回:string 
	 */

	public static String sprinthax(byte[] data, String split) {
		if (null == data)
			return null;
		int i;
		int len = data.length;
		StringBuffer buff = new StringBuffer();
		for (i = 0; i < len; i++) {
			String s = Integer.toHexString(data[i] & 0xFF);
			if (s.length() == 1)
				s = '0' + s;
			buff.append(s);
			buff.append(split);
		}
		return buff.toString();
	}	
	
	/*
	 * byte[] 转16进制串
	 * data: 要转化的字节数组
	 * len: 字节数组连接所用符号
	 * 返回:string 
	 */

	public static String hax2str(byte[] data, int start, int len) {
		if(data.length -start < len)
			return null;
		int i;
		StringBuffer buff = new StringBuffer();
		for (i = start; i < start+len; i++) {
			String s = Integer.toHexString(data[i] & 0xFF);
			if (s.length() == 1)
				s = '0' + s;
			buff.append(s);
		}
		return buff.toString();
	}
	
	
	/*
	 * byte[] 转16进制串
	 * data: 要转化的字节数组
	 */
	
	public static String hax2str(byte[] data) 
	{
		if (null == data)
		{
			return null;
		}
		int len = data.length;
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < len; i++) 
		{
			String s = Integer.toHexString(data[i]&0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s);
		}
		return buff.toString();
	}
	
	public static String makeworld(byte[] data, int start, int len) 
	{
		if(len != 2 || (data.length -start < len))
		{
			return null;
		}
		StringBuffer buff = new StringBuffer();
		byte[] world = new byte[2];
		world[0] = data[start+1];
		world[1] = data[start];
		
		for (int i = 0; i < 2; i++) 
		{
			String s = Integer.toHexString(world[i] & 0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s);
		}
		return buff.toString();
	}
	
	public static byte[] int2bytes(int num) {   //4位  底-----》高字节
	    byte[] b = new byte[4];  
	    for (int i = 0; i < 4; i++) {  
	        b[i] = (byte) (num >>> (24 - (3-i) * 8));  
	    }  
	    return b;  
	}  
	
	public static byte[] short2bytes(short num) {   //2位  底-----》高字节
	    //byte[] b = new byte[2];  
	    /*for (int i = 0; i < 2; i++) {  
	        b[i] = (byte) (num >>> (24 - (3-i) * 8));  
	    }*/  
	    
	    byte[]   ba   =   new   byte[2]; 
	    ba[0]   =   (byte)(num   &   0xff); 
	    ba[1]   =   (byte)((num >> 8)   &   0xff);
	    return ba;  
	}  
	
	public static int bytes2int(byte[] b){
		int len =b.length;
		int mask =0xFF;
		int n=0;		
		for(int i=len-1; i>=0; i--){
			n <<= 8;
			n |= b[i] & mask;
		}	
		return n;
	}
	
	public static int bytes2int(byte[] b,int pos ,int len)
	{
		byte []buf = new byte[len];
		System.arraycopy(b, pos, buf, 0, len);
		return bytes2int(buf);
	}


	public static String bcd2str(byte[] bcd) {
		StringBuffer temp=new StringBuffer(bcd.length*2);

	    for(int i=0;i<bcd.length;i++){
	     temp.append(Integer.toHexString(bcd[i] & 0x0f));
	     temp.append(Integer.toHexString((bcd[i]& 0xf0)>>>4));
	    }
	    return temp.toString().substring(0,1).equalsIgnoreCase("0")?temp.toString().substring(1):temp.toString();
	}
	/**
	 * 
	 * @param str ASCII 码
	 * @return BCD码
	 */
	
	public static byte[] str2bcd(byte[] str) {
		int len = str.length;
		int bcd_len = (len%2 ==0 )? len/2 : len/2+1;	
		int low_ch = 0;
		byte[] temp = new byte[bcd_len];
		for(int i=0; i<bcd_len; i++){
			if(i*2+1 == len)
			{
				low_ch = 0;
			}else{
				low_ch = (str[i*2+1] - 48)<<4;
			}
			temp[i] = (byte) (low_ch | (str[i*2] - 48));
			
		 }	
		return temp;
	}
	
	public static byte[] misstr2bcd(byte[] str) {
		int len = str.length;
		int bcd_len = (len%2 ==0 )? len/2 : len/2+1;	
		int low_ch = 0;
		byte[] temp = new byte[bcd_len];
		for(int i=0; i<bcd_len; i++){
			if(i*2+1 == len)
			{
				low_ch = 0x30;
			}else{
				low_ch = (str[i*2+1] - 48)<<4;
			}
			temp[i] = (byte) (low_ch | (str[i*2] - 48));
			
		 }	
		return temp;
	}
	
	public static byte[] misDtstr2bcd(byte[] str) {
		int len = str.length;
		int bcd_len = (len%2 ==0 )? len/2 : len/2+1;	
		int low_ch = 0;
		byte[] temp = new byte[bcd_len];
		for(int i=0; i<bcd_len; i++){
			if(i*2+1 == len)
			{
				low_ch = 0x90;
			}else{
				low_ch = (str[i*2+1] - 48)<<4;
			}
			temp[i] = (byte) (low_ch | (str[i*2] - 48));
			
		 }	
		return temp;
	}
	
	public static int endofstr(byte[] str){
		if(null == str)
			return 0;
		int len = str.length;
		int index = 0;
		while(index < len){
			if(str[index] == 0)
				return index;
			index++;
		}
		return len;
	}
	
	/**
	 * 把byte[]数组按照GBK方式取转成字符串。（字符串的反向转化）
	 * @param bytes
	 * @return
	 */
	public static String byteToGBKStr(byte[] bytes){
		String str="";
		try {			
			str =new String(bytes,"GBK");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	public static void main(String[] args){
		byte[] b={(byte)0x01,(byte)0x78};
		System.out.println(Commen.hax2str(b));
	}

}
