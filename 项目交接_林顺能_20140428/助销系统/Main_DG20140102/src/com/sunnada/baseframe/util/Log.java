package com.sunnada.baseframe.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Environment;

/**
 * File: LogUtil.java
 * Description: 日志工具类
 * @author 丘富铨
 * @date 2013-1-17
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class Log
{
	public static boolean 						fileLog 		= true;												// 是否将日志写入日志文件
	public static final SimpleDateFormat 		sdf 			= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		// 日志的时间格式
	public static final String 					DEFAULT_PREFIX 	= "baseframe";										// 默认的日志前缀
	
	/**
	 * 日志
	 * @param prefix
	 * @param msg
	 */
	public static final void log(String msg)
	{
		log(DEFAULT_PREFIX, msg);
	}
	
	/**
	 * 日志
	 * @param prefix
	 * @param msg
	 */
	public static final void log(String prefix,String msg)
	{
		if(fileLog)
		{
			logFile(prefix, msg);
		}
		android.util.Log.v(prefix, msg);
	}

	/**
	 * 日志
	 * @param prefix
	 * @param msg
	 */
	public static final void i(String prefix,String msg)
	{
		log(prefix, msg);
	}
	
	/**
	 * 日志
	 * @param prefix
	 * @param msg
	 */
	public static final void d(String prefix,String msg)
	{
		log(prefix, msg);
	}
	
	/**
	 * 日志
	 * @param prefix
	 * @param msg
	 */
	public static final void e(String prefix,String msg)
	{
		log(prefix, msg);
	}


	/**
	 * 输出byte数组
	 * @param msg
	 */
	public static final void log(byte[] msg)
	{
		log(DEFAULT_PREFIX, msg);
	}


	/**
	 * 输出byte数组
	 * @param msg
	 */
	public static final void log(byte[] msg,int pos,int len)
	{
		log(DEFAULT_PREFIX, msg,pos,len);
	}
	
	/**
	 * 输出byte数组
	 * @param prefix 前缀
	 * @param msg
	 */
	public static final void log(String prefix,byte[] msg,int pos,int len)
	{
		byte[] buf = new byte[len];
		System.arraycopy(msg, pos, buf, 0, len);
		log(prefix, buf);
	}
	
	/**
	 * 输出byte数组
	 * @param prefix 前缀
	 * @param msg
	 */
	public static final void log(String prefix, byte[] msg)
	{
		String haxStr = "";
		String decStr = "";
		
		if(msg != null)
		{
			for(int i = 0;i<msg.length;i++)
			{
				haxStr += String.format("%02X ", msg[i]);
				decStr += String.format("%02d ", msg[i]);
			}
		}
		else
		{
			haxStr +="null";
			decStr +="null";
		}
		
		haxStr +="(16进制,长度"+msg.length+")";
		decStr +="(10进制,长度"+msg.length+")";
		if(fileLog)
		{
			logFile(prefix+"(16)", haxStr);
			logFile(prefix+"(10)", decStr);
		}
		android.util.Log.v(prefix+"(16)", haxStr);
		//android.util.Log.v(prefix+"(10)", decStr);
	}
	
	/**
	 * 记录异常日志
	 * @param e
	 */
	public static final void log(Throwable e)
	{
		String excep = "组装异常信息失败";
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintWriter ps = new PrintWriter(baos);
			e.printStackTrace(ps);
			ps.flush();
			excep = new String(baos.toByteArray());
			baos.close();
			
		} catch (Exception e2)
		{
			e.printStackTrace();
		}
		android.util.Log.e(DEFAULT_PREFIX, excep);
		if(fileLog)
		{
			logFile(DEFAULT_PREFIX, excep);
		}
	}
	
	/**
	 * 将日志记录到日志文件
	 * @param prefix
	 * @param msg
	 */
	private synchronized static final void logFile(String prefix,String msg)
	{
		DataOutputStream dos = null;
		try
		{
			String tmpMsg = sdf.format(new Date()) + "  --  " + prefix + ":" + msg + "\r\n";
			// 
			File file = new File(Environment.getExternalStorageDirectory() + "/sunnada/logs/" + DEFAULT_PREFIX + "_" + 
					new SimpleDateFormat("yyyy_MM_dd").format(new Date())+".log");
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			
			dos = new DataOutputStream(new FileOutputStream(file, true));
			dos.write(tmpMsg.getBytes());
			dos.flush();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(dos!=null)
			{
				try
				{
					dos.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static boolean isFileLog()
	{
		return fileLog;
	}
	
	public static void setFileLog(boolean fileLog) 
	{
		Log.fileLog = fileLog;
	}
}
