package com.sunnada.baseframe.listener;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class MyGestureListener implements GestureDetector.OnGestureListener
{
	private IExcute mExcute;
	public MyGestureListener(IExcute excute)
	{
		this.mExcute = excute;
	}
	
	//在滚动时调用
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) 
	{
		return false;
	}
	
	//在按下动作时被调用
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return false;
	}

	//在抛掷动作时被调用
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) 
	{
		if(e1 != null && e2!= null)
		{
			float distanceX = Math.abs(e1.getX() - e2.getX());
			float distanceY = Math.abs(e1.getY() - e2.getY());
			if(velocityY > 0)
			{
				if(Math.asin(distanceY/(Math.sqrt(distanceX*distanceX + distanceY*distanceY))) >= Math.PI/3)
				{
					if(mExcute != null)
					{
						mExcute.excuteResult(0);
					}
				}
			}
			else
			{
				if(Math.asin(distanceY/(Math.sqrt(distanceX*distanceX + distanceY*distanceY))) >= Math.PI/3)
				{
					if(mExcute != null)
					{
						mExcute.excuteResult(1);
					}
				}
			}
			return true;
		}
		else
		{
			return false;	
		}
	}

	//在长按时被调用
	@Override
	public void onLongPress(MotionEvent e) 
	{
	}

	//在按住时被调用
	@Override
	public void onShowPress(MotionEvent e)
	{
	}

	//在抬起时被调用
	@Override
	public boolean onSingleTapUp(MotionEvent e) 
	{
		return false;
	}
}
