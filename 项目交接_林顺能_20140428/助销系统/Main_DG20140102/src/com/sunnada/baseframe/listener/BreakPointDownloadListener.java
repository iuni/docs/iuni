package com.sunnada.baseframe.listener;
/**
 * File: BreakPointDownloadListener.java
 * Description: 断点下载监听器
 * @author 丘富铨
 * @date 2013-3-25
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public interface BreakPointDownloadListener
{
	public void onDownloading(long currentSize, long allSize);
	public void onDownloadSuccess(long currentSize, long allSize);
	public void onDownloadFailed(long currentSize, long allSize, String error);
}
