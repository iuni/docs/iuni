package com.sunnada.baseframe.identify;

import com.sunnada.bluetooth.BlueMethod;

import android.app.ProgressDialog;
import android.content.Context;

public class Interface 
{
	public  static BlueMethod 		jnicall = new BlueMethod();
	private static ProgressDialog 	m_progress;
	public  static byte 			sunnada_test = 0x01;
	
	//private static final String SDCARD_ROOT_PATH = Environment.getExternalStorageDirectory().getPath();
	//public  static final String IMAGE_PATH = SDCARD_ROOT_PATH + "/hotcard/images/idc/";
	
	public static String getSubString(String str) 
	{
		int nIndex = str.indexOf(0);
		if(nIndex != -1)
		{
			return str.substring(0, nIndex);
		}
		return str;
	}
	
	public static void attachProgressDialog(Context context, String titleStr, String contentStr)
	{
		m_progress = new ProgressDialog(context);
		m_progress.setMax(100);
		m_progress.incrementProgressBy(1);
		m_progress.setTitle(titleStr);
		m_progress.setMessage(contentStr);
		m_progress.setCancelable(false);
		m_progress.show();
	}
	
	public static void detachProgressDialog()
	{
		if(m_progress != null)
		{
			m_progress.dismiss();
			m_progress = null;
		}
	}
}
