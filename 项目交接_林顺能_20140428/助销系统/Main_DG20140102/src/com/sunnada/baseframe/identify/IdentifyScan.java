package com.sunnada.baseframe.identify;

import java.io.File;

import com.sunnada.baseframe.bean.IdentifyMsg;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

public class IdentifyScan 
{
	private String sIdentifyMsg = "";
	
	public IdentifyScan(Context context) 
	{
		// 初始化拍照
		Interface.jnicall.Mini_camera_init(context);
		Interface.jnicall.Mini_camera_set_tip_info("请将身份证放入红框内拍照(请勿倒置)！", 20, Color.RED);
		Interface.jnicall.Mini_camera_set_exit_info("退出");
		Interface.jnicall.Mini_camera_set_takeagain_info("重拍", 20, Color.RED);
		Interface.jnicall.Mini_camera_set_takeok_info("完成", 20, Color.RED);
		//Interface.jnicall.Mini_camera_set_focus_mode(0x00);
		
		Interface.jnicall.Mini_camera_ratio(1600, 1200);
		Interface.jnicall.Mini_pic_set_blurdetect_level(1);
		
		//Interface.jnicall.Mini_camera_set_display_orientation(180);
	}
	
	// 身份证拍照
	public int takePhoto(String filename) 
	{
		int ret = -1;
		try
		{
			ret = Interface.jnicall.Mini_camera_play(filename.getBytes("GBK"), null, null);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	// 扫描身份证信息
	// pic_file 需扫描的文件的路径+名称
	public String[] scanIdentify(String pic_file) 
	{		
		String[] sResults = null;
		
		try 
		{
			sResults = Interface.jnicall.Mini_pic_ana(pic_file.getBytes("GBK"), null, 0);
			if(sResults == null) 
			{
				Log.e("", "身份证识别失败!");
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sResults;
	}
	
	// file_path: 身份证图片存放的路径
	// file_key:  身份证图片名称的关键字
	public IdentifyMsg scanIdentify(String file_path,String file_key) 
	{
		IdentifyMsg idenMsg = new IdentifyMsg();
		
		File file = new File(file_path);
		File[] files = file.listFiles();
		if(files != null) 
		{
			for (File fl : files) 
			{
				String file_name = fl.toString();
				if(file_name.toLowerCase().indexOf(file_key.toLowerCase()) != -1)
				{
					String[] sResults = scanIdentify(file_name);
					if(sResults != null && sResults.length >= 8) 
					{
						// 姓名
						if(!"null".equals(getValue(sResults[0]))) 
						{
							idenMsg.name = getValue(sResults[0]);
						}
						// 身份证
						if(!"null".equals(getValue(sResults[5]))) 
						{
							idenMsg.identify_no = getValue(sResults[5]);
						}
						// 民族
						if(!"null".equals(getValue(sResults[2]))) 
						{
							idenMsg.minzu = getValue(sResults[2]);
						}
						// 出生日期
						if(!"null".equals(getValue(sResults[3]))) 
						{
							idenMsg.birth_date = getValue(sResults[3]);
						}
						// 地址
						if(!"null".equals(getValue(sResults[4]))) 
						{
							idenMsg.address = getValue(sResults[4]);
						}
						// 发证单位
						if(!"null".equals(getValue(sResults[6]))) 
						{
							idenMsg.sign_org = getValue(sResults[6]);
						}
						// 性别
						if(!"null".equals(getValue(sResults[1]))) 
						{
							if("女".equals(getValue(sResults[1]))) 
							{
								idenMsg.sex = "01";
							}
							else
							{
								idenMsg.sex = "02";
							}
						}
						// 有效期
						if(!"null".equals(getValue(sResults[7]))) 
						{
							String dead_time = getValue(sResults[7]);
							String[] ss = dead_time.split("-");
							if(ss.length > 1) 
							{
								idenMsg.begin_time = ss[0].replaceAll("\\.", "");
								idenMsg.end_time = ss[1].replaceAll("\\.", "");
							}
						}
					}
				}				
			}
		}
		return idenMsg;
	}
	
	// file_name  身份证图片存放的路径
	public IdentifyMsg scanIdentifySigal(String file_name) 
	{
		IdentifyMsg idenMsg = new IdentifyMsg();
		
		String[] sResults = scanIdentify(file_name);
		if(sResults != null && sResults.length >= 8) 
		{
			// 姓名
			if(!"null".equals(getValue(sResults[0]))) 
			{
				idenMsg.name = getValue(sResults[0]);
			}
			// 身份证
			if(!"null".equals(getValue(sResults[5]))) 
			{
				idenMsg.identify_no = getValue(sResults[5]);
			}
			// 民族
			if(!"null".equals(getValue(sResults[2]))) 
			{
				idenMsg.minzu = getValue(sResults[2]);
			}
			// 出生日期
			if(!"null".equals(getValue(sResults[3]))) 
			{
				idenMsg.birth_date = getValue(sResults[3]);
			}
			// 地址
			if(!"null".equals(getValue(sResults[4]))) 
			{
				idenMsg.address = getValue(sResults[4]);
			}
			// 发证单位
			if(!"null".equals(getValue(sResults[6]))) 
			{
				idenMsg.sign_org = getValue(sResults[6]);
			}
			// 性别
			if(!"null".equals(getValue(sResults[1]))) 
			{
				if("女".equals(getValue(sResults[1]))) 
				{
					idenMsg.sex = "01";
				}
				else
				{
					idenMsg.sex = "02";
				}
			}
			// 有效期
			if(!"null".equals(getValue(sResults[7]))) 
			{
				String dead_time = getValue(sResults[7]);	
				String[] ss = dead_time.split("-");
				if(ss.length > 1) 
				{
					idenMsg.begin_time = ss[0].replaceAll("\\.", "");
					idenMsg.end_time = ss[1].replaceAll("\\.", "");
				}
			}
		}
		return idenMsg;
	}
	
	private String getValue(String des) 
	{
		String src = "null";
		if(des != null) 
		{
			String[] ss = des.split(":");
			if(ss.length > 1) 
			{
				src = ss[1];
			}
		}		
		return src;
	}
	
	
	public String getsIdentifyMsg() 
	{
		return sIdentifyMsg;
	}

	public void setsIdentifyMsg(String sIdentifyMsg) 
	{
		this.sIdentifyMsg = sIdentifyMsg;
	}

}
