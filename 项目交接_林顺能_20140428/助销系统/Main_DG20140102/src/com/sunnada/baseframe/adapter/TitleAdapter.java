package com.sunnada.baseframe.adapter;
import java.util.List;

import com.sunnada.baseframe.bean.ChildBean;
import com.sunnada.baseframe.bean.GroupBean;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * @author gy
 * @param <T>
 * @date 2013-8-22下午5:17:14
 */
public abstract class TitleAdapter extends BaseAdapter
{
	private                    static          final int VIEW_TYPE        = 2;
	private                    static          final int TYPE_1           = 0;
	private                    static          final int TYPE_2           = 1;

	@Override
	public int getItemViewType(int position) 
	{
		int[] pos=getPosition(position);
		if (pos.length==1) 
		{
			return TYPE_1;
		}
		else 
		{
			return TYPE_2;
		}
	}
	
	@Override
	public int getViewTypeCount() 
	{
		return VIEW_TYPE;
	}

	@Override
	public int getCount() 
	{
		// TODO 自动生成的方法存根
		int sum=0;
		for (int i = 0; i < getChild().size(); i++) 
		{
			sum+=getChild().get(i).size();
		}
		return getGroup().size()+sum;
	}
	 
	@Override
	public Object getItem(int position) 
	{
		// TODO 自动生成的方法存根
		int[] pos=getPosition(position);
		if (pos.length==1)
		{
			return getGroup().get(pos[0]);
		}
		else 
		{
			return getChild().get(pos[0]).get(pos[1]);
		}
	}
	
	public abstract List<GroupBean> getGroup();
	public abstract List<List<ChildBean>> getChild();
	
	public long getGroupItemId(int position)
	{
		return 0;
	}
	
	public long getChildItemId(int groupPos,int childPos)
	{
		return 0;
	}
	
	public int[] getPosition(int position)
	{
		int pos[];
		int index=0;
		for (int i = 0; i < getChild().size(); i++) 
		{
			position-=getChild().get(i).size()+1;
			if (position<0) 
			{
				index=i;
				position+=getChild().get(i).size()+1;
				break;
			}
		}
		if (position==0) 
		{
			pos=new int[]{index};
		}
		else 
		{
			pos=new int[]{index,position-1};
		}
		return pos;
	}
	
	@Override
	public long getItemId(int position) 
	{
		int[] pos=getPosition(position);
		if (pos.length==1) 
		{
			return getGroupItemId(pos[0]);
		}
		else 
		{
			return getChildItemId(pos[0],pos[1]);
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		// TODO 自动生成的方法存根	
		int[] pos=getPosition(position);
		int type = getItemViewType(position);
		switch (type) 
		{
		case TYPE_1:
			convertView=getGroupView(convertView,getGroup().get(pos[0]),pos);
			break;
			
		case TYPE_2:
			convertView=getChildView(convertView,getChild().get(pos[0]).get(pos[1]),pos);
			break;
		}
		return convertView;
	}
	
	public abstract View getGroupView(View convertView,Object groupItem,int[] position);
	public abstract View getChildView(View convertView,Object childItem,int[] position);
}
