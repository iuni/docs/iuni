package com.sunnada.baseframe.adapter;

import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CompareAdapter extends BaseAdapter
{
	private List<Map<String, String>> mData;
	private int 					  mResource;
	private LayoutInflater			  mInflater;
	
	public CompareAdapter(List<Map<String, String>> mData, int mResource, Context mContext)
	{
		this.mData = mData;
		this.mResource = mResource;
		mInflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount()
	{
		return mData.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mData.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView tvTitle = null;
		TextView tvContent1 = null;
		TextView tvContent2 = null;
		TextView tvContent3 = null;
		ViewHolder holder   = null;
	
		if(convertView == null)
		{
			convertView = mInflater.inflate(mResource, null);
			tvTitle = (TextView) convertView.findViewById(R.id.tv_title); 
			tvContent1 = (TextView) convertView.findViewById(R.id.tv_content1);
			tvContent2 = (TextView) convertView.findViewById(R.id.tv_content2);
			tvContent3 = (TextView) convertView.findViewById(R.id.tv_content3);
			holder = new ViewHolder(tvTitle, tvContent1, tvContent2, tvContent3);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
			tvTitle = holder.tvTitle;
			tvContent1 = holder.tvContent1;
			tvContent2 = holder.tvContent2;
			tvContent3 = holder.tvContent3;
		}
		tvTitle.setText(mData.get(position).get("title"));
		tvContent1.setText(mData.get(position).get("content1"));
		tvContent2.setText(mData.get(position).get("content2"));
		tvContent3.setText(mData.get(position).get("content3"));
		return convertView;
	}

	class ViewHolder
	{
		public ViewHolder(TextView tvTitle, TextView tvContent1, TextView tvContent2, TextView tvContent3)
		{
			this.tvTitle = tvTitle;
			this.tvContent1 = tvContent1;
			this.tvContent2 = tvContent2;
			this.tvContent3 = tvContent3;
		}
		TextView tvTitle = null;
		TextView tvContent1 = null;
		TextView tvContent2 = null;
		TextView tvContent3 = null;
	}
}
