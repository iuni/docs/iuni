package com.sunnada.baseframe.adapter;

import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.PhoneModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryModelAdapter extends BaseAdapter
{
	private Context					mContext;
	private List<PhoneModel>		mData;
	private int						mResource;
	private OnClickListener			mListenner;

	public HistoryModelAdapter(Context context, List<PhoneModel> data, int resource, OnClickListener listenner)
	{
		this.mContext = context;
		this.mData = data;
		this.mResource = resource;
		this.mListenner = listenner;
	}

	@Override
	public int getCount()
	{
		return mData.size();
	}

	@Override
	public PhoneModel getItem(int arg0)
	{
		return mData.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2)
	{
		TextView mTvBand, mTvName, mTvOp, mTvCorenum, mTvPrice;
		ImageView mIvPreview;
		Button mBtnDetail;
		ViewHolder vHolder;
		if (convertView == null)
		{ 
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResource, null);
			mIvPreview = (ImageView) convertView.findViewById(R.id.ivPreview);
			mTvBand = (TextView) convertView.findViewById(R.id.tvBand);
			mTvName = (TextView) convertView.findViewById(R.id.tvName);
			mTvOp   = (TextView) convertView.findViewById(R.id.tvOp);
			mTvCorenum = (TextView) convertView.findViewById(R.id.tvCorenum);
			mTvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
			mBtnDetail = (Button) convertView.findViewById(R.id.btnDetail);
			vHolder = new ViewHolder(mTvBand, mTvName, mTvOp, mTvCorenum, mTvPrice, mBtnDetail, mIvPreview);
			convertView.setTag(vHolder);
		}
		else
		{
			vHolder = (ViewHolder) convertView.getTag();
			mIvPreview = vHolder.mIvPreview;
			mTvBand = vHolder.mTvBand;
			mTvName = vHolder.mTvName;
			mTvOp   = vHolder.mTvOp;
			mTvCorenum = vHolder.mTvCorenum;
			mTvPrice = vHolder.mTvPrice;
			mBtnDetail = vHolder.mBtnDetail;
		}
		final PhoneModel pm = mData.get(position);
		pm.showInImageView(mIvPreview);
		mTvBand.setText(pm.getBand());
		mTvName.setText(pm.getName());
		mTvCorenum.setText(pm.getCoreNum());
		mTvOp.setText(pm.getOp());
		mTvPrice.setText(pm.getPrice());
		mBtnDetail.setTag(pm);// ��������Model
		mBtnDetail.setOnClickListener(mListenner);
		return convertView;
	}

	class ViewHolder
	{
		TextView mTvBand, mTvName, mTvOp, mTvCorenum, mTvPrice;
		Button		mBtnDetail;
		ImageView 	mIvPreview;

		public ViewHolder(TextView mTvBand, TextView mTvName, TextView mTvOp, TextView mTvCorenum, TextView mTvPrice,
				Button mBtnDetail, ImageView mIvPreview)
		{
			this.mTvBand = mTvBand;
			this.mTvName = mTvName;
			this.mTvOp = mTvOp;
			this.mTvCorenum = mTvCorenum;
			this.mTvPrice = mTvPrice;
			this.mBtnDetail = mBtnDetail;
			this.mIvPreview = mIvPreview;
		}
	}
}
