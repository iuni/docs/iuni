package com.sunnada.baseframe.adapter;

import java.util.ArrayList;
import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PickNetWorkTelAdapter extends ArrayAdapter<String> implements OnClickListener
{
	private Activity		            m_context;
	
	private List<String>	            m_numberList	       = null;
	private List<String>	            m_feeList		       = null;
	private List<String>                mTelFeeLevelsList      = null;
	private List<View>                  mConvertViews          = new ArrayList<View>();

	private String			            selectedNum		       = null;
	private String                      mSelectedTelFeeLevel   = "";

	public PickNetWorkTelAdapter(Activity context, List<String> numberList, List<String> feeList)
	{
		super(context, R.layout.lay_picknetwork_telnum_listitem);
		m_context = context;
		m_numberList = numberList;
		m_feeList = feeList;
	}
	
	public PickNetWorkTelAdapter(Activity context, List<String> numberList, List<String> feeList,List<String> feeLevelList)
	{
		super(context, R.layout.lay_picknetwork_telnum_listitem);
		m_context = context;
		m_numberList = numberList;
		m_feeList = feeList;
		mTelFeeLevelsList = feeLevelList;
	}
	
	@Override
	public int getCount()
	{
		if (m_numberList == null)
		{
			return 0;
		}
		else
		{
			return (m_numberList.size() + 1) / 2;
		}
	};

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder viewHolder = new ViewHolder();
		LayoutInflater inflater = (LayoutInflater) m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout lay_num_1;
		LinearLayout lay_num_2;
		if(convertView == null)
		{
			convertView = inflater.inflate(R.layout.lay_picknetwork_telnum_listitem, null);
			lay_num_1 = (LinearLayout) convertView.findViewById(R.id.lay_num_1);
			lay_num_2 = (LinearLayout) convertView.findViewById(R.id.lay_num_2);
			viewHolder.setLayNum1(lay_num_1);
			viewHolder.setLayNum2(lay_num_2);
			convertView.setTag(viewHolder);
			mConvertViews.add(convertView);
		}
		else
		{
			viewHolder = (ViewHolder)convertView.getTag();
			lay_num_1 = viewHolder.getLayNum1();
			lay_num_2 = viewHolder.getLayNum2();
			lay_num_2.setVisibility(View.VISIBLE);
		}
		
		int size = m_numberList.size();
		
		int index1 = position * 2;
		int index2 = index1 + 1;

		if (index2 == size)
		{
			// 当数据末行时的特殊处理
			lay_num_2.setVisibility(View.GONE); 
		}

		String number1 = m_numberList.get(index1);
		
		if(selectedNum != null)
		{
			if(selectedNum.equals(number1))
			{
				restoreSelectedStatus(lay_num_1);
			}
			else
			{
				restoreNotSelectedStatus(lay_num_1);
			}
		}
		else
		{
			restoreNotSelectedStatus(lay_num_1);
		}
		
		String fee1 = null;
		if (m_feeList != null && m_feeList.size() > index1)
		{
			fee1 = m_feeList.get(index1);
		}
		if (StringUtil.isEmptyOrNull(fee1))
		{
			fee1 = "";
		}
		TextView et_num1 = (TextView) lay_num_1.getChildAt(0);
		TextView et_fee1 = (TextView) lay_num_1.getChildAt(1);

		et_num1.setText(number1.substring(0, 3) + "  " + number1.substring(3, 7) + "  " + number1.substring(7, 11));
		et_fee1.setText(adjustTelDetail(fee1));

		lay_num_1.setId(index1);
		
		lay_num_1.setTag(number1);
		lay_num_1.setOnClickListener(this);

		if (index2 != size)
		{
			String number2 = m_numberList.get(index2);
			
			if(selectedNum != null)
			{
				if(selectedNum.equals(number2))
				{
					restoreSelectedStatus(lay_num_2);
				}
				else
				{
					restoreNotSelectedStatus(lay_num_2);
				}
			}
			else
			{
				restoreNotSelectedStatus(lay_num_2);
			}
			
			String fee2 = null;
			if (m_feeList != null && m_feeList.size() > index2)
			{
				fee2 = m_feeList.get(index2);
			}
			
			if (StringUtil.isEmptyOrNull(fee2))
			{
				fee2 = "";
			}

			TextView et_num2 = (TextView) lay_num_2.getChildAt(0);
			TextView et_fee2 = (TextView) lay_num_2.getChildAt(1);

			et_num2.setText(number2.substring(0, 3) + "  " + number2.substring(3, 7) + "  " + number2.substring(7, 11));
			et_fee2.setText(adjustTelDetail(fee2));

			lay_num_2.setId(index2);
			lay_num_2.setTag(number2);
			lay_num_2.setOnClickListener(this);
		}
		return convertView;
	}

	// 恢复到未选中的状态
	public void restoreNotSelectedStatus(LinearLayout layView)
	{
		TextView et_num = (TextView) layView.getChildAt(0);
		TextView et_fee = (TextView) layView.getChildAt(1);
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			layView.setBackgroundResource(R.drawable.telnum_bg_slip_orange);
			et_num.setTextColor(ARGBUtil.getArgb(11));
			et_fee.setTextColor(ARGBUtil.getArgb(10));
		}
		else
		{
			layView.setBackgroundResource(R.drawable.telnum_bg_slip);
			et_num.setTextColor(Color.parseColor("#005461"));
			et_fee.setTextColor(Color.parseColor("#ABE6F3"));
		}
	}
	
	// 恢复到选中状态
	public void restoreSelectedStatus(LinearLayout layView)
	{
		TextView et_num = (TextView) layView.getChildAt(0);
		TextView et_fee = (TextView) layView.getChildAt(1);
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			layView.setBackgroundResource(R.drawable.selected_telnum_bg_slip_orange);
			et_num.setTextColor(ARGBUtil.getArgb(10));
			et_fee.setTextColor(ARGBUtil.getArgb(2));
		}
		else
		{
			layView.setBackgroundResource(R.drawable.selected_telnum_bg_slip);
			et_num.setTextColor(Color.parseColor("#9F0202"));
			et_fee.setTextColor(Color.parseColor("#F1593C"));
		}
	}
	
	// 号码详情字数为12个中文字符内,超出部分显示“…”;
	public String adjustTelDetail(String fee)
	{
		String telDetail = "";
		if(fee.length() <= 11)
		{
			telDetail = fee;
		}else
		{
			telDetail = fee.substring(0,11)+"...";
		}
		return telDetail;
	}
	
	public String getSelectedNumber()
	{
		return selectedNum;
	}

	public String getSelectedTelFeeLevel()
	{
		return mSelectedTelFeeLevel;
	}
	
	public void addData(List<String> newNumList, List<String> newFeeList)
	{
		m_numberList.addAll(newNumList);
		m_feeList.addAll(newFeeList);
		notifyDataSetChanged();
	}

	public void addData(List<String> newNumList, List<String> newFeeList,List<String> newFeeLevelList)
	{
		m_numberList.addAll(newNumList);
		m_feeList.addAll(newFeeList);
		mTelFeeLevelsList.addAll(newFeeLevelList);
		notifyDataSetChanged();
	}

	// 获取号码列表的长度
	public int getTelNumSize()
	{
		if (m_numberList == null)
		{
			return 0;
		}
		else
		{
			return m_numberList.size();
		}
	}
	
	// 清空号码及号码详情列表,擦除选中状态
	public void clear()
	{
		if(m_numberList != null)
		{
			m_numberList.clear();
		}
		
		if(m_feeList != null)
		{
			m_feeList.clear();
		}
		selectedNum = null;
		notifyDataSetChanged();
	}

	@Override
	public void onClick(View view)
	{
		try
		{
			selectedNum = view.getTag().toString();
			mSelectedTelFeeLevel = mTelFeeLevelsList.get(view.getId());
			
			LinearLayout lay_num_1;
			LinearLayout lay_num_2;
			
			for(int i=0; i<mConvertViews.size(); i++)
			{
				ViewHolder viewHolder = (ViewHolder) mConvertViews.get(i).getTag();
				lay_num_1 = viewHolder.getLayNum1();
				lay_num_2 = viewHolder.getLayNum2();
				restoreNotSelectedStatus(lay_num_1);
				restoreNotSelectedStatus(lay_num_2);
			}
			
			restoreSelectedStatus((LinearLayout)view);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		view.requestFocus();
	}
	
	class ViewHolder
	{
		private LinearLayout mLayNum1;
		private LinearLayout mLayNum2;
		
		public LinearLayout getLayNum1() 
		{
			return mLayNum1;
		}
		
		public void setLayNum1(LinearLayout layNum1) 
		{
			this.mLayNum1 = layNum1;
		}
		
		public LinearLayout getLayNum2() 
		{
			return mLayNum2;
		}
		
		public void setLayNum2(LinearLayout layNum2) 
		{
			this.mLayNum2 = layNum2;
		}
	}
}
