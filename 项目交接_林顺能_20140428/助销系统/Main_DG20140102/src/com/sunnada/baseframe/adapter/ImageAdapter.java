package com.sunnada.baseframe.adapter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.sunnada.baseframe.bean.Statics;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ConstantData;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.ui.GalleryFlow;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.BitmapPoolUtil.onImageGotListener;

public class ImageAdapter extends BaseAdapter 
{
	private Context 				mContext;
	private List<PhoneModel> 		mListPhoneModel 	= null;
	private Map<String, Bitmap>		mMapReflectBitmap 	= new HashMap<String, Bitmap>();
	private Map<String, Bitmap>		mBigBitmap 			= new HashMap<String, Bitmap>();
	private static int 				REFLECTION_GAP	 	= 3;			// 倒影图和原图之间的距离
	private final int				WIDTH				= 210;
	private final int				HEIGHT				= 280;
	public  final int				BIG_WIDTH			= 420;
	public  final int				BIG_HEIGHT			= 560;
	
	public ImageAdapter(Context context, List<PhoneModel> models) 
	{
		mContext 		= context;
		mListPhoneModel = models;
	}
	
	public Object getItem(int position) 
	{
		return position;
	}

	public long getItemId(int position) 
	{
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) 
	{
		try
		{
			if(convertView == null)
			{
				convertView  = new ImageView(mContext);
				convertView.setLayoutParams(new GalleryFlow.LayoutParams(WIDTH, HEIGHT));
			}
			Log.e("getView", "position = " + position);
			Log.e("pic", "getView1" + " position:" + position);
			getBitmap((ImageView)convertView, position);
			Log.e("pic", "getView2" + " position:" + position);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return convertView;
	}
	
	public void getBitmap(final ImageView imageView, int position)
	{
		getBimap(imageView, position, new int[]{WIDTH, HEIGHT}, false);
	}
	
	public void getBimap(final ImageView imageView, int position, final int[] size, boolean isReload)
	{
		PhoneModel model = mListPhoneModel.get(position); 
		final String strLocalPath = Statics.PATH_PIC + model.getId() + "/" + model.getBigPic();
		// 如果容器中已经存在有倒影的bitmap
		if(mMapReflectBitmap.containsKey(strLocalPath)) 
		{
			if(isReload)
			{
				Log.e("getBitmap", "缓存中存在，选择重新加载" + position);
				mMapReflectBitmap.remove(strLocalPath).recycle();
				// 移除原始图片
				BitmapPoolUtil.remove(strLocalPath);
				getBimap(imageView, position, size, false);
			}
			else
			{
				Log.e("getBitmap", "缓存中存在:" + mMapReflectBitmap.get(strLocalPath));
				imageView.setImageBitmap(mMapReflectBitmap.get(strLocalPath));
			}
		} 
		else
		{
			Log.e("getBitmap", "缓存中不存在...position:" + position);	
			// 如果容器中不存在, 则从本地取或者从网络取
			final Bitmap originalImage = BitmapPoolUtil.getBitmap(strLocalPath, 
			ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + model.getBigPic(), 
			new onImageGotListener() 
			{
				@Override
				public void onImageGot(boolean result) 
				{ 
					if(result == true) 
					{
						Bitmap bitmap = BitmapPoolUtil.getBitmap(strLocalPath, null, null, size);
						Bitmap newBitmap = getReflectionBitmap(bitmap);
						// 将有倒影的图片加到容器里面
						imageView.setImageBitmap(newBitmap);
						mMapReflectBitmap.put(strLocalPath, newBitmap);
					}
					else
					{
						imageView.setImageResource(R.drawable.bg_no_model);
					}
				}
			}, size);
			// 如果本地不存在原始图片
			if(originalImage == null) 
			{
				imageView.setImageResource(R.drawable.bg_model_loading);
			}
			else
			{
				Bitmap bitmapWithReflection = getReflectionBitmap(originalImage);
				imageView.setImageBitmap(bitmapWithReflection);
				// 将有倒影的图片加到容器里面
				mMapReflectBitmap.put(strLocalPath, bitmapWithReflection);
			}
		}
	}
	
	// 刷新当前选中图为清晰图
	public void getBigBitmap(final ImageView imageView, final int position, final int[] size)
	{
		Log.e("pic", "getBigBitmap1" + " position:" + position);
		new Thread()
		{
			@Override
			public void run()
			{
				PhoneModel model = mListPhoneModel.get(position); 
				final String strLocalPath = Statics.PATH_PIC + model.getId() + "/" + model.getBigPic();
				Bitmap bigOriginalBitmap = BitmapPoolUtil.getBitmap(strLocalPath, 
					ConstantData.getServerPath() + ConstantData.rangeFilePath + "/" + model.getBigPic(), 
					new onImageGotListener() 
					{
						@Override
						public void onImageGot(boolean result) 
						{ 
							if(result == true) 
							{
								Bitmap bitmap = BitmapPoolUtil.getBitmap(strLocalPath, null, null, size);
								final Bitmap newBitmap = getReflectionBitmap(bitmap);
								imageView.postDelayed(new Runnable()
								{
									@Override 
									public void run()  
									{
										Log.e("getBigBitmap", "刷新选中大图");
										imageView.setImageBitmap(newBitmap);
									}
								}, 300);
								mBigBitmap.put(strLocalPath, newBitmap);
								Log.e("getBigBitmap", "回收大图原始图片资源：" + newBitmap);
								bitmap.recycle();
								bitmap = null;
							}
							else
							{
								imageView.post(new Runnable()
								{
									public void run()
									{
										imageView.setImageResource(R.drawable.bg_no_model);
									}
								});
							}
						}
					}, size, false);
					// 如果本地不存在原始图片
					Log.e("getBigBitmap", " position:" + position);
					if(bigOriginalBitmap == null) 
					{
						Log.e("getBigBitmap", " position1:" + position);
						imageView.post(new Runnable()
						{
							public void run()
							{
								imageView.setImageResource(R.drawable.bg_model_loading);
							}
						});
					}
					else
					{
						
						final Bitmap bitmap = getReflectionBitmap(bigOriginalBitmap);
						Log.e("getBigBitmap", " position 2:" + position);
						imageView.postDelayed(new Runnable()
						{
							@Override 
							public void run()  
							{
								Log.e("pic", "getBigBitmap2" + " position:" + position);
								Log.e("getBigBitmap", "刷新选中大图");
								imageView.setImageBitmap(bitmap);
							}
						}, 300);
						mBigBitmap.put(strLocalPath, bitmap);
						Log.e("getBigBitmap", "回收大图原始图片资源：" + bigOriginalBitmap);
						bigOriginalBitmap.recycle();
						bigOriginalBitmap = null;
					}
			}
		}.start();
	}

	public float getScale(boolean focused, int offset) 
	{
		return Math.max(0, 1.0f/(float) Math.pow(2, Math.abs(offset)));
	}

	@Override
	public int getCount() 
	{
		//return mImages.length;
		return mListPhoneModel.size();
	}

	private static Bitmap getReflectionBitmap(Bitmap originalImage) 
	{
		int width  = originalImage.getWidth();
		int height = originalImage.getHeight();

		// 创建矩阵对象
		Matrix matrix = new Matrix();
		// 指定一个角度以0,0为坐标进行旋转
		//matrix.setRotate(30);
		// 指定矩阵(x轴不变，y轴相反)
		matrix.preScale(1, -1);

		// 将矩阵应用到该原图之中，返回一个宽度不变，高度为原图1/2的倒影位图
		Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height*3/4, width, height/4, matrix, false);
		// 创建一个宽度不变，高度为原图+倒影图高度的位图
		Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height+height/4), Config.ARGB_8888);

		// 将上面创建的位图初始化到画布
		Canvas canvas = new Canvas(bitmapWithReflection);
		canvas.drawBitmap(originalImage, 0, 0, null);
		canvas.drawBitmap(reflectionImage, 0, height+REFLECTION_GAP, null);
		
		//Paint deafaultPaint = new Paint();
		//deafaultPaint.setAntiAlias(false);
		//canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);
		
		Paint paint = new Paint();
		paint.setAntiAlias(false);
		// 参数一:为渐变起初点坐标x位置， 参数二:为y轴位置， 参数三和四:分辨对应渐变终点， 最后参数为平铺方式，
		// 这里设置为镜像Gradient是基于Shader类，所以我们通过Paint的setShader方法来设置这个渐变
		LinearGradient shader = new LinearGradient(0, originalImage.getHeight(), 0, bitmapWithReflection.getHeight()+REFLECTION_GAP, 
				0x70ffffff, 0x00ffffff, TileMode.MIRROR);
		// 设置阴影
		paint.setShader(shader);
		paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_IN));
		// 用已经定义好的画笔构建一个矩形阴影渐变效果
		canvas.drawRect(0, height, width, bitmapWithReflection.getHeight()+REFLECTION_GAP, paint);
		
		// 回收reflectionImage
		reflectionImage.recycle();
		reflectionImage = null;
		
		return bitmapWithReflection;
	}
	
	public void clearImages() 
	{
		try
		{
			// 清除bitmap
			Iterator<Entry<String, Bitmap>> itr = mMapReflectBitmap.entrySet().iterator();
			while(itr.hasNext()) 
			{
				Entry<String, Bitmap> entry = itr.next();
				String strLocalPath = entry.getKey();
				Bitmap bitmap = entry.getValue();
				if(bitmap != null && !bitmap.isRecycled()) 
				{
					Log.e("", "正在释放: " + strLocalPath + " 的倒影图片...");
					bitmap.recycle();
					bitmap = null;
				}
				strLocalPath = null;
				entry = null;
			}
			
			itr = mBigBitmap.entrySet().iterator();
			while(itr.hasNext()) 
			{
				Entry<String, Bitmap> entry = itr.next();
				Bitmap bitmap = entry.getValue();
				if(bitmap != null && !bitmap.isRecycled()) 
				{
					Log.e("getBigBitmap", "回收清晰图倒影资源...");
					bitmap.recycle();
					bitmap = null;
					System.gc();
				}
				itr.remove();
				entry = null;
			}
			mBigBitmap.clear();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	// 清除当前没显示的图片资源
	public void recycleImage(int firstPos, int lastPos) 
	{
		try
		{
			for(int i = 0; i < mListPhoneModel.size(); i++) 
			{
				if(i >= firstPos && i <= lastPos) 
				{
					//Log.e("", "cur :"+i); 
					continue;
				}
				String strLocalPath = Statics.PATH_PIC + mListPhoneModel.get(i).getId() 
						+ "/" + mListPhoneModel.get(i).getBigPic();
				if(mMapReflectBitmap.containsKey(strLocalPath)) 
				{
					Bitmap bitmap = mMapReflectBitmap.remove(strLocalPath);
					if(bitmap != null && !bitmap.isRecycled())
					{
						bitmap.recycle();
						bitmap = null;
						Log.e("", "释放图片资源, position: " + i + ", 路径: " + strLocalPath);
						System.gc();
					}
				}
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}

	public void clearBigImages()
	{
		if(mListPhoneModel.size() > 0)
		{
			for(int i = 0; i < mListPhoneModel.size(); i++) 
			{
				String strLocalPath = Statics.PATH_PIC + mListPhoneModel.get(i).getId() 
						+ "/" + mListPhoneModel.get(i).getBigPic();
				if(mBigBitmap.containsKey(strLocalPath))
				{
					Bitmap bitmap = mBigBitmap.remove(strLocalPath);
					if(bitmap != null && !bitmap.isRecycled())
					{
						bitmap.recycle();
						bitmap = null;
						Log.e("", "释放清晰图片资源, position: " + i + ", 路径: " + strLocalPath);
						System.gc();
					}
				}
			}
			this.notifyDataSetChanged();
		}
	}
}
