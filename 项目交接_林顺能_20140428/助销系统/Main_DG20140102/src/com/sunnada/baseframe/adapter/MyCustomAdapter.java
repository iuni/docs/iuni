package com.sunnada.baseframe.adapter;

import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ChildBean;
import com.sunnada.baseframe.bean.GroupBean;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class MyCustomAdapter extends TitleAdapter
{
	//private Context							mCon;
	private List<GroupBean>						mGroupList;
	private List<List<ChildBean>>				mChildList;
	private LayoutInflater						mLayoutInflater;
	
	public MyCustomAdapter(Context context, List<GroupBean> groupList, List<List<ChildBean>> childList) 
	{
		//this.mCon = context;
		this.mGroupList = groupList;
		this.mChildList = childList;
		mLayoutInflater = LayoutInflater.from(context);
	}

	@Override
	public List<GroupBean> getGroup() 
	{
		return mGroupList;
	}

	@Override
	public List<List<ChildBean>> getChild() 
	{
		return mChildList;
	}

	@Override
	public View getGroupView(View convertView, Object groupItem,int[] pos) 
	{
		if(convertView == null)
		{
			convertView = mLayoutInflater.inflate(R.layout.lay_parent_bill, null);
		}
		TextView tv_subject = (TextView)convertView.findViewById(R.id.tv_title);
		TextView tv_detail = (TextView)convertView.findViewById(R.id.tv_detail);
		tv_subject.setText(mGroupList.get(pos[0]).getmSubject());
		tv_detail.setText(mGroupList.get(pos[0]).getmDetail());
		return convertView;
	}

	@Override
	public View getChildView(View convertView, Object childItem,int[] pos) 
	{
		if(convertView == null)
		{
			convertView = mLayoutInflater.inflate(R.layout.lay_child_bill, null);
		}
		TextView tv_subject = (TextView)convertView.findViewById(R.id.tv_item_title);
		TextView tv_detail = (TextView)convertView.findViewById(R.id.tv_item_detail);
		tv_subject.setText("-"+mChildList.get(pos[0]).get(pos[1]).getmSubject());
		tv_detail.setText(mChildList.get(pos[0]).get(pos[1]).getmDetail());
		return convertView;
	}
}
