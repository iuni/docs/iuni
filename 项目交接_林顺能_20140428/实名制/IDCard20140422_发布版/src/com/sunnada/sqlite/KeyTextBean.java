package com.sunnada.sqlite;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KeyTextBean implements Serializable
{    
	private String 			mKeyValue;
	private String 			mTextValue;

	public KeyTextBean() 
	{
		
	}

	public KeyTextBean(String key, String text) 
	{        
		super();
		
		this.mKeyValue = key;        
		this.mTextValue = text;    
	}    

	@Override
	public String toString() 
	{
		return mTextValue;
	}

	public String getKey() 
	{
		return mKeyValue;
	}

	public void setKey(String key) 
	{
		this.mKeyValue = key;
	}

	public String getText() 
	{
		return mTextValue;
	}

	public void setText(String text) 
	{
		this.mTextValue = text;
	}
}
	 