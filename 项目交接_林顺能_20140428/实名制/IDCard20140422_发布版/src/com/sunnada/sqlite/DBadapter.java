package com.sunnada.sqlite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public abstract class DBadapter 
{
	protected static String 			DB_TABLE;
	private Context 					mContext;
	public static SQLiteDatabase 		mDB;
	private static Semaphore  			mSem = new Semaphore(1);
	private DBHelper 					mDbHelper;

	public DBadapter(Context context) 
	{
		this.mContext = context;
	}
	
	public boolean open() throws SQLException
	{
		try 
		{
			mSem.acquire();
		} 
		catch (InterruptedException e) 
		{
			mSem.release();
			e.printStackTrace();
			return false;
		}
		
		mDbHelper = new DBHelper(mContext);
		mDB = mDbHelper.getWritableDatabase();
		
		if(mDB == null) 
		{
			mSem.release();
			return false;
		}
		return true;
	}

	public void close()
	{
		if(mDB != null) 
		{
			mDB.close();
		}
		if(mDbHelper != null) 
		{
			mDbHelper.close();
		}
		mSem.release();
	}

	public static HashMap<String, String> cursorToMap(Cursor cursor) 
	{
		HashMap<String, String> map = new HashMap<String, String>();
		if(cursor == null || !cursor.moveToFirst()) 
		{
			return new HashMap<String, String>();
		}
		else 
		{
			String[] names = cursor.getColumnNames();
			for(String name:names) 
			{
				map.put(name, cursor.getString(cursor.getColumnIndex(name)));
			}
		}
		return map;
	}

	public static List<Map<String, String>> cursorToList(Cursor cursor)
	{
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		if(cursor != null && cursor.moveToFirst())
		{
			do 
			{
				HashMap<String, String> map = new HashMap<String, String>();
				String[] names = cursor.getColumnNames();
				for(String name:names) 
				{
					map.put(name, cursor.getString(cursor.getColumnIndex(name)));
				}
				list.add(map);
			} while (cursor.moveToNext());
		}
		return list;
	}
}
