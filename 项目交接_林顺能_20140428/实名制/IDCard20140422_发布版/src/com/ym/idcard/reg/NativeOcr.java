package com.ym.idcard.reg;

public class NativeOcr 
{
	private static final String LIB = "IDCardengine";
	private static final String TAG = "NativeOcr";
	private static boolean mCancel;
	private static int mProgress;

	static {
		try {
			System.loadLibrary("IDCardengine");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int Progress(int paramInt1, int paramInt2) {
		return 100;
	}

	public static int getProgress() {
		return mProgress;
	}

	public static void reset() {
		mProgress = 0;
		mCancel = false;
	}

	public native int CodeConvert(long paramLong, byte[] paramArrayOfByte,
			int paramInt);

	public native int DoLineOCR(long paramLong1, long paramLong2,
			byte[] paramArrayOfByte, int paramInt);

	public native int GetCropAndDeskewInfo(long paramLong1, long paramLong2,
			int[] paramArrayOfInt, long paramLong3);

	public native int HistEqualizationForRGB(long paramLong);

	public native int ImageConvert(long paramLong1, long paramLong2,
			int paramInt);

	public native int closeBCR(long[] paramArrayOfLong);

	public native int codeConvert(long paramLong, byte[] paramArrayOfByte,
			int paramInt);

	public native int doImageBCR(long paramLong1, long paramLong2,
			long[] paramArrayOfLong);

	public void finalize() {
	}

	public native void freeBField(long paramLong1, long paramLong2, int paramInt);

	public native int freeImage(long paramLong, long[] paramArrayOfLong);

	public native int getFieldId(long paramLong);

	public native int getFieldRect(long paramLong, int[] paramArrayOfInt);

	public native int getFieldText(long paramLong, byte[] paramArrayOfByte,
			int paramInt);

	public native int getLastErr(long paramLong, byte[] paramArrayOfByte,
			int paramInt);

	public native long getNextField(long paramLong);

	public native int getPinYin(long paramLong, byte[] paramArrayOfByte,
			int paramInt);

	public native int getVersionString(byte[] paramArrayOfByte, int paramInt);

	public native long getYData(byte[] paramArrayOfByte, int paramInt1,
			int paramInt2);

	public native int hasPinYin(long paramLong);

	public native int imageChecking(long paramLong1, long paramLong2,
			int paramInt);

	public native long loadImageMem(long paramLong1, long paramLong2,
			int paramInt1, int paramInt2, int paramInt3);

	public native void setProgressFunc(long paramLong, boolean paramBoolean);

	public native int setoption(long paramLong, byte[] paramArrayOfByte1,
			byte[] paramArrayOfByte2);

	public native int startBCR(long[] paramArrayOfLong,
			byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt);
}
