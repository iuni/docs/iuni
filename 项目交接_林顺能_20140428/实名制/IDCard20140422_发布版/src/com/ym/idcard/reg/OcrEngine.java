package com.ym.idcard.reg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
//import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.chinaunicom.custinforegist.api.Product;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.base.Setting;
import com.chinaunicom.custinforegist.kit.util.StringUtil;

public class OcrEngine 
{
	private static final String TAG = OcrEngine.class.getSimpleName();

	public static final int BIDC_NON = 0;
	public static final int BIDC_NAME = 1;
	public static final int BIDC_CARDNO = 3;
	public static final int BIDC_SEX = 4;
	public static final int BIDC_BIRTHDAY = 5;
	public static final int BIDC_ADDRESS = 6;
	public static final int BIDC_ISSUE_AUTHORITY = 7;
	public static final int BIDC_VALID_PERIOD = 9;
	public static final int BIDC_FOLK = 11;
	public static final int BIDC_MEMO = 99;

	public static final int IDC_COUNTRY = 10;
	public static final int IDC_ENGINE_PN = 18;
	public static final int IDC_MODEL = 16;
	public static final int IDC_NAME_CH = 2;
	public static final int IDC_PN = 12;
	public static final int IDC_USE_CHARACE = 15;
	public static final int IDC_VEHICLE_TYPE = 14;
	public static final int IDC_VIN = 17;

	public static final int MIN_HEIGHT_LIMIT = 720;
	public static final int MIN_WIDTH_LIMIT = 1024;

	public static final int OCR_CODE_B5 = 2;
	public static final int OCR_CODE_GB = 1;
	public static final int OCR_CODE_GB2B5 = 3;
	public static final int OCR_CODE_NIL = 0;
	public static final int OCR_LAN_CENTEURO = 7;
	public static final int OCR_LAN_CHINESE = 2;
	public static final int OCR_LAN_ENGLISH = 1;
	public static final int OCR_LAN_EUROPEAN = 3;
	public static final int OCR_LAN_JAPAN = 6;
	public static final int OCR_LAN_NIL = 0;
	public static final int OCR_LAN_RUSSIAN = 4;

	public static final int RECOG_BLUR = 3;
	public static final int RECOG_BLUR_TIP = 5;
	public static final int RECOG_CANCEL = -1;
	public static final int RECOG_CROP_DESKEW = 6;
	public static final int RECOG_FAIL = -2;
	public static final int RECOG_LANGUAGE = 4;
	public static final int RECOG_NONE = 0;
	public static final int RECOG_OK = 1;
	public static final int RECOG_SMALL = 2;

	private boolean 		OPT_CANCEL;
	private Bitmap 			bitmap;
	private Bitmap 			fieldBitmap;
	protected boolean 		mBeCancel;
	protected NativeOcr 	mOcr = new NativeOcr();
	protected long 			m_pCurField;
	protected long 			m_pEngine;
	protected long 			m_pField;
	protected long 			m_pImage;
	protected long 			m_ppEngine[] = new long[1];
	protected long 			m_ppField[] = new long[1];
	protected long 			m_ppImage[] = new long[1];

	public OcrEngine() 
	{
		
	}

	private static boolean checkOcrLanguage(int i) 
	{
		switch (Product.REG_MARK) 
		{
		case 3:
			if (i != 1 && i != 3) {
				return false;
			} else {
				return true;
			}
		case 11:
			if (i == 1 || i == 4) {
				return true;
			} else {
				return false;
			}
		case 12:
			if (i == 1 || i == 2 || i == 6) {
				return true;
			} else {
				return false;
			}
		default:
			if (i == 1 || i == 2 || i == 3) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static int getProgress() {
		return NativeOcr.getProgress();
	}

	public boolean checkOcrEngine(int i) {
		boolean flag = false;
		if (checkOcrLanguage(i) && startBCR(App.getAppLibPath() + "/ScanBcr_mob.cfg",App.getAppLibPath(), i)) {
			ImageEngine imageengine = new ImageEngine();
			if (imageengine.init(1, 90)) {
				imageengine.finalize();
				flag = true;
			} else {
				imageengine.finalize();
			}
		}
		return flag;
	}

	public void closeBCR() {
		if (m_ppEngine != null && mOcr != null) {
			mOcr.closeBCR(m_ppEngine);
			m_ppEngine[0] = 0L;
			m_pEngine = 0L;
		}
	}

	public void doCancel() {
		OPT_CANCEL = true;
	}

	public boolean doImageBCR() {
		boolean flag = false;
		mBeCancel = false;
		if (Setting.is86DetectionOn())
			mOcr.setoption(m_pEngine, StringUtil.convertToUnicode("-aic"), null);
		int i = mOcr.doImageBCR(m_pEngine, m_pImage, m_ppField);
		if (i == 1) {
			m_pField = m_ppField[0];
			m_pCurField = m_pField;
			flag = true;
		} else {
			if (i == 3) {
				mBeCancel = true;
				flag = false;
			}
		}
		return flag;
	}

	public boolean fields2Bizcard(IdentityCard card, int ocrLanguage,
			int keyLanguage) {
		if (card != null) {
			try {
				bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().toString() + "/idcard/img_ocr.bmp");
			} catch (OutOfMemoryError e) {
			}
			while (!isFieldEnd()) 
			{
				switch (getFieldId()) {
				case 1:
					card.setName(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 2:
					card.setNo(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 3:
					card.setSex(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 4:
					card.setFlok(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 5:
					card.setBirthday(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 6:
					card.setAddress(getFieldText(ocrLanguage, keyLanguage));
					break;
				case 7:
					card.setIssueAuthority(getFieldText(ocrLanguage,
							keyLanguage));
					break;
				case 8:
					card.setValidPreod(getFieldText(ocrLanguage, keyLanguage));
					break;
				default:
					break;
				}
				getNextField();
			}
			return true;
		}
		return false;
	}

	public void finalize() {
		m_ppEngine = null;
		m_ppImage = null;
		m_ppField = null;
		mOcr = null;
		m_pEngine = 0L;
		m_pImage = 0L;
		m_pField = 0L;
	}

	public void freeBFields() {
		if (mOcr != null) {
			mOcr.freeBField(m_pEngine, m_pField, 0);
			m_ppField[0] = 0L;
			m_pField = 0L;
			m_pCurField = 0L;
		}
	}

	public void freeImage() {
		if (mOcr != null) {
			mOcr.freeImage(m_pEngine, m_ppImage);
			m_ppImage[0] = 0L;
			m_pImage = 0L;
		}
	}

	public void getCropAndDeskewInfo(Rect rect, long l) {
		int ai[] = new int[4];
		mOcr.GetCropAndDeskewInfo(m_pEngine, m_pImage, ai, l);
		rect.left = ai[0];
		rect.top = ai[1];
		rect.right = ai[2];
		rect.bottom = ai[3];
		long l1 = l / 100L;
		Log.v(TAG, "getCropAndDeskewInfo --> x1 =" + rect.left + ";;y1 ="
				+ rect.top + ";;x2 =" + rect.right + ";;y2 =" + rect.bottom);
		Log.v(TAG, "getCropAndDeskewInfo --> angleOut =" + l1);
	}

	public int getFieldId() {
		return mOcr.getFieldId(m_pCurField);
	}

	public String getFieldImage() {
		String s = null;
		String s1;
		Rect rect = getFieldRect();
		Log.e(TAG, (new StringBuilder(String.valueOf(rect.left))).append(" ")
				.append(rect.top).append(" ").append(rect.right).append(" ")
				.append(rect.bottom).toString());
		rect.left = Math.abs(rect.left);
		rect.top = Math.abs(rect.top);
		rect.right = Math.abs(rect.right);
		rect.bottom = Math.abs(rect.bottom);
		Log.e(TAG, (new StringBuilder(String.valueOf(rect.left))).append(" ")
				.append(rect.top).append(" ").append(rect.right).append(" ")
				.append(rect.bottom).toString());
		if (bitmap == null || rect.left < 0 || rect.top < 0 || rect.right <= 0
				|| rect.bottom <= 0) {
			// break MISSING_BLOCK_LABEL_362;
		} else {
			fieldBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top,
					rect.width(), rect.height());
			s1 = (new StringBuilder(String.valueOf((new Date()).getTime())))
					.append(".jpg").toString();
			FileOutputStream fileoutputstream = null;
			try {
				fileoutputstream = new FileOutputStream(new File(App.FIELD_DIR,
						s1));
				fieldBitmap.compress(
						android.graphics.Bitmap.CompressFormat.JPEG, 50,
						fileoutputstream);
				s = (new StringBuilder(String.valueOf(App.FIELD_DIR)))
						.append("/").append(s1).toString();
				fileoutputstream.flush();
				fileoutputstream.close();
				if (fieldBitmap != null && !fieldBitmap.isRecycled()) {
					fieldBitmap.recycle();
					fieldBitmap = null;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return s;
		// FileNotFoundException filenotfoundexception;
		// filenotfoundexception;
		// Log.e("fields2Bizcard", filenotfoundexception);
		// if(fieldBitmap != null && !fieldBitmap.isRecycled())
		// {
		// fieldBitmap.recycle();
		// fieldBitmap = null;
		// }
		// continue; /* Loop/switch isn't completed */
		// IOException ioexception;
		// ioexception;
		// Log.e("fields2Bizcard", ioexception);
		// if(fieldBitmap != null && !fieldBitmap.isRecycled())
		// {
		// fieldBitmap.recycle();
		// fieldBitmap = null;
		// }
		// if(true) goto _L2; else goto _L1
		// _L1:
		// Exception exception;
		// exception;
		// if(fieldBitmap != null && !fieldBitmap.isRecycled())
		// {
		// fieldBitmap.recycle();
		// fieldBitmap = null;
		// }
		// throw exception;
	}

	public Rect getFieldRect() {
		Rect rect = new Rect();
		int ai[] = new int[4];
		mOcr.getFieldRect(m_pCurField, ai);
		rect.left = ai[0];
		rect.top = ai[1];
		rect.right = ai[2];
		rect.bottom = ai[3];
		return rect;
	}

	public String getFieldText(int ocrLang, int keyLang) {
		byte[] abyte0 = new byte[256];
		mOcr.getFieldText(m_pCurField, abyte0, 256);
		String s;
		if (keyLang == 3 && ocrLang == 2) {
			mOcr.codeConvert(m_pEngine, abyte0, keyLang);
			s = StringUtil.convertBig5ToUnicode(abyte0);
		} else if (ocrLang == 3)
			s = StringUtil.convertAscIIToUnicode(abyte0);
		else if (ocrLang == 4)
			s = StringUtil.convertAscIIToUnicodeRS(abyte0);
		else if (ocrLang == 6)
			s = StringUtil.convertAscIIToUnicodeJP(abyte0);
		else
			s = StringUtil.convertGbkToUnicode(abyte0);
		return s;
	}

	public void getNextField() {
		if (!isFieldEnd())
			m_pCurField = mOcr.getNextField(m_pCurField);
	}

	public String getPinYin() {
		long l = m_pCurField;
		byte abyte0[] = new byte[256];
		mOcr.getPinYin(l, abyte0, 256);
		return StringUtil.convertGbkToUnicode(abyte0);
	}

	public long getYData(byte abyte0[], int i, int j) {
		return mOcr.getYData(abyte0, i, j);
	}

	public boolean hasPinYin() {
		boolean flag = true;
		long l = m_pCurField;
		if (mOcr.hasPinYin(l) != 1)
			flag = false;
		return flag;
	}

	public void histEqualizationForRGB() {
		mOcr.HistEqualizationForRGB(m_pImage);
	}

	public boolean isBlurImage() {
		boolean flag = false;
		if (mOcr != null && mOcr.imageChecking(m_pEngine, m_pImage, 0) == 2)
			flag = true;
		return flag;
	}

	public boolean isCancel() {
		boolean flag;
		if (!mBeCancel && !OPT_CANCEL)
			flag = false;
		else
			flag = true;
		return flag;
	}

	public boolean isFieldEnd() {
		return m_pCurField == 0;
	}

	public boolean loadImageMem(long ex, int width, int height, int comp) {
		if (ex != 0L) {
			m_pImage = mOcr.loadImageMem(m_pEngine, ex, width, height, comp);
			if (m_pImage != 0) {
				m_ppImage[0] = m_pImage;
				return true;
			}
		}
		return false;
	}

	public int recognize(byte abyte0[], int ocrLanguage, int keyLanguage,
			boolean isBlurDetectionOn, IdentityCard bizcard, Handler handler, String s) 
	{
		byte result = -2;
		Log.v("OcrAdapter", "开始识别...");
		OPT_CANCEL = false;
		if (ocrLanguage > 29 && ocrLanguage < 40) 
		{
			ocrLanguage = 3;
		}
		if (ocrLanguage > 19 && ocrLanguage < 30) 
		{
			ocrLanguage = 2;
		}
		// byte byte1 = 4;
		// if (checkOcrLanguage(i)) {
		if (startBCR(App.getAppLibPath() + "/ScanBcr_mob.cfg", App.getAppLibPath(), ocrLanguage)) 
		{
			ImageEngine engine = new ImageEngine();
			if (engine.init(1, 90) && engine.load(abyte0, s)) 
			{
				int width = engine.getWidth();
				int height = engine.getHeight();
				int comp = engine.getComponent();
				
				/*
				Log.d("", "Build.MODEL = " + Build.MODEL);
				if (!Build.MODEL.contains("SO-01")
						&& !Build.MODEL.contains("X10")
						&& !Build.MODEL.contains("E10")
						&& (width < MIN_WIDTH_LIMIT || height < MIN_HEIGHT_LIMIT)) 
				{
					Log.e("", "params illegal...");
					engine.finalize();
					closeBCR();
					return OPT_CANCEL? -1:2;
				}
				*/
				if (loadImageMem(engine.getDataEx(), width, height, comp)) 
				{
					Log.v(TAG, "inside loadImageMem");
					setProgressFunc(true);
					if (isBlurDetectionOn && isBlurImage() && !OPT_CANCEL) 
					{
						freeImage();
						engine.finalize();
						closeBCR();
						return OPT_CANCEL ? -1 : 3;
					}
					if (doImageBCR()) 
					{
						Log.v(TAG, "inside doImageBCR");
						if (fields2Bizcard(bizcard, ocrLanguage, keyLanguage))
						{
							result = 1;
						}
						freeBFields();
					} 
					else if (isCancel()) 
					{
						result = -1;
						freeBFields();
					}
					freeImage();
					engine.finalize();
					closeBCR();
				}
			}
		}
		return OPT_CANCEL ? -1:result;
		// } else {
		// byte1 = 4;
		// }// goto _L2; else goto _L1
		// return byte1;
	}
	
	// public int recognizeMulti(byte abyte0[], int i, int j, boolean flag,
	// Bizcard bizcard, Handler handler, String s) {
	// byte byte0;
	// OPT_CANCEL = false;
	// byte0 = -2;
	// if (i > 29 && i < 40)
	// i = 3;
	// byte byte1 = 4;
	// if (checkOcrLanguage(i)) {
	// ImageEngine imageengine = new ImageEngine();
	// if (imageengine.init(1, 90) && imageengine.load(abyte0, s)) {
	// int k = imageengine.getWidth();
	// int l = imageengine.getHeight();
	// int i1 = imageengine.getComponent();
	// if (k < 1024 || l < 720) {
	// imageengine.finalize();
	// byte1 = 2;
	// // continue; /* Loop/switch isn't completed */
	// }
	// if (loadImageMem(imageengine.getDataEx(), k, l, i1)) {
	// setProgressFunc(true);
	// if (flag && isBlurImage()) {
	// freeImage();
	// imageengine.finalize();
	// if (OPT_CANCEL)
	// byte1 = -1;
	// else
	// byte1 = 3;
	// // continue; /* Loop/switch isn't completed */
	// }
	// if (doImageBCR()) {
	// if (fields2Bizcard(bizcard, i, j))
	// byte0 = 1;
	// freeBFields();
	// } else if (isCancel()) {
	// byte0 = -1;
	// freeBFields();
	// }
	// freeImage();
	// }
	// }
	// imageengine.finalize();
	// if (OPT_CANCEL)
	// byte1 = -1;
	// else
	// byte1 = byte0;
	// } else {
	// byte1 = 4;
	// }// goto _L2; else goto _L1
	// return byte1;
	// }

	public void setCancel(boolean flag) {
		OPT_CANCEL = flag;
	}

	public void setPause(boolean flag) 
	{
		//OPT_PAUSE = flag;
	}

	public void setProgressFunc(boolean flag) 
	{
		if (m_pEngine != 0L && mOcr != null)
		{
			mOcr.setProgressFunc(m_pEngine, flag);
		}
	}

	public void setStop(boolean flag) 
	{
		//OPT_STOP = flag;
	}

	public boolean startBCR(String s, String s1, int i) 
	{
		boolean flag = false;
		if (mOcr.startBCR(m_ppEngine, StringUtil.convertUnicodeToAscii(s1),
				StringUtil.convertUnicodeToAscii(s), i) == 1) {
			m_pEngine = m_ppEngine[0];
			flag = true;
		}
		return flag;
	}
}
