package com.ym.idcard.reg;

import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.EventUtil;

@SuppressLint("HandlerLeak")
public class CameraActitivy extends BaseActivity implements
		android.view.SurfaceHolder.Callback, View.OnClickListener 
{
	public static final int 	RESULT_CODE_TAKE_SUC = 0;
	private boolean 			cameraMode;
	private String 				flashMode = "auto";
	private ArrayList<String> 	imagesName = new ArrayList<String>();
	private boolean 			isSupportFocusArea;
	private TextView 			mIndicator;
	private Button 				mDone;
	private ImageButton 		mFlash;
	private ImageButton 		mShutter;
	private SurfaceHolder 		mSurfaceHolder;
	private SurfaceView 		mSurfaceView;
	private CameraManager 		mCameraManager;

	private Handler 			mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			case CameraManager.MSG_PIC_TAKEN_SUC:
				if (cameraMode) 
				{
					imagesName.add((String) message.obj);
					mCameraManager.initDisplay();
				}
				else 
				{
					Intent data = new Intent();
					data.putExtras(message.getData());
					setResult(RESULT_CODE_TAKE_SUC, data);
					finish();
				}
				break;
			case CameraManager.MSG_PIC_TAKEN_FAIL:
				App.showToast(R.string.camera_take_picture_error);
				mCameraManager.initDisplay();
				break;
			case CameraManager.MSG_PIC_TAKEN_FAIL2:
				mCameraManager.initDisplay();
				break;
			default:
				break;
			}
			mShutter.setEnabled(true);
		}
	};

	private View.OnTouchListener mLsnOnTouch = new android.view.View.OnTouchListener()
	{

		public boolean onTouch(View view, MotionEvent event) 
		{
			if (!EventUtil.isDoubleClick() && isSupportFocusArea && event.getAction() == 0)
			{
				try
				{
					mCameraManager.requestFocusArea(event, mSurfaceView);
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
			return false;
		}
	};

	@SuppressWarnings("deprecation")
	private void initViews() 
	{
		mIndicator = (TextView) findViewById(R.id.tv_camera_indicator);
		mShutter = (ImageButton) findViewById(R.id.btn_camera_shutter);
		mFlash = (ImageButton) findViewById(R.id.btn_camera_flash);
		mDone = (Button) findViewById(R.id.btn_back);
		mShutter.setOnClickListener(this);
		mFlash.setOnClickListener(this);
		mDone.setOnClickListener(this);
		if (!DeviceUtils.checkExternalMemoryAvailable()) 
		{
			mIndicator.setText(R.string.camera_sdcard_error);
		}
		mSurfaceView = (SurfaceView) findViewById(R.id.camera_preview);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mSurfaceView.setOnTouchListener(mLsnOnTouch);
	}

	private void safeRelease() 
	{
		if (imagesName != null) 
		{
			imagesName.clear();
		}
	}

	private void setFlashMode(String model) 
	{
		if (model.equals(Camera.Parameters.FLASH_MODE_AUTO)) 
		{
			mFlash.setImageResource(R.drawable.btn_flash_auto);
		} 
		else if (model.equals(Camera.Parameters.FLASH_MODE_ON))
		{
			mFlash.setImageResource(R.drawable.btn_flash_on);
		} 
		else 
		{
			mFlash.setImageResource(R.drawable.btn_flash_off);
		}
		flashMode = model;
		mCameraManager.setCameraFlashMode(flashMode);
		PreferencesBCR.saveFlashMode(this, flashMode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if (resultCode == 104) 
		{
			if ("tf302".equals(Build.DEVICE))
			{
				mCameraManager.closeCamera();
				try 
				{
					mCameraManager.openCamera(mSurfaceHolder);
				} 
				catch (RuntimeException e)
				{
					e.printStackTrace();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			mCameraManager.initDisplay();
		}
	}

	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		setContentView(R.layout.activity_camera);
		mCameraManager = new CameraManager(this, mHandler);
		flashMode = PreferencesBCR.getFlashMode(this);
		initViews();
	}

	@Override
	protected void onDestroy()
	{
		safeRelease();
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int i, KeyEvent keyevent)
	{
		boolean flag = true;
		switch (i)
		{
		default:
			flag = super.onKeyDown(i, keyevent);
			// fall through
		case 23: // '\027'
		case 27: // '\033'
		case 80: // 'P'
			return flag;
		}
	}

	@Override
	protected void onResume() 
	{
		imagesName.clear();
		super.onResume();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		try 
		{
			mCameraManager.openCamera(holder);
			if (flashMode == null || !mCameraManager.isSupportFlash(flashMode))
				flashMode = mCameraManager.getDefaultFlashMode();
			if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) 
			{
				mFlash.setImageResource(R.drawable.btn_flash_on);
			} 
			else if (Camera.Parameters.FLASH_MODE_OFF.equals(flashMode)) 
			{
				mFlash.setImageResource(R.drawable.btn_flash_off);
			} 
			else if (Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode))
			{
				mFlash.setImageResource(R.drawable.btn_flash_auto);
			}
			mCameraManager.setCameraFlashMode(flashMode);
			isSupportFocusArea = mCameraManager.isSupportFocusArea();
		}
		catch (Exception e) 
		{
			App.showToast(R.string.camera_open_error);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		if (width > height) 
		{
			mCameraManager.setPreviewSize(width, height);
		} 
		else 
		{
			mCameraManager.setPreviewSize(height, width);
		}
		mCameraManager.initDisplay();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		mCameraManager.closeCamera();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.btn_back:
//			App.showAlert(this, R.string.dialog_logout_message,
//					R.string.logout,
//					new DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//					new LogoutTask().execute();
//					ActivityHelper.goLogin(CameraActitivy.this);
//					App.setAccessToken(null);
//					finish();
//				}
//			},R.string.cancel,null);
			finish();
			break;
		case R.id.btn_camera_shutter:
			if (!EventUtil.isDoubleClick())
			{
				if (imagesName.size() == 10)
				{
					App.showToast(R.string.camera_images_limit);
				} 
				else if (DeviceUtils.checkExternalMemoryAvailable())
				{
					mShutter.setEnabled(false);
					mCameraManager.requestFocuse();
				} 
				else
				{
					App.showToast(R.string.camera_sdcard_error);
				}
			}
			break;
		case R.id.btn_camera_flash:
			String model = mCameraManager.getNextFlashMode(flashMode);
			if (model == null || flashMode.equals(model))
			{
				App.showToast(R.string.camera_support_flash_error);
			}
			else 
			{
				setFlashMode(model);
			}
			break;
		default:
			break;
		}
	}
}
