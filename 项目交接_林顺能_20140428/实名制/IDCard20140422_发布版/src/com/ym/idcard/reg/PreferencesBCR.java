package com.ym.idcard.reg;

import android.content.Context;

public class PreferencesBCR 
{
	public static final String BCR_CFG 									= "config";
	public static final String BCR_PIN 									= "checkSN";
	public static final String BIZCARD_EDIT_SAVE_PROMPT_SHARE_MYCARD 	= "save_prompt_share_mycard";
	public static final String CAMERA_FLASH_MODE 						= "camera_flash_mode";
	public static final String FIRST_BOOT 								= "first";
	public static final String MAIN_SORT_TYPE 							= "bcr_main_sort_type";
	public static final String OCR_SUCCESS 								= "bcr_ocr_success";
	public static final String PIN 										= "SN";
	public static final String PRRV 									= "prrv";
	public static final String RECOG_DONE 								= "recog_done";
	public static final String START_SUCCESS 							= "bcr_start_success";

	public PreferencesBCR() 
	{
		
	}

	public static int getCardType(Context context) {
		return context.getSharedPreferences("config", 0).getInt("card_type", 5);
	}

	public static String getFlashMode(Context context) {
		return context.getSharedPreferences("config", 0).getString(
				"camera_flash_mode", null);
	}

	public static boolean getOcrSuccess(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean(
				"bcr_ocr_success", false);
	}

	public static String getPIN(Context context) {
		String s = context.getSharedPreferences("checkSN", 0).getString("SN",
				"nothing");
		if ("".equals(s))
			s = "nothing";
		return s;
	}

	public static int getSortType(Context context) {
		return context.getSharedPreferences("config", 0).getInt(
				"bcr_main_sort_type", 4);
	}

	public static boolean getStartSuccess(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean(
				"bcr_start_success", false);
	}

	public static int getUiLanguage(Context context) {
		return context.getSharedPreferences("config", 0).getInt("ui_language",
				0);
	}

	public static boolean isFeedBack(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean("prrv",
				false);
	}

	public static boolean isFirstBoot(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean("first",
				false);
	}

	public static boolean isPIN(Context context) {
		boolean flag;
		if ("nothing".equals(getPIN(context)))
			flag = false;
		else
			flag = true;
		return flag;
	}

	public static boolean isPromptShareMycard(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean(
				"save_prompt_share_mycard", false);
	}

	public static boolean isRecogDone(Context context) {
		return context.getSharedPreferences("config", 0).getBoolean(
				"recog_done", false);
	}

	public static void saveCardType(Context context, int i) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putInt("card_type", i);
		editor.commit();
	}

	public static void saveFlashMode(Context context, String s) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putString("camera_flash_mode", s);
		editor.commit();
	}

	public static void saveOcrSuccess(Context context) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("bcr_ocr_success", true);
		editor.commit();
	}

	public static void saveSortType(Context context, int i) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putInt("bcr_main_sort_type", i);
		editor.commit();
	}

	public static void saveStartSuccess(Context context) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("bcr_start_success", true);
		editor.commit();
	}

	public static void saveUiLanguage(Context context, int i) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putInt("ui_language", i);
		editor.commit();
	}

	public static void setFeedBack(Context context, boolean flag) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("prrv", flag);
		editor.commit();
	}

	public static void setFirstBoot(Context context, boolean flag) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("first", flag);
		editor.commit();
	}

	public static void setPIN(Context context, String s) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("checkSN", 0).edit();
		editor.putString("SN", s);
		editor.commit();
	}

	public static void setPromptShareMycard(Context context, boolean flag) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("save_prompt_share_mycard", flag);
		editor.commit();
	}

	public static void setRecogDone(Context context, boolean flag) {
		android.content.SharedPreferences.Editor editor = context
				.getSharedPreferences("config", 0).edit();
		editor.putBoolean("recog_done", flag);
		editor.commit();
	}
}
