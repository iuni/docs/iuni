package com.chinaunicom.custinforegist.kit.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import org.apache.http.impl.cookie.DateUtils;

import android.os.Environment;

public class DebugUtils {

	public static void saveExceptionLog(Throwable ex){
		
		File parent = new File(Environment.getExternalStorageDirectory()+"/IdCard/log/");
		if(!parent.exists()){
			parent.mkdirs();
		}
		
		final StackTraceElement[] stack = ex.getStackTrace();
		final String message = ex.getMessage();
		String fileName = "crash-" + DateUtils.formatDate(new Date(), "yyyy-MM-dd-HH-mm-ss")+ ".log";
		File file = new File(Environment.getExternalStorageDirectory()+"/IdCard/log/", fileName);
		try {
			//file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			if(message!=null)
				fos.write(message.getBytes());
			for (int i = 0; i < stack.length; i++) {
				fos.write(stack[i].toString().getBytes());
			}
			fos.flush();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
