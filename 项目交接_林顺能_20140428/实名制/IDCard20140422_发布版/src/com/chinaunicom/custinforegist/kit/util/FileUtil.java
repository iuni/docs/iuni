package com.chinaunicom.custinforegist.kit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

public class FileUtil {

	public static String newImageName() {
		return String.valueOf(UUID.randomUUID()).replaceAll("-", "") + ".j";
	}
	
	public static String newImageScaleName() {
		return String.valueOf(UUID.randomUUID()+"-scale").replaceAll("-", "") + ".j";
	}
	
	
	public static void forceDelete(File file) throws IOException {
		if (file.isDirectory()) {
			deleteDirectory(file);
		} else {
			if(file.exists()){
				if (!file.delete())
					throw new IOException("Unable to delete file: "+file);
			} else {
				throw new FileNotFoundException("File does not exist: "+file);
			}
		}
	}
	
	public static void cleanDirectory(File file) throws IOException {
		if (!file.exists())
			throw new IllegalArgumentException(file + " does not exist");
		if (!file.isDirectory())
			throw new IllegalArgumentException(file + " is not a directory");
		File[] contentFiles = file.listFiles();
		if (contentFiles == null)
			throw new IOException("Failed to list contents of " + file);
		int i = 0;
		while (i < contentFiles.length) {
			forceDelete(contentFiles[i]);
			i++;
		}
	}
	
	public static void deleteDirectory(File file) throws IOException {
		if (file.exists()) {
			cleanDirectory(file);
			if (!file.delete())
				throw new IOException("Unable to delete directory " + file
						+ ".");
		}
	}
	
	public static boolean deleteFile(String s) {
		File file = new File(s);
		boolean flag;
		if (file.exists())
			flag = file.delete();
		else
			flag = true;
		return flag;
	}

	public static byte[] getBytesFromFile(File file) throws IOException {
		FileInputStream fileinputstream;
		fileinputstream = new FileInputStream(file);
		long l = file.length();
		if (l > 0x7fffffffL) {
			fileinputstream.close();
			throw new IOException((new StringBuilder("File is to large "))
					.append(file.getName()).toString());
		}
		byte[] abyte0 = new byte[(int) l];
		int i = 0;
		while (i < abyte0.length) {
			int j = fileinputstream.read(abyte0, i, abyte0.length - i);
			if (j >= 0) {
				i += j;
			}
		}
		if (i < abyte0.length)
			throw new IOException((new StringBuilder(
					"Could not completely read file ")).append(file.getName())
					.toString());
		fileinputstream.close();
		return abyte0;
	}

	public static byte[] getBytesFromFile(String s) throws IOException {
		return getBytesFromFile(new File(s));
	}
}
