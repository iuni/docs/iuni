package com.chinaunicom.custinforegist.kit.util;

public class CheckUtils 
{
	public static boolean isCorrentPassword(String pwd) 
	{
		return pwd.matches("\\d{6}");
	}
	
   	// SQL防注入
	private static boolean hasSQLKey(String valstr, boolean checkSymbol) 
	{
		// 检测特殊字符
		// 暂时考虑反斜杠
		if(checkSymbol)
		{
			if(valstr.indexOf("\\") != -1)
			{
				return true;
			}
		}
		
		// 检测SQL关键字
		if(valstr.indexOf("select") != -1
				|| valstr.indexOf("insert") != -1
				|| valstr.indexOf("update") != -1
				|| valstr.indexOf("delete") != -1
				/*
				|| valstr.indexOf("and") != -1
				|| valstr.indexOf("or") != -1
				|| valstr.indexOf("join") != -1
				|| valstr.indexOf("union") != -1
				|| valstr.indexOf("truncate") != -1
				*/
				|| valstr.indexOf("drop") 	!= -1
				|| valstr.indexOf("SELECT") != -1
				|| valstr.indexOf("INSERT") != -1
				|| valstr.indexOf("UPDATE") != -1
				|| valstr.indexOf("DELETE") != -1
				/*
				|| valstr.indexOf("and") != -1
				|| valstr.indexOf("or") != -1
				|| valstr.indexOf("join") != -1
				|| valstr.indexOf("union") != -1
				|| valstr.indexOf("truncate") != -1
				*/
				|| valstr.indexOf("DROP") != -1)
		{
			return true;
		}
		return false;
	}
	
	// 判断姓名是否合法
	public static boolean isNameValid(String type, String val)
	{
		if (!"07".equals(type)) 
		{
			return (val.matches("[\u4E00-\u9FA5.·()（）]+") && getChineseCount(val) >= 2);
		}
		else 
		{
			try 
			{
				// 判断是否有SQL字符
				if(hasSQLKey(val, true))
				{
					return false;
				}
				return val.getBytes("GBK").length >= 3 && !val.matches("\\d*");
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}
	}
	
	/*
	// 检测时间格式
	private boolean checkIDEndTime(String idno) 
	{
		try {
			// 提取年、月、日
			int year  = Integer.parseInt(idno.substring(6, 10));
			int month = Integer.parseInt(idno.substring(10, 12));
			int day   = Integer.parseInt(idno.substring(12, 14));

			// 检测年
			if(year < 1900 || year > 3000) 
			{
				return false;
			}
			// 检测月
			if(month < 1 || month > 12) 
			{
				return false;
			}
			// 检测日
			if(day < 1 || day > 31) 
			{
				return false;
			}
			// 小月
			if((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) 
			{
				return false;
			}
			// 2月
			if(month == 2) 
			{
				if((year%4 == 0 && year%100 != 0) || year%400 == 0) 
				{
					if(day > 29) 
					{
						return false;
					}
				}
				else 
				{
					if(day > 28) 
					{
						return false;
					}
				}
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	*/

	// 判断证件号码是否合法
	public static boolean isNoValid(String type, String val) 
	{
		if ("01".equals(type) || "02".equals(type)) 
		{
			/*
			if(val.length() < 18)
			{
				return false;
			}
			if(checkIDEndTime(val) == false)
			{
				return false;
			}
			return true;
			*/
			//return val.matches("[0-9]{17}[0-9Xx]{1}");
			return check18IdentifyID(val);
		} 
		else if ("05".equals(type)) 
		{
			return val.matches("^[H,M](\\d{8}|\\d{10})");
		} 
		else if ("06".equals(type)) 
		{
			/*
			if(val.length() == 11)
			{
				char c = val.charAt(10);
				System.out.println("c :" + c);
				if(c < 'A' ||(c >'Z'&& c <'a'|| c > 'z'))
				{
					return false;
				}
			}
			*/
			return val.matches("(\\d{10}[(][0-9a-zA-Z][)])|(\\d{10}[（][0-9a-zA-Z][）])|(\\d{8})");
			//return val.matches("(\\d{10}[0-9a-zA-Z])|(\\d{8})|");
		} 
		else if ("03".equals(type) || "04".equals(type))
		{
			return val.matches("[0-9a-zA-Z]{6,}");
		}
		else if("07".equals(type))
		{
			// 判断是否有SQL字符
			if(hasSQLKey(val, true))
			{
				return false;
			}
			return val.matches("[0-9a-zA-Z]{6,}");
		}
		return false;
	}
	
	/*
	private boolean isPattern(String str, String pattern)
	{
		// /^[-|+]?\\d*([.]\\d{0,2})?$
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.find();
	}
	*/

	// 检测身份证合法性
	private static boolean check18IdentifyID (String identifyID) 
	{
	    long lSumQT 	= 0;
	    int[] R 		= {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 }; // 加权因子 
	    char[] sChecker = new char[]{'1','0','X', '9', '8', '7', '6', '5', '4', '3', '2'};
	    
	    // 检验长度 
	    if(18 != identifyID.length())
	    {
	    	System.out.println("长度不符合....");
	    	return false;  
	    }
	    
	    // 校验数字 
	    for (int i=0; i<18; i++)
	    {  
	    	char c = identifyID.charAt(i);
	        if ((c < 48 ||c > 57) && !(('X' == c || 'x' == c) && 17 == i))
	        {  
	        	System.out.println("存在非法字母...");
	            return false;  
	        }  
	    }  
	    // 验证最末的校验码
	    for (int i=0; i<=16; i++)
	    {      
	        lSumQT += (identifyID.charAt(i)-48) * R[i];  
	    }  
	    
	    if (sChecker[(int) (lSumQT%11)] != identifyID.charAt(17))  
	    {  
	    	System.out.println("校验码不符合...");
	        return false;  
	    }
	    return true;          
	}

	// 判断地址是否合法
	public static int isAddressValid(String type, String val) 
	{
		PLog.i("t:" + type + " val:" + val);
		// 防SQL注入
		if(hasSQLKey(val, true))
		{
			return 1;
		}
		// 
		if ("01".equals(type) || "02".equals(type)) 
		{
			if(getChineseCount(val) < 8)
			{
				return 2;
			}
			return 0;
		} 
		else if ("03".equals(type) || "04".equals(type) || "05".equals(type)) 
		{
			if(getChineseCount(val) < 8)
			{
				return 2;
			}
			return 0;
		}
		else if ("06".equals(type)) 
		{
			if(getChineseCount(val) < 3)
			{
				return 2;
			}
			return 0;
		}
		else if ("07".equals(type)) 
		{
			if(getChineseCount(val) < 2)
			{
				return 2;
			}
			return 0;
		}
		return 2;
	}

	// 判断输入是否为汉字
	public static boolean isChinese(char a)
	{
		int v = (int) a;
		return (v >= 19968 && v <= 171941);
	}

	public static int getChineseCount(String s) 
	{
		int count = 0;
		if (null == s || "".equals(s.trim()))
			return count;
		String[] cha = s.split("");
		for (String str : cha)
		{
			if (str.matches("[\u4E00-\u9FA5]"))
			{
				count++;
			}
		}
		return count;
	}
	
	// 判断联系人号码是否合规
	public static boolean isContactTelValid(String contactTel) 
	{
		if(contactTel.matches("[0-9]{6,}") == false)
		{
			return false;
		}
		
		try
		{
			long nValue = Long.parseLong(contactTel);
			int nLen = contactTel.length();
			// 6位号码
			if(nLen == 6) 
			{
				if(nValue == 0 || nValue == 111111 || nValue == 222222 || nValue == 333333 
						|| nValue == 444444 || nValue == 555555 || nValue == 666666 
						|| nValue == 777777 || nValue == 888888 || nValue == 999999 
						|| nValue == 12345 || nValue == 123456 || nValue == 234567 || nValue == 345678 || nValue == 456789 
						|| nValue == 54321 || nValue == 654321 || nValue == 765432 || nValue == 876543 || nValue == 987654) 
				{
					return false;
				}
				return true;
			}
			// 7位号码
			else if(nLen == 7) 
			{
				if(nValue == 0 || nValue == 1111111 || nValue == 2222222 || nValue == 3333333 
						|| nValue == 4444444 || nValue == 5555555 || nValue == 6666666 
						|| nValue == 7777777 || nValue == 8888888 || nValue == 9999999 
						|| nValue == 123456 || nValue == 1234567 || nValue == 2345678 || nValue == 3456789 
						|| nValue == 654321 || nValue == 7654321 || nValue == 8765432 || nValue == 9876543) 
				{
					return false;
				}
				return true;
			}
			// 8位号码
			else if(nLen == 8) 
			{
				if(nValue == 0 || nValue == 11111111 || nValue == 22222222 || nValue == 33333333 
						|| nValue == 44444444 || nValue == 55555555 || nValue == 66666666 
						|| nValue == 77777777 || nValue == 88888888 || nValue == 99999999 
						|| nValue == 1234567 || nValue == 12345678 || nValue == 23456789 
						|| nValue == 7654321 || nValue == 87654321 || nValue == 98765432) 
				{
					return false;
				}
				return true;
			}
			// 9位号码
			else if(nLen == 9) 
			{
				if(nValue == 0 || nValue == 111111111 || nValue == 22222222 || nValue == 333333333 
						|| nValue == 444444444 || nValue == 555555555 || nValue == 666666666 
						|| nValue == 777777777 || nValue == 888888888 || nValue == 999999999 
						|| nValue == 123456789 
						|| nValue == 987654321) 
				{
					return false;
				}
				return true;
			}
			// 10位号码
			else if(nLen == 10) 
			{
				if("0000000000".equals(contactTel) || "1111111111".equals(contactTel) 
						|| "2222222222".equals(contactTel) || "3333333333".equals(contactTel) 
						|| "4444444444".equals(contactTel) || "5555555555".equals(contactTel)
						|| "6666666666".equals(contactTel) || "7777777777".equals(contactTel)
						|| "8888888888".equals(contactTel) || "9999999999".equals(contactTel)
						|| "0123456789".equals(contactTel) || "9876543210".equals(contactTel)) 
				{
					return false;
				}
				return true;
			}
			// 11位号码
			else if(nLen == 11) 
			{
				if("00000000000".equals(contactTel) || "11111111111".equals(contactTel) 
						|| "22222222222".equals(contactTel) || "33333333333".equals(contactTel) 
						|| "44444444444".equals(contactTel) || "55555555555".equals(contactTel)
						|| "66666666666".equals(contactTel) || "77777777777".equals(contactTel)
						|| "88888888888".equals(contactTel) || "99999999999".equals(contactTel)) 
				{
					return false;
				}
				return true;
			}
			// 12位号码
			else if(nLen == 12) 
			{
				if("000000000000".equals(contactTel) || "111111111111".equals(contactTel) 
						|| "222222222222".equals(contactTel) || "333333333333".equals(contactTel) 
						|| "444444444444".equals(contactTel) || "555555555555".equals(contactTel)
						|| "666666666666".equals(contactTel) || "777777777777".equals(contactTel)
						|| "888888888888".equals(contactTel) || "999999999999".equals(contactTel)) 
				{
					return false;
				}
				return true;
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// 判断联系人号码是否合规
	public static boolean isContactAddrValid(String contactAddr) 
	{
		// 防止SQL关键字录入
		if(hasSQLKey(contactAddr, true)) 
		{
			return false;
		}
		if(getChineseCount(contactAddr) < 8)
		{
			return false;
		}
		return true;
	}
}
