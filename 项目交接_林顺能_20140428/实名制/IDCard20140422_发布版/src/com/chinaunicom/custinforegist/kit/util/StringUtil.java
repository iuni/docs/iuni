package com.chinaunicom.custinforegist.kit.util;

// 字符串操作
public class StringUtil 
{
	// 判断字符串是否为空
	public static boolean isEmptyOrNull(String str)
	{
		return str == null || str.trim().length() <= 0;
	}
	
	// 根据长度截取字符串
	public static String getStrByLen(String srcStr, int nLen) 
	{
		int nStrLen = srcStr.length();
		if(nStrLen > nLen) 
		{
			return srcStr.subSequence(0, nLen) + "...";
		}
		return srcStr;
	}
	
	public static String convertAscIIToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte[] data = filterAndCut(abyte0);
		if (data != null) 
		{
			try 
			{
				s = new String(data, "ISO-8859-1");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertAscIIToUnicodeJP(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "Shift_JIS");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertAscIIToUnicodeRS(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "Windows-1251");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertBig5ToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "big5");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertGbkToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "GBK");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}// goto _L2; else goto _L1
		return s.trim();
	}

	public static byte[] convertToUnicode(String s) 
	{
		try 
		{
			byte[] abyte1 = s.getBytes("utf-8");
			final int i = abyte1.length;
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] convertUnicodeToAscii(String s) 
	{
		try 
		{
			final int i = s.length();
			byte[] abyte1 = s.getBytes("US-ASCII");
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] convertUnicodeToGbk(String s) 
	{
		try 
		{
			final int i = s.length();
			byte[] abyte1 = s.getBytes("GBK");
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] filter(byte abyte0[]) 
	{
		final int i = abyte0.length;
		byte abyte1[] = new byte[i];
		int k = 0;
		for (int j = 0; j < i; j++) 
		{
			int l;
			if (abyte0[j] == 13) 
			{
				l = k;
			} 
			else 
			{
				l = k + 1;
				abyte1[k] = abyte0[j];
			}
			k = l;
		}
		return abyte1;
	}

	public static byte[] filterAndCut(byte abyte0[]) 
	{
		int i = strlen(abyte0);
		byte abyte1[];
		if (i < 1) 
		{
			abyte1 = null;
		} 
		else 
		{
			abyte1 = new byte[i];
			int j = 0;
			int k = 0;
			while (j < i) 
			{
				int l;
				if (abyte0[j] == 13) 
				{
					l = k;
				} 
				else 
				{
					l = k + 1;
					abyte1[k] = abyte0[j];
				}
				j++;
				k = l;
			}
		}
		return abyte1;
	}

	public static boolean isEngString(String s) 
	{
		String s1 = s.replaceAll(" ", "");
		for (int i = 0; i < s1.length(); i++) 
		{
			if ((s1.charAt(i) < 'A' || s1.charAt(i) > 'Z') && (s1.charAt(i) < 'a' || s1.charAt(i) > 'z')) 
			{
				
			} 
			else 
			{
				return true;
			}
		}
		return false;
	}

	public static int strlen(byte abyte0[]) 
	{
		int i = -1;
		if (abyte0 != null) 
		{
			if (abyte0.length != 0) 
			{
				for (int j = 0; j < abyte0.length; j++) 
				{
					if (abyte0[j] == 0) 
					{
						i = j;
						break;
					}
				}
			} 
			else 
			{
				i = 0;
			}
		}
		return i;
	}

	public void finalize() 
	{
		
	}
}
