package com.chinaunicom.custinforegist.kit.image;

import java.io.ByteArrayOutputStream;
import java.lang.ref.SoftReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.chinaunicom.custinforegist.kit.util.ImageUtil;

public class RecoImageView extends ImageView 
{
	//private static final String 		TAG = RecoImageView.class.getSimpleName();
	//private int 						angle;
	private SoftReference<Bitmap> 		bitmapTemp;
	private SoftReference<Bitmap> 		bitmapView;
	//private Rect 						corpRect;
	//private int 						height;
	private int 						heightOrg;
	private String 						imagePath;
	//private int 						width;
	private int 						widthOrg;

	public RecoImageView(Context context) 
	{
		super(context);
	}

	public RecoImageView(Context context, AttributeSet attributeset) 
	{
		super(context, attributeset);
	}

	public RecoImageView(Context context, AttributeSet attributeset, int i) 
	{
		super(context, attributeset, i);
	}

	protected void finalize() throws Throwable 
	{
		releaseRes();
		super.finalize();
	}

	public byte[] getIconByte() 
	{
		byte abyte0[]= null;
		try {

			if (widthOrg == 0 || heightOrg == 0) {
				android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(imagePath, options);
				widthOrg = options.outWidth;
				heightOrg = options.outHeight;
			}
			android.graphics.BitmapFactory.Options options1 = new android.graphics.BitmapFactory.Options();
			options1.outWidth = widthOrg;
			options1.outHeight = heightOrg;
			int i = ImageUtil.computeSampleSize(options1, 60, 4800);
			int j = ImageUtil.computeSampleSize(options1, 80, 4800);
			ByteArrayOutputStream bytearrayoutputstream;
			byte abyte1[];
			if (i <= j)
				i = j;
			options1.inSampleSize = i;
			options1.inJustDecodeBounds = false;
			bitmapTemp = new SoftReference<Bitmap>(BitmapFactory.decodeFile(imagePath, options1));
			bytearrayoutputstream = new ByteArrayOutputStream();
			((Bitmap) bitmapTemp.get()).compress(
					android.graphics.Bitmap.CompressFormat.JPEG, 100,
					bytearrayoutputstream);
			bytearrayoutputstream.close();
			abyte1 = bytearrayoutputstream.toByteArray();
			((Bitmap) bitmapTemp.get()).recycle();
			bitmapTemp = null;
			abyte0 = abyte1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// _L2:
		return abyte0;
		// IOException ioexception;
		// ioexception;
		// Debug.e("", ioexception);
		// ((Bitmap)bitmapTemp.get()).recycle();
		// bitmapTemp = null;
		// abyte0 = null;
		// if(true) goto _L2; else goto _L1
		// _L1:
		// Exception exception;
		// exception;
		// ((Bitmap)bitmapTemp.get()).recycle();
		// bitmapTemp = null;
		// throw exception;
	}

	public void releaseRes() 
	{
		if (bitmapView != null && !((Bitmap) bitmapView.get()).isRecycled()) 
		{
			((Bitmap) bitmapView.get()).recycle();
			bitmapView = null;
		}
	}

	public void setImageAngleCrop(int i, Rect rect) 
	{
		//angle = i;
		//corpRect = rect;
	}

	public void setImagePath(String s, int i, int j) 
	{
		releaseRes();
		imagePath = s;
		//width = i;
		//height = j;
	}
}
