package com.chinaunicom.custinforegist.service;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.chinaunicom.custinforegist.kit.util.HttpUtils;
import com.chinaunicom.custinforegist.kit.util.HttpUtils.OnDownloadListener;

//import com.simico.officeonline.kit.log.TLog;
//import com.simico.officeonline.kit.util.HttpUtils;
//import com.simico.officeonline.kit.util.HttpUtils.OnDownloadListener;

@SuppressLint("HandlerLeak")
public class DownloadService extends Service 
{
	private static final String 		TAG = DownloadService.class.getSimpleName();
	private static final int 			MAX_DOWNLOAD_NUM = 3;
	private static final int 			IDLE_DELAY = 60*1000; 			//60s
	
	public static final String 			ACTION_DOWNLOAD = "com.simico.officeonline.action.download";
	
	public static final String 			DOWNLOAD_URL = "url";
	public static final String 			DOWNLOAD_PATH = "path";
	
	private final IBinder 				binder = new DownloadServiceBinder();

	private Map<String, DownloadThread> runningThread = new HashMap<String, DownloadThread>();
	private Map<String, DownloadThread> waitingThread = new HashMap<String, DownloadThread>();
	private int 						mServiceStartId = -1;
	private boolean 					mServiceInUse = false;
	
	public class DownloadServiceBinder extends Binder 
	{
		public DownloadService getService() 
		{
			return DownloadService.this;
		}
	}
	
	private Handler mDelayedStopHandler = new Handler() 
	{
        @Override
        public void handleMessage(Message msg) 
        {
        	Log.d(TAG, "延迟销毁时间已到准备销毁下载服务");
            // Check again to make sure nothing is downloading right now
            if (mServiceInUse || waitingThread.size() > 0 || runningThread.size() > 0) 
            {
            	Log.d(TAG, "还有下载的任务不进行销毁");
            	return;
            }
            stopSelf(mServiceStartId);
        }
    };
    
    @Override
	public void onCreate() 
	{
		super.onCreate();
		//Log.d(TAG, "下载服务被创建:onCreate");
		
	    // If the service was idle, but got killed before it stopped itself, the
        // system will relaunch it. Make sure it gets stopped again in that case.
        Message msg = mDelayedStopHandler.obtainMessage();
        mDelayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
	}
    
    @Override
	public int onStartCommand(Intent intent, int flags, int startId) 
    {
    	//Log.d(TAG, "下载服务开始:onStartCommand");
		mServiceStartId = startId;
		mDelayedStopHandler.removeCallbacksAndMessages(null);
		
		if (intent != null) 
		{
            String action = intent.getAction();
            if(ACTION_DOWNLOAD.equals(action)) 
            {
            	// do samething
            	
            }
		}
		
		// make sure the service will shut down on its own if it was
        // just started but not bound to and nothing is playing
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        Message msg = mDelayedStopHandler.obtainMessage();
        mDelayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
        
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) 
	{
		//Log.d(TAG, "绑定下载服务");
		mDelayedStopHandler.removeCallbacksAndMessages(null);
		mServiceInUse = true;
		return binder;
	}
	
	@Override
	public void onRebind(Intent intent) 
	{
		//Log.d(TAG, "重新绑定下载服务");
		mDelayedStopHandler.removeCallbacksAndMessages(null);
		mServiceInUse = true;
	}
	
	@Override
	public boolean onUnbind(Intent intent) 
	{
		//Log.d(TAG, "解绑下载服务");
	    mServiceInUse = false;
	    
	    if (waitingThread.size() > 0  || runningThread.size() > 0) 
	    {
	    	//Log.d(TAG, "还有下载的任务延迟销毁下载服务");
            Message msg = mDelayedStopHandler.obtainMessage();
            mDelayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
            return true;
        }
	    // No active download thread, OK to stop the service right now
	    stopSelf(mServiceStartId);
        return true;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		//Log.d(TAG, "下载服务销毁:onDestroy");
		
		// make sure there aren't any other messages coming
        mDelayedStopHandler.removeCallbacksAndMessages(null);
	}
	
	public boolean hasTask(String urlKey) 
	{
		if(!TextUtils.isEmpty(urlKey)) 
		{
			DownloadThread t = waitingThread.get(urlKey);
			DownloadThread w = runningThread.get(urlKey);
			if(t == null && w == null) 
			{
				return false;
			}
			return true;
		}
		return false;
	}
	
	public boolean addTask(DownloadTask task) 
	{
		if(!TextUtils.isEmpty(task.getUrl())) 
		{
			synchronized (waitingThread) 
			{
				DownloadThread t = waitingThread.get(task.getUrl());
				DownloadThread w = runningThread.get(task.getUrl());
				if(t == null && w == null) 
				{
					waitingThread.put(task.getUrl(), new DownloadThread(task, new WeakReference<Context>(this)));
					//Log.d(TAG, "新任务已添加至等待队列:"+task.getUrl());
					// start waiting thread
					startDownload();
					return true;
				} 
				else if(t != null) 
				{
					//Log.d(TAG, "该任务正在等待下载...");
				} 
				else if(w != null) 
				{
					//Log.d(TAG, "该任务正在下载...");
				}
			}
		}
		return false;
	}
	
	private DownloadThread getWaitingTask() 
	{
		synchronized (waitingThread) 
		{
			Iterator<String> keys = waitingThread.keySet().iterator();
			if(keys.hasNext()) 
			{
				DownloadThread thread =  waitingThread.remove(keys.next());
				//Log.d(TAG, "提取等待任务:"+ thread.getUrl());
				return thread;
			}
		}
		return null;
	}
	
	private void startDownload() 
	{
		if(runningThread.size() >=0 && runningThread.size() < MAX_DOWNLOAD_NUM) 
		{
			DownloadThread thread = getWaitingTask();
			if(thread == null) 
			{
				//Log.d(TAG, "没有等待任务");
				if(runningThread.size() == 0) 
				{
					//Log.d(TAG, "没有正在执行任务等待关闭下载服务");
					mDelayedStopHandler.removeCallbacksAndMessages(null);
			        Message msg = mDelayedStopHandler.obtainMessage();
			        mDelayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
				}
				return;
			}
			synchronized(runningThread) 
			{
				runningThread.put(thread.getUrl(), thread);
				Iterator<String> keys = runningThread.keySet().iterator();
				while(keys.hasNext()) 
				{
					DownloadThread th = runningThread.get(keys.next());
					if(!th.isRunning()) 
					{
						th.execute();
					}
				}
			}
		} 
		else 
		{
			//Log.d(TAG, "下载队列已满");
		}
	}
	
	private void removeThread(String key) 
	{
		synchronized(runningThread) 
		{
			//Log.d(TAG, "下载任务:"+key+"已完成尝试下载其他任务");
			runningThread.remove(key);
			// 尝试开始下载其他文件
			startDownload();
		}
	}
	
	class DownloadThread extends AsyncTask<Void, Void, Void> 
	{
		//private WeakReference<Context> 	weakContext;
		private boolean 					isCanceled;
		private boolean 					isRunning;
		private DownloadTask 				task;
		
		public boolean isRunning() 
		{
			return isRunning;
		}
		
		public String getUrl() 
		{
			return task.getUrl();
		}
		
		public DownloadThread(DownloadTask task, WeakReference<Context> weakContext) 
		{
			this.task = task;
			//this.weakContext = weakContext;
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			isRunning = true;
			File file  = new File(task.getLocalPath());
			File parent = file.getParentFile();
			if(!parent.exists()) 
			{
				parent.mkdirs();
			}
			
			/*
			HttpUtils.toFile(task.getUrl(), task.getLocalPath(), weakContext.get(), new OnDownloadListener() 
			{
				@Override
				public void onError(String url) 
				{
					Log.d("","下载失败:"+url);
					if(task.getOnDownloadListener() != null) 
					{
						task.getOnDownloadListener().onError(url);
					}
				}
				
				@Override
				public void onDownload(int readLen, long copyLen, long fileLen) 
				{
					Log.d("","正在下载:"+readLen +"/"+copyLen +" "+fileLen);
					if(task.getOnDownloadListener() != null) 
					{
						task.getOnDownloadListener().onDownload(readLen, copyLen, fileLen);
					}
				}
			});
			*/
			
			/*
			HttpUtils.toFile(task.getFtpIp(), task.getFtpPort(), 
				task.getFtpUser(), task.getFtpPwd(), 
				task.getFtpPath(), task.getLocalPath(), 
				new OnDownloadListener() 
				{
					@Override
					public void onError(String url) 
					{
						//Log.d("","下载失败:"+url);
						if(task.getOnDownloadListener() != null) 
						{
							task.getOnDownloadListener().onError(url);
						}
					}

					@Override
					public void onDownload(int readLen, long copyLen, long fileLen) 
					{
						//Log.d("","正在下载:"+readLen +"/"+copyLen +" "+fileLen);s
						if(task.getOnDownloadListener() != null) 
						{
							task.getOnDownloadListener().onDownload(readLen, copyLen, fileLen);
						}
					}
				}
			);
			*/
			
			HttpUtils.downLoadAPKWithBreakPoint(
					task.getUrl(), 
					task.getLocalPath(), 
					task.getFileBreakPoint(),
					new OnDownloadListener() 
					{
						@Override
						public void onError(String url) 
						{
							//Log.d("","下载失败:"+url);
							if(task.getOnDownloadListener() != null) 
							{
								task.getOnDownloadListener().onError(url);
							}
						}

						@Override
						public void onDownload(int readLen, long copyLen, long fileLen) 
						{
							//Log.d("","正在下载:"+readLen +"/"+copyLen +" "+fileLen);s
							if(task.getOnDownloadListener() != null) 
							{
								task.getOnDownloadListener().onDownload(readLen, copyLen, fileLen);
							}
						}
					}
				);
			return null;
		}
		
		@Override
		protected void onCancelled() 
		{
			isRunning = false;
			removeThread(task.getUrl());
			super.onCancelled();
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			isRunning = false;
			removeThread(task.getUrl());
			super.onPostExecute(result);
		}

		public boolean isCanceled() 
		{
			return isCanceled;
		}

		public void setCanceled(boolean isCanceled) 
		{
			this.isCanceled = isCanceled;
		}
	}
}
