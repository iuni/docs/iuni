package com.chinaunicom.custinforegist.service;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.api.model.Version;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.HeartbeatRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

// 心跳服务
public class HeartbeatService extends Service 
{
	private Timer 				mHeartbeatTimer;
	private static final int 	DELAY						= 10*60*1000;
	private HeartbeatRequest	mHeartbeatRequest			= null;
	private String[]			mStrLac						= new String[1];
	private String[]			mStrCi						= new String[1];
	
	private Timer				mTimer;
	
	// 获取心跳服务
	public class HeartbeatBinder extends Binder
	{
		public HeartbeatService getHeartbeatService() 
		{
			return HeartbeatService.this;
		}
	}
	
	@Override
	public void onCreate() 
	{
		super.onCreate();
		
		// 启动心跳定时器
		resetHeartbeatTimer();
	}
	
	@Override
	public IBinder onBind(Intent intent) 
	{
		return new HeartbeatBinder();
	}
	
	// 复位定时器
	public void resetHeartbeatTimer() 
	{
		synchronized(this) 
		{
			Log.v("", "复位心跳定时器...");
			// 取消定时器
			cancelHeartbeatTimer();
			// 启动定时器
			startHeartbeatTimer();
		}
	}
	
	// 取消定时器
	private void cancelHeartbeatTimer() 
	{
		if(mHeartbeatTimer != null) 
		{
			mHeartbeatTimer.cancel();
			mHeartbeatTimer = null;
		}
	}
	
	// 启动定时器
	private void startHeartbeatTimer() 
	{
		mHeartbeatTimer = new Timer();
		mHeartbeatTimer.schedule(new TimerTask() 
		{
			public void run() 
			{
				mHeartbeatTimer = null;
				Log.e("", "心跳时间到...");
				
				if(StringUtil.isEmptyOrNull(mStrLac[0]) || StringUtil.isEmptyOrNull(mStrCi[0])) 
				{
					DeviceUtils.getLacci(mStrLac, mStrCi);
				}
				
				mHeartbeatRequest = new HeartbeatRequest(App.getAgentId(), App.getAgentPwd(), "1", mStrLac[0], mStrCi[0]);
				App.getSpiceManager().execute(mHeartbeatRequest, new HeartbeatListener());
				// 启动超时定时器
				startTimer();
			}
		}, DELAY);
	}
	
	private final class HeartbeatListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消超时定时器
			cancelTimer();
			// 重启心跳定时器
			resetHeartbeatTimer();
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			// 取消超时定时器
			cancelTimer();
			// 重启心跳定时器
			resetHeartbeatTimer();
			
			try 
			{
				Map<String, String> result = mHeartbeatRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 保存登录状态
					App.setAccessToken("login-success");
					// 保存密钥
					String key = result.get(BaseRequest.REGISTER_KEY);
					App.setDesKey(key);
					
					// FTP服务器参数
					Interface.checkFtpInfo(result);
					// 检测升级
					Version version = Interface.checkUpdate(result, "");
					if(version != null) 
					{
						String strLog = "请您升级电子实名制客户端!";
						if (!StringUtil.isEmptyOrNull(version.getUpdateLog())) 
						{
							strLog = version.getUpdateLog();
						}
						Interface.sendUpdateNotify(App.context(), version.getUpdateTitle(), strLog, version);
					}
					// 输入状态
					Interface.checkInputType(result);
					// 菜单开关
					Interface.checkMenuType(result);
					// 证件类型开关
					Interface.checkCertType(result);
					// 检测公告
					Interface.checkMail(HeartbeatService.this, result, true);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	// 启动超时定时器
	private void startTimer() 
	{
		if(mTimer != null) 
		{
			mTimer.cancel();
			mTimer = null;
		}
		mTimer = new Timer();
		mTimer.schedule(new TimerTask() 
		{
			public void run() 
			{
				mTimer = null;
				Log.e("", "心跳超时...");
				
				// 心跳超时，重新启动超时定时器
				resetHeartbeatTimer();
			}
		}, BaseRequest.PROGRESS_TIMEOUT);
	}
	
	// 取消超时定时器
	private void cancelTimer() 
	{
		if(mTimer != null) 
		{
			mTimer.cancel();
			mTimer = null;
		}
	}
}
