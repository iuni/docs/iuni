package com.chinaunicom.custinforegist.service;

import com.chinaunicom.custinforegist.kit.util.HttpUtils.OnDownloadListener;

public class DownloadTask 
{
	private String 					url;
	private String 					localPath;			// 本地下载路径
	private int 					fileSize;
	private long					mBreakpoint;
	private int 					progress;
	private int 					state;
	private String 					name;
	private OnDownloadListener 		onDownloadListener;
	private String 					ftpIp;				// FTP服务器IP
	private int 					ftpPort;			// FTP服务器端口
	private String 					ftpPath;			// FTP服务器路径
	private String 					ftpUser;			// FTP服务器用户名
	private String 					ftpPwd;				// FTP服务器密码
	
	// 获取下载URL
	public String getUrl() 
	{
		return this.url;
	}

	// 设置URL
	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getFtpIp() 
	{
		return ftpIp;
	}

	public void setFtpIp(String ftpIp) 
	{
		this.ftpIp = ftpIp;
	}

	public int getFtpPort() 
	{
		return ftpPort;
	}

	public void setFtpPort(int ftpPort) 
	{
		this.ftpPort = ftpPort;
	}

	public String getFtpPath() 
	{
		return ftpPath;
	}

	public void setFtpPath(String ftpPath) 
	{
		this.ftpPath = ftpPath;
	}

	public String getFtpUser() 
	{
		return ftpUser;
	}

	public void setFtpUser(String ftpUser) 
	{
		this.ftpUser = ftpUser;
	}

	public String getFtpPwd() 
	{
		return ftpPwd;
	}

	public void setFtpPwd(String ftpPwd) 
	{
		this.ftpPwd = ftpPwd;
	}

	public String getLocalPath() 
	{
		return localPath;
	}

	public void setLocalPath(String path) 
	{
		this.localPath = path;
	}

	public int getFileSize() 
	{
		return fileSize;
	}

	public void setFileSize(int fileSize) 
	{
		this.fileSize = fileSize;
	}
	
	// 获取断点位置
	public long getFileBreakPoint()
	{
		return mBreakpoint;
	}
	
	// 设置断点位置
	public void setBreakPoint(long breakpoint)
	{
		this.mBreakpoint = breakpoint;
	}

	public int getProgress() 
	{
		return progress;
	}

	public void setProgress(int progress) 
	{
		this.progress = progress;
	}

	public int getState() 
	{
		return state;
	}

	public void setState(int state) 
	{
		this.state = state;
	}

	public OnDownloadListener getOnDownloadListener() 
	{
		return onDownloadListener;
	}

	public void setOnDownloadListener(OnDownloadListener onDownloadListener) 
	{
		this.onDownloadListener = onDownloadListener;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}
}
