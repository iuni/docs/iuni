package com.chinaunicom.custinforegist.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public class IdentityCard implements Parcelable 
{
	private String 		typeValue = "01";
	private String 		typeName = "居民身份证/临时身份证";
	private String 		name;					// 证件姓名
	private String 		no;						// 证件号码
	private String 		sex;					// 证件性别
	private String 		birthday;				// 出生日期
	private String 		flok;					// 民族
	private String 		strAddr;				// 证件地址
	private String		strRealAddr;			// 国政通返回的证件地址
	private String 		issueAuthority;			// 签发机关
	private String 		validPreod;				// 有效期
	private String 		picPath;
	private String 		picScalePath;
	
	private String 		mUserTel;
	private String 		mContactName;			// 联系人
	private String 		mContactTel;
	private String 		mContactAddr;			// 联系人通信地址
	
	public IdentityCard() 
	{
		
	}
	
	public IdentityCard(Parcel source) 
	{
		typeName = source.readString();
		typeValue = source.readString();
		mUserTel = source.readString();
		
		mContactName = source.readString();
		mContactTel = source.readString();
		mContactAddr = source.readString();
		
		picPath = source.readString();
		picScalePath = source.readString();
		name = source.readString();
		no = source.readString();
		sex = source.readString();
		birthday = source.readString();
		flok = source.readString();
		strAddr = source.readString();
		strRealAddr = source.readString();
		issueAuthority = source.readString();
		validPreod = source.readString();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeString(typeName);
		dest.writeString(typeValue);
		dest.writeString(mUserTel);
		
		dest.writeString(mContactName);
		dest.writeString(mContactTel);
		dest.writeString(mContactAddr);
		
		dest.writeString(picPath);
		dest.writeString(picScalePath);
		dest.writeString(name);
		dest.writeString(no);
		dest.writeString(sex);
		dest.writeString(birthday);
		dest.writeString(flok);
		dest.writeString(strAddr);
		dest.writeString(strRealAddr);
		dest.writeString(issueAuthority);
		dest.writeString(validPreod);
	}

	// 获取返档号码
	public String getTel() 
	{
		return mUserTel;
	}

	// 设置返档号码
	public void setTel(String userTel) 
	{
		this.mUserTel = userTel;
	}

	// 获取联系人
	public String getContactName() 
	{
		return mContactName;
	}

	// 设置联系人
	public void setContactName(String contactName) 
	{
		this.mContactName = contactName;
	}
	
	// 获取联系人电话
	public String getContactTel() 
	{
		return mContactTel;
	}

	// 设置联系人电话
	public void setContactTel(String contactTel) 
	{
		this.mContactTel = contactTel;
	}

	// 获取联系人通信地址
	public String getContactAddress() 
	{
		return mContactAddr;
	}

	// 设置联系人通信地址
	public void setContactAddress(String contactAddress) 
	{
		this.mContactAddr = contactAddress;
	}
	
	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getNo() 
	{
		/*
		if(no != null)
		{
			if(no.indexOf("(wrong number)") != -1)
			{
				no.replace("(wrong number)", "");
			}
		}
		*/
		return no;
	}

	public void setNo(String no) 
	{
		this.no = no;
	}

	public String getSex() 
	{
		return sex;
	}

	public void setSex(String sex) 
	{
		this.sex = sex;
	}

	public String getBirthday() 
	{
		return birthday;
	}

	public void setBirthday(String birthday) 
	{
		this.birthday = birthday;
	}

	public String getFlok() 
	{
		return flok;
	}

	public void setFlok(String flok) 
	{
		this.flok = flok;
	}

	public String getAddress() 
	{
		return strAddr;
	}

	public void setAddress(String address) 
	{
		this.strAddr = address;
	}
	
	// 获取国政通返回的证件地址
	public String getRealAddress() 
	{
		return strRealAddr;
	}
	
	// 设置国政通返回的证件地址
	public void setRealAddress(String address) 
	{
		this.strRealAddr = address;
	}
	
	// 获取签发机关
	public String getIssueAuthority() 
	{
		return issueAuthority;
	}
	
	// 设置签发机关
	public void setIssueAuthority(String issueAuthority) 
	{
		this.issueAuthority = issueAuthority;
	}

	public String getValidPreod() 
	{
		return validPreod;
	}

	public void setValidPreod(String validPreod) 
	{
		this.validPreod = validPreod;
	}

	@Override
	public int describeContents() 
	{
		return 0;
	}
	
	@Override
	public String toString() 
	{
		return "证件号:"+no +"\r\n"+
				"姓名:"+name+"\r\n"+
				"性别:"+sex+"\r\n"+
				"民族:"+flok+"\r\n"+
				"出生日期:"+birthday+"\r\n"+
				"住址:"+strAddr+"\r\n"+
				"签发机关:"+issueAuthority+"\r\n"+
				"有效期:"+validPreod;
	}
	
	public String getPicPath() 
	{
		return picPath;
	}

	public void setPicPath(String picPath) 
	{
		this.picPath = picPath;
	}

	public String getPicScalePath() 
	{
		return picScalePath;
	}

	public void setPicScalePath(String picScalePath) 
	{
		this.picScalePath = picScalePath;
	}

	public String getTypeValue() 
	{
		return typeValue;
	}

	public void setTypeValue(String typeValue) 
	{
		this.typeValue = typeValue;
	}

	public String getTypeName() 
	{
		return typeName;
	}

	public void setTypeName(String typeName) 
	{
		this.typeName = typeName;
	}

	public final static Parcelable.Creator<IdentityCard> CREATOR = new Creator<IdentityCard>() 
	{
		@Override
		public IdentityCard[] newArray(int size) 
		{
			return new IdentityCard[size];
		}
		
		@Override
		public IdentityCard createFromParcel(Parcel source) 
		{
			return new IdentityCard(source);
		}
	};
}
