package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class GetLoginRandomRequest extends BaseRequest
{
	public String			mStrAgentID;				// 发展人编码
	public String			mStrAgentPswd;				// 发展人密码
	
	public GetLoginRandomRequest(String strAgentID, String strAgentPswd) 
	{
		mStrAgentID 	= strAgentID;
		mStrAgentPswd 	= strAgentPswd;
	}
	
	@Override
	protected String getMethod() 
	{
		return "clientLoginFirst";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "clientLoginFirstResponse";
	}
	
	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mStrAgentID, mEncryptKey)));
		params.add(new Param(AGENT_PASSWORD, 	DES.encryptDES(mStrAgentPswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		params.add(new Param(VERSION_NAME, 		DeviceUtils.getVersionName()));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		return params;
	}
}
