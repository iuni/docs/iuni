package com.chinaunicom.custinforegist.api;

public class ApiParm {
	private String key;
	private Object value;
	
	public ApiParm(){}
	
	public ApiParm(String key, Object value) {
		super();
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
