package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class HeartbeatRequest extends BaseRequest 
{
	private String 		strAgentId;
	private String 		strAgentPswd;
	private String 		strLac;
	private String 		strCi;
	
	public HeartbeatRequest(String agentId, String agentPasswd, String lac, String ci) 
	{
		super();
		
		this.strAgentId 	= agentId;
		this.strAgentPswd 	= agentPasswd;
		this.strLac			= lac;
		this.strCi			= ci;
	}

	public HeartbeatRequest(String agentId, String agentPasswd, String sendSMSflag, String lac, String ci) 
	{
		super();
		
		this.strAgentId 	= agentId;
		this.strAgentPswd 	= agentPasswd;
		this.strLac 		= lac;
		this.strCi			= ci;
	}
	
	@Override
	protected String getMethod() 
	{
		return "HeartBeat";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "HeartBeatResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(strAgentId, mEncryptKey)));
		params.add(new Param(AGENT_PASSWORD, 	DES.encryptDES(strAgentPswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		DES.encryptDES("01", mEncryptKey)));
		params.add(new Param(VERSION_CODE, 		DES.encryptDES("1." + DeviceUtils.getVersionCode(), mEncryptKey)));
		params.add(new Param(VERSION_NAME, 		DES.encryptDES(DeviceUtils.getVersionName(), mEncryptKey)));
		params.add(new Param(LAC, 				strLac));
		params.add(new Param(CI, 				strCi));
		params.add(new Param(TERMINAL_TYPE,		mTerminalType));		// 0x01-M66不开机自启动 0x02-M66开机自启动
		return params;
	}
}
