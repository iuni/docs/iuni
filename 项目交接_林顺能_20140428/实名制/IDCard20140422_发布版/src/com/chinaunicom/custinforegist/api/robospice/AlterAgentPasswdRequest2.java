package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class AlterAgentPasswdRequest2 extends BaseRequest 
{
	private String 		agentId;
	//private String 	oldPasswd;
	private String 		newPasswd;
	private String 		identifyId;
	private String 		communicaID;

	public AlterAgentPasswdRequest2(String agentId, String oldPasswd, String newPasswd,String identifyId,String communicaId) 
	{
		super();
		
		this.agentId 		= agentId;
		//this.oldPasswd 	= oldPasswd;
		this.newPasswd 		= newPasswd;
		this.identifyId 	= identifyId;
		this.communicaID 	= communicaId;
	}

	@Override
	protected String getMethod() 
	{
		return "alterAgentPasswdOne";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "alterAgentPasswdOneResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	communicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(OLD_PASSWORD, 		null));
		params.add(new Param(NEW_PASSWORD, 		DES.encryptDES(newPasswd, mEncryptKey)));
		params.add(new Param(IDENTIFY_ID, 		DES.encryptDES(identifyId, mEncryptKey)));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
