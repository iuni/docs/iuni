package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;
import android.util.Log;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.base.App;

public class UploadCertificateInfoRequestNew extends BaseRequest 
{
	private String 		mStrCommunicaID;
	private String 		mStrAgentId;
	private String 		mStrTelphone;
	private String 		mStrCertificateName;
	private String 		mStrCertificateType;
	private String 		mStrCertificateNum;
	private String 		mStrCertificateAdd;
	
	private String		mStrContactName;			// 联系人姓名
	private String		mStrContactTel;				// 联系人电话
	private String		mStrContactAddr;			// 联系人通信地址
	
	public UploadCertificateInfoRequestNew(String strCommunicaID, String strAgentId, String strTelphone, 
			String strCertificateName, String strCertificateType, String strCertificateNum, String strCertificateAdd, 
			String strContactName, String strContactTel, String strContactAddr) 
	{
		mStrCommunicaID 		= strCommunicaID;
		mStrAgentId 			= strAgentId;
		mStrTelphone 			= strTelphone;
		mStrCertificateName 	= strCertificateName;
		mStrCertificateType 	= strCertificateType;
		mStrCertificateNum 		= strCertificateNum;
		mStrCertificateAdd 		= strCertificateAdd;
		
		mStrContactName			= strContactName;
		mStrContactTel			= strContactTel;
		mStrContactAddr			= strContactAddr;
		
		Log.e("", "### 证件地址为: " + mStrCertificateAdd);
	}
	
	@Override
	protected String getMethod() 
	{
		return "uploadCerInfoNew";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "uploadCerInfoNewResponse";
	}
	
	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	mStrCommunicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mStrAgentId, 		App.getDesKey())));
		params.add(new Param(USER_TEL, 			DES.encryptDES(mStrTelphone, 		App.getDesKey())));
		params.add(new Param(USER_NAME, 		DES.encryptDES(mStrCertificateName, App.getDesKey())));
		params.add(new Param(USER_TYPE, 		DES.encryptDES(mStrCertificateType, App.getDesKey())));
		params.add(new Param(USER_NO, 			DES.encryptDES(mStrCertificateNum, 	App.getDesKey())));
		params.add(new Param(USER_ADDR, 		DES.encryptDES(mStrCertificateAdd, 	App.getDesKey())));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		// 联系人
		if(TextUtils.isEmpty(mStrContactName)) 
		{
			params.add(new Param(CONTACT_NAME, ""));
		}
		else
		{
			params.add(new Param(CONTACT_NAME, 	DES.encryptDES(mStrContactName, App.getDesKey())));
		}
		// 联系电话
		if(TextUtils.isEmpty(mStrContactTel)) 
		{
			params.add(new Param(CONTACT_TEL, 	""));
		}
		else
		{
			params.add(new Param(CONTACT_TEL, 	DES.encryptDES(mStrContactTel, App.getDesKey())));
		}
		// 联系人通信地址
		if(TextUtils.isEmpty(mStrContactAddr)) 
		{
			params.add(new Param(CONTACT_ADDR, 	""));
		}
		else
		{
			params.add(new Param(CONTACT_ADDR, 	DES.encryptDES(mStrContactAddr, App.getDesKey())));
		}
		return params;
	}
}
