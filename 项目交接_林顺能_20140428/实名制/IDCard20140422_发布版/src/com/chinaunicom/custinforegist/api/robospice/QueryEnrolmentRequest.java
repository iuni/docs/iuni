package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class QueryEnrolmentRequest extends BaseRequest 
{
	private String 		agentId;
	private String 		beginTime;
	private String 		endTime;
	private int 		queryStart;
	private int 		queryAll;
	private String		mStrFdChanned;
	
	public QueryEnrolmentRequest(String agentId, String beginTime, String endTime, int queryStart, int queryAll, String strFdChanned) 
	{
		this.agentId 		= agentId;
		this.beginTime 		= beginTime;
		this.endTime 		= endTime;
		this.queryStart 	= queryStart;
		this.queryAll 		= queryAll;
		this.mStrFdChanned	= strFdChanned;
	}
	
	@Override
	protected String getMethod() 
	{
		return "queryRealName";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "queryRealNameResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		Log.e("", "beginTime = " + beginTime);
		Log.e("", "endTime = " + endTime);

		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(BEGIN_TIME, 		DES.encryptDES(beginTime, mEncryptKey)));
		params.add(new Param(END_TIME, 			DES.encryptDES(endTime, mEncryptKey)));
		params.add(new Param(QUERY_START, 		DES.encryptDES(String.valueOf(queryStart), mEncryptKey)));
		params.add(new Param(QUERY_COUNT, 		DES.encryptDES(String.valueOf(queryAll), mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		params.add(new Param(VERSION_NAME, 		DeviceUtils.getVersionName()));
		params.add(new Param(VERSION_CODE, 		"1." + DeviceUtils.getVersionCode()));
		params.add(new Param(FD_CHANNED, 		mStrFdChanned));
		return params;
	}
}
