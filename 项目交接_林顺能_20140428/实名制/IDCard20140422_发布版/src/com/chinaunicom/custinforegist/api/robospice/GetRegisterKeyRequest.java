package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;

public class GetRegisterKeyRequest extends BaseRequest 
{
	private String communicaId;
	private String agentId;
	private String identifyId;

	public GetRegisterKeyRequest(String communicaId, String agentId, String identifyId) 
	{
		super();
		this.communicaId = communicaId;
		this.agentId = agentId;
		this.identifyId = identifyId;
	}

	@Override
	protected String getMethod() 
	{
		return "mobileClientLoginCheck";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "mobileClientLoginCheckResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	communicaId));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(IDENTIFY_ID, 		DES.encryptDES(identifyId, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
