package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;

public class MobileClientLogOffRequest extends BaseRequest 
{
	private String agentId;

	public MobileClientLogOffRequest(String agentId) 
	{
		super();
		this.agentId = agentId;
	}

	@Override
	protected String getMethod() 
	{
		return "mobileClientLogOff";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "mobileClientLogOffResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();

		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
