package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class TelGetRequest extends BaseRequest 
{
	private String 		mStrAgentId;
	private String 		mStrIccid;
	private String		mStrTelLast4;

	public TelGetRequest(String agentId, String iccid, String telLast4) 
	{
		this.mStrAgentId 	= agentId;
		this.mStrIccid 		= iccid;
		this.mStrTelLast4	= telLast4;
	}

	@Override
	protected String getMethod() 
	{
		return "NetCardFind";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "NetCardFindResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();

		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mStrAgentId, mEncryptKey)));
		params.add(new Param(ICCID_NUMBER, 		DES.encryptDES(mStrIccid, mEncryptKey)));
		params.add(new Param(VERSION_NAME, 		DeviceUtils.getVersionName()));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		params.add(new Param(TEL_LAST4,			mStrTelLast4));
		return params;
	}
}
