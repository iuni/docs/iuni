package com.chinaunicom.custinforegist.api.robospice;

public class Param 
{
	private String 			mKey		= null;
	private String 			mVal		= null;

	public String getKey() 
	{
		return mKey;
	}

	public void setKey(String key) 
	{
		this.mKey = key;
	}

	public String getVal() 
	{
		return mVal;
	}

	public void setVal(String val) 
	{
		this.mVal = val;
	}

	public Param(String key, String val) 
	{
		super();
		this.mKey = key;
		this.mVal = val;
	}
	
	@Override
	public String toString() 
	{
		return "<"+mKey+">" + mVal + "</"+mKey+">";
	}
}
