package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

// 忘记密码获取校验码
public class GetIdentifyIDRequest2 extends BaseRequest 
{
	private String 		agentId;
	//private String 	agentTelephone;
	
	public GetIdentifyIDRequest2(String agentId, String agentTelephone) 
	{
		super();
		
		this.agentId = agentId;
		//this.agentTelephone = agentTelephone;
	}
	
	@Override
	protected String getMethod() 
	{
		return "getIdentifyIDOne";
	}
	
	@Override
	protected String getResponseMethod() 
	{
		return "getIdentifyIDOneResponse";
	}
	
	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(AGENT_TEL, 		null));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
