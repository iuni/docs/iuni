package com.chinaunicom.custinforegist.api.model;

import java.io.Serializable;

// 版本升级信息
@SuppressWarnings("serial")
public class Version implements Serializable
{
	private boolean 	mIsNeedUpdate;
	private String 		mStrVersionCode;
	private String 		mStrVersionName;
	private String		mStrUpdateTitle;
	private String 		mStrUpdateLog;
	private String 		mStrFtpPath;
	
	public boolean isNeedUpdate() 
	{
		return mIsNeedUpdate;
	}

	public void setNeedUpdate(boolean needUpdate) 
	{
		this.mIsNeedUpdate = needUpdate;
	}
	
	public String getVersionCode() 
	{
		return mStrVersionCode;
	}
	
	public void setVersionCode(String versionCode) 
	{
		this.mStrVersionCode = versionCode;
	}
	
	// 获取版本名称
	public String getVersionName() 
	{
		return mStrVersionName;
	}
	
	// 设置版本名称
	public void setVersionName(String versionName) 
	{
		this.mStrVersionName = versionName;
	}
	
	// 获取升级标题
	public String getUpdateTitle() 
	{
		return mStrUpdateTitle;
	}
	
	// 设置升级标题
	public void setUpdateTitle(String strUpdateTitle) 
	{
		this.mStrUpdateTitle = strUpdateTitle;
	}
	
	// 获取升级修改内容
	public String getUpdateLog() 
	{
		return mStrUpdateLog;
	}
	
	// 设置升级修改内容
	public void setUpdateLog(String updateLog) 
	{
		this.mStrUpdateLog = updateLog;
	}
	
	public String getFtpPath() 
	{
		return mStrFtpPath;
	}
	
	public void setFtpPath(String ftpPath) 
	{
		this.mStrFtpPath = ftpPath;
	}
}
