package com.chinaunicom.custinforegist.activity.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.PLog;
import com.sunnada.sqlite.MsgDBAdapter;

// 查看新的公告
public class NewMsgActivity extends BaseActivity implements OnClickListener
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_msg);
		
		// 初始化数据
		initData();
	}
	
	// 初始化数据
	private void initData() 
	{
		Intent intent 	= getIntent();
		String idStr 	= intent.getStringExtra("id");
		String titleStr = intent.getStringExtra("title");
		String msgStr 	= intent.getStringExtra("msg");
		PLog.v("", "ID为: " + idStr + ", 标题为: " + titleStr + ", 内容为: " + msgStr);
		MsgDBAdapter.updateDataByID(NewMsgActivity.this, idStr, "0");
		
		// 显示新的公告
		App.showAlert(NewMsgActivity.this, titleStr, msgStr, "确定", 
				new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();
						finish();
						// 启动公告箱
						//startMsgActivity();
						ActivityHelper.goMain(NewMsgActivity.this);
					}
				}, 
				"取消", 
				new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();
						finish();
						// 启动公告箱
						//startMsgActivity();
						finish();
					}
				});
	}
	
	// 启动公告箱
	protected void startMsgActivity() 
	{
		Intent intent = new Intent(NewMsgActivity.this, MsgActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}
}
