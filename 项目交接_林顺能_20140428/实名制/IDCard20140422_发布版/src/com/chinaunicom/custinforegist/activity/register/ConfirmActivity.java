package com.chinaunicom.custinforegist.activity.register;

import java.io.File;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.main.HelpDesActivity;
import com.chinaunicom.custinforegist.api.ApiClient;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.UploadCertificateInfoRequest;
import com.chinaunicom.custinforegist.api.robospice.UploadCertificateInfoRequestNew;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.CheckUtils;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class ConfirmActivity extends BaseActivity implements OnClickListener
{
	private final int 						MSG_UPLOAD_SUC 			= 0x01;
	private final int 						MSG_UPLOAD_FAIL 		= 0x02;
	private final int						MSG_UPLOAD_TIMEOUT		= 0x03;
	
	private TextView						mTvTitle;
	private ImageView						mIvStatus;
	private TextView						mTvStatus;
	private TextView						mTvTel;
	private TextView						mTvTypeName;
	private TextView 						mTvName;
	private TextView						mTvNo;
	private EditText						mEtAddr;
	private TextView						mTvAddr;
	private TextView						mTvContactName;
	private TextView						mTvContactPhone;
	private TextView						mTvContactAddr;
	
	private ImageView 						mIvTipAddress;
	//private ImageView 					pic;
	
	private String 							mStrUploadType;
	private String 							mStrCommunicaId;
	private IdentityCard 					mCard;
	private String 		 					mStrTypeFlag;				// 类型标识，用来判断标题显示(ICCID, 手机号)
	private UploadCertificateInfoRequest 	mUploadRequest;
	private UploadCertificateInfoRequestNew mUploadRequestNew;
	
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what) 
			{
			// 证件信息上传成功
			case MSG_UPLOAD_SUC:
				if(mCard != null && !TextUtils.isEmpty(mCard.getPicPath()))
				{
					new File(mCard.getPicPath()).delete();
				}
				App.showAlert(ConfirmActivity.this,
					//getString(R.string.upload_suc_message, mCard.getTel()),
					(String)msg.obj,
					R.string.ok, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
							ActivityHelper.goMain(ConfirmActivity.this);
							finish();
						}
					});
				break;
			
			// 证件信息上传失败
			case MSG_UPLOAD_FAIL:
				System.out.println("========MSG_UPLOAD_FAIL=========");
				String desc = (String) msg.obj;
				if(TextUtils.isEmpty(desc) || "anyType{}".equals(desc)) 
				{
					desc = "服务器响应超时！返档失败，请勿销售。(序号：39003）";
				}
				App.showAlert(ConfirmActivity.this, desc,
						R.string.ok, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.dismiss();
								ActivityHelper.goMain(ConfirmActivity.this);
								finish();
							}
						});
				break;
			
			// 证件信息上传超时
			case MSG_UPLOAD_TIMEOUT:
				System.out.println("========CONFIRM_SUBMIT=========");
				App.showAlert(ConfirmActivity.this, "服务器响应超时！返档失败，请勿销售。(序号：39003）");
				//App.showAlert(ConfirmActivity.this, "服务器响应超时！完成返档前，请勿销售。(序号：3997)");
				break;
				
			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_phone_step4);
		
		mStrUploadType 	= getIntent().getExtras().getString("uploadType");
		mStrCommunicaId = getIntent().getExtras().getString("communicaId");
		mCard 			= getIntent().getExtras().getParcelable("card");
		mStrTypeFlag 	= getIntent().getStringExtra("typeFlag");
		
		// 初始化控件
		initViews();
	}
	
	// 初始子控件
	private void initViews() 
	{
		// 标题
		mTvTitle = (TextView) findViewById(R.id.tvTitle);
		mTvTitle.setText(mStrTypeFlag.equals("iccid")? getString(R.string.register_iccid):getString(R.string.register_phone));
		// 步骤信息
		mIvStatus = (ImageView) findViewById(R.id.iv_status);
		mTvStatus = (TextView) findViewById(R.id.tv_status);
		if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
		{
			mIvStatus.setImageResource(R.drawable.image_step3_new);
			mTvStatus.setText(R.string.phone_step3_without_certify_type);
		}
		else
		{
			mIvStatus.setImageResource(R.drawable.image_step4);
			mTvStatus.setText(R.string.phone_step4_with_certify_type);
		}
		// 证件类型
		mTvTypeName = (TextView) findViewById(R.id.tv_type);
		if (!TextUtils.isEmpty(mCard.getTypeName())) 
		{
			mTvTypeName.setText(mCard.getTypeName());
		}
		// 电话号码
		mTvTel = (TextView) findViewById(R.id.tv_tel);
		if (!TextUtils.isEmpty(mCard.getTel())) 
		{
			mTvTel.setText(mCard.getTel());
		}
		// 证件姓名
		mTvName = (TextView) findViewById(R.id.tv_name);
		if (!TextUtils.isEmpty(mCard.getName()))
		{
			mTvName.setText(mCard.getName());
		}
		// 证件号码
		mTvNo = (TextView) findViewById(R.id.tv_no);
		if (!TextUtils.isEmpty(mCard.getNo())) 
		{
			mTvNo.setText(mCard.getNo());
		}
		// 证件地址
		mEtAddr = (EditText) findViewById(R.id.et_address);
		mTvAddr	= (TextView) findViewById(R.id.tv_address);
		mIvTipAddress = (ImageView) findViewById(R.id.iv_tip_address);
		// 国政通如果有开启并且有返回证件地址, 则不做地址校验 20131230
		if(mCard.getTypeValue().equals("01") && Interface.mStrGztFlag.equals("1")) 
		{
			mTvAddr.setVisibility(View.VISIBLE);
			mEtAddr.setVisibility(View.GONE);
			// 国政通返回的证件地址非空
			if (!StringUtil.isEmptyOrNull(mCard.getRealAddress())) 
			{
				mTvAddr.setText(mCard.getRealAddress());
				mIvTipAddress.setVisibility(View.VISIBLE);
				mIvTipAddress.setImageResource(R.drawable.ic_correct);
				// 比较国政通返回的与身份证识别返回的证件地址是否一致
				if(!StringUtil.isEmptyOrNull(mCard.getAddress()) && !mCard.getAddress().equals(mCard.getRealAddress())) 
				{
					App.showAlert(ConfirmActivity.this, 
							"您拍照识别的证件地址与公安部身份信息库信息不一致，提交时将以公安部身份信息库证件地址信息为准。");
				}
			}
			else
			{
				// 比较国政通返回的与身份证识别返回的证件地址是否一致
				if(!StringUtil.isEmptyOrNull(mCard.getAddress())) 
				{
					App.showAlert(ConfirmActivity.this, 
							"您拍照识别的证件地址与公安部身份信息库信息不一致，提交时将以公安部身份信息库证件地址信息为准。");
				}
			}
		}
		else
		{
			mTvAddr.setVisibility(View.GONE);
			mEtAddr.setVisibility(View.VISIBLE);
			// 添加监听事件
			mEtAddr.addTextChangedListener(new SimpleTextWatcher() 
			{
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) 
				{
					if(StringUtil.isEmptyOrNull(s.toString())) 
					{
						mIvTipAddress.setVisibility(View.INVISIBLE);
					}
					else
					{
						mIvTipAddress.setVisibility(View.VISIBLE);
						if (CheckUtils.isAddressValid(mCard.getTypeValue(), s.toString()) != 0)
						{
							mIvTipAddress.setImageResource(R.drawable.ic_error);
						} 
						else 
						{
							mIvTipAddress.setImageResource(R.drawable.ic_correct);
						}
					}
				}
			});
			// 身份证识别返回的证件地址非空
			if(!StringUtil.isEmptyOrNull(mCard.getAddress())) 
			{
				mEtAddr.setText(mCard.getAddress());
				mEtAddr.setSelection(mCard.getAddress().length());
			}
		}
		
		// 联系人信息
		mTvContactName = (TextView) findViewById(R.id.tv_contact_name);
		mTvContactName.setText(mCard.getContactName());
		mTvContactPhone = (TextView) findViewById(R.id.tv_contact_phone);
		mTvContactPhone.setText(mCard.getContactTel());
		mTvContactAddr = (TextView) findViewById(R.id.tv_contact_address);
		mTvContactAddr.setText(mCard.getContactAddress());
		
		findViewById(R.id.btn_back).setOnClickListener(this);
		findViewById(R.id.btn_submit).setOnClickListener(this);
		findViewById(R.id.btn_help).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
			// 返回
			case R.id.btn_back:
				ConfirmActivity.this.finish();
				break;
				
			// 提交
			case R.id.btn_submit:
				handleUpload();
				break;
				
			// 帮助
			case R.id.btn_help:
				goHelp();
				break;
				
			default:
				break;
		}
	}

	private void goHelp() 
	{
		Intent intent = new Intent(this, HelpDesActivity.class);
		intent.putExtra(Interface.HELP_TYPE, "f4");
		startActivity(intent);
		//App.showAlert(this, R.string.app_help_register_4, R.string.ok, null);
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		/*
		if(pic != null) 
		{
			Drawable d = pic.getDrawable();
			pic.setImageDrawable(null);
			recycleDrawable(d);
		}
		*/
		// 调用垃圾回收器
		Interface.causeGC();
	}
	
	// 提交信息
	private void handleUpload() 
	{
		// 国政通如果有开启并且有返回证件地址, 则不做地址校验 20131230
		if(mCard.getTypeValue().equals("01") && Interface.mStrGztFlag.equals("1")) 
		{
			
		}
		else
		{
			final String address = mEtAddr.getText().toString();
			// 地址非空
			if (StringUtil.isEmptyOrNull(address)) 
			{
				mEtAddr.requestFocus();
				mIvTipAddress.setVisibility(View.VISIBLE);
				mIvTipAddress.setImageResource(R.drawable.ic_error);
				App.showAlert(ConfirmActivity.this, "请输入证件地址！");
				return;
			}
			// 地址合法性判断
			int result = CheckUtils.isAddressValid(mCard.getTypeValue(), address);
			if (result != 0) 
			{
				mEtAddr.requestFocus();
				mIvTipAddress.setVisibility(View.VISIBLE);
				mIvTipAddress.setImageResource(R.drawable.ic_error);
				// 
				//String tempstr = (result==1? "您输入的证件地址不合法, 请重新输入！":"您输入的证件地址长度太短, 请重新输入！");
				String tempstr = "您输入的证件地址不合规, 请重新输入！";
				App.showAlert(ConfirmActivity.this, tempstr);
				return;
			}
			// 设置新地址, 有可能修改过, 也有可能没修改过
			mCard.setAddress(address);
		}
		
		// 判断网络是否OK
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(ConfirmActivity.this, getString(R.string.network_exception));
			return;
		}
		
		Interface.attachProgressDialog(this, "提示信息", "正在提交证件信息...");
		// 调用国政通
		if(mCard.getTypeValue().equals("01") && Interface.mStrGztFlag.equals("1")) 
		{
			mUploadRequestNew = new UploadCertificateInfoRequestNew(
					mStrCommunicaId, 
					App.getAgentId(), 
					mCard.getTel(), 
					mCard.getName(), 
					mCard.getTypeValue(), 
					mCard.getNo(), 
					mCard.getRealAddress(), 
					mCard.getContactName(), 
					mCard.getContactTel(), 
					mCard.getContactAddress());
			executeRequest(mHandler, MSG_UPLOAD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mUploadRequestNew, new UploadRequestListener());
		}
		// 未调用国政通
		else
		{
			mUploadRequest = new UploadCertificateInfoRequest(
					mStrCommunicaId, 
					App.getAgentId(), 
					App.getDesKey(), 
					mCard.getTel(), 
					mCard.getName(), 
					mCard.getNo(), 
					mCard.getTypeValue(), 
					mCard.getAddress(), 
					mCard.getContactName(), 
					mCard.getContactTel(), 
					mCard.getContactAddress());
			executeRequest(mHandler, MSG_UPLOAD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mUploadRequest, new UploadRequestListener());
		}
	}
	
	// 上传证件信息的监听事件
	public final class UploadRequestListener implements RequestListener<String>
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_UPLOAD_FAIL).sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售(序号：39001)").sendToTarget();
				//mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售。（序号：3998）").sendToTarget();
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				Map<String,String> result = null;
				if(mCard.getTypeValue().equals("01") && Interface.mStrGztFlag.equals("1")) 
				{
					result = mUploadRequestNew.getResult();
				}
				else
				{
					result = mUploadRequest.getResult();
				}
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				// 文本上传成功
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 上传图片
					if("1".equals(mStrUploadType)) 
					{
						mHandler.obtainMessage(MSG_UPLOAD_SUC, desc).sendToTarget();
					} 
					else
					{
						mHandler.obtainMessage(MSG_UPLOAD_SUC, desc).sendToTarget();
				    }
				}
				else 
				{
					mHandler.obtainMessage(MSG_UPLOAD_FAIL, desc).sendToTarget();
				} 
			}
			catch (Exception e)
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售(序号：39001)").sendToTarget();
			}
		}
	}
	
	class UploadTask extends AsyncTask<String, Void, Void> 
	{
		@Override
		protected Void doInBackground(String... params) 
		{
			try 
			{
				File picFile = new File(mCard.getPicScalePath());
				if(ApiClient.ftpUpload(picFile.getParentFile().getAbsolutePath()+File.separator, picFile.getName()))
				{
					mHandler.obtainMessage(MSG_UPLOAD_SUC).sendToTarget();
				} 
				else 
				{
					mHandler.obtainMessage(MSG_UPLOAD_FAIL, "证件图片上传失败。").sendToTarget();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_UPLOAD_FAIL, "返档失败，请勿销售(序号：39001)").sendToTarget();
			}
			return null;
		}
	}
}
