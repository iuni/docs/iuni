package com.chinaunicom.custinforegist.activity.register;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.main.HelpDesActivity;
import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.TelCheckRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.FileUtil;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class TelphoneActivity extends BaseActivity implements OnClickListener 
{
	private final int					MSG_CHECK_TEL_SUC		= 0x01;
	private final int 					MSG_CHECK_SUC_OLD 		= 0x02;
	private final int					MSG_CHECK_TEL_FAIL		= 0x03;
	private final int					MSG_CHECK_TEL_TIMEOUT	= 0x04;
	
	private ImageView					mIvStatus;
	private EditText 					mEtTelNo;
	private ImageView 					mIvTipTel;

	private String 						mStrTel;
	private String 						mStrCommunicaId;
	private String 						mStrUploadType; 			// 0 1
	private TelCheckRequest 			request;
	
	private Handler 					mHandler = new Handler() 
	{
		public void handleMessage(android.os.Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what)
			{
			// 校验号码成功
			case MSG_CHECK_TEL_SUC:
				Intent intent = null;
				IdentityCard card = new IdentityCard();
				// 只允许身份证
				if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
				{
					intent = new Intent(TelphoneActivity.this, PictureActivity.class);
					card.setTypeValue(getResources().getStringArray(R.array.type_card_value)[0]);
					card.setTypeName(getResources().getStringArray(R.array.type_card_name)[0]);
				}
				// 允许其他证件类型
				else
				{
					intent = new Intent(TelphoneActivity.this, CustTypeActivity.class);
				}
				card.setTel(mStrTel);
				intent.putExtra("card", card);
				intent.putExtra("communicaId", mStrCommunicaId);
				intent.putExtra("uploadType", mStrUploadType);
				intent.putExtra("typeFlag", "tel");
				startActivity(intent);
				break;
				
			case MSG_CHECK_SUC_OLD:
				App.showAlert(TelphoneActivity.this,
						mStrTel +"用户为老用户，请用户登录中国联通网上营业厅或手机营业厅或发送彩信至10010进行补登记。", R.string.ok,
						new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface arg0, int arg1) 
							{
								mEtTelNo.setText("");
								arg0.dismiss();
							}
						});
				break;
				
			case MSG_CHECK_TEL_FAIL:
				String desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) 
				{
					App.showAlert(TelphoneActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。(序号：29003）");
					//App.showAlert(TelphoneActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。（序号：2997）");
				}
				else 
				{
					App.showAlert(TelphoneActivity.this, desc+"");
				}
				break;
				
			case MSG_CHECK_TEL_TIMEOUT:
				App.showAlert(TelphoneActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。(序号：29003）");
				//App.showAlert(TelphoneActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。（序号：2027）");
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_phone_step1);
		
		// 初始化控件
		initViews();
		// 删除照片文件夹
		try
		{
			FileUtil.deleteDirectory(new File(App.getImagesDir()));
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) 
	{
		super.onNewIntent(intent);
		
		if(intent != null && intent.getExtras()!= null) 
		{
			boolean clear = intent.getExtras().getBoolean("clear");
			if(clear) 
			{
				mEtTelNo.setText("");
			}
		}
		mIvTipTel.setVisibility(View.GONE);
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		try 
		{
			FileUtil.deleteDirectory(new File(App.getImagesDir()));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		// 调用垃圾回收器
		Interface.causeGC();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mIvStatus = (ImageView) findViewById(R.id.iv_status);
		// 只允许身份证
		if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
		{
			mIvStatus.setImageResource(R.drawable.image_step1_new);
		}
		else
		{
			mIvStatus.setImageResource(R.drawable.image_step1);
		}
		
		mIvTipTel = (ImageView)findViewById(R.id.iv_tip_tel);
		mEtTelNo = (EditText)findViewById(R.id.et_tel_no);
		mEtTelNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				mIvTipTel.setVisibility(View.VISIBLE);
				if(Interface.isSymbol(s))
				{
					s  = s.subSequence(0, s.length() - 1);
					mEtTelNo.setText(s);
					mEtTelNo.setSelection(s.length());
					return;
				}
				if(TextUtils.isEmpty(s)  || s.length() != 11)
				{
					mIvTipTel.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mIvTipTel.setImageResource(R.drawable.ic_correct);
				}
				super.onTextChanged(s, start, before, count);
			}
		});
		
		// 返回
		findViewById(R.id.btn_back).setOnClickListener(this);
		// 校验手机号码
		findViewById(R.id.btn_check_telphone).setOnClickListener(this);
		// 帮助
		findViewById(R.id.btn_help).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		// 返回
		case R.id.btn_back:			
			finish();
			break;
		
		// 帮助
		case R.id.btn_help:		
			goHelp();
			break;
		
		// 下一步
		case R.id.btn_check_telphone:
			/*
			Intent intent = new Intent(TelphoneActivity.this, CustTypeActivity.class);
			IdentityCard card = new IdentityCard();
			card.setTel(mTel);
			intent.putExtra("card", card);
			intent.putExtra("communicaId", communicaId);
			intent.putExtra("uploadType", uploadType);
			intent.putExtra("typeFlag", "tel");
			startActivity(intent);
			*/
			handleCheckTelphone();
			break;
			
		default:
			break;
		}
	}
	
	private void goHelp() 
	{
		Intent intent = new Intent(this, HelpDesActivity.class);
		intent.putExtra(Interface.HELP_TYPE, "f1");
		startActivity(intent);
		//App.showAlert(this, R.string.app_help_register_phone, R.string.ok, null);
	}
	
	// 显示错误
	private void showError(String errorStr)
	{
		App.showAlert(this, errorStr, R.string.ok, null);
		mIvTipTel.setVisibility(View.VISIBLE);
		mIvTipTel.setImageResource(R.drawable.ic_error);
	}

	// 校验手机号码
	private void handleCheckTelphone() 
	{
		mStrTel = mEtTelNo.getText().toString().trim();
		// 判断号码是否为空
		if(isNull(mStrTel)) 
		{
			showError("请输入手机号！");
			return;
		}
		// 判断号码长度
		if(mStrTel.length() != 11) 
		{
			showError("请输入11位手机号码！");
			return;
		}
		// 判断号码里面是否是纯数字
		try
		{
			byte[] bytes = mStrTel.getBytes();
			for(int i=0; i<11; i++)
			{
				if(bytes[i] < 0x30 || bytes[i] > 0x39) 
				{
					showError("您输入的号码有误，请重新输入！");
					return;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			showError("您输入的号码有误，请重新输入！");
			return;
		}
		// 非联通号码
		if(isUnicomNumber(mStrTel) == false) 
		{
			showError("非联通号码，请重新输入！");
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			App.showAlert(TelphoneActivity.this, getString(R.string.network_exception));
			return;
		}
		
		Interface.attachProgressDialog(this, "号码校验", "正在校验号码...");
		request = new TelCheckRequest(App.getAgentId(), mStrTel, App.getDesKey(), "01");
		executeRequest(mHandler, MSG_CHECK_TEL_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, request, new TelCheckRequestListener());
	}
	
	// 校验是否是联通手机号码
	private boolean isUnicomNumber(String telstr)
	{
		try
		{
			telstr = telstr.substring(0, 3);
			if(telstr.equals("130") || telstr.equals("131") || telstr.equals("132") || telstr.equals("155") 
					|| telstr.equals("156") || telstr.equals("185") || telstr.equals("186") || telstr.equals("145"))
			{
				return true;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public final class TelCheckRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL).sendToTarget();
			}
			else
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
				//mHandler.obtainMessage(MSG_CHECK_TEL_FAIL, "该手机号未验证通过，请勿销售（序号：2998）").sendToTarget();
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String, String> result = request.getResult();
				// 检测公告
				Interface.checkMail(TelphoneActivity.this, result, false);
				// 检测交易状态
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if(BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 业务时序
					mStrCommunicaId = result.get(BaseRequest.COMMUNICATION_ID);
					// 证件照片上传开关
					String type = result.get(BaseRequest.UPLOAD_TYPE);
					Log.e("", "uploadType: " + mStrUploadType);
					mStrUploadType = DES.decryptDES(type, App.getDesKey());
					// 输入状态
					Interface.checkInputType(result);
					// 证件类型开关
					Interface.checkCertType(result);
					// 国政通开关
					Interface.mStrGztFlag = result.get(BaseRequest.GZT_FLAG);
					Log.e("", "国政通开关: " + Interface.mStrGztFlag);
					// 
					mHandler.obtainMessage(MSG_CHECK_TEL_SUC, mStrCommunicaId).sendToTarget();
				} 
				//else if("0001".equals(tradeState)) 
				//{
				//	mHandler.sendEmptyMessage(MSG_CHECK_SUC_OLD);
				//} 
				else 
				{
					// 获取错误指示
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHECK_TEL_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
				//mHandler.obtainMessage(MSG_CHECK_TEL_FAIL, "该手机号未验证通过，请勿销售（序号：2998）").sendToTarget();
			}
		}
	}
}
