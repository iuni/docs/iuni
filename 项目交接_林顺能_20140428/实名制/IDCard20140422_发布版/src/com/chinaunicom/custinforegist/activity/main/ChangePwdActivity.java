package com.chinaunicom.custinforegist.activity.main;

import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.ActivateActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.api.robospice.AlterAgentPasswdRequest;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.CheckUtils;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class ChangePwdActivity extends BaseActivity implements OnClickListener 
{
	private static final int 			MSG_CHANGE_SUC 		= 0x01;
	private static final int 			MSG_CHANGE_FAIL 	= 0x02;
	private static final int			MSG_CHANGE_TIMEOUT	= 0x03;

	private EditText 					newPwd, newPwd2, oldPwd;
	private ImageView 					tipNew, tipNew2, tipOld;

	private TextView					mTextRecPerson;
	private Button 						mBtnSubmit;
	private Button 						mBtnBack;
	private AlterAgentPasswdRequest 	mRequest;

	Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what) 
			{
			// 修改密码成功
			case MSG_CHANGE_SUC:
				App.setAgentPwd(newPwd.getText().toString());
				App.showAlert(ChangePwdActivity.this,
						"您的登录密码已修改成功，请您妥善保存修改后的密码!", R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.dismiss();
								ActivityHelper.goMain(ChangePwdActivity.this);
								finish();
							}
						});
				break;
				
			// 修改密码失败
			case MSG_CHANGE_FAIL:
				String desc = (String) msg.obj;
				if (TextUtils.isEmpty(desc) || "anyType{}".equals(desc)) {
					App.showAlert(ChangePwdActivity.this, "修改失败, 请重试!");
				} else {
					App.showAlert(ChangePwdActivity.this, desc);
				}
				mBtnSubmit.setEnabled(true);
				break;
			
			// 修改密码超时
			case MSG_CHANGE_TIMEOUT:
				App.showAlert(ChangePwdActivity.this, "服务请求超时, 请重试!");
				mBtnSubmit.setEnabled(true);
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_pwd);

		mBtnSubmit = (Button)findViewById(R.id.btn_submit);
		mBtnSubmit.setOnClickListener(this);
		
		mBtnBack = (Button)findViewById(R.id.btn_back);
		mBtnBack.setOnClickListener(this);

		tipNew 	= (ImageView) findViewById(R.id.iv_tip_new_pwd);
		tipNew2 = (ImageView) findViewById(R.id.iv_tip_new_pwd2);
		tipOld 	= (ImageView) findViewById(R.id.iv_tip_old_pwd);

		newPwd = (EditText) findViewById(R.id.et_new_pwd);
		newPwd.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				tipNew.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s) || !CheckUtils.isCorrentPassword(s.toString())) {
					tipNew.setImageResource(R.drawable.ic_error);
				} else {
					tipNew.setImageResource(R.drawable.ic_correct);
				}
				String new2 = newPwd2.getText().toString();
				if (!TextUtils.isEmpty(new2) && !TextUtils.isEmpty(s)) {
					if(CheckUtils.isCorrentPassword(new2) && CheckUtils.isCorrentPassword(s.toString())
							&& new2.equals(s.toString()))
						tipNew2.setImageResource(R.drawable.ic_correct);
					else
						tipNew2.setImageResource(R.drawable.ic_error);
				}
				//changeBtnState();
			}
		});

		newPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
		newPwd2.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				tipNew2.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)
						|| !CheckUtils.isCorrentPassword(s.toString())
						|| !s.toString().equals(newPwd.getText().toString())) {
					tipNew2.setImageResource(R.drawable.ic_error);
				} else {
					tipNew2.setImageResource(R.drawable.ic_correct);
				}
				//changeBtnState();
			}
		});

		oldPwd = (EditText) findViewById(R.id.et_old_pwd);
		oldPwd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				tipOld.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)
						|| !CheckUtils.isCorrentPassword(s.toString())) {
					tipOld.setImageResource(R.drawable.ic_error);
				} else {
					tipOld.setImageResource(R.drawable.ic_correct);
				}
				//changeBtnState();
			}
		});
		
		mTextRecPerson = (TextView)findViewById(R.id.text_rec_person);
		mTextRecPerson.setText(App.getAgentId());

		//mProgress = (ProgressView) findViewById(R.id.progress);
		//mProgress.setText("正在修改密码…");
		
		//changeBtnState();
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		// 
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		// 调用垃圾回收器
		Interface.causeGC();
		
	}

	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		// 返回
		case R.id.btn_back:
			//goBackActivate();
			// 跳转到登录界面
			ActivityHelper.goLogin(ChangePwdActivity.this, "false");
			break;
			
		// 修改密码
		case R.id.btn_submit:
			handleChangePwd();
			break;
			
		default:
			break;
		}
	}

	// 回退到激活界面
	protected void goBackActivate() 
	{
		Intent intent = new Intent(ChangePwdActivity.this, ActivateActivity.class);
		startActivity(intent);
		finish();
	}

	// 修改用户密码
	private void handleChangePwd() 
	{
		if (TextUtils.isEmpty(oldPwd.getText().toString())) 
		{
			App.showAlert(this, "请输入原密码！", R.string.ok, null);
			oldPwd.requestFocus();
			tipOld.setVisibility(View.VISIBLE);
			tipOld.setImageResource(R.drawable.ic_error);
			return;
		}
		if (!CheckUtils.isCorrentPassword(oldPwd.getText().toString())) 
		{
			App.showAlert(this, "原密码位数有误！", R.string.ok, null);
			oldPwd.requestFocus();
			tipOld.setVisibility(View.VISIBLE);
			tipOld.setImageResource(R.drawable.ic_error);
			return;
		}
		if (TextUtils.isEmpty(newPwd.getText().toString())) 
		{
			App.showAlert(this, "请输入新密码！", R.string.ok, null);
			newPwd.requestFocus();
			tipNew.setVisibility(View.VISIBLE);
			tipNew.setImageResource(R.drawable.ic_error);
			return;
		}

		if (!CheckUtils.isCorrentPassword(newPwd.getText().toString())) 
		{
			App.showAlert(this, "新密码输入有误！", R.string.ok, null);
			newPwd.requestFocus();
			tipNew.setVisibility(View.VISIBLE);
			tipNew.setImageResource(R.drawable.ic_error);
			return;
		}

		if (TextUtils.isEmpty(newPwd2.getText().toString())) 
		{
			App.showAlert(this, "请重新输入新密码！", R.string.ok, null);
			newPwd2.requestFocus();
			tipNew2.setVisibility(View.VISIBLE);
			tipNew2.setImageResource(R.drawable.ic_error);
			return;
		}
		if (!newPwd.getText().toString().equals((newPwd2.getText().toString()))) 
		{
			App.showAlert(this, "您重复输入的新密码不一致，请重新输入！", R.string.ok, null);
			return;
		}
		if(Interface.isSimple(newPwd.getText().toString().trim()))
		{
			App.showAlert(this, "您输入的新密码过于简单，请重新输入！", R.string.ok, null);
			return;
		}
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(ChangePwdActivity.this, getString(R.string.network_exception));
			return;
		}

		/*
		mProgress.setState(ProgressView.STATE_LOADING);
		mBtnSubmit.setEnabled(false);
		*/
		Interface.attachProgressDialog(ChangePwdActivity.this, "修改登录密码", "正在修改登录密码...");

		mRequest = new AlterAgentPasswdRequest(App.getAgentId(), oldPwd.getText().toString(), newPwd.getText().toString());
		executeRequest(mHandler, MSG_CHANGE_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mRequest, new AlterRequestListener());
		/*
		new Thread() 
		{
			public void run() 
			{
				SystemClock.sleep(2000);
				mHandler.sendEmptyMessage(MSG_CHANGE_SUC);
			}
		}.start();
		*/
	}

	/*
	private void changeBtnState() 
	{
		if (TextUtils.isEmpty(oldPwd.getText().toString())
				|| !CheckUtils.isCorrentPassword(oldPwd.getText().toString())
				|| TextUtils.isEmpty(newPwd.getText().toString())
				|| !CheckUtils.isCorrentPassword(newPwd.getText().toString())
				|| TextUtils.isEmpty(newPwd2.getText().toString())
				|| !newPwd.getText().toString()
						.equals((newPwd2.getText().toString()))) {
			findViewById(R.id.btn_submit).setEnabled(false);
		} else {
			findViewById(R.id.btn_submit).setEnabled(true);
		}
	}
	*/

	public final class AlterRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException arg0) 
		{
			// 取消定时器
			cancelTimer();
			if(arg0.getCause() instanceof java.net.SocketTimeoutException) {
				mHandler.obtainMessage(MSG_CHANGE_FAIL, "服务请求超时, 请重试!").sendToTarget();
			} else {
				mHandler.sendEmptyMessage(MSG_CHANGE_FAIL);
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				Map<String, String> result = mRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mHandler.sendEmptyMessage(MSG_CHANGE_SUC);
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHANGE_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_CHANGE_FAIL);
			}
		}
	}
}
