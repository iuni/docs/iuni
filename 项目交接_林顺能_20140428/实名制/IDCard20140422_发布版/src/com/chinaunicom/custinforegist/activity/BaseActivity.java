package com.chinaunicom.custinforegist.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.MobileClientLogOffRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.octo.android.robospice.request.listener.RequestListener;

public class BaseActivity extends Activity implements VisibilityControl 
{
	private boolean 			_isVisiable;
	private Timer				mRequestTimer = null;
	
	private final int 			LOGOUT_TIMEOUT = 1000*60*60*6;
	//private final int 		LOGOUT_TIMEOUT = 1000*60;
	protected boolean 			isNeedCheckTimeout = true;
	
	protected OnClickListener 	hideSoftKeyBoardListener = new View.OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			DeviceUtils.hideSoftKeyboard(v);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//getWindow().setFlags(128, 128);
		
		IntentFilter filter = new IntentFilter("exit");
		registerReceiver(exit, filter);
	}
	
	@Override
	protected void onStart() 
	{
		super.onStart();
	}
	
	@Override
	protected void onStop() 
	{
		super.onStop();
	}
	
	protected void onDestroy() 
	{
		unregisterReceiver(exit);
		// 程序退出时，清空通知栏信息
		//Interface.cancelAllNotify(this);
		super.onDestroy();
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		_isVisiable = false;
		//Log.e("", "BaseActivity onPause()..." + isNeedCheckTimeout);
		if(isNeedCheckTimeout) 
		{
			App.setLastPauseTime(SystemClock.elapsedRealtime());
			//Log.e("", "BaseActivity setLastTime..." + SystemClock.elapsedRealtime());
		}
	}
	
	@Override
	protected void onResume() 
	{
		_isVisiable = true;
		long pauseTime = App.getLastPauseTime();
		//PLog.log("agent id:"+App.getAgentId() +" agent token:"+App.getAccessToken());
		/*
		if (isNeedCheckTimeout && ((pauseTime != 0 && System.currentTimeMillis() - pauseTime >= 1000 * 60 * 30) 
				|| TextUtils.isEmpty(App.getAgentId()) || TextUtils.isEmpty(App.getAccessToken()) )) {
			App.showToast("您登陆的代理商客户端系统已超时，请重新登陆！");
			PLog.log("need relogin");
			App.setAccessToken(null);
			sendBroadcast(new Intent("exit"));
			ActivityHelper.goLogin(this);
		}
		// entryTime = System.currentTimeMillis();
		*/
		//Log.e("", "BaseActivity onResume..." + isNeedCheckTimeout + "   " + App.getAccessToken());
		if (isNeedCheckTimeout 
				&& (App.getAccessToken() != null) 
				&&(pauseTime != 0 && SystemClock.elapsedRealtime()-pauseTime >= LOGOUT_TIMEOUT)) 
		{
			/*
			App.showAlert(
					BaseActivity.this, 
					"您登录的代理商客户端系统已超时, 请重新登录！", 
					R.string.button_ok, 
					new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							dialog.dismiss();
							logout();
							App.setAccessToken(null);
							ActivityHelper.goLogin(BaseActivity.this, "false");
						}
					});
			*/
			logout();
			App.setAccessToken(null);
			ActivityHelper.goLogin(BaseActivity.this, "false");
		}
		App.saveDisplaySize(this);
		super.onResume();
	}
	
	@Override
	public boolean isVisible() 
	{
		return _isVisiable;
	}

	protected void recycleDrawable(Drawable d) 
	{
		if (d != null && d instanceof BitmapDrawable) 
		{
			Bitmap bit = ((BitmapDrawable) d).getBitmap();
			if (!bit.isRecycled())
			{
				bit.recycle();
			}
		}
	}

	protected void logout() 
	{
		MobileClientLogOffRequest request = new MobileClientLogOffRequest(App.getAgentId());
		App.getSpiceManager().execute(request, null);
	}
	
	BroadcastReceiver exit = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			finish();
		}
	};
	
	// 请求服务
	public void executeRequest(final Handler handler, final int msgID, long timeout, 
		 BaseRequest request, RequestListener<String> requestListener) 
	{
		App.getSpiceManager().execute(request, requestListener);
		// 取消定时器
		if(mRequestTimer != null) 
		{
			mRequestTimer.cancel();
			mRequestTimer = null;
		}
		// 启动定时器
		mRequestTimer = new Timer();
		mRequestTimer.schedule(new TimerTask() 
		{
			@Override
			public void run() 
			{
				Log.e("", "请求超时!");
				handler.sendEmptyMessage(msgID);
				mRequestTimer = null;
			}
		}, timeout);
	}
	
	// 取消定时器
	public void cancelTimer() 
	{
		if(mRequestTimer != null) 
		{
			mRequestTimer.cancel();
			mRequestTimer = null;
		}
		// 复位心跳定时器
		App.resetHeartbeatTimer();
	}
	
	// 判断文字是否为空
	public boolean isNull(String instr) 
	{
		if(instr == null) 
		{
			return true;
		}
		if(instr.trim().equals("")) 
		{
			return true;
		}
		return false;
	}
}
