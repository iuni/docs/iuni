package com.chinaunicom.custinforegist.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.R;

// 帮助Activity
public class HelpActivity extends BaseActivity implements OnClickListener
{
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 初始化布局
		setContentView(R.layout.activity_help);
		initViews();
	}
	
	// 初始化布局
	private void initViews()
	{
		try 
		{
			// 返回
			findViewById(R.id.btn_back).setOnClickListener(this);
			// 帮助1
			findViewById(R.id.btn_help1).setOnClickListener(this);
			// 帮助2
			findViewById(R.id.btn_help2).setOnClickListener(this);
			// 帮助3
			findViewById(R.id.btn_help3).setOnClickListener(this);
			// 帮助4
			findViewById(R.id.btn_help4).setOnClickListener(this);
			// 帮助5
			findViewById(R.id.btn_help5).setOnClickListener(this);
			// 帮助6
			findViewById(R.id.btn_help6).setOnClickListener(this);
			// 帮助7
			findViewById(R.id.btn_help7).setOnClickListener(this);
			// 帮助8
			findViewById(R.id.btn_help8).setOnClickListener(this);
			// 帮助8
			findViewById(R.id.btn_help9).setOnClickListener(this);
			// 帮助8
			findViewById(R.id.btn_help10).setOnClickListener(this);
			// 帮助8
			findViewById(R.id.btn_help11).setOnClickListener(this);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onClick(View v)
	{
		int resID = v.getId();
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(resID, 1000)) 
		{
			return;
		}
		switch(resID)
		{
		// 返回
		case R.id.btn_back:
			finish();
			break;
			
		// 帮助
		case R.id.btn_help1:
		case R.id.btn_help2:
		case R.id.btn_help3:
		case R.id.btn_help4:
		case R.id.btn_help5:
		case R.id.btn_help6:
		case R.id.btn_help7:
		case R.id.btn_help8:
		case R.id.btn_help9:
		case R.id.btn_help10:
		case R.id.btn_help11:
			Intent intent = new Intent(HelpActivity.this, HelpDesActivity.class);
			if(R.id.btn_help1 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "01");
			}else if(R.id.btn_help2 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "02");
			}else if(R.id.btn_help3 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "03");
			}else if(R.id.btn_help4 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "04");
			}else if(R.id.btn_help5 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "05");
			}else if(R.id.btn_help6 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "06");
			}else if(R.id.btn_help7 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "07");
			}else if(R.id.btn_help8 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "08");
			}else if(R.id.btn_help9 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "09");
			}else if(R.id.btn_help10 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "10");
			}else if(R.id.btn_help11 == resID) {
				intent.putExtra(Interface.HELP_TYPE, "11");
			}
			// 启动帮助详情Activity
			startActivity(intent);
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		// 调用垃圾回收器
		Interface.causeGC();
	}
}
