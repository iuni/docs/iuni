package com.chinaunicom.custinforegist.activity.register;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.api.ApiClient;
import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.QueryEnrolmentRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.ui.dialog.ComDialog;
import com.chinaunicom.custinforegist.ui.view.DragListView;
import com.chinaunicom.custinforegist.ui.view.DragListView.OnRefreshLoadingMoreListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint({ "HandlerLeak", "SimpleDateFormat" })
public class QueryEnrolmentActivity extends BaseActivity implements OnClickListener, OnRefreshLoadingMoreListener
{
	private static final int 		MSG_QUERY_SUC 		= 0x01;
	private static final int 		MSG_QUERY_FAIL 		= 0x02;
	private static final int		MSG_QUERY_TIMEOUT	= 0x03;
	
	private static final String		USER_TYPE			= "user_type";
	private static final String		USER_SEQ 			= "user_seq";
	private static final String		USER_NUMBER 		= "user_number";
	private static final String		USER_TIME 			= "user_time";

	private EditText 				mEtStartTime, mEtEndTime;
	private ImageView 				mIvStartTime, mIvEndTime;
	private TextView				mTvCheckType;
	
	//{"0000", "0001", "0002", "0003", "0004"};
	//{"全部", "电子实名客户端返档", "代理商短信返档", "OTA返档", "PC返档"};
	private final String[] 			mStrCheckTypeKey 	= {"0000", "0001", "0003"};
	private final String[] 			mStrCheckTypeText 	= {"全部", "电子实名客户端返档", "OTA返档"};
	private int						mSelIndex 			= 0x00;
	private String					mStrFdChanned;
	
	private int 					mTimeFlag;
	private String 					mStrBeginTime;
	private String 					mStrEndTime;

	private QueryEnrolmentRequest 	queryEnrolmentRequest;
	private int 					mQueryStart 		= 1;
	private static final int 		mQueryCnt 			= 15;
	private int 					mQueryTotal 		= 0;

	private Semaphore				mSem 				= new Semaphore(0);
	private DragListView 			mLvRecord;
	private List<Map<String, String>> mListRec 			= new ArrayList<Map<String, String>>();
	
	private Handler 				mHandler 			= new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			mSem.release();
			
			switch (msg.what) 
			{
			case MSG_QUERY_TIMEOUT:
				App.showAlert(QueryEnrolmentActivity.this, "服务请求超时, 请重试！");
				break;
				
			case MSG_QUERY_SUC:
				String queryInfo = (String)msg.obj;
				fillListViewWithRecords(queryInfo);
				break;
				
			case MSG_QUERY_FAIL:
				// 调试使用
				//fillListViewWithRecords("");
				String desc = (String)msg.obj;
				if (TextUtils.isEmpty(desc) || "anyType{}".equals(desc)) 
				{
					//App.showToast("请求失败, 请重试!");
					App.showAlert(QueryEnrolmentActivity.this, "请求失败, 请重试！");
				} 
				else 
				{
					//App.showToast(desc + "");
					App.showAlert(QueryEnrolmentActivity.this, desc + "");
				}
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_query_enrolment);
		
		// 初始化控件
		initViews();
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		// 调用垃圾回收器
		Interface.causeGC();
	}

	@SuppressWarnings("deprecation")
	private void initViews() 
	{
		// 初始化时间
		Date date = Calendar.getInstance().getTime();
		String tempstr = String.format("%04d-%02d-%02d", date.getYear()+1900, date.getMonth()+1, date.getDate());
		mStrBeginTime = String.format("%04d%02d%02d", date.getYear()+1900, date.getMonth()+1, date.getDate());
		mStrEndTime = mStrBeginTime;
		
		// 开始时间
		initBeginTime(tempstr);
		// 结束时间
		initEndTime(tempstr);
		// 渠道类型
		initCheckType();
		
		// 返回
		findViewById(R.id.btn_back).setOnClickListener(this);
		// 查询
		Button btnSubmit = (Button)findViewById(R.id.btn_submit);
		btnSubmit.setOnClickListener(this);
		
		// 返档记录列表
		initRecordView();
	}
	
	// 初始化开始时间
	private void initBeginTime(String timeStr) 
	{
		mIvStartTime = (ImageView)findViewById(R.id.iv_tip_start_time);
		mIvStartTime.setVisibility(View.INVISIBLE);
		// 
		mEtStartTime = (EditText)findViewById(R.id.et_start_time);
		mEtStartTime.setInputType(InputType.TYPE_NULL);
		mEtStartTime.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mIvStartTime.setVisibility(View.VISIBLE);
				mIvStartTime.setImageResource(TextUtils.isEmpty(s)? R.drawable.ic_error:R.drawable.ic_correct);
			}
		});
		mEtStartTime.setText(timeStr);
		mEtStartTime.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				mTimeFlag = 0;
				showDatePickerDialog();
			}
		});
		mEtStartTime.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if (hasFocus == true) 
				{
					mTimeFlag = 0;
					hideIM(v);
					showDatePickerDialog();
				}
			}
		});
	}
	
	// 初始化结束时间
	private void initEndTime(String timeStr)  
	{
		mIvEndTime = (ImageView)findViewById(R.id.iv_tip_end_time);
		mIvEndTime.setVisibility(View.INVISIBLE);
		// 
		mEtEndTime = (EditText) findViewById(R.id.et_end_time);
		mEtEndTime.setInputType(InputType.TYPE_NULL);
		mEtEndTime.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mIvEndTime.setVisibility(View.VISIBLE);
				mIvEndTime.setImageResource(TextUtils.isEmpty(s)? R.drawable.ic_error:R.drawable.ic_correct);
			}
		});
		mEtEndTime.setText(timeStr);
		mEtEndTime.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				mTimeFlag = 1;
				showDatePickerDialog();
			}
		});
		mEtEndTime.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if (hasFocus == true) 
				{
					mTimeFlag = 1;
					hideIM(v);
					showDatePickerDialog();
				}
			}
		});
	}
	
	private void showDatePickerDialog() 
	{
		Calendar calendar = Calendar.getInstance();
		// 
		new DatePickerDialog(this, mDateSetListener, calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
	}
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() 
	{
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) 
		{
			String mm;
			String dd;
			
			if (monthOfYear <= 9) 
			{
				mm = "0" + (monthOfYear+1);
			} 
			else 
			{
				mm = String.valueOf(monthOfYear+1);
			}
			if (dayOfMonth <= 9) 
			{
				dd = "0" + dayOfMonth;
			} 
			else 
			{
				dd = String.valueOf(dayOfMonth);
			}
			if (mTimeFlag == 0) 
			{
				mEtStartTime.setText(String.valueOf(year) + "-" + mm + "-" + dd);
				mStrBeginTime = String.valueOf(year) + mm + dd;
			}
			else 
			{
				mEtEndTime.setText(String.valueOf(year) + "-" + mm + "-" + dd);
				mStrEndTime = String.valueOf(year) + mm + dd;
			}
		}
	};
	
	// 隐藏手机键盘
	private void hideIM(View edt) 
	{
		try 
		{
			InputMethodManager im = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
			IBinder windowToken = edt.getWindowToken();
			if (windowToken != null) 
			{
				im.hideSoftInputFromWindow(windowToken, 0);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 初始化渠道类型
	private void initCheckType() 
	{
		findViewById(R.id.lay_check_type).setOnClickListener(this);
		mTvCheckType = (TextView) findViewById(R.id.tv_check_type);
		mTvCheckType.setText(mStrCheckTypeText[0]);
	}
	
	// 初始化返档记录列表
	private void initRecordView() 
	{
		mLvRecord = (DragListView) findViewById(R.id.lv_record);
		mLvRecord.setOnRefreshListener(this);
	}
	
	// 下拉刷新执行
	public void onRefresh() 
	{
		// 清空计数信号量
		while(mSem.tryAcquire());
		// 
		if (mQueryStart <= 1) 
		{
			mQueryStart = 1;
			App.showAlert(this, "已是第一页！");
			mLvRecord.onRefreshComplete();
			return;
		}
		// 处理查询请求
		mQueryStart -= mQueryCnt;
		handlerQueryEnrolment();
		// 
		new Thread() 
		{
			public void run() 
			{
				try
				{
					mSem.acquire();
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							mLvRecord.onRefreshComplete();
							mLvRecord.onLoadMoreComplete(false);
						}
					});
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	// 点击加载更多
	public void onLoadMore() 
	{
		// 清空计数信号量
		while(mSem.tryAcquire());
		// 
		if (mQueryStart+mQueryCnt > mQueryTotal) 
		{
			App.showAlert(this, "已是最后一页！");
			mLvRecord.onLoadMoreComplete(true);
			return;
		}
		// 处理查询请求
		mQueryStart += mQueryCnt;
		handlerQueryEnrolment();
		// 
		new Thread() 
		{
			public void run() 
			{
				try
				{
					mSem.acquire();
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							if (mQueryStart+mQueryCnt > mQueryTotal) 
							{
								mLvRecord.onLoadMoreComplete(true);
							}
							else
							{
								mLvRecord.onLoadMoreComplete(false);
							}
						}
					});
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击多次
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		
		switch (v.getId()) 
		{
		case R.id.btn_back:
			finish();
			break;
			
		// 选择渠道类型
		case R.id.lay_check_type:
			procSelCheckType();
			break;
			
		// 查询
		case R.id.btn_submit:
			procQuery();
			break;
			
		default:
			break;
		}
	}
	
	// 选择渠道类型
	private void procSelCheckType() 
	{
		final ComDialog dialog = ActivityHelper.getPinterestDialog(this);
		dialog.setType();
		dialog.setTitle("渠道类型");
		dialog.setSingleChoiceItems(mStrCheckTypeText, mSelIndex, 
			new OnItemClickListener() 
			{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int which, long arg3) 
				{
					if (dialog != null) 
					{
						dialog.dismiss();
					}
					mSelIndex = which;
					mTvCheckType.setText(mStrCheckTypeText[which]);
				}
			});
		dialog.setNegativeButton(getString(R.string.cancel), null);
		dialog.setCancelable(true);
		dialog.show();
	}
	
	// 处理查询
	private void procQuery() 
	{
		String tempStart = mEtStartTime.getText().toString();
		if(isNull(tempStart)) 
		{
			App.showAlert(this, "请选择开始时间！");
			return;
		}
		String tempEnd = mEtEndTime.getText().toString();
		if(isNull(tempEnd)) 
		{
			App.showAlert(this, "请选择结束时间！");
			return;
		}
		// 检测开始时间是否小于结束时间
		int nResult = validateInputDate(tempStart, tempEnd);
		if (nResult == -1) 
		{
			App.showAlert(this, "您输入的时间格式不合规！");
			return;
		}
		else if (nResult == 1) 
		{
			App.showAlert(this, "请确认结束时间是否大于等于开始时间！");
			return;
		}
		else if (nResult == 2) 
		{
			App.showAlert(this, "请确认时间间隔范围在一个月以内！");
			return;
		}
		mStrFdChanned = mStrCheckTypeKey[mSelIndex];
		// 复位ListView
		mLvRecord.onLoadMoreComplete(false);// 20140421 aso
		mLvRecord.setVisibility(View.GONE);
		// 开始查询记录
		mQueryStart = 1;
		mQueryTotal = 0;
		//mListData.clear();
		handlerQueryEnrolment();
	}
	
	// 检查日期是否合法
	private int validateInputDate(String dateStr1, String dateStr2) 
	{
		try 
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = sdf.parse(dateStr1);
			Date date2 = sdf.parse(dateStr2);
			// date1 > date2
			if (date1.compareTo(date2) > 0) 
			{
				return 1;
			}
			// 间隔时间不能超过1个月
			double timeval = (date2.getTime()-date1.getTime())/1000/3600/24;
			if(timeval > 31.0) 
			{
				return 2;
			}
			return 0;
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
			return -1;
		}
	}
	
	// 处理查询请求
	private void handlerQueryEnrolment() 
	{
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(QueryEnrolmentActivity.this, getString(R.string.network_exception));
			return;
		}

		Interface.attachProgressDialog(this, "查询", "正在查询...");
		queryEnrolmentRequest = new QueryEnrolmentRequest(App.getAgentId(), mStrBeginTime, mStrEndTime, 
				mQueryStart, mQueryCnt, mStrFdChanned);
		executeRequest(mHandler, MSG_QUERY_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, queryEnrolmentRequest, new QueryEnrolmentRequestListener());
	}
	
	private class QueryEnrolmentRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if (e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_QUERY_FAIL, "服务请求超时, 请重试!").sendToTarget();
			}
			else 
			{
				mHandler.obtainMessage(MSG_QUERY_FAIL).sendToTarget();
				Log.e("error", "Exception  onRequestFailure");
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String, String> result = queryEnrolmentRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				System.out.println("tradeState :" + tradeState);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					String queryInfo = result.get("queryInfo");
					String queryString = DES.decryptDES(queryInfo, ApiClient.mEncryptKey);
					mQueryTotal = Integer.parseInt(DES.decryptDES(result.get("count"), ApiClient.mEncryptKey));
					mHandler.obtainMessage(MSG_QUERY_SUC, queryString).sendToTarget();
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_QUERY_FAIL, desc).sendToTarget();
					Log.e("error", "des :" + desc);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_QUERY_FAIL);
				Log.e("error", "Exception  onRequestSuccess");
			}
		}
	}
	
	private void fillListViewWithRecords(String queryInfo) 
	{
		try 
		{
			// queryInfo = "01,13011112221,2013-06-06 12:12:15|"
			// + "02,13011112222,2013-06-07 12:12:15|"
			// + "03,13011112223,2013-06-08 12:12:15|"
			// + "04,13011112224,2013-06-03 12:12:15|"
			// + "05,13011112225,2013-06-02 12:12:15|"
			// + "06,13011112221,2013-06-06 12:12:15|"
			// + "02,13011112222,2013-06-07 12:12:15|"
			// + "10,13011112224,2013-06-03 12:12:15";

			// 清除
			mListRec.clear();
			
			/*
			map.put(USER_SEQ, 		"序号");
			map.put(USER_NUMBER, 	"登记号码");
			map.put(USER_TIME, 		"登记时间");
			mListRec.add(map);
			*/
			// 添加
			String[] rows = null;
			if (queryInfo != null && !"".equalsIgnoreCase(queryInfo)) 
			{
				Log.v("", queryInfo);
				rows = queryInfo.split("\\|");
				for (int i = 0; i < rows.length; i++) 
				{
					String[] items = rows[i].split(",");
					// 暂时屏蔽短信返档和PC返档
					if(items[3].equals("0002") || items[3].equals("0004"))
					{
						continue;
					}
					Map<String, String> map = new HashMap<String, String>();
					map.put(USER_SEQ, 		items[0]);
					map.put(USER_NUMBER, 	items[1]);
					map.put(USER_TIME, 		items[2]);
					map.put(USER_TYPE, 		items[3]); // 渠道类型
					mListRec.add(map);
				}
			}
			/*
			// 
			for (int i=mQueryCnt-rows.length; i>0; i--) 
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put(USER_SEQ, 		USER_BLANK);
				map.put(USER_NUMBER, 	USER_BLANK);
				map.put(USER_TIME, 		USER_BLANK);
				mListRec.add(map);
			}
			*/
			//
			SimpleAdapter adapter = new QueryAdapter(QueryEnrolmentActivity.this, mListRec, R.layout.activity_query_enrolment_listitem, 
					new String[]{USER_NUMBER, USER_TIME}, 
					new int[]{R.id.tv_check_phone, R.id.tv_check_time});
			mLvRecord.setAdapter(adapter);
			mLvRecord.setVisibility(View.VISIBLE);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 返档记录适配器
	private class QueryAdapter extends SimpleAdapter
	{
		public QueryAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null) 
			{
				convertView = super.getView(position, convertView, parent);
			}
			// 返档类型
			ImageView imageView = (ImageView)convertView.findViewById(R.id.iv_check_type);
			TextView textView = (TextView)convertView.findViewById(R.id.tv_check_type);
			String strCheckType = mListRec.get(position).get(USER_TYPE);
			// 代理商短信返档
			if("0002".equals(strCheckType)) 
			{
				imageView.setImageResource(R.drawable.icon_wo_sms);
				textView.setText("代理商短信返档");
			}
			// OTA返档
			else if("0003".equals(strCheckType)) 
			{
				imageView.setImageResource(R.drawable.icon_wo_ota);
				textView.setText("OTA返档");
			}
			// PC返档
			else if("0004".equals(strCheckType)) 
			{
				imageView.setImageResource(R.drawable.icon_wo_pc);
				textView.setText("PC返档");
			}
			//电子实名客户端返档
			else 
			{
				imageView.setImageResource(R.drawable.icon_wo_phone);
				textView.setText("电子实名客户端返档");
			}
			((TextView)convertView.findViewById(R.id.tv_check_phone)).setText(mListRec.get(position).get(USER_NUMBER));
			((TextView)convertView.findViewById(R.id.tv_check_time)).setText(mListRec.get(position).get(USER_TIME));
			return convertView;
		}
	}
	
	class Record 
	{
		private String id;
		private String loginNo;
		private String time;
		
		public String getId() 
		{
			return id;
		}

		public void setId(String id) 
		{
			this.id = id;
		}

		public String getLoginNo() 
		{
			return loginNo;
		}

		public void setLoginNo(String loginNo) 
		{
			this.loginNo = loginNo;
		}

		public String getTime() 
		{
			return time;
		}

		public void setTime(String time) 
		{
			this.time = time;
		}
	}
}
