package com.chinaunicom.custinforegist.activity.login;

import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.api.model.Version;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.MobileClientLoginRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.kit.util.PLog;
import com.chinaunicom.custinforegist.ui.dialog.EnvSetDialog;
import com.chinaunicom.custinforegist.ui.view.SwitchView;
import com.chinaunicom.custinforegist.ui.view.SwitchView.OnCheckedChangeListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class LoginActivity extends BaseActivity implements OnClickListener, OnCheckedChangeListener
{
	private static final String			TAG								= "登录";
	//protected static final int 		MSG_GET_RANDOM_SUC 				= 0x01;
	//protected static final int 		MSG_GET_RANDOM_FAIL 			= 0x02;
	//protected static final int 		MSG_GET_RANDOM_TIMEOUT 			= 0x03;
	protected static final int 			MSG_LOGIN_SUC 					= 0x04;
	protected static final int 			MSG_LOGIN_FAIL 					= 0x05;
	protected static final int			MSG_LOGIN_TIMEOUT				= 0x06;
	
	private EditText 					mEtAgentNo;
	private ImageView 					mIvTipNo;
	private EditText					mEtAgentPswd;
	private ImageView					mIvTipPswd;
	//private EditText					mEtRandom;
	//private ImageView					mIvTipRandom;
	//private Button					mBtnGetRandom;					// 获取验证码
	//private LinearLayout				mLinearRandomTips;				// 验证码布局
	private SwitchView 					mSwitchView;
	
	private String 						mStrAgentID;					// 发展人编码
	private String 						mStrAgentPswd;					// 发展人密码
	//private String					mStrRandom;						// 验证码
	//private boolean					mIsGetRandom;					// 是否已经获取过验证码
	
	//private String 					mStrCommunicationID;
	private String[]					mStrLac = new String[1];
	private String[]					mStrCi = new String[1];
	
	private MobileClientLoginRequest 	mLoginRequest;
	private Version						mVersionInfo;
	//private GetLoginRandomRequest 	mLoginGetRandomRequest;
	//private LoginRequest 				mLoginRequest;
	
	/*
	// 验证码定时器
	private CountDownTimer mCountTimer = new CountDownTimer(180*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			mBtnGetRandom.setText(String.format("剩余%d秒", millisUntilFinished/1000));
		}
		
		@Override
		public void onFinish() 
		{
			mBtnGetRandom.setEnabled(true);
			mBtnGetRandom.setText("获取验证码");
			mIsGetRandom = false;
			// 隐藏验证码提示布局
			mLinearRandomTips.setVisibility(View.GONE);
		}
	};
	*/
	
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(android.os.Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what) 
			{
			/*
			// 获取验证码成功
			case MSG_GET_RANDOM_SUC:
				// 禁用获取验证码按钮
				mBtnGetRandom.setEnabled(false);
				mIsGetRandom = true;
				// 验证码输入框获得焦点
				mEtRandom.setText("");
				mEtRandom.requestFocus();
				// 显示验证码提示布局
				mLinearRandomTips.setVisibility(View.VISIBLE);
				// 启动倒计时
				startCountDownTimer();
				// 显示提示信息
				App.showAlert(LoginActivity.this, (String)msg.obj);
				break;
				
			// 获取验证码失败
			case MSG_GET_RANDOM_FAIL:
				String desc1 = (String) msg.obj;
				if (TextUtils.isEmpty(desc1) || "anyType{}".equals(desc1)) 
				{
					App.showAlert(LoginActivity.this, "获取验证码失败，请重新获取！");
				} 
				else 
				{
					App.showAlert(LoginActivity.this, desc1 + "");
				}
				break;
				
			// 获取验证码超时
			case MSG_GET_RANDOM_TIMEOUT:
				App.showAlert(LoginActivity.this, "获取验证码请求超时, 请重试！");
				break;
			*/
				
			// 登陆成功
			case MSG_LOGIN_SUC:
				// 取消倒计时
				//mCountTimer.cancel();
				//mIsGetRandom = false;
				// 
				App.setAgentId(mStrAgentID);
				App.setAgentPwd(mStrAgentPswd);
				// 
				if(mVersionInfo == null) 
				{
					App.setLastPauseTime(SystemClock.elapsedRealtime());
					ActivityHelper.goMain(LoginActivity.this);
					finish();
				}
				else
				{
					// 启动升级Activity
					Intent intent = new Intent(LoginActivity.this, UpdateActivity.class);
					intent.putExtra("updateType", "00");
					intent.putExtra("version", mVersionInfo);
					startActivity(intent);
					finish();
					
					/*
					String strLog = "请您升级电子实名制客户端!";
					if (!StringUtil.isEmptyOrNull(mVersionInfo.getUpdateLog())) 
					{
						strLog = mVersionInfo.getUpdateLog();
					}
					App.showAlert(LoginActivity.this, mVersionInfo.getUpdateTitle(), strLog, 
							"立即更新",
							new DialogInterface.OnClickListener() 
							{
								@Override
								public void onClick(DialogInterface dialog, int arg1) 
								{
									dialog.dismiss();
									App.setLastPauseTime(SystemClock.elapsedRealtime());
									// 启动升级Activity
									Intent intent = new Intent(LoginActivity.this, UpdateActivity.class);
									intent.putExtra("updateType", "00");
									intent.putExtra("version", mVersionInfo);
									startActivity(intent);
									finish();
								}
							}, 
							"稍后更新", 
							new DialogInterface.OnClickListener() 
							{
								@Override
								public void onClick(DialogInterface dialog, int which) 
								{
									dialog.dismiss();
									App.setLastPauseTime(SystemClock.elapsedRealtime());
									ActivityHelper.goMain(LoginActivity.this);
									finish();
								}
							});
					*/
				}
				break;
				
			// 登陆失败
			case MSG_LOGIN_FAIL:
				String desc = (String)msg.obj;
				if (TextUtils.isEmpty(desc) || "anyType{}".equals(desc)) 
				{
					App.showAlert(LoginActivity.this, "登录失败, 请重试!");
				}
				else 
				{
					App.showAlert(LoginActivity.this, desc + "");
				}
				break;
				
			// 登陆超时
			case MSG_LOGIN_TIMEOUT:
				App.showAlert(LoginActivity.this, "登录请求超时, 请重试!");
				break;
				
			default:
				break;
			}
		}
	};

	/*
	private BroadcastReceiver rec = new BroadcastReceiver() 
	{
		public void onReceive(android.content.Context context, Intent intent) 
		{
			finish();
		}
	};
	*/
	
	// 自动登录标志改变监听
	@Override
	public void onCheckedChanged(boolean isChecked) 
	{
		App.setAutoLoginFlag(!isChecked);
		//Toast.makeText(LoginActivity.this, String.valueOf(isChecked), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		setContentView(R.layout.activity_login);
		
		// 初始化控件
		initViews();
		//  
		isNeedCheckTimeout = false;
		
		// 第一次启动的时候判断是否自动登录
		String strFirstLaunch = getIntent().getStringExtra(Interface.FIRST_LAUNCH);
		if("true".equals(strFirstLaunch) && App.getAutoLoginFlag()) 
		{
			// 获取发展人工号和密码
			mStrAgentID = App.getAgentId();
			mStrAgentPswd = App.getAgentPwd();
			if(StringUtil.isEmptyOrNull(mStrAgentID) || StringUtil.isEmptyOrNull(mStrAgentPswd)) 
			{
				return;
			}
			// 
			PLog.e(TAG, "自动登录");
			Interface.attachProgressDialog(LoginActivity.this, "登录", "正在登录...");
			if(StringUtil.isEmptyOrNull(mStrLac[0]) || StringUtil.isEmptyOrNull(mStrCi[0])) 
			{
				DeviceUtils.getLacci(mStrLac, mStrCi);
			}
			// 检测网络
			if(!DeviceUtils.hasInternet()) 
			{
				App.showAlert(LoginActivity.this, getString(R.string.network_exception));
				return;
			}
			mLoginRequest = new MobileClientLoginRequest(mStrAgentID.trim(), mStrAgentPswd.trim(), "1", mStrLac[0], mStrCi[0]);
			executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mLoginRequest, new MobileClientLoginListener());
		}
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 发展人编码
		mIvTipNo 	= (ImageView) findViewById(R.id.iv_tip_no);
		mEtAgentNo 	= (EditText) findViewById(R.id.et_agent_no);
		mEtAgentNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvTipNo.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipNo.setVisibility(View.VISIBLE);
					if (s.length() < 10) 
					{
						mIvTipNo.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvTipNo.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		// 发展人密码
		mIvTipPswd = (ImageView) findViewById(R.id.iv_tip_pswd);
		mEtAgentPswd = (EditText) findViewById(R.id.et_pswd);
		mEtAgentPswd.setText("");
		mEtAgentPswd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvTipPswd.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipPswd.setVisibility(View.VISIBLE);
					if (s.length() != 6) 
					{
						mIvTipPswd.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvTipPswd.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		/*
		// 校验码
		mIvTipRandom = (ImageView) findViewById(R.id.iv_tip_random);
		mEtRandom = (EditText) findViewById(R.id.et_random);
		mEtRandom.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(isNull(s.toString())) 
				{
					mIvTipRandom.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipRandom.setVisibility(View.VISIBLE);
					if(s.length() != 6) 
					{
						mIvTipRandom.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipRandom.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		*/
		
		// 获取验证码
		//mBtnGetRandom = (Button) findViewById(R.id.btn_get_random);
		//mBtnGetRandom.setOnClickListener(this);
		// 验证码提示布局
		//mLinearRandomTips = (LinearLayout) findViewById(R.id.linear_random_tips);
		//mLinearRandomTips.setVisibility(View.GONE);
		// 登录
		Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(this);
		//mIsGetRandom = false;
		// 自动登录
		mSwitchView = (SwitchView) findViewById(R.id.switch_view);
		mSwitchView.setOnCheckedChangeListener(this);
		mSwitchView.setChecked(!App.getAutoLoginFlag());
		// 忘记密码
		TextView tvForgetPswd = (TextView)findViewById(R.id.tv_forget_pswd);
		tvForgetPswd.setOnClickListener(this);
		tvForgetPswd.setText(Html.fromHtml("<u>忘记密码</u>"));
		//tvForgetPswd.setTextColor(Color.parseColor("#0000FF"));
		// 激活
		TextView tvActivate = (TextView)findViewById(R.id.tv_first);
		tvActivate.setOnClickListener(this);
		tvActivate.setText(Html.fromHtml("<u>激活</u>"));
		
		/*
		findViewById(R.id.root).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.f_1).setOnClickListener(hideSoftKeyBoardListener);
		//findViewById(R.id.ly_auth_code_container).setOnClickListener(hideSoftKeyBoardListener);
		*/
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			sendBroadcast(new Intent("exit"));
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		// 设置初始发展人编码
		String strAgentID = App.getAgentId();
		if(!StringUtil.isEmptyOrNull(strAgentID)) 
		{
			mEtAgentNo.setText(strAgentID);
			mEtAgentNo.setSelection(strAgentID.length());
		}
		else
		{
			mEtAgentNo.setText("");
		}
		mEtAgentPswd.setText(App.getAgentPwd());
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		// 
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		
		// 取消倒计时
		//mCountTimer.cancel();
		// 卸载广播接收器
		//unregisterReceiver(rec);
	}
	
	/*
	// 启动定时器
	private void startCountDownTimer() 
	{
		mCountTimer.cancel();
		mCountTimer.start();
	}
	*/
	
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		/*
		// 获取验证码
		case R.id.btn_get_random:
			handleGetRandom();
			break;
		*/
		
		// 登陆
		case R.id.btn_login:
			handleLogin();
			break;
			
		// 忘记密码
		case R.id.tv_forget_pswd:
			startActivity(new Intent(LoginActivity.this, ForgetPwdActivity.class));
			break;
			
		// 第一次登录激活
		case R.id.tv_first:
			startActivity(new Intent(LoginActivity.this, ActivateActivityNew.class));
			break;
			
		/*
		// 修改密码
		case R.id.btn_change_pwd:
			if (mProgress.getVisibility() != View.VISIBLE) 
			{
				Intent intent = new Intent(LoginActivity.this, ChangePwdActivity2.class);
				startActivity(intent);
			}
			break;
		*/
			
		default:
			break;
		}
	}
	
	/*
	// 获取验证码
	private void handleGetRandom() 
	{
		// 校验发展人编码
		mStrAgentID = mEtAgentNo.getText().toString();
		if (TextUtils.isEmpty(mStrAgentID)) 
		{
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			mEtAgentNo.requestFocus();
			App.showAlert(LoginActivity.this, "请输入发展人编码！");
			return;
		}
		// 校验发展人密码
		mStrAgentPswd = mEtAgentPswd.getText().toString();
		if(TextUtils.isEmpty(mStrAgentPswd)) 
		{
			mIvTipPswd.setVisibility(View.VISIBLE);
			mIvTipPswd.setImageResource(R.drawable.ic_error);
			mEtAgentPswd.requestFocus();
			App.showAlert(LoginActivity.this, "请输入发展人密码！");
			return;
		}
		if(mStrAgentPswd.length() != 6) 
		{
			mIvTipPswd.setVisibility(View.VISIBLE);
			mIvTipPswd.setImageResource(R.drawable.ic_error);
			mEtAgentPswd.requestFocus();
			App.showAlert(LoginActivity.this, "请输入6位发展人密码！");
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(LoginActivity.this, getString(R.string.network_exception));
			return;
		}
		
		Interface.attachProgressDialog(LoginActivity.this, "登录", "正在获取验证码...");
		mLoginGetRandomRequest = new GetLoginRandomRequest(mStrAgentID, mStrAgentPswd);
		executeRequest(mHandler, MSG_GET_RANDOM_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mLoginGetRandomRequest, new GetLoginRandomListener());
	}
	
	public final class GetLoginRandomListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消计时器
			cancelTimer();
			if(e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_GET_RANDOM_FAIL, "获取验证码请求超时，请重试！").sendToTarget();
			} 
			else
			{
				mHandler.sendEmptyMessage(MSG_GET_RANDOM_FAIL);
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消计时器
				cancelTimer();
				Map<String, String> result = mLoginGetRandomRequest.getResult();
				String strTradeState = result.get(BaseRequest.TRADE_STATE);
				String strDescription = result.get(BaseRequest.DESCRIPTION);
				// 获取验证码成功
				if (BaseRequest.TRADE_STATE_OK.equals(strTradeState))
				{
					mStrCommunicationID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.obtainMessage(MSG_GET_RANDOM_SUC, strDescription).sendToTarget();
				} 
				// 获取验证码失败
				else 
				{
					mHandler.obtainMessage(MSG_GET_RANDOM_FAIL, strDescription).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_GET_RANDOM_FAIL);
			}
		}
	}
	*/
	
	// 处理登陆
	private void handleLogin() 
	{
		// 检测发展人编码
		mStrAgentID = mEtAgentNo.getText().toString();
		if (TextUtils.isEmpty(mStrAgentID)) 
		{
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			mEtAgentNo.requestFocus();
			App.showAlert(this, "请输入发展人编码！");
			return;
		}
		if("971538".equals(mStrAgentID)) 
		{
			EnvSetDialog dialog = new EnvSetDialog(this);
			dialog.show();
			return;
		}
		// 检测发展人密码
		mStrAgentPswd = mEtAgentPswd.getText().toString();
		if (isNull(mStrAgentPswd)) 
		{
			mIvTipPswd.setVisibility(View.VISIBLE);
			mIvTipPswd.setImageResource(R.drawable.ic_error);
			mEtAgentPswd.requestFocus();
			App.showAlert(this, "请输入发展人密码！");
			return;
		}
		if(mStrAgentPswd.length() != 6)
		{
			mIvTipPswd.setVisibility(View.VISIBLE);
			mIvTipPswd.setImageResource(R.drawable.ic_error);
			mEtAgentPswd.requestFocus();
			App.showAlert(this, "请输入6位发展人密码！");
			return;
		}
		
		/*
		// 是否已经获取过验证码
		if(mIsGetRandom == false) 
		{
			App.showAlert(this, "请获取验证码！");
			return;
		}
		// 检测验证码输入
		mStrRandom = mEtRandom.getText().toString();
		if (isNull(mStrRandom)) 
		{
			mIvTipRandom.setVisibility(View.VISIBLE);
			mIvTipRandom.setImageResource(R.drawable.ic_error);
			mEtRandom.requestFocus();
			App.showAlert(this, "请输入验证码！");
			return;
		}
		if(mStrRandom.length() != 6) 
		{
			mIvTipRandom.setVisibility(View.VISIBLE);
			mIvTipRandom.setImageResource(R.drawable.ic_error);
			mEtRandom.requestFocus();
			App.showAlert(this, "请输入6位验证码！");
			return;
		}
		*/
		
		// 检测网络
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(LoginActivity.this, getString(R.string.network_exception));
			return;
		}
		// 获取LAC和CI
		if(StringUtil.isEmptyOrNull(mStrLac[0]) || StringUtil.isEmptyOrNull(mStrCi[0])) 
		{
			DeviceUtils.getLacci(mStrLac, mStrCi);
		}
		
		/*
		Interface.attachProgressDialog(LoginActivity.this, "登录", "正在登录...");
		mLoginRequest = new LoginRequest(mStrCommunicationID, mStrAgentID, mStrAgentPswd, mLac, mCi, mStrRandom);
		executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mLoginRequest, new LoginListener());
		*/
		
		Interface.attachProgressDialog(LoginActivity.this, "登录", "正在登录...");
		mLoginRequest = new MobileClientLoginRequest(mStrAgentID.trim(), mStrAgentPswd.trim(), "1", mStrLac[0], mStrCi[0]);
		executeRequest(mHandler, MSG_LOGIN_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mLoginRequest, new MobileClientLoginListener());
	}
	
	// 登录事件回调
	public final class MobileClientLoginListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_LOGIN_FAIL, "登录请求超时, 请重试！").sendToTarget();
			}
			else 
			{
				mHandler.sendEmptyMessage(MSG_LOGIN_FAIL);
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String, String> result = mLoginRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 保存登录状态
					App.setAccessToken("login-success");
					// 保存密钥
					String key = result.get(BaseRequest.REGISTER_KEY);
					App.setDesKey(key);
					
					// 输入状态
					Interface.checkInputType(result);
					// FTP服务器参数
					Interface.checkFtpInfo(result);
					// 检测升级
					mVersionInfo = Interface.checkUpdate(result, TAG);
					// 菜单开关
					Interface.checkMenuType(result);
					// 证件类型开关
					Interface.checkCertType(result);
					// 检测公告
					Interface.checkMail(LoginActivity.this, result, false);
					mHandler.sendEmptyMessage(MSG_LOGIN_SUC);
				}
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_LOGIN_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_LOGIN_FAIL);
			}
		}
	}
}
