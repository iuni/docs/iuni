package com.chinaunicom.custinforegist.activity.main;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.register.IccidActivity;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.PLog;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.R;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

// 主界面
public class MainActivity extends BaseActivity implements OnClickListener
{
	private LinearLayout 			mLayPhoneRegister;		// 录入手机号返档
	private LinearLayout 			mLayIccidRegister;		// 扫描手机卡条形码返档
	private LinearLayout 			mLayQuery;				// 查询返档记录
	private LinearLayout			mLayMsg;				// 公告箱
	private LinearLayout			mLayHelp;				// 帮助
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mLayPhoneRegister = (LinearLayout)findViewById(R.id.btn_phone_register);
		mLayPhoneRegister.setOnClickListener(this);
		
		mLayIccidRegister = (LinearLayout)findViewById(R.id.btn_iccid_register);
		mLayIccidRegister.setOnClickListener(this);
		
		mLayQuery = (LinearLayout)findViewById(R.id.btn_query);
		mLayQuery.setOnClickListener(this);
		
		mLayMsg = (LinearLayout)findViewById(R.id.btn_msg);
		mLayMsg.setOnClickListener(this);
		
		mLayHelp = (LinearLayout)findViewById(R.id.btn_help);
		mLayHelp.setOnClickListener(this);
		
		findViewById(R.id.btn_modify_pswd).setOnClickListener(this);
		findViewById(R.id.btn_logoff).setOnClickListener(this);
		findViewById(R.id.mailbox).setOnClickListener(this);
		
		// 手机号返档
		if("1".equals(Interface.mStrMenuFlag.subSequence(0, 1))) 
		{
			mLayPhoneRegister.setVisibility(View.VISIBLE);
		}
		else
		{
			mLayPhoneRegister.setVisibility(View.GONE);
		}
		// ICCID返档
		if("1".equals(Interface.mStrMenuFlag.subSequence(1, 2)))
		{
			mLayIccidRegister.setVisibility(View.VISIBLE);
		}
		else
		{
			mLayIccidRegister.setVisibility(View.GONE);
		}
		// 返档记录查询
		if("1".equals(Interface.mStrMenuFlag.subSequence(2, 3)))
		{
			mLayQuery.setVisibility(View.VISIBLE);
		}
		else
		{
			mLayQuery.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		
		// 检查公告
		checkMail();
	}
	
	// 公告检测
	private void checkMail() 
	{
		if(Interface.mHasMail) 
		{
			PLog.e("", "has mails...");
			Interface.mHasMail = false;
			App.showAlert(
					MainActivity.this, 
					Interface.mStrMailTitle, 
					StringUtil.getStrByLen(Interface.mStrMailText, 20), 
					R.string.detail, 
					new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int arg1) 
						{
							dialog.dismiss();
							startActivity(new Intent(MainActivity.this, MsgActivity.class));
						}
					},
					R.string.skip, 
					null);
		}
	}
	
	// 监听事件
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		// 手机号码注册
		case R.id.btn_phone_register:
			ActivityHelper.goTelphone(MainActivity.this);
			break;
			
		// ICCID扫描注册
		case R.id.btn_iccid_register:
			Intent intent = new Intent(MainActivity.this, IccidActivity.class);
			startActivity(intent);
			break;
		
		// 历史记录查询
		case R.id.btn_query:
			ActivityHelper.goQueryEnrolment(MainActivity.this);
			break;
			
		// 公告箱
		case R.id.btn_msg:
		case R.id.mailbox:
			startActivity(new Intent(MainActivity.this, MsgActivity.class));
			break;

		// 帮助
		case R.id.btn_help:
			startActivity(new Intent(MainActivity.this, HelpActivity.class));
			break;
			
		// 修改密码
		case R.id.btn_modify_pswd:
			startActivity(new Intent(MainActivity.this, ChangePwdActivity2.class));
			break;
			
		// 注销
		case R.id.btn_logoff:
			App.showAlert(this, R.string.dialog_logout_message, 
					R.string.logout,
					new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
							logout();
							//new LogoutTask().execute();
							ActivityHelper.goLogin(MainActivity.this, "false");
							App.setAccessToken(null);
							finish();
						}}, 
					R.string.cancel, 
					null);
			break;

		default:
			break;
		}
	}
}
