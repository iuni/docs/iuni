package com.chinaunicom.custinforegist.activity;

import android.content.Context;
import android.content.Intent;

import com.chinaunicom.custinforegist.activity.login.ActivateActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.login.LoginActivity;
import com.chinaunicom.custinforegist.activity.login.WelcomeActivity;
import com.chinaunicom.custinforegist.activity.main.MainActivity;
import com.chinaunicom.custinforegist.activity.register.IccidActivity;
import com.chinaunicom.custinforegist.activity.register.PictureActivity;
import com.chinaunicom.custinforegist.activity.register.QueryEnrolmentActivity;
import com.chinaunicom.custinforegist.activity.register.TelphoneActivity;
import com.chinaunicom.custinforegist.ui.dialog.ComDialog;
import com.chinaunicom.custinforegist.R;
import com.ym.idcard.reg.CameraActitivy;

public class ActivityHelper 
{
	// 转到新特性介绍界面
	public static void goWelcome(Context context) 
	{
		Intent intent = new Intent(context, WelcomeActivity.class);
		context.startActivity(intent);
	}
	
	public static void goLogin(Context context, String launchFlag) 
	{
		context.sendBroadcast(new Intent("exit"));
		
		Intent intent = new Intent(context, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(Interface.FIRST_LAUNCH, launchFlag);
		context.startActivity(intent);
	}

	public static void goActivate(Context context) 
	{
		context.sendBroadcast(new Intent("exit"));
		Intent intent = new Intent(context, ActivateActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	public static ComDialog getPinterestDialog(Context context) 
	{
		return new ComDialog(context, R.style.dialog_pinterest);
	}

	public static ComDialog getPinterestDialogCancelable(Context context) 
	{
		ComDialog dialog = new ComDialog(context, R.style.dialog_pinterest);
		dialog.setCanceledOnTouchOutside(true);
		return dialog;
	}
	
	public static void goMain(Context context) 
	{
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(0x34c40000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	public static void goMain(Context context, boolean flag) 
	{
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(0x34c40000);
		context.startActivity(intent);
	}
	
	/*
	// 调转到登记界面
	public static void goTip(Context context) 
	{
		Intent intent = new Intent(context, TipActivity.class);
		intent.setFlags(0x34c40000);
		//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	*/

	// 调转到记录界面
	public static void goBarcodeRecog(Context context) 
	{
		Intent intent = new Intent(context, IccidActivity.class);
		intent.setFlags(0x34c40000);
		intent.putExtra("clear", true);
		context.startActivity(intent);
	}
	
	public static void goTelphone(Context context) 
	{
		Intent intent = new Intent(context, TelphoneActivity.class);
		intent.setFlags(0x34c40000);
		intent.putExtra("clear", true);
		context.startActivity(intent);
	}
	
	public static void goTelphone(Context context, boolean clear) 
	{
		Intent intent = new Intent(context, TelphoneActivity.class);
		intent.setFlags(0x34c40000);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("clear", clear);
		context.startActivity(intent);
	}
	
	public static Intent goPicture(Context context) 
	{
		Intent intent = new Intent(context, PictureActivity.class);
		//context.startActivity(intent);
		return intent;
	}

	// 跳转到代理商实名统计界面
	public static Intent goQueryEnrolment(Context context) 
	{
		Intent intent = new Intent(context, QueryEnrolmentActivity.class);
		context.startActivity(intent);
		return intent;
	}
	
	public static Intent getTakePictureIntent(Context context) 
	{
		Intent intent = new Intent(context, CameraActitivy.class);
		intent.putExtra("isMyCard", true);
		intent.putExtra("allow", true);
		// startActivityForResult(intent1, 105);
		return intent;
	}
}
