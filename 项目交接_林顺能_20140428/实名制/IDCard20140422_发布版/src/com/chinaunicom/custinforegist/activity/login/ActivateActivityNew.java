package com.chinaunicom.custinforegist.activity.login;

import java.util.Map;

import com.chinaunicom.custinforegist.api.robospice.AgentActivateRequest;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.GetIdentifyIDRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

import android.content.Context;
import android.os.Bundle;

// 激活(新流程)
public class ActivateActivityNew extends ForgetPwdActivity
{
	private Context 					mContext = this;
	private GetIdentifyIDRequest 		mGetIdentifyIDRequest;
	private AgentActivateRequest 		mAgentActivateRequest;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 初始化布局和控件
		setContentView(R.layout.activity_activate_new);
		initViews();
	}
	
	// 找回密码
	@Override
	protected void handleGetIdentifyID() 
	{
		/*
		if(needGetCode) 
		{
			App.showToast("请先获取验证码!");
			btnCode.setEnabled(true);
			btnSubmit.setEnabled(false);
			return;
		}
		identifyId = etCode.getText().toString();
		if (TextUtils.isEmpty(identifyId)) 
		{
			App.showToast("请输入验证码!");
			etCode.requestFocus();
			return;
		}
		*/
		
		mStrAgentID = mEtNo.getText().toString();
		if (isNull(mStrAgentID)) 
		{
			//App.showToast("请输入发展人编码！");
			App.showAlert(this, "请输入发展人编码！", R.string.ok, null);
			mEtNo.requestFocus();
			return;
		}
		if(!DeviceUtils.hasInternet()) 
		{
			//App.showToast(R.string.network_exception);
			App.showAlert(this, R.string.network_exception, R.string.ok, null);
			return;
		}
		Interface.attachProgressDialog(mContext, "获取校验码", "正在获取校验码...");
		mGetIdentifyIDRequest = new GetIdentifyIDRequest(mStrAgentID, null);
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mGetIdentifyIDRequest, new ActiavteGetIdentifyIDListener());
	}

	public final class ActiavteGetIdentifyIDListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, "服务请求超时, 请重试！").sendToTarget();
			} else {
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mGetIdentifyIDRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					//String key = result.get("registerKey");
					//App.setAccessToken(key);
					mStrCommunicaID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_SUC, desc).sendToTarget();
				} 
				else 
				{
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}
	}
	
	// 修改密码
	@Override
	protected void handleModifyPswd()
	{
		// 检测发展人编码
		mStrAgentID = mEtNo.getText().toString();
		if (isNull(mStrAgentID)) 
		{
			//App.showToast("请输入验证码！");
			App.showAlert(this, "请输入发展人编码！");
			return;
		}
		// 检测校验码
		mStrIdentifyID = mEtCode.getText().toString();
		if (isNull(mStrIdentifyID)) 
		{
			//App.showToast("请输入验证码！");
			App.showAlert(this, "请输入验证码！");
			return;
		}
		if (mStrIdentifyID.length() != 6) 
		{
			//App.showToast("请正确输入验证码！");
			App.showAlert(this, "请正确输入验证码！");
			return;
		}
		// 检测密码
		mStrPswd = mEtPswd.getText().toString();
		if (isNull(mStrPswd)) 
		{
			//App.showToast("请输入新密码！");
			App.showAlert(this, "请输入新密码！");
			return;
		}
		if (mStrPswd.length() != 6) 
		{
			//App.showToast("请正确输入新密码！");
			App.showAlert(this, "请正确输入新密码！");
			return;
		}
		// 检测确认密码
		mStrNewPswd = mEtNewPswd.getText().toString();
		if (isNull(mStrNewPswd)) 
		{
			//App.showToast("请输入确认密码！");
			App.showAlert(this, "请输入确认密码！");
			return;
		}
		if (mStrNewPswd.length() != 6) 
		{
			//App.showToast("请正确输入确认密码！");
			App.showAlert(this, "请正确输入确认密码！");
			return;
		}
		// 检测新密码和确认密码的一致性
		if(mStrPswd.equals(mStrNewPswd) == false)
		{
			//App.showToast("新密码与确认密码不一致, 请重新输入！");
			App.showAlert(this, "新密码与确认密码不一致, 请重新输入！");
			return;
		}
		if(Interface.isSimple(mStrNewPswd)) 
		{
			App.showAlert(this, "您输入的新密码过于简单，请重新输入！");
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			//App.showToast(R.string.network_exception);
			App.showAlert(this, R.string.network_exception, R.string.ok, null);
			return;
		}
		
		// 取消倒计时
		//mCountDownTimer.cancel();
		// 
		Interface.attachProgressDialog(mContext, "修改密码", "正在修改登录密码...");
		mAgentActivateRequest = new AgentActivateRequest(mStrCommunicaID, mStrAgentID, mStrIdentifyID, mStrPswd);
		executeRequest(mHandler, MSG_MODIFY_PSWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mAgentActivateRequest, new ModifyPswdListener());
	}
	
	// 修改密码返回
	public final class ModifyPswdListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, "服务请求超时, 请重试！").sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mAgentActivateRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mHandler.obtainMessage(MSG_MODIFY_PSWD_SUC).sendToTarget();
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}
	}
}
