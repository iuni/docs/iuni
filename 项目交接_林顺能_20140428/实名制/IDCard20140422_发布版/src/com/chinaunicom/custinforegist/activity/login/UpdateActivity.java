package com.chinaunicom.custinforegist.activity.login;

import java.io.File;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RemoteViews;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.api.ApiClient;
import com.chinaunicom.custinforegist.api.model.Version;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.base.Constants;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.kit.util.HttpUtils.OnDownloadListener;
import com.chinaunicom.custinforegist.service.DownloadTask;
import com.chinaunicom.custinforegist.service.DownloadUtils;
import com.chinaunicom.custinforegist.service.DownloadUtils.ServiceToken;
import com.chinaunicom.custinforegist.ui.dialog.UpdateDialog;

// 升级Activity
public class UpdateActivity extends BaseActivity implements OnClickListener
{
	private static final String		TAG							= "升级";
	private static final String 	KEY_ISDOWNLOADSUCCESS 		= "isDownloadSuccess";
	private static final String 	KEY_BREAKPOINT 				= "breakPoint";
	private static final String 	KEY_LASTVERSION 			= "lastVersion";
	
	private NotificationManager 	mNotifyMgr;
	private Notification 			mNotice;
	private RemoteViews 			mLoadingView;
	private long 					mLastNotify;
	
	private ServiceToken 			mServiceToken;
	private String					mStrUpdateType;
	private Version					mVersionInfo;
	
	private Handler mHandler = new Handler() 
	{
		@SuppressWarnings("deprecation")
		public void handleMessage(android.os.Message msg) 
		{
			switch (msg.what) 
			{
			case 1:
				long copyLen = msg.getData().getLong("copyLen");
				long fileLen = msg.getData().getLong("fileLen");
				// 
				mLoadingView.setTextViewText(R.id.title, getString(R.string.notify_update_title, DeviceUtils.percent(copyLen, fileLen)));
				mNotifyMgr.notify(-999, mNotice);
				mLastNotify = System.currentTimeMillis();
				// 
				if(copyLen == fileLen)
				{
					updateSharedPreferance(fileLen, true, mVersionInfo.getVersionCode());
				}
				else
				{
					updateSharedPreferance(copyLen, false, mVersionInfo.getVersionCode());
				}
				break;
				
			case 2:
				mNotifyMgr.cancel(-999);
				break;
				
			case 3:
				mNotifyMgr.cancel(-999);
				Notification notify = new Notification(R.drawable.icon_main, getString(R.string.notify_update_fail_ticker),
						System.currentTimeMillis());
				notify.flags |= Notification.FLAG_AUTO_CANCEL;
				notify.setLatestEventInfo(App.context(), getString(R.string.notify_update_fail_title),
						//getString(R.string.notify_update_fail_content),
						(String)msg.obj,
						PendingIntent.getActivity(App.context(), 0, new Intent(), 0));
				mNotifyMgr.notify(-998, notify);
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update);
		
		// 绑定下载服务
		mServiceToken = DownloadUtils.bindToService(this);
		// 获取升级类型
		mStrUpdateType = getIntent().getStringExtra("updateType");
		// 获取升级
		mVersionInfo = (Version) getIntent().getSerializableExtra("version");
		// 处理升级
		handleUpdate();
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		// 与绑定服务解绑
		DownloadUtils.unbindFromService(mServiceToken);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	// 处理升级
	private void handleUpdate() 
	{
		String strLog = "请您升级电子实名制客户端!";
		if (!StringUtil.isEmptyOrNull(mVersionInfo.getUpdateLog())) 
		{
			strLog = mVersionInfo.getUpdateLog();
		}
		UpdateDialog dialog = new UpdateDialog(this);
		dialog.setTitle(mVersionInfo.getUpdateTitle());
		dialog.setMessage(strLog);
		dialog.setPositiveButton("立即更新", new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				handleDownloadApk(mVersionInfo);
				finish();
				// 
				if("00".equals(mStrUpdateType)) 
				{
					ActivityHelper.goMain(UpdateActivity.this);
				}
				
			}
		});
		dialog.setNegativeButton("稍后更新", new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				finish();
				// 
				if("00".equals(mStrUpdateType)) 
				{
					ActivityHelper.goMain(UpdateActivity.this);
				}
				
			}
		});
		dialog.show();
	}
	
	@SuppressWarnings("deprecation")
	private void handleDownloadApk(final Version version) 
	{
		mNotifyMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotice = new Notification(R.drawable.icon_main, getString(R.string.notify_update_ticker), System.currentTimeMillis());
		mLoadingView = new RemoteViews(getPackageName(), R.layout.notify_update_download);
		mNotice.flags |= Notification.FLAG_ONGOING_EVENT;
		mNotice.contentView = mLoadingView;
		mNotice.contentIntent = PendingIntent.getActivity(this, 0, new Intent(), 0);

		/*
		// 判断下载文件夹有没有存在
		File downloadDir = new File(Constants.DOWNLOAD_DIR);
		if(downloadDir.exists() == false) 
		{
			Log.e("", "downloadPath dir no exist!");
			downloadDir.mkdirs();
		}
		else 
		{
			Log.e("", "downloadPath dir exists!");
		}
		*/
		
		SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
		// 是否已经下载成功
		boolean isDownloadSuccess = sp.getBoolean(KEY_ISDOWNLOADSUCCESS, true);
		// 断点的长度
		long breakPoint = sp.getLong(KEY_BREAKPOINT, 0L);
		// 当前下载前的版本
		String lastVersion = sp.getString(KEY_LASTVERSION, "0.0");

		Log.e(TAG, "isDownloadSuccess: " + isDownloadSuccess);
		Log.e(TAG, "breakPoint: " + breakPoint);
		Log.e(TAG, "lastversion: " + lastVersion);
		Log.e(TAG, "currentversion: " + mVersionInfo.getVersionCode());
		
		String downloadPath = Constants.DOWNLOAD_DIR + version.getVersionName() + ".apk";
		Log.e("", "downloadPath = " + downloadPath);
		// 
		DownloadTask task = new DownloadTask();
		// 本地下载路径
		task.setLocalPath(downloadPath);
		/*
		task.setFtpPath(version.getFtpPath());
		task.setFtpIp(ApiClient.FTP_URL);
		task.setFtpPort(Integer.valueOf(ApiClient.FTP_PORT));
		task.setFtpUser(ApiClient.FTP_USER_NAME);
		task.setFtpPwd(ApiClient.FTP_USER_PWD);
		*/
		// 远程url
		String url = "http://" + ApiClient.FTP_URL + ":" + ApiClient.FTP_PORT + "/" + version.getFtpPath();
		task.setUrl(url);
		Log.e(TAG, "url =  " + url);
		// 断点
		// 版本不一样，重新下载
		if(lastVersion.equals(mVersionInfo.getVersionCode()) == false) 
		{
			task.setBreakPoint(0);
		}
		else
		{
			// 第一次下载
			if(isDownloadSuccess)
			{
				task.setBreakPoint(0);
			}
			// 断点下载
			else
			{
				task.setBreakPoint(breakPoint);
			}
		}
		Log.e(TAG, "breakpoint2 =  " + task.getFileBreakPoint());
		
		// 下载监听
		task.setOnDownloadListener(new OnDownloadListener() 
		{
			@Override
			public void onError(String errorStr) 
			{
				mHandler.obtainMessage(3, errorStr).sendToTarget();
			}

			@Override
			public void onDownload(int readLen, long copyLen, long fileLen) 
			{
				if (System.currentTimeMillis() - mLastNotify >= 1000 || copyLen == fileLen) 
				{
					Message message = mHandler.obtainMessage(1);
					Bundle data = new Bundle();
					data.putLong("copyLen", copyLen);
					data.putLong("fileLen", fileLen);
					message.setData(data);
					message.sendToTarget();
				}
				if (copyLen == fileLen) 
				{
					mHandler.sendEmptyMessage(2);
					File file = new File(Constants.DOWNLOAD_DIR + version.getVersionName() + ".apk");
					Log.e(TAG, "将要安装的apk: " + file.getAbsolutePath());
					Log.e(TAG, "文件长度: " + file.length());
					DeviceUtils.installAPK(UpdateActivity.this, file);
				}
			}
		});
		DownloadUtils.addTask(task);
	}
	
	// 更新SP的状态
	private void updateSharedPreferance(long breakpoint, boolean status, String version)
	{
		Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
		editor.putLong(KEY_BREAKPOINT, breakpoint);
		editor.putBoolean(KEY_ISDOWNLOADSUCCESS, status);
		editor.putString(KEY_LASTVERSION, version);
		editor.commit();
	}
}
