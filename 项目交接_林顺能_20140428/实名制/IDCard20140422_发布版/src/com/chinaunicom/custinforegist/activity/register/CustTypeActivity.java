package com.chinaunicom.custinforegist.activity.register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.VisibilityControl;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.main.HelpDesActivity;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.ui.dialog.ComDialog;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class CustTypeActivity extends BaseActivity implements OnClickListener
{
	private TextView 		mTvTitle;
	private TextView 		mTvCurType;
	private IdentityCard 	mCard;		 		// 身份证信息
	private String 			mStrTypeFlag;		// 类型标识，用来判断标题显示(ICCID, 手机号)
	
	private String[]		mStrIDNames;		// 用户证件类型名称
	private String[]		mStrIDValues;		// 用户证件类型键值
	
	@Override
	protected void onCreate(Bundle bundle) 
	{ 
		super.onCreate(bundle);
		setContentView(R.layout.register_phone_step2);
		
		Bundle data = getIntent().getExtras();
		mCard = data.getParcelable("card");
		mStrTypeFlag = getIntent().getStringExtra("typeFlag");
		
		// 筛选有效的证件类型
		initCustType();
		// 初始化控件
		initViews();
		// 设置初始证件类型
		setType(mCard.getTypeValue());
	}
	
	// 筛选有效的证件类型
	private void initCustType()
	{
		String[] names  = getResources().getStringArray(R.array.type_card_name);
		String[] values = getResources().getStringArray(R.array.type_card_value);
		int count = 0;
		
		// 计算有效的证件类型
		for(int i=0; i<Interface.mStrCertifyFlag.length(); i++)
		{
			// 当前证件类型有效
			if("1".equals(Interface.mStrCertifyFlag.substring(i, i+1)))
			{
				count++;
			}
		}
		// 
		mStrIDNames  = new String[count];
		mStrIDValues = new String[count];
		count = 0;
		// 
		for(int i=0; i<Interface.mStrCertifyFlag.length(); i++)
		{
			// 当前证件类型有效
			if("1".equals(Interface.mStrCertifyFlag.substring(i, i+1)))
			{
				mStrIDNames[count] = names[i];
				mStrIDValues[count++] = values[i];
			}
		}
	}

	// 初始化控件
	private void initViews() 
	{
		findViewById(R.id.btn_back).setOnClickListener(this);
		findViewById(R.id.btn_help).setOnClickListener(this);
		
		mTvTitle = (TextView) findViewById(R.id.tvTitle);
		mTvTitle.setText(mStrTypeFlag.equals("iccid")? getString(R.string.register_iccid):getString(R.string.register_phone));
		
		findViewById(R.id.ly_type).setOnClickListener(this);
		mTvCurType = (TextView) findViewById(R.id.tv_cur_type);
		
		findViewById(R.id.btn_nextStep).setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
			case R.id.btn_back:
				finish();
				break;
				
			case R.id.ly_type:
				handleTypeSelect();
				break;
				
			case R.id.btn_help:
				goHelp();
				break;
				
			case R.id.btn_nextStep:
				goPicture();
				break;
				
			default:
				break;
		}
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		// 调用垃圾回收器
		Interface.causeGC();
	}

	// 跳转到身份证信息界面
	private void goPicture() 
	{
		Intent intent = getIntent();
		intent.setClass(this, PictureActivity.class);
		intent.putExtra("card", mCard);
		startActivity(intent);
	}

	private void goHelp()
	{
		Intent intent = new Intent(this, HelpDesActivity.class);
		intent.putExtra(Interface.HELP_TYPE, "f2");
		startActivity(intent);
		//App.showAlert(this, R.string.app_help_register_2, R.string.ok, null);
	}

	// 选择身份证类型
	private void handleTypeSelect() 
	{
		int index = 0;

		for (int i=0; i<mStrIDValues.length; i++) 
		{
			if (mCard.getTypeValue().equals(mStrIDValues[i])) 
			{
				index = i;
				break;
			}
		}

		if ((this instanceof VisibilityControl) && ((VisibilityControl) this).isVisible()) 
		{
			final ComDialog dialog = ActivityHelper.getPinterestDialog(this);
			dialog.setType();
			dialog.setTitle(R.string.type_select);
			dialog.setSingleChoiceItems(mStrIDNames, index, 
				new OnItemClickListener() 
				{
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int which, long arg3) 
					{
						if (dialog != null) 
						{
							dialog.dismiss();
						}
						mTvCurType.setText(mStrIDNames[which]);
						setType(mStrIDValues[which]);
					}
				});
			dialog.setNegativeButton(getString(R.string.cancel), null);
			dialog.setCancelable(true);
			dialog.show();
		}
	}
	
	private void setType(String type) 
	{
		try
		{
			mCard.setTypeValue(type);

			for (int i=0; i<mStrIDValues.length; i++) 
			{
				if (type.equals(mStrIDValues[i]))
				{
					mCard.setTypeName(mStrIDNames[i]);
					mTvCurType.setText(mStrIDNames[i]);
					break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
