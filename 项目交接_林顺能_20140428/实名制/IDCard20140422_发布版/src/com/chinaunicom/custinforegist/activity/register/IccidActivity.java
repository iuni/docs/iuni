package com.chinaunicom.custinforegist.activity.register;

import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.main.HelpDesActivity;
import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.TelCheckRequest;
import com.chinaunicom.custinforegist.api.robospice.TelGetRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.UIHandler;

public class IccidActivity extends BaseActivity implements OnClickListener
{
	private final String			TAG								= "扫描iccid返档";
	private final int				BARCODE_RECOGNISE				= 0xF001;
	private final int				BARCODE_RECOGNISE_FAIL			= 0xF002;
	
	private final int				MSG_GET_TELNO_SUCC				= 0x01;
	private final int				MSG_GET_TELNO_FAIL				= 0x02;
	private final int				MSG_GET_TELNO_TIMEOUT			= 0x03;
	
	private final int 				MSG_CHECK_TEL_SUC 				= 0x05;
	//private final int 			MSG_CHECK_SUC_OLD 				= 0x06;
	private final int 				MSG_CHECK_TEL_FAIL 				= 0x07;
	private final int				MSG_CHECK_TEL_TIMEOUT			= 0x08;
	
	private ImageView				mIvStatus;
	private ImageView				mIvIccidTip;					// ICCID输入提示
	private EditText				mEtIccid;						// ICCID输入框
	private ImageView				mIvTelTip;						// 手机号码尾4位
	private EditText				mEtTel;							// 手机号码尾4位
	
	private TelGetRequest			mTelGetRequest					= null;
	private TelCheckRequest			mTelCheckRequest				= null;
	
	private String 					mStrCommunicaId;
	private String 					mStrUploadType;
	private String 					mStrBarCode;
	private String					mStrTelLast4;
	private String					mStrTel;						// 获取到的手机号码
	
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message)
		{
			Interface.detachProgressDialog();
			switch(message.what)
			{
				case BARCODE_RECOGNISE_FAIL:
					App.showAlert(IccidActivity.this, "手机卡条形码扫描失败，请重新扫描或选择手动输入！");
					break;
					
				// 获取号码成功，进行号码校验
				case MSG_GET_TELNO_SUCC:
					Interface.attachProgressDialog(IccidActivity.this, "提示信息", "正在校验号码...");
					mTelCheckRequest = new TelCheckRequest(App.getAgentId(), mStrTel, App.getDesKey(), "02");
					executeRequest(mHandler, MSG_CHECK_TEL_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mTelCheckRequest, new TelCheckRequestListener());
					break;
					
				// 获取手机号码失败
				case MSG_GET_TELNO_FAIL:
					String desc = (String)message.obj;
					if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc))
					{
						//App.showAlert(BarCodeRecogActivity.this, "服务器响应超时！该手机卡未验证通过，请勿销售。（序号：1997）");
						App.showAlert(IccidActivity.this, "服务器响应超时！该手机卡未验证通过，请勿销售。(序号：19003）");
					} 
					else 
					{
						App.showAlert(IccidActivity.this, desc+"");
					}
					break;
					
				// 获取手机号码超时
				case MSG_GET_TELNO_TIMEOUT:
					//App.showAlert(BarCodeRecogActivity.this, "服务器响应超时！该手机卡未验证通过，请勿销售（序号：1997）");
					App.showAlert(IccidActivity.this, "服务器响应超时！该手机卡未验证通过，请勿销售。(序号：19003）");
					break;
					
				// 校验手机号码成功
				case MSG_CHECK_TEL_SUC:
					Intent intent = null;
					IdentityCard card = new IdentityCard();
					if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
					{
						intent = new Intent(IccidActivity.this, PictureActivity.class);
						card.setTypeValue(getResources().getStringArray(R.array.type_card_value)[0]);
						card.setTypeName(getResources().getStringArray(R.array.type_card_name)[0]);
					}
					else
					{
						intent = new Intent(IccidActivity.this, CustTypeActivity.class);
					}
					card.setTel(mStrTel);
					intent.putExtra("card", card);
					intent.putExtra("communicaId", mStrCommunicaId);
					intent.putExtra("uploadType", mStrUploadType);
					intent.putExtra("typeFlag", "iccid");
					startActivity(intent);
					break;
					
				/*
				case MSG_CHECK_SUC_OLD:
					App.showAlert(BarCodeRecogActivity.this,
							 UIHandler.mTelStr +"用户为老用户，请用户登录中国联通网上营业厅或手机营业厅或发送彩信至10010进行补登记。");
					break;
				*/
				
				// 校验手机号码失败
				case MSG_CHECK_TEL_FAIL:
					desc = (String)message.obj;
					if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc))
					{
						//App.showAlert(BarCodeRecogActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。（序号：2997）");
						App.showAlert(IccidActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。(序号：29003）");
					} 
					else 
					{
						App.showAlert(IccidActivity.this, desc+"");
					}
					break;
					
				// 校验手机号码超时
				case MSG_CHECK_TEL_TIMEOUT:
					//App.showAlert(BarCodeRecogActivity.this, "服务器响应超时！该手机号验证通过，请勿销售（序号：2997）");
					App.showAlert(IccidActivity.this, "服务器响应超时！该手机号未验证通过，请勿销售。(序号：29003）");
					break;
					
				default:
					break;
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_iccid_step1);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mIvStatus = (ImageView) findViewById(R.id.iv_status);
		// 只允许身份证
		if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
		{
			mIvStatus.setImageResource(R.drawable.image_step1_new);
		}
		else
		{
			mIvStatus.setImageResource(R.drawable.image_step1);
		}
		
		// ICCID号码
		mIvIccidTip = (ImageView) findViewById(R.id.iv_tip_iccid);
		mEtIccid = (EditText) findViewById(R.id.et_iccid);
		mEtIccid.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if(Interface.isSymbol(s))
				{
					s = s.subSequence(0, s.length()-1);
					mEtIccid.setText(s);
					mEtIccid.setSelection(s.length());
					return;
				}
				mIvIccidTip.setVisibility(View.VISIBLE);
				if(TextUtils.isEmpty(s)  || s.length() != 19 )
				{
					mIvIccidTip.setImageResource(R.drawable.ic_error);
				}
				else 
				{
					mIvIccidTip.setImageResource(R.drawable.ic_correct);
				}
				super.onTextChanged(s, start, before, count);
			}
		});
		
		// 手机号码尾4位
		mIvTelTip = (ImageView) findViewById(R.id.iv_tip_tel);
		mEtTel = (EditText) findViewById(R.id.et_tel);
		mEtTel.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if(Interface.isSymbol(s))
				{
					s = s.subSequence(0, s.length()-1);
					mEtTel.setText(s);
					mEtTel.setSelection(s.length());
					return;
				}
				mIvTelTip.setVisibility(View.VISIBLE);
				if(TextUtils.isEmpty(s)  || s.length() != 4)
				{
					mIvTelTip.setImageResource(R.drawable.ic_error);
				}
				else 
				{
					mIvTelTip.setImageResource(R.drawable.ic_correct);
				}
				super.onTextChanged(s, start, before, count);
			}
		});
		
		// 返回按钮
		findViewById(R.id.btn_back).setOnClickListener(this);
		// 帮助按钮
		findViewById(R.id.btn_help).setOnClickListener(this);
		// 一维码识别按钮
		findViewById(R.id.btn_scan_iccid).setOnClickListener(this);
		// 下一步
		findViewById(R.id.btn_check_iccid).setOnClickListener(this);
	}
	
	// 点击事件
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch(v.getId())
		{
		// 返回		
		case R.id.btn_back:
			finish();
			break;
			
		// 帮助
		case R.id.btn_help:
			goHelp();
			break;
			
		// 扫描条形码
		case R.id.btn_scan_iccid:
			handleScan();
			break;
			
		// 下一步
		case R.id.btn_check_iccid:
			getTel();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		// 调用垃圾回收器
		Interface.causeGC();
	}
	
	// 获取手机号码
	private void getTel()
	{
		// 校验ICCID号码
		mStrBarCode = mEtIccid.getText().toString().trim();
		if(TextUtils.isEmpty(mStrBarCode))
		{
			App.showAlert(this, "请输入19位ICCID号码！", R.string.ok, null);
			return;
		}
		if(mStrBarCode.length() < 19)
		{
			App.showAlert(this, "您输入的ICCID号码长度有误, 请重新输入！", R.string.ok, null);
			return;
		}
		// 校验手机号码
		mStrTelLast4 = mEtTel.getText().toString().trim();
		if(TextUtils.isEmpty(mStrTelLast4)) 
		{
			App.showAlert(this, "请输入手机号码后4位！", R.string.ok, null);
			return;
		}
		if(mStrTelLast4.length() < 4)
		{
			App.showAlert(this, "您输入的手机号码后4位有误，请重新输入！", R.string.ok, null);
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			App.showAlert(IccidActivity.this, getString(R.string.network_exception));
			return;
		}
		mTelGetRequest = new TelGetRequest(App.getAgentId().trim(), mStrBarCode, mStrTelLast4);
		executeRequest(mHandler, MSG_GET_TELNO_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mTelGetRequest, new GetTelListener());
		Interface.attachProgressDialog(this, "提示信息", "正在获取号码...");
	}

	private void goHelp()
	{
		Intent intent = new Intent(this, HelpDesActivity.class);
		intent.putExtra(Interface.HELP_TYPE, "g1");
		startActivity(intent);
		//App.showAlert(this, R.string.app_help_register_iccid, R.string.ok, null);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == BARCODE_RECOGNISE)
		{
			switch(resultCode)
			{
				case RESULT_OK:
					Log.i(TAG, "一维码识别成功");
					if(UIHandler.mBarCodeStr.trim().length() != 19)
					{
						mHandler.sendEmptyMessage(BARCODE_RECOGNISE_FAIL);
					}
					else 
					{
						mEtIccid.setText(UIHandler.mBarCodeStr);
					}
					break;
					
				case RESULT_CANCELED:
					Log.i(TAG, "一维码识别取消");
					break;
					
				default:
					break;
			}
		}
	}
	
	// 处理识别
	private void handleScan()
	{
		Intent intent = new Intent(IccidActivity.this, CaptureActivity.class);
		intent.putExtra("xml", R.xml.preferences);
		startActivityForResult(intent, BARCODE_RECOGNISE);
	}
	
	private final class GetTelListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL).sendToTarget();
			} 
			else 
			{
				//mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售（序号：1998）").sendToTarget();
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售(序号：19001)").sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 检测交易状态
				Map<String, String> result = mTelGetRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mStrTel = DES.decryptDES(result.get("cardnumber"), BaseRequest.mEncryptKey);
					Log.e(TAG, "telphone=" + mStrTel);
					mHandler.sendEmptyMessage(MSG_GET_TELNO_SUCC);
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_GET_TELNO_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售(序号：19001)").sendToTarget();
				//mHandler.obtainMessage(MSG_GET_TELNO_FAIL, "该手机卡未验证通过，请勿销售（序号：1998）").sendToTarget();
			}
		}
	}
	
	private final class TelCheckRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			cancelTimer();// 取消定时器
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL).sendToTarget();
			} 
			else 
			{
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
				//mHandler.obtainMessage(MSG_CHECK_FAIL, "该手机号未验证通过，请勿销售（序号：2998）").sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String,String> result = mTelCheckRequest.getResult();
				// 检测公告
				Interface.checkMail(IccidActivity.this, result, false);
				// 检测交易状态
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if(BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					//String communicaID = result.get("communicaID");
					mStrCommunicaId = result.get(BaseRequest.COMMUNICATION_ID);
					// 上传类型
					String type = result.get(BaseRequest.UPLOAD_TYPE);
					mStrUploadType = DES.decryptDES(type, App.getDesKey());
					// 输入状态
					Interface.checkInputType(result);
					// 证件开关
					Interface.checkCertType(result);
					// 国政通开关
					Interface.mStrGztFlag = result.get(BaseRequest.GZT_FLAG);
					Log.e("", "国政通开关: " + Interface.mStrGztFlag);
					// 
					mHandler.obtainMessage(MSG_CHECK_TEL_SUC, mStrCommunicaId).sendToTarget();
				} 
				//else if("0001".equals(tradeState)) 
				//{
				//	mHandler.sendEmptyMessage(MSG_CHECK_SUC_OLD);
				//} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHECK_TEL_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_CHECK_TEL_FAIL,  "该手机号未验证通过，请勿销售(序号：29001)").sendToTarget();
				//mHandler.obtainMessage(MSG_CHECK_FAIL,  "该手机号未验证通过，请勿销售（序号：2998）").sendToTarget();
				//mHandler.sendEmptyMessage(MSG_CHECK_FAIL);
			}
		}
	}
}
