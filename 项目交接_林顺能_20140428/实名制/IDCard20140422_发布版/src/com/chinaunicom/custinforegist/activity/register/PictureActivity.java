package com.chinaunicom.custinforegist.activity.register;

import java.io.File;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.activity.main.HelpDesActivity;
import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.api.model.IdentityCard;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.CheckIDRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.base.Setting;
import com.chinaunicom.custinforegist.kit.util.CheckUtils;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.FileUtil;
import com.chinaunicom.custinforegist.R;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.ym.idcard.reg.CameraActitivy;
import com.ym.idcard.reg.OcrEngine;

@SuppressLint("HandlerLeak")
public class PictureActivity extends BaseActivity implements OnClickListener 
{
	private static final int REQUEST_CODE_FRONT_PIC = 0x13;
	private static final int MSG_REG_SUC 			= 0x01;
	private static final int MSG_REG_FAIL 			= 0x02;
	
	private static final int MSG_CHECK_ID_SUC		= 0x03;
	private static final int MSG_CHECK_ID_FAIL		= 0x04;
	private static final int MSG_CHECK_ID_TIMEOUT	= 0x05;
	
	private TextView		mTvTitle;
	private ImageView		mIvStatus;
	private TextView		mTvStatus;
	private TextView 		mTvTel;
	private TextView 		mTvCustType;
	
	private EditText		mEtName;			// 证件姓名输入框
	private EditText 		mEtNo;				// 证件号码输入框
	//private EditText		mEtAddress;			// 证件地址输入框
	private EditText		mEtContactName;		// 联系人输入框
	private EditText		mEtContactPhone;	// 联系电话输入框
	private EditText		mEtContactAddr;		// 通信地址输入框
	
	private ImageView 		mIvTipName;
	private ImageView 		mIvTipNo;
	//private ImageView 	mIvTipAddress;
	private ImageView		mIvTipContactName;
	private ImageView		mIvTipContactPhone;
	private ImageView		mIvTipContactAddr;
	
	private IdentityCard 	mCard;
	private String 			mStrTypeFlag;		// 类型标识，用来判断标题显示(ICCID, 手机号)
	
	private String			mStrCommunicaId;
	private String			mStrUserAddr;
	private CheckIDRequest	mCheckIDRequest;
	
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what) 
			{
				// 身份证识别成功
				case MSG_REG_SUC:
					// 身份证姓名
					mEtName.setText(mCard.getName());
					// 身份证号码
					mEtNo.setText(checkIDNo(mCard.getNo()));
					// 身份证地址
					//mEtAddress.setText(mCard.getAddress());
					break;
					
				// 身份证识别失败
				case MSG_REG_FAIL:
					App.showAlert(PictureActivity.this, "识别失败！");
					break;
					
				// 验证身份证通过
				case MSG_CHECK_ID_SUC:
					Intent intent = getIntent();
					intent.setClass(PictureActivity.this, ConfirmActivity.class);
					// 业务时序
					intent.putExtra("communicaId", mStrCommunicaId);
					// 国政通返回的证件地址
					//mCard.setRealAddress("");
					mCard.setRealAddress(mStrUserAddr);
					intent.putExtra("card", mCard);
					startActivity(intent);
					break;
					
				// 验证身份证信息失败
				case MSG_CHECK_ID_FAIL:
					String desc = (String)msg.obj;
					if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) 
					{
						desc = "服务器响应超时！该证件未验证通过，请勿销售。";
					}
					App.showAlert(PictureActivity.this, 
							desc, 
							R.string.ok, 
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which) 
								{
									dialog.dismiss();
									ActivityHelper.goMain(PictureActivity.this);
									finish();
								}
							});
					break;
					
				// 验证身份证信息超时
				case MSG_CHECK_ID_TIMEOUT:
					App.showAlert(PictureActivity.this, "服务器响应超时！该证件未验证通过，请勿销售。");
					break;
					
				default:
					break;
			}
		}
	};
	
	// 去掉身份证识别结果号码里面的(wrong number)
	private String checkIDNo(String idno)
	{
		try 
		{
			if(idno != null)
			{
				for(int i=0; i<26; i++)
				{
					String tempstr = String.format("%d", 0x61+i);
					idno.replace(tempstr, "");
				}
				idno.replace("(", "");
				idno.replace(")", "");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return idno;
	}

	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.register_phone_step3);
		
		Bundle data 	= getIntent().getExtras();
		mCard 			= data.getParcelable("card");
		mStrCommunicaId = getIntent().getStringExtra("communicaId");
		mStrTypeFlag 	= getIntent().getStringExtra("typeFlag");
		// 初始化控件
		initViews();
	}

	@Override
	protected void onNewIntent(Intent intent) 
	{
		if(intent != null) 
		{
			Bundle data = intent.getExtras();
			if(data != null)
			{
				mCard = data.getParcelable("card");
			}
		}
		super.onNewIntent(intent);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		// 
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		// 调用垃圾回收器
		Interface.causeGC();
	}

	// 初始化控件
	private void initViews()
	{
		// 标题
		mTvTitle = (TextView) findViewById(R.id.tvTitle);
		mTvTitle.setText(mStrTypeFlag.equals("iccid")? R.string.register_iccid:R.string.register_phone);
		// 步骤状态
		mIvStatus = (ImageView) findViewById(R.id.iv_status);
		mTvStatus = (TextView) findViewById(R.id.tv_status);
		if(Interface.IDCARD_ONLY.equals(Interface.mStrCertifyFlag)) 
		{
			mIvStatus.setImageResource(R.drawable.image_step2_new);
			mTvStatus.setText(R.string.phone_step2_without_certify_type);
		}
		else
		{
			mIvStatus.setImageResource(R.drawable.image_step3);
			mTvStatus.setText(R.string.phone_step3_with_certify_type);
		}
		// 号码
		mTvTel = (TextView) findViewById(R.id.tv_tel);
		mTvTel.setText(mCard.getTel());
		// 证件类型
		mTvCustType = (TextView) findViewById(R.id.tv_type);
		mTvCustType.setText(mCard.getTypeName());
		// 身份证姓名
		mIvTipName = (ImageView) findViewById(R.id.iv_tip_name);
		mEtName	= (EditText) findViewById(R.id.et_name);
		// 姓名
		mEtName.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				/*
				String ss = s.toString();
				if(Interface.isSymbol(ss)) 
				{
					s  = s.subSequence(0, s.length()-1);
					mEtName.setText(s);
					mEtName.setSelection(s.length());
					return;
				}
				*/
				
				if(TextUtils.isEmpty(s)) 
				{
					mIvTipName.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipName.setVisibility(View.VISIBLE);
					if (!CheckUtils.isNameValid(mCard.getTypeValue(), s.toString())) 
					{
						mIvTipName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipName.setImageResource(R.drawable.ic_correct);
						mCard.setName(s.toString());
					}
				}
			}
		});
		// 证件号码
		String type = mCard.getTypeValue();
		Log.e("", "type = " + mCard.getTypeValue());
		mIvTipNo = (ImageView) findViewById(R.id.iv_tip_no);
		mEtNo = (EditText) findViewById(type.equals("06")? R.id.et_no2:R.id.et_no1);
		mEtNo.setVisibility(View.VISIBLE);
		// 证件号
		mEtNo.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				/*
				if(Interface.isSymbol(s))
				{
					s = s.subSequence(0, s.length() - 1);
					mEtNo.setText(s);
					mEtNo.setSelection(s.length());
					return;
				}
				*/
				
				if(isNull(s.toString())) 
				{
					mIvTipNo.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipNo.setVisibility(View.VISIBLE);
					if (!CheckUtils.isNoValid(mCard.getTypeValue(), s.toString())) 
					{
						mIvTipNo.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mCard.setNo(s.toString());
						mIvTipNo.setImageResource(R.drawable.ic_correct);
					}
				}
				
				/*
				String tempstr = s.toString();
				if(isNull(tempstr) || tempstr.length() != 18)
				{
					mIvTipNo.setImageResource(R.drawable.ic_error);
				}
				else
				{
					mIvTipNo.setImageResource(R.drawable.ic_correct);
				}
				*/
			}
		});
		
		// 证件地址
		/*
		mIvTipAddress = (ImageView) findViewById(R.id.iv_tip_address);
		mEtAddress = (EditText) findViewById(R.id.et_address);
		// 地址
		mEtAddress.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				mIvTipAddress.setVisibility(View.VISIBLE);
				if (isNull(s.toString()) || isAddressValid(mCard.getTypeValue(), s.toString()) != 0)
				{
					mIvTipAddress.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mCard.setAddress(s.toString());
					mIvTipAddress.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		*/
		
		// 联系人姓名
		mIvTipContactName 	= (ImageView) findViewById(R.id.iv_tip_contact_name);
		mEtContactName	= (EditText) findViewById(R.id.et_contact_name);
		mEtContactName.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(TextUtils.isEmpty(s)) 
				{
					mIvTipContactName.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipContactName.setVisibility(View.VISIBLE);
					if (!CheckUtils.isNameValid(mCard.getTypeValue(), s.toString())) 
					{
						mIvTipContactName.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipContactName.setImageResource(R.drawable.ic_correct);
						mCard.setContactName(s.toString());
					}
				}
			}
		});
		mEtContactName.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus) 
				{
					mEtContactName.setHint("");
				}
				else
				{
					mEtContactName.setHint("非必填项");
				}
			}
		});
		
		// 联系人电话
		mIvTipContactPhone = (ImageView) findViewById(R.id.iv_tip_contact_phone);
		mEtContactPhone = (EditText) findViewById(R.id.et_contact_phone);
		mEtContactPhone.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(TextUtils.isEmpty(s)) 
				{
					mIvTipContactPhone.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipContactPhone.setVisibility(View.VISIBLE);
					if (!CheckUtils.isContactTelValid(s.toString())) 
					{
						mIvTipContactPhone.setImageResource(R.drawable.ic_error);
					} 
					else 
					{
						mIvTipContactPhone.setImageResource(R.drawable.ic_correct);
						mCard.setContactTel(s.toString());
					}
				}
			}
		});
		mEtContactPhone.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus) 
				{
					mEtContactPhone.setHint("");
				}
				else
				{
					mEtContactPhone.setHint("非必填项");
				}
			}
		});
		
		// 联系人地址
		mIvTipContactAddr = (ImageView) findViewById(R.id.iv_tip_contact_address);
		mEtContactAddr = (EditText) findViewById(R.id.et_contact_address);
		mEtContactAddr.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(TextUtils.isEmpty(s)) 
				{
					mIvTipContactAddr.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvTipContactAddr.setVisibility(View.VISIBLE);
					if (!CheckUtils.isContactAddrValid(s.toString())) 
					{
						mIvTipContactAddr.setImageResource(R.drawable.ic_error);
					}
					else 
					{
						mIvTipContactAddr.setImageResource(R.drawable.ic_correct);
						mCard.setContactAddress(s.toString());
					}
				}
			}
		});
		mEtContactAddr.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus) 
			{
				if(hasFocus) 
				{
					mEtContactAddr.setHint("");
				}
				else
				{
					mEtContactAddr.setHint("非必填项");
				}
			}
		});
		
		findViewById(R.id.btn_back).setOnClickListener(this);
		findViewById(R.id.btn_help).setOnClickListener(this);
		findViewById(R.id.btn_nextStep).setOnClickListener(this);
		// 证件识别
		Button btnScan = (Button) findViewById(R.id.btn_scan);
		btnScan.setOnClickListener(this);
		btnScan.setVisibility("01".equals(mCard.getTypeValue())? View.VISIBLE:View.GONE);
		if(mCard.getTypeValue().equals("01"))
		{
			// 输入类型 1、 手工输入   2、拍照扫描  3、 2+1 
			if(Interface.mInputType.equals("1"))
			{
				btnScan.setVisibility(View.GONE);
			}
			else if(Interface.mInputType.equals("2"))
			{
				// 禁止输入
				forbidInput();
			}
		}
	}

	// 禁止输入
	private void forbidInput()
	{
		mEtName.setEnabled(false);
		mEtNo.setEnabled(false);
		mEtContactName.setEnabled(false);
		mEtContactPhone.setEnabled(false);
		mEtContactAddr.setEnabled(false);
		// 防止输入法自动弹出
		((ScrollView)(findViewById(R.id.sv_container))).setScrollContainer(false);
	}

	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
			// 返回
			case R.id.btn_back:
				finish();
				break;
				
			// 扫描身份证
			case R.id.btn_scan:
				handleFrontPicture();
				break;
				
			// 帮助
			case R.id.btn_help:
				goHelp();
				break;
				
			// 下一步
			case R.id.btn_nextStep:
				goConfirm();
				break;
				
			default:
				break;
		}
	}
	
	// 跳转到确认信息界面
	private void goConfirm() 
	{
		// 判断证件姓名
		final String name = mEtName.getText().toString();
		if (isNull(name)) 
		{
			mEtName.requestFocus();
			mIvTipName.setVisibility(View.VISIBLE);
			mIvTipName.setImageResource(R.drawable.ic_error);
			App.showAlert(PictureActivity.this, "请输入证件姓名！");
			return;
		}
		if (!CheckUtils.isNameValid(mCard.getTypeValue(), name)) 
		{
			mEtName.requestFocus();
			mIvTipName.setVisibility(View.VISIBLE);
			mIvTipName.setImageResource(R.drawable.ic_error);
			App.showAlert(PictureActivity.this, "您输入的证件姓名不正确, 请重新输入！");
			return;
		}
		
		// 判断证件号码
		final String no = mEtNo.getText().toString();
		if (TextUtils.isEmpty(no)) 
		{
			mEtNo.requestFocus();
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			App.showAlert(PictureActivity.this, "请输入证件号码！");
			return;
		}
		if (!CheckUtils.isNoValid(mCard.getTypeValue(), no)) 
		{
			mEtNo.requestFocus();
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			App.showAlert(PictureActivity.this, "您输入的证件号码不合法, 请重新输入！");
			return;
		}
		
		/*
		// 判断证件地址
		final String address = mEtAddress.getText().toString();
		if (TextUtils.isEmpty(address)) 
		{
			mEtAddress.requestFocus();
			mIvTipAddress.setVisibility(View.VISIBLE);
			mIvTipAddress.setImageResource(R.drawable.ic_error);
			App.showAlert(PictureActivity.this, "请输入证件地址！", R.string.button_ok, null);
			return;
		}
		int result = CheckUtils.isAddressValid(mCard.getTypeValue(), address);
		if (result != 0) 
		{
			mEtAddress.requestFocus();
			mIvTipAddress.setVisibility(View.VISIBLE);
			mIvTipAddress.setImageResource(R.drawable.ic_error);
			// 
			String tempstr = (result==1? "您输入的证件地址不合法, 请重新输入！":"您输入的证件地址长度太短, 请重新输入！");
			App.showAlert(PictureActivity.this, tempstr, R.string.button_ok, null);
			return;
		}
		*/
		
		// 联系人信息
		// 联系人姓名
		String contactName = mEtContactName.getText().toString().trim();
		if(!TextUtils.isEmpty(contactName)) 
		{
			if(!CheckUtils.isNameValid(mCard.getTypeValue(), contactName)) 
			{
				mEtContactName.requestFocus();
				mIvTipContactName.setVisibility(View.VISIBLE);
				mIvTipContactName.setImageResource(R.drawable.ic_error);
				App.showAlert(PictureActivity.this, "您输入的联系人不合规, 请重新输入！");
				return;
			}
		}
		else
		{
			contactName = "";
		}
		
		// 联系人电话
		String contactPhone = mEtContactPhone.getText().toString().trim();
		if(!TextUtils.isEmpty(contactPhone)) 
		{
			if(!CheckUtils.isContactTelValid(contactPhone)) 
			{
				mEtContactPhone.requestFocus();
				mIvTipContactPhone.setVisibility(View.VISIBLE);
				mIvTipContactPhone.setImageResource(R.drawable.ic_error);
				App.showAlert(PictureActivity.this, "您输入的联系电话不合规, 请重新输入！");
				return;
			}
		}
		else
		{
			contactPhone = "";
		}
		
		// 联系人通信地址
		String contactAddr = mEtContactAddr.getText().toString().trim();
		if(!TextUtils.isEmpty(contactAddr)) 
		{
			if(!CheckUtils.isContactAddrValid(contactAddr)) 
			{
				mEtContactAddr.requestFocus();
				mIvTipContactAddr.setVisibility(View.VISIBLE);
				mIvTipContactAddr.setImageResource(R.drawable.ic_error);
				App.showAlert(PictureActivity.this, "您输入的联系人通信地址不合规, 请重新输入！");
				return;
			}
		}
		else
		{
			contactAddr = "";
		}
		
		// 证件信息
		mCard.setName(name);
		mCard.setNo(no);
		//mCard.setAddress(address);
		// 联系人信息
		mCard.setContactName(contactName);
		mCard.setContactTel(contactPhone);
		mCard.setContactAddress(contactAddr);
		
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			App.showAlert(PictureActivity.this, getString(R.string.network_exception));
			return;
		}
		// 调用国政通接口验证身份证信息
		if(mCard.getTypeValue().equals("01") && Interface.mStrGztFlag.equals("1")) 
		{
			Interface.attachProgressDialog(this, "", "正在校验身份证信息...");
			mCheckIDRequest = new CheckIDRequest(mStrCommunicaId, App.getAgentId(), mCard.getTel(), mCard.getName(), "01", mCard.getNo());
			executeRequest(mHandler, MSG_CHECK_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mCheckIDRequest, new IDCheckRequestListener());
		}
		else
		{
			// 置位国政通标志
			Interface.mStrGztFlag = "0";
			// 跳转到确认界面
			Intent intent = getIntent();
			intent.setClass(this, ConfirmActivity.class);
			intent.putExtra("card", mCard);
			startActivity(intent);
		}
	}
	
	public final class IDCheckRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e != null && e.getCause() instanceof java.net.SocketTimeoutException)
			{
				mHandler.obtainMessage(MSG_CHECK_ID_FAIL).sendToTarget();
			}
			else
			{
				mHandler.obtainMessage(MSG_CHECK_ID_FAIL,  "该证件未验证通过，请勿销售！").sendToTarget();
			}
		}
		
		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String, String> result = mCheckIDRequest.getResult();
				// 检测交易状态
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if(BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 业务时序
					mStrCommunicaId = result.get(BaseRequest.COMMUNICATION_ID);
					// 证件地址
					mStrUserAddr = DES.decryptDES(result.get(BaseRequest.USER_ADDR), App.getDesKey());
					Log.e("", "证件地址为: " + mStrUserAddr);
					mHandler.obtainMessage(MSG_CHECK_ID_SUC).sendToTarget();
				}
				else 
				{
					// 获取错误指示
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHECK_ID_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_CHECK_ID_FAIL,  "该证件未验证通过，请勿销售！").sendToTarget();
			}
		}
	}
	
	// 调转到帮助界面
	private void goHelp() 
	{
		Intent intent = new Intent(this, HelpDesActivity.class);
		intent.putExtra(Interface.HELP_TYPE, "f3");
		startActivity(intent);
	}

	// 识别身份证
	private void handleScan() 
	{
		if (!TextUtils.isEmpty(mCard.getPicPath()) && new File(mCard.getPicPath()).exists()
				&& mCard.getTypeValue().equals("01")) 
		{
			Interface.attachProgressDialog(PictureActivity.this, "身份证识别", "正在识别中...");
			new ReconizeTask().execute(mCard.getPicPath(), null);
		} 
	}

	// 进行拍照
	private void handleFrontPicture() 
	{
		if(!"01".equals(mCard.getTypeValue())) 
		{
			App.showAlert(PictureActivity.this, "仅支持拍照二代身份证！");
			return;
		}
		/*
		// 弹出二代证预览提示
		MessageboxDialog messageboxDialog = new MessageboxDialog(PictureActivity.this, R.style.transparent_dialog);
		messageboxDialog.setTitle("将二代身份证置于框内, 离镜头10cm左右, 可自动识别。");
		messageboxDialog.setImage(R.drawable.image_idcard);
		messageboxDialog.setMessage("二代身份证");
		messageboxDialog.setPositiveListener(getString(R.string.ok), new View.OnClickListener() {
			@Override
			public void onClick(View arg0) 
			{
				Intent intent = ActivityHelper.getTakePictureIntent(PictureActivity.this);
				startActivityForResult(intent, REQUEST_CODE_FRONT_PIC);
			}
		});
		messageboxDialog.show();
		*/
		Intent intent = ActivityHelper.getTakePictureIntent(PictureActivity.this);
		startActivityForResult(intent, REQUEST_CODE_FRONT_PIC);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_FRONT_PIC && resultCode == CameraActitivy.RESULT_CODE_TAKE_SUC) 
		{
			if (data != null && data.getExtras() != null) 
			{
				String picPath = data.getExtras().getString("pic_path");// a8837cd7995846468b791872c7671000.j
				String scalePath = data.getExtras().getString("pic_scale_path");
				if (!TextUtils.isEmpty(picPath)) 
				{
					File imgFile = new File(picPath);
					if(imgFile.exists())
					{
						try 
						{
							if(!TextUtils.isEmpty(mCard.getPicPath()))
							{
								File oldFile = new File(mCard.getPicPath());
								oldFile.delete();
							}
							mCard.setPicPath(picPath);
							mCard.setPicScalePath(scalePath);
							// 拍照完成，开始识别身份证
							handleScan();
						} 
						catch (Exception e)
						{
							e.printStackTrace();
							App.showAlert(PictureActivity.this, "拍照失败, 请重拍！");
						} 
						catch (OutOfMemoryError e)
						{
							e.printStackTrace();
							App.showAlert(PictureActivity.this, "拍照失败, 请重拍！");
						}
					}
				}
			}
		}
	}

	// OCR识别身份证
	class ReconizeTask extends AsyncTask<String, Void, Void> 
	{
		@Override
		protected Void doInBackground(String... params) 
		{
			try 
			{
				byte[] data = FileUtil.getBytesFromFile(params[0]);
				OcrEngine mOcrEngine = new OcrEngine();
				mOcrEngine.recognize(data, Setting.getOcrLanguage(), Setting.getKeyLanguage(),
						Setting.isBlurDetectionOn(), mCard, null, "no_need");
				mHandler.obtainMessage(MSG_REG_SUC).sendToTarget();
			} 
			catch (Exception e) 
			{
				mHandler.obtainMessage(MSG_REG_FAIL).sendToTarget();
				e.printStackTrace();
			}
			Interface.detachProgressDialog();
			return null;
		}
	}
}
