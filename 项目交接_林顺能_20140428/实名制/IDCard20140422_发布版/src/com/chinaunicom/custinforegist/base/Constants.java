package com.chinaunicom.custinforegist.base;

import android.os.Environment;

public class Constants 
{
	private final static String BASE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/IdCard";
	public static final String DOWNLOAD_DIR = BASE_DIR + "/download/";
}
