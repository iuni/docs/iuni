package com.chinaunicom.custinforegist.ui.dialog;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chinaunicom.custinforegist.R;

public class DialogAdapter extends BaseAdapter 
{
	private Drawable[] 			_itemIcons;
	private CharSequence[] 		_items;
	private boolean 			_isSingleChoice;
	private int 				_index;

	public DialogAdapter(CharSequence[] items, Drawable[] itemIcons) 
	{
		_items = items;
		_itemIcons = itemIcons;
	}

	@Override
	public int getCount() 
	{
		return _items.length;
	}

	@Override
	public String getItem(int i) 
	{
		return _items[i].toString();
	}

	@Override
	public long getItemId(int id) 
	{
		return id;
	}

	public void setIsSingleChoice(boolean isSingleChoice) 
	{
		_isSingleChoice = isSingleChoice;
	}

	public void setIndex(int index) 
	{
		_index = index;
	}

	@Override
	public boolean isEnabled(int position) 
	{
		return true;
	}
	
	@Override
	public View getView(int i, View view, ViewGroup viewgroup) 
	{
		DialogHolder holder = null;
		if (view == null) 
		{
			holder = new DialogHolder();
			view = LayoutInflater.from(viewgroup.getContext()).inflate(R.layout.list_cell_dialog, null, false);
			holder.titleTv = (TextView) view.findViewById(R.id.title_tv);
			holder.iconIv = (ImageView) view.findViewById(R.id.icon_iv);
			holder.radio = (RadioButton)view.findViewById(R.id.radio);
			holder.divider = view.findViewById(R.id.list_divider);
			view.setTag(holder);
		} 
		else 
		{
			holder = (DialogHolder) view.getTag();
		}
		holder.titleTv.setText(getItem(i));
		if (_itemIcons.length > 0) 
		{
			holder.iconIv.setVisibility(View.VISIBLE);
			holder.iconIv.setImageDrawable(_itemIcons[i]);
		}
		//if (i == getCount()-1) {
		//	holder.divider.setVisibility(View.GONE);
		//} else {
		//	holder.divider.setVisibility(View.VISIBLE);
		//}
		if(_isSingleChoice) 
		{
			holder.radio.setVisibility(View.VISIBLE);
			if(i == _index) 
			{
				holder.radio.setChecked(true);
			} 
			else 
			{
				holder.radio.setChecked(false);
			}
		} 
		else 
		{
			holder.radio.setVisibility(View.GONE);
		}
		return view;
	}
	
	private class DialogHolder 
	{
		public View 			divider;
		public ImageView 		iconIv;
		public TextView 		titleTv;
		public RadioButton 		radio;
	}
}
