package com.chinaunicom.custinforegist.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.R;

public class MessageboxDialog extends Dialog implements View.OnClickListener
{
	private TextView				mInfoTitle		= null;
	private ImageView				mImageSrc		= null;
	private TextView 				mInfoText 		= null;
	private Button					mBtnOk			= null;
	private View.OnClickListener	mBtnOkListener 	= null;

	public static MessageboxDialog getInstance(Context context)
	{
		return new MessageboxDialog(context, R.style.transparent_dialog);
	}
	
	public MessageboxDialog(Context context, boolean cancelable,OnCancelListener cancelListener)
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public MessageboxDialog(Context context, int theme)
	{
		super(context, theme);
		initView();
	}

	public MessageboxDialog(Context context)
	{
		super(context);
		initView();
	}

	// 初始化布局
	private void initView()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.messagebox_dialog);
		
		/*
		// 设置内部控件的透明度
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.5f;
		getWindow().setAttributes(lp);
		*/

		// 顶部标题
		mInfoTitle = (TextView)findViewById(R.id.message_title);
		// 图像视图
		mImageSrc = (ImageView)findViewById(R.id.message_image);
		// 提示信息
		mInfoText = (TextView)findViewById(R.id.message_text);
		// 确定按钮
		mBtnOk = (Button)findViewById(R.id.message_ok);
		mBtnOk.setOnClickListener(this);
	}
	
	// 设置标题
	public void setTitle(CharSequence title)
	{
		mInfoText.setVisibility(View.VISIBLE);
		mInfoTitle.setText(title);
	}
	
	// 设置消息
	public void setMessage(CharSequence message)
	{
		mInfoText.setVisibility(View.VISIBLE);
		mInfoText.setText(message);
	}
	
	// 设置图像
	public void setImage(int resID)
	{
		mImageSrc.setVisibility(View.VISIBLE);
		mImageSrc.setImageResource(resID);
	}
	
	// 设置确定按钮的监听事件
	public void setPositiveListener(String okStr, View.OnClickListener okListener)
	{
		mBtnOk.setText(okStr);
		mBtnOk.setVisibility(View.VISIBLE);
		mBtnOkListener = okListener;
	}

	public void onClick(View v)
	{
		if(v == mBtnOk && mBtnOkListener != null)
		{
			mBtnOkListener.onClick(v);
		}
	}
}
