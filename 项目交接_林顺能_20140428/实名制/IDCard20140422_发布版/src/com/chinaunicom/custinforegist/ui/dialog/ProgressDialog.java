package com.chinaunicom.custinforegist.ui.dialog;

import com.chinaunicom.custinforegist.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressDialog extends Dialog
{
	private ProgressBar 	mCircleBar 	= null;
	private TextView 		mInfoText 	= null;

	public static ProgressDialog getInstance(Context context)
	{
		return new ProgressDialog(context, R.style.transparent_dialog);
	}
	
	public ProgressDialog(Context context, boolean cancelable,OnCancelListener cancelListener)
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public ProgressDialog(Context context, int theme)
	{
		super(context, theme);
		initView();
	}

	public ProgressDialog(Context context)
	{
		super(context);
		initView();
	}

	public void setMessage(CharSequence message)
	{
		mInfoText.setVisibility(View.VISIBLE);
		mInfoText.setText(message);
	}
	
	// 初始化布局
	private void initView()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.progress_dialog);
		
		// 设置内部控件的透明度
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.5f;
		getWindow().setAttributes(lp);
		
		mCircleBar = (ProgressBar)this.findViewById(R.id.progressBar);
		mInfoText = (TextView)findViewById(R.id.text_info);

		mCircleBar.setMax(1);
	}
	
	public void setProgress(int progress)
	{
		mCircleBar.setProgress(progress);
	}
	
	public void setMax(int max)
	{
		mCircleBar.setMax(max);
	}
}
