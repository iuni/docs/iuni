package com.chinaunicom.custinforegist.ui.dialog;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

// 接入环境设置Dialog
public class EnvSetDialog extends Dialog implements View.OnClickListener
{
	private EditText			mEtInput;
	
	public EnvSetDialog(Context context) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.dialog_env_set);
		this.setCanceledOnTouchOutside(false);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 输入框
		mEtInput = (EditText) findViewById(R.id.et_input);
		mEtInput.setText(App.getAccessEnv());
		// 返回
		findViewById(R.id.btn_cancel).setOnClickListener(this);
		// 设置
		findViewById(R.id.btn_ok).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回
		case R.id.btn_cancel:
			dismiss();
			break;
			
		// 设置
		case R.id.btn_ok:
			setEnv();
			break;
			
		default:
			break;
		}
	}
	
	// 设置接入环境
	private void setEnv() 
	{
		String strEnv = mEtInput.getText().toString();
		if(StringUtil.isEmptyOrNull(strEnv)) 
		{
			App.showAlert(getContext(), "请输入地址!");
			return;
		}
		App.setAccessEnv(strEnv);
		dismiss();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
