package com.chinaunicom.custinforegist.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;
import com.chinaunicom.custinforegist.R;

public class ProgressView extends RelativeLayout {
	public static final int STATE_NONE = 0;
	public static final int STATE_LOADING = 1;
	public static final int STATE_HIDE = 2;
	private int _state = STATE_NONE;
	private ImageView _spinner;
	private ObjectAnimator _spinnerAnim;
	private TextView _text;

	public ProgressView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public ProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ProgressView(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		inflate(context, R.layout.view_progress, this);
		_spinner = (ImageView) findViewById(R.id.loading_icon);
		_spinnerAnim = ObjectAnimator.ofFloat(_spinner, "rotation",
				new float[] { 2.0f, 8, 12, 21, 38, 63, 100, 149, 205, 255, 292,
						318, 336, 348, 356, 360 });
		_spinnerAnim.setStartDelay(500L);
		_spinnerAnim.setDuration(1000L);
		_spinnerAnim.setRepeatCount(-1);
		_spinnerAnim.setRepeatMode(1);
		_spinnerAnim.addListener(new AnimatorListener() {

			@Override
			public void onAnimationCancel(Animator animator) {
				_spinnerAnim.setCurrentPlayTime(0);
			}

			@Override
			public void onAnimationEnd(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
				if (_spinner.getVisibility() != View.VISIBLE)
					_spinnerAnim.cancel();
			}

			@Override
			public void onAnimationStart(Animator animator) {
			}
		});
		_text = (TextView) findViewById(R.id.loading_text);
	}

	private void updateView() {
		switch (_state) {
		case STATE_LOADING:
			setVisibility(View.VISIBLE);
			// Animation anim = AnimationUtils.loadAnimation(getContext(),
			// R.anim.show_progress);
			// startAnimation(anim);
			_spinnerAnim.start();
			break;
		case STATE_NONE:
			setVisibility(View.VISIBLE);
			_spinnerAnim.cancel();
			break;
		case STATE_HIDE:
			setVisibility(View.GONE);
			_spinnerAnim.cancel();
			break;
		}
	}

	public void setText(int text) {
		_text.setText(text);
	}

	public void setText(String text) {
		_text.setText(text);
	}

	public void setState(int state) {
		if (_state != state) {
			_state = state;
			updateView();
		}
	}
}
