package com.ym.idcard.reg;

import java.util.HashMap;

public class Product {

	public static HashMap<String,String> CfgNameMap= new HashMap<String,String>();
	public static String DEVICE_MARK;
	public static final String DEVICE_MARK_CH = "E";
	public static final String DEVICE_MARK_EU = "F";
	public static final String DEVICE_MARK_JP = "H";
	public static final String DEVICE_MARK_RS = "G";
	public static HashMap<String,String> DeviceMarkMap = new HashMap<String,String>();
	public static int REG_MARK = 0;
	public static final int REG_MARK_BCR_CH = 2;
	public static final int REG_MARK_BCR_EU = 3;
	public static final int REG_MARK_BCR_JP = 12;
	public static final int REG_MARK_BCR_RS = 11;
	public static HashMap<String,Integer> RegMarkMap = new HashMap<String,Integer>();

	static {
		RegMarkMap.put("scan.bcr.ch", 2);
		DeviceMarkMap.put("scan.bcr.ch", DEVICE_MARK_CH);
		CfgNameMap.put("scan.bcr.ch", "bcr.cfg");
		
		RegMarkMap.put("scan.bcr.eu", 3);
		DeviceMarkMap.put("scan.bcr.eu", DEVICE_MARK_EU);
		CfgNameMap.put("scan.bcr.eu", "bcr-eu.cfg");
		
		RegMarkMap.put("scan.bcr.rs", 11);
		DeviceMarkMap.put("scan.bcr.rs", DEVICE_MARK_RS);
		CfgNameMap.put("scan.bcr.rs", "bcr-rs.cfg");
		
		RegMarkMap.put("scan.bcr.jp",12);
		DeviceMarkMap.put("scan.bcr.jp", DEVICE_MARK_JP);
		CfgNameMap.put("scan.bcr.jp", "bcr-jp.cfg");
		
		RegMarkMap.put("scan.bcr.online", 2);
		DeviceMarkMap.put("scan.bcr.online", DEVICE_MARK_CH);
		CfgNameMap.put("scan.bcr.online", "bcr-online.cfg");
		
		RegMarkMap.put("com.yunmai.bcr.NetDragon", 2);
		DeviceMarkMap.put("com.yunmai.bcr.NetDragon", DEVICE_MARK_CH);
		CfgNameMap.put("com.yunmai.bcr.NetDragon", "bcr.cfg");
		
		RegMarkMap.put("com.yunmai.bcr.ginwave", 2);
		DeviceMarkMap.put("com.yunmai.bcr.ginwave", DEVICE_MARK_CH);
		CfgNameMap.put("com.yunmai.bcr.ginwave", "bcr-ginwave.cfg");
		
		RegMarkMap.put("hotcard.net", 5);
		DeviceMarkMap.put("hotcard.net", "L");
		CfgNameMap.put("hotcard.net", "ocr-net.cfg");
		
		RegMarkMap.put("hotcard.net.regged", 5);
		DeviceMarkMap.put("hotcard.net.regged", "L");
		CfgNameMap.put("hotcard.net.regged", "ocr-net.cfg");
		
		RegMarkMap.put("hotcard.net.eu", 16);
		DeviceMarkMap.put("hotcard.net.eu", "N");
		CfgNameMap.put("hotcard.net.eu", "ocr-net-eu.cfg");
		
		RegMarkMap.put("hotcard.doc.reader", 8);
		DeviceMarkMap.put("hotcard.doc.reader", "R");
		CfgNameMap.put("hotcard.doc.reader", "ocr-r.cfg");
		
		RegMarkMap.put("hotcard.doc.reader.regged", 8);
		DeviceMarkMap.put("hotcard.doc.reader.regged", "R");
		CfgNameMap.put("hotcard.doc.reader.regged", "ocr-r.cfg");
		
		RegMarkMap.put("hotcard.net.eu",14);
		DeviceMarkMap.put("hotcard.net.eu", "S");
		CfgNameMap.put("hotcard.net.eu", "ocr-r-eu.cfg");
		
		RegMarkMap.put("scan.bidc", 21);
		DeviceMarkMap.put("scan.bidc", "X");
		CfgNameMap.put("scan.bidc", "bidc.cfg");
		
		RegMarkMap.put("hotcard.plate.number", 20);
		DeviceMarkMap.put("hotcard.plate.number", "W");
		CfgNameMap.put("hotcard.plate.number", "plate-number.cfg");
	}
	
	public Product() {
	}

	public static String getCfgName(String s) {
		String s1;
		if (CfgNameMap.get(s) == null)
			s1 = "bcr.cfg";
		else
			s1 = CfgNameMap.get(s);
		return s1;
	}

	private static String getDeviceMark(String s) {
		String s1;
		if (DeviceMarkMap.get(s) == null)
			s1 = "";
		else
			s1 = (String) DeviceMarkMap.get(s);
		return s1;
	}

	private static int getRegMark(String s) {
		int i;
		if (RegMarkMap.get(s) == null)
			i = 0;
		else
			i = RegMarkMap.get(s);
		return i;
	}

	public static void init(String s) {
		DEVICE_MARK = getDeviceMark(s);
		REG_MARK = getRegMark(s);
	}
}
