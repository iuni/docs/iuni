package com.ym.idcard.reg;

public class NativeImage 
{
	private static final String LIB = "imageengine";
	private static final String TAG = "NativeImage";

	static {
		try {
			System.loadLibrary("imageengine");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public native int closeEngine(long paramLong);

	public native long createEngine();

	public void finalize() {}

	public native int freeImage(long paramLong);

	public native int getImageComponent(long paramLong);

	public native byte[] getImageData(long paramLong);

	public native long getImageDataEx(long paramLong);

	public native int getImageHeight(long paramLong);

	public native int getImageWidth(long paramLong);

	public native int getProperty(long paramLong, byte[] paramArrayOfByte);

	public native int initImage(long paramLong, int paramInt1, int paramInt2);

	public native int isGrayImage(long paramLong);

	public native int loadImage(long paramLong, byte[] paramArrayOfByte);

	public native int loadmemjpg(long paramLong, byte[] paramArrayOfByte, int paramInt);

	public native int saveImage(long paramLong, byte[] paramArrayOfByte);

	public native int scale(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt1, int paramInt2);

	public native int setSaveParams(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);

	public native int setSaveParamsEx(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3);
}
