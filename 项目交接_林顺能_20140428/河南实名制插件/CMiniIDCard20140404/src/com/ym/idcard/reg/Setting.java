package com.ym.idcard.reg;

public class Setting 
{
	public Setting() 
	{
		
	}

	public static void addDownloadCount() 
	{
		loaded();
		mDownloadCount = 1 + mDownloadCount;
		save();
	}

	public static void addSaveCount() 
	{
		loaded();
		mSaveCount = 1 + mSaveCount;
		save();
	}

	public static void check_dataLicense() 
	{
//		if (License.length() <= 0
//				&& FileUtil.makeSureFileExistEx(dataConfig) > 0)
//			try {
//				FileReader filereader = new FileReader(new File(dataConfig));
//				BufferedReader bufferedreader = new BufferedReader(filereader);
//				do {
//					String s = bufferedreader.readLine();
//					if (s == null) {
//						bufferedreader.close();
//						filereader.close();
//						break;
//					}
//					String s1 = s.trim();
//					if (s1.length() > 0) {
//						int i = s1.indexOf(";");
//						if (i != -1)
//							s1 = s1.substring(0, i).trim();
//						if (s1.length() > 0) {
//							int j = s1.indexOf("=");
//							if (j != -1) {
//								String s2 = s1.substring(0, j).trim();
//								String s3 = s1.substring(j + 1).trim();
//								if (s2.equals("License"))
//									License = s3;
//							}
//						}
//					}
//				} while (true);
//			} catch (Exception exception) {
//				Debug.e("", exception);
//			}
	}

	public static int getDownloadCount() 
	{
		loaded();
		return mDownloadCount;
	}

	public static int getGridNum() 
	{
		loaded();
		return mGridNum;
	}

	public static int getKeyLanguage() 
	{
		loaded();
		return mKeyLanguage;
	}

	public static String getLicense() 
	{
		loaded();
		return License;
	}

	public static int getOcrLanguage() 
	{
		loaded();
		return mOcrLanguage;
	}

	public static String getPimPwd() 
	{
		loaded();
		return mPimPwd;
	}

	public static String getPimUName() 
	{
		loaded();
		return mPimUName;
	}

	public static int getSaveCount() 
	{
		loaded();
		return mSaveCount;
	}

	public static int getTrialTimes() 
	{
		loaded();
		return 1;
	}

	public static int getUiLanguage() 
	{
		loaded();
		return mUiLanguage;
	}

	public static boolean haveSave() 
	{
		loaded();
		boolean flag;
		if (mSaveCount > 3 && mDownloadCount != 3)
			flag = false;
		else
			flag = true;
		return flag;
	}

	public static boolean is86DetectionOn() 
	{
		loaded();
		return m86DetectionOn;
	}

	public static boolean isAllExTipOn() 
	{
		loaded();
		return mAllExTipOn;
	}

	public static boolean isAutoCapture() 
	{
		loaded();
		return mAutoCapture;
	}

	public static boolean isBlurDetectionOn() 
	{
		loaded();
		return false;
	}

	public static boolean isInvite() 
	{
		loaded();
		return mInvite;
	}

	public static boolean isLoadCsvTipOn() 
	{
		loaded();
		return mLoadCsvTipOn;
	}

	public static boolean isOneExTipOn() 
	{
		loaded();
		return mOneExTipOn;
	}

	public static boolean isSyncAllBizcardTipOn() 
	{
		loaded();
		return mSyncAllBizcardTipOn;
	}

	public static boolean isSyncBizcardTipOn() 
	{
		loaded();
		return mSyncBizcardTipOn;
	}

	public static boolean isSysCamera() 
	{
		loaded();
		return mSysCamera;
	}

	public static boolean isUnlock() 
	{
		loaded();
		//return getLicense().equals(Register.calcLicense());
		return false;
	}

	public static boolean load()
    {
//        boolean flag;
//        String s;
//        int i;
//        flag = false;
//        s = (new StringBuilder(String.valueOf(App.getBaseDir()))).append("/").append(CONFIG_NAME).toString();
//        i = FileUtil.makeSureFileExistEx(s);
//        if(i < 0)
//            break MISSING_BLOCK_LABEL_816;
//        if(i <= 0) goto _L2; else goto _L1
//_L1:
//        flag = false;
//        FileReader filereader;
//        BufferedReader bufferedreader;
//        filereader = new FileReader(new File(s));
//        bufferedreader = new BufferedReader(filereader);
//_L5:
//        String s1 = bufferedreader.readLine();
//        if(s1 != null) goto _L4; else goto _L3
//_L3:
//        bufferedreader.close();
//        filereader.close();
//        flag = true;
//_L6:
//        check_dataLicense();
//_L7:
//        return flag;
//_L4:
//        String s3;
//        String s4;
//        String s2 = s1.trim();
//        if(s2.length() > 0)
//        {
//            int j = s2.indexOf(";");
//            if(j != -1)
//                s2 = s2.substring(0, j).trim();
//            if(s2.length() > 0)
//            {
//                int k = s2.indexOf("=");
//                if(k != -1)
//                {
//                    s3 = s2.substring(0, k).trim();
//                    s4 = s2.substring(k + 1).trim();
//                    if(!s3.equals("Trialtimes"))
//                        break MISSING_BLOCK_LABEL_227;
//                    mTrialTimes = Integer.parseInt(s4);
//                }
//            }
//        }
//          goto _L5
//        Exception exception;
//        exception;
//        Debug.e("", exception);
//          goto _L6
//        if(s3.equals("GridNum"))
//            mGridNum = Integer.parseInt(s4);
//        else
//        if(s3.equals("SysCamera"))
//        {
//            if(s4.equals("1"))
//                mSysCamera = true;
//            else
//                mSysCamera = false;
//        } else
//        if(s3.equals("BlurDet"))
//        {
//            if(s4.equals("0"))
//                mBlurDetectionOn = false;
//            else
//                mBlurDetectionOn = true;
//        } else
//        if(s3.equals("+86"))
//        {
//            if(s4.equals("0"))
//                m86DetectionOn = false;
//            else
//                m86DetectionOn = true;
//        } else
//        if(s3.equals("OneExTipOn"))
//        {
//            if(s4.equals("0"))
//                mOneExTipOn = false;
//            else
//                mOneExTipOn = true;
//        } else
//        if(s3.equals("AllExTipOn"))
//        {
//            if(s4.equals("0"))
//                mAllExTipOn = false;
//            else
//                mAllExTipOn = true;
//        } else
//        if(s3.equals("LoadCsvTipOn"))
//        {
//            if(s4.equals("0"))
//                mLoadCsvTipOn = false;
//            else
//                mLoadCsvTipOn = true;
//        } else
//        if(s3.equals("UiLan"))
//        {
//            if(s4.equals("1"))
//            {
//                mUiLanguage = 1;
//                mKeyLanguage = 1;
//            } else
//            if(s4.equals("2"))
//            {
//                mUiLanguage = 2;
//                mKeyLanguage = 1;
//            } else
//            if(s4.equals("3"))
//            {
//                mUiLanguage = 3;
//                mKeyLanguage = 1;
//            } else
//            if(s4.equals("4"))
//            {
//                mUiLanguage = 4;
//                mKeyLanguage = 3;
//            }
//        } else
//        if(s3.equals("OcrLan"))
//        {
//            if(s4.equals("1"))
//                mOcrLanguage = 1;
//            else
//            if(s4.equals("2"))
//                mOcrLanguage = 2;
//            else
//            if(s4.equals("3"))
//                mOcrLanguage = 3;
//            else
//            if(s4.equals("4"))
//                mOcrLanguage = 4;
//            else
//            if(s4.equals("6"))
//                mOcrLanguage = 6;
//            else
//            if(s4.equals("7"))
//                mOcrLanguage = 7;
//        } else
//        if(s3.equals("AutoCap"))
//        {
//            if(s4.equals("0"))
//                mAutoCapture = false;
//            else
//                mAutoCapture = true;
//        } else
//        if(s3.equals("License"))
//            License = s4;
//        else
//        if(s3.equals("DC"))
//            mDownloadCount = Integer.parseInt(s4);
//        else
//        if(s3.equals("SC"))
//            mSaveCount = Integer.parseInt(s4);
//        else
//        if(s3.equals("INVITE"))
//            if(s4.equals("1"))
//                mInvite = true;
//            else
//                mInvite = false;
//          goto _L5
//_L2:
//        check_dataLicense();
//        flag = save();
//          goto _L7
//        check_dataLicense();
//          goto _L7
		return false;
    }

	private static void loaded() 
	{
		if (!loaded) 
		{
			loaded = true;
			load();
		}
	}

	public static int remainCount() 
	{
		loaded();
		return 30 - mSaveCount;
	}

	public static boolean save()
    {
//        String s;
//        boolean flag;
//        s = (new StringBuilder(String.valueOf(App.getBaseDir()))).append("/").append(CONFIG_NAME).toString();
//        flag = false;
//        FileWriter filewriter;
//        BufferedWriter bufferedwriter;
//        String s1;
//        filewriter = new FileWriter(new File(s));
//        bufferedwriter = new BufferedWriter(filewriter);
//        s1 = (new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf("Trialtimes="))).append(mTrialTimes).toString()))).append("\nGridNum=").toString()))).append(mGridNum).toString()))).append("\nSysCamera=").toString();
//        if(!mSysCamera) goto _L2; else goto _L1
//_L1:
//        String s2 = (new StringBuilder(String.valueOf(s1))).append("1").toString();
//_L17:
//        String s3 = (new StringBuilder(String.valueOf(s2))).append("\nBlurDet=").toString();
//        if(!mBlurDetectionOn) goto _L4; else goto _L3
//_L3:
//        String s4 = (new StringBuilder(String.valueOf(s3))).append("1").toString();
//_L18:
//        String s5 = (new StringBuilder(String.valueOf(s4))).append("\n+86=").toString();
//        if(!m86DetectionOn) goto _L6; else goto _L5
//_L5:
//        String s6 = (new StringBuilder(String.valueOf(s5))).append("1").toString();
//_L19:
//        String s7 = (new StringBuilder(String.valueOf(s6))).append("\nOneExTipOn=").toString();
//        if(!mOneExTipOn) goto _L8; else goto _L7
//_L7:
//        String s8 = (new StringBuilder(String.valueOf(s7))).append("1").toString();
//_L20:
//        String s9 = (new StringBuilder(String.valueOf(s8))).append("\nAllExTipOn=").toString();
//        if(!mAllExTipOn) goto _L10; else goto _L9
//_L9:
//        String s10 = (new StringBuilder(String.valueOf(s9))).append("1").toString();
//_L21:
//        String s11 = (new StringBuilder(String.valueOf(s10))).append("\nLoadCsvTipOn=").toString();
//        if(!mLoadCsvTipOn) goto _L12; else goto _L11
//_L11:
//        String s12 = (new StringBuilder(String.valueOf(s11))).append("1").toString();
//_L22:
//        String s13 = (new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf(s12))).append("\nUiLan=").toString()))).append(mUiLanguage).toString()))).append("\nOcrLan=").toString()))).append(mOcrLanguage).toString()))).append("\nAutoCap=").toString();
//        if(!mAutoCapture) goto _L14; else goto _L13
//_L13:
//        String s14 = (new StringBuilder(String.valueOf(s13))).append("1").toString();
//_L23:
//        String s15 = (new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf((new StringBuilder(String.valueOf(s14))).append("\nLicense=").toString()))).append(License).toString()))).append("\nDC=").toString()))).append(mDownloadCount).toString()))).append("\nSC=").toString()))).append(mSaveCount).toString()))).append("\nINVITE=").toString();
//        if(!mInvite) goto _L16; else goto _L15
//_L15:
//        String s17 = (new StringBuilder(String.valueOf(s15))).append("1").toString();
//_L24:
//        bufferedwriter.write(s17);
//        bufferedwriter.close();
//        filewriter.close();
//        flag = true;
//_L25:
//        Exception exception;
//        String s16;
//        if(License.length() > 0)
//            try
//            {
//                FileWriter filewriter1 = new FileWriter(new File(dataConfig));
//                BufferedWriter bufferedwriter1 = new BufferedWriter(filewriter1);
//                bufferedwriter1.write((new StringBuilder(String.valueOf("License="))).append(License).toString());
//                bufferedwriter1.close();
//                filewriter1.close();
//            }
//            catch(Exception exception1)
//            {
//                Debug.e("", exception1);
//            }
//        return flag;
//_L2:
//        s2 = (new StringBuilder(String.valueOf(s1))).append("0").toString();
//          goto _L17
//_L4:
//        s4 = (new StringBuilder(String.valueOf(s3))).append("0").toString();
//          goto _L18
//_L6:
//        s6 = (new StringBuilder(String.valueOf(s5))).append("0").toString();
//          goto _L19
//_L8:
//        s8 = (new StringBuilder(String.valueOf(s7))).append("0").toString();
//          goto _L20
//_L10:
//        s10 = (new StringBuilder(String.valueOf(s9))).append("0").toString();
//          goto _L21
//_L12:
//        s12 = (new StringBuilder(String.valueOf(s11))).append("0").toString();
//          goto _L22
//_L14:
//        s14 = (new StringBuilder(String.valueOf(s13))).append("0").toString();
//          goto _L23
//_L16:
//        s16 = (new StringBuilder(String.valueOf(s15))).append("0").toString();
//        s17 = s16;
//          goto _L24
//        exception;
//        Debug.e("", exception);
//          goto _L25
		return false;
    }

	public static void set86Detection(boolean flag) 
	{
		loaded();
		m86DetectionOn = flag;
	}

	public static void setAllExTipOn(boolean flag) 
	{
		loaded();
		mAllExTipOn = flag;
	}

	public static void setAutoCapture(boolean flag) 
	{
		loaded();
		mAutoCapture = flag;
	}

	public static void setBlurDetection(boolean flag) 
	{
		loaded();
		//mBlurDetectionOn = flag;
	}

	public static void setDownloadCount(int i) 
	{
		loaded();
		mDownloadCount = i;
	}

	public static void setGridNum(int i) 
	{
		loaded();
		mGridNum = i;
	}

	public static void setIsInvite(boolean flag) 
	{
		loaded();
		mInvite = flag;
	}

	public static void setKeyLanguage(int i) 
	{
		loaded();
		mKeyLanguage = i;
	}

	public static void setLicense(String s) 
	{
		loaded();
		License = s;
	}

	public static void setLoadCsvTipOn(boolean flag) 
	{
		loaded();
		mLoadCsvTipOn = flag;
	}

	public static void setOcrLanguage(int i) 
	{
		loaded();
		mOcrLanguage = i;
	}

	public static void setOneExTipOn(boolean flag)
	{
		loaded();
		mOneExTipOn = flag;
	}

	public static void setPimPwd(String s) 
	{
		loaded();
		mPimPwd = s;
	}

	public static void setPimUName(String s) 
	{
		loaded();
		mPimUName = s;
	}

	public static void setSaveCount(int i) 
	{
		loaded();
		mSaveCount = i;
	}

	public static void setSyncAllBizcardTipOn(boolean flag) 
	{
		loaded();
		mSyncAllBizcardTipOn = flag;
	}

	public static void setSyncBizcardTipOn(boolean flag) 
	{
		loaded();
		mSyncBizcardTipOn = flag;
	}

	public static void setSysCamera(boolean flag) 
	{
		loaded();
		mSysCamera = flag;
	}

	public static void setTrialTimes() 
	{
		loaded();
		mTrialTimes = 1 + mTrialTimes;
	}

	public static void setUiLanguage(int i) 
	{
		loaded();
		mUiLanguage = i;
	}

	public void finalize() 
	{
	}

	public void reset() 
	{
		m86DetectionOn = false;
		//mBlurDetectionOn = true;
		mAutoCapture = true;
		mOcrLanguage = 1;
		mKeyLanguage = 1;
		mUiLanguage = 1;
	}

	public static String CONFIG_NAME;
	public static String License = "";
	//private static final String TAG = "Setting";
	//private static final String TAG_86_DETECTION = "+86";
	//private static final String TAG_AUTO_CAPTURE = "AutoCap";
	//private static final String TAG_AllExTipOn_Show = "AllExTipOn";
	//private static final String TAG_BLUR_DETECTION = "BlurDet";
	//private static final String TAG_DOWNLOAD_COUNT = "DC";
	//private static final String TAG_Grid = "GridNum";
	//private static final String TAG_INVITE = "INVITE";
	//private static final String TAG_License = "License";
	//private static final String TAG_LoadCsvTipOn_Show = "LoadCsvTipOn";
	//private static final String TAG_OCR_LANGUAGE = "OcrLan";
	//private static final String TAG_OneExTipOn_Show = "OneExTipOn";
	//private static final String TAG_SAVE_COUNT = "SC";
	//private static final String TAG_SYS_CAMERA = "SysCamera";
	//private static final String TAG_TRIAL_TIMES = "Trialtimes";
	//private static final String TAG_UI_LANGUAGE = "UiLan";
	public static final int UI_LAN_CHINESE_S = 3;
	public static final int UI_LAN_CHINESE_T = 4;
	public static final int UI_LAN_ENGLISH = 1;
	public static final int UI_LAN_JAPANESE = 2;
	public static final int UI_LAN_NONE = 0;
	//private static String dataConfig;
	public static boolean loaded = false;
	private static boolean m86DetectionOn = false;
	private static boolean mAllExTipOn = true;
	private static boolean mAutoCapture = true;
	//private static boolean mBlurDetectionOn = true;
	private static int mDownloadCount = 0;
	private static int mGridNum = 3;
	private static boolean mInvite = true;
	private static int mKeyLanguage = 1;
	private static boolean mLoadCsvTipOn = true;
	private static int mOcrLanguage = 2;
	private static boolean mOneExTipOn = true;
	private static String mPimPwd = "";
	private static String mPimUName = "";
	private static int mSaveCount = 0;
	private static boolean mSyncAllBizcardTipOn = true;
	private static boolean mSyncBizcardTipOn = true;
	private static boolean mSysCamera = false;
	private static int mTrialTimes = 0;
	private static int mUiLanguage = 1;

	static {
		CONFIG_NAME = "bcr.cfg";
		//dataConfig = (new StringBuilder(String.valueOf(App.getAppPath()))).append("/").append(CONFIG_NAME).toString();
	}
}
