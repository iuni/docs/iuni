package com.ai.cmccha.sc.idcard.util;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

public class BitmapUtil
{
	// 缩放图像
	public static Bitmap scaleBitmap(Bitmap bitmap, float scale)
	{
		if (scale == 1.0f)
		{
			return bitmap;
		}
		else
		{
			return Bitmap.createScaledBitmap(bitmap, (int) (scale * (float) bitmap.getWidth()),
					(int) (scale * (float) bitmap.getHeight()), true);
		}
	}

	// 按照固定的尺寸解码照片文件
	public static Bitmap decodeFile(String fileName, int wantWidth, int wantHeight)
	{
		// 动态调整opts的inSampleSize，从而达到每个图片取出来的高度都是75px
		BitmapFactory.Options opts = new BitmapFactory.Options();
		// 设置inJustDecodeBounds为true后，decodeFile并不分配空间，但可计算出原始图片的长度和宽度，即opts.width和opts.height
		opts.inJustDecodeBounds = true;
		// 默认只加载SD卡中的文件，获得这个图片的宽和高
		BitmapFactory.decodeFile(fileName, opts);
		opts.inJustDecodeBounds = false;
		int be = 0;
		float scale = 0;
		if (opts.outWidth == 0 || opts.outHeight == 0)
		{
			return null;
		}
		if (opts.outHeight / opts.outWidth >= (wantHeight / wantWidth))
		{
			wantWidth = (int) (wantHeight * opts.outWidth / opts.outHeight);
			scale = opts.outHeight / (float) wantHeight;
		}
		else
		{
			wantHeight = (int) (wantWidth * opts.outHeight / opts.outWidth);
			scale = opts.outWidth / (float) wantWidth;
		}
		if (scale <= 1)
		{
			be = 1;
		}
		else if (scale % 1 == 0)
		{
			be = (int) scale;
		}
		else
		{
			be = (int) scale + 1;
		}
		opts.inSampleSize = be;
		Bitmap bitmap = BitmapFactory.decodeFile(fileName, opts);
		Bitmap newBitmap = null;
		if (scale % 1 != 0)
		{
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			float scaleWidth = ((float) wantWidth) / width;
			float scaleHeight = ((float) wantHeight) / height;
			Matrix matrix = new Matrix();
			matrix.postScale(scaleWidth, scaleHeight);
			newBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
			bitmap.recycle();
		}
		else
		{
			newBitmap = bitmap;
		}
		bitmap = null;
		return newBitmap;
	}

	public static byte[] Bitmap2Bytes(Bitmap bm)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}
}
