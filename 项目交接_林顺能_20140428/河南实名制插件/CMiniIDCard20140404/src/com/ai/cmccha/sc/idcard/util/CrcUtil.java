package com.ai.cmccha.sc.idcard.util;

// 调用方法new CCR_exchange({0x34}).getCRC();
public class CrcUtil 
{
	/* CRC余式表 */
	private static char[] Pro_General_CrcTbl = new char[]
			{
		0x0000, 0x1021, 0x2042, 0x3063, 
		0x4084, 0x50A5, 0x60C6, 0x70E7, 
		0x8108, 0x9129, 0xA14A, 0xB16B, 
		0xC18C, 0xD1AD, 0xE1CE, 0xF1EF
	};
	
	/***********************************************************************
	* 函数名称 : Pro_General_CalcCrc
	* 函数功能 : 计算Crc检验码函数(CCITT 16)
	* 入口参数 : ptr     :    字符串首地址
	*           crc_pre :    前个CRC值
	*           len     :    有效数据包长度
	* 出口参数 : crc值
	* 注意事项 : 无
	* 修改记录 : YYYY/MM/DD BY
	***********************************************************************/
	public static byte[] Pro_General_CalcCrc(byte[] ptr, int crc_pre, int len)
	{
		char da;
		char crc = (char)crc_pre;
		
		for(int index=0; index<len; index++)
		{
			// 暂存CRC的高4位
			da = (char)(crc>>12);
			// 取CRC的低12位
			crc <<= 4;
			// CRC高4位和本字节的高4位相加后查表计算CRC,再加前次CRC的余数
			crc ^= Pro_General_CrcTbl[(da^(ptr[index]>>4))&0x0f];
			//System.out.printf("\r\n ***%04x*** \r\n", (int)crc);

			// 暂存CRC的高4位
			da = (char)(crc>>12);
			// 取CRC的低12位
			crc <<= 4;
			// CRC高4位和本字节的低4位相加后查表计算CRC,再加前次CRC的余数
			crc ^= Pro_General_CrcTbl[(da^(ptr[index]&0x0f))&0x0f];
			//System.out.printf("\r\n ###%04x### \r\n", (int)crc);
		}
		
		byte [] temp = new byte[2];
		temp[0] = (byte)crc;
		temp[1] = (byte)(crc>>8);
		return temp;
	}
}
