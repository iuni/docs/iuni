package com.ai.cmccha.sc.idcard.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneFormatUtil
{
	// 电话号码格式判断
	public static boolean checkPhone(String phone)
	{	//"^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$"
		//Pattern pattern = Pattern.compile("^\\d{3}-?\\d{8}|\\d{4}-?\\d{8}$");
		Pattern pattern = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher matcher = pattern.matcher(phone);
		if (matcher.matches())
		{
			return true;
		}
		return false;
	}
	
	/**
     * 验证手机号码（支持国际格式，+86135xxxx...（中国内地），+00852137xxxx...（中国香港））
     * @param mobile 移动、联通、电信运营商的号码段
     *<p>移动的号段：134(0-8)、135、136、137、138、139、147（预计用于TD上网卡）
     *、150、151、152、157（TD专用）、158、159、187（未启用）、188（TD专用）</p>
     *<p>联通的号段：130、131、132、155、156（世界风专用）、185（未启用）、186（3g）</p>
     *<p>电信的号段：133、153、180（未启用）、189</p>
     * @return 验证成功返回true，验证失败返回false
     */ 
    private static boolean checkMobile(String mobile) 
    { 
        String regex = "(\\+\\d+)?1[3458]\\d{9}$"; 
        return Pattern.matches(regex,mobile); 
    } 
    
    /**
     * 验证固定电话号码
     * @param phone 电话号码，格式：国家（地区）电话代码 + 区号（城市代码） + 电话号码，如：+8602085588447
     * <p><b>国家（地区） 代码 ：</b>标识电话号码的国家（地区）的标准国家（地区）代码。它包含从 0 到 9 的一位或多位数字，
     *  数字之后是空格分隔的国家（地区）代码。</p>
     * <p><b>区号（城市代码）：</b>这可能包含一个或多个从 0 到 9 的数字，地区或城市代码放在圆括号——
     * 对不使用地区或城市代码的国家（地区），则省略该组件。</p>
     * <p><b>电话号码：</b>这包含从 0 到 9 的一个或多个数字 </p>
     * @return 验证成功返回true，验证失败返回false
     */ 
    public static boolean checkFixPhone(String phone) 
    { 
        String regex = "(\\+\\d+)?(\\d{3,4}\\-?)?\\d{7,8}$"; 
        return Pattern.matches(regex, phone); 
    } 
    
	public static int checkPhoneNew(String phone)
	{
		// 判断是否为固定电话
		if(checkFixPhone(phone))
		{
			return 1;
		}
		
		// 判断是否为手机号码
		else if(phone.length() == 11)
		{
			if(checkMobile(phone))
			{
				return 2;
			}
			return 0;
		}
		else 
		{
			// 为其他号码的情况
			return 3;
		}
	}
	
	// 价钱格式是否正确判断
	public static boolean isPrice(String str)
	{
		int begin = 0;
		boolean once = true;
		//判断数值是否为空
		if(str == null || str.trim().equals(""))
		{
			return false;
		}
		str = str.trim();
		
		if (str.startsWith("+") || str.startsWith("-")) 
		{
			//只有加减号
			if (str.length() == 1)
	        {
				// "+" "-"
				return false;
			}
			begin = 1;
		}
		for (int i = begin; i < str.length(); i++) 
		{
			if (!Character.isDigit(str.charAt(i)))
			{
				if (str.charAt(i) == '.' && once) 
				{
					// '.' can only once
					once = false;
				}
				else
				{
					return false;
				}
			}
		}
		if (str.length() == (begin + 1) && !once)
		{
			// "." "+." "-."
			return false;
		}
		return true;
	}
}