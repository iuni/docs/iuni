package com.ai.cmccha.sc.idcard.listener;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.ai.cmccha.sc.idcard.util.ButtonUtil;

// ��ֹ������
public class SpinnerTouchListener implements OnTouchListener
{
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		if(event.getAction() == MotionEvent.ACTION_DOWN)
		{
			return ButtonUtil.isFastDoubleClick(v.getId());
		}
		return false;
	}
}