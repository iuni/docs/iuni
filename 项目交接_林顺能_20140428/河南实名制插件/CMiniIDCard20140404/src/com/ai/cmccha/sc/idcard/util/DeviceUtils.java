package com.ai.cmccha.sc.idcard.util;

import java.io.File;
import java.text.NumberFormat;

import com.ai.cmccha.sc.idcard.App;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class DeviceUtils 
{
	public static final String 	SDCARD_ROOT_PATH 	= Environment.getExternalStorageDirectory().getPath();
	public static final String 	SDCARD_BASE_PATH 	= SDCARD_ROOT_PATH + "/idcard";
	public static String 		mStrImei			= null;

	public static boolean checkExternalMemoryAvailable() 
	{
		boolean flag;
		if (getExternalMemoryAvailableSize()/1024L/1024L < 1L) 
		{
			flag = false;
		}
		else
		{
			flag = true;
		}
		return flag;
	}

	public static long getExternalMemoryAvailableSize() 
	{
		long l;
		if (Environment.getExternalStorageState().equals("mounted")) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			l = (long) statfs.getBlockSize() * (long) statfs.getAvailableBlocks();
		} 
		else 
		{
			l = -1L;
		}
		return l;
	}
	
	public static String getDeviceId() 
	{
		if(!StringUtil.isEmptyOrNull(mStrImei)) 
		{
			return mStrImei;
		}
		TelephonyManager tm = (TelephonyManager) App.context().getSystemService(Context.TELEPHONY_SERVICE);
		if(tm != null) 
		{
			String strImei = tm.getDeviceId();
			if(!StringUtil.isEmptyOrNull(strImei)) 
			{
				mStrImei = strImei;
				return mStrImei;
			}
		}
		return "000000000000000";
	}
	
	public static void hideSoftKeyboard(View view) 
	{
		((InputMethodManager) App.context().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	public static boolean hasInternet() 
	{
		boolean flag;
		if (((ConnectivityManager) App.context().getSystemService(
				Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null)
			flag = true;
		else
			flag = false;
		return flag;
	}
	
	public static int getVersionCode() 
	{
		int versionCode = 0;
		try {
			versionCode = App.context().getPackageManager().getPackageInfo(App.context().getPackageName(), 0).versionCode;
			Log.e("", "versionCode = " + versionCode);
		} catch (PackageManager.NameNotFoundException ex) {
			versionCode = 0;
		}
		return versionCode;
	}
	
	public static String getVersionName() 
	{
		String name = "";
		try 
		{
			name = App.context().getPackageManager().getPackageInfo(App.context().getPackageName(), 0).versionName;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			name = "";
		}
		return name;
	}
	
	public static void installAPK(Context context,File file) 
	{
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),"application/vnd.android.package-archive");
		context.startActivity(intent);
	}
	
	public static String percent(double p1, double p2) 
	{
		String str;
		double p3 = p1 / p2;
		NumberFormat nf = NumberFormat.getPercentInstance();
		nf.setMinimumFractionDigits(2);
		str = nf.format(p3);
		return str;
	}
}
