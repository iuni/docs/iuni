package com.ai.cmccha.sc.idcard.bean;

import android.graphics.Bitmap;

public class ContectInfo 
{
	private 	String 		name;
	private 	String 		number;
	private 	Bitmap 		photo;
	private     long 		raw_contact_id;
	
	public long getRaw_contact_id() 
	{
		return raw_contact_id;
	}
	
	public void setRaw_contact_id(long raw_contact_id)
	{
		this.raw_contact_id = raw_contact_id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getNumber()
	{
		return number;
	}
	
	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public Bitmap getPhoto()
	{
		return photo;
	}
	
	public void setPhoto(Bitmap photo) 
	{
		this.photo = photo;
	}
}
