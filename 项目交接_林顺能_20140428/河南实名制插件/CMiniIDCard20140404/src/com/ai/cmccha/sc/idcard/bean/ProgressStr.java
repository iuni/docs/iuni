package com.ai.cmccha.sc.idcard.bean;

public class ProgressStr 
{
	public static String[][] FinaceSignInStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在签到, 请稍候...", 				"80"}
	};
	
	public static String[][] FinaceConsumeStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在消费, 请稍候...", 				"60"},
		{"正在获取打印信息...", 				"70"},
		{"正在打印...", 						"80"}
	};
	
	public static String[][] FinaceUnConsumeStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在消费撤销, 请稍候...", 			"60"},
		{"正在获取打印信息...", 				"70"},
		{"正在打印...", 						"80"}
	};
	
	public static String[][] FinaceInquiryStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在查询银行卡余额, 请稍候...", 	"60"}
	};
	
	public static String[][] FinaceAtmWithdrawStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在助农取款, 请稍候...", 			"60"},
		{"正在获取打印信息...", 				"70"},
		{"正在打印...", 						"80"}
	};
	
	public static String[][] FinaceClearingStr = 
	{
		{"正在获取服务器参数...", 			"20"},
		{"正在连接服务器...", 				"30"},
		{"正在初始化MISPOS模块...", 			"50"},
		{"正在结算, 请稍候...", 				"60"},
		{"正在获取结算信息...", 				"70"},
		{"正在打印结算信息...", 				"80"}
	};
	
	public static String[][] FinacePrintStepStr = 
	{
		{"正在初始化MISPOS模块...",  		"30"},
		{"正在获取MISPOS参数...", 			"50"},
		{"正在获取打印参数...", 				"70"},
		{"正在打印...", 						"85"}
	};
}
