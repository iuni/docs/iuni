package com.ai.cmccha.sc.idcard.dialog;

import com.ai.cmccha.sc.idcard.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

// 滚动条框类
public class ProgressDialog extends Dialog implements View.OnClickListener
{
	private Button 						mNegativeButton 		= null;					// 取消按钮
	private View.OnClickListener 		mNegativeListener		= null;					// 取消按钮监听
	
	private TextView 					mTitleView 				= null;					// 标题文本框
	private TextView 					mMessageView 			= null;					// 消息文本框
	private TextView 					mPercentView 			= null;					// 消息文本框
	private TextView 					mCountView 				= null;					// 消息文本框
	private ImageView 					mIconView 				= null;					// 图标控件

	private ProgressBar 				mLengthBar 				= null;
	private ProgressBar 				mCircleBar 				= null;
	
	public static ProgressDialog getInstance(Context context)
	{
		return new ProgressDialog(context, R.style.transparent_dialog);
	}
	
	public ProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener)
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public ProgressDialog(Context context, int theme)
	{
		super(context, theme);
		initView();
	}

	public ProgressDialog(Context context)
	{
		super(context);
		initView();
	}
	
	// 设置滚动条标题
	@Override
	public void setTitle(CharSequence titleStr)
	{
		mTitleView.setVisibility(View.VISIBLE);
		mTitleView.setText(titleStr);
	}
	
	// 设置滚动条消息
	public void setMessage(CharSequence msgStr)
	{
		mMessageView.setVisibility(View.VISIBLE);
		mMessageView.setText(msgStr);
	}
	
	// 初始化滚动条视图
	private void initView()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.progress_dialog);

		mTitleView 		= (TextView)findViewById(R.id.titleTextView);
		mMessageView 	= (TextView)findViewById(R.id.contentTextView);
		mPercentView 	= (TextView)findViewById(R.id.percentTextView);
		mCountView 		= (TextView)findViewById(R.id.countTextView);
		mNegativeButton = (Button)findViewById(R.id.cancelButton);
		mIconView 		= (ImageView)findViewById(R.id.iconImageView);
		mLengthBar 		= (ProgressBar)findViewById(R.id.lengthProgressBar);
		mCircleBar 		= (ProgressBar)findViewById(R.id.circleProgressBar);
		
		mLengthBar.setMax(1);
		mCircleBar.setMax(1);
		mNegativeButton.setOnClickListener(this);
		mNegativeButton.setVisibility(View.GONE);
		mLengthBar.setVisibility(View.GONE);
		
		mPercentView.setText("0%");
		mCountView.setText("0/0");
		mMessageView.setMovementMethod(ScrollingMovementMethod.getInstance()); 
		mMessageView.setVisibility(View.GONE);
		mTitleView.setVisibility(View.GONE);
		findViewById(R.id.percentLayout).setVisibility(View.GONE);
	}
	
	// 设置取消按钮
	public void setNegetiveListener(String buttonStr, View.OnClickListener negetiveListener)
	{
		mNegativeButton.setText(buttonStr);
		mNegativeButton.setVisibility(View.VISIBLE);
		mNegativeListener = negetiveListener;
	}

	// 设置图标
	public void setIcon(Bitmap bitmap)
	{
		mIconView.setImageBitmap(bitmap);
	}
	
	public void setIcon(int resId)
	{
		mIconView.setImageResource(resId);
	}
	
	public void setIcon(Drawable drawable)
	{
		mIconView.setImageDrawable(drawable);
	}
	
	public void setProgress(int progress)
	{
		mLengthBar.setProgress(progress);
		mCircleBar.setProgress(progress);
		
		mPercentView.setText(((int)progress*100/mLengthBar.getMax())+"%");
		mCountView.setText(progress+"/"+mLengthBar.getMax());
	}
	
	public void setMax(int max)
	{
		mLengthBar.setMax(max);
		mCircleBar.setMax(max);
	}

	// 设置
	public void setIndeterminate(boolean indeterminate)
	{
		if(indeterminate)
		{
			mLengthBar.setVisibility(View.GONE);
			mCircleBar.setVisibility(View.VISIBLE);
			findViewById(R.id.percentLayout).setVisibility(View.GONE);
			
		}else
		{
			mCircleBar.setVisibility(View.GONE);
			mLengthBar.setVisibility(View.VISIBLE);
			findViewById(R.id.percentLayout).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v)
	{
		dismiss();
		if(v == mNegativeButton && mNegativeListener != null)
		{
			mNegativeListener.onClick(v);
		}
	}
}
