package com.ai.cmccha.sc.idcard.pack;

public class InData 
{
	private Pdu 		mPdu;
	private String 		mStrID;							// 主命令
	private String 		mStrSubID;						// 子命令
	private String 		mStrServiceCommandID;			// 业务时序
	private int 		mCheckByte;
	private int 		mType;
	private String 		mStrErrorMsg;
	private boolean 	mIsError = false;

	// 获取PDU
	public Pdu getPdu() 
	{
		return mPdu;
	}

	// 设置PDU
	public void setPdu(Pdu pdu) 
	{
		this.mPdu = pdu;
	}
	
	// 获取主命令
	public String getId()
	{
		return mStrID;
	}

	// 设置主命令
	public void setId(String id) 
	{
		this.mStrID = id;
	}
	
	// 获取子命令
	public String getSubid() 
	{
		return mStrSubID;
	}
	
	// 设置子命令
	public void setSubid(String subid) 
	{
		this.mStrSubID = subid;
	}
	
	// 获取业务时序
	public String getServiceCommandID() 
	{
		return mStrServiceCommandID;
	}

	// 设置业务时序
	public void setServiceCommandID(String serviceCommandID) 
	{
		this.mStrServiceCommandID = serviceCommandID;
	}
	
	public int getCheckByte() 
	{
		return mCheckByte;
	}

	public void setCheckByte(int checkByte) 
	{
		this.mCheckByte = checkByte;
	}

	public int getType() 
	{
		return mType;
	}
	
	public void setType(int type) 
	{
		this.mType = type;
	}

	// 获取错误信息
	public String getErrorMsg() 
	{
		return mStrErrorMsg;
	}

	// 获取错误信息
	public void setErrorMsg(String errorMsg) 
	{
		this.mStrErrorMsg = errorMsg;
	}
	
	public boolean isError() 
	{
		return mIsError;
	}
	
	public void setError(boolean isError) 
	{
		this.mIsError = isError;
	}
}

