package com.ai.cmccha.sc.idcard.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil
{
	// 获取当前时间 ->14位 字符串
	public static String GetTimeString14() 
	{
		try 
		{
			// 定义格式
			SimpleDateFormat strTime = new SimpleDateFormat("yyyyMMddhhmmss");
			strTime.setTimeZone(strTime.getTimeZone());
			// 获取日期
			return strTime.format(new Date());
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// 将格式为20120909121212转换为2012-09-09 12:12:12
	public static String converDateString(String timeStr) 
	{
		if(StringUtil.isEmptyOrNull(timeStr)) 
		{
			return "时间格式错误";
		}
		
		StringBuffer sb = new StringBuffer(timeStr.trim());
		if(timeStr.length() > 3) 
		{
			sb.insert(4, '-');
		}
		if(timeStr.length() > 6) 
		{
			sb.insert(7, '-');
		}
		if(timeStr.length() > 8) 
		{
			sb.insert(10, ' ');
		}
		if(timeStr.length() > 10) 
		{
			sb.insert(13, ':');
		}
		if(timeStr.length() > 12)
		{
			sb.insert(16, ':');
		}
		return sb.toString();
	}
	
	// 格式化时间
	public static String formatDate(Date date, String pattern) 
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	// 时间格式判断
	public static boolean checkDate(String date, String format) 
	{
		DateFormat df = new SimpleDateFormat(format);
		Date d = null;
		try
		{
			d = df.parse(date);
        }
		catch(Exception e)
		{
			// 如果不能转换,肯定是错误格式
            return false;
        }
        String s1 = df.format(d);
        // 转换后的日期再转换回String,如果不等,逻辑错误.如format为"yyyy-MM-dd",date为
        // "2006-02-31",转换为日期后再转换回字符串为"2006-03-03",说明格式虽然对,但日期
        // 逻辑上不对.
        return date.equals(s1);
    }
}