package com.ai.cmccha.sc.idcard.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.Environment;
import android.util.Log;

import com.ai.cmccha.sc.idcard.pack.Buffer;
import com.ai.cmccha.sc.idcard.uilogic.UIHandler;

public class HttpInterface
{
	private static String  TAG 			 		=  "HttpInterface"; 
	private static URL 	   mUrl; 											// 地址
	private static String  mHttpUrl;										// 地址
	private static int 	   CONN_TIME_OUT  		=  10000;					// 连接超时时间10S
	private static int 	   RECV_TIME_OUT  		=  10000;			    	// 接收超时时间10s
	private static int     READ_TIME_OUT  		=  10000;					// 读取超时10s
	
	// 设置连接超时时间
	static boolean setConnTimeOut(int time_out)
	{
		CONN_TIME_OUT = time_out;
		return true;
	}
	
	// 设置接收超时时间
	static boolean setRecvTimeOut(int time_out)
	{
		RECV_TIME_OUT = time_out;
		return true;
	}
	
	// 设置HTTP路径
	public static boolean setHttpUrl(String url)
	{
		try 
		{
			mHttpUrl = url;
			mUrl 	 = null;
			mUrl 	 = new URL(url);
		} 
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	// 创建文件所在的目录
	public static void makeDir(String path)
	{
		File file = new File(path);
		// 父文件存在
		if(file.getParentFile().exists())
		{
			file.mkdir();
		}
		// 父文件不存在
		else
		{
			makeDir(file.getParent());
			file.mkdir();
		}
	}

	// 检查本地文件是否存在，返回长度值
	public static long localFileExists(String path)
	{
		File file = new File(path);
		// 本地文件不存在
		if( !file.exists() )
		{
			return 0;
		}
		else
		{
			return file.length();
		}
	}

	// http断点下载版本
	public static int breakpointDownLoadXml(String remotePath, String localPath)
	{
		int result = 0;
		if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			UIHandler.MsgBoxOne("SD卡不可用，请插入SD卡");
		}
		else
		{
			HttpURLConnection urlConnection = null;
			RandomAccessFile accessFilec = null;
			InputStream is = null;
			long tempBreakPoint = localFileExists(localPath);
			long totalLength = 0;
			File file = null;
			try
			{
				//先连接确定远程文件的大小
				urlConnection = (HttpURLConnection)new URL(remotePath).openConnection();
				urlConnection.setConnectTimeout(CONN_TIME_OUT);
				urlConnection.setReadTimeout(READ_TIME_OUT);
				totalLength = urlConnection.getContentLength();
				urlConnection.disconnect();
				urlConnection = null;
				//远程文件不存在
				if(totalLength == 0)
				{
					result = 0;
				}
				//本地文件大于远程文件
				else if(totalLength < tempBreakPoint)
				{
					result = 1;
				}
				//文件已经存在
				else if(totalLength == tempBreakPoint)
				{
					result = 2;
				}
				//下载
				else
				{
					//开始下载
					file = new File(localPath);
					accessFilec = new RandomAccessFile(file,"rwd");
					if(totalLength != accessFilec.length())
					{
						//设定文件的大小
						accessFilec.setLength(tempBreakPoint);
					}
					urlConnection = (HttpURLConnection)new URL(remotePath).openConnection();
					urlConnection.setConnectTimeout(CONN_TIME_OUT);
					urlConnection.setReadTimeout(READ_TIME_OUT);
					urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-"+totalLength);
					Log.v("下载APK", "设定断点:" + tempBreakPoint);
					accessFilec.seek(tempBreakPoint);
					byte[] buf = new byte[1024*2];
					int len = 0;
					is = urlConnection.getInputStream();
					while((len = is.read(buf))>0)
					{
						accessFilec.write(buf, 0, len);
						tempBreakPoint += len;
					}
					result = 3;
				}
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
			finally
			{
				UIHandler.initBusiStop();
				if(is!= null)
				{
					try
					{
						is.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				if(accessFilec != null)
				{
					try
					{
						accessFilec.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}	
				if(urlConnection != null)
				{
					urlConnection.disconnect();
					urlConnection = null;
				}
			}
		}
		return result;
	}
	
	
	// http断点下载
	public static int breakpointDownLoad(String remotePath, String localPath)
	{
		int result = 4;
		if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			UIHandler.MsgBoxOne("SD卡不可用，请插入SD卡");
		}
		else
		{
			HttpURLConnection urlConnection = null;
			RandomAccessFile accessFilec = null;
			InputStream is = null;
			long tempBreakPoint = localFileExists(localPath);
			long totalLength = 0;
			File file = null;
			int progress = 0;
			int count = 0;
			try
			{
				//先连接确定远程文件的大小
				urlConnection = (HttpURLConnection)new URL(remotePath).openConnection();
				urlConnection.setConnectTimeout(CONN_TIME_OUT);
				urlConnection.setReadTimeout(READ_TIME_OUT);
				totalLength = urlConnection.getContentLength();
				urlConnection.disconnect();
				urlConnection = null;
				//远程文件不存在
				if(totalLength == 0)
				{
					result = 0;
				}
				//本地文件大于远程文件
				else if(totalLength < tempBreakPoint)
				{
					result = 1;
				}
				//文件已经存在
				else if(totalLength == tempBreakPoint)
				{
					result = 2;
				}
				//下载
				else
				{
					//开始下载
					file = new File(localPath);
					accessFilec = new RandomAccessFile(file,"rwd");
					if(totalLength != accessFilec.length())
					{
						//设定文件的大小
						accessFilec.setLength(tempBreakPoint);
					}
					urlConnection = (HttpURLConnection)new URL(remotePath).openConnection();
					urlConnection.setConnectTimeout(CONN_TIME_OUT);
					urlConnection.setReadTimeout(READ_TIME_OUT);
					urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-"+totalLength);
					Log.v("下载APK", "设定断点:" + tempBreakPoint);
					accessFilec.seek(tempBreakPoint);
					byte[] buf = new byte[1024*2];
					int len = 0;
					is = urlConnection.getInputStream();
					while((len = is.read(buf))>0)
					{
						accessFilec.write(buf, 0, len);
						tempBreakPoint += len;
						count ++;
						if(count == 5)
						{
							count = 0;
							progress = (int) (tempBreakPoint*100.0f/totalLength);
							UIHandler.initBusiStep("下载进度","下载进度..." , progress);
							android.util.Log.v("当前下载量:", tempBreakPoint+"/"+totalLength);
						}
					}
					result = 3;
				}
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
			finally
			{
				UIHandler.initBusiStop();
				if(is != null)
				{
					try
					{
						is.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				if(accessFilec != null)
				{
					try
					{
						accessFilec.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}	
				if(urlConnection != null)
				{
					urlConnection.disconnect();
					urlConnection = null;
				}
			}
		}
		return result;
	}
	
	// 发送数据并且接收
	public static boolean sendData(byte[] send_data, Buffer[] buffer)
	{
		return sendData(send_data, buffer, RECV_TIME_OUT);
	}
	
	// 发送并接收
	// recv_len[0] 默认buffer的大小,不传为4k
	// 这里用HttpURLConnection实现,当然也可以HttpClient实现
	public static boolean sendData(byte[] send_data, Buffer[] buffer, int time_out)
	{
		int send_len = 0;
		// 连接
		HttpURLConnection httpConn 		= null;
		// 输入流
		InputStream 	  inputStream 	= null;
		// 输出流
		OutputStream 	  outputStream	= null;	 
		// 是否接收成功
		boolean bSuccess  = false;
		byte[]  temp_buf  = new byte[1024];
		int temp_len = 0;
		try 
		{
			send_data = new String(send_data).getBytes("gb2312");
			send_len  = send_data.length;
		} 
		catch (UnsupportedEncodingException e1)
		{
			e1.printStackTrace();
		}
		if (mHttpUrl == null || mUrl == null) 
		{
			return false;
		}
		try 
		{ 
			// 创建连接
			httpConn = (HttpURLConnection) mUrl.openConnection();
			if(httpConn == null)
			{
				Log.e(TAG, "创建连接失败...\n");
				return false;
			}
			// 连接超时
			httpConn.setConnectTimeout(CONN_TIME_OUT);
			// 接收超时时间
			httpConn.setReadTimeout(time_out);
			// 允许输入输出
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);  
			httpConn.setRequestMethod("POST");
			// POST请求不能使用缓存  
			httpConn.setUseCaches(false);
			// 类型Message with multiple, related parts
			// http://blog.sina.com.cn/s/blog_86d3fabb01012lp4.html
			httpConn.setRequestProperty("Content-Type", "multipart/related");
			// 这里需要给出文本的长度,否则报错
			httpConn.setRequestProperty("Content-length", "" + send_len);
			httpConn.setRequestProperty("charset", "gb2312");
			// 连接,当然网上说getOutputStream会调Connect
			httpConn.connect();
			
			// 阻塞，不能调用获取返回码
			// resp_code = httpConn.getResponseCode();
			// System.out.println("resp_code: " + resp_code);
			
			// 获取输入流
			outputStream = httpConn.getOutputStream();
			if (outputStream == null) 
			{
				Log.e(TAG, "连接服务器失败,地址:" + mHttpUrl + "\n");
				httpConn.disconnect();
				httpConn = null;
				return false;
			}
			                 
			// 发送
			outputStream.write(send_data, 0, send_len);
			// 写完关闭
			outputStream.close();
			outputStream 	= null; 
		
			// 获取输入流
			inputStream  = httpConn.getInputStream();
			if (inputStream == null) 
			{
				Log.e(TAG, "连接服务器失败,地址:" + mHttpUrl + "\n");
				httpConn.disconnect();
				httpConn = null;
				return false;
			}
			 
			// 接收
			while ((temp_len = inputStream.read(temp_buf, 0, 1024)) > 0) 
			{
				for (int i = 0; i < temp_len; i++) 
				{
					buffer[0].add(temp_buf[i]);
				}
			} 
						
			inputStream.close();
			inputStream = null;
			// 成功
			bSuccess = true;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				// 关闭连接
				if(httpConn != null)
				{
					httpConn.disconnect();
					httpConn = null;
				}
				if (inputStream != null) 
				{
					inputStream.close();
					inputStream = null;
				}
				if (outputStream != null) 
				{
					outputStream.close();
					outputStream = null;
				} 
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		return bSuccess;
	}
}
