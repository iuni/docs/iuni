package com.ai.cmccha.sc.plugin;

public abstract class PluginInterface
{
	protected String	label;
	protected String	desc;
	protected int		icon;

	public String getLabel()
	{
		return label;
	}

	public String getDesc()
	{
		return desc;
	}

	public int getIcon()
	{
		return icon;
	}
}
