package com.ai.cmccha.sc.idcard.bean;

public class VersionInfo 
{
	private String version;
	private String ccr;
	
	public VersionInfo(String version, String ccr)
	{
		this.version = version;
		this.ccr	 = ccr;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version) 
	{
		this.version = version;
	}

	public String getCcr() 
	{
		return ccr;
	}

	public void setCcr(String ccr) 
	{
		this.ccr = ccr;
	}
}
