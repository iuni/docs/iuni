package com.ai.cmccha.sc.idcard.adpater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public abstract class DBadapter
{
	private Context					m_context;
	public static SQLiteDatabase	db;
	// 防止两个线程同时访问，一个访问结束，而另一个还未结束
	private static Semaphore		m_sem	= new Semaphore(1);
	private DBHelper				dbhelper;
	// 数据库表名
	protected static String			DB_TABLE;

	public DBadapter(Context context)
	{
		this.m_context = context;
	}

	/**
	 * @Title: ${open}
	 * @Description: ${打开数据库，返回数据库对象}
	 * @param $
	 *            {} 无参数
	 * @return ${} 无返回值
	 * @throws SQLException
	 */
	public boolean open() throws SQLException
	{
		try
		{
			m_sem.acquire();
		}
		catch (InterruptedException e)
		{
			m_sem.release();
			e.printStackTrace();
			return false;
		}
		dbhelper = new DBHelper(m_context);
		db = dbhelper.getWritableDatabase();
		if (db == null)
		{
			m_sem.release();
			return false;
		}
		return true;
	}

	/**
	 * @Title: ${close}
	 * @Description: ${关闭数据库}
	 * @param $
	 *            {} 无参数
	 * @return ${} 无返回值
	 * @throws
	 */
	public void close()
	{
		if (db != null)
		{
			db.close();
		}
		if (dbhelper != null)
		{
			dbhelper.close();
		}
		m_sem.release();
	}

	/**
	 * 单行游标转化成Map
	 * 
	 * @param cursor
	 * @return
	 */
	public static HashMap<String, String> cursorToMap(Cursor cursor)
	{
		HashMap<String, String> map = new HashMap<String, String>();
		if (cursor == null || !cursor.moveToFirst())
		{ 
			return new HashMap<String, String>();
		}
		else  
		{
			String[] names = cursor.getColumnNames();
			for (String name : names)
			{
				map.put(name, cursor.getString(cursor.getColumnIndex(name)));
			}
		}
		return map;
	}

	/**
	 * 多行游标转换成List<Map>
	 * 
	 * @param cursor
	 * @return
	 */
	public static List<Map<String, String>> cursorToList(Cursor cursor)
	{
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		if (cursor != null && cursor.moveToFirst())
		{
			do
			{
				HashMap<String, String> map = new HashMap<String, String>();
				String[] names = cursor.getColumnNames();
				for (String name : names)
				{ 
					map.put(name, cursor.getString(cursor.getColumnIndex(name)));
				}
				list.add(map);
			} while (cursor.moveToNext());
		}
		return list;
	}
}
