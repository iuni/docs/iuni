package com.ai.cmccha.sc.idcard.bean;

public class PsamInfo 
{
	private String 				psamID; 			// PSAM卡号 ASCII
	public byte[] 				psamIDArray;
	private String 				psamStartDate; 		// PSAM 启用日期
	private String 				psamEndDate; 		// PSAM有效期

	private byte 				comm_type; 			// 通信类型
	private byte[] 				m_fsk_1; 			// FSK拨号号码
	private byte[] 				m_fsk_2; 			// 备用FSK拨号号码

	private byte[] 				apn;
	private byte[] 				user;
	private byte[] 				password;

	private byte[] 				smsc;
	private byte[] 				smscBack;

	private String 				masterIP;
	private int 				masterSendPort;
	private int 				masterRecvPort;

	private String 				masterIPBak;
	private int 				masterSendPortBak;
	private int 				masterRecvPortBak;

	private String 				updateIP;
	private int 				updateSendPort;
	private int 				updateRecvPort;

	private String 				updateIPBak;
	private int 				updateSendPortBak;
	private int 				updateRecvPortBak;

	private byte[] 				bindingNo;

	private String 				msg;

	private String 				agent;			// 代理商姓名
	private String 				address;		// 安装地点
	private String 				telno;			// 联系电话

	public String getAgent() 
	{
		return agent;
	}

	public void setAgent(String agent) 
	{
		this.agent = agent;
	}

	public String getAddress() 
	{
		return address;
	}

	public void setAddress(String address) 
	{
		this.address = address;
	}

	public String getTelno() 
	{
		return telno;
	}

	public void setTelno(String telno) 
	{
		this.telno = telno;
	}

	public String getPsamID() 
	{
		return psamID;
	}

	public void setPsamID(String psamID) 
	{
		this.psamID = psamID;
	}

	public String getPsamStartDate() 
	{
		return psamStartDate;
	}

	public void setPsamStartDate(String psamStartDate) 
	{
		this.psamStartDate = psamStartDate;
	}

	public String getPsamEndDate() 
	{
		return psamEndDate;
	}

	public void setPsamEndDate(String psamEndDate) 
	{
		this.psamEndDate = psamEndDate;
	}

	public byte getComm_type() 
	{
		return comm_type;
	}

	public void setComm_type(byte comm_type) 
	{
		this.comm_type = comm_type;
	}

	public byte[] getM_fsk_1() 
	{
		return m_fsk_1;
	}

	public void setM_fsk_1(byte[] m_fsk_1) 
	{
		this.m_fsk_1 = m_fsk_1;
	}

	public byte[] getM_fsk_2() 
	{
		return m_fsk_2;
	}

	public void setM_fsk_2(byte[] m_fsk_2) 
	{
		this.m_fsk_2 = m_fsk_2;
	}

	public byte[] getApn() 
	{
		return apn;
	}

	public void setApn(byte[] apn) 
	{
		this.apn = apn;
	}

	public byte[] getUser() 
	{
		return user;
	}

	public void setUser(byte[] user) 
	{
		this.user = user;
	}

	public byte[] getPassword() 
	{
		return password;
	}

	public void setPassword(byte[] password) 
	{
		this.password = password;
	}

	public byte[] getSmsc() 
	{
		return smsc;
	}

	public void setSmsc(byte[] smsc) 
	{
		this.smsc = smsc;
	}

	public byte[] getSmscBack() 
	{
		return smscBack;
	}

	public void setSmscBack(byte[] smscBack) 
	{
		this.smscBack = smscBack;
	}

	public String getMasterIP() 
	{
		return masterIP;
	}

	public void setMasterIP(String masterIP) 
	{
		this.masterIP = masterIP;
	}

	public int getMasterSendPort() 
	{
		return masterSendPort;
	}

	public void setMasterSendPort(int masterSendPort) 
	{
		this.masterSendPort = masterSendPort;
	}

	public int getMasterRecvPort() 
	{
		return masterRecvPort;
	}

	public void setMasterRecvPort(int masterRecvPort) 
	{
		this.masterRecvPort = masterRecvPort;
	}

	public String getMasterIPBak() 
	{
		return masterIPBak;
	}

	public void setMasterIPBak(String masterIPBak) 
	{
		this.masterIPBak = masterIPBak;
	}

	public int getMasterSendPortBak() 
	{
		return masterSendPortBak;
	}

	public void setMasterSendPortBak(int masterSendPortBak) 
	{
		this.masterSendPortBak = masterSendPortBak;
	}

	public int getMasterRecvPortBak() 
	{
		return masterRecvPortBak;
	}

	public void setMasterRecvPortBak(int masterRecvPortBak) 
	{
		this.masterRecvPortBak = masterRecvPortBak;
	}

	public String getUpdateIP() 
	{
		return updateIP;
	}

	public void setUpdateIP(String updateIP) 
	{
		this.updateIP = updateIP;
	}

	public int getUpdateSendPort() 
	{
		return updateSendPort;
	}

	public void setUpdateSendPort(int updateSendPort)
	{
		this.updateSendPort = updateSendPort;
	}

	public int getUpdateRecvPort() 
	{
		return updateRecvPort;
	}

	public void setUpdateRecvPort(int updateRecvPort) 
	{
		this.updateRecvPort = updateRecvPort;
	}

	public String getUpdateIPBak() 
	{
		return updateIPBak;
	}

	public void setUpdateIPBak(String updateIPBak) 
	{
		this.updateIPBak = updateIPBak;
	}

	public int getUpdateSendPortBak() 
	{
		return updateSendPortBak;
	}

	public void setUpdateSendPortBak(int updateSendPortBak) 
	{
		this.updateSendPortBak = updateSendPortBak;
	}

	public int getUpdateRecvPortBak() 
	{
		return updateRecvPortBak;
	}

	public void setUpdateRecvPortBak(int updateRecvPortBak) 
	{
		this.updateRecvPortBak = updateRecvPortBak;
	}

	public byte[] getBindingNo() 
	{
		return bindingNo;
	}

	public void setBindingNo(byte[] bindingNo) 
	{
		this.bindingNo = bindingNo;
	}

	public String getMsg() 
	{
		return msg;
	}

	public void setMsg(String msg) 
	{
		this.msg = msg;
	}
}
