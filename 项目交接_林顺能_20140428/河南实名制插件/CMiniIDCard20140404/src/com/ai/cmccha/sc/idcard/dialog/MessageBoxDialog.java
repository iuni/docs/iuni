package com.ai.cmccha.sc.idcard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.R;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;

// 界面提示消息框
public class MessageBoxDialog extends Dialog implements View.OnClickListener
{
	private Button 					mPositiveButton 		= null; 		// 确定按钮
	private Button 					mNegativeButton 		= null; 		// 取消按钮
	private Button 					mNeutralButton 			= null; 		// neutralButton按钮
	private View.OnClickListener 	mPositiveListener 		= null; 		// 确定按钮监听
	private View.OnClickListener 	mNegativeListener 		= null; 		// 取消按钮监听
	private View.OnClickListener 	mNeutralListener 		= null; 		// neutral按钮监听

	private TextView 				mTitleView 				= null; 		// 标题文本框
	private TextView 				mMessageView 			= null; 		// 消息文本框
	private ImageView 				mIconView 				= null; 		// 图标控件

    public static int 				MARGIN_SIZE 			= 50;
	//private static List<MessageBoxDialog> mList = new ArrayList<MessageBoxDialog>();

	public MessageBoxDialog(Context context, boolean cancelable, OnCancelListener cancelListener) 
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public MessageBoxDialog(Context context, int theme) 
	{
		super(context, theme);
		initView();
	}

	public MessageBoxDialog(Context context) 
	{
		super(context);
		initView();
	}

	// 获取MessageBoxDialog的实例
	public static MessageBoxDialog getInstance(Context context) 
	{
		return new MessageBoxDialog(context, R.style.transparent_dialog);
	}

	// 设置标题
	@Override
	public void setTitle(CharSequence title) 
	{
		super.setTitle(title);
		mTitleView.setText(title);
	}

	public void setMessage(CharSequence message) 
	{
		mMessageView.setText(message);
	}
	
	public void setMessage(CharSequence message,int textsize)
	{
		mMessageView.setTextSize((float) textsize);
		mMessageView.setText(message);
	}

	// 初始化布局
	private void initView() 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.messagebox_dialog);

		mIconView 		= (ImageView) findViewById(R.id.iconImageView);
		mTitleView 		= (TextView) findViewById(R.id.titleTextView);
		mMessageView 	= (TextView) findViewById(R.id.contentTextView);

		mPositiveButton 	= (Button) findViewById(R.id.okButton);
		mNegativeButton 	= (Button) findViewById(R.id.cancelButton);
		mNeutralButton 		= (Button) findViewById(R.id.neutralButton);

		mPositiveButton.setOnClickListener(this);
		mNegativeButton.setOnClickListener(this);
		mNeutralButton.setOnClickListener(this);

		mPositiveButton.setVisibility(View.GONE);
		mNegativeButton.setVisibility(View.GONE);
		mNeutralButton.setVisibility(View.GONE);
		mMessageView.setMovementMethod(ScrollingMovementMethod.getInstance());

		// 把创建的MessageBoxDialog实例加入链表当中
		//mList.add(this);
	}

	// 设置确定按钮
	public void setPositiveListener(String buttonStr, View.OnClickListener positive) 
	{
		mPositiveButton.setText(buttonStr);
		mPositiveButton.setVisibility(View.VISIBLE);
		mPositiveListener = positive;
	}

	// 设置确定按钮文本
	public void setPositiveText(String buttonStr) 
	{
		mPositiveButton.setText(buttonStr);
	}

	// 设置取消按钮
	public void setNegativeListener(String buttonStr, View.OnClickListener negative) 
	{
		mNegativeButton.setText(buttonStr);
		mNegativeButton.setVisibility(View.VISIBLE);
		mNegativeListener = negative;
	}

	// 设置neutral按钮
	public void setNeutralListener(String buttonStr, View.OnClickListener neutral) 
	{
		mNeutralButton.setText(buttonStr);
		mNeutralButton.setVisibility(View.VISIBLE);
		mNeutralListener = neutral;
	}

	// 设置视图
	@SuppressWarnings("deprecation")
	public void setView(View view) 
	{
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(MARGIN_SIZE, 7, MARGIN_SIZE, 7);
		view.setLayoutParams(lp);
		LinearLayout layout = (LinearLayout) findViewById(R.id.contentLayout);
		layout.removeAllViewsInLayout();
		layout.addView(view);
	}

	public void setIcon(Bitmap bitmap) 
	{
		mIconView.setImageBitmap(bitmap);
	}

	public void setIcon(int resId) 
	{
		mIconView.setImageResource(resId);
	}

	public void setIcon(Drawable drawable) 
	{
		mIconView.setImageDrawable(drawable);
	}

	@Override
	public void onClick(View view) 
	{
		if (ButtonUtil.isFastDoubleClick(view.getId(), 500)) 
		{
			return;
		}

		if (view == mPositiveButton && mPositiveListener != null) 
		{
			mPositiveListener.onClick(view);
		} 
		else if (view == mNegativeButton && mNegativeListener != null) 
		{
			mNegativeListener.onClick(view);
		} 
		else if (view == mNeutralButton && mNeutralListener != null) 
		{
			mNeutralListener.onClick(view);
		}

		// 移除链表当中本MessageBoxDialog的实例
		try {
			dismiss();
			//mList.remove(mList.indexOf(this));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	// 移除所有的MessageBoxDialog
	public static void removeAllInstance() 
	{
		while (mList.size() != 0) 
		{
			MessageBoxDialog dialog = mList.get(0);
			dialog.dismiss();
			mList.remove(0);
		}
	}
	*/
}
