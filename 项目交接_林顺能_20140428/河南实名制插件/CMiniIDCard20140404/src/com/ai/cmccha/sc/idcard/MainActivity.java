package com.ai.cmccha.sc.idcard;

import java.lang.reflect.Constructor;

import me.sfce.android.framework.plugin.annotation.PluginFeatureA;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.ai.cmccha.sc.idcard.uilogic.UILogic;
import com.ai.cmccha.sc.idcard.util.Log;
import com.ai.cmccha.sc.idcard.util.SharedUtil;

// 主程序入口
@PluginFeatureA
public class MainActivity extends Activity implements View.OnClickListener
{
	private static final String	TAG			= "主Activity";
	private MainActivity		mThis		= this;
	private UILogic				mUILogic	= null;		// 界面子类

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Log.e(TAG, "跳转到主界面...");
		// 保存主程序传过来的服务器地址
		if(getIntent()!=null && getIntent().getExtras() != null)
		{
			String url1 = getIntent().getExtras().getString(App.URL1);
			String url2 = getIntent().getExtras().getString(App.URL2);
			String url3 = getIntent().getExtras().getString(App.URL3);
			String url4 = getIntent().getExtras().getString(App.URL4);
			String url5 = getIntent().getExtras().getString(App.URL5);
			String ms_tel = getIntent().getExtras().getString(App.ms_tel);
			String bossCode = getIntent().getExtras().getString(App.bossCode);
			String optrid = getIntent().getExtras().getString(App.optrid);
			String sc_tel = getIntent().getExtras().getString(App.sc_tel);
			Log.e(TAG, "url1:" + url1);
			Log.e(TAG, "url2:" + url2);
			Log.e(TAG, "url3:" + url3);
			Log.e(TAG, "url4:" + url4);
			Log.e(TAG, "url5:" + url5);
			Log.e(TAG, "ms_tel:" + ms_tel); 
			Log.e(TAG, "bossCode:" + bossCode); 
			Log.e(TAG, "optrid:" + optrid); 
			Log.e(TAG, "sc_tel:" + sc_tel); 
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.URL1, url1);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.URL2, url2);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.URL3, url3);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.URL4, url4);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.URL5, url5);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.ms_tel, ms_tel);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.optrid, optrid);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.bossCode, bossCode);
			SharedUtil.putString(App.ShARED_FILE_NAME, this, App.sc_tel, sc_tel);
		}
		goto_Layout(MainMenuLayout.class);
	}

	// 点击事件
	public void onClick(View v)
	{
		switch (v.getId())
		{
			default:
				break;
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		return;
	}

	// 跳转方法，id为跳转页面的id，一般从R.layout中取出来
	public void goto_Layout(int id)
	{
		// 调用原界面的销毁方法
		if (mUILogic != null)
		{
			mUILogic.onClose();
			mUILogic = null;
		}
		mUILogic = UILogic.getInstance(id, mThis);
		mUILogic.go();
	}

	// 跳转方法，参数为要跳转的类对象
	public void goto_Layout(UILogic incUILogic)
	{
		// 调用原界面的销毁方法
		if (mUILogic != null)
		{
			mUILogic.onClose();
			mUILogic = null;
		}
		mUILogic = incUILogic;
		mUILogic.go();
	}

	// 跳转方法，参数为要跳转的类对象
	public void goto_Layout(Class<?> clazz)
	{
		Log.e(TAG, "goto_Layout....");
		// 调用原界面的销毁方法
		if (mUILogic != null)
		{
			mUILogic.onClose();
			mUILogic = null;
		}
		try
		{
			Constructor<?> constructor = clazz.getConstructor(com.ai.cmccha.sc.idcard.MainActivity.class);
			mUILogic = (UILogic) constructor.newInstance(mThis);
			mUILogic.go();
			Log.e(TAG, "mUILogic.go....");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (mUILogic != null)
		{
			if (mUILogic.onKeyDown(keyCode, event))
			{
				return true;
			}
		}
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		switch (e.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				break;
			default:
				break;
		}
		return super.onTouchEvent(e);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (mUILogic != null)
		{
			if (mUILogic.onActivityResult(requestCode, resultCode, data))
			{
				return;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// 重载的toast
	protected void Toast(final String msgStr, final int nType)
	{
		long curTime = System.currentTimeMillis();
		long timeSpace = Math.abs(curTime - UILogic.mLastToastTime);
		if (timeSpace > 2000)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					Toast(nType, msgStr);
				}
			});
			UILogic.mLastToastTime = curTime;
		}
	}

	// 界面toast
	// nType: 0-短时间内的toast, 1-长时间的toast
	// msgStr: 想要的toast的内容
	protected void Toast(int nType, String msgStr)
	{
		View layout = getLayoutInflater().inflate(R.layout.toast_layout,
				(ViewGroup) findViewById(R.id.toast_layout_root));
		// ImageView image =
		// (ImageView)layout.findViewById(R.id.iv_toast_image);
		// image.setImageResource(R.drawable.note);
		TextView text = (TextView) layout.findViewById(R.id.tv_toast_text);
		text.setText(msgStr);
		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.setDuration(nType == 0 ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}
}
