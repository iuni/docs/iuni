package com.ai.cmccha.sc.idcard.util;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

public class SystemUtil
{
	// 获取版本号
	public static final String getVersionName(Context context, String packageName) 
	{
		try
		{
			// 获取PackageManager的实例
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo = packageManager.getPackageInfo(packageName, 0);
			return packInfo.versionName;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
        return "0.0.0";
	}
	
	// 安装APK
	public static void installApk(File file, Context context)
	{
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		// 执行的数据类型
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	// 卸载APK
	public static void unInstallApk(Context context,String packageName)
	{
		Intent intent = new Intent(Intent.ACTION_DELETE);  
		// 设置Uri  
		intent.setData(Uri.parse("package:" + packageName));  
		// 卸载程序  
		context.startActivity(intent);
	}
}