package com.ai.cmccha.sc.idcard.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.R;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;

// 界面提示消息框
public class MessageBoxDialogTextView extends Dialog implements View.OnClickListener
{
	private Button 					mButtonOK 			= null; 		// 确定按钮
	private Button 					mButtonCancel 		= null; 		// 取消按钮

	private View.OnClickListener 	mListenerOK 		= null; 		// 确定按钮监听
	private View.OnClickListener 	mListenerCancel 	= null; 		// 取消按钮监听
	

	private TextView 				mTitleView 				= null; 		// 标题文本框
	private TextView 				mMessageView 			= null; 		// 消息文本框
	private ImageView 				mIconView 				= null; 		// 图标控件
    static int 						MARGIN_SIZE = 50;
    
	//private static List<MessageBoxDialogWithEdit> mList = new ArrayList<MessageBoxDialogWithEdit>();

	// 构造
	public MessageBoxDialogTextView(Context context, boolean cancelable, OnCancelListener cancelListener) 
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public MessageBoxDialogTextView(Context context, int theme) 
	{
		super(context, theme);
		initView();
	}

	public MessageBoxDialogTextView(Context context) 
	{
		super(context);
		initView();
	}

	// 获取MessageBoxDialog的实例
	public static MessageBoxDialogTextView getInstance(Context context) 
	{
		return new MessageBoxDialogTextView(context, R.style.transparent_dialog);
	}

	// 设置标题
	@Override
	public void setTitle(CharSequence title) 
	{
		super.setTitle(title);
		mTitleView.setText(title);
	}

	public void setMessage(CharSequence message) 
	{
		mMessageView.setText(message);
	}

	// 初始化布局
	private void initView() 
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.messagebox_dialog_textview);

		mIconView 		= (ImageView) findViewById(R.id.iconImageView);
		mTitleView 		= (TextView) findViewById(R.id.titleTextView);
		mMessageView 	= (TextView) findViewById(R.id.contentTextView);

		mButtonOK 		= (Button) findViewById(R.id.okButton);
		mButtonCancel 	= (Button) findViewById(R.id.cancelButton);


		mButtonOK.setOnClickListener(this);
		mButtonCancel.setOnClickListener(this);


		mButtonOK.setVisibility(View.GONE);
		mButtonCancel.setVisibility(View.GONE);

		mMessageView.setMovementMethod(ScrollingMovementMethod.getInstance());

		// 把创建的MessageBoxDialog实例加入链表当中
		//mList.add(this);
	}

	// 设置确定按钮
	public void setButtonOKListener(String buttonStr, View.OnClickListener listener) 
	{
		mButtonOK.setText(buttonStr);
		mButtonOK.setVisibility(View.VISIBLE);
		mListenerOK = listener;
	}
	
	// 设置取消按钮
	public void setButtonCancelListener(String buttonStr, View.OnClickListener listener) 
	{
		mButtonCancel.setText(buttonStr);
		mButtonCancel.setVisibility(View.VISIBLE);
		mListenerCancel = listener;
	}
	
	// 设置确定按钮文本
	public void setButtonOKText(String buttonStr) 
	{
		mButtonOK.setText(buttonStr);
	}
	
	// 设置取消按钮文本
	public void setButtonCancelText(String buttonStr) 
	{
		mButtonOK.setText(buttonStr);
	}
	
	// 设置视图
	@SuppressWarnings("deprecation")
	public void setView(View view) 
	{
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(MARGIN_SIZE, 7, MARGIN_SIZE, 7);
		view.setLayoutParams(lp);
		LinearLayout layout = (LinearLayout) findViewById(R.id.contentLayout);
		layout.removeAllViewsInLayout();
		layout.addView(view);
	}

	public void setIcon(Bitmap bitmap) 
	{
		mIconView.setImageBitmap(bitmap);
	}

	public void setIcon(int resId) 
	{
		mIconView.setImageResource(resId);
	}

	public void setIcon(Drawable drawable) 
	{
		mIconView.setImageDrawable(drawable);
	}

	@Override
	public void onClick(View view) 
	{
		if (ButtonUtil.isFastDoubleClick(view.getId(), 500)) 
		{
			return;
		}

		if (view == mButtonOK && mListenerOK != null) 
		{
			mListenerOK.onClick(view);
		} 
		else if (view == mButtonCancel && mListenerCancel != null) 
		{
			mListenerCancel.onClick(view);
		} 
	}
	
}
