package com.ai.cmccha.sc.idcard.util;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * 尺寸相关工具类
 * @author wkh
 *
 */

public class DimensionUtil 
{
	/**
	 * 把Dip单位的值转化成Px
	 * @param resources
	 * @param dip
	 * @return
	 */
	public static int getPxFromDip(Resources resources,int dip) 
	{
		float fDip = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics()); 
		int iDip = Math.round(fDip);
		return iDip;
	}
	
	/**
	 * 让ListView的高度重新匹配
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView)
	{ 
        ListAdapter listAdapter = listView.getAdapter();  
        if (listAdapter == null) 
        { 
            // pre-condition 
            return; 
        } 
        try 
        {
        	int totalHeight = 0;
            int totalCount = listAdapter.getCount();
            for (int i = 0; i < totalCount; i++) 
            {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
		} 
        catch (Exception e) 
        {
			e.printStackTrace();
		} 
    } 
}
