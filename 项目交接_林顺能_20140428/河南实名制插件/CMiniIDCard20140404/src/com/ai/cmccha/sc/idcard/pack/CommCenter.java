package com.ai.cmccha.sc.idcard.pack;

import java.util.ArrayList;

public class CommCenter 
{
	static 
	{
		System.loadLibrary("syd_comm");
	}
	
	// 函数名称：	CalcCrc16 
	// 函数描述：	生成16位CRC码
	// 参数说明：	tp		： 要计算CRC的数据包
	// 				crc_pre	： 初始值，一般为0;
	public native static int CalcCrc16(byte[] tp, short nCrcPre, byte[] szCrc);

	// 函数名称：	Encrypt
	// 函数描述：	(数据头+数据体 )加密
	// 参数说明：	data	：数据包
	// 返回值: 		加密后的数据
	public native static int Encrypt(byte[] data);
	
	// 函数名称：	Decrypt
	// 函数描述：	(数据头+数据体 )解密
	// 参数说明： 	data	：数据包
	// 返回值: 		解密后的数据
	public native static byte[] Decrypt(byte[] data);
	
	// 函数名称：	XORCRC 
	// 函数描述：	PSAM卡号CRC异或加密，在用设备进行des加密之前调用 
	// 参数说明： 	psam	：指向psam卡号的数组，非字符串 
	//				crc		: 数据头+数据体 计算出的CRC 
	// 返回值: 		CRC异或后的PSAM卡号
	public native static byte[] XORCRC(byte[] psam, char crc);

	// 数据包转义
	public static byte[] transferredMeaning(byte[] srcData) 
	{
		int nLen = srcData.length;
		ArrayList<Byte> list = new ArrayList<Byte>(100);
		
		list.add((byte) 0x7E);
		for (int i = 0; i < nLen; i++) 
		{
			if (srcData[i] == 0x7E) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x7D);
			} 
			else if (srcData[i] == 0x5E) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x5D);
			} 
			else if (srcData[i] == 0x7F) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x5F);
			} 
			else 
			{
				list.add((byte) srcData[i]);
			}
		}
		list.add((byte)0x7F);
		
		int nDesLen = list.size();
		byte[] res = new byte[nDesLen];
		for (int i = 0; i < nDesLen; i++) 
		{
			res[i] = list.get(i);
		}
		return res;
	}
	
	// 数据包转义还原
	public static byte[] reverseTransferredMeaning(byte[] srcData) 
	{
		int nLen = srcData.length;
		ArrayList<Byte> list = new ArrayList<Byte>(100);
		
		list.add((byte)0x7E);
		for (int i = 1; i < nLen-1; i++) 
		{
			if(i == nLen -2) 
			{
				list.add((byte)srcData[i]);
				break;
			}
			if (srcData[i] ==(byte)0x5E && srcData[i+1] == (byte)0x7D) 
			{
				list.add((byte) 0x7E);
				i++;
			} 
			else if (srcData[i] == 0x5E &&  srcData[i+1] == (byte)0x5D) 
			{
				list.add((byte) 0x5E);
				i++;
			} 
			else if (srcData[i] == 0x5E &&  srcData[i+1] == (byte)0x5F) 
			{
				list.add((byte) 0x7F);
				i++;
			} 
			else 
			{
				list.add((byte) srcData[i]);
			}
		}
		list.add((byte) 0x7F);
		
		int nDesLen = list.size();
		byte[] res = new byte[nDesLen];
		for (int i = 0; i < nDesLen; i++) 
		{
			res[i] = list.get(i);
		}
		return res;
	}

}
