package com.ai.cmccha.sc.idcard.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedUtil 
{	
	// �����ַ�������
	public static void putString(String filename, Context context, String key, String val) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(key, val);
		editor.commit();
	}
	
	// ��ȡ�ַ�������
	public static String getString(String filename, Context context, String key, String defaultVal) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		return prefs.getString(key, defaultVal);
	}
	
	// ������������
	public static void putInt(String filename, Context context, String key, int val) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(key, val);
		editor.commit();
	}
	
	// ��ȡ��������
	public static int getInt(String filename, Context context, String key, int defaultVal) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		return prefs.getInt(key, defaultVal);
	}
	
	// ���ò�������
	public static void putBoolean(String filename, Context context, String key, boolean val) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putBoolean(key, val);
		editor.commit();
	}
	
	// ��ȡ��������
	public static boolean getBoolean(String filename, Context context, String key, boolean defaultVal) 
	{
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		return prefs.getBoolean(key, defaultVal);
	}
	
	// ɾ������
	public static void deleteKey(String filename, Context context, String key) 
	{
		if(key == null)
		{
			return;
		}
		SharedPreferences prefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.remove(key);
		editor.commit(); 
	}
}
