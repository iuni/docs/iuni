package com.ai.cmccha.sc.idcard.database;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class BluetoothInfoDBAdapter extends DBadapter 
{
	// 数据库表名
	private static final String	DB_TABLE		= "BLUETOOTH";
	
	// 表中一条数据的ID
	public static final String	KEY_ID			= "_id";
	
	// 蓝牙连接的标识
	public static final String	KEY_BT_TAG		= "bluetooth_tag";
	
	// 表中字段：蓝牙名称
	public static final String	KEY_BT_NAME		= "bluetooth_name";									

	// 表中字段：蓝牙地址
	public static final String	KEY_BT_MAC		= "bluetooth_mac";											
	
	public BluetoothInfoDBAdapter(Context context)
	{
		super(context);
	}
	
	// 插入蓝牙设备数据
	public long insertData(String strBluetoothTag, String strBluetoothName, String strBluetoothMac) 
	{
		ContentValues values = new ContentValues();
		values.put(KEY_BT_TAG, 	strBluetoothTag);
		values.put(KEY_BT_NAME, strBluetoothName);
		values.put(KEY_BT_MAC, 	strBluetoothMac);
		
		return db.insert(DB_TABLE, KEY_ID, values);
	}

	// 删除数据
	public boolean deleteData(long rowId)
	{
		return db.delete(DB_TABLE, KEY_ID + "=" + rowId, null) > 0;
	}
	
	// 根据蓝牙类型获取蓝牙设备
	// 返回蓝牙设备的mac地址
	public String fetchDataByTag(String strBluetoothTag) 
	{
		Cursor cursor = db.query(DB_TABLE, 
				new String[] { KEY_ID, KEY_BT_TAG, KEY_BT_NAME, KEY_BT_MAC }, 
				KEY_BT_TAG + "='" + strBluetoothTag + "'", 
				null, null, null, null);
		if (cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "总共搜索到" + nCount + "条蓝牙设备记录");
			while(cursor.moveToNext()) 
			{
				String mac = cursor.getString(cursor.getColumnIndex(KEY_BT_MAC));
				cursor.close();
				return mac;
			}
			cursor.close();
		}
		return null;
	}
	
	// 更新蓝牙数据
	public boolean updateData(String strBluetoothTag, String strBluetoothName, String strBluetoothMac) 
	{
		ContentValues args = new ContentValues();
		args.put(KEY_BT_NAME, 	strBluetoothName);
		args.put(KEY_BT_MAC, 	strBluetoothMac);
		return db.update(DB_TABLE, args, KEY_BT_TAG + "='" + strBluetoothTag + "'", null) > 0;
	}
	
	// 根据蓝牙设备类型获取蓝牙设备的mac地址
	public String getBluetoothMac(String strBluetoothTag) 
    {
		try
		{
			// 打开数据库
			open();
			// 获取蓝牙设备的MAC地址
			String macStr = fetchDataByTag(strBluetoothTag);
			// 关闭数据库
			close();
			return macStr;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
    }
	
	// 根据蓝牙设备类型更新蓝牙设备的MAC地址
	public void setBluetoothMac(String strBluetoothName, String strBluetoothMac, String strBluetoothTag)  
	{
		try
		{
			// 打开数据库
			open();
			// 查询数据库是否已经已经有对应类型的记录
	        if(fetchDataByTag(strBluetoothTag) == null) 
	        {
	        	// 没有, 插入记录
	        	insertData(strBluetoothTag, strBluetoothName, strBluetoothMac);
	        }
	        else
	        {
	        	// 有, 更新记录
	        	updateData(strBluetoothTag, strBluetoothName, strBluetoothMac);
	        }
	        close();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
}
