﻿package com.ai.cmccha.sc.idcard.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class HttpClientUtil
{
	public static final boolean	encrypted	= true;
	public static CookieStore	mCookieStore;

	public static String post(String host, String uri, Map<CharSequence, CharSequence> params) throws Exception
	{
		return post(null, host + uri, 20, params);
	}

	public static String post(String url, int timeout, Map<CharSequence, CharSequence> params) throws Exception
	{
		return post(null, url, timeout, params);
	}

	public static String post(Context context, String url, int timeout, Map<CharSequence, CharSequence> params)
			throws Exception
	{
		String result = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		httpClient.setKeepAliveStrategy(new ConnectionKeepAliveStrategy()
		{
			@Override
			public long getKeepAliveDuration(HttpResponse response, HttpContext context)
			{
				return 1800000;
			}
		});
		// if (context != null)
		// {
		// String cookie = SharedUtil.getString(App.ShARED_FILE_NAME, context,
		// App.COOKIE, "");
		// Log.e("HttpClientUtil", "读取cookie..." + cookie);
		// }
		// addCookie2Store();
		// for (SerializableCookie serializableCookie : App.cookieList)
		// {
		// mCookieStore.addCookie(serializableCookie.getCookie());
		// }
		httpClient.setCookieStore(mCookieStore);
		try
		{
			httpClient.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);
			httpClient.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			HttpPost httpPost = new HttpPost(url);
			httpPost.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);
			httpPost.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			if (!encrypted)
			{
				if (null != params)
				{
					for (Entry<CharSequence, CharSequence> param : params.entrySet())
					{
						CharSequence value = param.getValue();
						if (null == value)
						{
							value = "";
						}
						Pattern pattern = Pattern.compile("([\\u4E00-\\u9FA5])");
						Matcher matcher = pattern.matcher(value);
						if (matcher.find())
						{
							value = URLEncoder.encode(value.toString(), "utf-8");
						}
						nameValuePairs.add(new BasicNameValuePair(param.getKey().toString(), value.toString()));
					}
				}
			}
			else
			{
				Log.e("HttpClientUtil", "encrypted:" + encrypted);
				String paramSuffix = null;
				if (url.contains("?"))
				{
					paramSuffix = url.substring(url.indexOf("?") + 1);
				}
				Log.e("HttpClientUtil", "paramSuffix:" + paramSuffix);
				String param = makeParams(paramSuffix, params);
				Log.e("HttpClientUtil", "param:" + param);
				nameValuePairs.add(new BasicNameValuePair("ms", param));
			}
			Log.e("HttpClientUtil", "nameValuePairs.add...");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			Log.e("HttpClientUtil", "setEntity...");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			Log.e("HttpClientUtil", "execute...");
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			Log.e("HttpClientUtil", "statusCode:" + statusCode);
			if (statusCode == HttpStatus.SC_OK)
			{
				CookieStore cookieStore = httpClient.getCookieStore();
				mCookieStore = cookieStore;
				InputStream is = httpResponse.getEntity().getContent();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024 * 10];
				int len = 0;
				while ((len = is.read(buf)) > 0)
				{
					baos.write(buf, 0, len);
				}
				result = new String(baos.toByteArray(), "GBK");
				Log.e("HttpClientUtil", "result :" + result);
				if (encrypted)
				{
					try
					{
						if (result.startsWith("ms="))
						{
							result = result.substring(3);
						}
						result = new MsDesPlus().decrypt(result);
					}
					catch (Exception e)
					{
						e.printStackTrace();
						throw new Exception();
					}
				}
			}
			else
			{
				throw new Exception();
			}
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			Log.e("HttpClientUtil", e.getMessage());
			throw new RuntimeException(e);
		}
		finally
		{
			httpClient.getConnectionManager().shutdown();
		}
		return result;
	}

	// 构造参数
	public static String makeParams(String paramSuffix, Map<CharSequence, CharSequence> params) throws JSONException,
			Exception
	{
		JSONObject json = new JSONObject();
		if (null != paramSuffix)
		{
			for (String entry : paramSuffix.split("&"))
			{
				String kv[] = entry.split("=");
				json.put(kv[0], kv[1]);
			}
		}
		if (null != params)
		{
			for (Map.Entry<CharSequence, CharSequence> entry : params.entrySet())
			{
				String key = entry.getKey().toString();
				String value = entry.getValue().toString();
				json.put(key, value);
			}
		}
		String result;
		try
		{
			result = new MsDesPlus().encrypt(json.toString());
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
	}

	// 这里的list 估计你要从别的地方传进来。
	/*
	private static void addCookie2Store()
	{
		List<SerializableCookie> cookieList = (List<SerializableCookie>) getIntent().getSerializableExtra("cookie");
		for (SerializableCookie serializableCookie : cookieList)
		{
			mCookieStore.addCookie(serializableCookie.getCookie());
		}
	}
	*/
}
