package com.ai.cmccha.sc.idcard.pack;

import java.util.HashMap;

import com.ai.cmccha.sc.idcard.util.Commen;
import com.ai.cmccha.sc.idcard.util.StringUtil;

public class Pdu 
{
	public HashMap<String, byte[]> pdus = new HashMap<String, byte[]>();

	public Pdu(byte[] pdu) 
	{
		parse(pdu);
	}

	public Pdu() 
	{
		
	}
	
	public void put(String key, byte b) 
	{
		byte[] arr = new byte[] { b };
		this.put(key, arr);
	}
	
	public void put(String key, byte[] value) 
	{
		this.pdus.put(key, value);
	}

	public boolean containsKey(String key) 
	{
		key = key.toLowerCase();
		return this.pdus.containsKey(key);
	}
	
	public byte[] get(String key) 
	{
		key = key.toLowerCase();
		return this.pdus.get(key);
	}
	
	public boolean parse(byte[] pdu) 
	{
		int i 	= 0;
		int len = pdu.length;
		
		// pdu长度校验
		while (i < len) 
		{
			// 对怡丰厂家做特殊考虑, 如果长度为0肯定是DES加密时补充的0x00
			if(pdu[i] == 0x00) 
			{
				if(i == 0) 
				{
					return false;
				}
				else 
				{
					len = i;
					break;
				}
			}
			//
			if (i+4 > len) 
			{
				return false;
			}
			int lenPdu = (pdu[i]&0xFF) | ((pdu[i+1]&0xFF)<<8);
			if (i+lenPdu > len) 
			{
				return false;
			}
			i += lenPdu;
		}
		
		i = 0;
		while (i < len) 
		{
			int lenPdu = (pdu[i] & 0xFF) | ((pdu[i + 1] & 0xFF) << 8);
			String key = Commen.makeWord(pdu, i+2, 2);
			// PDU单元内容
			byte[] unit = new byte[lenPdu-4];
			System.arraycopy(pdu, i+4, unit, 0, lenPdu-4);
			// 由于收件箱业务更新时会有多个0146，需要对多个0146进行合并
			if("0146".equals(key) && unit[2] == (byte)0x04) 
			{
				byte 	total = 0x00;
				byte[] 	oldunit = null;
				int 	ilen = 0;
				int 	offset = 0;
				byte[] 	newunit = null;
				if(this.pdus.containsKey("0146") == false) 
				{
					newunit = new byte[2+unit.length];
				}
				else 
				{
					oldunit = this.pdus.get("0146");
					total = oldunit[5];
					ilen = oldunit.length;
					newunit = new byte[ilen+unit.length-4];
				}
				total++;
				// 数据域(2bytes) + 类型(1 byte) + CRC(2 bytes) 
				System.arraycopy(unit, 0, newunit, offset, 5);
				offset += 5;
				// 邮件总数
				newunit[offset] = total;
				offset += 1;
				if(total > 1) 
				{
					System.arraycopy(oldunit, offset, newunit, offset, ilen-6);
					offset += ilen-6;
				}
				int nMailLen = unit.length-5;
				newunit[offset++] = (byte)(nMailLen);
				System.arraycopy(unit, 5, newunit, offset, nMailLen);
				unit = newunit;
			}
			pdus.put(key, unit);
			i += lenPdu;
		}
		return true;
	}
	
	public String getMsg() 
	{
		if (!pdus.containsKey("0201")) 
		{
			return null;
		}
		byte[] unit = pdus.get("0201");
		int nErrCode = Commen.bytes2int(new byte[] {unit[0], unit[1], unit[2], unit[3]});
		if(nErrCode == 0) 
		{
			return null;
		}
		int len = unit[4]&0xFF;
		if(len == 0) 
		{
			return null;
		}
		return StringUtil.encodeWithGBK(unit, 5, len);
	}

	
	// 把传入的数据依次转化为如下格式：数据长度+子命令标识+命令内容
	@Override
	public String toString() 
	{
		StringBuffer sb = new StringBuffer();
		
		for (String key:pdus.keySet()) 
		{
			// PDU单元内容
			String strValue = Commen.sprinthax(pdus.get(key), "");
			// PDU单元长度
			int nPduLen = strValue.length()/2 + 2 + 2;
			String strLen = Integer.toHexString(nPduLen);
			// 保证PDU单元长度四字节对齐
			int i = 4 - strLen.length();
			while (i-- != 0) 
			{
				strLen = "0" + strLen;
			}
			// 数据长度+子命令标识+命令内容
			sb.append(tail2head(strLen)).append(tail2head(key)).append(strValue);
		}
		return sb.toString();
	}
	
	private String tail2head(String str) 
	{
		if (str.length() != 4) 
		{
			return str;
		}
		return str.substring(2, 4) + str.substring(0, 2);
	}
}
