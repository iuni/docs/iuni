package com.ai.cmccha.sc.idcard.util;

import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

public class DesUtil
{
	private static String DES 		= "DES/ECB/NoPadding";
	private static String TriDes 	= "DESede/ECB/NoPadding";
	
	// DES����
	public static byte[] desEncrypt(byte[] key, byte[] data) 
	{
		try
		{
			/*
			int nLen = data.length;
			if(nLen%8 != 0) 
			{
				nLen = ((nLen-1)/8+1)*8;
			}
			byte[] temp = new byte[nLen];
			Arrays.fill(temp, (byte)0x00);
			System.arraycopy(data, 0, temp, 0, data.length);
			*/
			
			KeySpec ks = new DESKeySpec(key);
			SecretKeyFactory kf = SecretKeyFactory.getInstance("DES");
			SecretKey ky = kf.generateSecret(ks);
			
			Cipher c = Cipher.getInstance(DES);
			c.init(Cipher.ENCRYPT_MODE, ky);
			return c.doFinal(data);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// DES����
	public static byte[] desDecrypt(byte[] key, byte[] data) 
	{
		try
		{
			KeySpec ks = new DESKeySpec(key);
			SecretKeyFactory kf = SecretKeyFactory.getInstance("DES");
			SecretKey ky = kf.generateSecret(ks);
			
			Cipher c = Cipher.getInstance(DES);
			c.init(Cipher.DECRYPT_MODE, ky);
			return c.doFinal(data);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] tridesEncrypt(byte[] key, byte[] data) 
	{
		try
		{
			if(data == null || data.length <= 0) 
			{
				return null;
			}
			
			int len = data.length;
			if (len % 8 != 0)
			{
				len = ((len-1)/8+1)*8;
			}
			byte[] needData = new byte[len];
			Arrays.fill(needData, (byte)0x00);
			System.arraycopy(data, 0, needData, 0, data.length);
			
			byte[] k = new byte[24];
			if (key.length == 16) 
			{
				System.arraycopy(key, 0, k, 0, key.length);
				System.arraycopy(key, 0, k, 16, 8);
			} 
			else
			{
				System.arraycopy(key, 0, k, 0, 24);
			}
			
			KeySpec ks = new DESedeKeySpec(k);
			SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede");
			SecretKey ky = kf.generateSecret(ks);
			
			Cipher c = Cipher.getInstance(TriDes);
			c.init(Cipher.ENCRYPT_MODE, ky);
			return c.doFinal(needData);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] tridesDecrypt(byte[] key, byte[] data) 
	{
		try
		{
			if(data == null || data.length <= 0) 
			{
				return null;
			}
			
			int len = data.length;
			if (len % 8 != 0)
			{
				len = ((len-1)/8+1)*8;
			}
			byte[] needData = new byte[len];
			Arrays.fill(needData, (byte)0x00);
			System.arraycopy(data, 0, needData, 0, data.length);
			
			byte[] k = new byte[24];
			if (key.length == 16)
			{
				System.arraycopy(key, 0, k, 0, key.length);
				System.arraycopy(key, 0, k, 16, 8);
			} 
			else
			{
				System.arraycopy(key, 0, k, 0, 24);
			}
			KeySpec ks = new DESedeKeySpec(k);
			SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede");
			SecretKey ky = kf.generateSecret(ks);
			
			Cipher c = Cipher.getInstance(TriDes);
			c.init(Cipher.DECRYPT_MODE, ky);
			return c.doFinal(needData);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}