package com.ai.cmccha.sc.idcard.util;

import android.util.Log;

public class Commen 
{
	// 16进制字符串转byte[]
	// "F56573"转化后为{0xF5, 0x65, 0x73}
	public static byte[] hexstr2byte(String s) 
	{
		try
		{
			if (s.length()%2 != 0) 
			{
				return null;
			}
			
			int len = s.length()/2;
			byte[] temp = new byte[len];
			for(int i = 0; i < len; i++)
			{
				temp[i] = (byte)Integer.parseInt(s.substring(2*i, 2*i+2), 16);
			}
			return temp;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static String hex2str(byte[] data) 
	{
		return hex2str(data, 0, data.length);
	}
	
	// byte[] 		转16进制串
	// data		: 	要转化的字节数组
	// len		: 	长度
	public static String hex2str(byte[] data, int offset, int len) 
	{
		if(null == data || data.length-offset < len) 
		{
			return null;
		}
		StringBuffer buff = new StringBuffer();
		for (int i = offset; i < offset+len; i++) 
		{
			String s = Integer.toHexString(data[i]&0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s);
		}
		return buff.toString();
	}
	
	// 日志输出	byte[] 16进制串
	// data	: 	要输出的数组
	// len	: 	数组长度
	// tag	:	日志tag
	// type	: 	当type='i' 时用log.i 其它用 log.e 输出
	public static void printhax(byte[] data, int len, String tag, char type) 
	{
		if (null == data)
		{
			return;
		}
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < len; i++) 
		{
			String s = Integer.toHexString(data[i]&0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s + " ");
		}
		if (type == 'i') 
		{
			Log.i(tag, buff.toString());
		} 
		else 
		{
			Log.e(tag, buff.toString());
		}
	}
	
	// byte[]数组转十六进制字符串
	public static String mishax(byte[] data, int len) 
	{
		if (null == data) 
		{
			return null;
		}
		
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < len; i++) 
		{
			String s = Integer.toHexString(data[i]&0xFF);
			if (s.length() == 1) 
			{
				s = '0' + s;
			}
			buff.append(s + " ");
		}
		return buff.toString();
	}
	
	// 对十六进制数据进行倒置
	public static byte[] swapHexArray(byte[] data) 
	{
		if(null == data) 
		{
			return null;
		}
		
		byte temp = data[1];
		data[1] = data[0];
		data[0] = temp;
		return data;
	}
	
	// byte[] 		转16进制串
	// data		: 	要转化的字节数组
	// split	: 	字节数组连接所用符号
	public static String sprinthax(byte[] data, String split) 
	{
		if (null == data)
		{
			return null;
		}
		int len = data.length;
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < len; i++) 
		{
			String s = Integer.toHexString(data[i] & 0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s);
			buff.append(split);
		}
		return buff.toString();
	}
	
	public static String makeWord(byte[] data, int start, int len) 
	{
		if(null == data || 2 != len || (data.length-start < len)) 
		{
			return null;
		}
		StringBuffer buff = new StringBuffer();
		byte[] word = new byte[] {data[start+1], data[start]};
		for (int i = 0; i < 2; i++) 
		{
			String s = Integer.toHexString(word[i]&0xFF);
			if (s.length() == 1)
			{
				s = '0' + s;
			}
			buff.append(s);
		}
		return buff.toString();
	}
	
	// 将整型拆分为4个字节
	public static byte[] int2bytes(int number) 
	{
	    byte[] bytes = new byte[4];  
	    for (int i = 0; i < 4; i++) 
	    {  
	    	bytes[0] = (byte)((number>>0)&0xFF);
	    	bytes[1] = (byte)((number>>8)&0xFF);
	    	bytes[2] = (byte)((number>>16)&0xFF);
	    	bytes[3] = (byte)((number>>24)&0xFF);
	    }  
	    return bytes;
	}  
	
	// 将短整型拆分为2个字节
	public static byte[] short2bytes(short number) 
	{
		byte[] bytes = new byte[2];
	    for (int i = 0; i < 2; i++) 
	    {
	    	bytes[0] = (byte)((number>>0)&0xFF);
	    	bytes[1] = (byte)((number>>8)&0xFF);
	    }
	    return bytes;
	}  
	
	// 将字节流转换成整型
	public static int bytes2int(byte[] bytes) 
	{
		return bytes2int(bytes, 0, bytes.length);
	}
	
	public static int bytes2int(byte[] bytes, int offset, int len) 
	{
		if(null == bytes || 0 == len) 
		{
			return 0;
		}
		
		int n = 0;
		for(int i=len-1; i>=offset; i--) 
		{
			n <<= 8;
			n |= bytes[i]&0xFF;
		}
		return n;
	}
	
	public static String bcd2str(byte[] bcd) 
	{
		StringBuffer temp = new StringBuffer(bcd.length*2);
		
		for (int i = 0; i < bcd.length; i++) 
		{
			temp.append(Integer.toHexString((bcd[i]&0x0F) >> 0));
			temp.append(Integer.toHexString((bcd[i]&0xF0) >> 4));
		}
		String tempstr = temp.toString();
		if(tempstr.substring(0, 1).equalsIgnoreCase("0")) 
		{
			return tempstr.substring(1);
		}
		return tempstr;
	}
	
	public static byte[] str2bcd(byte[] str) 
	{
		int 	len 	= str.length;
		int 	bcd_len = (len%2==0)? len/2:len/2+1;
		int 	low_ch 	= 0;
		byte[] 	temp 	= new byte[bcd_len];
		
		for(int i=0; i<bcd_len; i++) 
		{
			if(i*2+1 == len)
			{
				low_ch = 0;
			}
			else
			{
				low_ch = (str[i*2+1] - 0x30)<<4;
			}
			temp[i] = (byte) (low_ch | (str[i*2] - 0x30));
		 }	
		return temp;
	}
	
	public static byte[] misstr2bcd(byte[] str) 
	{
		int 	len 	= str.length;
		int 	bcd_len = (len%2==0)? len/2:len/2+1;	
		int 	low_ch 	= 0;
		byte[] 	temp 	= new byte[bcd_len];
		
		for(int i=0; i<bcd_len; i++) 
		{
			if(i*2+1 == len)
			{
				low_ch = 0x30;
			}
			else 
			{
				low_ch = (str[i*2+1] - 48)<<4;
			}
			temp[i] = (byte) (low_ch | (str[i*2] - 48));
			
		 }	
		return temp;
	}
	
	public static byte[] misDtstr2bcd(byte[] str) 
	{
		int len 	= str.length;
		int bcd_len = (len%2==0)? len/2:len/2+1;
		int low_ch 	= 0;
		byte[] temp = new byte[bcd_len];
		
		for(int i=0; i<bcd_len; i++) 
		{
			if(i*2+1 == len) 
			{
				low_ch = 0x90;
			}
			else 
			{
				low_ch = (str[i*2+1]-0x30)<<4;
			}
			temp[i] = (byte)(low_ch|(str[i*2]-0x30));
		 }	
		return temp;
	}
	
	public static int endofstr(byte[] str) 
	{
		if(null == str)
		{
			return 0;
		}
		int len = str.length;
		int index = 0;
		while(index < len) 
		{
			if(str[index] == 0)
			{
				return index;
			}
			index++;
		}
		return len;
	}
	
	// 将整型转换成ASCII码
	public static byte[] int2Ascii(int nValue) 
	{
		String tempstr = String.valueOf(nValue);
		int nLen = tempstr.length();
		byte[] dest = new byte[nLen];
		for(int i=0; i<nLen; i++) 
		{
			String temp = tempstr.substring(i, i+1);
			dest[i] = (byte)(Integer.parseInt(temp)+0x30);
		}
		return dest;
	}
	
	public static void main(String[] args) 
	{
		byte[] bytes = {(byte)0x01,(byte)0x78};
		System.out.println(Commen.hex2str(bytes));
	}
}
