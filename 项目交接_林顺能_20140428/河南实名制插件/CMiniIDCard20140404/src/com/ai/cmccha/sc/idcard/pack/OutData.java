package com.ai.cmccha.sc.idcard.pack;

public class OutData 
{
	private 	Pdu 		mPdu;
	private 	String 		mID;					// 主命令
	private 	String 		mSubID;					// 子命令
	private 	String 		mServiceCommandID;
	private 	int 		mCheckByte;
	private 	int 		mType;
	private 	String 		mStrErrorMsg;
	
	// 获取PDU单元
	public Pdu getPdu() 
	{
		return mPdu;
	}
	
	// 设置PDU单元
	public void setPdu(Pdu pdu) 
	{
		this.mPdu = pdu;
	}
	
	// 获取主命令
	public String getId() 
	{
		return mID;
	}
	
	// 设置子命令
	public void setId(String id) 
	{
		this.mID = id;
	}
	
	// 获取子命令
	public String getSubid() 
	{
		return mSubID;
	}
	
	// 设置子命令
	public void setSubid(String subid) 
	{
		this.mSubID = subid;
	}

	// 获取业务时序
	public String getServiceCommandID() 
	{
		return mServiceCommandID;
	}

	// 设置业务时序
	public void setServiceCommandID(String serviceCommandID) 
	{
		this.mServiceCommandID = serviceCommandID;
	}

	public int getCheckByte() 
	{
		return mCheckByte;
	}

	public void setCheckByte(int checkByte) 
	{
		this.mCheckByte = checkByte;
	}

	public int getType() 
	{
		return mType;
	}

	public void setType(int type) 
	{
		this.mType = type;
	}

	// 获取错误信息
	public String getErrorMsg() 
	{
		return mStrErrorMsg;
	}
	
	// 设置错误信息
	public void setErrorMsg(String errorMsg) 
	{
		this.mStrErrorMsg = errorMsg;
	}
}
