package com.ai.cmccha.sc.idcard.util;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import android.os.Environment;
import android.util.Log;

public class FileOpr
{
	// 判断SD卡是否准备好
	public static boolean isSdcardReady() 
	{
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			return true;
		}
		return false;
	}
	
	// 获取SD卡路径
	public static String getSdcardPath() 
	{
		return Environment.getExternalStorageDirectory().getPath();
	}
	
	// 获得控制台用户输入的信息 
	// // 可以返回用户输入的信息，不足之处在于不支持中文输入，有待进一步改进。
	public static String getInputMessage() throws IOException
	{
		System.out.println("请输入您的命令∶");
		byte buffer[] = new byte[1024];
		int count = System.in.read(buffer);
		char[] ch = new char[count - 2];// 最后两位为结束符，删去不要
		for (int i = 0; i < count - 2; i++)
		{
			ch[i] = (char) buffer[i];
		}
		String str = new String(ch);
		return str;
	}
	
	// 1.以文件流的方式复制文件
	// 该方法经过测试，支持中文处理，并且可以复制多种类型，比如txt，xml，jpg，doc等多种格式
	// src 文件源目录
	// dest 文件目的目录
	public static void copyFile(String src, String dest) throws IOException
	{
		FileInputStream in = new FileInputStream(src);
		File file = new File(dest);
		if (!file.exists())
		{
			file.createNewFile();
		}
		FileOutputStream out = new FileOutputStream(file);
		int c;
		byte buffer[] = new byte[1024];
		while ((c = in.read(buffer)) != -1)
		{
			for (int i = 0; i < c; i++)
			{
				out.write(buffer[i]);
			}
		}
		in.close();
		out.close();
	}
	
	// 三.写文件
	// 1.利用PrintStream写文件
	public static void PrintStreamDemo()
	{
		try
		{
			FileOutputStream out = new FileOutputStream("D:/test.txt");
			PrintStream p = new PrintStream(out);
			for (int i = 0; i < 10; i++)
			{
				p.println("This is " + i + " line");
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	// 2.利用StringBuffer写文件
	// 该方法可以设定使用何种编码，有效解决中文问题。
	public static void StringBufferDemo() throws IOException
	{
		File file = new File("/root/sms.log");
		if (!file.exists())
			file.createNewFile();
		FileOutputStream out = new FileOutputStream(file, true);
		for (int i = 0; i < 10000; i++)
		{
			StringBuffer sb = new StringBuffer();
			sb.append("这是第" + i + "行:前面介绍的各种方法都不关用,为什么总是奇怪的问题 ");
			out.write(sb.toString().getBytes("utf-8"));
		}
		out.close();
	}
	
	// 文件夹重命名
	public static boolean renameDir(String strOldDir, String strNewDir) 
	{
		Log.d("", "旧文件夹为: " + strOldDir + ", 新文件夹为：" + strNewDir);
		File newFile = new File(strNewDir);
		if(newFile.exists()) 
		{
			Log.e("", "重命名文件夹失败, 新文件夹已存在");
			return false;
		}
		File oldFile = new File(strOldDir);
		if(oldFile.exists() == false) 
		{
			Log.e("", "重命名文件夹失败, 旧文件夹不存在");
			return false;
		}
		if(oldFile.renameTo(newFile) == false) 
		{
			Log.e("", "重命名文件夹失败");
			return false;
		}
		Log.i("", "重命名文件夹成功");
		return true;
	}
	
	// 文件重命名
	// path: 文件目录
	// oldname: 原来的文件名
	// newname 新文件名
	public static void renameFile(String path, String oldname, String newname)
	{
		// 新的文件名和以前文件名不同时,才有必要进行重命名
		if (!oldname.equals(newname))
		{
			File oldfile = new File(path + "/" + oldname);
			File newfile = new File(path + "/" + newname);
			// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
			if (newfile.exists()) 
			{
				System.out.println(newname + "已经存在！");
			}
			else
			{
				oldfile.renameTo(newfile);
			}
		}
	}
	
	// 转移文件目录 转移文件目录不等同于复制文件，复制文件是复制后两个目录都存在该文件，而转移文件目录则是转移后，只有新目录中存在该文件。
	// filename: 文件名
	// oldpath: 旧目录
	// newpath: 新目录
	// cover: 若新目录下存在和转移文件具有相同文件名的文件时，是否覆盖新目录下文件，cover=true将会覆盖原文件，否则不操作
	public static void changeDirectory(String filename, String oldpath, String newpath, boolean cover)
	{
		if (!oldpath.equals(newpath)) 
		{
			File oldfile = new File(oldpath + "/" + filename);
			File newfile = new File(newpath + "/" + filename);
			// // 若在待转移目录下，已经存在待转移文件
			if (newfile.exists())
			{
				// 覆盖
				if (cover)
				{
					oldfile.renameTo(newfile);
				}
				else
				{
					System.out.println("在新目录下已经存在：" + filename);
				}
			}
			else
			{
				oldfile.renameTo(newfile);
			}
		}
	}

	// 六.读文件
	// 1.利用FileInputStream读取文件
	public static String FileInputStreamDemo(String path) throws IOException
	{
		File file = new File(path);
		if (!file.exists() || file.isDirectory())
			throw new FileNotFoundException();
		FileInputStream fis = new FileInputStream(file);
		byte[] buf = new byte[1024];
		StringBuffer sb = new StringBuffer();
		while ((fis.read(buf)) != -1)
		{
			sb.append(new String(buf));
			buf = new byte[1024];// 重新生成，避免和上次读取的数据重复
		}
		return sb.toString();
	}

	// 2.利用BufferedReader读取
	// 在IO操作，利用BufferedReader和BufferedWriter效率会更高一点
	public static String BufferedReadFile(String path) throws IOException
	{
		File file = new File(path);
		if (!file.exists() || file.isDirectory())
		{
			throw new FileNotFoundException();
		}
		// BufferedReader br=new BufferedReader(new FileReader(file));
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
		String temp = null;
		StringBuffer sb = new StringBuffer();
		temp = br.readLine();
		while (temp != null)
		{
			sb.append(temp + " ");
			temp = br.readLine();
		}
		return sb.toString();
	}
	
	// 读文件
	public static String BufferedReadFile(String path, String charset) throws IOException
	{
		File file = new File(path);
		if (!file.exists() || file.isDirectory())
		{
			throw new FileNotFoundException();
		}
		// BufferedReader br=new BufferedReader(new FileReader(file));
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
		String temp = null;
		StringBuffer sb = new StringBuffer();
		temp = br.readLine();
		while (temp != null)
		{
			sb.append(temp + " ");
			temp = br.readLine();
		}
		return sb.toString();
	}
	
	// 七.创建文件(文件夹)
	// 1.创建文件夹
	public static void createDir(String path) 
	{
		File dir = new File(path);
		if (!dir.exists())
		{
			dir.mkdirs();
		}
	}
	
	// 2.创建新文件
	public static void createFile(String path, String filename) throws IOException
	{
		File file = new File(path + "/" + filename);
		if (!file.exists())
		{
			file.createNewFile();
		}
	}
	
	// 3.判断文件夹是否存在
	public static boolean isDirExist(String strDirPath) 
	{
		File file = new File(strDirPath);
		if(file.exists() && file.isDirectory()) 
		{
			return true;
		}
		return false;
	}
	
	// 4.判断文件是否存在
	public static boolean isFileExist(String strFilePath) 
	{
		File file = new File(strFilePath);
		if(file.exists()) 
		{
			return true;
		}
		return false;
	}

	// 八.删除文件(目录)
	// 1.删除文件
	public static void delFile(String path, String filename)
	{
		File file = new File(path + "/" + filename);
		if (file.exists() && file.isFile())
		{
			file.delete();
		}
	}

	// 2.删除目录
	// 要利用File类的delete()方法删除目录时，必须保证该目录下没有文件或者子目录，否则删除失败，
	// 因此在实际应用中，我们要删除目录，必须利用递归删除该目录下的所有子目录和文件，然后再删除该目录。
	public static void delDir(String path) 
	{
		File dir = new File(path);
		if (dir.exists())
		{
			File[] tmp = dir.listFiles();
			for (int i = 0; i < tmp.length; i++)
			{
				if (tmp[i].isDirectory())
				{
					delDir(path + "/" + tmp[i].getName());
				}
				else
				{
					tmp[i].delete();
				}
			}
			dir.delete();
		}
	}
	
	// 获取指定文件夹下面的所有文件
	public static List<String> getAllFileName(File file)
	{
		// 不存在或非目录
		if (!file.isDirectory())
		{
			return null;
		}
		List<String> list = new ArrayList<String>();
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++)
		{
			File temp = files[i];
			if (temp.isFile())
			{
				list.add(temp.getAbsolutePath());
			}
			else if (temp.isDirectory())
			{
				List<String> tempList = getAllFileName(temp);
				list.addAll(tempList);
			}
		}
		return list;
	}

	public synchronized static boolean downFile(String url, String localPath) throws IOException
	{
		// String FileName = null;
		// FileName = url.substring(localPath.lastIndexOf("/") + 1);
		URL Url = new URL(url);
		URLConnection conn = Url.openConnection();
		conn.connect();
		InputStream is = conn.getInputStream();
		int fileSize = conn.getContentLength();// 根据响应获取文件大小
		// 获取内容长度为0
		if (fileSize <= 0)
		{ 
			return false;
		}
		// 没有下载流
		if (is == null)
		{
			return false;
		}
		File file = new File(localPath);
		if (!file.getParentFile().exists())
		{
			file.getParentFile().mkdirs();
		}
		if (!file.exists())
		{
			file.createNewFile();
		}
		System.out.println(file.toString());
		FileOutputStream FOS = new FileOutputStream(file); // 创建写入文件内存流，通过此流向目标写文件
		System.out.println("创建");
		byte buf[] = new byte[1024];
		int numread;
		while ((numread = is.read(buf)) != -1)
		{
			FOS.write(buf, 0, numread);
		}
		try
		{
			is.close();
			FOS.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}
}
