package com.ai.cmccha.sc.idcard.util;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil
{
	// 判断字符串为空
	public static boolean isEmptyOrNull(String str)
	{
		return str == null || str.trim().length() <= 0 
				|| str.trim().equals("null") || str.trim().equals("NULL");
	}
	
	public static boolean isEmptyOrZero(String str)
	{
		return isEmptyOrNull(str) || "0".equals(str);
	}
	
	// 判断字符是否不为空
	public static boolean isNotEmpty(String str) 
	{
		return !isEmptyOrNull(str);
	}
	
	// 判断是否纯数字
	public static boolean isIntNumber(String str) 
	{
		if(isEmptyOrNull(str)) 
		{
			return false;
		}
		return str.matches("[0-9]+");
	}
	
	// 判断字符串str是不是纯数字的字符串
	public static boolean isFloatNum(String str) 
	{
		if(isEmptyOrNull(str)) 
		{
			return false;
		}
		return str.matches("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$");
	}
	
	// 判断字符串str是否符合pattern类型的正则表达式
	public static boolean isPattern(String str,String pattern)
	{
		// /^[-|+]?\\d*([.]\\d{0,2})?$
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.find();
	}
	
	// 获取字符串的子串
	public static String getSubString(String str) 
	{
		int nIndex = str.indexOf(0);
		if(nIndex != -1)
		{
			return str.substring(0, nIndex);
		}
		return str;
	}
	
	// 格式化整型
	// num 整型
	// pattern 模式如：000    00.00
	public static String fomatInteger(int number, String pattern) 
	{
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(number);
	}
	
	// 采用GBK对字符进行编码
	public static String encodeWithGBK(byte[] buf) 
	{
		return encode(buf, 0, buf.length, "GBK");
	}
	
	// 采用GBK对字符进行编码
	public static String encodeWithGBK(byte[] buf, int from, int len)
	{
		return encode(buf, from, len, "GBK");
	}
	
	// 对字符编码
	public static String encode(byte[] buf, int offset, int len, String charset) 
	{
		try
		{
			return new String(buf, offset, len, charset);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// 调整字符串长度
	public static String adjustStringLength(String str, int length)
	{
		if(str == null)
		{
			return null;
		}
		if(str.length() > length)
		{
			String result = str.substring(0, length) + "...";
			return result;
		}
		else
		{
			return str;
		}
	}
	
	public static String convertAscIIToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte[] data = filterAndCut(abyte0);
		if (data != null) 
		{
			try 
			{
				s = new String(data, "ISO-8859-1");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertAscIIToUnicodeJP(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "Shift_JIS");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertAscIIToUnicodeRS(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "Windows-1251");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertBig5ToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "big5");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return s.trim();
	}

	public static String convertGbkToUnicode(byte abyte0[]) 
	{
		String s = "";
		byte abyte1[] = filterAndCut(abyte0);
		if (abyte1 != null) 
		{
			try 
			{
				s = new String(abyte1, "GBK");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}// goto _L2; else goto _L1
		return s.trim();
	}

	public static byte[] convertToUnicode(String s) 
	{
		try 
		{
			byte[] abyte1 = s.getBytes("utf-8");
			final int i = abyte1.length;
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] convertUnicodeToAscii(String s) 
	{
		try 
		{
			final int i = s.length();
			byte[] abyte1 = s.getBytes("US-ASCII");
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] convertUnicodeToGbk(String s) 
	{
		try 
		{
			final int i = s.length();
			byte[] abyte1 = s.getBytes("GBK");
			byte[] abyte0 = new byte[i + 1];
			for (int j = 0; j < i; j++) 
			{
				abyte0[j] = abyte1[j];
			}
			abyte0[i] = 0;
			return abyte0;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] filter(byte abyte0[]) 
	{
		final int i = abyte0.length;
		byte abyte1[] = new byte[i];
		int k = 0;
		for (int j = 0; j < i; j++) 
		{
			int l;
			if (abyte0[j] == 13) 
			{
				l = k;
			} 
			else 
			{
				l = k + 1;
				abyte1[k] = abyte0[j];
			}
			k = l;
		}
		return abyte1;
	}

	public static byte[] filterAndCut(byte abyte0[]) 
	{
		int i = strlen(abyte0);
		byte abyte1[];
		if (i < 1) 
		{
			abyte1 = null;
		} 
		else 
		{
			abyte1 = new byte[i];
			int j = 0;
			int k = 0;
			while (j < i) 
			{
				int l;
				if (abyte0[j] == 13) 
				{
					l = k;
				} 
				else 
				{
					l = k + 1;
					abyte1[k] = abyte0[j];
				}
				j++;
				k = l;
			}
		}
		return abyte1;
	}

	public static boolean isEngString(String s) 
	{
		String s1 = s.replaceAll(" ", "");
		for (int i = 0; i < s1.length(); i++) 
		{
			if ((s1.charAt(i) < 'A' || s1.charAt(i) > 'Z') && (s1.charAt(i) < 'a' || s1.charAt(i) > 'z')) 
			{
				
			} 
			else 
			{
				return true;
			}
		}
		return false;
	}

	public static int strlen(byte abyte0[]) 
	{
		int i = -1;
		if (abyte0 != null) 
		{
			if (abyte0.length != 0) 
			{
				for (int j = 0; j < abyte0.length; j++) 
				{
					if (abyte0[j] == 0) 
					{
						i = j;
						break;
					}
				}
			} 
			else 
			{
				i = 0;
			}
		}
		return i;
	}
}