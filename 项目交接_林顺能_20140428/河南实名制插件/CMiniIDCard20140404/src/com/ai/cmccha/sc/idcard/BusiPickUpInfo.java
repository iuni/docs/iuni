package com.ai.cmccha.sc.idcard;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.bean.ProductInfo;
import com.ai.cmccha.sc.idcard.database.ProductInfoDBAdapter;
import com.ai.cmccha.sc.idcard.dialog.SelectProductDialog;
import com.ai.cmccha.sc.idcard.listener.SimpleTextWatcher;
import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.util.Base64;
import com.ai.cmccha.sc.idcard.util.BitmapUtil;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;
import com.ai.cmccha.sc.idcard.util.CollectionUtil;
import com.ai.cmccha.sc.idcard.util.FileUtil;
import com.ai.cmccha.sc.idcard.util.HttpClientUtil;
import com.ai.cmccha.sc.idcard.util.IDCardUtils;
import com.ai.cmccha.sc.idcard.util.Log;
import com.ai.cmccha.sc.idcard.util.NetworkUtil;
import com.ai.cmccha.sc.idcard.util.SharedUtil;
import com.ai.cmccha.sc.idcard.util.StringUtil;
import com.ai.cmccha.sc.idcard.util.TimeUtil;
import com.ym.idcard.reg.CameraActivity;
import com.ym.idcard.reg.IdentityCard;
import com.ym.idcard.reg.OcrEngine;
import com.ym.idcard.reg.Setting;

// 信息采集
public class BusiPickUpInfo extends Busi_Frame
{
	private final String					TITLE_STR		= "信息采集提示";
	private final String					TAG				= "信息采集";
	private final int						MSG_TAKE_IDCARD	= 0xF001;
	private final int						MSG_REG_SUC		= 0xF002;
	private final int						MSG_REG_FAIL	= 0xF003;
	private ImageView						mIvIdcard1		= null;
	private ImageView						mIvIdcard2		= null;
	private EditText						mEtNo;
	private EditText						mEtName;
	private EditText						mEtSex;
	private EditText						mEtAddr;
	private EditText						mEtValidDate;
	private EditText						mEtOpenTel;
	private EditText						mEtContactTel;
	private TextView						mTvOpenTel;
	// 校验补录电话
	private Button							mBtnCheckTel;
	// 联系人姓名
	private EditText						mEtContactName;
	// 通信地址
	private EditText						mEtContactAddr;
	private ImageView						mIvNo;
	private ImageView						mIvName;
	private ImageView						mIvSex;
	private ImageView						mIvAddr;
	private ImageView						mIvValidDate;
	private ImageView						mIvOpenTel;
	private ImageView						mIvContactTel;
	private ImageView						mIvContactName;
	private ImageView						mIvContactAddr;
	private int								mIdcardType		= 0x01;
	private IdentityCard					mCard			= new IdentityCard();
	private String							mStrImageDir	= null;
	private Bitmap							mBitmap1		= null;
	private Bitmap							mBitmap2		= null;
	// 上传照片宽
	private final int						PIC_WIDTH		= 110;
	// 上传照片高
	private final int						PIC_HEIGHT		= 110;
	// sim号码
	private EditText						mEtSim;
	// 选择套餐
	private EditText						mEtSelectProduct;
	// 返回套餐信息
	private Map<String, List<ProductInfo>>	mProData;
	// 当前选中套餐
	private ProductInfo						mCurPro;
	// sim卡校验布局
	private View							mLaySim;
	// 套餐选择布局
	private View							mLaySelectPro;
	private String							ms_tel;
	private String							bossCode;
	private String							optrid;
	private String							scTel;
	private ProductInfoDBAdapter	   		mProInfoDB; 
	// 是否校验过开户电话
	private boolean							mCheckedTel;
	// 是否校验过sim
	private boolean							mCheckedSim;

	public BusiPickUpInfo(MainActivity context)
	{
		super(context);
	}

	@Override
	public void go()
	{
		super.go();
		Log.e(TAG, "go...");
		mMainViewStub.setLayoutResource(R.layout.lay_pickup_info);
		mMainViewStub.inflate();
		mTvBusiFrameTitle.setText("信息采集");
		// 初始化控件
		initViews();
		mStrImageDir = App.getImagesDir() + System.currentTimeMillis() + "/";
		mProInfoDB = new ProductInfoDBAdapter(mMainActivity);
		ms_tel = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.ms_tel, "");
		bossCode = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.bossCode, "");
		optrid = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.optrid, "");
		scTel = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.sc_tel, "");
		Log.e(TAG, "读取msTel:" + ms_tel);
		Log.e(TAG, "读取bossCode:" + bossCode);
		Log.e(TAG, "读取optrid:" + optrid);
		Log.e(TAG, "读取scTel:" + scTel);
	}

	// 初始化控件
	public void initViews()
	{
		mLaySim = mMainActivity.findViewById(R.id.lay_sim_check);
		mLaySelectPro = mMainActivity.findViewById(R.id.lay_select_pro);
		mTvOpenTel = (TextView) mMainActivity.findViewById(R.id.tv_open_tel);
		// 校验补录电话
		mBtnCheckTel = (Button) mMainActivity.findViewById(R.id.btn_check_tel);
		mBtnCheckTel.setOnClickListener(this);
		// sim号码输入框
		mEtSim = (EditText) mMainActivity.findViewById(R.id.et_sim);
		// sim号码校验按钮
		mMainActivity.findViewById(R.id.btn_check_sim).setOnClickListener(this);
		// 选择套餐输入框
		mEtSelectProduct = (EditText) mMainActivity.findViewById(R.id.et_select_package);
		// 选择套餐按钮
		mMainActivity.findViewById(R.id.btn_select_package).setOnClickListener(this);
		// 证件号码
		mIvNo = (ImageView) mMainActivity.findViewById(R.id.iv_no);
		mEtNo = (EditText) mMainActivity.findViewById(R.id.et_no);
		mEtNo.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvNo.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvNo.setVisibility(View.VISIBLE);
					if (!IDCardUtils.isNoValid("01", s.toString()))
					{
						mIvNo.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvNo.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		// 证件姓名
		mIvName = (ImageView) mMainActivity.findViewById(R.id.iv_name);
		mEtName = (EditText) mMainActivity.findViewById(R.id.et_name);
		mEtName.addTextChangedListener(new SimpleTextWatcher()
		{ 
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvName.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvName.setVisibility(View.VISIBLE);
					if (!IDCardUtils.isNameValid("01", s.toString()))
					{
						mIvName.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvName.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		// 证件性别
		mIvSex = (ImageView) mMainActivity.findViewById(R.id.iv_sex);
		mEtSex = (EditText) mMainActivity.findViewById(R.id.et_sex);
		mEtSex.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvSex.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvSex.setVisibility(View.VISIBLE);
					if ("男".equals(s.toString()) || "女".equals(s.toString()))
					{
						mIvSex.setImageResource(R.drawable.ic_correct);
					}
					else
					{
						mIvSex.setImageResource(R.drawable.ic_error);
					}
				}
			}
		});
		// 证件地址
		mIvAddr = (ImageView) mMainActivity.findViewById(R.id.iv_addr);
		mEtAddr = (EditText) mMainActivity.findViewById(R.id.et_addr);
		mEtAddr.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvAddr.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvAddr.setVisibility(View.VISIBLE);
					if (IDCardUtils.isAddressValid("01", s.toString()) != 0)
					{
						mIvAddr.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mCard.setAddress(s.toString());
						mIvAddr.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		// 证件有效日期
		mIvValidDate = (ImageView) mMainActivity.findViewById(R.id.iv_valid_date);
		mEtValidDate = (EditText) mMainActivity.findViewById(R.id.et_valid_date);
		mEtValidDate.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvValidDate.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvValidDate.setVisibility(View.VISIBLE);
					mIvValidDate.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		// 开户电话
		mIvOpenTel = (ImageView) mMainActivity.findViewById(R.id.iv_open_tel);
		mEtOpenTel = (EditText) mMainActivity.findViewById(R.id.et_open_tel);
		mEtOpenTel.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvOpenTel.setVisibility(View.INVISIBLE);
				}
				else
				{
					mCheckedTel = false;
					mIvOpenTel.setVisibility(View.VISIBLE);
					if (s.length() != 11)
					{
						mIvOpenTel.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvOpenTel.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		// 联系电话
		mIvContactTel = (ImageView) mMainActivity.findViewById(R.id.iv_contact_tel);
		mEtContactTel = (EditText) mMainActivity.findViewById(R.id.et_contact_tel);
		mEtContactTel.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvContactTel.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvContactTel.setVisibility(View.VISIBLE);
					if (s.length() != 11)
					{
						mIvContactTel.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvContactTel.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		// 联系人姓名
		mIvContactName = (ImageView) mMainActivity.findViewById(R.id.iv_contact_name);
		mEtContactName = (EditText) mMainActivity.findViewById(R.id.et_contact_name);
		mEtContactName.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvContactName.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvContactName.setVisibility(View.VISIBLE);
					mIvContactName.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		// 通信地址
		mIvContactAddr = (ImageView) mMainActivity.findViewById(R.id.iv_contact_addr);
		mEtContactAddr = (EditText) mMainActivity.findViewById(R.id.et_contact_addr);
		mEtContactAddr.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
					mIvContactAddr.setVisibility(View.INVISIBLE);
				}
				else
				{
					mIvContactAddr.setVisibility(View.VISIBLE);
					mIvContactAddr.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		mEtSim.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3)
			{
				if (StringUtil.isEmptyOrNull(s.toString()))
				{
				}
				else
				{
					mCheckedSim = false;
				}
			}
		});
		// 身份证正面照
		mIvIdcard1 = (ImageView) mMainActivity.findViewById(R.id.iv_idcard_1);
		mIvIdcard1.setOnClickListener(this);
		// 身份证反面照
		mIvIdcard2 = (ImageView) mMainActivity.findViewById(R.id.iv_idcard_2);
		mIvIdcard2.setOnClickListener(this);
		// 上一步
		mMainActivity.findViewById(R.id.btn_prev).setOnClickListener(this);
		// 提交
		mMainActivity.findViewById(R.id.btn_submit).setOnClickListener(this);
		if (UIHandler.IS_BACK_TRAIL)
		{
			// 补录界面不需要校验sim卡和选择套餐
			mLaySim.setVisibility(View.GONE);
			mLaySelectPro.setVisibility(View.GONE);
			mTvOpenTel.setText("补录电话:");
			mBtnCheckTel.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onActivityResult(int requestCode, int resultCode, final Intent intent)
	{
		if (requestCode == MSG_TAKE_IDCARD)
		{
			if (resultCode == Activity.RESULT_OK)
			{
				new Thread()
				{
					public void run()
					{
						try
						{
							UIHandler.initBusiStart("身份证识别");
							UIHandler.initBusiStep("正在进行识别...", 50);
							//
							byte[] data = FileUtil.getBytesFromFile(intent.getStringExtra("IMAGE_NAME"));
							OcrEngine mOcrEngine = new OcrEngine();
							mOcrEngine.recognize(data, Setting.getOcrLanguage(), Setting.getKeyLanguage(),
									Setting.isBlurDetectionOn(), mCard, null, "no_need");
							UIHandler.sendMessage(MSG_REG_SUC);
						}
						catch (Exception e)
						{
							UIHandler.sendMessage(MSG_REG_FAIL);
							e.printStackTrace();
							Log.e("", e.toString());
						}
						finally
						{
							UIHandler.initBusiStop();
						}
					}
				}.start();
			}
			else if (resultCode == Activity.RESULT_CANCELED)
			{
			}
			return true;
		}
		return super.onActivityResult(requestCode, resultCode, intent);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			recycleBitmap();
			mMainActivity.goto_Layout(R.layout.lay_mainmenu);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v)
	{
		if(ButtonUtil.isFastDoubleClick(v.getId()))
		{
			return;
		}
		switch (v.getId())
		{
			// 返回
			case R.id.btn_busi_frame_back:
				recycleBitmap();
				mMainActivity.goto_Layout(R.layout.lay_mainmenu);
				break;
			// 身份证正面照
			case R.id.iv_idcard_1:
				mIdcardType = 0x01;
				procTakeIdcard(0x01);
				break;
			// 身份证反面照
			case R.id.iv_idcard_2:
				mIdcardType = 0x02;
				procTakeIdcard(0x02);
				break;
			// 上一步
			case R.id.btn_prev:
				mMainActivity.goto_Layout(R.layout.lay_mainmenu);
				break;
			// 信息提交
			case R.id.btn_submit:
				procSubmit();
				break;
			// 选择套餐
			case R.id.btn_select_package:
				getProList();
				break;
			// 校验sim
			case R.id.btn_check_sim:
				procCheck();
				break;
			// 校验补录号码
			case R.id.btn_check_tel:
				checkTelNum();
				break;
			default:
				super.onClick(v);
				break;
		}
	}
	
	private Handler	mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				// 通过Handler将选中的套餐信息传过来
				case UIHandler.MSG_PRO_SELECTED:
					mCurPro = (ProductInfo) msg.obj;
					mEtSelectProduct.setText(mCurPro.getName());
					break;
				default:
					break;
			}
		}
	};

	// 校验补录号码
	private void checkTelNum()
	{
		final String telNum = mEtOpenTel.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(telNum))
		{
			UIHandler.MsgBoxOne("请输入补录号码！");
			return;
		}
		if (telNum.length() != 11)
		{
			UIHandler.MsgBoxOne("补录号码输入有误！");
			return;
		}
		UIHandler.initProgressStart("补录号码校验", "正在校验...");
		new Thread()
		{
			@Override
			public void run()
			{
				if (!NetworkUtil.isConnect(mMainActivity))
				{
					UIHandler.MsgBoxOne("请检查当前的网络连接状态！");
					return;
				}
				String url = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.URL5, "");
				Log.e(TAG, "读取url5:" + url);
				Map<CharSequence, CharSequence> params = new HashMap<CharSequence, CharSequence>();
				params.put("msTel", ms_tel);
				params.put("bossCode", bossCode);
				params.put("optrid", optrid);
				params.put("scTel", scTel);
				params.put("SvcNum", telNum);
				String returnMessage = "请求超时！";
				try
				{
					String response = HttpClientUtil.post(mMainActivity, url, 5000, params);
					Log.e(TAG, "补录号码校验返回信息:" + response);
					JSONObject res = new JSONObject(response);
					String returnCode = res.getString("returnCode");
					returnMessage = res.getString("returnMessage");
					if (returnCode.equals("0000"))
					{
						// 校验成功
						Log.e(TAG, "校验成功...");
						UIHandler.InitProgressStop();
						Toast("校验成功！", 0);
						mCheckedTel = true;
						// UIHandler.MsgBoxOne("校验成功！");
					}
					else
					{
						// 校验失败
						Log.e(TAG, "校验失败...");
						UIHandler.MsgBoxOne("校验失败，失败原因：\n" + returnMessage);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Log.e(TAG, "校验失败,异常..." + e.toString());
					UIHandler.MsgBoxOne("校验失败，失败原因：\n未知错误！");
				}
			}
		}.start();
	}

	// 信息校验
	private void procCheck()
	{
		final String sim = mEtSim.getText().toString().trim();
		final String svcNum = mEtOpenTel.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(sim))
		{
			UIHandler.MsgBoxOne("请输入sim号码！");
			return;
		}
		if (StringUtil.isEmptyOrNull(svcNum))
		{
			UIHandler.MsgBoxOne("请输入开户电话！");
			return;
		}
		UIHandler.initProgressStart("sim校验", "正在校验...");
		new Thread()
		{
			@Override
			public void run()
			{
				if (!NetworkUtil.isConnect(mMainActivity))
				{
					UIHandler.MsgBoxOne("请检查当前的网络连接状态！");
					return;
				}
				Map<CharSequence, CharSequence> params = new HashMap<CharSequence, CharSequence>();
				params.put("msTel", ms_tel);
				params.put("bossCode", bossCode);
				params.put("optrid", optrid);
				params.put("scTel", scTel);
				params.put("IccId", sim);
				params.put("SvcNum", svcNum);
				String url = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.URL3, "");
				Log.e(TAG, "读取url3:" + url);
				String returnMessage = "请求超时！";
				try
				{
					String response = HttpClientUtil.post(mMainActivity, url, 5000, params);
					Log.e(TAG, "sim校验返回信息:" + response);
					JSONObject res = new JSONObject(response);
					String returnCode = res.getString("returnCode");
					returnMessage = res.getString("returnMessage");
					if (returnCode.equals("0000"))
					{
						// 校验成功
						Log.e(TAG, "校验成功...");
						UIHandler.InitProgressStop();
						mCheckedSim = true;
						Toast("校验成功！", 0);
						// UIHandler.MsgBoxOne("校验成功！");
					}
					else
					{
						// 校验失败
						Log.e(TAG, "校验失败...");
						UIHandler.MsgBoxOne("校验失败，失败原因：\n" + returnMessage);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Log.e(TAG, "校验失败，异常..." + e.toString());
					UIHandler.MsgBoxOne("校验失败，失败原因：\n未知错误！");
				}
			}
		}.start();
	}

	// 先获取套餐列表，然后显示
	private void getProList()
	{
		mProData = new HashMap<String, List<ProductInfo>>();
		final String bill_id = mEtOpenTel.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(bill_id))
		{
			UIHandler.MsgBoxOne("请先填写开户电话！");
			return;
		}
		if (bill_id.length() != 11)
		{
			UIHandler.MsgBoxOne("开户电话填写有误！");
			return;
		}
		UIHandler.initProgressStart("获取套餐列表", "正在获取列表...");
		if(todayHadLoad())
		{
			// 今天已经加载过套餐列表
			Log.e(TAG, "今日已经加载过列表，从数据库读取...");
			mProInfoDB.open();
			mProData.put(ProductInfo.goTone, mProInfoDB.fetchDataByTag(ProductInfo.goTone));
			mProData.put(ProductInfo.mZone, mProInfoDB.fetchDataByTag(ProductInfo.mZone));
			mProData.put(ProductInfo.easyOwn, mProInfoDB.fetchDataByTag(ProductInfo.easyOwn));
			mProInfoDB.close();
			if (!CollectionUtil.isEmptyOrNull(mProData))
			{
				UIHandler.InitProgressStop();
				Log.e(TAG, "从数据库读取成功...");
				UIHandler.sendMessage(UIHandler.MSG_GET_PROLIST_SUC);
			}
			else
			{
				UIHandler.MsgBoxOne("获取列表失败，失败原因：\n本地数据库未空！");
			}
		}
		else
		{
			// 今天还未加载过套餐列表
			Log.e(TAG, "今日还未加载过列表，从服务器读取...");
			new Thread()
			{
				@Override
				public void run()
				{
					String returnMessage = "请求超时！";
					try
					{
						if (!NetworkUtil.isConnect(mMainActivity))
						{
							UIHandler.MsgBoxOne("请检查当前的网络连接状态！");
							return;
						}
						String response = "";
						if(UIHandler.IS_DEBUG)
						{
							response = "{\"returnCode\":\"0000\",\"returnMsg\":\"调用成功\",\"returnCode_goTone\":\"0000\",\"returnMsg_goTone\":\"调用成功\",\"returnCode_mZone\":\"0000\",\"returnMsg_mZone\":"
									+ "调用成功\",\"returnCode_easyOwn\":\"0000\",\"returnMsg_easyOwn\":\"调用成功\",\"returnCode_cardProInfo\":\"1001\",\"returnMsg_cardProInfo\":\"在号码未用表中未找到号码:15238351861号码"
									+ "信息;\n查询参数:[号码=15238351861,地区=A]\nMID:001\","
									+ "\"goToneList\":[{\"BRAND_ID\":\"100011100001\",\"PRODUCT_CODE\":\"PIXFXSJG\",\"CAN_SEL\":\"\",\"NOTES\":\"\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"月功能费280元/月，"
									+ "包含国内数据流量10G,套餐外流量0.29元/M，不允许做为共享终端的主卡\",\"PRODUCT_NAME\":\"4G飞享套餐数据卡280元\",\"PRODUCT_ID\":\"100165000368\"},"
									+ "{\"BRAND_ID\":\"100011100001\",\"PRODUCT_CODE\":\"PIXFXSJF\",\"CAN_SEL\":\"\",\"NOTES\":\"\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"月功能费40元/月，包含国内数据流量"
									+ "400M,套餐外流量0.29元/M，不允许做为共享终端的主卡\",\"PRODUCT_NAME\":\"4G飞享套餐数据卡40元\",\"PRODUCT_ID\":\"100165000367\"}],"
									+ "\"mZoneList\":[{\"BRAND_ID\":\"100011100003\",\"PRODUCT_CODE\":\"PIXDSWSNE\",\"CAN_SEL\":\"\",\"NOTES\":\"\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"月功能使用费108元/"
									+ "月，免费使用来电显示和手机邮箱(免费版)，包含省内移动数据流量2.5G，本地主叫30分钟，短信100条，超出后本地主叫0.12元/分钟，国内长途0.25元/分钟，移动数据流量1元/M，短信0.1"
									+ "元/条。本地被叫免费，其他执行标准资费。\",\"PRODUCT_NAME\":\"动感地带上网套餐（省内版）-108元\",\"PRODUCT_ID\":\"100166000089\"},"
									+ "{\"BRAND_ID\":\"100011100003\",\"PRODUCT_CODE\":\"PIXDSWSND\",\"CAN_SEL\":\"\",\"NOTES\":\"\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"月功能使用费58元/月，免费使用来"
									+ "电显示和手机邮箱(免费版)，包含省内移动数据流量1G，本地主叫30分钟，短信100条，超出后本地主叫0.12元/分钟，国内长途0.25元/分钟，移动数据流量1元/M，短信0.1元/条。本地被叫"
									+ "免费，其他执行标准资费。\",\"PRODUCT_NAME\":\"动感地带上网套餐（省内版）-58元\",\"PRODUCT_ID\":\"100166000088\"}],"
									+ "\"easyOwnList\":[{\"BRAND_ID\":\"100011100002\",\"PRODUCT_CODE\":\"PIXPGPA\",\"CAN_SEL\":\"\",\"NOTES\":\"经分9012856\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"无套餐费"
									+ "，仅支持上网功能，费用按照0.3元/m收取，需与苹果皮配合使用。按照现行GPRS规定，执行双封顶。\",\"PRODUCT_NAME\":\"苹果皮数据卡专属资费\",\"PRODUCT_ID\":\"100166000167\"},"
									+ "{\"BRAND_ID\":\"100011100002\",\"PRODUCT_CODE\":\"PIXDZKQA\",\"CAN_SEL\":\"\",\"NOTES\":\"经分9012629\",\"ERR_MSG\":\"\",\"PRODUCT_TYPE\":\"OFFER_PLAN\",\"PRODUCT_DESC\":\"电子考勤校和套餐，10元"
									+ "最低消费：1、套餐包括：电子考勤功能包（7元），30分钟本地通话。2、超出部分：流量1元/M；本地通话0.22元/分钟，省内长途0.3元/分钟，本地接听免费；国内长途拨打0.6元/分钟，国"
									+ "内漫游接听0.4元/分钟。短信0.1元/条，其他标准资费。\",\"PRODUCT_NAME\":\"电子考勤校和套餐\",\"PRODUCT_ID\":\"100165000365\"}],"
									+ "\"cardProInfoMap\":{}}";
						}
						else
						{
							String url = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.URL4, "");
							Log.e(TAG, "读取url4:" + url);
							Map<CharSequence, CharSequence> params = new HashMap<CharSequence, CharSequence>();
							params.put("msTel", ms_tel);
							params.put("bossCode", bossCode);
							params.put("optrid", optrid);
							params.put("scTel", scTel);
							params.put("BILL_ID", bill_id);
							response = HttpClientUtil.post(mMainActivity, url, 5000, params);
							Log.e(TAG, "获取套餐列表返回信息:" + response);
						}
						JSONObject res = new JSONObject(response);
						String returnCode = res.getString("returnCode");
						returnMessage = res.getString("returnMsg");
						if (returnCode.equals("0000"))
						{
							// 获取成功
							Log.e(TAG, "获取成功...");
							// 解析json
							getProFormJson(res);
							UIHandler.InitProgressStop();
							if (!CollectionUtil.isEmptyOrNull(mProData))
							{
								UIHandler.sendMessage(UIHandler.MSG_GET_PROLIST_SUC);
							}
							else
							{
								Log.e(TAG, "获取失败,解析列表失败...");
								UIHandler.MsgBoxOne("获取列表失败，失败原因：\n解析列表失败！");
							}
						}
						else
						{
							// 获取失败
							Log.e(TAG, "获取失败,返回码..." + returnCode);
							UIHandler.MsgBoxOne("获取列表失败，失败原因：\n" + returnMessage);
						}
					}
					catch (Exception e)
					{
						// 获取失败
						e.printStackTrace();
						Log.e(TAG, "获取失败,异常..." + e.toString());
						UIHandler.MsgBoxOne("获取失败，失败原因：\n未知错误！");
					}
				}
			}.start();
		}
	}

	// 今天是否已经加载过套餐列表
	private boolean todayHadLoad()
	{
		Date curDate = new Date();
		String strCurDate = TimeUtil.formatDate(curDate, "yyyy-MM-dd");
		String strLastDate = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.LAST_EDIT_TIME, "");
		if(StringUtil.isEmptyOrNull(strLastDate))
		{
			return false;
		}
		if(isOverDate(strCurDate, strLastDate))
		{
			// 今天尚未加载过列表
			return false;
		}
		return true;
	} 

	// 比较当前时间是否超过上次操作时间
	private boolean isOverDate(String strCurDate, String strLastDate)
	{
		Log.e(TAG, "cur:" + strCurDate + " last:" + strLastDate);
		String[] subCur = strCurDate.split("-");
		String[] subLast = strLastDate.split("-");
		if(Integer.parseInt(subCur[0]) > Integer.parseInt(subLast[0]))
		{
			return true;
		}
		if(Integer.parseInt(subCur[1]) > Integer.parseInt(subLast[1]))
		{
			return true;
		}
		if(Integer.parseInt(subCur[2]) > Integer.parseInt(subLast[2]))
		{
			return true;
		}
		return false;
	}

	// 处理提交
	private void procSubmit()
	{
		String error = checkIdinfo();
		if (!StringUtil.isEmptyOrNull(error))
		{
			UIHandler.MsgBoxOne("以下信息未正确填写：\n" + error);
			return;
		}
		/*
		if (UIHandler.IS_BACK_TRAIL && !mCheckedTel) 
		{
			UIHandler.MsgBoxOne("请先校验补录号码！");
			return;
		}
		if(!UIHandler.IS_BACK_TRAIL && !mCheckedSim)
		{
			UIHandler.MsgBoxOne("请先校验sim卡号！");
			return;
		}
		*/
		UIHandler.initProgressStart("证件信息提交", "正在提交...");
		new Thread()
		{
			@Override
			public void run()
			{
				String returnMessage = "请求超时！";
				try
				{
					if (!NetworkUtil.isConnect(mMainActivity))
					{
						UIHandler.MsgBoxOne("请检查当前的网络连接状态！");
						return;
					}
					Log.e("", "info:" + mCard.toString());
					Map<CharSequence, CharSequence> params = getParams();
					String url = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.URL2, "");
					Log.e(TAG, "读取url2:" + url);
					String response = HttpClientUtil.post(mMainActivity, url, 5000, params);
					Log.e(TAG, "上传用户信息返回数据:" + response);
					JSONObject res = new JSONObject(response);
					String returnCode = res.getString("returnCode");
					returnMessage = res.getString("returnMsg");
					if (returnCode.equals("0000"))
					{
						// 提交成功
						Log.e(TAG, "提交成功...");
						UIHandler.InitProgressStop();
						recycleBitmap();
						UIHandler.MsgBoxOne(TAG, "提交成功！", UIHandler.MSG_COMMIT_SUC);
					}
					else
					{
						// 提交失败
						Log.e(TAG, "提交失败,返回码..." + returnCode);
						UIHandler.MsgBox("提示信息", "提交失败，失败原因：\n" + returnMessage + "\n是否重试？", "重试", "放弃",
								UIHandler.MSG_COMMIT, 0);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Log.e(TAG, "提交失败,异常..." + e.toString());
					UIHandler.MsgBox("提示信息", "提交失败，失败原因：\n" + returnMessage + "\n是否重试？", "重试", "放弃",
							UIHandler.MSG_COMMIT, 0);
				}
			}

			private Map<CharSequence, CharSequence> getParams()
			{
				Map<CharSequence, CharSequence> params = new HashMap<CharSequence, CharSequence>();
				CharSequence sexType = "0";
				if (mCard.getSex().equals("男"))
				{
					sexType = "1";
				}
				params.put("msTel", ms_tel);
				params.put("bossCode", bossCode);
				params.put("optrid", optrid);
				params.put("scTel", scTel);
				
				params.put("MS_VERSION", "MS");
				mCard.setContactTel(mEtOpenTel.getText().toString().trim());
				params.put("BILL_ID", mCard.getContactTel());
				params.put("ContMobile", mEtContactTel.getText().toString().trim());
				params.put("CUST_NAME", mCard.getName());
				params.put("CUST_CERT_TYPE", mCard.getTypeName());
				params.put("CUST_CERT_NO", mCard.getNo());
				params.put("GENDER", sexType);
				params.put("NATION", mCard.getFlok());
				if(!StringUtil.isEmptyOrNull(mCard.getBirthday()))
				{
					String birth = mCard.getBirthday().replace("年", "-").replace("月", "-").replace("日", "");
					params.put("BIRTHDAY", birth);
				}
				params.put("CUST_CERT_ADDR", mCard.getAddress());
				String[] date = mCard.getValidPreod().split("-");
				String expdate = date[1].replaceAll("\\.", "-");
				params.put("CERT_EXPDATE", expdate);
				params.put("ISSUING_AUTHORITY", mCard.getIssueAuthority());
				params.put("ContName", mEtContactName.getText().toString().trim());
				params.put("ResideAddr", mEtContactAddr.getText().toString().trim());
				Bitmap bit1 = BitmapUtil.decodeFile(mStrImageDir + "1.jpg", PIC_WIDTH, PIC_HEIGHT);
				Bitmap bit2 = BitmapUtil.decodeFile(mStrImageDir + "2.jpg", PIC_WIDTH, PIC_HEIGHT);
				String pic1 = Base64.encode(BitmapUtil.Bitmap2Bytes(bit1));
				String pic2 = Base64.encode(BitmapUtil.Bitmap2Bytes(bit2));
				Log.e("图片大小：", "pic1: " + pic1.length() + " pic2 :" + pic2.length());
				bit1.recycle();
				bit2.recycle();
				params.put("PIC_Z", pic1);
				params.put("PIC_F", pic2);
				params.put("SvcNum", mEtOpenTel.getText().toString().trim());
				params.put("IccId", mEtSim.getText().toString().trim());
				Log.e("", "mCurPro:" + mCurPro);
				if (mCurPro != null)
				{
					params.put("OF_CODE", mCurPro.getCode());
					params.put("OF_NAME", mCurPro.getName()); 
				}
				return params;
			}
		}.start();
	}

	// 从json中解析套餐列表
	private void getProFormJson(JSONObject res) throws JSONException
	{
		Log.e(TAG, "开始解析json...");
		mProData.clear();
		JSONArray qqt = res.getJSONArray("goToneList");
		JSONArray dgdd = res.getJSONArray("mZoneList");
		JSONArray szx = res.getJSONArray("easyOwnList");
		if (qqt != null)
		{
			List<ProductInfo> temp1 = parsePro(qqt, ProductInfo.goTone);
			if (temp1 == null)
			{
				temp1 = new ArrayList<ProductInfo>();
			}
			Log.e(TAG, "全球通套餐个数：" + temp1.size());
			mProData.put(ProductInfo.goTone, temp1);
		}
		if (dgdd != null)
		{
			List<ProductInfo> temp2 = parsePro(dgdd, ProductInfo.mZone);
			if (temp2 == null)
			{
				temp2 = new ArrayList<ProductInfo>();
			}
			Log.e(TAG, "动感地带套餐个数：" + temp2.size());
			mProData.put(ProductInfo.mZone, temp2);
		}
		if (szx != null)
		{
			List<ProductInfo> temp3 = parsePro(szx, ProductInfo.easyOwn);
			if (temp3 == null)
			{
				temp3 = new ArrayList<ProductInfo>();
			}
			Log.e(TAG, "神州行套餐个数：" + temp3.size());
			mProData.put(ProductInfo.easyOwn, temp3);
		}
		saveToDB(mProData);
		Date date = new Date();
		String strDate = TimeUtil.formatDate(date, "yyyy-MM-dd");
		SharedUtil.putString(App.ShARED_FILE_NAME, mMainActivity, App.LAST_EDIT_TIME, strDate);
		Log.e(TAG, "解析json完成...");
	}

	// 保存菜单列表到数据库
	private void saveToDB(Map<String, List<ProductInfo>> data)
	{
		mProInfoDB.open();
		// 先清空原有数据再保存
		mProInfoDB.clear();
		for (Entry<String, List<ProductInfo>> entry : data.entrySet()) 
		{
			   for(ProductInfo pro : entry.getValue())
			   {
				   mProInfoDB.insert(pro);
			   }
		}
		mProInfoDB.close();
	} 

	// 从json中解析套餐数据
	private List<ProductInfo> parsePro(JSONArray array, String brandType) throws JSONException
	{
		List<ProductInfo> result = new ArrayList<ProductInfo>();
		for (int i = 0; i < array.length(); i++)
		{
			JSONObject json = array.getJSONObject(i);
			ProductInfo pro = new ProductInfo();
			pro.setID(json.getString("PRODUCT_ID"));
			pro.setNote(json.getString("NOTES"));
			pro.setCode(json.getString("PRODUCT_CODE"));
			pro.setBrandID(json.getString("BRAND_ID"));
			pro.setCanCel(true);
			pro.setName(json.getString("PRODUCT_NAME"));
			pro.setErrMsg(json.getString("ERR_MSG"));
			pro.setType(json.getString("PRODUCT_TYPE"));
			pro.setDesc(json.getString("PRODUCT_DESC"));
			pro.setBrandType(brandType);
			result.add(pro);
		}
		return result;
	}

	// 释放图像资源
	private void recycleBitmap()
	{
		if (mBitmap1 != null)
		{
			mBitmap1.recycle();
			mBitmap1 = null;
		}
		if (mBitmap2 != null)
		{
			mBitmap2.recycle();
			mBitmap2 = null;
		}
	}

	// 处理拍照
	private void procTakeIdcard(int nStatus)
	{
		Intent intent = new Intent(mMainActivity, CameraActivity.class);
		if (nStatus == 0x01)
		{
			intent.putExtra("IMAGE_NAME", mStrImageDir + "1.jpg");
		}
		else
		{
			intent.putExtra("IMAGE_NAME", mStrImageDir + "2.jpg");
		}
		mMainActivity.startActivityForResult(intent, MSG_TAKE_IDCARD);
	}

	// 检查用户录入信息
	private String checkIdinfo()
	{
		StringBuffer sb = new StringBuffer();
		String name = mEtName.getText().toString().trim();
		String no = mEtNo.getText().toString().trim();
		String sex = mEtSex.getText().toString().trim();
		String address = mEtAddr.getText().toString().trim();
		String date = mEtValidDate.getText().toString().trim();
		String openTel = mEtOpenTel.getText().toString().trim();
		String contTel = mEtContactTel.getText().toString().trim();
		String contName = mEtContactName.getText().toString().trim();
		String contAddr = mEtContactAddr.getText().toString().trim();
		
		if (StringUtil.isEmptyOrNull(no))
		{
			sb.append("证件号码、");
		}
		else if (!IDCardUtils.isNoValid("01", no))
		{
			sb.append("证件号码、");
		}
		if (StringUtil.isEmptyOrNull(name))
		{
			sb.append("姓名、");
		}
		else if (!IDCardUtils.isNameValid("01", name))
		{
			sb.append("姓名、");
		}
		if (StringUtil.isEmptyOrNull(sex))
		{
			sb.append("性别、");
		}
		else if (!"男".equals(sex) && !"女".equals(sex))
		{
			sb.append("性别、");
		}
		if (StringUtil.isEmptyOrNull(address))
		{
			sb.append("地址、");
		}
		else if (IDCardUtils.isAddressValid("01", address) != 0)
		{
			sb.append("地址、");
		}
		if (StringUtil.isEmptyOrNull(date))
		{
			sb.append("有效期、");
		}
		// else if (!StringUtil.isEmptyOrNull(IDCardUtils.checkIDEndTime(date)))
		// {
		// sb.append("有效期、");
		// }
		if (StringUtil.isEmptyOrNull(openTel))
		{
			sb.append("开户电话、");
		}
		else if (openTel.length() != 11)
		{
			sb.append("开户电话、");
		}
		if (StringUtil.isEmptyOrNull(contTel))
		{
			sb.append("联系电话、");
		}
		else if (contTel.length() != 11)
		{
			sb.append("联系电话、");
		}
		if (StringUtil.isEmptyOrNull(contName))
		{
			sb.append("联系人姓名、");
		}
		// else if (IDCardUtils.isAddressValid("01", address) != 0)
		// {
		// sb.append("联系人姓名、");
		// }
		if (StringUtil.isEmptyOrNull(contAddr))
		{
			sb.append("通信地址、");
		}
		// else if (IDCardUtils.isAddressValid("01", address) != 0)
		// {
		// sb.append("通信地址、");
		// }
		
		/*
		if(!UIHandler.IS_BACK_TRAIL)
		{
			if(StringUtil.isEmptyOrNull(mEtSim.getText().toString().trim()))
			{
				sb.append("sim卡号、");
			}
			if (mCurPro == null)
			{
				sb.append("套餐信息、");
			}
		}
		*/
		if (!StringUtil.isEmptyOrNull(sb.toString()))
		{
			if (sb.toString().endsWith("、"))
			{
				return sb.toString().trim().substring(0, sb.toString().trim().length() - 1) + "。";
			}
			else
			{
				return sb.toString().toString().trim() + "。";
			}
		}
		return sb.toString();
	}

	@Override
	public void MsgOK(int msgID)
	{
		switch (msgID)
		{
			// 身份证识别成功
			case MSG_REG_SUC:
				procOcrOk();
				break;
			// 身份证识别失败
			case MSG_REG_FAIL:
				MsgBoxOne(TITLE_STR, "身份证识别失败！", 0, UIHandler.MSG_ERROR);
				break;
			// 提交
			case UIHandler.MSG_COMMIT:
				procSubmit();
				break;
			// 获取套餐列表成功
			case UIHandler.MSG_GET_PROLIST_SUC:
				SelectProductDialog dlg = new SelectProductDialog(mMainActivity, mHandler, mProData);
				dlg.show();
				break;
			// 提交成功
			case UIHandler.MSG_COMMIT_SUC:
				Log.e(TAG, "to lay_mainmenu");
				mMainActivity.goto_Layout(R.layout.lay_mainmenu);
				break;
		}
	}

	@Override
	protected void MsgCancle(int msgID)
	{
		mMainActivity.goto_Layout(R.layout.lay_mainmenu);
	}

	// 处理识别成功
	private void procOcrOk()
	{
		if (mIdcardType == 0x01)
		{
			// 身份证姓名
			mEtNo.setText(mCard.getNo());
			// 身份证姓名
			mEtName.setText(mCard.getName());
			// 身份证性别
			mEtSex.setText(mCard.getSex());
			// 证件地址
			mEtAddr.setText(mCard.getAddress());
			// 联系人姓名
			mEtContactName.setText(mCard.getName());
			// 联系人通信地址
			mEtContactAddr.setText(mCard.getAddress());
			// 身份证正面图像
			if (mBitmap1 != null)
			{
				mBitmap1.recycle();
				mBitmap1 = null;
			}
			mBitmap1 = BitmapUtil.decodeFile(mStrImageDir + "1.jpg", mIvIdcard1.getWidth(), mIvIdcard1.getHeight());
			// mBitmap1 = BitmapFactory.decodeFile(mStrImageDir+"1.jpg");
			if (mBitmap1 != null)
			{
				mIvIdcard1.setImageBitmap(mBitmap1);
			}
		}
		else if (mIdcardType == 0x02)
		{
			try
			{
				// 证件有效期
				// Log.e("", "证件有效期为: " + mCard.getValidPreod());
				String[] ss = mCard.getValidPreod().split("-");
				if (ss.length > 1)
				{
					mEtValidDate.setText(ss[1]);
				}
				// 身份证返回图像
				if (mBitmap2 != null)
				{
					mBitmap2.recycle();
					mBitmap2 = null;
				}
				mBitmap2 = BitmapUtil.decodeFile(mStrImageDir + "2.jpg", mIvIdcard2.getWidth(), mIvIdcard2.getHeight());
				// mBitmap2 = BitmapFactory.decodeFile(mStrImageDir+"2.jpg");
				if (mBitmap2 != null)
				{
					mIvIdcard2.setImageBitmap(mBitmap2);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Log.e("", e.toString());
			}
		}
	}
}
