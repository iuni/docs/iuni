package com.ai.cmccha.sc.idcard.util;

import java.util.Collection;
import java.util.Map;

// 集合工具类
@SuppressWarnings("rawtypes")
public class CollectionUtil
{
	// 判断顺序容器是否为空或者null
	public static final boolean isEmptyOrNull(Collection c) 
	{
		return c == null || c.size() == 0;
	}
	
	// 判断关联容器是否为空或者null
	public static final boolean isEmptyOrNull(Map c) 
	{
		return c == null || c.size() == 0;
	}
}
