package com.ai.cmccha.sc.idcard.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// DBHelper做创建数据表和更新数据表的操作
public class DBHelper extends SQLiteOpenHelper
{
	// 数据库名称为data
	public static final String 	DB_NAME 		= "CUMini.db";
	// 数据库版本
	public static int 			DB_VERSION 		= 1;
	// 传入Context
	public static Context 		mMainActivity;
	
	// 创建蓝牙表
	private final String 		mDbBluetooth 	= "CREATE TABLE BLUETOOTH ("
													+ "_id TEXT PRIMARY KEY, " 
													+ "bluetooth_tag TEXT,"
													+ "bluetooth_name TEXT,"
													+ "bluetooth_mac TEXT)";
	
	// 创建参数表
	private final String 		mDbParam 		= "CREATE TABLE PARAMS ("
													+ "_id integer PRIMARY KEY autoincrement, " 
													+ "s_type varchar(4),"
													+ "s_code varchar(10),"
													+ "s_value TEXT,"
													+ "s_relationcode varchar(10),"
													+ "s_param varchar(10))";
	
	// 创建参数表
	private final String 		mDbProInfo 		= "CREATE TABLE PRODUCT_INFO ("
													+ "_id integer PRIMARY KEY autoincrement, " 
													+ "pro_id TEXT,"
													+ "pro_name TEXT,"
													+ "pro_note TEXT,"
													+ "pro_code TEXT,"
													+ "pro_brand_id TEXT,"
													+ "pro_errmsg TEXT,"
													+ "pro_type TEXT,"
													+ "pro_desc TEXT,"
													+ "pro_cancel TEXT,"
													+ "pro_brand_type TEXT)";

	// 构造
	public DBHelper(Context context) 
	{
		super(context, DB_NAME, null, DB_VERSION);
		mMainActivity = context;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(mDbBluetooth);
		db.execSQL(mDbParam);
		db.execSQL(mDbProInfo);
	}

	/**
	 * 当数据库需要升级的时候，Android系统会主动的调用这个方法。
	 * 一般我们在这个方法里边删除数据表，并建立新的数据表，
	 * 当然是否还需要做其他的操作，完全取决于应用的需求。
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS BLUETOOTH");
		db.execSQL("DROP TABLE IF EXISTS PARAMS");
		db.execSQL("DROP TABLE IF EXISTS PRODUCT_INFO");
		// 重建数据库
		onCreate(db);
	}
}
