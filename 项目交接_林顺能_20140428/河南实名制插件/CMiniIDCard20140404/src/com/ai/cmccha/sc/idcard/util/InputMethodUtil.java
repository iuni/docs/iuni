package com.ai.cmccha.sc.idcard.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class InputMethodUtil
{
	// ���뷨����
	public static InputMethodManager	mInputMethod	= null;

	public static void closeSoftKeyboard(Activity activity)
	{
		if (mInputMethod == null)
		{
			mInputMethod = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		}
		if (mInputMethod != null)
		{
			if (mInputMethod.isActive())
			{
				try
				{
					View view = activity.getCurrentFocus();
					if (view != null)
					{
						mInputMethod.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					}

					// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
					// mInputMethod.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
					// InputMethodManager.HIDE_NOT_ALWAYS);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static void closeSoftKeyboard(Dialog dialog)
	{
		if (mInputMethod == null)
		{
			mInputMethod = (InputMethodManager) dialog.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		}
		if (mInputMethod != null)
		{
			if (mInputMethod.isActive())
			{
				try
				{
					View view = dialog.getCurrentFocus();
					if (view != null)
					{
						mInputMethod.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					}

					// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
					// mInputMethod.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
					// InputMethodManager.HIDE_NOT_ALWAYS);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
};
