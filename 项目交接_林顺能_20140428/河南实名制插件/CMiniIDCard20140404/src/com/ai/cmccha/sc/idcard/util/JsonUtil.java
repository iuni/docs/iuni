package com.ai.cmccha.sc.idcard.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.util.Log;

// JSON工具类
public class JsonUtil 
{
	// JSON交互
	public static JSONObject excute(String strServerUrl, JSONObject jsonObject) 
	{
		try 
		{
			HttpPost request = new HttpPost(strServerUrl);
			Log.d("发送的json串", jsonObject.toString());
			// 绑定到请求 Entry
			StringEntity se = new StringEntity(jsonObject.toString());
			request.setEntity(se);
			// 发送请求
			HttpResponse httpResponse = new DefaultHttpClient().execute(request);
			// 得到应答的字符串，这也是一个 JSON 格式保存的数据
			String retSrc = EntityUtils.toString(httpResponse.getEntity());
			Log.d("返回的json串", retSrc);
			// 生成 JSON 对象
			JSONObject result = new JSONObject(retSrc);
			return result;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
}
