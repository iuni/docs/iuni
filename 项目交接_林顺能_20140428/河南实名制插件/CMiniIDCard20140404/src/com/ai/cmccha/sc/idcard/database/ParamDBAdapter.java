package com.ai.cmccha.sc.idcard.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ai.cmccha.sc.idcard.bean.CrcData;
import com.ai.cmccha.sc.idcard.bean.KeyTextBean;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;



// 分类存放：   
// 0x00: 	各类CRC 
// 0x01:	业务菜单 
// 0x02:	套餐信息(产品包信息) 
// 0x03:	增值业务列表 
// 0x04:	特服业务列表
// 0x05:	选号规则列表 
// 0x06:	品牌列表 
// 0x07:	开机欢迎语更新 
// 0x08:	打印凭条广告信息 
// 0x09:	终端收件箱内容更新
// 0x0A:	3G套餐信息
// 0x21：	远程升级的服务器地址
public class ParamDBAdapter extends DBadapter 
{
	// 数据库表名
	private static final String DB_TABLE 		= "PARAMS";
	// 表中一条数据的ID
	private static final String KEY_ID 			= "_id";
	// 表中字段：参数类型
	private static final String KEY_TYPE 		= "s_type";
	// 表中字段：参数键值
	private static final String KEY_CODE 		= "s_code";
	// 表中字段：参数内容
	private static final String KEY_VALUE 		= "s_value";
	// 表中字段：套餐对应品牌的code
	private static final String KEY_RELATION 	= "s_relationcode";
	// 表中字段：附加信息
	private static final String KEY_PARAM 		= "s_param";
	
	// 邮件ID
	public static final String	MAIL_ID			= "mail_id";
	// 邮件日期
	public static final String	MAIL_TIME		= "mail_time";
	// 邮件内容
	public static final String	MAIL_RES		= "mail_res";
	
	public ParamDBAdapter(Context context) 
	{
		super(context);
	}
	
	// 插入数据
	public long insertData(String type, String value) 
	{
		return insertData(type, "", value);
	}
	
	// 插入数据
	private long insertData(String type, String key, String value) 
	{
		return insertData(type, key, value, "", "");
	}
	
	public long insertData(String type, String key, String value, String ralationcode, String param) 
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TYPE, 	type);
		initialValues.put(KEY_CODE, 	key);
		initialValues.put(KEY_VALUE, 	value);
		initialValues.put(KEY_RELATION, ralationcode);
		initialValues.put(KEY_PARAM, 	param);
		return db.insert(DB_TABLE, KEY_ID, initialValues);
	}
	
	// 打印某中类型在本地数据库中的值
	public void printMsgByType(String type) 
	{
		Cursor cursor = db.query(DB_TABLE, 
				new String[] {KEY_ID, KEY_TYPE, KEY_CODE, KEY_VALUE, KEY_RELATION, KEY_PARAM },
				KEY_TYPE + "= ?", 
				new String[] { type }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "查询到的记录数: " + nCount);
			if(nCount != 0) 
			{
				while (cursor.moveToNext()) 
				{
					Log.i("", "查询类型为: " + type 
							+ cursor.getString(cursor.getColumnIndex(KEY_ID)) 		+ ","
							+ cursor.getString(cursor.getColumnIndex(KEY_TYPE)) 	+ ","
							+ cursor.getString(cursor.getColumnIndex(KEY_CODE)) 	+ ","
							+ cursor.getString(cursor.getColumnIndex(KEY_VALUE)) 	+ ","
							+ cursor.getString(cursor.getColumnIndex(KEY_RELATION)) + ","
							+ cursor.getString(cursor.getColumnIndex(KEY_PARAM))
						 );
				}
			}
			cursor.close();
		}
	}
	
	// 获取某中类型在本地数据库中的值
	public HashMap<String, String> getMsgByType(String type) 
	{
		HashMap<String, String> mapMsg = new HashMap<String, String>();
		Cursor cursor = db.query(DB_TABLE, 
				new String[] {KEY_ID, KEY_TYPE, KEY_CODE, KEY_VALUE, KEY_RELATION, KEY_PARAM },
				KEY_TYPE + "= ?", 
				new String[] { type }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "查询到的记录数: " + nCount);
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					mapMsg.put(cursor.getString(cursor.getColumnIndex(KEY_CODE)), 
							cursor.getString(cursor.getColumnIndex(KEY_VALUE)));
				}
			}
			cursor.close();
			return mapMsg;
		}
		return null;
	}
	
	// 根据存储的类型返回相关的信息
	public List<KeyTextBean> getContentByType(String type) 
	{
		List<KeyTextBean> listBean = new ArrayList<KeyTextBean>();
		Cursor cursor = db.query(DB_TABLE, 
				new String[] { KEY_CODE, KEY_VALUE, KEY_PARAM }, 
				KEY_TYPE + "= ?", 
				new String[] { type }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "查询到的记录数: " + nCount);
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					KeyTextBean keyBean = new KeyTextBean(cursor.getString(cursor.getColumnIndex(KEY_CODE)),
							cursor.getString(cursor.getColumnIndex(KEY_VALUE)));
					listBean.add(keyBean);
				}
			}
			cursor.close();
		}
		return listBean;
	}
	
	// 根据存储的类型、参数type 返回相关的信息
	public List<KeyTextBean> getContentByType(String type, String paramType) 
	{
		Cursor cursor = db.query(DB_TABLE, 
				new String[] { KEY_CODE, KEY_VALUE, KEY_PARAM }, 
				KEY_TYPE + "= ? and "+ KEY_PARAM + "=?", 
				new String[] { type, paramType }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "查询到的记录数: " + nCount);
			
			List<KeyTextBean> listBean = new ArrayList<KeyTextBean>();
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					KeyTextBean keyBean = new KeyTextBean(cursor.getString(cursor.getColumnIndex(KEY_CODE)),
							cursor.getString(cursor.getColumnIndex(KEY_VALUE)));
					listBean.add(keyBean);
				}
			}
			cursor.close();
			return listBean;
		}
		return null;
	}
	
	// 根据存储的类型、关系code、参数type 返回相关的信息
	// @param type 存储类型                                （比如：套餐-02）
	// @param relation_code 关联类型         （如：    套餐所属品牌code）
	// @param param_type    参数类型         （如：    付费类型）
	// @return
	public List<KeyTextBean> getContentByType(String type, String relation_code, String param_type) 
	{
		Cursor cursor = db.query(DB_TABLE, 
				new String[] { KEY_CODE, KEY_VALUE, KEY_PARAM },
				KEY_TYPE + "= ? and " + KEY_RELATION + "=? and "+ KEY_PARAM + "=?", 
				new String[] { type, relation_code, param_type }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "总共的记录数: " + nCount);
			
			List<KeyTextBean> listBean = new ArrayList<KeyTextBean>();
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					KeyTextBean keyBean = new KeyTextBean(cursor.getString(cursor.getColumnIndex(KEY_CODE)),
							cursor.getString(cursor.getColumnIndex(KEY_VALUE)));
					listBean.add(keyBean);
				}
			}
			cursor.close();
			return listBean;
		}
		return null;
	}
	
	// 删除记录
	public boolean deletebytype(String type) 
	{
		return db.delete(DB_TABLE, KEY_TYPE + "= ?", new String[] { type }) > -1;
	}
	
	// 插入菜单
	public long insertMenu(String code, String name) 
	{
		return insertData("01", code, name);
	}
	
	// 所有的菜单
	public void getAllMenu() 
	{
		Cursor cursor = db.query(DB_TABLE, new String[] { KEY_CODE, KEY_VALUE }, KEY_TYPE + "= ?", new String[] { "01" }, null, null, null); 
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "总共的菜单数: " + nCount);
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					String menu = cursor.getString(cursor.getColumnIndex(KEY_VALUE));
					Log.e("DB", menu);
				}
			}
			cursor.close();
		}
	}
	
	// 读取收件箱的中信息
	public List<Map<String, Object>> getMails(String type) 
	{ 
		String sql = "select * from PARAMS where s_type='" + type + "' order by _id desc ";
		Cursor cursor = db.rawQuery(sql, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "总共的邮件数: " + nCount);
			
			List<Map<String, Object>> listMails = new ArrayList<Map<String,Object>>(); 
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					Map<String, Object> item = new HashMap<String, Object>();
					// 邮件序号
					String strMailID = cursor.getString(cursor.getColumnIndex(KEY_ID));
					item.put(MAIL_ID, strMailID);
					// 邮件日期
					String strMailTime = cursor.getString(cursor.getColumnIndex(KEY_CODE)).substring(0, 8);
		 			item.put(MAIL_TIME, strMailTime);
		 			// 邮件内容
		 			String strMailRes = cursor.getString(cursor.getColumnIndex(KEY_VALUE));
					item.put(MAIL_RES, strMailRes);
					
					Log.e("", "邮件序号: " + strMailID + ", 邮件日期: " + strMailTime + ", 邮件内容: " + strMailRes);
					listMails.add(item);
				}
			}
			cursor.close();
			return listMails;
		}
		return null;
	}
	
	// 根据邮件序号来删除收件箱的内容
	public boolean deleteMail(String strMailID) 
	{
		if(db.delete(DB_TABLE,  KEY_ID + " = ?", new String[]{ strMailID }) > 0) 
		{
			return true;
		}
		return false;
	}
	
	// 插入号段
	public long insertNumber(String strNumberCode, String strNumberName) 
	{
		return insertData("02", strNumberCode, strNumberName);
	}
	
	// 插入3G套餐数据
	public long insertCombo_3G(String strPackageCode, String strPackageName, String strBrandCode, String strPayType) 
	{
		return insertData("0A", strPackageCode, strPackageName, strBrandCode, strPayType);	
	}
	
	// 插入品牌信息
	public long insertBrand(String strBrandCode, String strBrandName, String strBrandType)
	{
		return insertData("06", strBrandCode, strBrandName, null, strBrandType);
	}
	
	// 插入邮件信息
	public long insertMail(String strMailTime, String strMailRes, String strMailType, String strReversed) 
	{
		return insertData("09", strMailTime, strMailRes, strMailType, strReversed);
	}
	
	// 获取参数版本CRC
	public String getParamCrc(String strParamCode) 
	{
		Cursor cursor = db.query(DB_TABLE, new String[]{KEY_VALUE}, KEY_CODE + "=?", new String[]{strParamCode}, null, null, null);
		String crc = "0";
		
		if(cursor == null) 
		{
			return null;
		}
		if(cursor.getCount() == 0) 
		{
			cursor.close();
			return null;
		}
		while (cursor.moveToNext()) 
		{
			crc = cursor.getString(cursor.getColumnIndex(KEY_VALUE));
		}
		cursor.close();
		return crc;
	}
	
	// 更新或者插入CRC
	public void updateOrInsertCRC(String type, String crc) 
	{
		String sql = "SELECT * from " + DB_TABLE + " WHERE " + KEY_TYPE + "='00' AND " + KEY_CODE + "='" + type + "'";
		Log.e("", "查询语句: " + sql);
		
		Cursor cursor = db.rawQuery(sql, null);
		if(cursor == null || cursor.getCount() == 0)  
		{
			Log.d("", "不存在参数版本, 插入...");
			// 插入记录
			insertData("00", type, crc);
			// 更新内存里的CRC信息
			sycCRCData(type, crc);
			// 关闭游标
			if(cursor != null) 
			{
				cursor.close();
			}
			return;
		}
		if(cursor != null) 
		{
			Log.e("", "查询到的记录: " + cursor.getCount());
			// 更新记录
			ContentValues args = new ContentValues();
			args.put(KEY_VALUE, crc);
			db.update(DB_TABLE, args, KEY_TYPE + " = ? and " + KEY_CODE + " = ?", new String[] { "00", type });
			// 更新内存里的CRC信息
			sycCRCData(type, crc);
			cursor.close();
		}
	}
	
	// 得到本地数据库中的所有CRC数据, 并更新到内存
	public void getAllCRC() 
	{
		Cursor cursor = db.query(DB_TABLE, 
				new String[] { KEY_CODE, KEY_VALUE },
				KEY_TYPE + " = ?", 
				new String[] { "00" }, 
				null, null, null);
		if(cursor != null) 
		{
			int nCount = cursor.getCount();
			Log.e("", "查询到的CRC记录数: " + nCount);
			if(nCount > 0) 
			{
				while (cursor.moveToNext()) 
				{
					String code  = cursor.getString(cursor.getColumnIndex(KEY_CODE));
					String value = cursor.getString(cursor.getColumnIndex(KEY_VALUE));
					sycCRCData(code, value);
				}
			}
			cursor.close();
		}
	}
	
	// 更新内存里的CRC信息
	private void sycCRCData(String strCrcType, String crcStr) 
	{
		if ("01".equalsIgnoreCase(strCrcType)) 
		{
			CrcData.mCrcMenu = crcStr;
			Log.d("", "菜单crc更新为: " + crcStr);
		} 
		else if ("02".equalsIgnoreCase(strCrcType)) 
		{
			CrcData.mCrcNumber = crcStr;
			Log.d("", "号段crc更新为: " + crcStr);
		}
		else if ("03".equalsIgnoreCase(strCrcType)) 
		{
			CrcData.mCrcPrint = crcStr;
			Log.d("", "打印广告语crc更新为: " + crcStr);
		} 
		else if ("04".equalsIgnoreCase(strCrcType)) 
		{
			CrcData.mCrcMail = crcStr;
			Log.d("", "邮件crc更新为: " + crcStr);
		} 
	}
}
