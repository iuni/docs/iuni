package com.ai.cmccha.sc.idcard.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;

import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.util.CrcUtil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

public class FtpHelper
{
	private static final String	 	TAG				=  "FtpHelper";		// 提示框标题
	private	static FTPClient	 	mFtpClient 		=  new FTPClient(); // 创建FTP客户端
	protected static byte[] 		mCrc 			=  new byte[2];		// 检查完整性，暂时没有用到
	
	// 连接服务器
	public static boolean connect(String hostname, int port, String username,String password) throws IOException
	{
		// 设置超时时间
		mFtpClient.setConnectTimeout(20*1000);
		mFtpClient.connect(hostname, port);
		mFtpClient.setControlEncoding("GBK");
		if (FTPReply.isPositiveCompletion(mFtpClient.getReplyCode())) 
		{
			if (mFtpClient.login(username, password)) 
			{
				return true;
			}
		}
		disconnect();
		return false;
	}
	
	// 下载版本文件方法
	public static int downloadVersion(String remote, String local) throws IOException
	{
		// 设置被动模式
		mFtpClient.enterLocalPassiveMode();
		// 设置以二进制方式传输
		mFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
		// 检查远程文件是否存在
		FTPFile[] files = mFtpClient.listFiles(new String(remote
			.getBytes("GBK"), "iso-8859-1"));
		if (files.length == 0) 
		{
			// 远程文件不存在
			Log.d(TAG, "File not exist!");
				return 0;
		}
		long lRemoteSize = files[0].getSize();
		File f = new File(local);
		// 本地存在文件，进行断点下载
		if (f.exists())
		{
			long localSize = f.length();
			// 判断本地文件大小是否大于远程文件大小
			if (localSize > lRemoteSize)
			{
				// 本地文件大于远程文件，下载中止
				Log.d(TAG, "Download length illegal!");
				return 1;
			} 
			else if (localSize == lRemoteSize) 
			{
				Log.d(TAG, "Download complete!");
				return 2;
			}
			// 进行断点续传，并记录状态
			FileOutputStream out = new FileOutputStream(f, true);
			mFtpClient.setRestartOffset(localSize);
			InputStream in = mFtpClient.retrieveFileStream(new String(remote
					.getBytes("GBK"), "iso-8859-1"));
			byte[] bytes = new byte[1024];
			long step = lRemoteSize / 100;
			long process = localSize / step;
			int c;
			while ((c = in.read(bytes)) != -1) 
			{
				out.write(bytes, 0, c);
				localSize += c;
				long nowProcess = localSize / step;
				if (nowProcess > process) 
				{
					process = nowProcess;
				}
			}
			UIHandler.initBusiStop();
			in.close();
			out.close();
			boolean isDo = mFtpClient.completePendingCommand();
			if (isDo)
			{
				// Download_From_Break_Success
				Log.d(TAG, "Download_From_Break_Success!");
				return 3;
			}
			else
			{
				// Download_From_Break_Failed
				Log.d(TAG, "Download_From_Break_Failed!");
				return 4;
			}
		}
		// 本地不存在文件，进行完全下载
		else
		{
			OutputStream out = new FileOutputStream(f);
			InputStream in = mFtpClient.retrieveFileStream(new String(remote
					.getBytes("GBK"), "iso-8859-1"));
			byte[] bytes = new byte[1024];
			long step;
			step = lRemoteSize / 100;
			long process = 0;
			long localSize = 0L;
			int c;
			while ((c = in.read(bytes)) != -1) 
			{
				out.write(bytes, 0, c);
				localSize += c;
				long nowProcess = localSize / step;
				if (nowProcess > process)
				{
					process = nowProcess;
				}
			}
			UIHandler.initBusiStop();
			System.out.println(bytes);
			mCrc = CrcUtil.Pro_General_CalcCrc(bytes, 0, bytes.length);
			in.close();
			out.close();
			boolean upNewStatus = mFtpClient.completePendingCommand();
			if (upNewStatus) 
			{
				// Download_New_Success
				Log.d(TAG, "Download_New_Success!");
				return 5;
			} 
			else
			{
				// Download_New_Failed
				Log.d(TAG, "Download_New_Failed!");
				return 6;
			}
		}
	}
	
	public static int download(String remote, String local) throws IOException
	{
		// 设置被动模式
		mFtpClient.enterLocalPassiveMode();
		// 设置以二进制方式传输
		mFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
		// 检查远程文件是否存在
		FTPFile[] files = mFtpClient.listFiles(new String(remote
				.getBytes("GBK"), "iso-8859-1"));
		if (files.length == 0) 
		{
			// 远程文件不存在
			Log.d(TAG, "File not exist!");
			return 0;
		}
		long lRemoteSize = files[0].getSize();
		File f = new File(local);
		// 本地存在文件，进行断点下载
		if (f.exists())
		{
			long localSize = f.length();
			// 判断本地文件大小是否大于远程文件大小
			if (localSize > lRemoteSize)
			{
				// 本地文件大于远程文件，下载中止
				Log.d(TAG, "Download length illegal!");
				return 1;
			} 
			else if (localSize == lRemoteSize) 
			{
				Log.d(TAG, "Download complete!");
				return 2;
			}
			// 进行断点续传，并记录状态
			FileOutputStream out = new FileOutputStream(f, true);
			mFtpClient.setRestartOffset(localSize);
			InputStream in = mFtpClient.retrieveFileStream(new String(remote
					.getBytes("GBK"), "iso-8859-1"));
			byte[] bytes = new byte[1024];
			long step = lRemoteSize / 100;
			long process = localSize / step;
			int c;
			while ((c = in.read(bytes)) != -1) 
			{
				out.write(bytes, 0, c);
				localSize += c;
				long nowProcess = localSize / step;
				if (nowProcess > process) 
				{
					process = nowProcess;
					//if (process % 10 == 0)
					//{
						int i = (int)process;
						System.out.println("下载进度：" + process);
						UIHandler.initBusiStep("下载进度","下载进度..." ,i);
					//}
					// 更新文件下载进度,值存放在process变量中
				}
			}
			UIHandler.initBusiStop();
			in.close();
			out.close();
			boolean isDo = mFtpClient.completePendingCommand();
			if (isDo)
			{
				// Download_From_Break_Success
				Log.d(TAG, "Download_From_Break_Success!");
				return 3;
			}
			else
			{
				// Download_From_Break_Failed
				Log.d(TAG, "Download_From_Break_Failed!");
				return 4;
			}
		}
		// 本地不存在文件，进行完全下载
		else
		{
			OutputStream out = new FileOutputStream(f);
			InputStream in = mFtpClient.retrieveFileStream(new String(remote
					.getBytes("GBK"), "iso-8859-1"));
			byte[] bytes = new byte[1024];
			long step;
			step = lRemoteSize / 100;
			long process = 0;
			long localSize = 0L;
			int c;
			while ((c = in.read(bytes)) != -1) 
			{
				out.write(bytes, 0, c);
				localSize += c;
				long nowProcess = localSize / step;
				if (nowProcess > process)
				{
					process = nowProcess;
					//if (process % 10 == 0)
					//{
						int i = (int)process;
						System.out.println("下载进度：" + process);
						UIHandler.initBusiStep("下载进度","下载进度..." , i);
					//}
				}
			}
			UIHandler.initBusiStop();
			
			System.out.println(bytes);
			mCrc = CrcUtil.Pro_General_CalcCrc(bytes, 0, bytes.length);
			in.close();
			out.close();
			boolean upNewStatus = mFtpClient.completePendingCommand();
			if (upNewStatus)
			{
				// Download_New_Success
				Log.d(TAG, "Download_New_Success!");
				return 5;
			} 
			else 
			{
				// Download_New_Failed
				Log.d(TAG, "Download_New_Failed!");
				return 6;
			}
		}
	}
	
	// 获取项目的版本号
	public static String getProjectVersion(Context context)
	{
		try 
		{
			// 获取packagemanager的实例
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageManager packageManager =	context.getPackageManager();
			PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			return packInfo.versionName;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "0.0.0";
		}
	}
	
	//	获取指定目录下的文件名称列表
	public static String[] GetFileName(String currentDir) 
	{
		String[] dirs = null;
		 try 
		 {
			 if(currentDir==null)
				 dirs = mFtpClient.listNames();
			 else
				 dirs = mFtpClient.listNames(currentDir);
		 } 
		 catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
		 return dirs;
	}
	
	//	获取指定目录下的文件与目录信息集合
	public static FTPFile[] GetDirAndFilesInfo(String currentDir)
	{
		FTPFile[] files=null;
		 try
		 {
			 if(currentDir==null)
				 files=mFtpClient.listFiles();
			 else
				 files = mFtpClient.listFiles(currentDir);
		 }
		 catch(IOException ex)
		 {
			 ex.printStackTrace();
		 }
		 return files;
	}
	
	// 断开连接
	public static void disconnect() throws IOException
	{
		if (mFtpClient.isConnected())
		{
			mFtpClient.disconnect();
		}
	}
	
	// 版本判断，每个.来判断
	// true 升级
	public static boolean versionCompare(String strOld, String strNew)
	{
		String str1,str2;
		int pos;
		for (int i = 0; i < 3; i++) 
		{
			System.out.println("Old : " + strOld + "  New : " + strNew);
			pos = strOld.indexOf(".");
			// 没有找到"."返回-1
			if(pos <= 0 || pos >= strOld.length())
			{
				// 最后一个
				str1 = strOld;
			}
			// 分为两部分
			else 
			{
				str1 		= 	strOld.substring(0, pos);
				strOld 	= 	strOld.substring(pos + 1);
			} 
			pos = strNew.indexOf(".");
			if(pos <= 0 || pos >= strNew.length())
			{
				str2 = strNew;
			}
			else 
			{
				str2 = strNew.substring(0, pos);
				strNew = strNew.substring(pos + 1);
			}
			// System.out.println("str1 : " + str1 + "  str2 : " + str2);
			if(Integer.parseInt(str1) > Integer.parseInt(str2))
			{
				return false;
			}
			else if(Integer.parseInt(str1) < Integer.parseInt(str2))
			{
				return true;
			}
		}
		return false;
	}
}