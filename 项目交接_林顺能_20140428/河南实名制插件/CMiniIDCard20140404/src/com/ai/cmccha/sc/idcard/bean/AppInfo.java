package com.ai.cmccha.sc.idcard.bean;

import android.graphics.drawable.Drawable;

public class AppInfo
{
	private String 	 appName;      //应用程序名字
	private String 	 appVersion;   //应用程序版本
	private Drawable drawable;	 
	private Boolean  isUserApp;    //是否是用户程序
	private String   packageName;  //包名

	public String getPackageName() 
	{
		return packageName;
	}

	public void setPackageName(String packageName) 
	{
		this.packageName = packageName;
	}

	public String getAppName()
	{
		return appName;
	}

	public Boolean getIsUserApp() 
	{
		return isUserApp;
	}

	public void setIsUserApp(Boolean isUserApp)
	{
		this.isUserApp = isUserApp;
	}

	public void setAppName(String appName)
	{
		this.appName = appName;
	}

	public String getAppVersion()
	{
		return appVersion;
	}

	public void setAppVersion(String appVersion)
	{
		this.appVersion = appVersion;
	}

	public Drawable getDrawable()
	{
		return drawable;
	}

	public void setDrawable(Drawable drawable)
	{
		this.drawable = drawable;
	}

}
