package gionee.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.Settings;
import com.mediatek.common.featureoption.FeatureOption;

public class GnSettings {

    public static class System {

        public static String GPRS_CONNECTION_SETTING = Settings.System.GPRS_CONNECTION_SETTING;
        public static long DEFAULT_SIM_NOT_SET = Settings.System.DEFAULT_SIM_NOT_SET;
        public static String GPRS_CONNECTION_SIM_SETTING = Settings.System.GPRS_CONNECTION_SIM_SETTING;
        public static long GPRS_CONNECTION_SIM_SETTING_NEVER = Settings.System.GPRS_CONNECTION_SIM_SETTING_NEVER;
        public static int GPRS_CONNECTION_SETTING_DEFAULT = Settings.System.GPRS_CONNECTION_SETTING_DEFAULT;
        public static Uri DEFAULT_RINGTONE_URI = Settings.System.DEFAULT_RINGTONE_URI;
        public static String SMS_SIM_SETTING = Settings.System.SMS_SIM_SETTING;
        public static long DEFAULT_SIM_SETTING_ALWAYS_ASK = Settings.System.DEFAULT_SIM_SETTING_ALWAYS_ASK;
        public static String ENABLE_INTERNET_CALL = Settings.System.ENABLE_INTERNET_CALL;
        public static String AIRPLANE_MODE_ON = Settings.System.AIRPLANE_MODE_ON;
        public static long getLong(ContentResolver contentResolver,
                String gprsConnectionSimSetting, long defaultSimNotSet) {
            // TODO Auto-generated method stub
        	return Settings.System.getLong(contentResolver, gprsConnectionSimSetting, defaultSimNotSet);
        }

        public static int getInt(ContentResolver cr, String name, int def) {
            return Settings.System.getInt(cr, name, def);
        }

        public static int getInt(ContentResolver contentResolver, String gprsConnectionSetting,
                String gprsConnectionSettingDefault) {
            // TODO Auto-generated method stub
        	//Settings.System.getInt(contentResolver, gprsConnectionSetting, gprsConnectionSettingDefault);
		return 0;
        }

        /**
         * Roaming reminder mode<br/>
         * <b>Values: sim ID</b><br/>
         * 0 - once.<br/>
         * 1 - Always ask.<br/>
         * 2 - Never.<br/>  
         * @hide          
         */
        public static String ROAMING_REMINDER_MODE_SETTING = Settings.System.ROAMING_REMINDER_MODE_SETTING;

        /**
         * Dual SIM mode setting.<br/>
         * <b>Values:</b><br/>
         * 1 - SIM1 only mode.<br/>
         * 2 - SIM2 only mode.<br/>
         * 3 - Dual SIM mode.<br/>         
         * 4 - Flight mode.<br/> 
         * @hide                 
         */
        public static String DUAL_SIM_MODE_SETTING = Settings.System.DUAL_SIM_MODE_SETTING;

        /**
         * voice call default sim<br/>
         * <b>Values: sim ID</b><br/>
         * @hide
         */
        public static String VOICE_CALL_SIM_SETTING = Settings.System.VOICE_CALL_SIM_SETTING;

        /**
         * Voice call setting as Internet call  
         * @hide 
         */
        public static long VOICE_CALL_SIM_SETTING_INTERNET = Settings.System.VOICE_CALL_SIM_SETTING_INTERNET;

        /**
         * video call default sim<br/>
         * <b>Values: sim ID</b><br/>
         * @hide
         */
        public static String VIDEO_CALL_SIM_SETTING = Settings.System.VIDEO_CALL_SIM_SETTING;
        
        // Gionee: 20120918 chenrui add for CR00696600 begin
        public static String ALERT_MISS_MSG = "alert_miss_msg";
        public static String ALERT_MISS_MSG_INTERVAL = "alert_miss_msg_interval";
        // Gionee: 20120918 chenrui add for CR00696600 end
        
        //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 begin
        /**
         * Subscription to be used for data call on a multi sim device. The supported values
         * are 0 = SUB1, 1 = SUB2.
         * @hide
         */
       public static String MULTI_SIM_DATA_CALL_SUBSCRIPTION = "multi_sim_data_call";
       //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 end
    }
}
