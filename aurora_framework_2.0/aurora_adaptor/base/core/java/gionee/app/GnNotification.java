package gionee.app;

import android.app.Notification;

public class GnNotification extends Notification {
    // [SystemUI] Support "Dual SIM". {
    /**
     * The showing type of SIM info in the status bar.
     * 0-not shown.
     * 1-shown at notification item.
     * 2-shown at status bar.
     * 3-shown at both notification item and status bar.
     */
    //public int simInfoType;

    /**
     * The SIM id for notification in the status bar.
     */
    //public long simId;
    // [SystemUI] Support "Dual SIM". }
	public GnNotification(){
		super();
	}
}
