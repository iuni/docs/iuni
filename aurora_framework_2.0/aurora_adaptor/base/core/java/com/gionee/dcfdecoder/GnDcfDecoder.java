
package com.gionee.dcfdecoder;

import com.mediatek.dcfdecoder.DcfDecoder;

import android.net.Uri;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.content.ContentResolver;

public class GnDcfDecoder {

    public static Bitmap forceDecodeUri(ContentResolver cr, Uri uri, Options opts, boolean consume) {

        return new DcfDecoder().forceDecodeUri(cr, uri, opts, consume);
    }

}
