package gionee.telephony;

import android.content.Context;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyProperties;
import android.os.ServiceManager;

public class GnTelephonyManager {
    //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 begin
    /** SIM card state: SIM Card Deactivated, only the sim card is activated,
     *   we can get the other sim card state(eg:ready, pin lock...)
     *@hide
     */
     public static int SIM_STATE_DEACTIVATED = 0x0A;
   //Gionee guoyx 20130223 add for Qualcomm solution CR00773050 end

    private static GnTelephonyManager mInstance = new GnTelephonyManager();
    //Gionee guoyx 20130218 add for CR00766605 begin
    //This is use for Quclomm solution if in the MTK solution return the value false. 
    public static boolean isMultiSimEnabled() {
        return false;
    }
	//Gionee guoyx 20130218 add for CR00766605 end
    
    public static GnTelephonyManager getDefault() {
        return mInstance;
    }

    public static boolean hasIccCardGemini(int geminiSim2) {
        return telManager.hasIccCardGemini(geminiSim2);
    }
    
    private static TelephonyManager telManager = TelephonyManager.getDefault();

    public static String getLine1Number() {
        return telManager.getLine1Number();
    }

    public static boolean hasIccCard() {
        return telManager.hasIccCard();
    }

    public static int getDataState() {
        return telManager.getDataState();
    }

    public static String getLine1NumberGemini(int slotId) {
//        return MmsApp.getApplication().getTelephonyManager().getLine1NumberGemini(slotId);
        return telManager.getLine1Number();
    }
    
    public static boolean isNetworkRoamingGemini(int simId) {
//        return (simId == Phone.GEMINI_SIM_1)
//            ? "true".equals(SystemProperties.get(TelephonyProperties.PROPERTY_OPERATOR_ISROAMING))
//            : "true".equals(SystemProperties.get(TelephonyProperties.PROPERTY_OPERATOR_ISROAMING_2));
    	return telManager.isNetworkRoamingGemini(simId);
    }

    public static int getDataStateGemini(int simId) {
    	return telManager.getDataStateGemini(simId);
    }
    
    public static void listenGemini(PhoneStateListener listener, int state, int simId) {
//        ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE))
//                .listenGemini(listener, state, simId);
    	telManager.listenGemini(listener, state, simId);
    }
    
    public static int getCallStateGemini(int simId) {
//        return ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE))
//                .getCallStateGemini(simId);
    	return telManager.getCallStateGemini(simId);
    }

    public static int getSimStateGemini(int currentSlotId) {
        return telManager.getSimStateGemini(currentSlotId);
    }
    
    public static String getVoiceMailNumberGemini(int slotId) {
        return telManager.getVoiceMailNumberGemini(slotId);
    }
    
    public static String getDeviceIdGemini(int simId) {
        return telManager.getDeviceIdGemini(simId);
    }
    
    public static String getSN() {
        ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
        
        try {
            return iTel.getSN();
        } catch (Exception ex) {
            // the phone process is restarting.
            return null;
        }
    }

    public static String getNetworkOperatorGemini(int simId) {
        return telManager.getNetworkOperatorGemini(simId);
    }

    public static String getSubscriberIdGemini(int simId) {
        return telManager.getSubscriberIdGemini(simId);
    }
    
    public static String getSimOperatorGemini(int simId) {
        return telManager.getSimOperatorGemini(simId);
    }

    //gionee fengxb 2013-1-24 add for CR00767833 start
    private static int[] sSimStatusViews;

    private static final int SIM_STATUS_COUNT = 9;

    public static int getSIMStateIcon(int simStatus) {
        if (simStatus <= -1 || simStatus >= SIM_STATUS_COUNT) {
            return -1;
        }
        if (sSimStatusViews == null) {
            initStatusIcons();
        }
        return sSimStatusViews[simStatus];
    }

    public static void initStatusIcons() {
        if (sSimStatusViews == null) {
            sSimStatusViews = new int[SIM_STATUS_COUNT];
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_RADIOOFF] = com.android.internal.R.drawable.zzzzz_gn_sim_radio_off;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_LOCKED] = com.android.internal.R.drawable.zzzzz_gn_sim_locked;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_INVALID] = com.android.internal.R.drawable.zzzzz_gn_sim_invalid;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_SEARCHING] = com.android.internal.R.drawable.zzzzz_gn_sim_searching;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_ROAMING] = com.android.internal.R.drawable.zzzzz_gn_sim_roaming;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_CONNECTED] = com.android.internal.R.drawable.zzzzz_gn_sim_connected;
            sSimStatusViews[PhoneConstants.SIM_INDICATOR_ROAMINGCONNECTED] = com.android.internal.R.drawable.zzzzz_gn_sim_roaming_connected;
        }
    }
    //gionee fengxb 2013-1-24 add for CR00767833 end
    
    public static boolean isValidSimState(int slotId) {
        return false;
    }
    
    public static int getIccSubSize(int slotId) {
        return 0;
    }
    
    //gionee jiaoyuan 20130329 add by CR00791251 start
    public static boolean isVTCallActive() {
    	  return false;
    }
    
    public static void showCallScreenWithDialpad(boolean show) {
    	
    }
    //gionee jiaoyuan 20130329 add by CR00791251 end

	public static int getDefaultSubscription(Context context) {
		return 0;
	}
	
	public static int getPreferredDataSubscription(Context context) {
		return 0;
	}
	
	public static String getNetworkOperatorNameGemini(int simId) {
		return telManager.getNetworkOperatorName();
	}
	
	public static boolean isSimClosed(Context context, int slot) {
        return false;
	}
}
