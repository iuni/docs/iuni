package com.gionee.internal.telephony;

import java.util.List;
import com.android.internal.telephony.UsimGroup;
import com.android.internal.telephony.IIccPhoneBook;

public class GnIIccPhoneBook {
    
    public static boolean removeUSIMGroupById(IIccPhoneBook icc, int groupId) {
        try {
            return icc.removeUSIMGroupById(groupId);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static List<UsimGroup> getUsimGroups(IIccPhoneBook icc) {
        try {
            return icc.getUsimGroups();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static int insertUSIMGroup(IIccPhoneBook icc, String grpName) {
        try {
            return icc.insertUSIMGroup(grpName);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public static int updateUSIMGroup(IIccPhoneBook icc, int nGasId, String grpName) {
        try {
            return icc.updateUSIMGroup(nGasId, grpName);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public static boolean addContactToGroup(IIccPhoneBook icc, int adnIndex, int grpIndex) {
        try {
            return icc.addContactToGroup(adnIndex, grpIndex);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static boolean removeContactFromGroup(IIccPhoneBook icc, int adnIndex, int grpIndex) {
        try {
            return icc.removeContactFromGroup(adnIndex, grpIndex);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static int getUSIMGrpMaxNameLen(IIccPhoneBook icc) {
        try {
            return icc.getUSIMGrpMaxNameLen();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public static int getUSIMGrpMaxCount(IIccPhoneBook icc) {
        try {
            return icc.getUSIMGrpMaxCount();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}

