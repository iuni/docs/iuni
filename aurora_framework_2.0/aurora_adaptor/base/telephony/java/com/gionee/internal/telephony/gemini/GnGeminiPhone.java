package com.gionee.internal.telephony.gemini;

import com.android.internal.telephony.gemini.GeminiPhone;

public class GnGeminiPhone{
    public static String EVENT_PRE_3G_SWITCH = GeminiPhone.EVENT_PRE_3G_SWITCH;
    public static String EVENT_3G_SWITCH_DONE = GeminiPhone.EVENT_3G_SWITCH_DONE;
    public static String EVENT_3G_SWITCH_START_MD_RESET = GeminiPhone.EVENT_3G_SWITCH_START_MD_RESET;
    public static String EVENT_3G_SWITCH_LOCK_CHANGED = GeminiPhone.EVENT_3G_SWITCH_LOCK_CHANGED;
    public static String EXTRA_3G_SWITCH_LOCKED = GeminiPhone.EXTRA_3G_SWITCH_LOCKED;
    public static String EXTRA_3G_SIM = GeminiPhone.EXTRA_3G_SIM;
}
