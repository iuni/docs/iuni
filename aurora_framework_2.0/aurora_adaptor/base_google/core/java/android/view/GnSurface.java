package android.view;
import android.graphics.Bitmap;
import android.os.Build;
import java.lang.reflect.Method;
public class GnSurface{

public static Bitmap screenshot(int width, int height)
{
  if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.view.Surface");
                  Method method=sPolicy.getMethod("screenshot", int.class,int.class);
                  return  (Bitmap)method.invoke(null,width, height);
          }catch(Exception e){}
    }
  else
  {
    try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.view.SurfaceControl");
                  Method method=sPolicy.getMethod("screenshot", int.class,int.class);
                  return (Bitmap)method.invoke(null,width, height);
      }catch(Exception e){}
  } 
 return null;

}
}
