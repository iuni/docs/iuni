/**
	File Description:
		An observer for observing and sending sticky broadcasts of flip states.
	Author: fengjy@gionee.com
	Create Date: 2012/04/19
	Change List:
*/

package com.gionee.server;

import android.app.ActivityManagerNative;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.UserHandle;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.UEventObserver;
import android.util.Log;

import java.io.FileReader;
import java.io.FileNotFoundException;

public class GnFlipObserver extends UEventObserver {
    private static final String TAG = GnFlipObserver.class.getSimpleName();
    private static final boolean LOG = true;

    private static final String FLIP_UEVENT_MATCH = "DEVPATH=/devices/virtual/switch/hall";
    private static final String FLIP_STATE_PATH = "/sys/class/switch/hall/state";
    private static final String FLIP_NAME_PATH = "/sys/class/switch/hall/name";

    private int mFlipState;
    private int mPrevFlipState;
    private String mFlipName;

    private final WakeLock mWakeLock;  // held while there is a pending route change

    public GnFlipObserver(Context context) {
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "FlipObserver");
        mWakeLock.setReferenceCounted(false);

        startObserving(FLIP_UEVENT_MATCH);

        init();  // set initial status
    }

    @Override
    public void onUEvent(UEventObserver.UEvent event) {
        if (LOG) Log.v(TAG, "Flip UEVENT: " + event.toString());

        try {
            update(event.get("SWITCH_NAME"), Integer.parseInt(event.get("SWITCH_STATE")));
        } catch (NumberFormatException e) {
        	Log.e(TAG, "Could not parse switch state from event " + event);
        }
    }

    private synchronized final void init() {
        char[] buffer = new char[1024];

        String newName = mFlipName;
        int newState = mFlipState;
        mPrevFlipState = mFlipState;
        try {
            FileReader file = new FileReader(FLIP_STATE_PATH);
            int len = file.read(buffer, 0, 1024);
            newState = Integer.valueOf((new String(buffer, 0, len)).trim());

            file = new FileReader(FLIP_NAME_PATH);
            len = file.read(buffer, 0, 1024);
            newName = new String(buffer, 0, len).trim();

        } catch (FileNotFoundException e) {
        	Log.w(TAG, "This kernel does not have flip support");
        } catch (Exception e) {
        	Log.e(TAG, "" , e);
        }

        update(newName, newState);
    }

    private synchronized final void update(String newName, int newState) {
        // Retain only relevant bits
        int newOrOld = newState | mFlipState;
        int delay = 0;
        // reject all suspect transitions: only accept state changes from:
        // - a: flip off to flip on
        // - b: flip on to flip off
        if (mFlipState == newState || ((newOrOld & (newOrOld - 1)) != 0)) {
            return;
        }

        mFlipName = newName;
        mPrevFlipState = mFlipState;
        mFlipState = newState;

        if (mHandler.hasMessages(0)) {
            delay = 1000;
        }
        mWakeLock.acquire();
        mHandler.sendMessageDelayed(mHandler.obtainMessage(0,
                                                           mFlipState,
                                                           mPrevFlipState,
                                                           mFlipName),
                                    delay);
    }

    private final void sendIntent(int flipState, int prevFlipState, String flipName) {
        if (flipState != prevFlipState) {
            //  Pack up the values and broadcast them to everyone
            Intent intent = new Intent(Intent.GN_ACTION_FLIP_OVER);
            intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
            intent.putExtra("state", flipState);
            intent.putExtra("name", flipName);

            if (LOG) Log.v(TAG, "Intent.GN_ACTION_FLIP_OVER: state: "+flipState+" name: "+flipName);
            // TODO: Should we require a permission?
            ActivityManagerNative.broadcastStickyIntent(intent, null, UserHandle.USER_ALL);
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            sendIntent(msg.arg1, msg.arg2, (String)msg.obj);
            mWakeLock.release();
        }
    };
}
