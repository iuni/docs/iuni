package com.aurora.internal.widget;

import com.aurora.utils.DensityUtil;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class ShutdownLayout extends LinearLayout {

	public ShutdownLayout(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public ShutdownLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public ShutdownLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public ShutdownLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {    
				setPadding(getPaddingLeft(),DensityUtil.dip2px(getContext(), 136f), getPaddingRight(), getPaddingBottom());
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {    
				setPadding(getPaddingLeft(),DensityUtil.dip2px(getContext(), 276f),  getPaddingRight(), getPaddingBottom());
	    }   
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	

}
