package com.aurora.utils;
import android.content.Context;

public class Utils2Icon extends AbstractIconGetter {
	public static final String TAG = "Utils2Icon";
	public static Utils2Icon mInstance;
	private Utils2Icon(Context context){
		super(context);
	}
	public synchronized static Utils2Icon getInstance(Context context){
		if(mInstance == null){
			mInstance = new Utils2Icon(context);
		}
		return mInstance;
	}
}
