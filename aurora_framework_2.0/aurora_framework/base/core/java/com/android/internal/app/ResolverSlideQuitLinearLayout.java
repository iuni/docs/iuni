package com.android.internal.app;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.widget.LinearLayout;

public class ResolverSlideQuitLinearLayout extends LinearLayout {


	private int mTouchDownY;
	
	private int mTouchSlop = 8;
	
	private int mPositionInScreen;
	
	private boolean mMoved = false;
	
	private ObjectAnimator mAnimator;
	
	private int mHeight;
	
	private int mDuration = 200;

	public ResolverSlideQuitLinearLayout(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public ResolverSlideQuitLinearLayout(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		init();
	}

	public ResolverSlideQuitLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}

	public ResolverSlideQuitLinearLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init();
	}
	
	private void init(){
		mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
	}
	

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		int action = ev.getActionMasked();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			mTouchDownY = (int) ev.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			int currentY = (int) ev.getY();
			Log.d("tr", "onInterceptTouchEvent-->"+getTranslationY());
			mMoved = getTranslationY() >= 0.0f && Math.abs(currentY- mTouchDownY) >mTouchSlop;
			return mMoved ;
		
		}
		return super.onInterceptTouchEvent(ev);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getActionMasked();
		switch (action) {
		case MotionEvent.ACTION_MOVE:

			int currentY = (int) event.getY();
			int translationY = (int) getTranslationY();
			if(/*currentY < mTouchDownY && */translationY >= 0){
				translationY = translationY + (currentY - mTouchDownY);
			}
			if(translationY >=0){
				setTranslationY(translationY);
				Log.d("tr", "trY-->"+getTranslationY());
			}
			mMoved= translationY > 0;
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			if(mMoved){
				float transY = getTranslationY();
				if(transY < mHeight /2){
					playScrollAnimator(transY, 0,false);
				}else{
					playScrollAnimator(transY, mHeight,true);
				}
			}
			
			break;
		
		default:
			break;
		}
		return super.onTouchEvent(event) && mMoved;
	}


	
	private void playScrollAnimator(float currentTranslationY,float targetTranslationY,final boolean finished){
		mAnimator = ObjectAnimator.ofFloat(this, "translationY", currentTranslationY,targetTranslationY);
		mAnimator.setDuration(mDuration);
		mAnimator.setInterpolator(new AccelerateInterpolator());
		mAnimator.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				if(finished){
					Context activity = getContext();
					if(activity instanceof Activity){
						((Activity)activity).finish();
					}
				}
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
		mAnimator.start();
	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		mHeight = getMeasuredHeight();
		mPositionInScreen = getResources().getDisplayMetrics().heightPixels - mHeight;
	}

}
