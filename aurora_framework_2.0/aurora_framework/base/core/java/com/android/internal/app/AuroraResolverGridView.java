package com.android.internal.app;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import aurora.view.ViewPager;
import com.android.internal.app.AuroraResolverActivity.DisplayResolveInfo;
import com.aurora.R;

public class AuroraResolverGridView extends LinearLayout {

	private Context mContext;
	/** 
	 * 保存实体对象链表 
	 * */
	private List<DisplayResolveInfo> mDataList;
	private AuroraGridViewPager mViewPager;
	private LinearLayout mIndexLayout;
	private ImageView[] mIndex;
	/** 
	 * ViewPager当前页 
	 * */
	private int mCurrentIndex;
	/**
	 *  ViewPager页数
	 *   */
	private int mPageCount;
	/** 
	 * 默认一页9个item
	 * 
	 *  */
	private int mPageItemCount = 9;

	/** 
	 * 保存每个页面的GridView视图 
	 * */
	private List<View> mPageListViews;
	
	/**
	 * 最大列数
	 */
	private int mMaxColumns = 3;

	 private  Intent[] mInitialIntents;
	 
	 private  Intent mIntent;
	 
	 private int mLaunchedFromUid;
	    
	 
	 private PackageManager mPm;
	 
	 
	 private List<ResolveInfo> mResolveList;
	 
	 private AuroraResolverActivity mActivity;
	 
	 private onItemSeletctedListener mCallBack;
	 
	 private int mIndexSize;
	 private int mIndexMargin;
	 
	public AuroraResolverGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		initDatas();
	}

	@SuppressWarnings("unchecked")
	public AuroraResolverGridView(Context context) {
		super(context);
		this.mContext = context;
		initDatas();
	}
	
	private void initDatas(){
		mIndexSize = getResources().getDimensionPixelOffset(R.dimen.aurora_page_index_size);
		mIndexMargin =  getResources().getDimensionPixelOffset(R.dimen.aurora_page_index_margin);
	}
	
	public void init( List<DisplayResolveInfo> list,Intent intent,
            Intent[] initialIntents, List<ResolveInfo> rList, int launchedFromUid,PackageManager pm,AuroraResolverActivity activity){
		this.mDataList =  list;
		mInitialIntents = initialIntents;
		mIntent = new Intent(intent);
		mLaunchedFromUid = launchedFromUid;
		mResolveList = rList;
		mPm = pm;
		mActivity = activity;
		initView();
		initDots();
		setAdapter();
	}
	
	public void setCallBack(onItemSeletctedListener callback){
		this.mCallBack = callback;
	}
	
	/**
	 * 设置viewpager的adapter
	 */
	private void setAdapter() {
		mPageListViews = new ArrayList<View>();
		for (int i = 0; i < mPageCount; i++) {
			mPageListViews.add(getViewPagerItem(i));
		}
		mViewPager.setAdapter(new AuroraResolverViewPagerGridViewAdapter(mPageListViews));
	}

	private void initView() {
		View view = LayoutInflater.from(mContext).inflate(R.layout.aurora_resolver_grid_pager, null);
		mViewPager = (AuroraGridViewPager) view.findViewById(R.id.aurora_resolver_pager);
		mIndexLayout = (LinearLayout) view.findViewById(R.id.aurora_parentPanel);
		addView(view);
	}

	
	/**
	 * 初始化底部小圆点
	 */
	private void initDots() {
		int rowNumber = 0;
		int size = mDataList.size();
		if(size % mPageItemCount == 0){
			mPageCount = size / mPageItemCount;
		}else{
			mPageCount = mDataList.size() / mPageItemCount + 1;
		}
			mIndexLayout.removeAllViews();
			if (1 == mPageCount) {
				mIndexLayout.setVisibility(View.GONE);
				if(size <= 3){
					rowNumber = 1;
				}else if(size > 3 && size <= 6){
					rowNumber = 2;
				}else{
					rowNumber = 3;
				}
			} else if (1 < mPageCount) {
				rowNumber = mMaxColumns;
				mIndexLayout.setVisibility(View.VISIBLE);
				for (int j = 0; j < mPageCount; j++) {
					ImageView image = new ImageView(mContext);
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mIndexSize,
							mIndexSize);
					if(j != 0){
						params.leftMargin = mIndexMargin;
					}
					image.setBackgroundResource(R.drawable.aurora_page_index);
					mIndexLayout.addView(image, params);
				}
				
				mIndex = new ImageView[mPageCount];
				for (int i = 0; i < mPageCount; i++) {
					mIndex[i] = (ImageView) mIndexLayout.getChildAt(i);
					mIndex[i].setEnabled(false);
					mIndex[i].setTag(i);
				}
				mCurrentIndex = 0;
				mIndex[mCurrentIndex].setEnabled(true);
				mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

					@Override
					public void onPageSelected(int arg0) {
						setCurDot(arg0);
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub

					}
				});
				
			}
			mViewPager.setGridRowNumber(rowNumber);
	}
	
	
	
	
	

	/** 
	 * 当前底部小圆点
	 * 
	 */
	private void setCurDot(int positon) {
		if (positon < 0 || positon > mPageCount - 1 || mCurrentIndex == positon) {
			return;
		}
		mIndex[positon].setEnabled(true);
		mIndex[mCurrentIndex].setEnabled(false);
		mCurrentIndex = positon;
	}

	/**
	 * 设置每个页中的gridview的数据
	 * @param index
	 * @return
	 */
	private View getViewPagerItem(int index) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.aurora_resolver_grid, null);
		GridView gridView = (GridView)layout.findViewById(R.id.aurora_resolver_grid);
		gridView.setNumColumns(mMaxColumns);
		final ResolveListAdapter adapter = new ResolveListAdapter(mContext, 
				mIntent, 
				mInitialIntents, 
				mResolveList,
				mLaunchedFromUid,
				index, 
				mPageItemCount, 
				mPm, 
				mActivity);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(mCallBack != null){
					int selectedPositon = position + mCurrentIndex * mPageItemCount;
					Intent intent = adapter.intentForPosition(selectedPositon);
					ResolveInfo info = adapter.resolveInfoForPosition(selectedPositon);
					mCallBack.onSelected(selectedPositon, info,intent);
				}
			}
		});
		gridView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if(mCallBack != null){
					int selectedPositon = position + mCurrentIndex * mPageItemCount;
					Intent intent = adapter.intentForPosition(selectedPositon);
					ResolveInfo info = adapter.resolveInfoForPosition(selectedPositon);
					mCallBack.onLongSelected(selectedPositon, info,intent);
				}
				return true;
			}
		});
		return gridView;
	}
	
	public void handlePackagesChanged(){
		for(int i = 0;i < mPageCount;i++){
			GridView grid = (GridView) mPageListViews.get(i);
			((ResolveListAdapter)grid.getAdapter()).handlePackagesChanged();
		}
	}
	
	public interface onItemSeletctedListener{
		
		public void onSelected(int position,ResolveInfo resolverInfo,Intent intent);
		
		public void onLongSelected(int position,ResolveInfo resolverInfo,Intent intent);
		
	}
	
	
	
	
}
