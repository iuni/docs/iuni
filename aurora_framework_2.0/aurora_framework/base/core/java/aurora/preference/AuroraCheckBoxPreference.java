/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aurora.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import aurora.widget.AuroraCheckBox;
import aurora.widget.AuroraCheckedTextView;

/**
 * A {@link Preference} that provides checkbox widget
 * functionality.
 * <p>
 * This preference will store a boolean into the SharedPreferences.
 * 
 * @attr ref android.R.styleable#CheckBoxPreference_summaryOff
 * @attr ref android.R.styleable#CheckBoxPreference_summaryOn
 * @attr ref android.R.styleable#CheckBoxPreference_disableDependentsState
 */
public class AuroraCheckBoxPreference extends AuroraTwoStatePreference {

    private boolean mIsCycle = false;

    public AuroraCheckBoxPreference(
            Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Gionee <zhangxx> <2013-08-14> add for CR00857086 end
            TypedArray a = context.obtainStyledAttributes(attrs,
                    com.aurora.internal.R.styleable.AuroraCheckBoxPreference, defStyleAttr, 0);
            setSummaryOn(a.getString(com.aurora.internal.R.styleable.AuroraCheckBoxPreference_aurorasummaryOn));
            setSummaryOff(a.getString(com.aurora.internal.R.styleable.AuroraCheckBoxPreference_aurorasummaryOff));
            setDisableDependentsState(a.getBoolean(
                    com.aurora.internal.R.styleable.AuroraCheckBoxPreference_auroradisableDependentsState, false));
            setButtonToCycle(a.getBoolean(
                    com.aurora.internal.R.styleable.AuroraCheckBoxPreference_isCycle, false));
            a.recycle();
        // Gionee <zhangxx> <2013-08-14> add for CR00857086 begin
    }

    public AuroraCheckBoxPreference(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.checkBoxPreferenceStyle);
    }

    public AuroraCheckBoxPreference(Context context) {
        this(context, null);
    }

    public void setButtonToCycle(boolean cycle){
    	mIsCycle = cycle;
    }
    
    public boolean buttonIsCycle(){
    	return mIsCycle;
    }
    
    protected void onBindView(View view) {
        super.onBindView(view);

        AuroraCheckBox checkboxView = (AuroraCheckBox) view.findViewById(com.android.internal.R.id.checkbox);
        AuroraCheckedTextView checkedTextView = (AuroraCheckedTextView) view.findViewById(android.R.id.text1);
        Checkable checkableView = buttonIsCycle()?checkedTextView:checkboxView;
		if (buttonIsCycle()) {
			if(checkboxView != null){
				checkboxView.setVisibility(View.GONE);
			}
			if (checkedTextView != null && checkedTextView instanceof Checkable) {
				checkedTextView.setVisibility(View.VISIBLE);
			}
		}
		if(checkableView!=null){
			checkableView.setChecked(mChecked);
		}
        syncSummaryView(view);
    }
}