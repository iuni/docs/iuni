package aurora.widget;

import com.aurora.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * 布局中使用方法<br/>
 *&lt;aurora.widget.AuroraProgressBtn<br/>
        android:id="@+id/progressButton1"<br/>
        android:layout_width="wrap_content"<br/>
        android:layout_height="wrap_content"<br/>
        aurora:roundProgressColor="#CD3333"/><br/>
        aurora:roundProgressColor设置进度条颜色<br/>
 * @author 罗来刚
 *
 */
public class AuroraProgressBtn extends LinearLayout {
	private int progress = 0;
	private AuroraProgressBtnStste status = AuroraProgressBtnStste.START;
	private boolean showProgress = false;
	
	/**
	 * 获取环形进度条是否显示
	 * @return
	 */
	public boolean isShowProgress() {
		return showProgress;
	}

	/**
	 * 设置环形进度条是否显示
	 * @param showProgress 
	 */
	public void setShowProgress(boolean showProgress) {
		this.showProgress = showProgress;
		roundProgressView.setVisibility(showProgress?View.VISIBLE:View.GONE);
	}

	/**
	 * 控件状态
	 * @author luolaigang
	 *
	 */
	public static enum AuroraProgressBtnStste {
		/**
		 * 开始
		 */
		START, 
		/**
		 * 刷新
		 */
		REFRESH, 
		/**
		 * 队列等待
		 */
		QUEUE_WAITING, 
		/**
		 * 点击下载
		 */
		START_DOWNLOAD, 
		/**
		 * 普通等待
		 */
		WAIT
	};

	private RoundProgressBar roundProgressView;
	private ImageView controlBtn;

	public AuroraProgressBtn(Context context) {
		super(context);
	}

	public AuroraProgressBtn(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context, attrs);
		
	}

	private void initView(Context context, AttributeSet attrs) {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(com.aurora.R.layout.aurora_view_progressbtn, this);
		roundProgressView = (RoundProgressBar) findViewById(com.aurora.R.id.roundProgressBar);
		controlBtn = (ImageView) findViewById(com.aurora.R.id.control_btn);
		controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_start);
		TypedArray mTypedArray = context.obtainStyledAttributes(attrs, com.aurora.R.styleable.AuroraRoundProgressBar);	
		int roundProgressColor = mTypedArray.getColor(com.aurora.R.styleable.AuroraRoundProgressBar_roundProgressColor, Color.GREEN);  
		roundProgressView.setCricleProgressColor(roundProgressColor);
	}

	/**
	 * 设置当前图标状态(不包括原型进度条)
	 * @param status
	 */
	public void setStatus(AuroraProgressBtnStste status) {
		if (this.status == status) {
			return;
		}
		this.status = status;
		switch (status) {
		case START:
			controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_start);
			break;
		case REFRESH:
			controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_refresh);
			break;
		case QUEUE_WAITING:
			controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_queue_waiting);
			break;
		case START_DOWNLOAD:
			controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_start_download);
			break;
		case WAIT:
			controlBtn.setImageResource(com.aurora.R.drawable.aurora_progressbtn_wait);
			break;
		}
	}

	/**
	 * 设置进度(进度总数固定为100)
	 * @param progress 进度值
	 */
	public void setProgress(int progress) {
		this.progress = progress;
		roundProgressView.setProgress(progress);
	}

	/**
	 * 获取当前进度
	 * @return
	 */
	public int getProgress() {
		return progress;
	}

	/**
	 * 获取控件状态
	 * @return
	 */
	public AuroraProgressBtnStste getStatus() {
		return status;
	}
}
