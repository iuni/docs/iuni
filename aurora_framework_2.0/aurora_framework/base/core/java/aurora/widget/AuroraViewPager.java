package aurora.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import aurora.view.ViewPager;

/**
 * 提供一个可以禁止左右滑动的ViewPage
 * @author luolaigang
 *
 */
public class AuroraViewPager extends ViewPager {

	private boolean canScroll = true;

	/**
	 * 设置是否可以左右滑动
	 * @param isCanScroll
	 */
	public void setCanScroll(boolean isCanScroll) {
		this.canScroll = isCanScroll;
	}
	
	/**
	 * 获取当前是否可以滑动
	 * @return
	 */
	public boolean isCanScroll() {
		return canScroll;
	}

	public AuroraViewPager(Context context) {
		super(context);
	}

	public AuroraViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void scrollTo(int x, int y) {
		super.scrollTo(x, y);
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (canScroll) {
			return super.onTouchEvent(arg0);
		} else {
			return false;
		}

	}

	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
		// TODO Auto-generated method stub
		super.setCurrentItem(item, smoothScroll);
	}

	@Override
	public void setCurrentItem(int item) {
		// TODO Auto-generated method stub
		super.setCurrentItem(item);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		// TODO Auto-generated method stub
		if (canScroll) {
			return super.onInterceptTouchEvent(arg0);
		} else {
			return false;
		}

	}
}