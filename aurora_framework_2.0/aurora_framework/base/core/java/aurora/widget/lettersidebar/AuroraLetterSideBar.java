package aurora.widget.lettersidebar;


import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class AuroraLetterSideBar extends View{

	protected WindowManager mWindowManager;
	private float mScale = 1.0f;
	private boolean mShowBkg = false;
	public static String[] b = { "#", "A", "B", "C", "D", "E", "F", "G", "H",
		"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
		"V", "W", "X", "Y", "Z" };

	private OnTouchingLetterChangedListener onTouchingLetterChangedListener;	
	private Paint mPaint = new Paint();
	private int choose = -1;
	private final int DEF_TEXT_SIZE = 12;
	
	public AuroraLetterSideBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context);
	}

	public AuroraLetterSideBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	public AuroraLetterSideBar(Context context) {
		super(context);
		initView(context);
	}
	
	private void initView(Context context){
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics displayMetrics = getDisplayMetrics();
		if(displayMetrics != null){
			mScale = displayMetrics.densityDpi/160.0f;
		}
	}
	
	/**
	 * 重写这个方法
	 */
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mShowBkg) {
			//canvas.drawColor(Color.parseColor("#40000000"));
			setBackgroundResource(com.aurora.R.drawable.aurora_letterslidebarback);
		} else {
			setBackground(null);
		}

		int height = getHeight();
		int width = getWidth();
		int singleHeight = height / b.length;//-getResources().getInteger(com.aurora.R.integer.aurora_lettersidebar_text_height);
		for (int i = 0; i < b.length; i++) {
			mPaint.setColor(getResources().getColor(com.aurora.R.color.aurora_lettersidebar_text_color));
			//mPaint.setTypeface(Typeface.DEFAULT_BOLD);
			//mPaint.setAntiAlias(true);
			mPaint.setTextSize(DEF_TEXT_SIZE*mScale);//20
			if (i == choose) {
				mPaint.setColor(getResources().getColor(com.aurora.R.color.aurora_lettersidebar_text_color_pressed));
				mPaint.setFakeBoldText(true);
			}
			float xPos = width / 2 - mPaint.measureText(b[i]) / 2;
			float yPos = singleHeight * i + singleHeight;
			canvas.drawText(b[i], xPos, yPos, mPaint);
			mPaint.reset();
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		final int action = event.getAction();
		final float y = event.getY();
		final int oldChoose = choose;
		final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;
		final int c = (int) (y / getHeight() * b.length);

		switch (action) {
		case MotionEvent.ACTION_DOWN:
			mShowBkg = true;
			if (oldChoose != c && listener != null) {
				if (c >=0 && c < b.length) {
					listener.onTouchingLetterChanged(b[c],y);
					choose = c;
					invalidate();
				}
			}

			break;
		case MotionEvent.ACTION_MOVE:
			if (oldChoose != c && listener != null) {
				if (c >= 0 && c < b.length) {
					listener.onTouchingLetterChanged(b[c],y);
					choose = c;
					invalidate();
				}
			}
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			mShowBkg = false;
			choose = -1;
			invalidate();
			break;
		}
		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	/**
	 * 向外公开的方法
	 * 
	 * @param onTouchingLetterChangedListener
	 */
	public void setOnTouchingLetterChangedListener(
			OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
		this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
	}

	/**
	 * 接口
	 * @author coder
	 */
	public interface OnTouchingLetterChangedListener {
		public void onTouchingLetterChanged(String s,float positionOfY);
	}
	
	private DisplayMetrics getDisplayMetrics(){
		DisplayMetrics metric = new DisplayMetrics();
		mWindowManager.getDefaultDisplay().getMetrics(metric);
		return metric;
	}
}
